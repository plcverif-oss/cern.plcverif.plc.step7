/******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 * 
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.ui.tests

import cern.plcverif.base.models.expr.utils.TypedAstVariable
import cern.plcverif.plc.step7.gui.^extension.impl.Step7AstVariableCollector
import cern.plcverif.plc.step7.step7Language.ProgramFile
import com.google.inject.Inject
import java.util.Collections
import java.util.List
import java.util.Optional
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(XtextRunner)
@InjectWith(Step7LanguageUiInjectorProvider)
class Step7AstVariableCollectorTest {
	@Inject
	ParseHelper<ProgramFile> parseHelper

	@Test
	def void testFbEntry1() {
		val actualResult = collectVariables('''
			FUNCTION_BLOCK fb1
				VAR
					x : BOOL;
				END_VAR
			BEGIN 
				x := TRUE;
			END_FUNCTION_BLOCK 
		''');
		
		Assert.assertEquals(1, actualResult.size);
		
		val xVar = actualResult.get(0);
		Assert.assertEquals("instance.x", xVar.getVariableName());
		Assert.assertEquals("BOOL", xVar.plcType);
		Assert.assertTrue(xVar.topLevel);
		Assert.assertTrue(xVar.isLeaf);
	}

	@Test
	def void testFbEntry2() {
		val actualResult = collectVariables('''
			FUNCTION_BLOCK fb1
				VAR
					x : BOOL;
					y : INT;
				END_VAR
			BEGIN 
				x := TRUE;
			END_FUNCTION_BLOCK 
		''');
		
		Assert.assertEquals(2, actualResult.size);
		
		val xVar = actualResult.get(0);
		Assert.assertEquals("instance.x", xVar.getVariableName());
		Assert.assertEquals("BOOL", xVar.plcType);
		Assert.assertTrue(xVar.topLevel);
		Assert.assertTrue(xVar.isLeaf);
		
		val yVar = actualResult.get(1);
		Assert.assertEquals("instance.y", yVar.getVariableName());
		Assert.assertEquals("INT", yVar.plcType);
		Assert.assertTrue(yVar.topLevel);
		Assert.assertTrue(yVar.isLeaf);
	}
		
	@Test
	def void testFcEntry() {
		val actualResult = collectVariables('''
			FUNCTION fc1 : VOID
				VAR_INPUT
					x : BOOL;
				END_VAR
			BEGIN 
			END_FUNCTION
		''');
		
		Assert.assertEquals(1, actualResult.size);
		
		val xVar = actualResult.get(0);
		Assert.assertEquals("fc1.x", xVar.getVariableName());
		Assert.assertEquals("BOOL", xVar.plcType);
		Assert.assertTrue(xVar.topLevel);
		Assert.assertTrue(xVar.isLeaf);
	}
	
	@Test
	def void testObEntry() {
		val actualResult = collectVariables('''
			ORGANIZATION_BLOCK OB1
				VAR_TEMP
					x : BOOL;
				END_VAR
			BEGIN 
			END_ORGANIZATION_BLOCK
		''');
		
		Assert.assertEquals(0, actualResult.size);
	}
	
	@Test
	def void testInstanceDb() {
		val actualResult = collectVariables('''
			FUNCTION_BLOCK fb1
				VAR
					x : BOOL;
				END_VAR
			BEGIN 
			END_FUNCTION_BLOCK
			
			DATA_BLOCK db1 fb1
			BEGIN
			END_DATA_BLOCK
		''');
		
		Assert.assertEquals(2, actualResult.size);
		
		val xVar = actualResult.get(0);
		Assert.assertEquals("instance.x", xVar.getVariableName());
		Assert.assertEquals("BOOL", xVar.plcType);
		Assert.assertTrue(xVar.topLevel);
		Assert.assertTrue(xVar.isLeaf);
		
		val dbxVar = actualResult.get(1);
		Assert.assertEquals("db1.x", dbxVar.getVariableName());
		Assert.assertEquals("BOOL", dbxVar.plcType);
		Assert.assertTrue(dbxVar.topLevel);
		Assert.assertTrue(dbxVar.isLeaf);
	}
	
	@Test
	def void testSharedDb() {
		val actualResult = collectVariables('''
			FUNCTION_BLOCK fb1
				VAR
					x : BOOL;
				END_VAR
			BEGIN 
				x := TRUE;
			END_FUNCTION_BLOCK
			
			DATA_BLOCK shareddb1 
			STRUCT
				z : INT;
			END_STRUCT
			BEGIN
			END_DATA_BLOCK
		''');
		
		Assert.assertEquals(2, actualResult.size);
		
		{
			val xVar = actualResult.get(0);
			Assert.assertEquals("instance.x", xVar.getVariableName());
			Assert.assertEquals("BOOL", xVar.plcType);
			Assert.assertTrue(xVar.topLevel);
			Assert.assertTrue(xVar.isLeaf);
		}

		{
			val zVar = actualResult.get(1);
			Assert.assertEquals("shareddb1.z", zVar.getVariableName());
			Assert.assertEquals("INT", zVar.plcType);
			Assert.assertTrue(zVar.topLevel);
			Assert.assertTrue(zVar.isLeaf);
		}
	}
	
	@Test
	def void testArray() {
		val actualResult = collectVariables('''
			FUNCTION_BLOCK fb1
				VAR
					x : ARRAY[1..3] OF INT;
				END_VAR
			BEGIN 
			END_FUNCTION_BLOCK 
		''');
		
		Assert.assertEquals(4, actualResult.size);
		
		val xVar = actualResult.get(0);
		Assert.assertEquals("instance.x", xVar.getVariableName());
		Assert.assertTrue(xVar.topLevel);
		Assert.assertFalse(xVar.isLeaf);
		
		for (i : 1..3) {
			val arrayElementVar = actualResult.get(i);
			Assert.assertEquals('''instance.x[«i»]'''.toString, arrayElementVar.getVariableName());
			Assert.assertEquals("INT", arrayElementVar.plcType);
			//Assert.assertTrue(arrayElementVar.getCfaType() instanceof IntType);
			Assert.assertFalse(arrayElementVar.topLevel);
			Assert.assertTrue(arrayElementVar.isLeaf);
		}
	}
	
	@Test
	def void testStruct() {
		val actualResult = collectVariables('''
			FUNCTION_BLOCK fb1
				VAR
					s : STRUCT
							a : BOOL;
							b : INT;
						END_STRUCT;
				END_VAR
			BEGIN 
			END_FUNCTION_BLOCK 
		''');
		
		Assert.assertEquals(3, actualResult.size);
		
		val sVar = actualResult.get(0);
		Assert.assertEquals("instance.s", sVar.getVariableName());
		Assert.assertEquals("STRUCT", sVar.plcType);
		Assert.assertTrue(sVar.topLevel);
		Assert.assertFalse(sVar.isLeaf);
		
		val aVar = actualResult.get(1);
		Assert.assertEquals("instance.s.a", aVar.getVariableName());
		Assert.assertEquals("BOOL", aVar.plcType);
		Assert.assertFalse(aVar.topLevel);
		Assert.assertTrue(aVar.isLeaf);
		
		val bVar = actualResult.get(2);
		Assert.assertEquals("instance.s.b", bVar.getVariableName());
		Assert.assertEquals("INT", bVar.plcType);
		Assert.assertFalse(bVar.topLevel);
		Assert.assertTrue(bVar.isLeaf);
	}
	
	@Test
	def void testUdt() {
		val actualResult = collectVariables('''
			FUNCTION_BLOCK fb1
				VAR
					s : userDefined;
				END_VAR
			BEGIN 
			END_FUNCTION_BLOCK
			
			TYPE userDefined
			STRUCT
				a : BOOL;
				b : INT;
			END_STRUCT
			END_TYPE 
		''');
		
		Assert.assertEquals(3, actualResult.size);
		
		val sVar = actualResult.get(0);
		Assert.assertEquals("instance.s", sVar.getVariableName());
		Assert.assertEquals("STRUCT", sVar.plcType);
		Assert.assertTrue(sVar.topLevel);
		Assert.assertFalse(sVar.isLeaf);
		
		val aVar = actualResult.get(1);
		Assert.assertEquals("instance.s.a", aVar.getVariableName());
		Assert.assertEquals("BOOL", aVar.plcType);
		Assert.assertFalse(aVar.topLevel);
		Assert.assertTrue(aVar.isLeaf);
		
		val bVar = actualResult.get(2);
		Assert.assertEquals("instance.s.b", bVar.getVariableName());
		Assert.assertEquals("INT", bVar.plcType);
		Assert.assertFalse(bVar.topLevel);
		Assert.assertTrue(bVar.isLeaf);
	}
	
	private def List<TypedAstVariable> collectVariables(CharSequence plcProgram) {
		val ast = parseHelper.parse(plcProgram);
		
		Assert.assertNotNull(ast);
		return Step7AstVariableCollector.collectAll(Collections.singletonList(ast), Optional.of(ast.programUnits.get(0)));
	}
}
