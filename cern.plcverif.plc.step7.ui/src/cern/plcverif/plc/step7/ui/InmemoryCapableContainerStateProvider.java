package cern.plcverif.plc.step7.ui;

import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.resource.containers.IAllContainersState;
import org.eclipse.xtext.resource.containers.ResourceSetBasedAllContainersStateProvider;
import org.eclipse.xtext.ui.containers.ContainerStateProvider;

import com.google.inject.Inject;

import cern.plcverif.plc.step7.ui.InmemoryCapableResourceDescriptionsProvider.InmemoryCapableResourceSetBasedResourceDescriptions;

public class InmemoryCapableContainerStateProvider extends ContainerStateProvider {
	@Inject
	private ResourceSetBasedAllContainersStateProvider containerStateProvider;
	
	@Override
	public IAllContainersState get(IResourceDescriptions context) {
		if (context instanceof InmemoryCapableResourceSetBasedResourceDescriptions) {
			return containerStateProvider.get(context);
		}
		return super.get(context);
	}
}
