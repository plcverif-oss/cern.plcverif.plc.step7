/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 * 
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.ui.contentassist

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.plc.step7.indexing.Step7LanguageIndex
import cern.plcverif.plc.step7.scoping.Step7LanguageScopeProvider
import cern.plcverif.plc.step7.services.Step7LanguageGrammarAccess
import cern.plcverif.plc.step7.step7Language.ArrayDT
import cern.plcverif.plc.step7.step7Language.ArrayRef
import cern.plcverif.plc.step7.step7Language.CallParameterList
import cern.plcverif.plc.step7.step7Language.DataBlock
import cern.plcverif.plc.step7.step7Language.DeclarationSection
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit
import cern.plcverif.plc.step7.step7Language.FbOrUdtDT
import cern.plcverif.plc.step7.step7Language.FunctionBlock
import cern.plcverif.plc.step7.step7Language.Label
import cern.plcverif.plc.step7.step7Language.LeftValue
import cern.plcverif.plc.step7.step7Language.NamedCallParameterItem
import cern.plcverif.plc.step7.step7Language.NamedConstantDeclaration
import cern.plcverif.plc.step7.step7Language.NamedElement
import cern.plcverif.plc.step7.step7Language.NamedValueRef
import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.step7Language.ProgramUnit
import cern.plcverif.plc.step7.step7Language.QualifiedRef
import cern.plcverif.plc.step7.step7Language.SclAssignmentStatement
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall
import cern.plcverif.plc.step7.step7Language.Step7LanguagePackage
import cern.plcverif.plc.step7.step7Language.SubroutineCall
import cern.plcverif.plc.step7.step7Language.Variable
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock
import cern.plcverif.plc.step7.step7Language.VariableDeclarationDirection
import cern.plcverif.plc.step7.util.DataTypeUtil
import cern.plcverif.plc.step7.util.SimpleExpressionUtil
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import com.google.common.base.Preconditions
import com.google.inject.Inject
import java.util.Collections
import java.util.List
import java.util.Optional
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.jface.viewers.StyledString
import org.eclipse.swt.graphics.Image
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.TerminalRule
import org.eclipse.xtext.nodemodel.ICompositeNode
import org.eclipse.xtext.nodemodel.INode
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor

import static extension cern.plcverif.plc.step7.util.Step7LanguageHelper.*

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
class Step7LanguageProposalProvider extends AbstractStep7LanguageProposalProvider {
	@Inject
	Step7LanguageIndex index;

	@Inject Step7LanguageGrammarAccess access

	override completeKeyword(Keyword keyword, ContentAssistContext contentAssistContext,
		ICompletionProposalAcceptor acceptor) {
		val model = contentAssistContext.currentModel;

		val enclosingBlock = EmfHelper.getContainerOfType(model, ExecutableProgramUnit);
		if (enclosingBlock !== null && Step7LanguageHelper.isSclBlock(enclosingBlock)) {
			// The following keywords are forbidden in any SCL code.
			if (isAnyOf(
				keyword,
				access.stwBitRefMnemonicAccess.BIT_RESULTBRKeyword_2_0, // BR
				access.stwBitRefMnemonicAccess.OVERFLOW_STOREDOSKeyword_1_0, // OS
				access.stwBitRefMnemonicAccess.OVERFLOWOVKeyword_0_0, // OV
				access.flagRefAccess.RET_VALKeyword_2_1_0, // RET_VAL
				access.flagRefAccess.RET_VALKeyword_2_1_1 // #RET_VAL
			)) {
				return;
			}
		}

		if (model instanceof SclSubroutineCall) {
			// Don't propose expression keywords in calls, their usage is highly unlikely
			if (isAnyOf(
				keyword,
//				access.stwBitRefMnemonicAccess.BIT_RESULTBRKeyword_2_0, // BR
				access.flagRefAccess.ENOKeyword_0_1, // ENO
				access.standaloneExpressionAccess.NILKeyword_2_1, // NIL
				access.flagRefAccess.OKKeyword_1_1, // OK
//				access.stwBitRefMnemonicAccess.OVERFLOW_STOREDOSKeyword_1_0, // OS
//				access.stwBitRefMnemonicAccess.OVERFLOWOVKeyword_0_0, // OV
				access.flagRefAccess.RET_VALKeyword_2_1_0, // RET_VAL
				access.flagRefAccess.RET_VALKeyword_2_1_1, // #RET_VAL
				access.unaryOperatorAccess.NOTNOTKeyword_0_0, // NOT
				access.unaryOperatorAccess.PLUSPlusSignKeyword_1_0, // +
				access.unaryOperatorAccess.MINUSHyphenMinusKeyword_2_0 // -
			)) {
				return;
			}
		}
		if (keyword === access.standaloneExpressionAccess.NILKeyword_2_1) {
			println('''model: «contentAssistContext.currentModel»''')
			return
		}
		super.completeKeyword(keyword, contentAssistContext, acceptor);
	}

	override completeDataBlock_Structure(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		proposeAllVisibleFunctionBlocks(model, context, acceptor, "");
		proposeAllVisibleUdts(model, context, acceptor);
		acceptor.accept(createCompletionProposal("STRUCT", "STRUCT", getImage(model), context));
	}

	override completeDirectNamedRef_Ref(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {

		if (model instanceof SclSubroutineCall) {
			println(".")
			return;
		}

		if (model instanceof QualifiedRef) {
			// no qualified references to constants
			return;
		}

		if (lastNonWsNodeIsDot(context.currentNode)) {
			println(
				"It seems that the content assist tries to complete a qualified reference. Do not suggest unqualified references.");
			return;
		}

		val parentProgramUnit = EcoreUtil2.getContainerOfType(model, ProgramUnit);
		if (parentProgramUnit instanceof DataBlock) {
			// suggestion in DB initialization section
			proposeVariables(parentProgramUnit.allDefinedVariables.filter(Variable), model, context, acceptor);
		} else {
			Preconditions.checkState(parentProgramUnit instanceof ExecutableProgramUnit);

			// suggestion in program block
			val declSection = (parentProgramUnit as ExecutableProgramUnit).declarationSection;

			// suggest all locally defined top-level variables
			proposeVariables(declSection.eAllContents.filter(Variable).toIterable, model, context, acceptor);

			// suggest all local constants
			proposeAllNamedConstantsOfBlock(declSection, model, context, acceptor);

			// suggest all global variables
			val parentFile = EmfHelper.getContainerOfType(parentProgramUnit, ProgramFile);
			proposeVariables(
				parentFile.globalVariables.map[it|Step7LanguageHelper.getAllTopVariables(it)].flatten.toList, model,
				context, acceptor);

			// suggest all DBs
			proposeAllVisibleDataBlocks(model, context, acceptor, ".");

		// do not suggest I/O addresses
		}
	// super.completeDirectRef_Ref(model, assignment, context, acceptor);
	}

	def completeQualifiedRef_Ref(QualifiedRef model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {

		val variables = Step7LanguageScopeProvider.containedVariables(model.prefix);
		if (variables !== null) {
			proposeVariables(variables, model, context, acceptor);
		}
	}

	def INode lastLeafChild(INode node) {
		if (node instanceof ICompositeNode) {
			var current = node.firstChild;
			while (current !== null && current.nextSibling !== null) {
				current = current.nextSibling;
			}
			return lastLeafChild(current);
		} else {
			return node;
		}
	}

	def boolean lastNonWsNodeIsDot(INode currentNode) {
		var current = currentNode;
		while (current !== null && isWhitespaceNode(current) && current.previousSibling !== null) {
			current = lastLeafChild(current.previousSibling);
		}
		return current !== null && ".".equals(NodeModelUtils.getTokenText(current));
	}

	def List<String> fetchLastRef(INode currentNode) {
		var current = currentNode;
		while (isWhitespaceNode(current)) {
			current = lastLeafChild(current?.previousSibling);
		}

		val ret = newArrayList();
		while (!isWhitespaceNode(current)) {
			ret.add(NodeModelUtils.getTokenText(current));
			current = lastLeafChild(current?.previousSibling);
		}

		return ret.reverse;
	}

	def boolean isWhitespaceNodeOrNull(INode node) {
		return node === null || isWhitespaceNode(node);
	}
	
	def boolean isWhitespaceNode(INode node) {
		if (node === null) {
			return false;
		}

		return node?.grammarElement !== null && node.grammarElement instanceof TerminalRule &&
			(node.grammarElement as TerminalRule).name.equalsIgnoreCase("WS");
	}

	override completeQualifiedRef_Ref(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {

		println("Reference to be completed: " + fetchLastRef(context.currentNode));
		var refPrefix = fetchLastRef(context.currentNode).join("");
		val parentName = EmfHelper.getContainerOfType(model, NamedElement)?.name;
		var continuation = false;

		val firstSegment = refPrefix.split("\\.", 2);
		if (firstSegment.size > 1) {
			continuation = true;
			val firstSegmentDb = index.getVisibleEObjectDescriptions(model, Step7LanguagePackage.eINSTANCE.dataBlock).
				filter [ it |
					it.name.toString.equals(firstSegment.get(0))
				].toList;
			if (firstSegmentDb !== null && firstSegmentDb.size >= 1) {
				val firstSegmentEObject = firstSegmentDb.get(0).EObjectOrProxy;
				if (firstSegmentEObject instanceof DataBlock) {
					val realLocalDb = EmfHelper.getContainerOfType(model, ProgramFile).programUnits.filter(DataBlock).
						findFirst[it|it.name.equalsIgnoreCase(firstSegment.get(0))];
					// Improve: this only works for local DBs
					if (realLocalDb !== null && realLocalDb.structure instanceof FbOrUdtDT) {
						refPrefix = refPrefix.replaceFirst(firstSegment.get(0),
							(realLocalDb.structure as FbOrUdtDT).type.name);
						println(refPrefix);
					}
				}
			}

			// Specific content assist for FBs (call)
			val firstSegmentFb = index.getVisibleEObjectDescriptions(model,
				Step7LanguagePackage.eINSTANCE.functionBlock).filter[it|it.name.toString.equals(firstSegment.get(0))].
				toList;
			if (firstSegmentFb !== null && firstSegmentFb.size >= 1) {
				val programFiles = tryGetVisibleProgramFiles(model);
				val knownDbs = programFiles.map[it|it.programUnits].flatten.filter(DataBlock);
				// val localDbs = EmfHelper.getContainerOfType(model, ProgramFile)?.programUnits?.filter(DataBlock);
				val knownInstances = knownDbs.filter [it |
					it.structure instanceof FbOrUdtDT && (it.structure as FbOrUdtDT).type.name == firstSegment.get(0)
				].toList;

				for (item : knownInstances) {
					val proposedName = item.name;
					acceptor.accept(
						createCompletionProposal(proposedName + "();",
							new StyledString('''«proposedName»''').append(" -- FB call",
								StyledString.DECORATIONS_STYLER), getImage(model), 0, "", context));
				}
				// No more suggestions for FBs
				return;
			}
		}

		val refPrefixFinal = refPrefix;
		val visibleRefs = index.getVisibleEObjectDescriptions(model, Step7LanguagePackage.eINSTANCE.namedElement).filter [it |
			it.name.toString.startsWith(refPrefixFinal) ||
				it.name.toString.startsWith(parentName + "." + refPrefixFinal)
		].toList;
		for (visibleRef : visibleRefs) {
			val proposedName = visibleRef.name.toString;
			acceptor.accept(
				createCompletionProposal(visibleRef.name.skipFirst(1).toString,
					new StyledString('''«proposedName»''').append(" -- member variable",
						StyledString.DECORATIONS_STYLER), getImage(model), 0, "", context));
		}

		if (model instanceof SclSubroutineCall) {
			return;
		}

		if (model instanceof QualifiedRef) {
			// this is the continuation of a qualified reference, e.g. asking for completion for FB123.<cursor>
			println(model.prefix);

			val prefixNamedElement = getPrefixAsNamedElement(model.prefix);
			if (prefixNamedElement.present) {
				proposeVariables(prefixNamedElement.get.allDefinedVariables.filter(Variable), model, context, acceptor);
			}
		} else if (!lastNonWsNodeIsDot(context.currentNode) && !continuation) {
			// start of a qualified reference
			proposeAllVisibleFunctionBlocks(model, context, acceptor, ".");
			proposeAllVisibleDataBlocks(model, context, acceptor, ".");
		}
	}

	private def Optional<NamedElement> getPrefixAsNamedElement(NamedValueRef prefix) {
		switch (prefix) {
			DirectNamedRef: return Optional.of(prefix.ref)
			ArrayRef: return getPrefixAsNamedElement(prefix.ref)
			default: return Optional.empty
		}
	}

	private static def List<ProgramFile> tryGetVisibleProgramFiles(EObject e) {
		if (e === null) {
			return Collections.emptyList();
		}

		if (e.eResource !== null && e.eResource.resourceSet !== null) {
			return e.eResource.resourceSet.resources.map[res|res.contents].flatten.filter(ProgramFile).toList;
		} else if (e instanceof ProgramFile) {
			return Collections.singletonList(e);
		} else {
			val parentFile = EmfHelper.getContainerOfType(e, ProgramFile);
			if (parentFile === null) {
				return Collections.emptyList();
			} else {
				return Collections.singletonList(parentFile);
			}
		}
	}

	override completeSclAssignmentStatement_LeftValue(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		// completeDirectNamedRef_Ref(model, assignment, context, acceptor);
		// completeQualifiedRef_Ref(model, assignment, context, acceptor);
	}

	override completeNamedConstantRef_Ref(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {

		if (model instanceof QualifiedRef) {
			// no qualified references to constants
			return;
		}

		// only local constants can be referred
		val parentProgramUnit = EcoreUtil2.getContainerOfType(model, ExecutableProgramUnit);
		Preconditions.checkState(parentProgramUnit !== null);
		val declSection = parentProgramUnit.getDeclarationSection;
		proposeAllNamedConstantsOfBlock(declSection, model, context, acceptor);
	}

	override completeLabeledSclStatement_Labels(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		val parentProgramUnit = EcoreUtil2.getContainerOfType(model, ExecutableProgramUnit);
		Preconditions.checkState(parentProgramUnit !== null);
		val declSection = parentProgramUnit.getDeclarationSection;

		for (Label label : declSection.labelDeclarations.map[it.labels].flatten) {
			acceptor.accept(
				createCompletionProposal('''«label.name»: ''', '''«label.name» -- Label''', getImage(model), context));
		}
	}

	override complete_UnnamedConstant(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {

		if (model instanceof SclSubroutineCall) {
			return;
		}

		var suffix = "";
		if (model instanceof SclAssignmentStatement) {
			if (!isBoolVarLeftValue(model.leftValue)) {
				// We give no suggestion if model.leftValue is not a variable or if it is a non-Boolean variable
				return;
			}
			suffix = ";";
		}

		acceptor.accept(
			createCompletionProposal("TRUE" + suffix, new StyledString("TRUE"), getImage(model), 1001, "", context));
		acceptor.accept(
			createCompletionProposal("FALSE" + suffix, new StyledString("FALSE"), getImage(model), 1000, "", context));
	}

	/**
	 * Returns true iff the given left value is a reference to a Boolean variable.
	 */
	private def boolean isBoolVarLeftValue(LeftValue left) {
		if (left instanceof ArrayRef) {
			// Indexed access to a Boolean array?
			val referredVar = left.ref.referredVariable;
			if (referredVar.isPresent) {
				return DataTypeUtil.isArrayType(referredVar.get.dataType) &&
					DataTypeUtil.isBooleanElementaryType((referredVar.get.dataType as ArrayDT).baseType);
			}

			return false;
		} else {
			// Access to a Boolean variable?
			val referredVar = left.referredVariable;
			if (referredVar.isPresent) {
				return DataTypeUtil.isBooleanElementaryType(referredVar.get.dataType);
			}
			return false;
		}
	}

	override completeSclSubroutineCall_CalledUnit(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		if (model instanceof SclSubroutineCall) {
			// Do not propose calls as (nameless) parameters of calls.
			// foo(bar()); is valid, but unlikely
			return;
		}
			
		proposeAllVisibleFunctionBlocks(model, context, acceptor, ".");
		proposeAllVisibleFunctions(model, context, acceptor, "(");
	}
	
	override completeSclSubroutineCall_DataBlock(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		if (model instanceof SclSubroutineCall && (model as SclSubroutineCall).calledUnit instanceof FunctionBlock) {
			val calledFb = (model as SclSubroutineCall).calledUnit as FunctionBlock;
			val visibleDbs = index.getVisibleEObjectDescriptions(model, Step7LanguagePackage.eINSTANCE.dataBlock);
			for (db : visibleDbs) {
				if (db.EObjectOrProxy instanceof DataBlock && (db.EObjectOrProxy as DataBlock).structure === calledFb) {
					println("Propose: " + (db.EObjectOrProxy as DataBlock).name);
				}
			}
		} else if (!lastNonWsNodeIsDot(context.currentNode)) {
			// Propose all DBs
			proposeAllVisibleDataBlocks(model, context, acceptor, "();");
		}
		// super.completeSclSubroutineCall_DataBlock(model, assignment, context, acceptor)
	}
	
	override complete_SclSubroutineCall(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		return;
	}
	
	override complete_DataBlock(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		print(model);
	}

	override complete_CallParameterItem(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		if (model instanceof SclSubroutineCall) {
			val calledPou = model.calledUnit;
			proposeVariables(Step7LanguageHelper.allDefinedVariables(calledPou).filter(Variable), model, context,
				acceptor, " := ");
		}
		
	}

	override completeDirectRef_Field(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		return;
	}
	
	override completeSclSubroutineCall_CallParameter(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
//		if (model instanceof SclSubroutineCall) {
//			val callee = model.calledProgramUnit;
//			if (callee !== null) {
//				proposeVariables(Step7LanguageHelper.getBlockParameters(callee), model, context, acceptor, " := ");
//			}
//		}
	}

	override completeCallParameter_Parameter(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		return;
	}
	
	override completeCallParameter_Parameters(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		return;
	}
	
	override completeCallParameterItem_Parameter(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		var SubroutineCall call = null;
		
		if (model instanceof SclSubroutineCall) {
			call = model;
		} else if (model instanceof CallParameterList) {
			call = model.callOfParam;
		}
		
		var ExecutableProgramUnit callee = call?.calledProgramUnit;

		if (callee !== null) {
			val alreadyDefinedParams = if (call.callParameter instanceof CallParameterList) (call.callParameter as CallParameterList).parameters.filter(NamedCallParameterItem).map[it | it.parameter].toList else Collections.emptyList;
			
			proposeVariables(Step7LanguageHelper.getBlockParameters(callee).filter[it | !alreadyDefinedParams.contains(it)], model, context, acceptor, " := ");
		}
	}
	
	override completeCallParameterItem_Value(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		return;
	}
	
	override completeStwBitRef_Mnemonic(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		return;
	}	
	
	override completeDirectRef_Expr(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		return;
	}
	
	
	override completeVariableDeclarationLine_Type(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		// TODO propose all visible FBs, ARRAY and basic types
	}

	override completeFbOrUdtDT_Type(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {

		val containerVarBlock = EcoreUtil2.getContainerOfType(model, VariableDeclarationBlock);
		if (containerVarBlock !== null && containerVarBlock.direction == VariableDeclarationDirection.STATIC) {
			// FB instances are only allowed in VAR blocks
			proposeAllVisibleFunctionBlocks(model, context, acceptor, ";");
		}

		// UDTs are always permitted
		proposeAllVisibleUdts(model, context, acceptor);
	}

	private def proposeAllVisibleFunctionBlocks(EObject from, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor, String textToInsertPostfix) {
		proposeAllVisibleElementsOfType(from, context, acceptor, Step7LanguagePackage.eINSTANCE.functionBlock, "FB", 
			textToInsertPostfix);
	}
	
	private def proposeAllVisibleDataBlocks(EObject from, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor, String textToInsertPostfix) {
		proposeAllVisibleElementsOfType(from, context, acceptor, Step7LanguagePackage.eINSTANCE.dataBlock, "DB", 
			textToInsertPostfix);
	}

	private def proposeAllVisibleFunctions(EObject from, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor, String textToInsertPostfix) {
		proposeAllVisibleElementsOfType(from, context, acceptor, Step7LanguagePackage.eINSTANCE.function, "FC",
			textToInsertPostfix);
	}

	private def proposeAllVisibleUdts(EObject from, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		proposeAllVisibleElementsOfType(from, context, acceptor, Step7LanguagePackage.eINSTANCE.userDefinedDataType,
			"UDT");
	}

	private def proposeAllVisibleElementsOfType(EObject from, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor, EClass type, String descriptionString) {
		proposeAllVisibleElementsOfType(from, context, acceptor, type, descriptionString, "");
	}

	private def proposeAllVisibleElementsOfType(EObject from, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor, EClass type, String descriptionString, String textToInsertPostfix) {
		for (item : index.getVisibleEObjectDescriptions(from, type)) {
			val proposedName = item.name.lastSegment;
			acceptor.accept(
				createCompletionProposal(proposedName + textToInsertPostfix,
					new StyledString('''«proposedName»''').append(" -- " + descriptionString,
						StyledString.DECORATIONS_STYLER), getImage(from), 0, "", context));
		}
	}

	private def proposeAllNamedConstantsOfBlock(DeclarationSection declSection, EObject model,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		for (NamedConstantDeclaration constDecl : declSection.constantDeclarations.map [ constDeclBlock |
			constDeclBlock.constants
		].flatten) {
			acceptor.accept(
				createCompletionProposal(constDecl.name,
					new StyledString('''«constDecl.name»''').
						append(''' - Constant (= «SimpleExpressionUtil.evaluateSimpleExpression(constDecl.value)»)''',
							StyledString.DECORATIONS_STYLER), getImage(model), context));
		}
	}

	private def proposeVariables(Iterable<? extends Variable> variables, EObject model, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		proposeVariables(variables, model, context, acceptor, "");
	}

	private def proposeVariables(Iterable<? extends Variable> variables, EObject model, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor, String textToInsertPostfix) {
		for (Variable item : variables) {
			val proposedName = item.name;
			val varTypeStr = DataTypeUtil.toString(item.dataType);
			if (item.getDataType instanceof ArrayDT) {
				val proposal = createCompletionProposal(proposedName + "[]" + textToInsertPostfix,
						new StyledString(proposedName).append(''' - Variable of «varTypeStr»''',
							StyledString.DECORATIONS_STYLER), getImage(model), context);
				if (proposal instanceof ConfigurableCompletionProposal) {
					proposal.cursorPosition = proposedName.length + 1;
				}
				acceptor.accept(proposal);
					
			} else {
				acceptor.accept(
					createCompletionProposal(proposedName + textToInsertPostfix,
						new StyledString(proposedName).append(''' - Variable of «varTypeStr»''',
							StyledString.DECORATIONS_STYLER), getImage(model), context));
			}
		}
	}
		
	private static def isAnyOf(Keyword keyword, Keyword... options) {
		for (option : options) {
			if (keyword === option) {
				return true;
			}
		}
		return true;
	}
	
	override protected doCreateProposal(String proposal, StyledString displayString, Image image, int priority, ContentAssistContext context) {
		// Do not propose anything in comments
		if (context.currentNode.grammarElement == access.ML_COMMENTRule || context.currentNode.grammarElement == access.SL_COMMENTRule) {
			return null;
		}
		
		return super.doCreateProposal(proposal, displayString, image, priority, context);	
	}
	
}
