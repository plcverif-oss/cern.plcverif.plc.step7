/*******************************************************************************
 * (C) Copyright CERN 2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.ui.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;

import cern.plcverif.plc.step7.ui.sanitizer.Step7CodeSanitizer;

public class CodeSanitizationHandler extends AbstractHandler implements IHandler {
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		XtextEditor editor = EditorUtils.getActiveXtextEditor(event);
		if (editor != null && editor.getDocument() != null) {
			Step7CodeSanitizer.suggestSanitization(editor.getDocument(), false);
		} else {
			Shell shell = Display.getCurrent().getActiveShell();
			MessageDialog.openInformation(shell, "Code sanitization",
					"Code sanitization is not avaliable for this editor.");
		}
		return null;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}