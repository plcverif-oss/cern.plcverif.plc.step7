/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.ui.handler;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.xtext.parser.IParseResult;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;
import org.eclipse.xtext.ui.refactoring.impl.RenameElementProcessor;
import org.eclipse.xtext.ui.refactoring.ui.IRenameElementContext;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;

import com.google.inject.Inject;
import com.google.inject.Provider;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.plc.step7.step7Language.BlockAttributes;
import cern.plcverif.plc.step7.step7Language.DataBlock;
import cern.plcverif.plc.step7.step7Language.NamedElement;
import cern.plcverif.plc.step7.step7Language.ProgramFile;
import cern.plcverif.plc.step7.step7Language.Variable;

@SuppressWarnings("restriction")
public class ObfuscateHandler extends AbstractHandler implements IHandler {
	@Inject
	private Provider<RenameElementProcessor> processorProvider;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		XtextEditor editor = EditorUtils.getActiveXtextEditor(event);
		if (editor != null && editor.getDocument() != null) {
			// change the names
			editor.getDocument().modify(new IUnitOfWork.Void<XtextResource>() {
				@Override
				public void process(XtextResource xtextResource) throws Exception {
					modifyNames(xtextResource);
				}
			});
			editor.getDocument().addDocumentListener(new IDocumentListener() {
				
				@Override
				public void documentChanged(DocumentEvent arg0) {
					System.out.println("Change: " + arg0);
				}
				
				@Override
				public void documentAboutToBeChanged(DocumentEvent arg0) {
					// TODO Auto-generated method stub
					
				}
			});

			// remove attributes
			editor.getDocument().modify(new IUnitOfWork.Void<XtextResource>() {
				@Override
				public void process(XtextResource xtextResource) throws Exception {
					removeAttributes(xtextResource);
					System.out.println("1");
				}
			});
		} else {
			System.err.println("Not an Xtext editor.");
		}
		return null;
	}

	private void modifyNames(XtextResource xtextResource)
			throws OperationCanceledException, InvocationTargetException, CoreException, InterruptedException {
		IParseResult parseResult = xtextResource.getParseResult();
		if (parseResult != null && parseResult.getRootASTElement() instanceof ProgramFile) {
			ProgramFile programFile = (ProgramFile) parseResult.getRootASTElement();

			// collect already existing names in the file to avoid collisions
			Set<String> names = EmfHelper.getAllContentsOfTypeStream(programFile, NamedElement.class, false)
					.map(it -> it.getName()).collect(Collectors.toSet());

			int cntr = 0;
			// rename variables
			for (NamedElement element : EmfHelper.getAllContentsOfType(programFile, NamedElement.class, false)) {
				if (element instanceof Variable || element instanceof DataBlock) {
					cntr++;

					// don't change again, it's costly
					String originalName = element.getName();
					if (originalName.matches("(var|in|out|par|db|id)\\d*")) {
						continue;
					}

					// make the name unique
					while (names.contains(prefix(element) + cntr)) {
						cntr++;
					}

					String newName = prefix(element) + cntr;
					System.out.printf("Renaming %s (%s) to %s...%n", originalName, element.eClass().getName(), newName);
					rename(EcoreUtil.getURI(element), element.eClass(), newName);
				}
			}
		} else {
			System.err.println("Problem occurred during obfuscation.");
		}
	}

	private static void removeAttributes(XtextResource xtextResource) {
		IParseResult parseResult = xtextResource.getParseResult();
		if (parseResult != null && parseResult.getRootASTElement() instanceof ProgramFile) {
			ProgramFile programFile = (ProgramFile) parseResult.getRootASTElement();
			// remove attributes
			for (BlockAttributes blockAttrib : EmfHelper.getAllContentsOfType(programFile, BlockAttributes.class,
					false)) {
				EcoreUtil.remove(blockAttrib);
			}
		}
		
		System.out.println("Finished");
	}

	private static String prefix(NamedElement element) {
		if (element instanceof Variable) {
			return "var";
		} else if (element instanceof DataBlock) {
			return "db";
		} else {
			return "id";
		}
	}

	private void rename(URI uri, EClass eclass, String newName)
			throws OperationCanceledException, CoreException, InvocationTargetException, InterruptedException {
		// ideas from here:
		// https://www.eclipse.org/forums/index.php/t/1075778/
		RenameElementProcessor processor = processorProvider.get();
		processor.initialize(new IRenameElementContext.Impl(uri, eclass));
		processor.setNewName(newName);
		RefactoringStatus initialStatus = processor.checkInitialConditions(new NullProgressMonitor());
		if (initialStatus.isOK()) {
			RefactoringStatus finalStatus = processor.checkFinalConditions(new NullProgressMonitor(), null);
			if (finalStatus.isOK()) {
				Change change = processor.createChange(new NullProgressMonitor());
				new WorkspaceModifyOperation() {
					@Override
					protected void execute(IProgressMonitor monitor)
							throws CoreException, InvocationTargetException, InterruptedException {
							change.perform(monitor);
					}
				}.run(null);
			}
		}
		
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}