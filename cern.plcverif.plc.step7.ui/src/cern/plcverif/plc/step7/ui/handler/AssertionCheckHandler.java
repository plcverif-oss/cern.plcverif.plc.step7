/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.ui.handler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;

import com.google.inject.Inject;

import cern.plcverif.base.common.emf.textual.EmfModelPrinter;
import cern.plcverif.base.common.emf.textual.SimpleEObjectToText;
import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.gui.preferences.PlcverifPreferenceAccess;
import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.base.interfaces.data.result.JobStageTags;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.validation.CfaValidation;
import cern.plcverif.plc.step7.cfa.Step7CodeToCfa;
import cern.plcverif.plc.step7.cfa.exceptions.PlcCodeToCfaException;
import cern.plcverif.plc.step7.step7Language.ProgramFile;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.interfaces.data.VerificationStageTags;
import cern.plcverif.verif.jobmock.MockVerificationJob;
import cern.plcverif.verif.jobmock.MockVerificationJob.MockVerificationJobConfig;

/**
 * This is a dirty hack for diagnostic purposes.
 */
public class AssertionCheckHandler extends AbstractHandler implements IHandler {
	@Inject
	IResourceDescriptions resourceDescriptions;

	@Inject
	IResourceSetProvider resourceSetProvider;

	// @Inject
	// private javax.inject.Provider<EclipseResourceFileSystemAccess2>
	// fileAccessProvider;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;

			@SuppressWarnings("rawtypes")
			Iterator iter = structuredSelection.iterator();
			while (iter.hasNext()) {
				Object obj = iter.next();
				if (obj instanceof IFile) {
					IFile file = (IFile) obj;
					IProject project = file.getProject();
					java.nio.file.Path outputDir;
					try {
						IFolder srcGenFolder = PlcverifPreferenceAccess.getAndCreateOutputFolder(project, new NullProgressMonitor());
						
						IFolder targetFolder = srcGenFolder.getFolder(file.getName());
						if (!targetFolder.exists()) {
							targetFolder.create(true, true, new NullProgressMonitor());
						}
						outputDir = Paths.get(targetFolder.getRawLocation().makeAbsolute().toOSString());
					} catch (IOException | CoreException e) {
						// best effort
						e.printStackTrace();
						try {
							outputDir = Files.createTempDirectory("PLCverif-temp");
						} catch (IOException e1) {
							outputDir = null;
							// best effort
							e1.printStackTrace();
							throw new PlcverifPlatformException("Unable to locate the output directory.", e1);
						}
					}

					URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
					ResourceSet rs = resourceSetProvider.get(project);
					Resource r = rs.getResource(uri, true);

					ProgramFile programFile = (ProgramFile) r.getContents().get(0);
					try {
						CfaNetworkDeclaration cfa = Step7CodeToCfa.convert(IPlcverifLogger.NULL_LOGGER, programFile)
								.getCfa();
						CfaValidation.validate(cfa);
						IoUtils.writeAllContent(outputDir.resolve(file.getName() + ".cfd.txt"),
								EmfModelPrinter.print(cfa, SimpleEObjectToText.INSTANCE));

						MockVerificationJobConfig config = new MockVerificationJobConfig();
						config.setSettings("-job.backend.timeout = 600 " + " -reductions.fine_logging=true");
						VerificationResult result = MockVerificationJob.run(cfa, file.getName(), outputDir, config,
								true);
						System.err.printf("Result: %s%n", result.getResult().toString());
						List<JobStage> execStages = result.getStages(VerificationStageTags.BACKEND_EXECUTION);
						if (!execStages.isEmpty()) {
							System.err.printf("   execution runtime: %s ms%n",
									Long.toString(JobStage.sumLengthMs(execStages)));
						}

						result.getStages(JobStageTags.REDUCTIONS).forEach(
								it -> it.getLogItems().forEach(logitem -> System.out.println(logitem.getMessage())));
					} catch (PlcCodeToCfaException e) {
						System.err.println("Unable to convert PLC code to CFA.");
						e.printStackTrace();
					} catch (Exception e) {
						System.err.println("Exception.");
						e.printStackTrace();
					}
				}
			}
		}
		return null;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
