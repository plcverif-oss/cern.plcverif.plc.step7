package cern.plcverif.plc.step7.ui.handler;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;

public class RemoveCommentsHandler extends AbstractHandler implements IHandler {
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		XtextEditor editor = EditorUtils.getActiveXtextEditor(event);
		if (editor != null && editor.getDocument() != null) {
			removeComments(editor);
		} else {
			System.err.println("Not an Xtext editor.");
		}
		return null;
	}

	private static void removeComments(XtextEditor editor) {
		// remove comments
		String content = editor.getDocument().get();

		Pattern blockComment = Pattern.compile("(\\(\\*.*?\\*\\))", Pattern.MULTILINE | Pattern.DOTALL);
		Matcher matcher = blockComment.matcher(content);
		content = matcher.replaceAll("");

		Pattern lineComment = Pattern.compile("(//[^#].*)$", Pattern.MULTILINE);
		matcher = lineComment.matcher(content);
		content = matcher.replaceAll("");

		editor.getDocument().set(content);
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}