/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.ui.handler;

import java.io.IOException;
import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;

import com.google.inject.Inject;
import com.google.inject.Provider;

import cern.plcverif.base.common.emf.textual.EmfModelPrinter;
import cern.plcverif.base.common.emf.textual.SimpleEObjectToText;
import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.gui.preferences.PlcverifPreferenceAccess;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.serialization.CfaToXmi;
import cern.plcverif.base.models.cfa.trace.CfaInstantiationTrace;
import cern.plcverif.base.models.cfa.transformation.CfaInstantiator;
import cern.plcverif.base.models.cfa.validation.CfaValidation;
import cern.plcverif.base.models.cfa.visualization.CfaToDot;
import cern.plcverif.plc.step7.cfa.Step7CodeToCfa;
import cern.plcverif.plc.step7.cfa.exceptions.PlcCodeToCfaException;
import cern.plcverif.plc.step7.step7Language.ProgramFile;

public class CfaGenerationHandler extends AbstractHandler implements IHandler {
	@Inject
	IResourceDescriptions resourceDescriptions;

	@Inject
	IResourceSetProvider resourceSetProvider;

	@Inject
	private Provider<EclipseResourceFileSystemAccess2> fileAccessProvider;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;

			@SuppressWarnings("rawtypes")
			Iterator iter = structuredSelection.iterator();
			while (iter.hasNext()) {
				Object obj = iter.next();
				if (obj instanceof IFile) {
					IFile file = (IFile) obj;
					IProject project = file.getProject();

					URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
					ResourceSet rs = resourceSetProvider.get(project);
					Resource r = rs.getResource(uri, true);
					EcoreUtil.resolveAll(r);
					r.setURI(URI.createHierarchicalURI(new String[] {"__synthetic0.SCL"}, "", "")); // this is a hack
					System.out.println(r.getURI().lastSegment());
					boolean cfaRepresentationSucceeded = false;
					boolean cfaInstantiationSucceeded = false;

					try {
						System.out.printf(" === %s ===%n", file.getName());

						ProgramFile programFile = (ProgramFile) r.getContents().get(0);
						CfaNetworkDeclaration cfa = Step7CodeToCfa.convert(IPlcverifLogger.NULL_LOGGER, programFile)
								.getCfa();
						cfaRepresentationSucceeded = true;

						try {
							CfaValidation.validate(cfa);
						} catch (Exception e) {
							System.err.println("CfaValidation result for CFD: ERROR");
							e.printStackTrace();
						}

						// Save to file
						final EclipseResourceFileSystemAccess2 fsa = fileAccessProvider.get();
						fsa.setProject(project);
						fsa.setOutputPath(PlcverifPreferenceAccess.getOutputFolder());
						fsa.getOutputConfigurations().get(IFileSystemAccess.DEFAULT_OUTPUT)
								.setCreateOutputDirectory(true);
						fsa.setMonitor(new NullProgressMonitor());

						fsa.generateFile(file.getName() + ".cfd", CfaToXmi.serializeToString(cfa));
						fsa.generateFile(file.getName() + ".cfd.txt",
								EmfModelPrinter.print(cfa, SimpleEObjectToText.INSTANCE));
						fsa.generateFile(file.getName() + ".cfd.dot", CfaToDot.representCfd(cfa));

						CfaInstantiationTrace traceModel = CfaInstantiator.transformCfaNetwork(cfa, true);
						cfaInstantiationSucceeded = true;
						CfaNetworkInstance cfaInstance = traceModel.getInstance();
						fsa.generateFile(file.getName() + ".cfi.txt",
								EmfModelPrinter.print(cfaInstance, SimpleEObjectToText.INSTANCE));
						fsa.generateFile(file.getName() + ".cfi.dot", CfaToDot.representCfi(cfaInstance));
					} catch (PlcCodeToCfaException ex) {
						System.err.println("Exception: " + ex.getMessage());
						ex.printStackTrace();
					}
					catch (IOException e) {
						PlatformLogger.logError("CfaGenerationHandler error, unable to create files. " + e.getMessage(), e);
					} catch (Exception anyException) {
						PlatformLogger.logError("CfaGenerationHandler error: " + anyException.getMessage(), anyException);
					}

					System.err.printf("CFA representation: %s%n", (cfaRepresentationSucceeded ? "OK" : "ERROR"));
					System.err.printf("CFA instantiation:  %s%n", (cfaInstantiationSucceeded ? "OK" : "ERROR"));
				}
			}
		}
		return null;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
