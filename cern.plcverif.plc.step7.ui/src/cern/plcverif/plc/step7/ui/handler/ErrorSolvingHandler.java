/*******************************************************************************
 * (C) Copyright CERN 2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink
 *******************************************************************************/
package cern.plcverif.plc.step7.ui.handler;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.jface.text.IRegion;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.xtext.diagnostics.Severity;
import org.eclipse.xtext.parser.IParseResult;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;
import org.eclipse.xtext.ui.refactoring.impl.RenameElementProcessor;
import org.eclipse.xtext.ui.refactoring.ui.IRenameElementContext;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.IResourceValidator;
import org.eclipse.xtext.validation.Issue;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;

import cern.plcverif.plc.step7.Step7LanguageRuntimeModule;
import cern.plcverif.plc.step7.validation.Step7LanguageValidator;

@SuppressWarnings("restriction")
public class ErrorSolvingHandler extends AbstractHandler implements IHandler {
	@Inject
	private Provider<RenameElementProcessor> processorProvider;
	private IResourceValidator resourceValidator;
	
	Set<String> errorsToCommentOut = Set.of(Step7LanguageValidator.INVALID_FBORUDT_TYPE);
	
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		XtextEditor editor = EditorUtils.getActiveXtextEditor(event);
		if (editor != null && editor.getDocument() != null) {
			// change the names
			editor.getDocument().modify(new IUnitOfWork.Void<XtextResource>() {
				@Override
				public void process(XtextResource xtextResource) throws Exception {
					resolveUndefinedFunctionBlockError(xtextResource, editor.getDocument());
				}
			});
		} else {
			System.err.println("Not an Xtext editor.");
		}
		return null;
	}

	private void resolveUndefinedFunctionBlockError(XtextResource xtextResource, IXtextDocument xtextDocument)
			throws OperationCanceledException, InvocationTargetException, CoreException, InterruptedException {
		
		Injector injector = Guice.createInjector(new Step7LanguageRuntimeModule());
        IResourceValidator validator = injector.getInstance(IResourceValidator.class);
		List<Issue> validationIssues = validator.validate(xtextResource, CheckMode.ALL, CancelIndicator.NullImpl); 
		List<Issue> validationErrors = validationIssues.stream().filter(it -> it.getSeverity() == Severity.ERROR && errorsToCommentOut.contains(it.getCode())).collect(Collectors.toList());
		
		Set<Integer> alreadyCommented = new HashSet<>(); 
		List<Integer> lineNumbers = validationErrors.stream().map(vi -> {
			try {
				return Integer.valueOf(xtextDocument.getLineOfOffset(vi.getOffset()));
			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}).collect(Collectors.toList());
		int i = 0;
		for (Issue issue : validationErrors) {
			try {
                int lineNumber = lineNumbers.get(i);
                i++;
                IRegion lineInfo = xtextDocument.getLineInformation(lineNumber);
                String lineText = xtextDocument.get(lineInfo.getOffset(), lineInfo.getLength());
                String commentedLineText = "//Commented out by ERRORSOLVING_MODULE " + lineText;
                if (!alreadyCommented.contains(Integer.valueOf(lineNumber))) {
	                xtextDocument.replace(lineInfo.getOffset(), lineInfo.getLength(), commentedLineText);
	                alreadyCommented.add(Integer.valueOf(lineNumber));
                }
            } catch (BadLocationException ex) {
                ex.printStackTrace();
            }
        }
		
		System.out.println("Finished");

	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}