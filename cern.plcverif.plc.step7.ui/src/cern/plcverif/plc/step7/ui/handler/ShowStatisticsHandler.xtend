/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.ui.handler

import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.util.Step7Statistics
import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException
import org.eclipse.core.commands.IHandler
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.XtextEditor
import org.eclipse.xtext.ui.editor.utils.EditorUtils
import org.eclipse.xtext.util.concurrent.IUnitOfWork

class ShowStatisticsHandler extends AbstractHandler implements IHandler {
	override Object execute(ExecutionEvent event) throws ExecutionException {
		val XtextEditor editor = EditorUtils.getActiveXtextEditor(event);
		if (editor !== null && editor.getDocument() !== null) {
			val document = editor.document;
			document.readOnly(
				new IUnitOfWork.Void<XtextResource>() {
					override void process(XtextResource state) throws Exception {
						val stat = new Step7Statistics(state.contents.filter(ProgramFile).toList);
						MessageDialog.openInformation(editor.shell, "STEP 7 Statistics", getText(stat));
					}
				}
			);
		} else {
			System.err.println("Not an Xtext editor.");
		}
		return null;
	}

	private def getText(Step7Statistics stat) {
		return '''
			STEP 7 statistics
			- Total statements: «stat.countStatements»
			- Total program units: «stat.countProgramUnits», of which executable (FB, FC, OB): «stat.countExecutableProgramUnits»
			- Total top-level variables: «stat.countTopVariables»
			- Total variable names defined: «stat.countVariableNames»
		'''
	}
}
