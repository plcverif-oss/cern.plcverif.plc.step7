/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.ui

import cern.plcverif.plc.step7.ide.syntaxcoloring.Step7LanguageSemanticHighlightingCalculator
import cern.plcverif.plc.step7.ui.hover.Step7EObjectHoverProvider
import com.google.inject.Binder
import com.google.inject.name.Names
import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor
import org.eclipse.xtext.ide.editor.syntaxcoloring.ISemanticHighlightingCalculator
import org.eclipse.xtext.resource.IResourceDescriptionsProvider
import org.eclipse.xtext.resource.containers.IAllContainersState
import org.eclipse.xtext.ui.editor.contentassist.XtextContentAssistProcessor
import org.eclipse.xtext.ui.editor.hover.IEObjectHoverProvider

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
@FinalFieldsConstructor
class Step7LanguageUiModule extends AbstractStep7LanguageUiModule {
	def Class<? extends ISemanticHighlightingCalculator> bindISemanticHighlightingCalculator() {
		return Step7LanguageSemanticHighlightingCalculator;
	}
	
	def Class<? extends IEObjectHoverProvider> bindIEObjectHoverProvider() {
	    return Step7EObjectHoverProvider;
	}

	override configure(Binder binder) {
		super.configure(binder);
		binder.bind(String).annotatedWith(Names.named((XtextContentAssistProcessor.COMPLETION_AUTO_ACTIVATION_CHARS))).
			toInstance(".,")
	}
	
	def Class<? extends IResourceDescriptionsProvider> bindIResourceDescriptionsProvider() {
		return InmemoryCapableResourceDescriptionsProvider
	}

	override Class<? extends IAllContainersState.Provider> bindIAllContainersState$Provider() {
		return InmemoryCapableContainerStateProvider
	}
}
