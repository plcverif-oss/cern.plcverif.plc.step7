/******************************************************************************
 * (C) Copyright CERN 2017-2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 * 	 Xaver Fink
 *****************************************************************************/
package cern.plcverif.plc.step7.ui.quickfix

import org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider
import cern.plcverif.plc.step7.validation.Step7LanguageValidator
import org.eclipse.xtext.ui.editor.quickfix.Fix
import org.eclipse.xtext.validation.Issue
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor

/**
 * Custom quickfixes.
 *
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#quick-fixes
 */
class Step7LanguageQuickfixProvider extends DefaultQuickfixProvider {

	@Fix(Step7LanguageValidator.INVALID_FBORUDT_TYPE)
	def commentOutLine(Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(issue, 'Comment out', 'Comment out the line.', 'remove.png') [
			element, context |
			val xtextDocument = context.xtextDocument
			val line = xtextDocument.getLineOfOffset(issue.offset)
			val lineInfo = xtextDocument.getLineInformation(line);
            val lineText = xtextDocument.get(lineInfo.offset, lineInfo.length);
            val commentedLineText = "//Commented out by ERRORSOLVING_MODULE " + lineText;
			xtextDocument.replace(lineInfo.offset, lineInfo.length, commentedLineText)
		]
	}
}
