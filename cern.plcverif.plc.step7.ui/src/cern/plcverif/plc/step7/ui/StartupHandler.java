package cern.plcverif.plc.step7.ui;

import org.eclipse.ui.IStartup;

import cern.plcverif.plc.step7.ui.internal.Step7Activator;
import cern.plcverif.plc.step7.ui.sanitizer.Step7CodeSanitizer;

public class StartupHandler implements IStartup {
	@Override
	public void earlyStartup() {
		new XtextEditorOpeningListener(Step7Activator.CERN_PLCVERIF_PLC_STEP7_STEP7LANGUAGE,
				it -> Step7CodeSanitizer.suggestSanitization(it.getDocument(), true));
	}
}
