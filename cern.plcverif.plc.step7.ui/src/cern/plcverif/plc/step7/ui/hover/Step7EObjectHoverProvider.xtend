/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.ui.hover

import cern.plcverif.plc.step7.step7Language.NamedConstantDeclaration
import cern.plcverif.plc.step7.step7Language.Variable
import cern.plcverif.plc.step7.ui.labeling.Step7LanguageLabelProvider
import com.google.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.ui.editor.hover.html.DefaultEObjectHoverProvider

import static extension cern.plcverif.plc.step7.util.Step7LanguageHelper.getDataType
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import cern.plcverif.plc.step7.util.SimpleExpressionUtil
import cern.plcverif.plc.step7.step7Language.ProgramUnit
import cern.plcverif.plc.step7.step7Language.Function

class Step7EObjectHoverProvider extends DefaultEObjectHoverProvider {
	@Inject
	Step7LanguageLabelProvider labelProvider; 
	
	// Overrides
	override protected getFirstLine(EObject o) {
		return firstLine(o);
	}
	
	override protected getDocumentation(EObject o) {
		return description(o);
	}
	
	// Implementation
	private def dispatch String firstLine(EObject o) {
		val label = getLabel(o);
		return if (label !== null) '''«label.bold»''' else "";
	}
	
	private def dispatch String description(EObject o) {
		return super.getDocumentation(o);
	}
	
	private def dispatch String firstLine(Function fc) {
		return '''«getLabel(fc).bold» returns «labelProvider.typeName(fc.returnType)?.bold»''';
	}
	
	private def dispatch String description(ProgramUnit e) {
		return '''Defined in '«e.eResource?.URI?.toPlatformString(true)»'.''';
	}
	
	private def dispatch String firstLine(NamedConstantDeclaration e) {
		return '''Constant «e.name.bold» = «SimpleExpressionUtil.evaluateSimpleExpression(e.value)»''';
	}
	
	private def dispatch String description(NamedConstantDeclaration o) {
		return '''Constant.''';
	}
	
	private def dispatch String firstLine(Variable e) {
		return '''Variable «e.name.bold»: «labelProvider.typeName(e.dataType)» («Step7LanguageHelper.getVariableDirection(e).toString»)''';
	}
	
	private def dispatch String description(Variable o) {
		return '''Variable.''';
	}
	
	// Helpers
	private def static String bold(String str) {
		return '''<b>«str»</b>''';
	}
}