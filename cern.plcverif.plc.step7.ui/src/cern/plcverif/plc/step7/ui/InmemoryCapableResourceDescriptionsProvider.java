package cern.plcverif.plc.step7.ui;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.resource.impl.ResourceDescriptionsProvider;
import org.eclipse.xtext.resource.impl.ResourceSetBasedResourceDescriptions;

import com.google.inject.Inject;
import com.google.inject.Provider;

import cern.plcverif.plc.step7.util.Step7LanguageHelper;

public class InmemoryCapableResourceDescriptionsProvider extends ResourceDescriptionsProvider {
	public static class InmemoryCapableResourceSetBasedResourceDescriptions extends ResourceSetBasedResourceDescriptions {
	}

	@Inject
	private Provider<InmemoryCapableResourceSetBasedResourceDescriptions> resourceSetBasedResourceDescriptions;

	@Override
	public IResourceDescriptions getResourceDescriptions(ResourceSet resourceSet) {
		if (Boolean.TRUE.equals(resourceSet.getLoadOptions().get(Step7LanguageHelper.OPTION_IN_MEMORY_SUPPORT))) {
			ResourceSetBasedResourceDescriptions d = resourceSetBasedResourceDescriptions.get();
			d.setContext(resourceSet);
			return d;
		}
		return super.getResourceDescriptions(resourceSet);
	}
}
