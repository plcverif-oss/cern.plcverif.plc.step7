package cern.plcverif.plc.step7.ui;

import org.osgi.framework.BundleContext;

import com.google.inject.Injector;

import cern.plcverif.plc.step7.extension.Step7ExtensionActivator;
import cern.plcverif.plc.step7.ui.internal.Step7Activator;

public class Step7UiCustomActivator extends Step7Activator {
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		
		// Pass the injector to the PLCverif extension to avoid creating a new one!
		Injector step7Injector = getInjector(CERN_PLCVERIF_PLC_STEP7_STEP7LANGUAGE);
		Step7ExtensionActivator.setInjector(step7Injector);
	}
}
