package cern.plcverif.plc.step7.ui;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWindowListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.xtext.ui.editor.XtextEditor;

/**
 * Xtext editor opening listener registration.
 */
public class XtextEditorOpeningListener {
	@FunctionalInterface
	public static interface Callback {
		public void onEditorOpened(XtextEditor editor);
	}

	private final String languageName;
	private final IPartListener partListener;
	private final Set<IWorkbenchPage> listenedToPages = new HashSet<>();

	/**
	 * Registers a listener to the editor opening for the given language.
	 * 
	 * @param languageName
	 *            Language name
	 * @param callback
	 *            Callback when an editor is opened representing the given
	 *            language
	 */
	public XtextEditorOpeningListener(String languageName, Callback callback) {
		this.languageName = languageName;

		this.partListener = new PartAdapter() {
			@Override
			public void partOpened(IWorkbenchPart part) {
				// System.out.println("partOpened " + part);
				if (part != null && part instanceof XtextEditor) {
					XtextEditor xtextEditor = (XtextEditor) part;
					if (languageName.equals(xtextEditor.getLanguageName())) {
						callback.onEditorOpened(xtextEditor);
					}
				}
			}
		};

		register();
	}

	private void register() {
		PlatformUI.getWorkbench().addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(IWorkbenchWindow window) {
				// System.out.println("window activated: " + window);
				IWorkbenchPage activePage = window.getActivePage();
				if (!listenedToPages.contains(activePage)) {
					activePage.addPartListener(partListener);
					listenedToPages.add(activePage);
				}
			}
		});
	}

	@Override
	public String toString() {
		return super.toString() + " " + languageName;
	}

	// Adapters
	private static class PartAdapter implements IPartListener {
		@Override
		public void partActivated(IWorkbenchPart part) {
		}

		@Override
		public void partBroughtToTop(IWorkbenchPart part) {
		}

		@Override
		public void partClosed(IWorkbenchPart part) {
		}

		@Override
		public void partDeactivated(IWorkbenchPart part) {
		}

		@Override
		public void partOpened(IWorkbenchPart part) {
		}
	}

	private static class WindowAdapter implements IWindowListener {
		@Override
		public void windowActivated(IWorkbenchWindow window) {
		}

		@Override
		public void windowDeactivated(IWorkbenchWindow window) {
		}

		@Override
		public void windowClosed(IWorkbenchWindow window) {
		}

		@Override
		public void windowOpened(IWorkbenchWindow window) {
		}
	}
}
