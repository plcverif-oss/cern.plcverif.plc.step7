/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.ui.outline

import cern.plcverif.plc.step7.step7Language.ArrayDT
import cern.plcverif.plc.step7.step7Language.ConstDeclarationBlock
import cern.plcverif.plc.step7.step7Language.DataBlock
import cern.plcverif.plc.step7.step7Language.DataType
import cern.plcverif.plc.step7.step7Language.ElementaryDT
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit
import cern.plcverif.plc.step7.step7Language.FbOrUdtDT
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.FunctionBlock
import cern.plcverif.plc.step7.step7Language.NamedConstantDeclaration
import cern.plcverif.plc.step7.step7Language.OrganizationBlock
import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.step7Language.StlNetwork
import cern.plcverif.plc.step7.step7Language.StlStatementList
import cern.plcverif.plc.step7.step7Language.StructDT
import cern.plcverif.plc.step7.step7Language.UserDefinedDataType
import cern.plcverif.plc.step7.step7Language.Variable
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock
import cern.plcverif.plc.step7.util.DataTypeUtil
import cern.plcverif.plc.step7.util.SimpleExpressionUtil
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import cern.plcverif.plc.step7.util.StlHelper
import com.google.inject.Inject
import org.eclipse.xtext.ui.IImageHelper
import org.eclipse.xtext.ui.editor.outline.IOutlineNode
import org.eclipse.xtext.ui.editor.outline.impl.DefaultOutlineTreeProvider
import org.eclipse.xtext.ui.editor.outline.impl.DocumentRootNode
import cern.plcverif.plc.step7.step7Language.GlobalVariableBlock

/**
 * Customization of the default outline structure.
 *
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#outline
 */
class Step7LanguageOutlineTreeProvider extends DefaultOutlineTreeProvider {
 	@Inject
    IImageHelper imageHelper;
    
	protected def _createChildren(DocumentRootNode parentNode, ProgramFile e) {
		for (globalVarBlock : e.globalVariables){
			createNode(parentNode, globalVarBlock);
		}
		
		for (unit : e.programUnits) {
			createNode(parentNode, unit);
		}
	}
	
	// PROGRAM UNITS
	protected def _createChildren(IOutlineNode parentNode, ExecutableProgramUnit e) {
		for (varDeclBlock : e.declarationSection.variableDeclarations + 
				e.declarationSection.constantDeclarations
		) {
			createNode(parentNode, varDeclBlock);
		}
		
		if (e.statements instanceof StlStatementList) {
			createNode(parentNode, e.statements);
		}
	}
	
	protected def _text(OrganizationBlock e) {
		return '''«e.name»'''
	}
	
	protected def _image(OrganizationBlock e) {
		return image("OB");
	}
	
	protected def _text(FunctionBlock e) {
		return '''«e.name»'''
	}
	
	protected def _image(FunctionBlock e) {
		return image("FB");
	}
	
	protected def _text(Function e) {
		return '''«e.name» : «varTypeToString(e.returnType)»'''
	}
	
	protected def _image(Function e) {
		return image("FC");
	}
	
	protected def _createChildren(IOutlineNode parentNode, UserDefinedDataType e) {
		for (v : e.declaration.members.map[it | it.variables].flatten) {
			createNode(parentNode, v);
		}
	}
	
	protected def _text(UserDefinedDataType e) {
		return '''«e.name»'''
	}
	
	protected def _image(UserDefinedDataType e) {
		return image("UDT");
	}
	
	protected def _createChildren(IOutlineNode parentNode, DataBlock e) {
		if (e.structure instanceof StructDT) {
			for (v : (e.structure as StructDT).members.map[it | it.variables].flatten) {
				createNode(parentNode, v);	
			}
		}
	}
	
	protected def _text(DataBlock e) {
		val structure = e.structure;
		return '''DATA_BLOCK «e.name»«IF structure instanceof FbOrUdtDT» of «structure.type.name»«ENDIF»'''
	}
	
	protected def _image(DataBlock e) {
		if (e.structure instanceof FbOrUdtDT && (e.structure as FbOrUdtDT).type instanceof FunctionBlock) {
			return image("instanceDB");
		} else {
			return image("sharedDB");
		}
	}
	
	protected def _isLeaf(DataBlock e) {
		return !(e.structure instanceof StructDT);
	}
	
	
	// VARIABLE DECLARATION
	
	protected def _createChildren(IOutlineNode parentNode, VariableDeclarationBlock e) {
		for (variable : e.variables.map[it | it.variables].flatten) {
			createNode(parentNode, variable);
		}
	}
	
	protected def _text(VariableDeclarationBlock e) {
		return '''«e.direction.literal»'''
	}
	
	protected def _createChildren(IOutlineNode parentNode, GlobalVariableBlock e) {
		for (variable : e.variables.map[it | it.variables].flatten) {
			createNode(parentNode, variable);
		}
	}
	
	protected def _text(GlobalVariableBlock e) {
		return '''«e.direction.literal»'''
	}
	
	protected def _createChildren(IOutlineNode parentNode, Variable e) {
		// no children
		return;
	}
	
	protected def _isLeaf(Variable modelElement) {
		return true
	}
	
	protected def _text(Variable e) {
		return '''«e.name» : «Step7LanguageHelper.getDataType(e).varTypeToString»'''
	}
	
	
	// CONSTANT DECLARATION
	
	protected def _createChildren(IOutlineNode parentNode, ConstDeclarationBlock e) {
		for (const : e.constants) {
			createNode(parentNode, const);
		}
	}
	
	protected def _text(ConstDeclarationBlock e) {
		return "Constants"
	}
	
	protected def _createChildren(IOutlineNode parentNode, NamedConstantDeclaration e) {
		// no children
		return;
	}
	
	protected def _isLeaf(NamedConstantDeclaration modelElement) {
		return true
	}
	
	protected def _text(NamedConstantDeclaration e) {
		return '''«e.name» := «SimpleExpressionUtil.evaluateSimpleExpression(e.value)»'''
	}
	
	
	// STL NETWORKS
	
	protected def _createChildren(IOutlineNode parentNode, StlStatementList e) {
		for (network : e.networks) {
			createNode(parentNode, network);
		}
	}
	
	protected def _text(StlStatementList e) {
		return '''Networks'''
	}
	
	protected def _createChildren(IOutlineNode parentNode, StlNetwork e) {
		// no children
		return;
	}
	
	protected def _text(StlNetwork e) {
		return '''NETWORK «StlHelper.getNetworkTitle(e)»''';
	}
	
	protected def _image(StlNetwork e) {
		return image("NETWORK");
	}
	
	protected def _isLeaf(StlNetwork modelElement) {
		return true
	}
	
	
	// Helper
	private def image(String id) {
		return imageHelper.getImage('''«id».png''');
	}
	
	private def dispatch CharSequence varTypeToString(DataType e) {
		return "?"
	}
	
	private def dispatch CharSequence varTypeToString(Void e) {
		return "?"
	}
	
	private def dispatch CharSequence varTypeToString(ElementaryDT e) {
		return e.type.literal;
	}
	
	private def dispatch CharSequence varTypeToString(ArrayDT e) {
		return '''ARRAY[«FOR dim : e.dimensions SEPARATOR ', '»«DataTypeUtil.arrayRangeLower(dim)»..«DataTypeUtil.arrayRangeUpper(dim)»«ENDFOR»] OF «varTypeToString(e.baseType)»''';
	}
	
	private def dispatch CharSequence varTypeToString(StructDT e) {
		return "STRUCT";
	}
	
	private def dispatch CharSequence varTypeToString(FbOrUdtDT e) {
		return e.type.name;
	}
}
