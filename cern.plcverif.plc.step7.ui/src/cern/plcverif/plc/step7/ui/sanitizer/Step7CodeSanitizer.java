/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.ui.sanitizer;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;

/**
 * STEP 7 code sanitization with GUI.
 */
public final class Step7CodeSanitizer {
	private static class SanitizationReplacement {
		private final String patternString;
		private final String replacement;
	
		public SanitizationReplacement(String regexPattern, String replacement) {
			this.patternString = regexPattern;
			this.replacement = replacement;
		}
	
		public Pattern getPattern() {
			return Pattern.compile(patternString);
		}
	
		public String getPatternString() {
			return patternString;
		}
	
		public String getReplacement() {
			return replacement;
		}
	}

	private static final List<SanitizationReplacement> REPLACEMENTS = new ArrayList<>();
	static {
		// Unwanted Unicode characters, i.e. UTF-8 BOM -> UTF-8
		REPLACEMENTS.add(new SanitizationReplacement("[\uFEFF-\uFFFF]", ""));
		
		// TITLE = abc def ghi --> TITLE= abc def ghi
		REPLACEMENTS.add(new SanitizationReplacement("(?i)(?m)^(\\s*TITLE)[\\t ]+=([\\t ]*[^'\\t ]\\S*[\\t ]+\\S+.*)$", "$1=$2"));
		
		// TITLE = gshs'sddw --> TITLE= gshs'sddw
		REPLACEMENTS.add(new SanitizationReplacement("(?i)(?m)^(\\s*TITLE)[\\t ]+=([\\t ]*[^'\\t ]\\S*'\\S*.*[^'\\t \\n])", "$1=$2"));
		
		// TITLE = 'gshs'sddw' --> TITLE= 'gshs'sddw'
		REPLACEMENTS.add(new SanitizationReplacement("(?i)(?m)^(\\s*TITLE)[\\t ]+=([\\t ]*'\\S*'\\S*.*')$", "$1=$2"));
		
		// TITLE = --> TITLE= 
		REPLACEMENTS.add(new SanitizationReplacement("(?i)(?m)^(\\s*)TITLE[\\t ]+=\\s*$", "$1TITLE="));
		
		// L  0.3 --> L0.3
		REPLACEMENTS.add(new SanitizationReplacement("\\bLX?\\s+(\\d+\\.\\d+)", "L$1"));
		
		// LB  13 --> LB13
		REPLACEMENTS.add(new SanitizationReplacement("\\bL([BWD])\\s+(\\d+)([,;])", "L$1$2$3"));
		
		// [1+2.. 30] --> [1+2 .. 30]
		String expr = "[\\(\\)\\+\\-\\*/0-9a-zA-Z_]*[\\(\\)\\+\\-\\*/a-zA-Z_][\\(\\)\\+\\-\\*/0-9a-zA-Z_]*";
		REPLACEMENTS.add(new SanitizationReplacement(
				String.format("([\\[,]\\s*)(%s)\\.\\.\\s*([^\\],]+)(?=\\s*[\\],])", 
						expr).replace(" ", ""), "$1$2 .. $3"));
	}

	private Step7CodeSanitizer() {
		// Utility class.
	}
	
	/**
	 * Suggest sanitization on GUI if there are some possible STL code replacements.
	 * @param document Xtext document to be checked
	 * @param promptNeeded If true, the user will be asked whether sanitization is required
	 */
	public static void suggestSanitization(IXtextDocument document, boolean promptNeeded) {
		String content = document.get();
		if (isReplacementPossible(content)) {
			Shell shell = Display.getCurrent().getActiveShell();
			boolean sanitizationNeeded = !promptNeeded || MessageDialog.openQuestion(shell, "Automated sanitization",
					"This STEP 7 code contains some syntactic features that are not supported by PLCverif. Would you like to convert them to equivalents supported by PLCverif? It will not influence the behavior and will still be a valid STEP 7 code.");
			if (sanitizationNeeded) {
				StringBuilder log = new StringBuilder();
				for (SanitizationReplacement replacement : REPLACEMENTS) {
					content = loggerReplaceAll(content, replacement, log);
				}
				document.set(content);
				MessageDialog.openInformation(shell, "Automated sanitization",
						"Sanitization done successfully." + System.lineSeparator() + log.toString());
			}
		}
	}

	private static boolean isReplacementPossible(String text) {
		for (SanitizationReplacement replacement : REPLACEMENTS) {
			if (replacement.getPattern().matcher(text).find()) {
				return true;
			}
		}

		return false;
	}

	private static String loggerReplaceAll(CharSequence original, SanitizationReplacement replacement, StringBuilder log) {
		Matcher matcher = replacement.getPattern().matcher(original);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			String message = String.format("Replacement at character %s: '%s' replaced by '%s'.%n",
					matcher.toMatchResult().start(),
					matcher.group(0).trim(),
					matcher.group(0).trim().replaceAll(replacement.getPatternString(), replacement.getReplacement()));
			log.append(message);
			matcher.appendReplacement(sb, replacement.getReplacement());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}
}
