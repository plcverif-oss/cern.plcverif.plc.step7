/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/

/*
 * generated by Xtext 2.11.0
 */
package cern.plcverif.plc.step7.ui.labeling

import cern.plcverif.plc.step7.step7Language.AdditiveExpression
import cern.plcverif.plc.step7.step7Language.ArrayDT
import cern.plcverif.plc.step7.step7Language.DataType
import cern.plcverif.plc.step7.step7Language.ElementaryDT
import cern.plcverif.plc.step7.step7Language.Expression
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.FunctionBlock
import cern.plcverif.plc.step7.step7Language.MultiplicativeExpression
import cern.plcverif.plc.step7.step7Language.OrganizationBlock
import cern.plcverif.plc.step7.step7Language.ParameterDT
import cern.plcverif.plc.step7.step7Language.QualifiedRef
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock
import cern.plcverif.plc.step7.typecomputer.Step7CachedTypeProvider
import cern.plcverif.plc.step7.util.DataTypeUtil
import com.google.inject.Inject
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider
import org.eclipse.xtext.ui.label.DeclarativeLabelProvider

/**
 * Provides labels for EObjects.
 * 
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#label-provider
 */
class Step7LanguageLabelProvider extends DeclarativeLabelProvider {
	@Inject
	Step7CachedTypeProvider typeProvider;

	@Inject
	new(AdapterFactoryLabelProvider delegate) {
		super(delegate);
	}

	// Labels and icons can be computed like this:
//	def image(Greeting ele) {
//		'Greeting.gif'
//	}

	def text(QualifiedRef e) {
		e.prefix.toString + '::' + e.ref.toString 
	}
	
	def text(Expression e) {
		val computedType = typeProvider.getTypeDescriptorFor(e);
		return '''«e.class.simpleName» (exp.: «computedType.expectedDT.typeName», nom.: «computedType.nominalDT.typeName»)'''
	}
	
	def dispatch String typeName(Void type) { return "<null>"; }
	def dispatch String typeName(DataType type) { return "Unknown data type: " + type; }
	def dispatch String typeName(ElementaryDT type) { return type.type.literal; }
	def dispatch String typeName(ParameterDT type) { return type.type.literal; }
	def dispatch String typeName(ArrayDT type) { return '''ARRAY [«FOR dim : type.dimensions SEPARATOR ','»«DataTypeUtil.arrayRangeLower(dim)»..«DataTypeUtil.arrayRangeUpper(dim)»«ENDFOR»] OF «typeName(type.baseType)»'''; }
	
	def text(MultiplicativeExpression e) {
		val computedType = typeProvider.getTypeDescriptorFor(e);
		return '''«e.operator.toString» (exp.: «computedType.expectedDT.typeName», nom.: «computedType.nominalDT.typeName»)'''
	}
	
	def text(AdditiveExpression e) {
		val computedType = typeProvider.getTypeDescriptorFor(e);
		return '''«e.operator.toString» (exp.: «computedType.expectedDT.typeName», nom.: «computedType.nominalDT.typeName»)'''
	}
	
	def text(FunctionBlock e) {
		'''FUNCTION_BLOCK «e.name»'''
	}
	
	def text(Function e) {
		'''FUNCTION «e.name»'''
	}
	
	def text(OrganizationBlock e) {
		'''ORGANIZATION_BLOCK «e.name»'''
	}
	
	def text(VariableDeclarationBlock e) {
		'''«e.direction.toString.toUpperCase» block'''
	}
}
