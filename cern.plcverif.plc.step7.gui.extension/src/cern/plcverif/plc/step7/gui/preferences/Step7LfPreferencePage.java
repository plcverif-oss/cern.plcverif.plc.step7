/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.gui.preferences;

import java.io.IOException;

import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.gui.preferences.SettingsPreferenceStoreFactory;
import cern.plcverif.plc.step7.extension.Step7ParserExtension;
import cern.plcverif.plc.step7.extension.Step7Settings;
import cern.plcverif.plc.step7.step7Language.CompatbilityLevelEnum;

public class Step7LfPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	public Step7LfPreferencePage() {
		super(GRID);
	}

	@Override
	public void init(IWorkbench workbench) {
		try {
			Settings defaultSettings = SpecificSettingsSerializer.toGenericSettings(new Step7Settings());
			Preconditions.checkState(defaultSettings instanceof SettingsElement,
					"Default settings does not have appropriate root element for STEP 7 language frontend plug-in.");
			setPreferenceStore(SettingsPreferenceStoreFactory.createStore(Step7ParserExtension.CMD_ID,
					defaultSettings.toSingle()));
		} catch (IOException | SettingsParserException | SettingsSerializerException e) {
			throw new RuntimeException(e); // NOPMD
		}
	}

	@Override
	protected void createFieldEditors() {
		final Composite parent = getFieldEditorParent();

		addField(createCompilerFieldEditor(parent));
	}

	private static FieldEditor createCompilerFieldEditor(Composite parent) {
		return new RadioGroupFieldEditor(Step7Settings.COMPILER, "Compiler semantics:", 1,
				new String[][] { { "STEP7 v5.5", CompatbilityLevelEnum.STEP7_V55.getLiteral() },
						{ "TIA for S7-300", CompatbilityLevelEnum.TIA300.getLiteral() },
						{ "TIA for S7-1200", CompatbilityLevelEnum.TIA1200.getLiteral() },
						{ "TIA for S7-1500", CompatbilityLevelEnum.TIA1500.getLiteral() } },
				parent);
	}
}
