/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.gui.wizards;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.internal.Workbench;
import org.eclipse.ui.internal.progress.ProgressManagerUtil;

import cern.plcverif.base.common.utils.IoUtils;

@SuppressWarnings("restriction")
public class NewSclFileWizard extends Wizard implements INewWizard {

	private NewSclFileWizardPage newSclFileWizardPage;

	public static final String ID = "cern.plcverif.plc.step7.gui.wizards.NewSclFileWizard";

	@Override
	public final void addPages() {
		addPage(newSclFileWizardPage);
	}

	@Override
	public final boolean performFinish() {
		// Create the file.
		IFile file = newSclFileWizardPage.createNewFile();

		if (file != null) {
			try {
				// Set initial contents
				setInitialContent(file);
				file.setCharset("utf8", new NullProgressMonitor());

				// Open the newly created file.
				IDE.openEditor(Workbench.getInstance().getActiveWorkbenchWindow().getActivePage(), file);
			} catch (CoreException | IOException e) {
				MessageDialog.openError(ProgressManagerUtil.getDefaultParent(), "Error", e.getMessage());
			}
			return true;
		} else {
			return false;
		}
	}

	private static void setInitialContent(IFile file) throws CoreException, IOException {
		String initContent = IoUtils.readResourceFile("DefaultSclFile.txt", NewStlFileWizard.class.getClassLoader());
		try (InputStream stream = new ByteArrayInputStream(initContent.getBytes(StandardCharsets.UTF_8))) {
			file.setContents(stream, IResource.KEEP_HISTORY, new NullProgressMonitor());
		}
	}

	@Override
	public final void init(final IWorkbench workbench, final IStructuredSelection selection) {
		setWindowTitle("New SCL file");
		this.newSclFileWizardPage = new NewSclFileWizardPage(selection);
	}

}
