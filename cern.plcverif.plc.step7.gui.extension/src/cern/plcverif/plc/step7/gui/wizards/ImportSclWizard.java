/*******************************************************************************
 * (C) Copyright CERN 2023-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.gui.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;

public class ImportSclWizard extends Wizard implements IImportWizard {
	public static final String ID = "cern.plcverif.plc.step7.gui.wizards.ImportSclWizard";
	private ImportSclWizardPage wizardPage;

	@Override
	public void addPages() {
		addPage(wizardPage);
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		String name = "Import SCL/STL file";
		wizardPage = new ImportSclWizardPage(name, selection);
	}

	@Override
	public boolean performFinish() {
		return wizardPage.finish();
	}
}
