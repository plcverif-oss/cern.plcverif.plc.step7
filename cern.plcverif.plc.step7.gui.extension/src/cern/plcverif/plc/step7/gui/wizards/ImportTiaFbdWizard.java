/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.gui.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;

public class ImportTiaFbdWizard extends Wizard implements IImportWizard {
	public static final String ID = "cern.plcverif.plc.step7.gui.wizards.ImportTiaFbdWizard";
	private ImportTiaFbdWizardPage wizardPage;

	@Override
	public void addPages() {
		addPage(wizardPage);
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		String name = "Import TIA Portal FBD XML";
		wizardPage = new ImportTiaFbdWizardPage(name, selection);
	}

	@Override
	public boolean performFinish() {
		return wizardPage.finish();
	}
}
