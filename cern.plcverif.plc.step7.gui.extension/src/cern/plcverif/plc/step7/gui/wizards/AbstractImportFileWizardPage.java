/*******************************************************************************
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * This file contains parts of the Eclipse project, licensed under EPL.
 * 
 * Contributors:
 *   Michael Lettrich - initial API and implementation
 *   Daniel Darvas - improvements
 *******************************************************************************/

package cern.plcverif.plc.step7.gui.wizards;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.WizardResourceImportPage;

public abstract class AbstractImportFileWizardPage extends WizardResourceImportPage {
	protected Text sourceNameField;
	protected Button sourceBrowseButton;
	protected Button overwriteExistingResourcesCheckbox;
	private boolean entryChanged = false;
	protected boolean overrideSource = false;
	private static final String SELECT_SOURCE_TITLE = "Select source";
	protected static final String IMPORT_FILE_DEFAULT = "File to import:";
	protected static final String SOURCE_EMPTY_MESSAGE = "Source is empty.";
	protected static final String DESCRIPTION_LABEL = "";
	protected static final String BROWSE_BUTTON_TEXT = "Browse...";
	protected static final String ERROR_EXISTENCE = "The provided file does not exist.";
	protected static final String ERROR_OVERWRITE = "An equally named file in the workspace already exists.";
	protected static final String ERROR_NULL_MESSAGE = "No import source has been specified. Please try again.";
	protected static final String OVERRIDE_EXISTING = "Overwrite existing file";

	public AbstractImportFileWizardPage(String name, IStructuredSelection selection) {
		super(name, selection);
	}

	/**
	 * The Finish button was pressed. Try to do the required work now and answer
	 * a boolean indicating success. If false is returned then the wizard will
	 * not close.
	 *
	 * @return False if the wizard cannot be closed yet
	 */
	public abstract boolean finish();

	/**
	 * Returns the label to be shown next to the field of the file to be imported.
	 * @return Label to be displayed for file to be imported
	 */
	protected String getImportFileLabelText() {
		return IMPORT_FILE_DEFAULT;
	}

	/**
	 * Shall return the extensions supported for importing.
	 * 
	 * @return Array of supported extensions
	 */
	protected abstract String[] getSupportedFileTypes();

	/**
	 * Returns the file to be created upon importation of the selected file to
	 * the given resource path.
	 * 
	 * @param resourcePath
	 *            Import target path
	 * @param selectedPath
	 *            Import source
	 * @return Target file
	 */
	protected abstract IFile getTargetFile(IPath resourcePath, String selectedPath);

	@Override
	protected ITreeContentProvider getFileProvider() { // NOPMD
		// not needed
		return null;
	}

	@Override
	protected ITreeContentProvider getFolderProvider() { // NOPMD
		// not needed
		return null;
	}
	
	/**
	 * This is part of the Eclipse file importer and was taken in accordance
	 * with the Eclipse license.
	 */
	@Override
	protected void createSourceGroup(Composite parent) {
		Composite sourceContainerGroup = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		sourceContainerGroup.setLayout(layout);
		sourceContainerGroup.setFont(parent.getFont());
		sourceContainerGroup.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL));

		Label groupLabel = new Label(sourceContainerGroup, SWT.NONE);
		groupLabel.setText(getImportFileLabelText());
		groupLabel.setFont(parent.getFont());

		// source name entry field
		this.sourceNameField = new Text(sourceContainerGroup, SWT.BORDER);
		GridData data = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL);
		data.widthHint = SIZING_TEXT_FIELD_WIDTH;
		this.sourceNameField.setLayoutData(data);
		this.sourceNameField.setFont(parent.getFont());

		this.sourceNameField.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				updateFromSourceField();
			}
		});

		this.sourceNameField.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				// If there has been a key pressed then mark as dirty
				AbstractImportFileWizardPage.this.entryChanged = true;
			}
		});

		this.sourceNameField.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				// Clear the flag to prevent constant update
				if (AbstractImportFileWizardPage.this.entryChanged) {
					AbstractImportFileWizardPage.this.entryChanged = false;
					updateFromSourceField();
				}
			}
		});

		// source browse button
		this.sourceBrowseButton = new Button(sourceContainerGroup, SWT.PUSH);
		this.sourceBrowseButton.setText(BROWSE_BUTTON_TEXT);
		this.sourceBrowseButton.addListener(SWT.Selection, this);
		this.sourceBrowseButton.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		this.sourceBrowseButton.setFont(parent.getFont());
		setButtonLayoutData(this.sourceBrowseButton);
	}

	/**
	 * This is part of the Eclipse file importer and was taken in accordance
	 * with the Eclipse license. Create the import options specification
	 * widgets.
	 */
	protected void createOptionsGroupButtons(Group optionsGroup) {
		// overwrite checkbox
		this.overwriteExistingResourcesCheckbox = new Button(optionsGroup, SWT.CHECK);
		this.overwriteExistingResourcesCheckbox.setFont(optionsGroup.getFont());
		this.overwriteExistingResourcesCheckbox.setText(OVERRIDE_EXISTING);
		this.overwriteExistingResourcesCheckbox.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				widgetDefaultSelected(e);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				updateOverwrite();
				validateSourceGroup();
				updateWidgetEnablements();
			}
		});
	}

	/**
	 * This is part of the Eclipse file importer and was taken in accordance
	 * with the Eclipse license.
	 * 
	 * Update the receiver from the source name field.
	 */
	protected void updateFromSourceField() {
		// Update enablements when this is selected
		updateWidgetEnablements();
	}

	/**
	 * Check the state of the Overwrite checkbox and set
	 * <Code>overrideSource</Code> accordingly.
	 */
	protected void updateOverwrite() {
		this.overrideSource = this.overwriteExistingResourcesCheckbox.getSelection();
	}

	/**
	 * This is part of the Eclipse file importer and was taken in accordance
	 * with the Eclipse license.
	 * 
	 * Handle all events and enablements for widgets in this dialog
	 *
	 * @param event
	 *            Event
	 */
	public void handleEvent(Event event) {
		if (event.widget == this.sourceBrowseButton) {
			handleSourceBrowseButtonPressed();
		}

		super.handleEvent(event);
	}

	/**
	 * Open a file source browser so that the user can specify a file to import
	 * from.
	 */
	protected void handleSourceBrowseButtonPressed() {
		FileDialog dialog = new FileDialog(this.sourceNameField.getShell(), SWT.OpenDocument);
		dialog.setText(SELECT_SOURCE_TITLE);
		dialog.setFilterExtensions(getSupportedFileTypes());

		String selectedPath = dialog.open();
		if (selectedPath != null) {
			this.sourceNameField.setText(selectedPath);
		}
	}

	/**
	 * Get the handle to the open file on disk if the file exists. Else throw an
	 * illegal argument exception.
	 * 
	 * @param path
	 *            to the file on disk.
	 * @return a handle pointing to the file on disk
	 * @throws IllegalArgumentException
	 *             The file at the given path was not found.
	 */
	protected File getFilePath(String text) throws IllegalArgumentException {
		String path = text;
		// for invalid path, abstract, empty path name is generated.
		File file = new File(path);
		if (file.exists()) {
			return file;
		} else {
			throw new IllegalArgumentException("Cannot find file at path " + path);
		}
	}

	/**
	 * Save the file at path <Code>filePath</Code> into the project.
	 * 
	 * @param fileContent
	 *            content of the file as a string.
	 * @param filePath
	 *            string representation of the source path on disk.
	 * @param projectPath
	 *            path within the workspace where the file should be imported.
	 * @return true if importing the file was successful else return false.
	 */
	protected Boolean saveFileToWorkspace(String fileContent, String filePath, IPath projectPath) {
		IFile newFile = getFileHandle(projectPath, filePath);

		InputStream fileStream = new ByteArrayInputStream(fileContent.getBytes());
		try {

			// delete file if we override the old file
			if (this.overrideSource) {
				newFile.delete(true, null);
			}

			// create a new file if we import it
			newFile.create(fileStream, false, null);
		} catch (CoreException e) {
			e.printStackTrace();
			return false;
		}
		return true;

	}

	/**
	 * For the imported file we have to specify the correct type of delimiter
	 * used. If the delimiter of the source code is UNIX style (<Code>\n</Code>)
	 * we return <Code>\n</Code> If the delimiter of the source code is Windows
	 * style (<Code>\r\n</Code>) we return <Code>\r\n</Code>
	 * 
	 * @param fileContent
	 *            string representation of the file content.
	 * @return <Code>\n</Code> if UNIX style delimiters are detected.
	 *         <Code>\r\n</Code> if Windows style delimiters are detected.
	 */
	protected String setDelimiter(String fileContent) {
		if (fileContent.contains("\r")) {
			return "\r\n";
		} else {
			return "\n";
		}
	}

	/**
	 * Create a new file handle in the place specified in the
	 * <Code>project</Code> variable with the file name specified in the file
	 * path.
	 * 
	 * @param projectName
	 *            string representation of the path within the workspace to
	 *            which to import
	 * @param filePath
	 *            path of the file on disk that is about to be imported
	 * @return handle to the open file
	 */
	protected IFile getFileHandle(IPath projectPath, String filePath) {
		IPath fullPath = projectPath.append(new Path(filePath).lastSegment());
		return ResourcesPlugin.getWorkspace().getRoot().getFile(fullPath);
	}

	/**
	 * This is partially part of the Eclipse file importer and was taken in
	 * accordance with the Eclipse license.
	 * 
	 * @return
	 */
	@Override
	protected boolean validateSourceGroup() {
		String selectedPath = this.sourceNameField.getText();

		if (selectedPath == null || selectedPath.equals("")) {
			setErrorMessage(ERROR_NULL_MESSAGE);
			return false;
		}

		// check if the file exists on the hard drive
		else if (!(new File(selectedPath).exists())) {
			setErrorMessage(ERROR_EXISTENCE);
			return false;
		}

		// check if we overwrite source files in the
		else if (getTargetFile(getResourcePath(), selectedPath).exists() && !this.overrideSource) {
			setErrorMessage(ERROR_OVERWRITE);
			return false;
		} else {
			setErrorMessage(null);
			return true;
		}
	}
}