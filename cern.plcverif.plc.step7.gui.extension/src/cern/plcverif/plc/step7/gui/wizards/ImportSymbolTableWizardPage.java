/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.gui.wizards;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import cern.plcverif.plc.step7.symboltable.Symbol;
import cern.plcverif.plc.step7.symboltable.SymbolTableParser;
import cern.plcverif.plc.step7.symboltable.SymbolTableParser.PortalVersion;
import cern.plcverif.plc.step7.symboltable.SymbolsToGlobalVars;
import cern.plcverif.plc.tia.fbd.parser.TiaPortalVersion;

public class ImportSymbolTableWizardPage extends AbstractImportFileWizardPage {
	
	private Combo versionSelectCombo;
	
	protected static final String DESCRIPTION_LABEL = "Choose a symbol table (SDF) file to import.";

	protected ImportSymbolTableWizardPage(String name, IStructuredSelection selection) {
		super(name, selection);
		setTitle(name);
		setDescription(DESCRIPTION_LABEL);
	}

	@Override
	public boolean finish() {
		saveWidgetValues();
		updateWidgetEnablements();

		File file = getFilePath(sourceNameField.getText());
		
		
		String versionStr = this.versionSelectCombo.getItem(this.versionSelectCombo.getSelectionIndex());

		PortalVersion version;
		if (versionStr.equals("STEP7")) {
			version = PortalVersion.STEP7;
		} else if (versionStr.equals("TIA")) {
			version = PortalVersion.TIA;
		} else {	
			throw new UnsupportedOperationException("Unknown portal version string supplied through combo list: " + versionStr);
		}
		

		try {
			List<Symbol> symbols = SymbolTableParser.parse(file, version);
			String fileContent = SymbolsToGlobalVars.represent(symbols);

			String outFileName = replaceExtension(file.getName(), "scl");

			return saveFileToWorkspace(fileContent, outFileName, getResourcePath());
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	protected void createOptionsGroupButtons(Group optionsGroup) {
		super.createOptionsGroupButtons(optionsGroup);
		
		Composite composite = new Composite(optionsGroup, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(composite);
		GridLayoutFactory.swtDefaults().numColumns(2).applyTo(composite);
		
		Label versionSelectLabel = new Label(composite, SWT.NONE);
		versionSelectLabel.setText("Portal version:");
		GridDataFactory.fillDefaults()
			.grab(true, false)
			.align(SWT.LEFT, SWT.CENTER)
			.applyTo(versionSelectLabel);
		
		this.versionSelectCombo = new Combo(composite, SWT.READ_ONLY);
		this.versionSelectCombo.setItems(Stream.of(PortalVersion.values()).map(Enum::name).toArray(String[]::new));
		GridDataFactory.fillDefaults()
			.grab(true,  false)
			.align(SWT.FILL, SWT.CENTER).hint(200, SWT.DEFAULT)
			.applyTo(this.versionSelectCombo);
		this.versionSelectCombo.select(0);
	}

	// e.g. apple.sdf -> apple.scl if newExtension is scl
	private static String replaceExtension(String filename, String newExtension) {
		int lastDotIndex = filename.lastIndexOf(".");
		String filenameWithoutExtension;

		if (lastDotIndex >= 0) {
			filenameWithoutExtension = filename.substring(0, lastDotIndex);
		} else {
			// no dot in filename
			filenameWithoutExtension = filename;
		}

		return String.format("%s.%s", filenameWithoutExtension, newExtension);
	}

	@Override
	protected String[] getSupportedFileTypes() {
		return new String[] { "*.sdf" };
	}

	@Override
	protected IFile getTargetFile(IPath projectPath, String selectedPath) {
		String filename = new Path(selectedPath).lastSegment();
		return getFileHandle(projectPath, replaceExtension(filename, "scl"));
	}
	
	@Override
	protected String getImportFileLabelText() {
		return "Symbol table file to import:";
	}
}
