/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.gui.wizards;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.xtext.xbase.lib.Pair;

import cern.plcverif.plc.step7.gui.wizards.utils.FileUtils;
import cern.plcverif.plc.tia.fbd.Block;
import cern.plcverif.plc.tia.fbd.codegen.BlockToStl;
import cern.plcverif.plc.tia.fbd.codegen.OutputWiresTerminalFinderStrategy;
import cern.plcverif.plc.tia.fbd.parser.FbdXmlParser;
import cern.plcverif.plc.tia.fbd.parser.FbdXmlParserException;
import cern.plcverif.plc.tia.fbd.parser.TiaPortalVersion;

public class ImportTiaFbdWizardPage extends AbstractImportFileWizardPage {
	
	private Combo versionSelectCombo;
	
	protected static final String DESCRIPTION_LABEL = "Choose a TIA Portal FBD file (XML) to import.";
	protected static final String[] SUPPORTED_VERSIONS = { "V15", "V15.1", "V16", "V17", "V18" };

	protected ImportTiaFbdWizardPage(String name, IStructuredSelection selection) {
		super(name, selection);
		setTitle(name);
		setDescription(DESCRIPTION_LABEL);
	}
	
	@Override
	protected void createOptionsGroupButtons(Group optionsGroup) {
		super.createOptionsGroupButtons(optionsGroup);
		
		Composite composite = new Composite(optionsGroup, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(composite);
		GridLayoutFactory.swtDefaults().numColumns(2).applyTo(composite);
		
		Label versionSelectLabel = new Label(composite, SWT.NONE);
		versionSelectLabel.setText("TIA Portal version:");
		GridDataFactory.fillDefaults()
			.grab(true, false)
			.align(SWT.LEFT, SWT.CENTER)
			.applyTo(versionSelectLabel);
		
		this.versionSelectCombo = new Combo(composite, SWT.READ_ONLY);
		this.versionSelectCombo.setItems(SUPPORTED_VERSIONS);
		GridDataFactory.fillDefaults()
			.grab(true,  false)
			.align(SWT.FILL, SWT.CENTER).hint(200, SWT.DEFAULT)
			.applyTo(this.versionSelectCombo);
		this.versionSelectCombo.select(SUPPORTED_VERSIONS.length - 1);
	}

	@Override
	public boolean finish() {
		saveWidgetValues();
		updateWidgetEnablements();

		List<File> files = Arrays.stream(sourceNameField.getText().split(","))
				.map(name -> getFilePath(name))
				.collect(Collectors.toList());

		String versionStr = this.versionSelectCombo.getItem(this.versionSelectCombo.getSelectionIndex());

		TiaPortalVersion version;
		if (versionStr.equals("V15")) {
			version = TiaPortalVersion.V15;
		} else if (versionStr.equals("V15.1")) {
			version = TiaPortalVersion.V15_1;
		} else if (versionStr.equals("V16")){
			version = TiaPortalVersion.V16;
		} else if (versionStr.equals("V17")){
			version = TiaPortalVersion.V17;
		} else if (versionStr.equals("V18")){
			version = TiaPortalVersion.V18;
		} 
		else {	
			throw new UnsupportedOperationException("Unknown version number string supplied through combo list: " + versionStr);
		}
		
		List<Pair<String, String>> fails = new ArrayList<>();
		
		for (File file : files) {
			try {
				Block block = FbdXmlParser.parse(file.getAbsolutePath(), version);
				
				String fileContent = BlockToStl.translate(block, new OutputWiresTerminalFinderStrategy());
				String outFileName = FileUtils.replaceExtension(file.getAbsolutePath(), "scl");
	
				if (!saveFileToWorkspace(fileContent, outFileName, getResourcePath())) {
					fails.add(Pair.of(file.getName(), String.format("Error encountered while saving output file '%s' into the workspace.", outFileName)));
				}				
			} catch (FbdXmlParserException ex) {
				ex.printStackTrace();
				fails.add(Pair.of(file.getName(), ex.getMessage()));
			}
		}
		
		if (!fails.isEmpty()) {
			String errorMessage = "Could not import TIA Portal XML files. Make sure that the input file exists and it was exported either as an FBD or F_FBD.\nAffected files:\n";
			String fileErrors = String.join(",", (Iterable<String>) fails.stream().map(kv -> String.format("   %s: %s", kv.getKey(), kv.getValue()))::iterator);
			
			this.displayErrorDialog(errorMessage + fileErrors);
			return false;
		}
		
		return true;
	}

	@Override
	protected void handleSourceBrowseButtonPressed() {
		FileDialog dialog = new FileDialog(this.sourceNameField.getShell(), SWT.OpenDocument);
		dialog.setText("Select source");
		dialog.setFilterExtensions(getSupportedFileTypes());

		if (dialog.open() != null) {
			String path = dialog.getFilterPath();
			this.sourceNameField.setText(String.join(",", Arrays.stream(dialog.getFileNames()).map(f -> Paths.get(path, f).toString()).collect(Collectors.toList())));
		}
	}
	
	@Override
	protected boolean validateSourceGroup() {
		String selectedPath = this.sourceNameField.getText();

		if (selectedPath == null || selectedPath.equals("")) {
			setErrorMessage(ERROR_NULL_MESSAGE);
			return false;
		}

		String[] names = sourceNameField.getText().split(","); 
		
		if (!Arrays.stream(names).allMatch(name -> new File(name).exists())) {
			setErrorMessage(ERROR_EXISTENCE);
			return false;
		}

		if (Arrays.stream(names).anyMatch(name -> getTargetFile(getResourcePath(), name).exists()) && !this.overrideSource) {
			setErrorMessage(ERROR_OVERWRITE);
			return false;
		}

		setErrorMessage(null);
		return true;
	}
	
	@Override
	protected String[] getSupportedFileTypes() {
		return new String[] { "*.xml" };
	}

	@Override
	protected IFile getTargetFile(IPath projectPath, String selectedPath) {
		String filename = new Path(selectedPath).lastSegment();
		return getFileHandle(projectPath, FileUtils.replaceExtension(filename, "scl"));
	}
	
	@Override
	protected String getImportFileLabelText() {
		return "XML file to import:";
	}
}
