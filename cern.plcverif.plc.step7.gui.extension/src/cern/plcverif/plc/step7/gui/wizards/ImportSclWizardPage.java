/*******************************************************************************
 * (C) Copyright CERN 2023-2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.gui.wizards;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.xtext.xbase.lib.Pair;

import cern.plcverif.plc.step7.gui.wizards.utils.FileUtils;

public class ImportSclWizardPage extends AbstractImportFileWizardPage {
	
	protected static final String OVERRIDE_EXISTING = "Overwrite existing files";
	protected static final String DESCRIPTION_LABEL = "Choose a SCL/STL file to import.";

	protected ImportSclWizardPage(String name, IStructuredSelection selection) {
		super(name, selection);
		setTitle(name);
		setDescription(DESCRIPTION_LABEL);
	}
	
	@Override
	protected void createOptionsGroupButtons(Group optionsGroup) {
		super.createOptionsGroupButtons(optionsGroup);
		
		Composite composite = new Composite(optionsGroup, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(composite);
		GridLayoutFactory.swtDefaults().numColumns(2).applyTo(composite);
	}

	@Override
	public boolean finish() {
		saveWidgetValues();
		updateWidgetEnablements();

		List<File> files = Arrays.stream(sourceNameField.getText().split(","))
				.map(name -> getFilePath(name))
				.collect(Collectors.toList());
		
		List<Pair<String, String>> fails = new ArrayList<>();
		
		for (File file : files) {
			try {
				// Remove BOM symbol at start of UTF-8 BOM file 
				String fileContent = Files.readString(java.nio.file.Path.of(file.getPath()), StandardCharsets.UTF_8).replace("\uFEFF", "");
				String outFileName = FileUtils.replaceExtension(file.getAbsolutePath(), "scl");
	
				if (!saveFileToWorkspace(fileContent, outFileName, getResourcePath())) {
					fails.add(Pair.of(file.getName(), String.format("Error encountered while saving output file '%s' into the workspace.", outFileName)));
				}				
			} catch (IOException ex) {
				ex.printStackTrace();
				fails.add(Pair.of(file.getName(), ex.getMessage()));
			}
		}
		
		if (!fails.isEmpty()) {
			String errorMessage = "Could not import files. Make sure that the input file exists and it contained SCL/STL code.\nAffected files:\n";
			String fileErrors = String.join(",", (Iterable<String>) fails.stream().map(kv -> String.format("   %s: %s", kv.getKey(), kv.getValue()))::iterator);
			
			this.displayErrorDialog(errorMessage + fileErrors);
			return false;
		}
		
		return true;
	}

	@Override
	protected void handleSourceBrowseButtonPressed() {
		FileDialog dialog = new FileDialog(this.sourceNameField.getShell(), SWT.OpenDocument);
		dialog.setText("Select sources");
		dialog.setFilterExtensions(getSupportedFileTypes());

		if (dialog.open() != null) {
			String path = dialog.getFilterPath();
			this.sourceNameField.setText(String.join(",", Arrays.stream(dialog.getFileNames()).map(f -> Paths.get(path, f).toString()).collect(Collectors.toList())));
		}
	}
	
	@Override
	protected boolean validateSourceGroup() {
		String selectedPath = this.sourceNameField.getText();

		if (selectedPath == null || selectedPath.equals("")) {
			setErrorMessage(ERROR_NULL_MESSAGE);
			return false;
		}

		String[] names = sourceNameField.getText().split(","); 
		
		if (!Arrays.stream(names).allMatch(name -> new File(name).exists())) {
			setErrorMessage(ERROR_EXISTENCE);
			return false;
		}

		if (Arrays.stream(names).anyMatch(name -> getTargetFile(getResourcePath(), name).exists()) && !this.overrideSource) {
			setErrorMessage(ERROR_OVERWRITE);
			return false;
		}

		setErrorMessage(null);
		return true;
	}
	
	@Override
	protected String[] getSupportedFileTypes() {
		return new String[] { "*.udt;*.db;*.stl;*.scl" };
	}

	@Override
	protected IFile getTargetFile(IPath projectPath, String selectedPath) {
		String filename = new Path(selectedPath).lastSegment();
		return getFileHandle(projectPath, FileUtils.replaceExtension(filename, "scl"));
	}
	
	@Override
	protected String getImportFileLabelText() {
		return "Files to import:";
	}
}
