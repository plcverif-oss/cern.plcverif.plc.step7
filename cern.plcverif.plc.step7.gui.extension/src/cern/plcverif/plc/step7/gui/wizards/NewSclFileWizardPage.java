/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.gui.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;

public class NewSclFileWizardPage extends WizardNewFileCreationPage {

	public NewSclFileWizardPage(final IStructuredSelection selection) {
		super("NewSclFileWizardPage", selection);
		this.setTitle("SCL program file");
		this.setDescription("This wizard creates a new SCL program source file.");
		this.setFileExtension("scl");
	}

}
