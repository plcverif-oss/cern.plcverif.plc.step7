/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.gui.wizards.utils;

public final class FileUtils {

	private FileUtils() {}
	
	/**
	 * Replaces the extension for a given filename.
	 */
	public static String replaceExtension(String filename, String newExtension) {
		int lastDotIndex = filename.lastIndexOf('.');
		String filenameWithoutExtension;

		if (lastDotIndex >= 0) {
			filenameWithoutExtension = filename.substring(0, lastDotIndex);
		} else {
			// no dot in filename
			filenameWithoutExtension = filename;
		}

		return String.format("%s.%s", filenameWithoutExtension, newExtension);
	}
	
}
