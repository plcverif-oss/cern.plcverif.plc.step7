/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.gui.extension;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.component.IPvGuiPart;
import cern.plcverif.plc.step7.extension.Step7ExtensionActivator;
import cern.plcverif.verif.extensions.gui.IVerifGuiPartExtension;
import cern.plcverif.verif.extensions.gui.context.VerificationCaseContext;

public class Step7LfGuiExtension implements IVerifGuiPartExtension {
	@Override
	public IPvGuiPart<VerificationCaseContext> createPart(Composite composite, PvDataBinding parentBinding,
			VerificationCaseContext context, Optional<? extends SettingsElement> installationSettings) {
		Step7LfGui gui = new Step7LfGui(context);
//		Step7Activator.getInstance().getInjector(Step7Activator.CERN_PLCVERIF_PLC_STEP7_STEP7LANGUAGE)
//				.injectMembers(gui);
		Step7ExtensionActivator.getInjector().injectMembers(gui);
		gui.createPart(composite, parentBinding);
		return gui;
	}

}
