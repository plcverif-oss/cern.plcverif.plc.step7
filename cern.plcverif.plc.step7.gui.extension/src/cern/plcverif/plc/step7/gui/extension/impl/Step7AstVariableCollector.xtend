/******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 * 
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.gui.^extension.impl

import cern.plcverif.plc.step7.step7Language.ArrayDT
import cern.plcverif.plc.step7.step7Language.DataBlock
import cern.plcverif.plc.step7.step7Language.ElementaryDT
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit
import cern.plcverif.plc.step7.step7Language.FbOrUdtDT
import cern.plcverif.plc.step7.step7Language.FunctionBlock
import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.step7Language.StructDT
import cern.plcverif.plc.step7.step7Language.UserDefinedDataType
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine
import com.google.common.base.Preconditions
import java.util.Collection
import java.util.List
import java.util.Optional

import static cern.plcverif.plc.step7.util.Step7LanguageHelper.*
import cern.plcverif.plc.step7.step7Language.ProgramUnit
import cern.plcverif.plc.step7.util.DataTypeUtil
import cern.plcverif.plc.step7.cfa.impl.Step7TypeToExprType
import cern.plcverif.plc.step7.step7Language.ParameterDT
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.plc.step7.step7Language.VariableDeclarationDirection
import cern.plcverif.base.models.expr.utils.TypedAstVariable
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.util.SclSyntaxHelper

/**
 * Utility class to collect all STEP 7 AST variables in a collection of program files.
 */
final class Step7AstVariableCollector {
	private new() {	}
	
	/**
	 * Collect all (leaf and non-leaf) variables in the given STEP 7 program files.
	 * 
	 * If the entry block is given, the variables corresponding to that instance will be included as well.
	 *  
	 * TEMP variables are not included. For the array and struct variables non-leaf variable descriptors will be created too.
	 * 
	 * @param files Program files to analyse. Shall not be {@code null}.
	 * @param entryBlock Entry block. Shall not be {@code null}, can be {@link Optional#empty()}.
	 * @return Descriptors of the variables in the given program files.
	 */
	static def List<TypedAstVariable> collectAll(Collection<ProgramFile> files, Optional<ProgramUnit> entryBlock) {
		Preconditions.checkNotNull(files, "files");
		Preconditions.checkNotNull(entryBlock, "entryBlock");
		
		val List<TypedAstVariable> ret = newArrayList();
		if (entryBlock.isPresent) {
			collectEntryInstanceVariables(entryBlock.get, ret);
		}
		
		for (file : files) {
			collectAllVariables(file, ret);
		}
		
		return ret;
	}
	
	private static def void collectAllVariables(ProgramFile file, List<TypedAstVariable> accu) {
		collectAllGlobalVariables(file, accu);
		
		for (DataBlock db : file.programUnits.filter[it | isDataBlock(it)].filter(DataBlock)) {
			cern.plcverif.plc.step7.gui.^extension.impl.Step7AstVariableCollector.collectAllDbVariables(db, accu);
		}
	}
	
	private def static void collectEntryInstanceVariables(ProgramUnit entryBlock, List<TypedAstVariable> accu) {
		Preconditions.checkNotNull(entryBlock, "entryBlock");
		
		if (isFunctionBlock(entryBlock)) {
			createProgramUnitInstanceVariables(entryBlock as ExecutableProgramUnit, "instance", accu);
		} else if (isFunction(entryBlock)) {
			createProgramUnitInstanceVariables(entryBlock as ExecutableProgramUnit, entryBlock.name, accu);
			if (!DataTypeUtil.isVoid((entryBlock as Function).returnType)) {
				// non-void FC --> create RET_VAL variable too
				createVariables((entryBlock as Function).returnType, '''«entryBlock.name».«SclSyntaxHelper.RETVAL_VARIABLE_NAME»''', true, accu);
			}
		} else if (isOrganizationBlock(entryBlock)) {
			// Nothing to do.
		}else {
			throw new UnsupportedOperationException("Unexpected entry block type.");
		}
	}
	
		
	private def static void createProgramUnitInstanceVariables(ExecutableProgramUnit block, String prefix, List<TypedAstVariable> accu) {
		for (varBlock : block.declarationSection.variableDeclarations.filter[it | it.direction !== VariableDeclarationDirection.TEMP]) {
			for (line : varBlock.variables) {
				collectVariablesInDeclarationLine(line, prefix, true, accu);
			}
		}
	}
	
	
	private def static collectAllDbVariables(DataBlock db, List<TypedAstVariable> accu) {
		val dbStructure = db.structure;
		switch (dbStructure) {
			FbOrUdtDT, StructDT: createVariables(dbStructure, db.name, true, accu)
			default: throw new UnsupportedOperationException("Unexpected DB structure type: " + dbStructure)
		}
	}
	
	private def static collectAllGlobalVariables(ProgramFile file, List<TypedAstVariable> accu) {
		for (globalVarBlock : file.globalVariables) {
			for (line : globalVarBlock.variables) {
				collectVariablesInDeclarationLine(line, "", true, accu);
			}
		}
	}
	
	private def static void collectVariablesInDeclarationLine(VariableDeclarationLine vdl, String prefix, boolean topLevel, List<TypedAstVariable> accu) {
		for (v : vdl.variables) {
			if (topLevel && SclSyntaxHelper.isIgnoredGlobalVarName(v.name)) {
				// Nothing to do, this is an ignored variable, such as IX[0]
			} else {
				val newPrefix = if (prefix.isNullOrEmpty) v.name else '''«prefix».«v.name»''';
				createVariables(vdl.type, newPrefix, topLevel, accu);
			}
		}
	} 
	
	private def static dispatch void createVariables(ElementaryDT type, String prefix, boolean topLevel, List<TypedAstVariable> accu) {
		accu.add(new TypedAstVariable(prefix, type.type.literal, Step7TypeToExprType.s7typeToExprType(type), topLevel, true));
	}
	
	private def static dispatch void createVariables(ParameterDT type, String prefix, boolean topLevel, List<TypedAstVariable> accu) {
		accu.add(new TypedAstVariable(prefix, type.type.literal, CfaDeclarationSafeFactory.INSTANCE.createUnknownType, topLevel, true));
	}
	
	private def static dispatch void createVariables(StructDT type, String prefix, boolean topLevel, List<TypedAstVariable> accu) {
		val isDataBlockStructureDefinition = type.eContainer instanceof DataBlock;
		
		if (!isDataBlockStructureDefinition) {
			accu.add(new TypedAstVariable(prefix, "STRUCT", CfaDeclarationSafeFactory.INSTANCE.createUnknownType, topLevel, false));
		}
		
		for (member : type.members) {
			collectVariablesInDeclarationLine(member, prefix, isDataBlockStructureDefinition, accu);
		}
	}
	
	private def static dispatch void createVariables(ArrayDT type, String prefix, boolean topLevel, List<TypedAstVariable> accu) {
		accu.add(new TypedAstVariable(prefix, "ARRAY", CfaDeclarationSafeFactory.INSTANCE.createUnknownType, topLevel, false));
		
		for (multiIndex : DataTypeUtil.enumerateValidArrayIndices(type)) {
			createVariables(type.baseType, '''«prefix»[«FOR idxVal : multiIndex SEPARATOR ','»«idxVal»«ENDFOR»]''', false, accu);
		}
	}
	
	private def static dispatch void createVariables(FbOrUdtDT type, String prefix, boolean topLevel, List<TypedAstVariable> accu) {
		val referredUnit = type.type;
		switch (referredUnit) {
			FunctionBlock: createProgramUnitInstanceVariables(referredUnit, prefix, accu)
			UserDefinedDataType: createVariables(referredUnit.declaration, prefix, topLevel, accu)
			default: throw new UnsupportedOperationException()
		}
	}

}