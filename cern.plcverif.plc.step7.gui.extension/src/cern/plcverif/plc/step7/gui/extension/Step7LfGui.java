/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.gui.extension;

import static cern.plcverif.base.gui.layout.PvLayoutUtils.createLabeledRow;
import static cern.plcverif.plc.step7.util.Step7LanguageHelper.isExecutableProgramUnit;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import com.google.common.base.Preconditions;
import com.google.common.io.CharStreams;
import com.google.inject.Inject;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.gui.binding.IKeyLabelPair;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.swt.PvComboboxBinding;
import cern.plcverif.base.gui.binding.swt.PvComboboxDeferredBinding;
import cern.plcverif.base.gui.component.AbstractPvGuiPart;
import cern.plcverif.base.gui.layout.PvLayoutUtils;
import cern.plcverif.base.interfaces.exceptions.AstParsingException;
import cern.plcverif.base.models.expr.utils.TypedAstVariable;
import cern.plcverif.plc.step7.extension.Step7AstReference;
import cern.plcverif.plc.step7.extension.Step7Parser;
import cern.plcverif.plc.step7.extension.Step7ParserResult;
import cern.plcverif.plc.step7.extension.Step7Settings;
import cern.plcverif.plc.step7.gui.extension.impl.Step7AstVariableCollector;
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit;
import cern.plcverif.plc.step7.step7Language.NamedElement;
import cern.plcverif.plc.step7.step7Language.ProgramFile;
import cern.plcverif.plc.step7.step7Language.ProgramUnit;
import cern.plcverif.plc.step7.step7Language.VerificationAssertion;
import cern.plcverif.plc.step7.util.Step7LanguageHelper;
import cern.plcverif.plc.step7.util.TextualUtil;
import cern.plcverif.verif.extensions.gui.context.VerificationCaseContext;

public class Step7LfGui extends AbstractPvGuiPart<VerificationCaseContext> {
	@Inject
	private Step7Parser parser;

	private Map<String, String> previousContents = null;
	private Collection<ProgramFile> previousAst = null;

	private PvComboboxBinding bndEntryBlock = null;

	private Composite parentComposite = null;
	private Job runningUpdateJob = null;

	private Label lblErrorMessage = null;
	private Label lblErrorMessageHeader = null;

	public Step7LfGui(VerificationCaseContext context) {
		super(context);
		context.addContextUpdatedListener((String updatedId) -> {
			if (bndEntryBlock.isEnabled() && VerificationCaseContext.FILES_ID.equals(updatedId)) {
				// file list changed --> update context and this GUI part
				// (If the bndEntryBlock is not enabled, another frontend is selected.)
				restartParsingInBackground();
			}
		});
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		
		if (enabled) {
			restartParsingInBackground();
		} else {
			// Invalidate previous results
			previousContents = null;
			previousAst = null;
		}
	}

	@Override
	public void createPart(Composite composite, PvDataBinding bindingParent) {
		PvLayoutUtils.checkExpectedLayout(composite);
		Preconditions.checkState(parentComposite == null,
				"Step7LfGui does not support multiple executions of createPart.");
		this.parentComposite = composite;

		// 'Entry' GUI objects
		Label lblEntry = new Label(composite, SWT.NONE);
		lblEntry.setText("Entry block:");
		Combo cbEntry = new Combo(composite, SWT.READ_ONLY);
		createLabeledRow(composite, lblEntry, cbEntry);

		// 'Entry' data binding
		bndEntryBlock = new PvComboboxDeferredBinding(cbEntry, Step7Settings.ENTRY_BLOCK, bindingParent);
		registerBindingElement(bndEntryBlock);
		bndEntryBlock.addChangeListener(it -> {
			//// restartParsingInBackground();
			if (this.previousAst != null) {
				// If the AST is not known yet, there must be a running job
				// already that will fill the variables
				fillVariables(this.previousAst);
			}
			getContext().setEntryBlock(cbEntry); // this line was missing
		});

		// 'Error message' line
		lblErrorMessageHeader = new Label(composite, SWT.NONE);
		lblErrorMessageHeader.setVisible(false);

		lblErrorMessage = new Label(composite, SWT.WRAP);
		lblErrorMessage.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
		PvLayoutUtils.createLabeledRow(composite, lblErrorMessageHeader, lblErrorMessage);
		PvLayoutUtils.applyMediumWidthHint(lblErrorMessage);
		setErrorMessage(null);
	}

	private synchronized void restartParsingInBackground() {
		if (runningUpdateJob != null) {
			runningUpdateJob.cancel();
			runningUpdateJob.setName(runningUpdateJob.getName() + " (outdated)");
		}

		List<IFile> sourceFiles = getContext().getSelectedSourceFiles();
		runningUpdateJob = Job.create("Parsing STEP 7 PLC program source files", monitor -> {
			parseFilesAndUpdateContext(sourceFiles, monitor);
		});
		runningUpdateJob.schedule();
	}

	private void parseFilesAndUpdateContext(List<IFile> sourceFiles, IProgressMonitor monitor) {
		monitor.subTask("Loading program files");
		Map<String, String> filesWithContent;
		try {
			filesWithContent = loadFileContents(sourceFiles);
		} catch (FileNotFoundException ex) {
			Display.getDefault().asyncExec(() -> {
				MessageDialog.openError(Display.getDefault().getActiveShell(),
						"Error while parsing the PLC programs",
						String.format(
								"%s%nFix the verification case file manually or replace the missing file and try to open the verification case again.",
								ex.getMessage()));
			});
			return;
		} catch (Exception ex) {

			Display.getDefault().asyncExec(() -> {
				MessageDialog.openError(Display.getDefault().getActiveShell(), "Error while parsing the PLC programs",
						ex.getMessage());
			});
			return;
		}

		if (previousContents != null && previousContents.equals(filesWithContent) && previousAst != null) {
			// no change in contents -- no need to parse the files again

			// Entry block may have changed, refresh the variable list!
			this.parentComposite.getDisplay().syncExec(() -> {
				fillVariables(this.previousAst);
			});
			return;
		}

		this.previousContents = filesWithContent;
		this.previousAst = null; // to avoid inconsistency

		if (filesWithContent.isEmpty()) {
			// System.out.println("No source file has been selected.");
			// No file selected -- nothing to parse
		} else {
			monitor.worked(15);
			monitor.subTask("Looking for potential entry blocks");
			this.parentComposite.getDisplay().syncExec(() -> {
				// Fill the entry block list quickly, even if it is not fully
				// accurate.
				quickFillEntryBlocks(filesWithContent);
				quickFillAssertions(filesWithContent);
			});

			if (monitor.isCanceled()) {
				return;
			}

			// Parse the file
			monitor.subTask("Parsing source files");
			parser.setFiles(filesWithContent);
			Step7ParserResult parserResult = parser.parseCode();
			if (monitor.isCanceled()) {
				return;
			}

			Step7AstReference ast;
			try {
				ast = parserResult.getAstWithoutValidation();
				this.previousAst = ast.getAst();

				setErrorMessageOnUnresolvedDependency(previousAst, parserResult);
			} catch (AstParsingException e) {
				Display.getDefault().asyncExec(() -> {
					setErrorMessage(e.getMessage());
				});
				monitor.done();
				return;
			}

			monitor.worked(60);
			Display.getDefault().syncExec(() -> {
				monitor.subTask("Determining assertions in source files");
				fillAssertions(ast.getAst());

				monitor.subTask("Determining entry blocks in source files");
				fillEntryBlocks(ast.getAst());

				monitor.subTask("Determining variables in source files");
				fillVariables(ast.getAst());
			});
			monitor.done();
		}
	}

	private void setErrorMessageOnUnresolvedDependency(Collection<ProgramFile> loadedFiles,
			Step7ParserResult parserResult) {
		for (ProgramFile file : loadedFiles) {
			for (EObject e : EmfHelper.toIterable(file.eAllContents())) {
				for (EObject ref : e.eCrossReferences()) {
					if (ref instanceof NamedElement && ref.eIsProxy()) {
						Optional<String> filename = parserResult.getFilenameForUri(file.eResource().getURI());
						setErrorMessage(String.format("Unresolved dependency in %s: %s.", filename.orElse("unknown"),
								Step7LanguageHelper.textInSource(e)));
						return;
					}
				}
			}
		}

		setErrorMessage(null);
	}

	private void setErrorMessage(String message) {
		lblErrorMessage.getDisplay().syncExec(() -> {
			if (message == null || message.isEmpty()) {
				lblErrorMessage.setVisible(false);
				lblErrorMessage.setText("");
				PvLayoutUtils.setExcludeFromLayout(lblErrorMessage, true);
				PvLayoutUtils.setExcludeFromLayout(lblErrorMessageHeader, true);
			} else {
				lblErrorMessage.setVisible(true);
				lblErrorMessage.setText(message);
				PvLayoutUtils.setExcludeFromLayout(lblErrorMessage, false);
				PvLayoutUtils.setExcludeFromLayout(lblErrorMessageHeader, false);
			}
			// lblErrorMessage.getParent().pack();
			lblErrorMessage.getParent().requestLayout();
		});
	}

	/**
	 * Fills the entry block list using quick heuristic (without parsing the
	 * source files). This method assumes that it is called from the GUI thread.
	 *
	 * @param filesWithContent
	 *            Content of the source files
	 */
	private void quickFillEntryBlocks(Map<String, String> filesWithContent) {
		List<String> entryBlockList = new ArrayList<>();
		filesWithContent.values().stream()
				.forEach(it -> entryBlockList.addAll(TextualUtil.collectExecutableUnitNames(it)));
		bndEntryBlock.setPermittedItems(
				entryBlockList.stream().map(it -> new IKeyLabelPair.Impl(it, it)).collect(Collectors.toList()));
	}

	/**
	 * Fills the entry block list based on the parsed ASTs. This method assumes
	 * that it is called from the GUI thread.
	 *
	 * @param ast
	 *            Parsed source files
	 */
	private void fillEntryBlocks(Collection<ProgramFile> ast) {
		if (bndEntryBlock == null) {
			System.err.println("bndEntryBlock not initialised yet.");
			return;
		}

		List<String> entryBlockList = new ArrayList<>();
		for (ProgramFile programFile : ast) {
			programFile.getProgramUnits().stream().filter(it -> isExecutableProgramUnit(it))
					.filter(it -> !(it instanceof ExecutableProgramUnit) || !((ExecutableProgramUnit)it ).isBuiltIn()) // not built-in
					.forEach(it -> entryBlockList.add(it.getName()));
		}

		bndEntryBlock.setPermittedItems(
				entryBlockList.stream().map(it -> new IKeyLabelPair.Impl(it, it)).collect(Collectors.toList()));
	}

	private void fillVariables(Collection<ProgramFile> ast) {
		// Try to find instance variables (implicit instantiation of an FB)
		Optional<ProgramUnit> entryBlock = tryFindEntryBlock(ast);

		List<TypedAstVariable> elementaryVariables = Step7AstVariableCollector.collectAll(ast, entryBlock);
		getContext().setVariables(elementaryVariables);
	}

	private Optional<ProgramUnit> tryFindEntryBlock(Collection<ProgramFile> ast) {
		Optional<String> entryBlockName = bndEntryBlock.getSelectedItem();
		if (entryBlockName.isPresent()) {
			for (ProgramFile file : ast) {
				for (ProgramUnit unit : file.getProgramUnits()) {
					if (unit.getName().equalsIgnoreCase(entryBlockName.get())) {
						// found!
						return Optional.of(unit);
					}
				}
			}
		}

		return Optional.empty();
	}

	/**
	 * Fills the assertion list using quick heuristic (without parsing the
	 * source files). This method assumes that it is called from the GUI thread.
	 *
	 * @param filesWithContent
	 *            Content of the source files
	 */
	private void quickFillAssertions(Map<String, String> filesWithContent) {
		SortedSet<String> assertions = new TreeSet<>();
		filesWithContent.values().stream().forEach(it -> assertions.addAll(TextualUtil.collectAssertionTags(it)));

		getContext().setAssertions(new ArrayList<>(assertions));
	}

	private void fillAssertions(Collection<ProgramFile> ast) {
		// Fill list of assertions
		SortedSet<String> assertionTags = new TreeSet<>();
		for (ProgramFile programFile : ast) {
			for (VerificationAssertion assertionStmt : EmfHelper.getAllContentsOfType(programFile,
					VerificationAssertion.class, false)) {
				if (assertionStmt.getTag() != null) {
					assertionTags.add(assertionStmt.getTag());
				}
			}
		}
		getContext().setAssertions(new ArrayList<>(assertionTags));
	}

	private static Map<String, String> loadFileContents(Collection<IFile> sourceFiles) throws FileNotFoundException {
		Map<String, String> ret = new HashMap<>();

		for (IFile file : sourceFiles) {
			loadFileContents(file, ret);
		}

		return ret;
	}

	/**
	 * Loads the content of a single file into the given accumulator.
	 *
	 * @param file
	 *            File to load. It may contain wildcards, then all of the
	 *            matching files are loaded.
	 * @param accu
	 *            Accumulator for results
	 */
	private static void loadFileContents(IFile file, Map<String, String> accu) throws FileNotFoundException {
		String fileName = file.getProjectRelativePath().toString();
		String content = "";

		try {
			if (fileName.contains("*")) {
				// handle wildcards -- TODO this is a basic implementation,
				// assuming wildcards only in the last segment
				for (IResource fileCandidate : file.getParent().members()) {
					if (fileCandidate instanceof IFile) {
						String candidateName = ((IFile) fileCandidate).getName();
						if (candidateName.matches(Pattern.quote(fileName).replaceAll("\\*", "\\\\E.*\\\\Q"))) {
							loadFileContents((IFile) fileCandidate, accu);
						}
					}
				}
			} else {
				if (!file.exists()) {
					throw new FileNotFoundException(String.format("The file '%s' does not exist.", fileName));
				}

				try (InputStreamReader reader = new InputStreamReader(file.getContents(true), file.getCharset())) {
					content = String.join(System.lineSeparator(), CharStreams.readLines(reader));
					accu.put(fileName, content);
				}
			}
		} catch (FileNotFoundException e) {
			throw e;
		}catch (IOException | CoreException e) {
			throw new PlcverifPlatformException(String.format("Unable to load a selected source file (%s).", fileName),
					e); // NOPMD
		}
	}
}