/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/

package cern.plcverif.plc.step7.converter

import org.eclipse.xtext.conversion.impl.AbstractDeclarativeValueConverterService
import org.eclipse.xtext.conversion.ValueConverter
import org.eclipse.xtext.conversion.IValueConverter

class Step7ValueConverterService extends AbstractDeclarativeValueConverterService {
	
	@ValueConverter(rule = "SYMBOL")
	def IValueConverter<String> getSYMBOLConverter() {
    	return new SymbolToStringConverter().SYMBOL()
	}
	
	@ValueConverter(rule = "IdOrSymbol")
	def IValueConverter<String> getIdOrSymbolConverter() {
    	return new SymbolToStringConverter().SYMBOL()
	}
	
}
