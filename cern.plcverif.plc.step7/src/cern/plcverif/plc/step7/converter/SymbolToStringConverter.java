/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/

package cern.plcverif.plc.step7.converter;

import org.eclipse.xtext.common.services.DefaultTerminalConverters;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.nodemodel.INode;

public class SymbolToStringConverter extends DefaultTerminalConverters {
	@ValueConverter(rule = "SYMBOL")
	public IValueConverter<String> SYMBOL() {
		return new IValueConverter<String>() {
			@Override
			public String toString(String value) throws ValueConverterException {
				return value;
			}

			@Override
			public String toValue(String string, INode node) throws ValueConverterException {
				if (string == null) {
					return "";
				}
				
				String ret = string.trim();

				// remove leading #
				if (ret.trim().startsWith("#")) {
					ret = ret.substring(1);
				}

				// remove quotation marks if there is no space inside
				if (!ret.contains(" ") && ret.startsWith("\"") && ret.endsWith("\"")) {
					String withoutQuotes = ret.substring(1, ret.length() - 1);
					return withoutQuotes;
				}
				
				return ret;
			}
		};
	}
}
