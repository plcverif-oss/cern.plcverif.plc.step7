/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/

package cern.plcverif.plc.step7

import cern.plcverif.plc.step7.converter.Step7ValueConverterService
import org.eclipse.xtext.conversion.IValueConverterService
import cern.plcverif.plc.step7.naming.Step7QualifiedNameProvider

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class Step7LanguageRuntimeModule extends AbstractStep7LanguageRuntimeModule {
	override Class<? extends IValueConverterService> bindIValueConverterService() {
		return Step7ValueConverterService;
	}

//	override bindIGlobalScopeProvider() {
//		return Step7GlobalScopeProvider;
//	}	
	
//	override bindIContainer$Manager() {
//		return Step7ContainerManager;
//	}
	
	override bindIQualifiedNameProvider() {
		return Step7QualifiedNameProvider;
	}
}
