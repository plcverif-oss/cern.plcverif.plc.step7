/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.typecomputer

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.plc.step7.datatypes.S7IntegerType
import cern.plcverif.plc.step7.datatypes.S7RealType
import cern.plcverif.plc.step7.step7Language.AbstractExpression
import cern.plcverif.plc.step7.step7Language.AdditionOperator
import cern.plcverif.plc.step7.step7Language.AdditiveExpression
import cern.plcverif.plc.step7.step7Language.AndExpression
import cern.plcverif.plc.step7.step7Language.ArrayDT
import cern.plcverif.plc.step7.step7Language.ArrayDimensionRange
import cern.plcverif.plc.step7.step7Language.ArrayInitialization
import cern.plcverif.plc.step7.step7Language.ArrayRef
import cern.plcverif.plc.step7.step7Language.BoolConstant
import cern.plcverif.plc.step7.step7Language.CallParameter
import cern.plcverif.plc.step7.step7Language.CallParameterList
import cern.plcverif.plc.step7.step7Language.CharConstant
import cern.plcverif.plc.step7.step7Language.ComparisonExpression
import cern.plcverif.plc.step7.step7Language.ConstantInitializationElement
import cern.plcverif.plc.step7.step7Language.DataBlockInitialAssignment
import cern.plcverif.plc.step7.step7Language.DataType
import cern.plcverif.plc.step7.step7Language.DateAndTimeConstant
import cern.plcverif.plc.step7.step7Language.DateConstant
import cern.plcverif.plc.step7.step7Language.DbFieldAddressRef
import cern.plcverif.plc.step7.step7Language.DbFieldIndexedRef
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.DirectRef
import cern.plcverif.plc.step7.step7Language.ElementaryDT
import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum
import cern.plcverif.plc.step7.step7Language.EnCallParameterItem
import cern.plcverif.plc.step7.step7Language.EnoFlag
import cern.plcverif.plc.step7.step7Language.EqualityExpression
import cern.plcverif.plc.step7.step7Language.Expression
import cern.plcverif.plc.step7.step7Language.FlagRef
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.FunctionBlock
import cern.plcverif.plc.step7.step7Language.ImpliesExpression
import cern.plcverif.plc.step7.step7Language.InitializationRepetitionList
import cern.plcverif.plc.step7.step7Language.IntConstant
import cern.plcverif.plc.step7.step7Language.MultiplicativeExpression
import cern.plcverif.plc.step7.step7Language.NamedCallParameterItem
import cern.plcverif.plc.step7.step7Language.NamedConstantDeclaration
import cern.plcverif.plc.step7.step7Language.NamedConstantRef
import cern.plcverif.plc.step7.step7Language.NamedOrUnnamedConstantRef
import cern.plcverif.plc.step7.step7Language.NegatedUnnamedConstantRef
import cern.plcverif.plc.step7.step7Language.NilValue
import cern.plcverif.plc.step7.step7Language.OkValRef
import cern.plcverif.plc.step7.step7Language.OrExpression
import cern.plcverif.plc.step7.step7Language.OrganizationBlock
import cern.plcverif.plc.step7.step7Language.ParameterDT
import cern.plcverif.plc.step7.step7Language.ParameterTypeEnum
import cern.plcverif.plc.step7.step7Language.PowerExpression
import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.step7Language.QualifiedRef
import cern.plcverif.plc.step7.step7Language.RangeCaseElementValue
import cern.plcverif.plc.step7.step7Language.RealConstant
import cern.plcverif.plc.step7.step7Language.RetValCallParameterItem
import cern.plcverif.plc.step7.step7Language.RetValRef
import cern.plcverif.plc.step7.step7Language.SclAssignmentStatement
import cern.plcverif.plc.step7.step7Language.SclCaseStatement
import cern.plcverif.plc.step7.step7Language.SclForStatement
import cern.plcverif.plc.step7.step7Language.SclIfStatement
import cern.plcverif.plc.step7.step7Language.SclRepeatStatement
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall
import cern.plcverif.plc.step7.step7Language.SclWhileStatement
import cern.plcverif.plc.step7.step7Language.SingleCallParameter
import cern.plcverif.plc.step7.step7Language.SingleCaseElementValue
import cern.plcverif.plc.step7.step7Language.Step7LanguageFactory
import cern.plcverif.plc.step7.step7Language.StlAccuDecrementStatement
import cern.plcverif.plc.step7.step7Language.StlAccuIncrementStatement
import cern.plcverif.plc.step7.step7Language.StlAddIntConstStatement
import cern.plcverif.plc.step7.step7Language.StlAssignStatement
import cern.plcverif.plc.step7.step7Language.StlBitLogicStatement
import cern.plcverif.plc.step7.step7Language.StlBldStatement
import cern.plcverif.plc.step7.step7Language.StlFnStatement
import cern.plcverif.plc.step7.step7Language.StlFpStatement
import cern.plcverif.plc.step7.step7Language.StlLoadStatement
import cern.plcverif.plc.step7.step7Language.StlNopStatement
import cern.plcverif.plc.step7.step7Language.StlResetStatement
import cern.plcverif.plc.step7.step7Language.StlSetStatement
import cern.plcverif.plc.step7.step7Language.StlShiftRotateStatement
import cern.plcverif.plc.step7.step7Language.StlTransferStatement
import cern.plcverif.plc.step7.step7Language.StlWordLogicStatement
import cern.plcverif.plc.step7.step7Language.StringConstant
import cern.plcverif.plc.step7.step7Language.StringDT
import cern.plcverif.plc.step7.step7Language.StwBitRef
import cern.plcverif.plc.step7.step7Language.SubroutineCall
import cern.plcverif.plc.step7.step7Language.TimeConstant
import cern.plcverif.plc.step7.step7Language.TimeOfDayConstant
import cern.plcverif.plc.step7.step7Language.UnaryExpression
import cern.plcverif.plc.step7.step7Language.UnnamedConstant
import cern.plcverif.plc.step7.step7Language.UnnamedConstantRef
import cern.plcverif.plc.step7.step7Language.Variable
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine
import cern.plcverif.plc.step7.step7Language.VariableInitialization
import cern.plcverif.plc.step7.step7Language.VerificationAssertion
import cern.plcverif.plc.step7.step7Language.XorExpression
import cern.plcverif.plc.step7.step7Language.impl.NamedElementImpl
import cern.plcverif.plc.step7.util.DataTypeUtil
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import cern.plcverif.plc.step7.util.StlSemanticsHelper
import cern.plcverif.plc.step7.util.TypeEquals
import com.google.common.base.Preconditions
import java.util.HashMap
import java.util.Optional
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.eclipse.emf.ecore.util.EcoreUtil

import static cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum.*
import static com.google.common.base.Preconditions.*

import static extension cern.plcverif.plc.step7.util.DataTypeUtil.*
import static extension cern.plcverif.plc.step7.util.Step7LanguageHelper.*
import cern.plcverif.plc.step7.step7Language.BitMemoryAddress
import cern.plcverif.plc.step7.step7Language.LongMemoryAddress
import cern.plcverif.plc.step7.step7Language.MemoryAddress
import cern.plcverif.plc.step7.datatypes.S7Integer
import cern.plcverif.plc.step7.step7Language.DirectBitAccessRef
import cern.plcverif.plc.step7.step7Language.StructDT
import cern.plcverif.plc.step7.step7Language.DataBlock

class Step7TypeComputer {
	static boolean staticInitializationDone = false; // workaround for missing static constructor in Xtend
	static final HashMap<ElementaryTypeEnum, DataType> elementaryDT = newHashMap();
	static DataType UNKNOWN_NOMINAL_TYPE;
	static DataType ANY_NUM;
	static DataType ANY_BIT;
	static DataType BOOL_DT;
	static DataType POINTER_DT;

	static final Logger log = LogManager.getLogger(Step7TypeComputer);

	IElementaryTypeConversion typeConversion = new S7300ElementaryTypeConversion();
	Iterable<ProgramFile> files;

	val HashMap<AbstractExpression, DataType> nominalDataType = newHashMap();
	val HashMap<AbstractExpression, DataType> expectedDataType = newHashMap();
	val HashMap<AbstractExpression, String> additionalInvalidityMsg = newHashMap();

	private static def initializeDataTypes() {
		if (staticInitializationDone) {
			return;
		}

		// static initializer
		for (dt : ElementaryTypeEnum.enumConstants) {
			val newItem = Step7LanguageFactory.eINSTANCE.createElementaryDT();
			newItem.type = dt;
			elementaryDT.put(dt, newItem);
		}

		BOOL_DT = elementaryDT.get(BOOL);

		UNKNOWN_NOMINAL_TYPE = Step7LanguageFactory.eINSTANCE.createParameterDT();
		(UNKNOWN_NOMINAL_TYPE as ParameterDT).type = ParameterTypeEnum.ANY;

		ANY_NUM = Step7LanguageFactory.eINSTANCE.createParameterDT();
		(ANY_NUM as ParameterDT).type = ParameterTypeEnum.ANY_NUM;

		ANY_BIT = Step7LanguageFactory.eINSTANCE.createParameterDT();
		(ANY_BIT as ParameterDT).type = ParameterTypeEnum.ANY_BIT;

		POINTER_DT = Step7LanguageFactory.eINSTANCE.createParameterDT();
		(POINTER_DT as ParameterDT).type = ParameterTypeEnum.POINTER;

		staticInitializationDone = true;
	}

	private new(Iterable<ProgramFile> files) {
		this.files = files;
		initializeDataTypes();
	}

	static def compute(Iterable<ProgramFile> files) {
		val instance = new Step7TypeComputer(files);
		instance.compute();
		return instance;
	}

	def getType(AbstractExpression expr) {
		val type = tryGetType(expr);
		if (type.isPresent) {
			return type.get;
		} else {
			throw new RuntimeException("Unknown expression: " + expr);
		}
//		val nominalType = nominalDataType.get(expr);
//		val expectedType = expectedDataType.get(expr);
//
//		if (nominalType === null && expectedType === null) {
//			throw new RuntimeException("Unknown expression: " + expr)
//		}
//
//		if (nominalType === UNKNOWN_NOMINAL_TYPE) {
//			return expectedType;
//		}
//
//		return nominalType;
	}

	def Optional<DataType> tryGetType(AbstractExpression expr) {
		val nominalType = nominalDataType.get(expr);
		val expectedType = expectedDataType.get(expr);

		if (nominalType === null && expectedType === null) {
			return Optional.empty();
		}

		if (nominalType === null && expectedType !== null) {
			return Optional.of(expectedType);
		}

		if (nominalType === UNKNOWN_NOMINAL_TYPE) {
			return Optional.ofNullable(expectedType);
		}

		if (expectedType !== UNKNOWN_NOMINAL_TYPE && (nominalType.isAnyNum || nominalType.isAnyBit)) {
			return Optional.ofNullable(expectedType);
		}
		
		return Optional.of(nominalType);
	}

	def Optional<DataType> tryGetExpectedType(AbstractExpression expr) {
		val nominalType = nominalDataType.get(expr);
		val expectedType = expectedDataType.get(expr);

		if ((expectedType == ANY_BIT || expectedType == ANY_NUM) && nominalType !== null && nominalType !== UNKNOWN_NOMINAL_TYPE) {
			return Optional.of(nominalType);
		}

		if (expectedType !== null && expectedType !== UNKNOWN_NOMINAL_TYPE) {
			return Optional.of(expectedType);
		}

		return Optional.ofNullable(nominalType);
	}

	def getTypeDescriptor(AbstractExpression expr) {
		val computedNominal = nominalDataType.get(expr);
		val nominal = if (computedNominal === UNKNOWN_NOMINAL_TYPE) null else computedNominal;

		if (additionalInvalidityMsg.containsKey(expr)) {
			return new Step7TypeDescription(expr, nominal, expectedDataType.get(expr), typeConversion,
				additionalInvalidityMsg.get(expr));
		} else {
			return new Step7TypeDescription(expr, nominal, expectedDataType.get(expr), typeConversion);
		}
	}

	/**
	 * Computes the types for each top-level expression's expression tree.
	 */
	private def void compute() {
		for (file : files) {
			EcoreUtil.resolveAll(file);
			for (e : file.eAllContents.toIterable) {
				switch (e) {
					// TODO refactor this ugly big switch-case
					SclAssignmentStatement: {
						pushDownExpected(e.leftValue, e.leftValue.computeNominalType)
						computeTypeFor(e.rightValue, e.leftValue.computeNominalType)
					}
					DataBlockInitialAssignment: {
						pushDownExpected(e.leftValue, e.leftValue.computeNominalType)
						computeTypeFor(e.constant, e.leftValue.computeNominalType)
					}
					SclIfStatement: {
						computeTypeFor(e.condition, BOOL_DT)
						for (elsifBranch : e.elsifBranches) {
							computeTypeFor(elsifBranch.condition, BOOL_DT)
						}
					}
					SclWhileStatement:
						computeTypeFor(e.entryCondition, BOOL_DT)
					SclRepeatStatement:
						computeTypeFor(e.exitCondition, BOOL_DT)
					SubroutineCall:
						computeTypeForCall(e)
					VariableDeclarationLine:
						computeTypeForVarDeclLine(e)
					StringDT: {
						if (e.dimension !== null) {
							computeTypeFor(e.dimension, elementaryDT.get(INT))
						}
					}
					SclCaseStatement: {
						computeTypesForCaseStatement(e);
					}
					ArrayDimensionRange: {
						computeTypeFor(e.from, elementaryDT.get(INT))
						computeTypeFor(e.to, elementaryDT.get(INT))
					}
					DbFieldIndexedRef: {
						computeTypeFor(e.expr, elementaryDT.get(INT))
					}
					SclForStatement: {
						val loopVar = e.initialStatement.leftValue;
						val loopVarType = loopVar.computeNominalType;
						computeTypeFor(e.finalValue, loopVarType);
						if (e.by) {
							computeTypeFor(e.increment, loopVarType);
						}
					}
					VerificationAssertion: { // both SCL and STL
						computeTypeFor(e.expression, BOOL_DT);
					}
					// == STL ==
					// STL instructions with Boolean argument
					StlBitLogicStatement,
					StlResetStatement,
					StlSetStatement,
					StlFpStatement,
					StlFnStatement,
					StlAssignStatement: {
						computeTypeFor(e.arg, BOOL_DT);
					}
					// STL instructions with numeric argument
					StlLoadStatement: {
						// TODO_LOWPRI Implicit conversions may be needed here
						if (e.arg.computeNominalType.isBooleanElementaryType){
							computeTypeFor(e.arg, BOOL_DT);
						}	
						else if(e.arg.computeNominalType.isTime){
							computeTypeFor(e.arg, elementaryDT.get(TIME));
						}
						else if(e.arg.computeNominalType.isNumeric){
							computeTypeFor(e.arg, ANY_NUM);
						}
						// TODO: Tackle structs. Check WORDs
						//else if(e.arg.computeNominalType.structDataType){
						//	computeTypeFor(e.arg, elementaryDT.get(StructDT));
						//}
						else {
							computeTypeFor(e.arg, ANY_BIT);	
						}
					}
					StlTransferStatement: {
						// TODO_LOWPRI Implicit conversions may be needed here
						if (e.arg.computeNominalType.isBooleanElementaryType){
							computeTypeFor(e.arg, BOOL_DT);
						}	
						else if(e.arg.computeNominalType.isTime){
							computeTypeFor(e.arg, elementaryDT.get(TIME));
						}
						else if(e.arg.computeNominalType.isNumeric){
							computeTypeFor(e.arg, ANY_NUM);
						}
						//else if(e.arg.computeNominalType.structDataType){
						//	computeTypeFor(e.arg, elementaryDT.get(StructDT));
						//}
						else {
							computeTypeFor(e.arg, ANY_BIT);	
						}
					}					
					// STL instructions with integer argument
					StlBldStatement,
					StlNopStatement: {
						computeTypeFor(e.arg, elementaryDT.get(INT));
					}
					StlShiftRotateStatement: {
						computeTypeFor(e.arg, elementaryDT.get(INT));
					}
					StlWordLogicStatement: {
						computeTypeFor(e.arg, elementaryDT.get(StlSemanticsHelper.getArgumentType(e.mnemonic)));
					}
					StlAddIntConstStatement: {
						computeTypeFor(e.arg, elementaryDT.get(StlSemanticsHelper.ACCU_TYPE));
					}
					StlAccuIncrementStatement,
					StlAccuDecrementStatement: {
						computeTypeFor(e.arg, elementaryDT.get(BYTE));
					}
				}
			}
		}
	}

	private def computeTypesForCaseStatement(SclCaseStatement e) {
		// nominal type for selection
		var selectionType = EcoreUtil.copy(e.selection.computeNominalType());

		// nominal type for cases
		for (it : e.elements) {
			for (caseElementValue : it.eAllContents.toIterable) {
				switch (caseElementValue) {
					SingleCaseElementValue: {
						if (caseElementValue.value instanceof UnnamedConstantRef) {
							// don't type the named constant declarations as they may have different expected types 
							// (but their namedrefs will be typed in expressions)
							val type = caseElementValue.value.unwrapRefExpression(this).computeNominalType;
							selectionType = greaterType(selectionType, type);
						}
					}
					RangeCaseElementValue: {
						if (caseElementValue.lowerBound instanceof UnnamedConstantRef) {
							val type = caseElementValue.lowerBound.unwrapRefExpression(this).computeNominalType;
							selectionType = greaterType(selectionType, type);
						}
						if (caseElementValue.upperBound instanceof UnnamedConstantRef) {
							val type = caseElementValue.upperBound.unwrapRefExpression(this).computeNominalType;
							selectionType = greaterType(selectionType, type);
						}
					}
				}
			}
		}

		if (selectionType.isAnyNum) {
			// if there is no explicit typing, assume INT
			// TODO: this results in incorrect solution in case of a constant which is not explicitly typed, but cannot fit into INT
			selectionType = elementaryDT.get(INT);
		}

		// push down expected type
		pushDownExpected(e.selection, selectionType);
		for (it : e.elements) {
			for (caseElementValue : it.eAllContents.toIterable) {
				switch (caseElementValue) {
					SingleCaseElementValue: {
						if (caseElementValue.value instanceof UnnamedConstantRef) {
							pushDownExpected(caseElementValue.value, selectionType);
						}
					}
					RangeCaseElementValue: {
						if (caseElementValue.lowerBound instanceof UnnamedConstantRef) {
							pushDownExpected(caseElementValue.lowerBound, selectionType);
						}
						if (caseElementValue.upperBound instanceof UnnamedConstantRef) {
							pushDownExpected(caseElementValue.upperBound, selectionType);
						}
					}
				}
			}
		}
	}

	private def void computeTypeFor(AbstractExpression e, DataType expected) {
		// log.trace("Type {} is expected for expression {}.", expected, e);

		// First, infer the nominal type bottom-up, from the typed constants and variables.
		// (Instead of the nominal type it would be more precise to use a set of feasible types,
		// as e.g. 1 can be BOOL, INT or WORD depending on the context, however its computation would
		// be much more heavy.
		// NOTE consider this modification for TiaPortal if needed.)
		val nominal = e.computeNominalType;

		// Then, do the narrowing step: based on the expected type narrow the nominal type (top-down).
		if (!nominal.isAny && expected.isAny) {
			pushDownExpected(e, nominal);
		} else {
			pushDownExpected(e, expected);
		}
	}

	private def computeTypeForVarDeclLine(VariableDeclarationLine line) {
		if (line.isInitialized) {
			checkNotNull(line.initialization);
			val dt = line.type;
			
			if (dt instanceof ArrayDT) {
				computeTypeForVarInitialization(dt.baseType, line.initialization);
			} else {
				computeTypeForVarInitialization(dt, line.initialization);
			}
		}
		if (line.isBounded) {
			checkNotNull(line.lowerBound);
			checkNotNull(line.upperBound);
			
			val dt = line.type;
			if (dt instanceof ArrayDT) {
				computeTypeForVarInitialization(dt.baseType, line.lowerBound);
				computeTypeForVarInitialization(dt.baseType, line.upperBound);
			} else {
				computeTypeForVarInitialization(dt, line.lowerBound);
				computeTypeForVarInitialization(dt, line.upperBound);
			}
		}
	}

	private def void computeTypeForCall(SubroutineCall call) {
		if (call instanceof SclSubroutineCall) {
			// Only SCL subroutine calls are expressions, not STL calls 
			call.computeNominalType;
		}
		computeTypeForCallParameter(call, call.callParameter);
	}

	private def dispatch void computeTypeForCallParameter(SubroutineCall call, CallParameter callParameter) {
	}

	private def dispatch void computeTypeForCallParameter(SubroutineCall call, Void callParameter) {
		// parameterless call, nothing to do
	}

	private def dispatch void computeTypeForCallParameter(SubroutineCall call, SingleCallParameter callParameter) {
		if (call.calledUnit.class.equals(NamedElementImpl)) {
			// incomplete AST (most probably there are errors in the parsed source code
			return;
		}

		// checkArgument(call.calledUnit instanceof Function, "[Assert] Only FCs can be called with unnamed parameter.");
		if (!(call.calledUnit instanceof Function)) {
			// Only FCs can be called with unnamed parameter.
			return;
		}

		// single input
		callParameter.parameter.computeNominalType();
		val calledFunction = call.calledUnit as Function;
		val onlyInputVariable = calledFunction.tryGetOnlyInputVariable;
		if (onlyInputVariable.present) {
			computeTypeFor(callParameter.parameter, onlyInputVariable.get.dataType);
		} else if (!calledFunction.isBuiltIn) {
			// Invalid call, cannot proceed with typing.
		}
	}

	private def dispatch void computeTypeForCallParameter(SubroutineCall call, CallParameterList callParameters) {
		for (param : callParameters.parameters) {
			switch (param) {
				NamedCallParameterItem: {
					if (param.parameter.eContainer === null) {
						log.debug("Parameter has no container, therefore the type computation failed.");
					} else {
						computeTypeFor(param.value, param.parameter.dataType)
					}
				}
				EnCallParameterItem:
					computeTypeFor(param.value, BOOL_DT)
				RetValCallParameterItem: {
					if (call.isFcCall) {
						val calledFc = call.calledProgramUnit as Function;
						computeTypeFor(param.value, calledFc.returnType);
					} else {
						log.
							debug('''It is not possible to use RET_VAL in a call that is not a function call. Therefore the type computation failed.''');
					}
				}
				default:
					throw new IllegalStateException("Unknown parameter item type: " + param)
			}
//			param.value.computeNominalType();
//			pushDownExpected(param.value, Step7LanguageHelper.getDataType(param.parameter));
		}
	}

	private def DataType computeNominalType(AbstractExpression expr) {
		if (expr === null) {
			// should happen only in case of partially parsed ASTs
			return UNKNOWN_NOMINAL_TYPE;
		}

		if (nominalDataType.containsKey(expr)) {
			return nominalDataType.get(expr);
		}

		EcoreUtil.resolveAll(expr);
		val nominalType = noncachedNominalDataType(expr);
		nominalDataType.put(expr, nominalType);

		return nominalType;
	}

	private def dispatch DataType noncachedNominalDataType(Expression expr) {
		// This is a fallback propagation method for unknown expression types.
		// propagate computation for the contained expressions (try to recover)
		expr.eContents.filter(Expression).forEach[it.computeNominalType];

		log.warn("Unable to infer nominal type for the unknown expression '{}'. It has an unexpected type.", expr);
		return UNKNOWN_NOMINAL_TYPE;
	}

	private def dispatch DataType noncachedNominalDataType(QualifiedRef expr) {
		expr.prefix.computeNominalType
		return expr.ref.computeNominalType
	}

	private def dispatch DataType noncachedNominalDataType(FlagRef expr) {
		switch (expr) {
			EnoFlag:
				return BOOL_DT
			OkValRef:
				return BOOL_DT
			RetValRef: {
				val enclosingFunction = EmfHelper.getContainerOfType(expr, Function);
				if (enclosingFunction === null) {
					log.warn("Illegal use of RET_VAL value: it is not used within a function.");
					return UNKNOWN_NOMINAL_TYPE;
				} else {
					return enclosingFunction.returnType;
				}
			}
			default: {
				log.warn("Unable to infer nominal type for the flag reference '{}'. It has an unexpected type.", expr);
				return UNKNOWN_NOMINAL_TYPE;
			}
		}
	}
	
	private def dispatch DataType noncachedNominalDataType(StwBitRef expr) {
		return BOOL_DT;
	}

	private def dispatch DataType noncachedNominalDataType(ArrayRef expr) {
		for(ind : expr.index){
			ind.computeNominalType
		}
		
		val arrayDT = expr.ref.computeNominalType;
		if (arrayDT === null) {
			// most probably partial AST, parsing is in progress
			return UNKNOWN_NOMINAL_TYPE;
		}
		if (arrayDT instanceof ArrayDT) {
			return arrayDT.baseType;
		}
		if (arrayDT.isBitArray) {		
			// indexing word, e.g. wordvariable[0] --> BOOL
			// This is not permitted in Siemens languages, but it is permitted for Schneider.
			return BOOL_DT;
		}

		log.warn("Unable to infer nominal type for the array reference '{}'. It has an unexpected date type.", expr);
		return UNKNOWN_NOMINAL_TYPE;
	}
	
	private def dispatch DataType noncachedNominalDataType(DirectBitAccessRef expr) {
		return BOOL_DT;
	}
	
	

	private def dispatch DataType noncachedNominalDataType(DirectNamedRef expr) {
		switch (expr.ref) {
			Variable:
				return Step7LanguageHelper.getDataType(expr.ref as Variable)
			NamedConstantDeclaration:
				return UNKNOWN_NOMINAL_TYPE
			Function:
				return (expr.ref as Function).returnType
			default: {
				// expr.ref is a NamedElement
				log.trace(
					"Unable to infer nominal type for the direct named reference '{}'. It has an unexpected type.",
					expr);
					return null; // partial AST, fail silently
				}
			}
		}

		private def dispatch DataType noncachedNominalDataType(LongMemoryAddress expr) {
			return elementaryDT.get(DataTypeUtil.elementaryTypeOfAddress(expr));
		}

		private def dispatch DataType noncachedNominalDataType(BitMemoryAddress expr) {
			return BOOL_DT;
		}

		private def dispatch DataType noncachedNominalDataType(DbFieldAddressRef expr) {
			return elementaryDT.get(DataTypeUtil.elementaryTypeOfMemorySize(expr.field.memorySize));
		}

		private def dispatch DataType noncachedNominalDataType(DbFieldIndexedRef expr) {
			return elementaryDT.get(DataTypeUtil.elementaryTypeOfMemorySize(expr.fieldSize.memorySize));
		}

		private def dispatch DataType noncachedNominalDataType(SclSubroutineCall expr) {
			// Only SclSubroutineCall is an expression, STL calls are not, thus they do not have nominal types.
			if (expr.calledUnit instanceof Function) {
				return (expr.calledUnit as Function).returnType;
			} else if (expr.calledUnit instanceof OrganizationBlock || expr.calledUnit instanceof FunctionBlock ||
				expr.calledUnit instanceof Variable || expr.calledUnit instanceof DataBlock) {
				// the nominal type of any non-FC block is 'void'
				return elementaryDT.get(VOID);
			} else {
				return UNKNOWN_NOMINAL_TYPE;
			}
		}

		private def dispatch DataType noncachedNominalDataType(EqualityExpression expr) {
			expr.left.computeNominalType;
			expr.right.computeNominalType;
			// Validation is not done at type computation.
			return BOOL_DT;
		}

		private def dispatch DataType noncachedNominalDataType(ComparisonExpression expr) {
			expr.left.computeNominalType;
			expr.right.computeNominalType;
			// Validation is not done at type computation.
			return BOOL_DT;
		}

		private def dispatch DataType noncachedNominalDataType(AdditiveExpression expr) {
			val leftNominal = expr.left.computeNominalType;
			val rightNominal = expr.right.computeNominalType;

			// TOD + TIME = TOD
			if (leftNominal.isTimeOfDay && rightNominal.isTime) {
				return leftNominal;
			}

			// DT + TIME = DT, TIME + DT = DT
			if (expr.operator == AdditionOperator.PLUS && (leftNominal.isDateTime || rightNominal.isDateTime)) {
				if (leftNominal.isDateTime && rightNominal.isTime) {
					return leftNominal;
				} else if (rightNominal.isDateTime && leftNominal.isTime) {
					return rightNominal;
				} else {
					additionalInvalidityMsg.put(expr,
						"The addition of DT type is only allowed as DT+TIME or TIME+DT. DT+DT is not supported.");
					return UNKNOWN_NOMINAL_TYPE;
				}
			}

			// DT - DT = TIME
			if (expr.operator == AdditionOperator.MINUS && leftNominal.isDateTime && rightNominal.isDateTime) {
				// TIA modifications needed for proper support
				return elementaryDT.get(TIME);
			}

			// DATE - DATE = TIME
			if (expr.operator == AdditionOperator.MINUS && leftNominal.isDate && rightNominal.isDate) {
				// TIA modifications needed for proper support
				return elementaryDT.get(TIME);
			}

			// TOD - TOD = TIME
			if (expr.operator == AdditionOperator.MINUS && leftNominal.isTimeOfDay && rightNominal.isTimeOfDay) {
				// TIA modifications needed for proper support
				return elementaryDT.get(TIME);
			}

			// DT - TIME = DT
			if (expr.operator == AdditionOperator.MINUS && leftNominal.isDateTime && rightNominal.isTime) {
				// TIA modifications needed for proper support
				return leftNominal;
			}

			return greaterType(leftNominal, rightNominal);
		}

		private def dispatch DataType noncachedNominalDataType(MultiplicativeExpression expr) {
			val leftNominal = expr.left.computeNominalType;
			val rightNominal = expr.right.computeNominalType;

			if (leftNominal.isTimeLike) {
				// TIME * (TIME | INT) = TIME
				return leftNominal;
			}

			return greaterType(leftNominal, rightNominal);
		}

		private def dispatch DataType noncachedNominalDataType(PowerExpression expr) {
			val leftNominal = expr.left.computeNominalType;
			val rightNominal = expr.right.computeNominalType;

			val greater = greaterType(leftNominal, rightNominal);
			return greaterType(greater, elementaryDT.get(REAL)); // TIA modifications needed for proper support: LREAL
		}

		private def dispatch DataType noncachedNominalDataType(UnaryExpression expr) {
			val subexprNominal = expr.expr.computeNominalType;
			// Validation is not done at type computation.
			return subexprNominal;
		}

		private def dispatch DataType noncachedNominalDataType(AndExpression expr) {
			val leftNominal = expr.left.computeNominalType();
			val rightNominal = expr.right.computeNominalType();

			return noncachedNominalDataTypeForLogicOperator(leftNominal, rightNominal);
		}

		private def dispatch DataType noncachedNominalDataType(OrExpression expr) {
			val leftNominal = expr.left.computeNominalType();
			val rightNominal = expr.right.computeNominalType();

			return noncachedNominalDataTypeForLogicOperator(leftNominal, rightNominal);
		}

		private def dispatch DataType noncachedNominalDataType(XorExpression expr) {
			val leftNominal = expr.left.computeNominalType();
			val rightNominal = expr.right.computeNominalType();

			return noncachedNominalDataTypeForLogicOperator(leftNominal, rightNominal);
		}

		private def dispatch DataType noncachedNominalDataType(ImpliesExpression expr) {
			val leftNominal = expr.left.computeNominalType();
			val rightNominal = expr.right.computeNominalType();

			return noncachedNominalDataTypeForLogicOperator(leftNominal, rightNominal);
		}

		private def noncachedNominalDataTypeForLogicOperator(DataType leftNominal, DataType rightNominal) {
			if (leftNominal.isAnyBit || leftNominal.isAnyNum || leftNominal === UNKNOWN_NOMINAL_TYPE) {
				return rightNominal;
			}
			if (rightNominal.isAnyBit || rightNominal.isAnyNum || rightNominal === UNKNOWN_NOMINAL_TYPE) {
				return leftNominal;
			}

			if (leftNominal instanceof ElementaryDT && rightNominal instanceof ElementaryDT) {
				// take the bigger
				return elementaryDT.get(
					largerBitDataType((leftNominal as ElementaryDT).type, (rightNominal as ElementaryDT).type))
			}

			log.debug(
				"Unable to resolve the type for the logic operator with left nominal type '{}' and right nominal type '{}'.",
				leftNominal, rightNominal);
			return UNKNOWN_NOMINAL_TYPE;
		}

		private def dispatch DataType noncachedNominalDataType(UnnamedConstant expr) {
			log.error(
				"Unknown unnamed constant type: '{}'. The implementation of noncachedNominalDataType for this case is missing.",
				expr.class.name);
			throw new RuntimeException("Unknown unnamed constant type: " + expr.class.name)
		}

		private def dispatch DataType noncachedNominalDataType(UnnamedConstantRef expr) {
			val type = expr.constant.computeNominalType;
			return type;
		}

		private def dispatch DataType noncachedNominalDataType(NegatedUnnamedConstantRef expr) {
			val type = expr.negatedConstant.computeNominalType;
			return type;
		}

		private def dispatch DataType noncachedNominalDataType(NamedConstantRef expr) {
			return UNKNOWN_NOMINAL_TYPE;
		}

		private def dispatch DataType noncachedNominalDataType(BoolConstant expr) {
			return BOOL_DT;
		}

		private def dispatch DataType noncachedNominalDataType(IntConstant expr) {
			if (expr.value.type == S7IntegerType.Unknown) {
				// Here it's not good to return the smallest INT type. An IntConstant '123' can be INT, WORD, REAL, ...
				return ANY_NUM;
			} else {
				return elementaryDT.get(DataTypeUtil.integerDTtoElementaryDT(expr.value.type));
			}
		}

		private def dispatch DataType noncachedNominalDataType(RealConstant expr) {
			if (expr.value.type == S7RealType.Unknown) {
				return UNKNOWN_NOMINAL_TYPE;
			} else {
				return elementaryDT.get(DataTypeUtil.realDTtoElementaryDT(expr.value.type));
			}
		}

		private def dispatch DataType noncachedNominalDataType(CharConstant expr) {
			// TIA add WCHAR support
			return elementaryDT.get(CHAR);
		}

		private def dispatch DataType noncachedNominalDataType(DateConstant expr) {
			return elementaryDT.get(DATE);
		}

		private def dispatch DataType noncachedNominalDataType(DateAndTimeConstant expr) {
			// TIA add LDT, DLT support
			return elementaryDT.get(DATE_AND_TIME);
		}

		private def dispatch DataType noncachedNominalDataType(TimeOfDayConstant expr) {
			// TIA add LTOD support
			return elementaryDT.get(TIME_OF_DAY);
		}

		private def dispatch DataType noncachedNominalDataType(TimeConstant expr) {
			// TIA add LTIME support
			return elementaryDT.get(TIME);
		}

		private def dispatch DataType noncachedNominalDataType(StringConstant expr) {
			// TIA add WSTRING support
			val ret = Step7LanguageFactory.eINSTANCE.createStringDT();
			ret.dimension = Step7LanguageFactory.eINSTANCE.createIntConstant() => [
				value = S7Integer.create(Integer.toString(expr.value.stringValue.length))
			];
			return ret;
		}

		private def dispatch DataType noncachedNominalDataType(NilValue expr) {
			return POINTER_DT;
		}

		private def pushDownExpected(AbstractExpression expression, DataType expectedType) {
			if (expectedDataType.containsKey(expression)) {
				// already computed
				return;
			}

			val nominalType = nominalDataType.get(expression);
			// log.trace("For {}, nominal type: {}, expected type: {}", expression, nominalType, expectedType);

			if ((nominalType == UNKNOWN_NOMINAL_TYPE || nominalType.isAnyNum) && expectedType.isAnyNum && expression instanceof UnnamedConstant) {
				// Quick fix:
				// Try to infer the nominal type based on the value of the constant, as there are no more clues.
				inferNominalTypeBasedOnConstantValue(expression as UnnamedConstant);
			}

			expectedDataType.put(expression, expectedType);
			propagateExpectedType(expression, expectedType);
		}
	
	private def dispatch inferNominalTypeBasedOnConstantValue(UnnamedConstant constant) {
		// Do nothing by default.
	}
	
	private def dispatch inferNominalTypeBasedOnConstantValue(IntConstant constant) {
		// find the smallest type 
		val type = constant.value.smallestPotentialSignedType;
		if (type !== null) {
			val nominalType = elementaryDT.get(DataTypeUtil.integerDTtoElementaryDT(type));
			nominalDataType.put(constant, nominalType);
			return nominalType;
		}
		return null;
	}
	
	private def dispatch inferNominalTypeBasedOnConstantValue(RealConstant constant) {
		// very basic heuristic
		val nominalType = elementaryDT.get(ElementaryTypeEnum.REAL);
		nominalDataType.put(constant, nominalType);
		return nominalType;
	}

		private def void propagateExpectedType(AbstractExpression expr, DataType expectedType) {
			switch (expr) {
				// # Atomic expressions
				ArrayRef: {
					for (index : expr.index) {
						pushDownExpected(index, elementaryDT.get(INT))
					}
				}	
				DirectBitAccessRef:{
					pushDownExpected(expr.ref, expectedType);
				}			
				QualifiedRef: {
					pushDownExpected(expr.prefix, UNKNOWN_NOMINAL_TYPE);
					pushDownExpected(expr.ref, expectedType);
				}				
				DirectRef,
				MemoryAddress,
				UnnamedConstant,
				NamedConstantRef,
				NilValue: {
					// do nothing for atomic
				}
				UnnamedConstantRef: {
					pushDownExpected(expr.constant, expectedType);
				}
				NegatedUnnamedConstantRef: {
					pushDownExpected(expr.negatedConstant, expectedType);
				}
				// # Subroutine calls
				SubroutineCall: {
					// do nothing; the parameters will be resolved independently
				}
				// # Operations
				UnaryExpression:
					pushDownExpected(expr.expr, expectedType)
				// ## Logic/bitwise operations
				OrExpression: {
					val childExpected = greaterType(expr.computeNominalType, expectedType);
					pushDownExpected(expr.left, childExpected);
					pushDownExpected(expr.right, childExpected);
				}
				XorExpression: {
					val childExpected = greaterType(expr.computeNominalType, expectedType);
					pushDownExpected(expr.left, childExpected);
					pushDownExpected(expr.right, childExpected);
				}
				AndExpression: {
					val childExpected = greaterType(expr.computeNominalType, expectedType);
					pushDownExpected(expr.left, childExpected);
					pushDownExpected(expr.right, childExpected);
				}
				ImpliesExpression: {
					pushDownExpected(expr.left, BOOL_DT);
					pushDownExpected(expr.right, BOOL_DT);
				}
				// ## Arithmetic operations
				AdditiveExpression: {
					if (expectedType.isNumeric || expectedType.isAny || expectedType.isAnyNum) {
						// ANY_NUM + ANY_NUM = ANY_NUM
						// TODO maybe push down the greater of left and right's nominal type here!
						pushDownExpected(expr.left, expectedType);
						pushDownExpected(expr.right, expectedType);
					} else if (expectedType.isTimeLike && expr.operator == AdditionOperator.PLUS) {
						// TIME + TIME = TIME, but also DT - DT = TIME
						pushDownExpected(expr.left, expectedType);
						pushDownExpected(expr.right, expectedType);
					} else if (expectedType.isTimeOfDay) {
						// TOD + TIME = TOD, but TIME + TOD is forbidden
						pushDownExpected(expr.left, expectedType);
						pushDownExpected(expr.right, elementaryDT.get(TIME));
					} else if (expectedType.isDateTime && expr.operator == AdditionOperator.PLUS) {
						// we need to know the nominal types
						val leftNominal = expr.left.computeNominalType;
						val rightNominal = expr.right.computeNominalType;

						if (leftNominal.isDateTime && !rightNominal.isDateTime) {
							pushDownExpected(expr.left, expectedType);
							pushDownExpected(expr.right, elementaryDT.get(TIME));
						} else if (!leftNominal.isDateTime && rightNominal.isDateTime) {
							// TIME+DT is OK, but TIME-DT is not
							pushDownExpected(expr.left, elementaryDT.get(TIME)); // TIA modifications needed for proper support
							pushDownExpected(expr.right, expectedType);
						} else {
							log.warn("Failed to infer the type for the addition of '{}' and '{}'. Expected type is DT.",
								expr.left, expr.right);
							additionalInvalidityMsg.put(expr, "Failed to infer the type for addition.");
						}
					} else if (expr.operator == AdditionOperator.MINUS) {
						// handle special cases for operator '-':
						val leftNominal = expr.left.computeNominalType;
						val rightNominal = expr.right.computeNominalType;
						if (expectedType.isTime && leftNominal.isDateTime && rightNominal.isDateTime) {
							// DT - DT = TIME
							// TIA modifications needed for proper support
							val majoring = greaterType(leftNominal, rightNominal);
							pushDownExpected(expr.left, majoring);
							pushDownExpected(expr.right, majoring);
						} else if (expectedType.isDateTime && leftNominal.isDateTime && rightNominal.isTime) {
							// DT - TIME = DT
							pushDownExpected(expr.left, expectedType);
							pushDownExpected(expr.right, rightNominal);
						} else if (expectedType.isTime && leftNominal.isTime && rightNominal.isTime) {
							// TIME - TIME = TIME
							pushDownExpected(expr.left, expectedType);
							pushDownExpected(expr.right, expectedType);
						} else if (expectedType.isTime && leftNominal.isDate && rightNominal.isDate) {
							// DATE - DATE = TIME
							val majoring = greaterType(leftNominal, rightNominal);
							pushDownExpected(expr.left, majoring);
							pushDownExpected(expr.right, majoring);
						} else if (expectedType.isTime && leftNominal.isTimeOfDay && rightNominal.isTimeOfDay) {
							// TOD - TOD = TIME
							val majoring = greaterType(leftNominal, rightNominal);
							pushDownExpected(expr.left, majoring);
							pushDownExpected(expr.right, majoring);
						} else {
							log.warn("Failed to infer the type for the addition of '{}' and '{}'.", expr.left,
								expr.right);
							additionalInvalidityMsg.put(expr, "Failed to infer the type for addition.");
						}
					} else {
						log.warn("Failed to infer the type for the addition of '{}' and '{}'.", expr.left, expr.right);
						additionalInvalidityMsg.put(expr, "Failed to infer the type for addition.");
					// throw new RuntimeException('''Failed to infer the type for the addition of «expr.left» and «expr.right».''')
					}
				}
				MultiplicativeExpression: {
					if (expectedType.isInteger) {
						pushDownExpected(expr.left, expectedType);
						pushDownExpected(expr.right, expectedType);
					} else if (expectedType.isReal) {
						pushDownExpected(expr.left, expectedType);
						pushDownExpected(expr.right, expectedType);
					} else if (expectedType.isTimeLike) {
						// we need to know the nominal types
						val leftNominal = expr.left.computeNominalType;
						val rightNominal = expr.right.computeNominalType;

						if (leftNominal.isTimeLike && !rightNominal.isTimeLike) {
							pushDownExpected(expr.left, leftNominal);
							pushDownExpected(expr.right, elementaryDT.get(DINT));
						} else if (!leftNominal.isTimeLike && rightNominal.isTimeLike) {
							pushDownExpected(expr.left, elementaryDT.get(DINT));
							pushDownExpected(expr.right, rightNominal);
						} else {
							log.warn("Failed to infer the type for the time-integer multiplication of '{}' and '{}'.",
								expr.left, expr.right);
						}

					} else if (expectedType.isNumeric || expectedType.isAny || expectedType.isAnyNum) {
						// we cannot really know
						// TODO cleanup and unify with other expressions
						val greater = greaterType(expr.left.computeNominalType, expr.right.computeNominalType);

						pushDownExpected(expr.left, greater);
						pushDownExpected(expr.right, greater);
					} else {
						log.warn("Failed to infer the type for the multiplication of '{}' and '{}'.", expr.left,
							expr.right);
					}
				}
				PowerExpression: {
					pushDownExpected(expr.left, elementaryDT.get(REAL)); // TIA modifications needed for proper support (different REAL sizes exist)
					pushDownExpected(expr.right, elementaryDT.get(REAL));
				}
				// ## Comparison operations
				ComparisonExpression: {
					val leftNominal = expr.left.computeNominalType;
					val rightNominal = expr.right.computeNominalType;

					if ((leftNominal.isTimeLike && rightNominal.isTimeLike) ||
						(leftNominal.isDateTime && rightNominal.isDateTime) ||
						(leftNominal.isDate && rightNominal.isDate)) {
							val majoring = greaterType(leftNominal, rightNominal);
							pushDownExpected(expr.left, majoring);
							pushDownExpected(expr.right, majoring);
						} else {
							val greater = greaterType(leftNominal, rightNominal);
							pushDownExpected(expr.left, greater);
							pushDownExpected(expr.right, greater);
						}
					}
					EqualityExpression: {
						val leftNominal = expr.left.computeNominalType;
						val rightNominal = expr.right.computeNominalType;
						val greater = greaterType(leftNominal, rightNominal);
						pushDownExpected(expr.left, greater);
						pushDownExpected(expr.right, greater);
					}
					default:
						if (expr !== null) {
							log.warn("Not possible to propagate for '{}'.", expr)
						}
				}
			}

			private def dispatch void computeTypeForVarInitialization(DataType type,
				VariableInitialization initialization) {
				log.warn("Unknown initialization type: '{}'.", initialization);
			}

			private def dispatch void computeTypeForVarInitialization(DataType type,
				NamedOrUnnamedConstantRef initialization) {
				computeTypeFor(initialization, type);
			}

			private def dispatch void computeTypeForVarInitialization(DataType type,
				InitializationRepetitionList initialization) {
				computeTypeFor(initialization.times, elementaryDT.get(INT));

				if (type instanceof ArrayDT) {
					computeTypeForVarInitialization(type.baseType, initialization.toRepeat);
				} else {
					computeTypeForVarInitialization(type, initialization.toRepeat);
				}
			}

			private def dispatch void computeTypeForVarInitialization(DataType type,
				ArrayInitialization initialization) {
				initialization.elements.forEach[computeTypeForVarInitialization(type, it)];
			}

			private def dispatch void computeTypeForVarInitialization(DataType type,
				ConstantInitializationElement initialization) {
				computeTypeFor(initialization.constant, type);
			}

			private def greaterType(DataType dt1, DataType dt2) {
				if (dt1 === null || dt2 === null) {
					// Most probably we deal with an incomplete AST, give up.
					log.trace("Null type in greaterType({},{}).", dt1, dt2);
					return UNKNOWN_NOMINAL_TYPE;
				}

				if (dt1.isAny) {
					return dt2;
				}
				if (dt2.isAny) {
					return dt1;
				}

				if (dt1 instanceof ElementaryDT && dt2 instanceof ElementaryDT) {
					val elementaryType1 = (dt1 as ElementaryDT).type;
					val elementaryType2 = (dt2 as ElementaryDT).type;

					if (typeConversion.implicitlyConvertableTo(elementaryType1, elementaryType2)) {
						return dt2;
					}
					if (typeConversion.implicitlyConvertableTo(elementaryType2, elementaryType1)) {
						return dt1;
					}
				}

				if (dt1 instanceof ElementaryDT && (dt1.isNumeric || dt1.isBitArray || dt1.isBooleanElementaryType || dt1.isTimeLike) && dt2.isAnyNum) {
					return dt1;
				}
				if (dt2 instanceof ElementaryDT && (dt2.isNumeric || dt2.isBitArray || dt2.isBooleanElementaryType || dt2.isTimeLike) && dt1.isAnyNum) {
					return dt2;
				}

				if (TypeEquals.equalTypes(dt1, dt2)) {
					return dt1;
				}

				log.warn("Unable to compute the majoring type of '{}' and '{}'.", dt1, dt2);
				return UNKNOWN_NOMINAL_TYPE;
			}

			def copyCreated(AbstractExpression original, AbstractExpression copy) {
				val originalType = tryGetType(original);
				Preconditions.checkArgument(originalType.isPresent);

				nominalDataType.put(copy, nominalDataType.get(original));
				expectedDataType.put(copy, expectedDataType.get(original));
				additionalInvalidityMsg.put(copy, additionalInvalidityMsg.get(original));
			}

		}
		