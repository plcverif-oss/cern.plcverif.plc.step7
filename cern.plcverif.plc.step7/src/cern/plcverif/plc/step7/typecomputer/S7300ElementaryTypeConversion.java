/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.typecomputer;

import static cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum.BOOL;
import static cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum.BYTE;
import static cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum.DINT;
import static cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum.DWORD;
import static cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum.INT;
import static cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum.REAL;
import static cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum.S5TIME;
import static cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum.SINT;
import static cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum.TIME;
import static cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum.WORD;
import static com.google.common.collect.Sets.immutableEnumSet;

import java.util.EnumMap;
import java.util.Map;
import java.util.Set;

import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum;

public class S7300ElementaryTypeConversion implements IElementaryTypeConversion {
	private static final Map<ElementaryTypeEnum, Set<ElementaryTypeEnum>> CAN_BE_CONVERTED_TO_IN_SCL;

	static {
		CAN_BE_CONVERTED_TO_IN_SCL = new EnumMap<>(ElementaryTypeEnum.class);

		CAN_BE_CONVERTED_TO_IN_SCL.put(BOOL, immutableEnumSet(BYTE, WORD, DWORD));
		CAN_BE_CONVERTED_TO_IN_SCL.put(BYTE, immutableEnumSet(WORD, DWORD));
		CAN_BE_CONVERTED_TO_IN_SCL.put(WORD, immutableEnumSet(DWORD));
		CAN_BE_CONVERTED_TO_IN_SCL.put(INT, immutableEnumSet(DINT, REAL));
		CAN_BE_CONVERTED_TO_IN_SCL.put(DINT, immutableEnumSet(REAL));
		CAN_BE_CONVERTED_TO_IN_SCL.put(TIME, immutableEnumSet(S5TIME));
		
		CAN_BE_CONVERTED_TO_IN_SCL.put(SINT, immutableEnumSet(INT, DINT, REAL)); // PLCverif only
		// TIME -> S5TIME is added in extra (precision loss possible)
		// The implicit conversion of REAL, DATE_AND_TIME are not possible to
		// any data type.
		// STRING conversion is missing here as it is not an elementary type.
	}

	@Override
	public boolean implicitlyConvertableTo(ElementaryTypeEnum source, ElementaryTypeEnum target) {
		if (source == target) { // NOPMD
			// no conversion needed
			return true;
		}

		if (!CAN_BE_CONVERTED_TO_IN_SCL.containsKey(source)) {
			// 'source' cannot be converted to anything implicitly
			return false;
		}

		return CAN_BE_CONVERTED_TO_IN_SCL.get(source).contains(target);
	}

	@Override
	public boolean matchesAnyNum(ElementaryTypeEnum type) {
		return type == INT || type == DINT || type == REAL || type == SINT;
	}

	@Override
	public boolean matchesAnyBit(ElementaryTypeEnum type) {
		return matchesAnyNum(type) || type == BYTE || type == WORD || type == DWORD;
	}
}
