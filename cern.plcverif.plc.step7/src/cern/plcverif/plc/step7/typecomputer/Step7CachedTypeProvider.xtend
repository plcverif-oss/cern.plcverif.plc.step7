/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.typecomputer

import cern.plcverif.plc.step7.step7Language.Expression
import cern.plcverif.plc.step7.step7Language.ProgramFile
import com.google.inject.Inject
import java.util.Collections
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.util.IResourceScopeCache
import com.google.common.base.Preconditions

class Step7CachedTypeProvider {
	static final Logger log = LogManager.getLogger(Step7CachedTypeProvider);
	
	@Inject IResourceScopeCache cache;
	
	def Step7TypeComputer getTypesForFile(ProgramFile program) {
		if (cache === null) {
			System.err.println("Injection error.");
		}
		Preconditions.checkNotNull(cache, "Cache is null.");
		Preconditions.checkNotNull(program);
		Preconditions.checkNotNull(program.eResource);
		
		cache.get("types" -> program, program.eResource) [
			val types = Step7TypeComputer.compute(Collections.singleton(program));
			log.trace("Types computed for the resource '{}'.", program.eResource.URI.toString)
			return types;
		];
	}

	def Step7TypeComputer getTypesFor(EObject e) {
		val root = EcoreUtil2.getContainerOfType(e, ProgramFile);
		return getTypesForFile(root);
	}

	def getTypeDescriptorFor(Expression e) {
		val root = EcoreUtil2.getContainerOfType(e, ProgramFile);
		return getTypesForFile(root).getTypeDescriptor(e);
	}
}