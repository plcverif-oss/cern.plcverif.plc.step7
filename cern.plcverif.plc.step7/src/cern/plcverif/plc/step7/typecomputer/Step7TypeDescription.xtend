/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.typecomputer

import cern.plcverif.plc.step7.step7Language.BoolConstant
import cern.plcverif.plc.step7.step7Language.DataType
import cern.plcverif.plc.step7.step7Language.ElementaryDT
import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum
import cern.plcverif.plc.step7.step7Language.IntConstant
import cern.plcverif.plc.step7.step7Language.RealConstant
import cern.plcverif.plc.step7.step7Language.UnnamedConstant
import cern.plcverif.plc.step7.util.DataTypeUtil
import cern.plcverif.plc.step7.util.TypeEquals
import java.util.Optional
import org.eclipse.xtend.lib.annotations.Accessors

import static extension cern.plcverif.plc.step7.util.DataTypeUtil.*
import static com.google.common.base.Preconditions.*
import cern.plcverif.plc.step7.step7Language.StringDT
import cern.plcverif.plc.step7.step7Language.AbstractExpression

class Step7TypeDescription {
	IElementaryTypeConversion typeConversion;
	@Accessors(PUBLIC_GETTER) val AbstractExpression expression;
	@Accessors(PUBLIC_GETTER) val DataType nominalDT;
	@Accessors(PUBLIC_GETTER) val DataType expectedDT;
	Optional<Boolean> validity = Optional.empty;
	Optional<String> validityMessage = Optional.empty;

	new(AbstractExpression expression, DataType nominalDT, DataType expectedDT, IElementaryTypeConversion typeConversion) {
		this.expression = expression;
		this.nominalDT = nominalDT;
		this.expectedDT = expectedDT;
		this.typeConversion = typeConversion;
	}
	
	new(AbstractExpression expression, DataType nominalDT, DataType expectedDT, IElementaryTypeConversion typeConversion, String invalidMsg) {
		this.expression = expression;
		this.nominalDT = nominalDT;
		this.expectedDT = expectedDT;
		this.typeConversion = typeConversion;
		this.validity = Optional.of(false);
		this.validityMessage = Optional.of(invalidMsg);
	}

	def boolean isNominalTypeKnown() {
		return (nominalDT !== null && !nominalDT.isAny && !nominalDT.isAnyNum);
	}

	def boolean isExpectedTypeUnknown() {
		return expectedDT === null;
	}

	def boolean isValid() {
		computeValidityIfNeeded();

		checkState(validity.present);
		return validity.get;
	}

	def String getAdditionalValidityMessage() {
		computeValidityIfNeeded();

		if (validityMessage.present) {
			return validityMessage.get;
		} else {
			return "";
		}
	}

	private def computeValidityIfNeeded() {
		if (!validity.present) {
			validity = Optional.of(computeValidity());
		}
	}

	/**
	 * Checks whether the nominal type matches the expected type.
	 */
	private def boolean computeValidity() {
		if (TypeEquals.equalTypes(nominalDT, expectedDT)) {
			return true;
		}

		if (nominalDT instanceof ElementaryDT && expectedDT instanceof ElementaryDT) {
			val nominalType = (nominalDT as ElementaryDT).type;
			val expectedType = (expectedDT as ElementaryDT).type;
			return typeConversion.implicitlyConvertableTo(nominalType, expectedType);
		}
	
		if (nominalDT instanceof ElementaryDT) {
			val nominalType = nominalDT.type;
			if (expectedDT.isAnyNum) {
				val result = typeConversion.matchesAnyNum(nominalType);
				if (!result) {
					validityMessage = Optional.of('''The type «nominalType.literal» is not acceptable for ANY_NUM.''');
				}
				return result;
			}
	
			if (expectedDT.isAnyBit) {
				val result = typeConversion.matchesAnyBit(nominalType);
				if (!result) {
					validityMessage = Optional.of('''The type «nominalType.literal» is not acceptable for ANY_BIT.''');
				}
				return result;
			}
		}
	
		if (expectedDT.isAny || expectedDT.isAnyNum) {
			return true; // TODO improve type validity for ANYs
		}
		
		if ((expectedDT.isAnyNum || expectedDT.isInteger) && nominalDT.isAnyNum) {
			return true; // TODO improve type validity for ANYs
		}

		if (nominalDT instanceof ElementaryDT) {
			val ElementaryDT nominalElementaryDT = nominalDT;
			if (expectedDT.isAnyBit) {
				if (nominalElementaryDT.type.matchesAnyBit) {
					return true;
				}
			} else if (expectedDT.isAnyNum) {
				if (nominalElementaryDT.type.matchesAnyNum) {
					return true;
				}
			} else if (nominalElementaryDT.type == ElementaryTypeEnum.CHAR && expectedDT instanceof StringDT) {
				// CHAR to STRING implicit conversion is permitted
				return true;
			}
		}

		// if nominalDT is missing: check whether the value can fit in the range of the expectedDT
		if (!isNominalTypeKnown()) {
			if (expression instanceof UnnamedConstant && expectedDT instanceof ElementaryDT) {
				// valid only if it fits into the range
				val expectedElementaryType = (expectedDT as ElementaryDT).type;

				switch (expression) {
					BoolConstant:
						return (expectedElementaryType == ElementaryTypeEnum.BOOL)
					IntConstant: {
						if (expectedDT.isReal) {
							// an IntConstant can act a real number
							return true;
						}
						
						val withinRange = DataTypeUtil.elementaryDTtoIntegerDT(expectedElementaryType).isWithinRange(
							expression.value.intValue);
						if (!withinRange) {
							validityMessage = Optional.
								of('''The value «expression.value.intValue» is not within the range of «DataTypeUtil.toString(expectedDT)».''')
						}
						return withinRange;
					}
					RealConstant:
						return DataTypeUtil.isReal(expectedElementaryType)
					default:
						throw new RuntimeException("XXX")
				}
			}

			return true;
		}

		return false;
	}
}
