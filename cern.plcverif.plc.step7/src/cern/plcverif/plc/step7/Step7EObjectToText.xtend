/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7

import cern.plcverif.base.common.emf.textual.EObjectToText
import org.eclipse.emf.ecore.EObject
import cern.plcverif.plc.step7.step7Language.NamedElement

class Step7EObjectToText implements EObjectToText {
	public static final Step7EObjectToText INSTANCE = new Step7EObjectToText();

	private new() {
		// singleton
	}

	override text(EObject e) {
		return textDispatch(e);
	}

	private def dispatch String textDispatch(Void e) {
		return "<null>";
	}

	private def dispatch String textDispatch(EObject e) {
		return e.hashCode.toString;
	}

	private def dispatch String textDispatch(NamedElement e) {
		return '''{NamedElement name=«e.name»}''';
	}
}
