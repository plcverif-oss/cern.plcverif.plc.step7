/******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 * 
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 * 	 Xaver Fink - new features
 *   Martijn Potters - add support for SCL regions in TIA Portal
 *****************************************************************************/
package cern.plcverif.plc.step7.formatting2

import cern.plcverif.plc.step7.services.Step7LanguageGrammarAccess
import cern.plcverif.plc.step7.step7Language.AdditiveExpression
import cern.plcverif.plc.step7.step7Language.AndExpression
import cern.plcverif.plc.step7.step7Language.ArrayDT
import cern.plcverif.plc.step7.step7Language.ArrayDimensionRange
import cern.plcverif.plc.step7.step7Language.ArrayInitialization
import cern.plcverif.plc.step7.step7Language.ArrayRef
import cern.plcverif.plc.step7.step7Language.BitMemoryAddress
import cern.plcverif.plc.step7.step7Language.BlockAttribute
import cern.plcverif.plc.step7.step7Language.BlockAttributes
import cern.plcverif.plc.step7.step7Language.CallParameterItem
import cern.plcverif.plc.step7.step7Language.CallParameterList
import cern.plcverif.plc.step7.step7Language.CaseElement
import cern.plcverif.plc.step7.step7Language.CaseElementValue
import cern.plcverif.plc.step7.step7Language.ComparisonExpression
import cern.plcverif.plc.step7.step7Language.ConstDeclarationBlock
import cern.plcverif.plc.step7.step7Language.DataBlock
import cern.plcverif.plc.step7.step7Language.DataBlockInitialAssignment
import cern.plcverif.plc.step7.step7Language.DeclarationSection
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.ElsifBlock
import cern.plcverif.plc.step7.step7Language.EqualityExpression
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit
import cern.plcverif.plc.step7.step7Language.Expression
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.FunctionBlock
import cern.plcverif.plc.step7.step7Language.GlobalVariableBlock
import cern.plcverif.plc.step7.step7Language.InitializationRepetitionList
import cern.plcverif.plc.step7.step7Language.LabelDeclarationBlock
import cern.plcverif.plc.step7.step7Language.LabeledSclStatement
import cern.plcverif.plc.step7.step7Language.LabeledStlStatement
import cern.plcverif.plc.step7.step7Language.LongMemoryAddress
import cern.plcverif.plc.step7.step7Language.MultiplicativeExpression
import cern.plcverif.plc.step7.step7Language.NamedCallParameterItem
import cern.plcverif.plc.step7.step7Language.NamedConstantDeclaration
import cern.plcverif.plc.step7.step7Language.NegatedUnnamedConstantRef
import cern.plcverif.plc.step7.step7Language.OrExpression
import cern.plcverif.plc.step7.step7Language.OrganizationBlock
import cern.plcverif.plc.step7.step7Language.PowerExpression
import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.step7Language.QualifiedRef
import cern.plcverif.plc.step7.step7Language.SclAssignmentStatement
import cern.plcverif.plc.step7.step7Language.SclCaseStatement
import cern.plcverif.plc.step7.step7Language.SclContinueStatement
import cern.plcverif.plc.step7.step7Language.SclExitStatement
import cern.plcverif.plc.step7.step7Language.SclForStatement
import cern.plcverif.plc.step7.step7Language.SclGotoStatement
import cern.plcverif.plc.step7.step7Language.SclIfStatement
import cern.plcverif.plc.step7.step7Language.SclNullStatement
import cern.plcverif.plc.step7.step7Language.SclRepeatStatement
import cern.plcverif.plc.step7.step7Language.SclReturnStatement
import cern.plcverif.plc.step7.step7Language.SclStatement
import cern.plcverif.plc.step7.step7Language.SclStatementList
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall
import cern.plcverif.plc.step7.step7Language.SclWhileStatement
import cern.plcverif.plc.step7.step7Language.SclRegion
import cern.plcverif.plc.step7.step7Language.SingleCallParameter
import cern.plcverif.plc.step7.step7Language.SingleCaseElementValue
import cern.plcverif.plc.step7.step7Language.Step7LanguagePackage
import cern.plcverif.plc.step7.step7Language.StlAccuDecrementStatement
import cern.plcverif.plc.step7.step7Language.StlAccuIncrementStatement
import cern.plcverif.plc.step7.step7Language.StlAddIntConstStatement
import cern.plcverif.plc.step7.step7Language.StlAssignStatement
import cern.plcverif.plc.step7.step7Language.StlBitLogicStatement
import cern.plcverif.plc.step7.step7Language.StlBldStatement
import cern.plcverif.plc.step7.step7Language.StlFnStatement
import cern.plcverif.plc.step7.step7Language.StlFpStatement
import cern.plcverif.plc.step7.step7Language.StlJumpStatement
import cern.plcverif.plc.step7.step7Language.StlLoadStatement
import cern.plcverif.plc.step7.step7Language.StlNetwork
import cern.plcverif.plc.step7.step7Language.StlNopStatement
import cern.plcverif.plc.step7.step7Language.StlResetStatement
import cern.plcverif.plc.step7.step7Language.StlSetStatement
import cern.plcverif.plc.step7.step7Language.StlShiftRotateStatement
import cern.plcverif.plc.step7.step7Language.StlStatement
import cern.plcverif.plc.step7.step7Language.StlStatementList
import cern.plcverif.plc.step7.step7Language.StlTransferStatement
import cern.plcverif.plc.step7.step7Language.StlWordLogicStatement
import cern.plcverif.plc.step7.step7Language.StructDT
import cern.plcverif.plc.step7.step7Language.UnaryExpression
import cern.plcverif.plc.step7.step7Language.UnaryOperator
import cern.plcverif.plc.step7.step7Language.UnnamedConstant
import cern.plcverif.plc.step7.step7Language.UserDefinedDataType
import cern.plcverif.plc.step7.step7Language.Variable
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine
import cern.plcverif.plc.step7.step7Language.XorExpression
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import com.google.common.base.Strings
import com.google.inject.Inject
import java.util.List
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.GrammarUtil
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.formatting2.AbstractFormatter2
import org.eclipse.xtext.formatting2.IFormattableDocument
import org.eclipse.xtext.formatting2.ITextReplacerContext
import org.eclipse.xtext.formatting2.internal.AbstractTextReplacer
import org.eclipse.xtext.formatting2.regionaccess.ISemanticRegion
import cern.plcverif.plc.step7.step7Language.ImpliesExpression
import cern.plcverif.plc.step7.step7Language.StlJumpList

// See: https://de.slideshare.net/meysholdt/xtexts-new-formatter-api
class Step7LanguageFormatter extends AbstractFormatter2 {
	val int STL_COLUMN_WIDTH = 6;

	// @Inject extension Step7LanguageGrammarAccess
	@Inject Step7LanguageGrammarAccess access

	static class CapitalizerReplacer extends AbstractTextReplacer {
		protected new(IFormattableDocument document, ISemanticRegion toBeReplaced) {
			super(document, toBeReplaced)
		}

		override createReplacements(ITextReplacerContext context) {
			context.addReplacement(region.replaceWith(region.text.toUpperCase));
			return context;
		}
	}

	def dispatch void format(ProgramFile e, extension IFormattableDocument document) {
		e.verificationOptions.forEach[format];
		e.globalVariables.forEach[format];
		e.programUnits.forEach[format];

		capitalizeAllKeywords(document);
		capitalizeAllKeywordlikeEnums(document);
	}

	private def capitalizeAllKeywords(IFormattableDocument document) {
		val allKeywords = GrammarUtil.getAllKeywords(access.grammar);
		for (occurrence : textRegionAccess.regionForRootEObject.allRegionsFor.keywords(allKeywords)) {
			if (!occurrence.text.equals(occurrence.text.toUpperCase)) {
				document.addReplacer(new CapitalizerReplacer(document, occurrence));
			}
		}
	}

	private def capitalizeAllKeywordlikeEnums(IFormattableDocument document) {
		for (occurrence : textRegionAccess.regionForRootEObject.allRegionsFor.ruleCallsTo(
			access.variableDeclarationDirectionRule,
			access.globalVariableDeclarationDirectionRule,
			access.elementaryTypeEnumRule,
			access.parameterTypeEnumRule,
			access.multiplicationOperatorRule,
			access.unaryOperatorRule
		)) {
			if (!occurrence.text.equals(occurrence.text.toUpperCase)) {
				document.addReplacer(new CapitalizerReplacer(document, occurrence));
			}
		}
	}

	private def capitalizeAllStlMnemonics(StlStatementList statementList, IFormattableDocument document) {
		for (occurrence : statementList.allRegionsFor.ruleCallsTo(
			access.stlAccuArgumentlessMnemonicRule,
			access.stlArithmeticMnemonicRule,
			access.stlBitLogicOpMnemonicRule,
			access.stlBlockEndMnemonicRule,
			access.stlComparisonDataTypeRule,
			access.stlConversionMnemonicRule,
			access.stlFloatMathFunctionMnemonicRule,
			access.stlJumpMnemonicRule,
			access.stlParameterlessCallMnemonicRule,
			access.stlShiftRotateMnemonicRule,
			access.stlWordLogicMnemonicRule,
			access.stwBitRefMnemonicRule
		)) {
			if (!occurrence.text.equals(occurrence.text.toUpperCase)) {
				document.addReplacer(new CapitalizerReplacer(document, occurrence));
			}
		}
	}

	def dispatch void format(OrganizationBlock e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.organizationBlockAccess.ORGANIZATION_BLOCKKeyword_0).append[oneSpace];
		e.regionFor.ruleCall(access.organizationBlockAccess.nameIdOrSymbolParserRuleCall_1_0).append[newLine];

		formatProgramBlock(e, document);
	}

	def dispatch void format(FunctionBlock e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.functionBlockAccess.FUNCTION_BLOCKKeyword_0_0).append[oneSpace];
		e.regionFor.ruleCall(access.functionBlockAccess.nameIdOrSymbolParserRuleCall_1_0).append[newLine];

		formatProgramBlock(e, document);
	}

	def dispatch void format(Function e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.functionAccess.FUNCTIONKeyword_0).append[oneSpace];
		e.regionFor.keyword(access.functionAccess.colonKeyword_2).prepend[oneSpace].append[oneSpace];
		e.returnType.append[newLine];

		formatProgramBlock(e, document);
	}

	private def void formatProgramBlock(ExecutableProgramUnit e, extension IFormattableDocument document) {
		if (e.attributes !== null && !e.attributes.attributes.isEmpty) {
			e.attributes.surround[indent; highPriority].prepend[newLine].append[setNewLines(1, 2, 3); highPriority];
		}

		e.statements.surround[indent; highPriority];
		e.declarationSection.prepend[newLine].surround[indent; highPriority];
		e.regionFor.keyword("BEGIN").prepend[newLine].append[indent; newLine; highPriority];

		e.getAttributes.format;
		e.getDeclarationSection.format;
		e.getStatements.format;
	}

	def dispatch void format(DataBlock e, extension IFormattableDocument document) {
		e.prepend[setNewLines(2, 2, 5)].append[setNewLines(2, 2, 5)];
		e.structure.format;
		e.attributes.surround[indent].append[setNewLines(2, 2, 3)].format;

		e.regionFor.keyword(access.dataBlockAccess.DATA_BLOCKKeyword_0).append[oneSpace];
		if (e.structure instanceof StructDT) {
			e.regionFor.ruleCall(access.dataBlockAccess.nameIdOrSymbolParserRuleCall_1_0_0).append[newLine];
			e.structure.prepend[noSpace];
		} else {
			e.regionFor.ruleCall(access.dataBlockAccess.nameIdOrSymbolParserRuleCall_1_0_0).append[oneSpace];
		}
		e.regionFor.keyword(access.dataBlockAccess.BEGINKeyword_5).prepend[newLine].append[newLine];
		e.initialAssignments.forEach[surround[indent]; format];
		e.regionFor.keywords(access.dataBlockAccess.semicolonKeyword_6_1).forEach[prepend[noSpace].append[newLine]];

	}

	def dispatch void format(UserDefinedDataType e, extension IFormattableDocument document) {
		e.prepend[setNewLines(2, 2, 5)].append[setNewLines(2, 2, 5)];

		e.regionFor.keyword(access.userDefinedDataTypeAccess.TYPEKeyword_0).append[oneSpace];
		e.regionFor.ruleCall(access.userDefinedDataTypeAccess.nameIdOrSymbolParserRuleCall_1_0).append[newLine];
		e.declaration.format;

		e.regionFor.keyword(access.userDefinedDataTypeAccess.END_TYPEKeyword_6).prepend[newLine].append[noSpace];

	}

	def dispatch void format(DeclarationSection e, extension IFormattableDocument document) {
		e.constantDeclarations.forEach[format];
		e.variableDeclarations.forEach[format];
		e.labelDeclarations.forEach[format];
	}

	def dispatch void format(VariableDeclarationBlock e, extension IFormattableDocument document) {
		formatVarDeclBlock(e, e.variables, document);
	}

	def dispatch void format(GlobalVariableBlock e, extension IFormattableDocument document) {
		formatGlobalVarDeclBlock(e, e.variables, document);
	}

	def void formatVarDeclBlock(VariableDeclarationBlock e, List<VariableDeclarationLine> varDeclLines,
		extension IFormattableDocument document) {
		e.interior[indent];
		if (e.retain) {
			e.regionFor.keyword(access.variableDeclarationBlockAccess.retainRETAINKeyword_2_0).prepend[oneSpace].append[newLine];
		} else {
			e.regionFor.feature(Step7LanguagePackage.eINSTANCE.variableDeclarationBlock_Direction).append[newLine];
		}
		
		formatVarDeclLines(varDeclLines, document);

		e.prepend[setNewLines(1, 1, 3); lowPriority];
		e.append[setNewLines(1, 1, 3); lowPriority];
	}
	
	def void formatGlobalVarDeclBlock(GlobalVariableBlock e, List<VariableDeclarationLine> varDeclLines,
		extension IFormattableDocument document) {
		e.interior[indent];
		e.regionFor.feature(Step7LanguagePackage.eINSTANCE.variableDeclarationBlock_Direction).append[newLine];
		
		formatVarDeclLines(varDeclLines, document);

		e.prepend[setNewLines(1, 1, 3); lowPriority];
		e.append[setNewLines(1, 1, 3); lowPriority];
	}

	def void formatVarDeclLines(List<VariableDeclarationLine> varDeclLines, extension IFormattableDocument document) {
		varDeclLines.forEach[format];

		// trial: table-based formatting
		val varWidths = varDeclLines.map[it.variables.map[v|v.name.length].reduce[sum, size|sum + size + 2]].toList;
		val width = if(varWidths.isEmpty) 1 else varWidths.max + 1;
		for (line : varDeclLines) {
			val currentLen = line.variables.map[w|Step7LanguageHelper.namedElementAsInSourceCode(w).length].reduce [sum, size|
				sum + size + 2
			];
			line.regionFor.keyword(":").prepend[space = Strings.repeat(" ", width - currentLen)];
		}
	}

	def dispatch void format(ConstDeclarationBlock e, extension IFormattableDocument document) {
		e.interior[indent];
		e.append[newLine];

		e.regionFor.keyword(access.constDeclarationBlockAccess.CONSTKeyword_1_0_0).append[newLine];
		e.regionFor.keyword(access.constDeclarationBlockAccess.END_CONSTKeyword_1_0_2).prepend[newLine].append[noSpace];
		
		e.regionFor.keyword(access.constDeclarationBlockAccess.VAR_CONSTANTKeyword_1_1_0).append[newLine];
		e.regionFor.keyword(access.constDeclarationBlockAccess.END_VARKeyword_1_1_2).prepend[newLine].append[noSpace];
		
		e.constants.forEach[format];
		
		e.regionFor.keywords(access.constDeclarationBlockAccess.semicolonKeyword_1_0_1_1).forEach[append[newLine]];
		e.regionFor.keywords(access.constDeclarationBlockAccess.semicolonKeyword_1_1_1_1).forEach[append[newLine]];
	}

	def dispatch void format(LabelDeclarationBlock e, extension IFormattableDocument document) {
		e.interior[indent];
		e.append[newLine];

		e.regionFor.keyword(access.labelDeclarationBlockAccess.LABELKeyword_1).append[newLine];
		e.regionFor.keyword(access.labelDeclarationBlockAccess.END_LABELKeyword_3).prepend[newLine].append[noSpace];
		e.labels.forEach[format];
		e.regionFor.keywords(access.labelDeclarationBlockAccess.semicolonKeyword_2_2).forEach[append[newLine]];
		e.regionFor.keywords(access.labelDeclarationBlockAccess.commaKeyword_2_1_0).forEach [
			prepend[noSpace]
			append[oneSpace]
		];
	}

	def dispatch void format(VariableDeclarationLine e, extension IFormattableDocument document) {
		// e.regionFor.keyword(":").prepend[oneSpace].append[oneSpace];
		e.regionFor.keyword(":").append[oneSpace];
		e.regionFor.keyword(";").prepend[noSpace].append[setNewLines(1, 1, 2)];
		e.allRegionsFor.keywords(",").forEach[it.prepend[noSpace].append[oneSpace]];
		
		e.regionFor.keyword("to").prepend[oneSpace].append[oneSpace];
		e.regionFor.keyword(access.variableDeclarationLineAccess.getBoundedBoundKeyword_5_0_0).prepend[oneSpace].append[noSpace];
		e.regionFor.keyword(access.variableDeclarationLineAccess.getRightParenthesisKeyword_5_4).prepend[noSpace].append[noSpace];

		e.regionFor.keyword(access.variableDeclarationLineAccess.initializedColonEqualsSignKeyword_4_0_0).prepend [
			oneSpace
		].append[oneSpace];

		e.variables.forEach[format];
		e.type.format;
		e.initialization.format;
	}

	def dispatch void format(Variable e, extension IFormattableDocument document) {
	}

	def dispatch void format(ArrayInitialization e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.variableInitializationAccess.leftSquareBracketKeyword_0).append[noSpace];
		e.regionFor.keyword(access.variableInitializationAccess.rightSquareBracketKeyword_3).prepend[noSpace];

		e.elements.forEach[format];
	}

	def dispatch void format(InitializationRepetitionList e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.arrayInitializationElementAccess.leftParenthesisKeyword_1_2).prepend[noSpace].append [
			noSpace
		];
		e.regionFor.keyword(access.arrayInitializationElementAccess.rightParenthesisKeyword_1_4).prepend[noSpace].append [
			noSpace
		];

		e.toRepeat.format;
	}

	def dispatch void format(StructDT e, extension IFormattableDocument document) {
		e.surround[indent; highPriority];
		e.interior[indent; highPriority];

		e.regionFor.keyword("STRUCT").append[newLine];
		e.regionFor.keyword(access.structDTAccess.END_STRUCTKeyword_3).prepend[newLine; highPriority];

		e.members.forEach[format];

		formatVarDeclLines(e.members, document);
	}

	def dispatch void format(ArrayDT e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.arrayDTAccess.ARRAYKeyword_0).append[oneSpace];

		e.regionFor.keyword(access.arrayDTAccess.leftSquareBracketKeyword_1).prepend[oneSpace].append[noSpace];
		e.regionFor.keyword(access.arrayDTAccess.rightSquareBracketKeyword_4).prepend[noSpace].append[oneSpace];

		e.regionFor.keyword(access.arrayDTAccess.OFKeyword_5).prepend[oneSpace].append[oneSpace];
		e.regionFor.keyword(access.arrayDTAccess.commaKeyword_3_0).prepend[noSpace; highPriority].append [
			oneSpace;
			highPriority
		];

		e.baseType.format;
		e.dimensions.forEach[format];
	}

	def dispatch void format(ArrayDimensionRange e, extension IFormattableDocument document) {
		e.regionFor.ruleCall(access.arrayDimensionAccess.DOUBLE_DOTTerminalRuleCall_0_2).prepend[oneSpace].append [
			oneSpace
		];

		e.from.format;
		e.to.format;
	}

	def dispatch void format(NamedConstantDeclaration e, extension IFormattableDocument document) {
		e.prepend[noSpace].append[noSpace]; // ; is not contained by this rule
		e.regionFor.keyword(access.namedConstantDeclarationAccess.colonEqualsSignKeyword_2).prepend[oneSpace].append [
			oneSpace
		];

		e.value.format;
	}

	def dispatch void format(DataBlockInitialAssignment e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.dataBlockInitialAssignmentAccess.colonEqualsSignKeyword_1).prepend[oneSpace].append [
			oneSpace
		];

		e.leftValue.format;
		e.constant.format;
	}

	def dispatch void format(BlockAttributes e, extension IFormattableDocument document) {
		e?.attributes?.forEach[format];
	}

	def dispatch void format(BlockAttribute e, extension IFormattableDocument document) {
		e.append[setNewLines(1, 1, 3) lowPriority];
		e.regionFor.keyword(":").prepend[noSpace].append[oneSpace];
		e.regionFor.keyword("=").prepend[oneSpace].append[oneSpace];
	}

	def dispatch void format(SclStatementList e, extension IFormattableDocument document) {
		e.statements.forEach[format append[newLine; lowPriority]];
	}

	def dispatch void format(LabeledSclStatement e, extension IFormattableDocument document) {
		e.regionFor.keywords(access.labeledSclStatementAccess.colonKeyword_0_1).forEach [
			prepend[noSpace]
			append[oneSpace; highPriority]
		];
		e.labels.forEach[format];
		e.statement.format;
	}

	def dispatch void format(SclNullStatement e, extension IFormattableDocument document) {
		e.regionFor.keyword(";").prepend[setNewLines(0, 1, 1) lowPriority];
	}

	def dispatch void format(SclAssignmentStatement e, extension IFormattableDocument document) {
		e.regionFor.keyword(":=").prepend[oneSpace].append[oneSpace];

		e.leftValue.format;
		e.rightValue.format;

		formatSclStatementSemicolon(e, document);
	}

	def dispatch void format(SclSubroutineCall e, extension IFormattableDocument document) {
		e.regionFor.keyword("(").prepend[noSpace];

		if (e.callParameter instanceof CallParameterList &&
			(e.callParameter as CallParameterList).parameters.size >= 2) {
			e.regionFor.keyword("(").append[newLine];
			e.regionFor.keywordPairs("(", ")").forEach[interior[indent]];
			e.regionFor.keyword(")").prepend[newLine];
			e.allRegionsFor.keywords(",").forEach[append[newLine]];
		} else {
			e.regionFor.keyword(access.sclSubroutineCallAccess.leftParenthesisKeyword_2).prepend[noSpace].append [
				noSpace
			];
			e.regionFor.keyword(access.sclSubroutineCallAccess.rightParenthesisKeyword_4).prepend[noSpace];
		}

		e.callParameter.format;
	}

	def dispatch void format(SingleCallParameter e, extension IFormattableDocument document) {
		e.parameter.prepend[noSpace].append[noSpace];
		e.parameter.format;
	}

	def dispatch void format(CallParameterList e, extension IFormattableDocument document) {
		e.allRegionsFor.keyword(",").prepend[noSpace].append[oneSpace];
		e.parameters.forEach[format];
	}

	def dispatch void format(CallParameterItem e, extension IFormattableDocument document) {
		formatCallParamItemGeneric(e, document);
	}

	def dispatch void format(NamedCallParameterItem e, extension IFormattableDocument document) {
		e.parameter.format;

		formatCallParamItemGeneric(e, document);
	}

	private def void formatCallParamItemGeneric(CallParameterItem e, extension IFormattableDocument document) {
		e.regionFor.assignment(access.callParameterItemAccess.assignmentOperatorAssignment_0_2).prepend[oneSpace].append [
			oneSpace
		];
		e.regionFor.assignment(access.callParameterItemAccess.assignmentOperatorAssignment_1_2).prepend[oneSpace].append [
			oneSpace
		];
		e.regionFor.assignment(access.callParameterItemAccess.assignmentOperatorAssignment_2_2).prepend[oneSpace].append [
			oneSpace
		];

		e.value.format;
	}

	def dispatch void format(SclIfStatement e, extension IFormattableDocument document) {
		e.prepend[setNewLines(1, 2, 3)].append[setNewLines(1, 2, 3)];

		e.thenBranch.surround[indent];
		e.elsifBranches.forEach[it.elsifBranch.surround[indent]];
		if (e.^else) {
			e.elseBranch.surround[indent];
		}

		e.regionFor.keyword("IF").append[oneSpace];
		e.allRegionsFor.keywords("THEN").forEach[it.prepend[oneSpace].append[newLine]];
		e.allRegionsFor.keywords("ELSIF").forEach[it.prepend[newLine].append[oneSpace]];
		e.regionFor.keyword("ELSE").prepend[newLine].append[newLine];
		e.regionFor.keyword("END_IF").prepend[newLine].append[noSpace];
		formatSclStatementSemicolon(e, document);

		// format children
		e.thenBranch.format;
		e.elsifBranches.forEach[format];
		e?.elseBranch?.format;
	}

	def dispatch void format(ElsifBlock e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.elsifBlockAccess.ELSIFKeyword_0).prepend[newLine].append[oneSpace];
		e.regionFor.keyword(access.elsifBlockAccess.THENKeyword_2).prepend[oneSpace].append[newLine];

		e.condition.format;
		e.elsifBranch.format;
	}

	def dispatch void format(SclCaseStatement e, extension IFormattableDocument document) {
		e.prepend[noSpace].append[newLines = 2];

		interior(
			e.regionFor.keyword(access.sclCaseStatementAccess.OFKeyword_2),
			e.regionFor.keyword(access.sclCaseStatementAccess.END_CASEKeyword_5)
		)[indent];

		e.regionFor.keyword(access.sclCaseStatementAccess.CASEKeyword_0).append[oneSpace];
		e.regionFor.keyword(access.sclCaseStatementAccess.OFKeyword_2).prepend[oneSpace].append[noSpace; newLine];
		e.regionFor.keyword(access.sclCaseStatementAccess.elseELSEKeyword_4_0_0).prepend[newLine];
		e.elseStatements.prepend[newLine];
		e.regionFor.keyword(access.sclCaseStatementAccess.END_CASEKeyword_5).prepend[newLine].append[noSpace];

		e.selection.format;
		e.elements.forEach[prepend[newLine] format];
		e.elseStatements.surround[indent].format;
	}

	def dispatch void format(CaseElement e, extension IFormattableDocument document) {
		e.statements.surround[indent].format;
		e.regionFor.keyword(access.caseElementAccess.colonKeyword_2).prepend[noSpace].append[newLine];
		e.regionFor.keywords(access.caseElementAccess.commaKeyword_1_0).forEach[prepend[noSpace] append[oneSpace]];
		e.values.forEach[format];
	}

	def dispatch void format(CaseElementValue e, extension IFormattableDocument document) {
		e.regionFor.ruleCall(access.caseElementValueAccess.DOUBLE_DOTTerminalRuleCall_1_2).prepend[oneSpace].append [
			oneSpace
		];
		if (e instanceof SingleCaseElementValue) {
			e.value.format;
		}
	}

	def dispatch void format(SclForStatement e, extension IFormattableDocument document) {
		e.prepend[noSpace].append[setNewLines(2, 2, 3)];

		e.regionFor.keyword(access.sclForStatementAccess.FORKeyword_0).append[oneSpace];
		e.regionFor.keywords(access.sclForStatementAccess.TOKeyword_2, access.sclForStatementAccess.byBYKeyword_4_0_0).
			forEach[prepend[oneSpace].append[oneSpace]];
		e.regionFor.keyword(access.sclForStatementAccess.DOKeyword_5).prepend[oneSpace].append[newLine];
		e.regionFor.keyword(access.sclForStatementAccess.END_FORKeyword_7).prepend[newLine].append[noSpace];

		e.statements.surround[indent].format;

		e.initialStatement.format;
		e.finalValue.format;
		e.increment.format;

		formatSclStatementSemicolon(e, document);
	}

	def dispatch void format(SclRepeatStatement e, extension IFormattableDocument document) {
		e.append[setNewLines(2, 2, 3)];

		e.regionFor.keyword(access.sclRepeatStatementAccess.REPEATKeyword_0).append[noSpace; newLine];
		e.regionFor.keyword(access.sclRepeatStatementAccess.UNTILKeyword_2).prepend[newLine].append[oneSpace];
		e.regionFor.keyword(access.sclRepeatStatementAccess.END_REPEATKeyword_4).prepend[oneSpace].append[noSpace];

		e.statements.surround[indent];
		e.statements.format;
		e.exitCondition.format;

		formatSclStatementSemicolon(e, document);
	}

	def dispatch void format(SclWhileStatement e, extension IFormattableDocument document) {
		e.prepend[noSpace].append[setNewLines(2, 2, 3)];

		e.regionFor.keyword(access.sclWhileStatementAccess.WHILEKeyword_0).append[oneSpace];
		e.regionFor.keyword(access.sclWhileStatementAccess.DOKeyword_2).prepend[oneSpace].append[noSpace; newLine];
		e.regionFor.keyword(access.sclWhileStatementAccess.END_WHILEKeyword_4).prepend[newLine].append[noSpace];

		e.statements.surround[indent];
		e.statements.format;
		e.entryCondition.format;

		formatSclStatementSemicolon(e, document);
	}
	
	def dispatch void format(SclRegion e, extension IFormattableDocument document) {
		e.body.surround[indent];
		e.body.format;
	}

	def dispatch void format(SclGotoStatement e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.sclGotoStatementAccess.GOTOKeyword_0).append[oneSpace];
		formatSclStatementSemicolon(e, document);
	}

	def dispatch void format(SclExitStatement e, extension IFormattableDocument document) {
		formatSclStatementSemicolon(e, document);
	}

	def dispatch void format(SclContinueStatement e, extension IFormattableDocument document) {
		formatSclStatementSemicolon(e, document);
	}

	def dispatch void format(SclReturnStatement e, extension IFormattableDocument document) {
		formatSclStatementSemicolon(e, document);
	}

	private def void formatSclStatementSemicolon(SclStatement e, extension IFormattableDocument document) {
		e.regionFor.keyword(";").prepend[noSpace];
	}

	def dispatch void format(StlStatementList e, extension IFormattableDocument document) {
		e.networks.forEach[format];
		capitalizeAllStlMnemonics(e, document);
	}

	def dispatch void format(StlNetwork e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.stlNetworkAccess.NETWORKKeyword_1).prepend[setNewLines(1,2,3)].append[newLine];
		e.interior[indent];
		e.titleAttribute?.format;
		e.statements.forEach[format];

	}
	
	def dispatch void format(StlJumpList e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlJumpStatementAccess.mnemonicAssignment_0, e.mnemonic.toString, document);
		e.jumps.forEach[format];
		e.closingLabel.format;
	}

	def dispatch void format(StlStatement e, extension IFormattableDocument document) {
		e.append[newLine];
	}

	def dispatch void format(StlJumpStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlJumpStatementAccess.mnemonicAssignment_0, e.mnemonic.toString, document);
	}

	def dispatch void format(StlBitLogicStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlUnaryStatementAccess.mnemonicAssignment_0_1, e.mnemonic.toString,
			document);
	}

	def dispatch void format(StlAssignStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlUnaryStatementAccess.equalsSignKeyword_1_1, document);
	}

	def dispatch void format(StlResetStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlUnaryStatementAccess.RKeyword_2_1, document);
	}

	def dispatch void format(StlSetStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlUnaryStatementAccess.SKeyword_3_1, document);
	}

	def dispatch void format(StlFpStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlUnaryStatementAccess.FPKeyword_4_1, document);
	}

	def dispatch void format(StlFnStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlUnaryStatementAccess.FNKeyword_5_1, document);
	}

	def dispatch void format(StlLoadStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlUnaryStatementAccess.LKeyword_6_1, document);
	}

	def dispatch void format(StlTransferStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlUnaryStatementAccess.TKeyword_7_1, document);
	}

	def dispatch void format(StlAddIntConstStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlUnaryConstantStatementAccess.plusSignKeyword_0_1, document);
	}

	def dispatch void format(StlWordLogicStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlUnaryConstantStatementAccess.mnemonicAssignment_2_1, e.mnemonic.toString,
			document);
	}

	def dispatch void format(StlAccuIncrementStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlUnaryConstantStatementAccess.INCKeyword_3_1, document);
	}

	def dispatch void format(StlAccuDecrementStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlUnaryConstantStatementAccess.DECKeyword_4_1, document);
	}

	def dispatch void format(StlBldStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlUnaryConstantStatementAccess.BLDKeyword_5_1, document);
	}

	def dispatch void format(StlNopStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlUnaryConstantStatementAccess.NOPKeyword_6_1, document);
	}

	def dispatch void format(StlShiftRotateStatement e, extension IFormattableDocument document) {
		formatUnaryStlStatement(e, access.stlShiftRotateStatementAccess.mnemonicAssignment_0, e.mnemonic.toString,
			document);
	}

	private def void formatUnaryStlStatement(StlStatement e, Keyword mnemonicKeyword,
		extension IFormattableDocument document) {
		e.regionFor.keyword(mnemonicKeyword).append [
			space = Strings.repeat(" ", STL_COLUMN_WIDTH - mnemonicKeyword.value.length)
		]
		e.append[newLine; lowPriority];

	}

	private def void formatUnaryStlStatement(StlStatement e, Assignment mnemonicAssignment, String mnemonic,
		extension IFormattableDocument document) {
		e.regionFor.assignment(mnemonicAssignment).append [
			space = Strings.repeat(" ", STL_COLUMN_WIDTH - mnemonic.toString.length)
		]
		e.append[newLine; lowPriority];
	}

	def dispatch void format(LabeledStlStatement e, extension IFormattableDocument document) {
	}

	// Expressions
	def dispatch void format(Expression e, extension IFormattableDocument document) {
		println('''Unknown expression in formatter: «e»''');
	}

	def dispatch void format(ImpliesExpression e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.impliesExpressionAccess.hyphenMinusHyphenMinusGreaterThanSignKeyword_1_1).prepend [
			oneSpace
		].append[oneSpace];

		e.formatExpressionParens(document);
		e.left.format;
		e.right.format;
	}

	def dispatch void format(OrExpression e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.orExpressionAccess.ORKeyword_1_1).prepend[oneSpace].append[oneSpace];

		e.formatExpressionParens(document);
		e.left.format;
		e.right.format;
	}

	def dispatch void format(XorExpression e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.xorExpressionAccess.XORKeyword_1_1).prepend[oneSpace].append[oneSpace];

		e.formatExpressionParens(document);
		e.left.format;
		e.right.format;
	}

	def dispatch void format(AndExpression e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.andExpressionAccess.opStringAmpersandKeyword_1_1_0_0).prepend[oneSpace].append [
			oneSpace
		];
		e.regionFor.keyword(access.andExpressionAccess.opStringANDKeyword_1_1_0_1).prepend[oneSpace].append[oneSpace];

		e.formatExpressionParens(document);
		e.left.format;
		e.right.format;
	}

	def dispatch void format(EqualityExpression e, extension IFormattableDocument document) {
		e.regionFor.assignment(access.equalityExpressionAccess.operatorAssignment_1_1).prepend[oneSpace].append [
			oneSpace
		];

		e.formatExpressionParens(document);
		e.left.format;
		e.right.format;
	}

	def dispatch void format(ComparisonExpression e, extension IFormattableDocument document) {
		e.regionFor.assignment(access.comparisonExpressionAccess.operatorAssignment_1_1).prepend[oneSpace].append [
			oneSpace
		];

		e.formatExpressionParens(document);
		e.left.format;
		e.right.format;
	}

	def dispatch void format(AdditiveExpression e, extension IFormattableDocument document) {
		e.regionFor.assignment(access.additiveExpressionAccess.operatorAssignment_1_1).prepend[oneSpace].append [
			oneSpace
		];

		e.formatExpressionParens(document);
		e.left.format;
		e.right.format;
	}

	def dispatch void format(MultiplicativeExpression e, extension IFormattableDocument document) {
		e.regionFor.assignment(access.multiplicativeExpressionAccess.operatorAssignment_1_1).prepend[oneSpace].append [
			oneSpace
		];

		e.formatExpressionParens(document);
		e.left.format;
		e.right.format;
	}

	def dispatch void format(PowerExpression e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.powerExpressionAccess.asteriskAsteriskKeyword_1_1).prepend[oneSpace].
			append[oneSpace];

		e.formatExpressionParens(document);
		e.left.format;
		e.right.format;
	}

	def dispatch void format(UnaryExpression e, extension IFormattableDocument document) {
		if (e.op == UnaryOperator.NOT) {
			e.regionFor.assignment(access.unaryExpressionAccess.opAssignment_1).append[oneSpace];
		}

		e.formatExpressionParens(document);
		e.expr.format;
	}

	def dispatch void format(UnnamedConstant e, extension IFormattableDocument document) {
		e.formatExpressionParens(document);
	}

	def dispatch void format(DirectNamedRef e, extension IFormattableDocument document) {
		e.formatExpressionParens(document);
	}

	def dispatch void format(BitMemoryAddress e, extension IFormattableDocument document) {
		e.formatExpressionParens(document);

		capitalize(e, document);
	}

	def dispatch void format(LongMemoryAddress e, extension IFormattableDocument document) {
		e.formatExpressionParens(document);

		capitalize(e, document);
	}

	private def capitalize(EObject e, extension IFormattableDocument document) {
		textRegionAccess.regionForEObject(e).semanticRegions.forEach [ it |
			document.addReplacer(new CapitalizerReplacer(document, it))
		];
	}

	def dispatch void format(QualifiedRef e, extension IFormattableDocument document) {
		e.formatExpressionParens(document);
		e.regionFor.keyword(access.qualifiedRefAccess.fullStopKeyword_1_1).prepend[noSpace].append[noSpace];

		e.prefix.format;
		e.ref.format;
	}

	def dispatch void format(ArrayRef e, extension IFormattableDocument document) {
		e.formatExpressionParens(document);
		e.regionFor.keyword(access.unqualifiedRefAccess.leftSquareBracketKeyword_1_1).prepend[noSpace; highPriority].
			append[noSpace; highPriority];
		e.regionFor.keyword("[").prepend[noSpace; highPriority].append[noSpace; highPriority];
		e.regionFor.keyword(access.unqualifiedRefAccess.rightSquareBracketKeyword_1_4).prepend[noSpace];

		// Indices
		e.regionFor.keywords(access.unqualifiedRefAccess.commaKeyword_1_3_0).forEach [
			prepend[noSpace].append[oneSpace; highPriority]
		];

		e.ref.format;
		e.index.forEach[format];
	}

	private def void formatExpressionParens(Expression e, extension IFormattableDocument document) {
		e.regionFor.keywords(access.primaryExpressionAccess.leftParenthesisKeyword_2_0).forEach[append[noSpace]];
		e.regionFor.keywords(access.primaryExpressionAccess.rightParenthesisKeyword_2_2).forEach[prepend[noSpace]];
	}

	// Misc
	def dispatch void format(NegatedUnnamedConstantRef e, extension IFormattableDocument document) {
		e.regionFor.keyword(access.negatedUnnamedConstantRefAccess.hyphenMinusKeyword_1).append[noSpace highPriority];
	}
}
