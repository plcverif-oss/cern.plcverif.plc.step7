/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.indexing

import com.google.inject.Inject
import org.eclipse.xtext.resource.impl.ResourceDescriptionsProvider
import org.eclipse.xtext.resource.IContainer
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EClass

class Step7LanguageIndex {
	@Inject
	ResourceDescriptionsProvider resourceDescriptionsProvider;

	@Inject
	IContainer.Manager containerManager;

	def getVisibleEObjectDescriptions(EObject from, EClass type) {
		return from.getVisibleContainers.map[cont|cont.getExportedObjectsByType(type)].flatten;
	}

	def getVisibleContainers(EObject from) {
		val index = resourceDescriptionsProvider.getResourceDescriptions(from.eResource);
		val resourceDesc = index.getResourceDescription(from.eResource.URI);
		
		return containerManager.getVisibleContainers(resourceDesc, index);
	}
}
