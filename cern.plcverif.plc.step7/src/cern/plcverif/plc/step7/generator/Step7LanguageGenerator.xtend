/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import cern.plcverif.plc.step7.step7Language.ProgramFile
import org.eclipse.emf.ecore.util.EcoreUtil
import com.google.inject.Inject
import org.eclipse.xtext.validation.IResourceValidator
import org.eclipse.xtext.validation.CheckMode
import org.eclipse.xtext.util.CancelIndicator
import org.eclipse.xtext.validation.Issue
import org.eclipse.xtext.diagnostics.Severity
import cern.plcverif.base.common.emf.textual.EmfModelPrinter
import cern.plcverif.plc.step7.Step7EObjectToText

/**
 * Generates code from your model files on save.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#code-generation
 */
class Step7LanguageGenerator extends AbstractGenerator {
	@Inject IResourceValidator resourceValidator

	static def errorsToString(Iterable<Issue> issues) {
		val errors = issues.filter[it.severity == Severity.ERROR];
		return "Errors:" + System.lineSeparator + errors.map [d |
			'''- «d.message» at «d.lineNumber»:«d.column»«System.lineSeparator»'''
		].join;
	}

	static def warningsToString(Iterable<Issue> issues) {
		val warnings = issues.filter[it.severity == Severity.WARNING];
		return "Warnings:" + System.lineSeparator + warnings.map [d |
			'''- «d.message» at «d.lineNumber»:«d.column»«System.lineSeparator»'''
		].join;
	}

	static def errorsAndWarningsToString(Iterable<Issue> errorsAndWarnings) {
		return errorsToString(errorsAndWarnings) + System.lineSeparator() + warningsToString(errorsAndWarnings);
	}

	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		var CharSequence stringRepresentation = "N/A"
		EcoreUtil.resolveAll(resource);

		val issues = resourceValidator.validate(resource, CheckMode.ALL, CancelIndicator.NullImpl);
		val errors = issues.filter[it.severity == Severity.ERROR];
		if (errors.size > 0) {
			// invalid PLC program (contains errors; prints errors + warnings)
			val errorsAndWarnings = issues.filter[it.severity == Severity.ERROR || it.severity == Severity.WARNING];
			stringRepresentation = errorsAndWarningsToString(errorsAndWarnings);
		} else {
			// valid PLC program (no errors, prints AST)
			val programFile = resource.allContents.filter(typeof(ProgramFile)).head;
			stringRepresentation = EmfModelPrinter.print(programFile, Step7EObjectToText.INSTANCE);
		}

		fsa.generateFile(resource.URI.segmentsList.last.replaceAll("(?i)\\.scl", ".txt").replaceAll("(?i)\\.awl", ".txt"),
			stringRepresentation);
	}
}
