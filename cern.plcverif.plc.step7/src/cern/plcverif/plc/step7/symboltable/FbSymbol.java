/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.symboltable;

/**
 * Class to represent a function block (FB) symbol.
 */
public class FbSymbol extends Symbol {
	/**
	 * Address of the represented FB (FB number)
	 */
	private final int fbNumber;

	/**
	 * Creates a new FB symbol.
	 * 
	 * @param name
	 *            Name of the symbol
	 * @param fbNumber
	 *            Address of the represented FB
	 * @param comment
	 *            Comment attached to the symbol
	 */
	public FbSymbol(String name, int fbNumber, String comment) {
		super(name, comment);
		this.fbNumber = fbNumber;
	}

	/**
	 * Returns the address of the represented FB (FB number)
	 * 
	 * @return FB number
	 */
	public int getFbNumber() {
		return fbNumber;
	}
}
