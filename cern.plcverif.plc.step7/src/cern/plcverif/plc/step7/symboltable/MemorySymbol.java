/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.symboltable;

import com.google.common.base.Preconditions;

import cern.plcverif.plc.step7.datatypes.S7MemoryAddress;

/**
 * Class to represent a memory symbol.
 */
public class MemorySymbol extends Symbol {
	/**
	 * Memory address represented by the symbol.
	 */
	private final S7MemoryAddress address;

	/**
	 * PLC data type of the symbol.
	 */
	private final String type;

	/**
	 * Creates a new memory symbol
	 * 
	 * @param name
	 *            Name of the symbol
	 * @param address
	 *            Represented memory address
	 * @param type
	 *            PLC data type
	 * @param comment
	 *            Comment attached to the symbol
	 */
	public MemorySymbol(String name, String address, String type, String comment) {
		super(name, comment);
		this.address = S7MemoryAddress.create(Preconditions.checkNotNull(address));
		this.type = Preconditions.checkNotNull(type);
	}

	/**
	 * Returns the memory address represented by the symbol.
	 * 
	 * @return Memory address
	 */
	public S7MemoryAddress getAddress() {
		return address;
	}

	/**
	 * Returns PLC data type of the symbol.
	 * 
	 * @return PLC data type
	 */
	public String getType() {
		return type;
	}
}
