/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.symboltable;

import com.google.common.base.Preconditions;

/**
 * Class to represent a generic symbol, i.e., an entry in a symbol table.
 */
public class Symbol {
	/**
	 * Name of the symbol.
	 */
	private final String name;

	/**
	 * Comment attached to the symbol.
	 */
	private final String comment;

	/**
	 * Creates a new symbol.
	 * 
	 * @param name
	 *            Name of the symbol
	 * @param comment
	 *            Comment attached to the symbol
	 */
	public Symbol(String name, String comment) {
		this.name = Preconditions.checkNotNull(name);
		this.comment = Preconditions.checkNotNull(comment);
	}

	/**
	 * Returns the name of the symbol
	 * 
	 * @return Symbol name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the comment attached to the symbol.
	 * 
	 * @return Comment of the symbol
	 */
	public String getComment() {
		return comment;
	}

}