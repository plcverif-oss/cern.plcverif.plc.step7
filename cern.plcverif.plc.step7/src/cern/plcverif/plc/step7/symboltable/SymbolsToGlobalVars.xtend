/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 * 
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.symboltable

import java.util.List
import cern.plcverif.plc.step7.datatypes.S7AddressMemoryIdentifier
import java.util.Map

/**
 * Utility class to represent a symbol tables as global variable declarations.
 */
class SymbolsToGlobalVars {
	private new() {
		// Utility class.
	}

	/**
	 * Represents the given list of symbols as global variable and instance data block representations.
	 * @return SCL representation of the given symbols (where adequate). Never {@code null}.
	 */
	def static represent(List<Symbol> symbols) {
		val inputs = symbols.filter(MemorySymbol).filter[it|it.address.memoryIdentifier == S7AddressMemoryIdentifier.I].
			toList;
		val inputBlock = varBlock(inputs, "VAR_GLOBAL_INPUT", "END_VAR");

		val outputs = symbols.filter(MemorySymbol).
			filter[it|it.address.memoryIdentifier == S7AddressMemoryIdentifier.Q].toList;
		val outputBlock = varBlock(outputs, "VAR_GLOBAL_OUTPUT", "END_VAR");

		val statics = symbols.filter(MemorySymbol).
			filter[it|it.address.memoryIdentifier == S7AddressMemoryIdentifier.M].toList;
		val staticBlock = varBlock(statics, "VAR_GLOBAL", "END_VAR");

		val Map<Integer, FbSymbol> fbLookup = symbols.filter(FbSymbol).toMap([it|it.fbNumber], [it|it]);
		val instanceDbs = symbols.filter(InstanceSymbol).toList;
		val instanceDbBlock = instanceDbs(instanceDbs, fbLookup);

		return '''
			«inputBlock»
			
			«outputBlock»
			
			«staticBlock»
			
			«instanceDbBlock»
		'''
	}

	/**
	 * Represents the given memory symbols as a variable block, starting and ending with the given strings.
	 * Typically opening is {@code VAR_GLOBAL} or similar, closing is {@code END_VAR} or similar.
	 */
	private def static varBlock(List<MemorySymbol> symbols, String opening, String closing) '''
		«IF !symbols.isEmpty»
			«opening»
				«FOR symbol : symbols»
					«symbol.name» : «symbol.type»; // «symbol.comment» («symbol.address.originalStringRepresentation.replaceAll("\\s", "")»)
				«ENDFOR»
			«closing»
		«ENDIF»
	'''

	/**
	 * Represents the given instance data block symbols as instance data block declarations.
	 * The given FB lookup will be used to determine which function block is instantiated by
	 * each instance data block symbol.
	 */
	private def static instanceDbs(List<InstanceSymbol> symbols, Map<Integer, FbSymbol> fbLookup) '''
		«IF !symbols.isEmpty»
			«FOR symbol : symbols»
				DATA_BLOCK «symbol.name» «fbLookup.get(symbol.fbNumber)?.name» BEGIN END_DATA_BLOCK // «symbol.comment» («symbol.address.replaceAll("\\s", "")»)
			«ENDFOR»
		«ENDIF»
	'''
}
