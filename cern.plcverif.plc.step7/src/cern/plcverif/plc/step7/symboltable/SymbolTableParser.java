/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.symboltable;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.io.Files;

/**
 * Utility class to parse symbol tables exported in SDF format.
 */
public final class SymbolTableParser {
	public enum PortalVersion {
		STEP7, TIA
	}
	
	
	private SymbolTableParser() {
		// Utility class.
	}

	/**
	 * Parses the given file containing an SDF-formatted export of a symbol
	 * table.
	 * 
	 * @param file
	 *            Symbol table file (in SDF format) to be parsed
	 * @return List of symbols present in the file.
	 * @throws IOException
	 *             if it is not possible to read the given file
	 */
	public static List<Symbol> parse(File file, PortalVersion version) throws IOException {
		List<String> lines = Files.readLines(file, StandardCharsets.UTF_8);

		List<Symbol> ret = new ArrayList<>();
		for (String line : lines) {
			Optional<Symbol> parsedSymbol = parse(line, version);
			parsedSymbol.ifPresent(ret::add);
		}

		return ret;
	}

	/**
	 * Parses the given line (in SDF format), representing a symbol.
	 * 
	 * @param line
	 *            SDF line to be parsed
	 * @return Symbol object representing the parsed line, if possible. Returns
	 *         {@link Optional#empty()} if it is not possible to parse or
	 *         correctly represent the parsed line. Never {@code null}.
	 */
	public static Optional<Symbol> parse(String line, PortalVersion version) {
		// Very basic line parsing
		Pattern pattern = Pattern
				.compile("'([^']*)'".replace("'", "\"").replace(" ", "\\s*"));
		Matcher matcher = pattern.matcher(line.trim());
		List<String> symbolStrings = new ArrayList<>();
		while (matcher.find()) {
			symbolStrings.add(matcher.group().trim().replaceAll("\"", "").trim());
		}
		
		if (version == PortalVersion.STEP7) {
			if (symbolStrings.size() != 4) return Optional.empty();				
			return parse(symbolStrings.get(0), symbolStrings.get(1), symbolStrings.get(2), symbolStrings.get(3));
		} else if (version == PortalVersion.TIA) {
			if (symbolStrings.size() != 9) return Optional.empty();	
			return parse(symbolStrings.get(0), symbolStrings.get(1), symbolStrings.get(2), symbolStrings.get(6));
		} else {
			throw new UnsupportedOperationException("Unknown version number string supplied through combo list: " + version);
		}
	}

	/**
	 * Creates and returns the appropriate {@link Symbol} representation of a
	 * parsed symbol table line containing the given information.
	 * 
	 * UDT, FC and shared DB symbols are currently not represented. FB, instance
	 * DB and memory symbols are handled here.
	 * 
	 * @param name
	 *            Value of the name field. Shall not be {@code null}.
	 * @param address
	 *            Value of the address field. Shall not be {@code null}.
	 * @param type
	 *            Value of the type field. Shall not be {@code null}.
	 * @param comment
	 *            Value of the comment field. Shall not be {@code null}.
	 * @return Symbol object representing the parsed line, if possible. Returns
	 *         {@link Optional#empty()} if it is not possible to parse or
	 *         correctly represent the parsed line. Never {@code null}.
	 */
	private static Optional<Symbol> parse(String name, String address, String type, String comment) {
		if (address.startsWith("UDT")) {
			// UDT, no need for that for the moment
			return Optional.empty();
		} else if (address.startsWith("FC")) {
			// FC, no need for that for the moment
			return Optional.empty();
		} else if (address.startsWith("FB")) {
			int fbNumber = Integer.parseInt(address.replace("FB", "").trim());
			return Optional.of(new FbSymbol(name, fbNumber, comment));
		} else if (address.startsWith("DB")) {
			if (type.startsWith("FB")) {
				// instance DB
				int fbNumber = Integer.parseInt(type.replace("FB", "").trim());
				return Optional.of(new InstanceSymbol(name, address, fbNumber, comment));
			} else {
				// shared DB, no need for that for the moment
				return Optional.empty();
			}
		} else {
			return Optional.of(new MemorySymbol(name, address, type, comment));
		}
	}
}
