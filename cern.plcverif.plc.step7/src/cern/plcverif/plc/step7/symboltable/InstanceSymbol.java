/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.symboltable;

import com.google.common.base.Preconditions;

/**
 * Class to represent an instance data block (DB) symbol, i.e., an instance of a
 * function block.
 */
public class InstanceSymbol extends Symbol {
	private final String address;
	private final int fbNumber;

	/**
	 * Creates a new instance data block symbol.
	 * 
	 * @param name
	 *            Name of the symbol
	 * @param address
	 *            Address of the data block
	 * @param fbNumber
	 *            Address (FB number) of the function block this data block is
	 *            an instance of
	 * @param comment
	 *            Comment attached to the symbol
	 */
	public InstanceSymbol(String name, String address, int fbNumber, String comment) {
		super(name, comment);
		this.address = Preconditions.checkNotNull(address);
		this.fbNumber = fbNumber;
	}

	/**
	 * Returns the address of the represented data block
	 * 
	 * @return Data block address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Returns the address (FB number) of the function block this data block is
	 * an instance of
	 * 
	 * @return FB number
	 */
	public int getFbNumber() {
		return fbNumber;
	}
}
