/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 * 
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.validation

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.plc.step7.step7Language.CounterConstant
import cern.plcverif.plc.step7.step7Language.DbFieldAddressRef
import cern.plcverif.plc.step7.step7Language.DbFieldIndexedRef
import cern.plcverif.plc.step7.step7Language.EnCallParameterItem
import cern.plcverif.plc.step7.step7Language.EnoFlag
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit
import cern.plcverif.plc.step7.step7Language.NilValue
import cern.plcverif.plc.step7.step7Language.OkValRef
import cern.plcverif.plc.step7.step7Language.ParameterDT
import cern.plcverif.plc.step7.step7Language.ParameterTypeEnum
import cern.plcverif.plc.step7.step7Language.StlAccuArglessStatement
import cern.plcverif.plc.step7.step7Language.StlArithmeticStatement
import cern.plcverif.plc.step7.step7Language.StlComparisonDataType
import cern.plcverif.plc.step7.step7Language.StlComparisonStatement
import cern.plcverif.plc.step7.step7Language.StlConversionMnemonic
import cern.plcverif.plc.step7.step7Language.StlConversionStatement
import cern.plcverif.plc.step7.step7Language.StlFloatMathFunction
import cern.plcverif.plc.step7.step7Language.StlLoadStatement
import cern.plcverif.plc.step7.step7Language.StlLoadStwStatement
import cern.plcverif.plc.step7.step7Language.StlMcrStatement
import cern.plcverif.plc.step7.step7Language.StlTransferStwStatement
import cern.plcverif.plc.step7.step7Language.StringConstant
import cern.plcverif.plc.step7.step7Language.StringDT
import cern.plcverif.plc.step7.step7Language.TimerConstant
import cern.plcverif.plc.step7.typecomputer.Step7CachedTypeProvider
import cern.plcverif.plc.step7.util.DataTypeUtil
import com.google.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.validation.Check

import static cern.plcverif.plc.step7.step7Language.StlConversionMnemonic.*
import static cern.plcverif.plc.step7.validation.Step7LanguageValidator.UNSUPPORTED_IN_PLCVERIF

/**
 * Validator to show the features that are not supported by PLCverif.
 */
class UnsupportedFeatureValidator extends AbstractStep7ComposedValidator {
	@Inject
	Step7CachedTypeProvider typeProvider;

	// Unsupported data types
	@Check
	def void checkStringDt(StringDT e) {
		if (!inBuiltin(e)) {
			error('''STRING data type is not supported by PLCverif.''', e, null, UNSUPPORTED_IN_PLCVERIF);
		}
	}

	@Check
	def void checkStringDt(StringConstant e) {
		error('''STRING data type is not supported by PLCverif.''', e, null, UNSUPPORTED_IN_PLCVERIF);
	}

	@Check
	def void checkParamDt(ParameterDT e) {
		if (e.type === ParameterTypeEnum.TIMER || e.type === ParameterTypeEnum.COUNTER) {
			warning('''«e.type.toString» data type is not supported by PLCverif.''', e, null, UNSUPPORTED_IN_PLCVERIF);
		} else {
			if (!inBuiltin(e)) {
				error('''«e.type.toString» data type is not supported by PLCverif.''', e, null, UNSUPPORTED_IN_PLCVERIF);
			}
		}
	}

	// Timers and counters are not supported yet
	@Check
	def void checkTimers(TimerConstant e) {
		warning('''Non-IEC timer handling is not supported yet in PLCverif.''', e, null, UNSUPPORTED_IN_PLCVERIF);
	}

	@Check
	def void checkCounters(CounterConstant e) {
		warning('''Counter handling is not supported yet in PLCverif.''', e, null, UNSUPPORTED_IN_PLCVERIF);
	}

	// Pointer support
	@Check
	def void checkPointers(NilValue e) {
		error('''Pointer handling is not supported in PLCverif.''', e, null, UNSUPPORTED_IN_PLCVERIF);
	}

	@Check
	def void checkDbAddresses(DbFieldAddressRef e) {
		error('''Only symbolic data block addressing is supported in PLCverif.''', e, null, UNSUPPORTED_IN_PLCVERIF);
	}

	@Check
	def void checkDbAddresses(DbFieldIndexedRef e) {
		error('''Only symbolic data block addressing is supported in PLCverif.''', e, null, UNSUPPORTED_IN_PLCVERIF);
	}

	// Floats in STL
	@Check
	def void checkFloatsInStl(StlLoadStatement e) {
		// Warning on loading REALs to ACCU
		val computedType = typeProvider.getTypeDescriptorFor(e.arg);
		
		if (DataTypeUtil.isReal(computedType.nominalDT)) {
			warning('''Floating point number handling of the ACCU is not supported for STL in PLCverif.''', e, null,
				UNSUPPORTED_IN_PLCVERIF);
		}
	}
	
	@Check
	def void checkFloatsInStl(StlComparisonStatement e) {
		if (e.dataType === StlComparisonDataType.REAL) {
			warning('''Floating point number handling of the ACCU is not supported for STL in PLCverif.''', e, null,
				UNSUPPORTED_IN_PLCVERIF);
		}
	}

	@Check
	def void checkFloatsInStl(StlConversionStatement e) {
		if (e.mnemonic == DINT_TO_REAL || e.mnemonic == REAL_NEGATE || e.mnemonic == RND_MINUS ||
			e.mnemonic == RND_PLUS) {
			warning('''Floating point number handling of the ACCU is not supported for STL in PLCverif.''', e, null,
				UNSUPPORTED_IN_PLCVERIF);
		}
	}

	@Check
	def void checkFloatsInStl(StlArithmeticStatement e) {
		if (e.dataType === StlComparisonDataType.REAL) {
			warning('''Floating point number handling of the ACCU is not supported for STL in PLCverif.''', e, null,
				UNSUPPORTED_IN_PLCVERIF);
		}
	}
	
	@Check
	def void checkFloatsInStl(StlFloatMathFunction e) {
		warning('''Floating point number handling of the ACCU is not supported for STL in PLCverif.''', e, null,
			UNSUPPORTED_IN_PLCVERIF);
	}

	// Etc
	@Check
	def void checkFlags(EnCallParameterItem e) {
		warning('''The EN flag is not supported yet in PLCverif. It will not influence the execution of the called block.''', e, null, UNSUPPORTED_IN_PLCVERIF);
	}

	@Check
	def void checkFlags(EnoFlag e) {
		error('''The ENO flag is not supported yet in PLCverif.''', e, null, UNSUPPORTED_IN_PLCVERIF);
	}

	@Check
	def void checkFlags(OkValRef e) {
		error('''The OK flag is not supported yet in PLCverif.''', e, null, UNSUPPORTED_IN_PLCVERIF);
	}

	@Check
	def void checkUnsupportedStlFeature(StlMcrStatement e) {
		unsupportedStlFeature(e, e.mnemonic.toString.toUpperCase);
	}
	
	@Check
	def void checkUnsupportedStlFeature(StlAccuArglessStatement e) {
		unsupportedStlFeature(e, e.mnemonic.toString.toUpperCase);
	}
	
	@Check
	def void checkUnsupportedStlFeature(StlLoadStwStatement e) {
		unsupportedStlFeature(e, "L STW");
	}
	
	@Check
	def void checkUnsupportedStlFeature(StlTransferStwStatement e) {
		unsupportedStlFeature(e, "T STW");
	}
	
	
	@Check
	def void checkUnsupportedStlFeature(StlConversionStatement e) {
		switch (e.mnemonic) {
			case StlConversionMnemonic.BCD_TO_INT,
			case StlConversionMnemonic.INT_TO_BCD,
			case StlConversionMnemonic.BCD_TO_DINT,
			case StlConversionMnemonic.DINT_TO_BCD,
			case StlConversionMnemonic.CAW,
			case StlConversionMnemonic.CAD:
				unsupportedStlFeature(e, e.mnemonic.literal)
			case StlConversionMnemonic.REAL_NEGATE,
			case StlConversionMnemonic.DINT_TO_REAL,
			case StlConversionMnemonic.RND_MINUS,
			case StlConversionMnemonic.RND_PLUS,
			case StlConversionMnemonic.ROUND,
			case StlConversionMnemonic.TRUNCATE:
				unsupportedStlFeature(e, e.mnemonic.literal)
			default:
				return
		}
	}
	
	private def unsupportedStlFeature(EObject statement, String statementName) {
		warning('''The STL statement '«statementName»' is not supported in PLCverif.''', statement, null, UNSUPPORTED_IN_PLCVERIF);
	}
	
	
	private static def boolean inBuiltin(EObject e) {
		val parentUnit = EmfHelper.getContainerOfType(e, ExecutableProgramUnit);
		
		return parentUnit !== null && parentUnit.isBuiltIn;
	}
}
