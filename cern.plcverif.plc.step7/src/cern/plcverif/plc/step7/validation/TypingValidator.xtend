/******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 * 	 Jean-Charles Tournier - Nov 2019 - Add support for DirectBitAccessRef
 *****************************************************************************/
package cern.plcverif.plc.step7.validation

import cern.plcverif.plc.step7.datatypes.S7Integer
import cern.plcverif.plc.step7.datatypes.S7IntegerType
import cern.plcverif.plc.step7.datatypes.S7TimePeriod
import cern.plcverif.plc.step7.datatypes.S7TimePeriodType
import cern.plcverif.plc.step7.step7Language.ArrayDT
import cern.plcverif.plc.step7.step7Language.ArrayRef
import cern.plcverif.plc.step7.step7Language.BoolConstant
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.ElementaryDT
import cern.plcverif.plc.step7.step7Language.Expression
import cern.plcverif.plc.step7.step7Language.IntConstant
import cern.plcverif.plc.step7.step7Language.NamedConstantDeclaration
import cern.plcverif.plc.step7.step7Language.QualifiedRef
import cern.plcverif.plc.step7.step7Language.RealConstant
import cern.plcverif.plc.step7.step7Language.SclForStatement
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall
import cern.plcverif.plc.step7.step7Language.Step7LanguagePackage
import cern.plcverif.plc.step7.step7Language.UnaryExpression
import cern.plcverif.plc.step7.step7Language.UnaryOperator
import cern.plcverif.plc.step7.step7Language.UnnamedConstant
import cern.plcverif.plc.step7.typecomputer.Step7CachedTypeProvider
import cern.plcverif.plc.step7.util.DataTypeUtil
import com.google.inject.Inject
import org.eclipse.xtext.validation.Check

import static cern.plcverif.plc.step7.util.Step7LanguageHelper.*
import static cern.plcverif.plc.step7.validation.Step7LanguageValidator.*

import static extension cern.plcverif.plc.step7.util.DataTypeUtil.*

class TypingValidator extends AbstractStep7ComposedValidator {
	@Inject
	Step7CachedTypeProvider typeProvider;

	@Check(NORMAL)
	def checkConstantRanges(UnnamedConstant e) {
		val computedType = typeProvider.getTypeDescriptorFor(e);
		if (computedType.isValid) {

			if (unnamedConstantValue(e) instanceof S7Integer) {
				// check against the expected type if untyped INT
				val intConst = unnamedConstantValue(e) as S7Integer;

				if (intConst.type == S7IntegerType.Unknown) {
					if (DataTypeUtil.isInteger(computedType.expectedDT) &&
						computedType.expectedDT instanceof ElementaryDT && !intConst.isWithinRange(
							DataTypeUtil.elementaryDTtoIntegerDT((computedType.expectedDT as ElementaryDT).type))) {
						errorMessageForOutOfRangeConstant(e);
					}

					// don't try anything else for untyped integer constants
					return;
				}
			}
		}

		if (!unnamedConstantValue(e).isWithinRange) {
			errorMessageForOutOfRangeConstant(e);
		}

		// special cases
		// S5TIME (unsafe TIME->S5TIME implicit conversion)
		if (computedType.expectedDT.equalsS5Time && computedType.nominalDT.equalsTime &&
			unnamedConstantValue(e) instanceof S7TimePeriod) {
			val timeConst = unnamedConstantValue(e) as S7TimePeriod;
			if (!timeConst.isWithinRange(S7TimePeriodType.S5TIME)) {
				errorMessageForOutOfRangeConstant(e);
			}
		}
	}

	private def errorMessageForOutOfRangeConstant(UnnamedConstant e) {
		error('''«unnamedConstantValue(e).originalStringRepresentation» is not within its permitted value range.''', e,
			null, OUT_OF_RANGE_CONSTANT);
	}

	@Check(NORMAL)
	def checkBooleanExpressionType(Expression expr) {
		val typeDescriptor = typeProvider.getTypeDescriptorFor(expr);
		val expectedType = typeDescriptor.expectedDT;
		if (DataTypeUtil.isBooleanElementaryType(expectedType)) {
			if (typeDescriptor.isNominalTypeKnown) {
				val nominalType = typeDescriptor.nominalDT;
				if (!DataTypeUtil.isBooleanElementaryType(nominalType)) {
					warning('Boolean type expected. 1', expr, null, INVALID_BOOLEAN_EXPRESSION);
				}
			} else if (expectedType === null || !expectedType.isBooleanElementaryType){
				// maybe 0 or 1 untyped integer
				if (!isIntConstantRepresentingBool(expr)) {
					warning('Boolean type expected. 2', expr, null, INVALID_BOOLEAN_EXPRESSION);
				}
			}
		}
	}

	private def boolean isIntConstantRepresentingBool(Expression expr) {
		if (expr instanceof IntConstant) {
			if (expr.value.isSyntacticallyValid && expr.value.type == S7IntegerType.Unknown) {
				if (expr.value.intValue == 0 || expr.value.intValue == 1)
					return true;
			}
		} else if (expr instanceof DirectNamedRef) {
			// could also be a named constant with value 0 or 1
			if (expr.ref instanceof NamedConstantDeclaration) {
				return isIntConstantRepresentingBool((expr.ref as NamedConstantDeclaration).value);
			}
		}
		return false;
	}

	@Check(NORMAL)
	def checkExpressionType(Expression expr) {
		if (expr.eContainer instanceof NamedConstantDeclaration) {
			// it is normal not to have a type for named constants
			return;
		}

		val computedType = typeProvider.getTypeDescriptorFor(expr);
		if (computedType.isExpectedTypeUnknown && !computedType.isNominalTypeKnown) {
			if (expr instanceof SclSubroutineCall) {
				println("Strange SclSubroutineCall without nominal type: " + expr.class.name)
			}
			info('''Undefined type for «expr.toDisplayString»''', expr, null, UNDEFINED_TYPE);
		} else if (!computedType.isExpectedTypeUnknown && !computedType.isValid) {
			error('''Invalid type conversion: «DataTypeUtil.toString(computedType.nominalDT)» does not match the expected «DataTypeUtil.toString(computedType.expectedDT)» type. «computedType.additionalValidityMessage»''',
				expr, null, INVALID_EXPRESSION_TYPE);
		}
		
//		if (computedType.expectedDT.isAny) {
//			warning('''ANY as expected type''', expr, null, INVALID_EXPRESSION_TYPE);
//		}

		if (computedType.expectedDT.equalsS5Time && expr.eContainer instanceof Expression) {
			// no arithmetic operation is permitted for S5TIME, except for the unary +
			if (!(expr.eContainer instanceof UnaryExpression && (expr.eContainer as UnaryExpression).op == UnaryOperator.PLUS)) {
				warning('No arithmetic operation can be performed on S5TIME values.', expr, null,
					FORBIDDEN_S5TIME_ARITHMETICS);
			}
		}
		
	}
	
	private def String toDisplayString(Expression expr) {
		switch (expr) {
			DirectNamedRef: return expr.ref.name
			IntConstant: return '''«expr.value.originalStringRepresentation»'''
			BoolConstant: return '''«expr.value.originalStringRepresentation»'''
			RealConstant: return '''«expr.value.originalStringRepresentation»'''
			QualifiedRef: return '''«expr.prefix.toDisplayString».«expr.ref.toDisplayString»'''
			default: return expr.toString 
		}
	}

	@Check(NORMAL)
	def checkForLoopVarType(SclForStatement e) {
		// 'initialStatement's 'leftValue' shall be a variable of type INT or DINT (p. 12-18)
		val loopVar = e.initialStatement.leftValue;
		val loopVarType = typeProvider.getTypeDescriptorFor(loopVar);
		if (!loopVarType.nominalDT.isInteger) {
			error('''The loop variable is not an integer variable.''', e.initialStatement,
				Step7LanguagePackage.eINSTANCE.sclAssignmentStatement_LeftValue, INVALID_FOR_STATEMENT);
		}
	}
	
	@Check(NORMAL)
	def checkArryRefIndexValidity(ArrayRef e) {
		val arrayVarType = typeProvider.getTypeDescriptorFor(e.ref);
		if (!arrayVarType.nominalTypeKnown) {
			System.err.println("Unknown nominal type at checkArrayRefIndexValidity. Strange.");
			return;
		}
		val nominalDt = arrayVarType.nominalDT;
		if (!nominalDt.isArrayType) {
			// It should be marked as invalid by other validation rules.
			return;
		}
		
		val arrayDt = nominalDt as ArrayDT;
		if (e.index.size > arrayDt.dimensions.size) {
			error('''The referred array is only «arrayDt.dimensions.size»-dimensional, but the reference contains «e.index.size» dimensions.''',
				e, null, INVALID_ARRAYREF_INDEX);
		}
	}
	
}
