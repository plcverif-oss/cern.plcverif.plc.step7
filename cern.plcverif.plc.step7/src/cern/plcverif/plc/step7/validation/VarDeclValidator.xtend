/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 * 
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.validation

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.plc.step7.step7Language.ConstantInitializationElement
import cern.plcverif.plc.step7.step7Language.ElementaryDT
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit
import cern.plcverif.plc.step7.step7Language.FbOrUdtDT
import cern.plcverif.plc.step7.step7Language.FunctionBlock
import cern.plcverif.plc.step7.step7Language.ParameterDT
import cern.plcverif.plc.step7.step7Language.ProgramUnit
import cern.plcverif.plc.step7.step7Language.Step7LanguagePackage
import cern.plcverif.plc.step7.step7Language.Variable
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock
import cern.plcverif.plc.step7.step7Language.VariableDeclarationDirection
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine
import cern.plcverif.plc.step7.step7Language.VariableInitialization
import cern.plcverif.plc.step7.util.DataTypeUtil
import com.google.common.base.Preconditions
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.validation.Check

import static cern.plcverif.plc.step7.validation.Step7LanguageValidator.*

import static extension cern.plcverif.plc.step7.util.DataTypeUtil.*
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import cern.plcverif.plc.step7.util.VariableViewUtil
import cern.plcverif.plc.step7.step7Language.StructDT
import cern.plcverif.plc.step7.step7Language.DataBlock
import cern.plcverif.plc.step7.step7Language.UserDefinedDataType

class VarDeclValidator extends AbstractStep7ComposedValidator {
	static final Logger log = LogManager.getLogger(VarDeclValidator);

	@Check(NORMAL)
	def checkStructVariableValidity(VariableDeclarationLine varDeclLine) {
		if (varDeclLine.eContainer instanceof StructDT && varDeclLine.variables.size > 1 &&
			EmfHelper.getContainerOfType(varDeclLine, ExecutableProgramUnit) !== null) {
			error('''It is forbidden to define multiple variables in one line inside a STRUCT definition, if it is within an executeble program unit's variable declaration block.''',
				varDeclLine, null, INVALID_STRUCT_VARIABLE);
		}
	}

	@Check(NORMAL)
	def checkVariableInitializationBracketBalance(VariableInitialization varInit) {
		// The variable initialization has brackets around the values optionally. However, it shall not be unbalanced.
		val plcText = NodeModelUtils.getNode(varInit).text.trim;

		val leftBracketPresent = plcText.startsWith("[");
		val rightBracketPresent = plcText.endsWith("]");

		if ((leftBracketPresent && !rightBracketPresent) || (!leftBracketPresent && rightBracketPresent)) {
			error('''Unbalanced brackets in variable initialization. («plcText»)''', varInit, null,
				UNBALANCED_VAR_INIT_BRACKETS);
		}
	}

	@Check
	def void checkInvalidLocalFbInstance(VariableDeclarationLine declarationLine) {
		if (declarationLine.type instanceof FbOrUdtDT) {
			if ((declarationLine.type as FbOrUdtDT).type instanceof FunctionBlock) {
				// local instance declaration
				Preconditions.checkNotNull(declarationLine.eContainer);
				Preconditions.checkState(declarationLine.eContainer instanceof VariableDeclarationBlock);

				val parentVarBlock = declarationLine.eContainer as VariableDeclarationBlock;

				if (declarationLine.isInitialized) {
					error("Local FB instances must not be initialized.", declarationLine,
						Step7LanguagePackage.eINSTANCE.variableDeclarationLine_Initialization,
						INVALID_LOCAL_FBINSTANCE);
					}

					if (parentVarBlock.direction != VariableDeclarationDirection.STATIC) {
						error("Local FB instances can only be declared in VAR blocks.", declarationLine,
							Step7LanguagePackage.eINSTANCE.variableDeclarationLine_Type, INVALID_LOCAL_FBINSTANCE);
					}

					val parentProgramUnit = EmfHelper.getContainerOfType(parentVarBlock, ExecutableProgramUnit)
					if (parentProgramUnit === null || !Step7LanguageHelper.isFunctionBlock(parentProgramUnit)) {
						error("Local FB instances can only be declared in function blocks.", declarationLine,
							Step7LanguagePackage.eINSTANCE.variableDeclarationLine_Type, INVALID_LOCAL_FBINSTANCE);
					}
				}
			}
		}

	@Check
	def checkInvalidVariableInitialization(VariableDeclarationLine declarationLine) {
		Preconditions.checkNotNull(declarationLine);

		if (declarationLine.isInitialized) {
			Preconditions.checkNotNull(declarationLine.eContainer);
			val parentVarBlock = EcoreUtil2.getContainerOfType(declarationLine, VariableDeclarationBlock);
			if (parentVarBlock === null) {
				log.trace("parentVarBlock == null in checkInvalidVariableInitialization");
				return;
			}

			if (parentVarBlock.direction == VariableDeclarationDirection.INOUT) {
				error("Initialization of variables is forbidden in a VAR_IN_OUT block.", declarationLine,
					Step7LanguagePackage.eINSTANCE.variableDeclarationLine_Initialization,
					FORBIDDEN_VARIABLE_INITIALIZATION);
				}

				if (parentVarBlock.direction == VariableDeclarationDirection.TEMP) {
					error("Initialization of variables is forbidden in a VAR_TEMP block.", declarationLine,
						Step7LanguagePackage.eINSTANCE.variableDeclarationLine_Initialization,
						FORBIDDEN_VARIABLE_INITIALIZATION);
					}

					if (declarationLine.variables.size > 1) {
						error(
							"Initialization of variables is forbidden if there are multiple variables defined in the same line.",
							declarationLine, Step7LanguagePackage.eINSTANCE.variableDeclarationLine_Initialization,
							FORBIDDEN_VARIABLE_INITIALIZATION);
						} else if (declarationLine.variables.filter[v|v.isReference].size > 0) {
							// initialization is forbidden for reference variables
							error("Initialization is forbidden for reference variables ('AT').", declarationLine,
								Step7LanguagePackage.eINSTANCE.variableDeclarationLine_Initialization,
								FORBIDDEN_VARIABLE_INITIALIZATION);
							}
						}
					}

	@Check
	def checkAnyNumOnlyInBuiltin(ParameterDT e) {
		if (e.isAnyNum) {
			// check where is it used
			val containingProgramUnit = EmfHelper.getContainerOfType(e, ProgramUnit);
			if (containingProgramUnit === null) {
				println("Strange.");
				return;
			}
			if (containingProgramUnit instanceof ExecutableProgramUnit) {
				if (!containingProgramUnit.isBuiltIn) {
					error("Usage of 'ANY_NUM' data type is only permitted in built-in functions.",
						e, null, INVALID_ANY_NUM);
				}
			}
		}
	}

	@Check(NORMAL)
	def checkTransitiveView(Variable v) {
		if (v.isReference) {
			val referredVar = v.ref;
			if (referredVar.isReference) {
				error('''Transitive views are not permitted. Variable '«v.name»' is a view at '«referredVar.name»' that is already a variable view itself.''',
					v, Step7LanguagePackage.eINSTANCE.variable_Ref, FORBIDDEN_TRANSITIVE_VIEW);
			}
		}
	}

	@Check
	def checkViewTypeCompatibility(Variable v) {
		if (v.isReference) {
			val referredVar = v.ref;
			val diagnosis = VariableViewUtil.diagnoseIncompatibility(v, referredVar);
			if (diagnosis.isPresent) {
				error('''Incompatibly variable views. «diagnosis.get»''', v,
					Step7LanguagePackage.eINSTANCE.variable_Ref, INCOMPATIBLE_VAR_VIEWS);
			}
		}
	}
	
	/**
	 * Raises ERROR (INVALID_VAR_VIEW) if a variable is a variable view (has 'AT'), 
	 * but is defined within a data block or UDT.
	 */
	@Check
	def checkViewContainer(Variable v) {
		if (v.isReference && EmfHelper.isContainedInAny(v, DataBlock, UserDefinedDataType)) {
			error(
				'''Variable views ('AT') are not permitted in DATA_BLOCK and TYPE (UDT) definitions.''',
				v,
				Step7LanguagePackage.eINSTANCE.variable_Ref,
				INVALID_VAR_VIEW
			);
		}
	} 
						

	/** 
	 * Raises ERROR (INVALID_VAR_INIT_TYPE) if the type of the variable initialization
	 * does not match the type of the variable:
	 * <ul>
	 * <li>An elementary variable has initialization other than a single {@link ConstantInitializationElement}.
	 * </ul>
	 */
	@Check
	def checkVarInitType(VariableDeclarationLine e) {
		if (DataTypeUtil.isElementaryType(e.type)) {
			if (e.isInitialized && !(e.initialization instanceof ConstantInitializationElement)) {
				val typeName = (e.type as ElementaryDT).type.literal;
				error(
					'''A variable with type «typeName» needs to be initialized with a single value.''',
					e.initialization,
					null,
					INVALID_VAR_INIT_TYPE
				);
			}
		}
	}
}
