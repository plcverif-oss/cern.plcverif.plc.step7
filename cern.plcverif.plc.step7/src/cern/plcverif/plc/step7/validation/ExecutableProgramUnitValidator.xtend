/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.validation

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.plc.step7.step7Language.DeclarationSection
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.OrganizationBlock
import cern.plcverif.plc.step7.step7Language.ProgramUnit
import cern.plcverif.plc.step7.step7Language.SclAssignmentStatement
import cern.plcverif.plc.step7.step7Language.Step7LanguagePackage
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock
import cern.plcverif.plc.step7.step7Language.VariableDeclarationDirection
import cern.plcverif.plc.step7.util.DataTypeUtil
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import com.google.common.base.Preconditions
import org.eclipse.xtext.validation.Check

import static cern.plcverif.plc.step7.validation.Step7LanguageValidator.*

class ExecutableProgramUnitValidator extends AbstractStep7ComposedValidator {
	@Check
	def checkInvalidDeclarationSections(VariableDeclarationBlock varBlock) {
		Preconditions.checkNotNull(varBlock);
		Preconditions.checkNotNull(varBlock.eContainer);
		Preconditions.checkState(varBlock.eContainer instanceof DeclarationSection);
		val containerBlock = varBlock.eContainer.eContainer;

		if (containerBlock !== null && containerBlock instanceof ProgramUnit) {
			val containerProgramUnit = containerBlock as ProgramUnit;

			// support matrix:
			// ..............OB...FC...FB 
			// VAR_INPUT          +    +
			// VAR_OUTPUT         +    +
			// VAR_IN_OUT         +    +
			// VAR_TEMP      +    +    +
			// VAR                     +
			// CONST         +    +    + 
			// LABEL         +    +    +
			errorForUnsupportedDeclarationBlock(OrganizationBlock, VariableDeclarationDirection.INPUT,
				containerProgramUnit, varBlock);
			errorForUnsupportedDeclarationBlock(OrganizationBlock, VariableDeclarationDirection.OUTPUT,
				containerProgramUnit, varBlock);
			errorForUnsupportedDeclarationBlock(OrganizationBlock, VariableDeclarationDirection.INOUT,
				containerProgramUnit, varBlock);
			errorForUnsupportedDeclarationBlock(OrganizationBlock, VariableDeclarationDirection.STATIC,
				containerProgramUnit, varBlock);
//			errorForUnsupportedDeclarationBlock(Function, VariableDeclarationDirection.STATIC, containerProgramUnit,
//				varBlock); // according to the documentation this is forbidden, still it compiles
		}
	}

	private def errorForUnsupportedDeclarationBlock(
		Class<?> programUnitType,
		VariableDeclarationDirection varBlockType,
		ProgramUnit containerProgramUnit,
		VariableDeclarationBlock varBlock
	) {
		if (programUnitType.isInstance(containerProgramUnit) && varBlock.direction == varBlockType) {
			error('''«varBlockType» block is not supported in a(n) «programUnitType.simpleName».''', varBlock, null,
				INVALID_DECLARATION_SECTIONS);
		}
	}

	@Check
	def void checkObImplementationLanguage(OrganizationBlock ob) {
		if (ob.statements !== null && !Step7LanguageHelper.isSclBlock(ob) && !Step7LanguageHelper.isStlBlock(ob)) {
			error('''The implementation language of the organization block '«ob.name»' is unsupported. OBs can only be implemented in SCL or STL.''',
				ob.statements, null, INVALID_STATEMENTLIST_LANGUAGE);
		}
	}
	
	@Check
	def void checkMissingReturnValue(Function function) {
		if (Step7LanguageHelper.isSclBlock(function) && !DataTypeUtil.isVoid(function.returnType)) {
			// We care only about SCL functions that are not VOID
			val returnValAmts = EmfHelper.getAllContentsOfType(
				function.statements,
				SclAssignmentStatement,
				false,
				[it|it.leftValue instanceof DirectNamedRef && (it.leftValue as DirectNamedRef).ref == function]
			);

			if (returnValAmts.isEmpty) {
				error(
					'''The function «function.name» does not set any return value. '«function.name» := ...;' assignment is missing.''',
					function,
					Step7LanguagePackage.eINSTANCE.function_ReturnType,
					MISSING_RETURN_VALUE
				);
			}
		}
	}
}
