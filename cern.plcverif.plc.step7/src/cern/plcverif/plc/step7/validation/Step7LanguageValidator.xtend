/******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.validation

import org.eclipse.xtext.validation.ComposedChecks

/**
 * This class contains custom validation rules. 
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
@ComposedChecks(validators=#[ExecutableProgramUnitValidator, VarDeclValidator, NamingValidator, StatementValidator,
	ExpressionValidator, DataTypeValidator, TypingValidator, TiaValidator, BlockAttributeValidator, CodesmellValidator, 
	SclValidator, StlValidator, UnsupportedFeatureValidator])
class Step7LanguageValidator extends AbstractStep7LanguageValidator {
	public static val CONST_AS_LEFTVALUE = 'CONST_AS_LEFTVALUE' // StatementValidator
	public static val DUPLICATE_LABEL = 'DUPLICATE_LABEL' // StatementValidator
	public static val EMPTY_STRUCT = 'EMPTY_STRUCT' // DataTypeValidator
	public static val FORBIDDEN_S5TIME_ARITHMETICS = 'FORBIDDEN_S5TIME_ARITHMETICS' // TypingValidator
	public static val FORBIDDEN_TRANSITIVE_VIEW = 'FORBIDDEN_TRANSITIVE_VIEW' // VarDeclValidator
	public static val FORBIDDEN_VARIABLE_INITIALIZATION = 'FORBIDDEN_VARIABLE_INITIALIZATION' // VarDeclValidator
	public static val ILLEGAL_SCL_ELEMENT = 'ILLEGAL_SCL_ELEMENT' // SclValidator
	public static val ILLEGAL_STL_ELEMENT = 'ILLEGAL_STL_ELEMENT' // StlValidator
	public static val ILLEGAL_USE_OF_VOID = 'ILLEGAL_USE_OF_VOID' // DataTypeValidator
	public static val INCOMPATIBLE_VAR_VIEWS = 'INCOMPATIBLE_VAR_VIEWS' // VarDeclValidator
	public static val INVALID_ADDRESS = 'INVALID_ADDRESS' // ExpressionValidator
	public static val INVALID_ARRAY_INIT_SIZE = 'INVALID_ARRAY_INIT_SIZE' // DataTypeValidator
	public static val INVALID_ARRAY_RANGE = 'INVALID_ARRAY_RANGE' // DataTypeValidator
	public static val INVALID_ARRAY_TYPE = 'INVALID_ARRAY_TYPE' // DataTypeValidator
	public static val INVALID_ARRAYREF_INDEX  = 'INVALID_ARRAYREF_INDEX' // TypingValidator, ExpressionValidator
	public static val INVALID_ANY_NUM = 'INVALID_ANY_NUM' // ExecutableProgramUnitValidator
	public static val INVALID_ASSERTION = 'INVALID_ASSERTION' // StatementValidator
	public static val INVALID_BLOCK_ATTRIBUTE = 'INVALID_BLOCK_ATTRIBUTE' // BlockAttributeValidator
	public static val INVALID_BLOCK_KEYWORD = 'INVALID_BLOCK_KEYWORD' // NamingValidator
	public static val INVALID_BOOLEAN_EXPRESSION = 'INVALID_BOOLEAN_EXPRESSION' // TypingValidator
	public static val INVALID_CALL_ASSIGNMENT_OPERATOR = 'INVALID_CALL_ASSIGNMENT_OPERATOR' // StatementValidator
	public static val INVALID_CALL_PARAMETERS = 'INVALID_CALL_PARAMETERS' // StatementValidator
	public static val INVALID_CASE_RANGE = 'INVALID_CASE_RANGE' // StatementValidator
	public static val INVALID_DECLARATION_SECTIONS = 'INVALID_DECLARATION_SECTIONS' // ExecutableProgramUnitValidator
	public static val INVALID_DIRECTBITACCESS_TYPE = 'INVALID_DIRECTBITACCESS_TYPE' // DataTypeValidator
	public static val INVALID_ELEMENTARY_TYPE = 'INVALID_ELEMENTARY_TYPE' // TiaValidator
	public static val INVALID_EXPRESSION_TYPE = 'INVALID_EXPRESSION_TYPE' // TypingValidator
	public static val INVALID_FB_CALL_DB = 'INVALID_FB_CALL_DB' // StatementValidator
	public static val INVALID_FB_OUTPUT_ASSIGNMENT = 'INVALID_FB_OUTPUT_ASSIGNMENT' // StatementValidator
	public static val INVALID_FBORUDT_TYPE = 'INVALID_FBORUDT_TYPE' // DataTypeValidator
	public static val INVALID_FOR_STATEMENT = 'INVALID_FOR_STATEMENT' // StatementValidator, TypingValidator
	public static val INVALID_GOTO_STATEMENT = 'INVALID_GOTO_STATEMENT' // StatementValidator
	public static val INVALID_IDENTIFIER = 'INVALID_IDENTIFIER' // NamingValidator
	public static val INVALID_INDEXING = 'INVALID_INDEXING' // ExpressionValidator
	public static val INVALID_LOCAL_FBINSTANCE = 'INVALID_LOCAL_FBINSTANCE' // VarDeclValidator
	public static val INVALID_LOOP_STATEMENT = 'INVALID_LOOP_STATEMENT' // StatementValidator
	public static val INVALID_OUTPUT_PARAMETER = 'INVALID_OUTPUT_PARAMETER' // StatementValidator
	public static val INVALID_PARAMDT_USAGE = 'INVALID_PARAMDT_USAGE' // DataTypeValidator
	public static val INVALID_REFERENCE = 'INVALID_REFERENCE' // ExpressionValidator
	public static val INVALID_RETVAL_ASSIGNMENT = 'INVALID_RETVAL_ASSIGNMENT' // StatementValidator
	public static val INVALID_SIMPLE_EXPRESSION = 'INVALID_SIMPLE_EXPRESSION' // ExpressionValidator
	public static val INVALID_STATEMENTLIST_LANGUAGE = 'INVALID_STATEMENTLIST_LANGUAGE' // ExecutableProgramUnitValidator
	public static val INVALID_STL_ARGUMENT = 'INVALID_STL_ARGUMENT' // StlValidator
	public static val INVALID_STRINGDT_DIMENSION = 'INVALID_STRINGDT_DIMENSION' // DataTypeValidator
	public static val INVALID_STRUCT_VARIABLE = 'INVALID_STRUCT_VARIABLE' // VarDeclValidator
	public static val INVALID_SUBROUTINE_CALL = 'INVALID_SUBROUTINE_CALL' // StatementValidator
	public static val INVALID_TIA_LOCAL_VARIABLE_REF = 'INVALID_TIA_LOCAL_VARIABLE_REF' // TiaValidator
	public static val INVALID_USE_OF_IMPLICATION = 'INVALID_USE_OF_IMPLICATION' // ExpressionValidator
	public static val INVALID_VAR_INIT_TYPE = 'INVALID_VAR_INIT_TYPE' // VarDeclValidator
	public static val INVALID_VAR_VIEW = 'INVALID_VAR_VIEW' // VarDeclValidator
	public static val OUT_OF_RANGE_CONSTANT = 'OUT_OF_RANGE_CONSTANT' // TypingValidator
	public static val MISSING_RETURN_VALUE = 'MISSING_RETURN_VALUE' // ExecutableProgramUnitValidator
	public static val NON_UNIQUE_BLOCK_NAME = 'NON_UNIQUE_BLOCK_NAME' // NamingValidator
	public static val NON_UNIQUE_FIELD_NAME = 'NON_UNIQUE_FIELD_NAME' // NamingValidator
	public static val TOO_MANY_ARRAY_DIMENSIONS = 'TOO_MANY_ARRAY_DIMENSIONS' // DataTypeValidator
	public static val UNBALANCED_VAR_INIT_BRACKETS = 'UNBALANCED_VAR_INIT_BRACKETS' // VarDeclValidator
	public static val UNDEFINED_TYPE = 'UNDEFINED_TYPE' // TypingValidator
	public static val UNPARSABLE_STEP7_CONSTANT = 'UNPARSABLE_STEP7_CONSTANT' // DataTypeValidator
	public static val UNSUPPORTED_IN_PLCVERIF = 'UNSUPPORTED_IN_PLCVERIF'// UnsupportedFeatureValidator
	public static val UNSUPPORTED_STL_STATEMENT = 'UNSUPPORTED_STL_STATEMENT' // StlValidator
	public static val UNUSED_LABEL = 'UNUSED_LABEL' // StatementValidator

	public static val MAX_ARRAY_DIMENSIONS = 6
	public static val MIN_STRING_DT_DIMENSION = 1
	public static val MAX_STRING_DT_DIMENSION = 254

// Only the constants are defined here, the rules are implemented in various Validator classes, referenced in @ComposedChecks

	public static val SKIP_EXPENSIVE_NONFATAL_VALIDATION = 'SKIP_EXPENSIVE_NONFATAL_VALIDATION';
}
