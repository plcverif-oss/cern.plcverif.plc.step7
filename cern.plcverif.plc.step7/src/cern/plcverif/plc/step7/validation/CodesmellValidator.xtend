/******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 * 
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Jean-Charles Tournier - November 2019 - Add support for DirectBitAccessRef
 *****************************************************************************/
package cern.plcverif.plc.step7.validation

import cern.plcverif.plc.step7.step7Language.AndExpression
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.EqualityExpression
import cern.plcverif.plc.step7.step7Language.Expression
import cern.plcverif.plc.step7.step7Language.LeftValue
import cern.plcverif.plc.step7.step7Language.OrExpression
import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.step7Language.SclAssignmentStatement
import cern.plcverif.plc.step7.step7Language.SclCaseStatement
import cern.plcverif.plc.step7.step7Language.SclForStatement
import cern.plcverif.plc.step7.step7Language.SclIfStatement
import cern.plcverif.plc.step7.step7Language.SclRepeatStatement
import cern.plcverif.plc.step7.step7Language.SclStatement
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall
import cern.plcverif.plc.step7.step7Language.SclWhileStatement
import cern.plcverif.plc.step7.step7Language.UnaryExpression
import cern.plcverif.plc.step7.step7Language.UnaryOperator
import cern.plcverif.plc.step7.step7Language.UserDefinedDataType
import cern.plcverif.plc.step7.step7Language.Variable
import cern.plcverif.plc.step7.step7Language.VariableDeclarationDirection
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine
import cern.plcverif.plc.step7.step7Language.XorExpression
import cern.plcverif.plc.step7.typecomputer.Step7CachedTypeProvider
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import cern.plcverif.plc.step7.validation.sa.ExpressionComplexity
import com.google.inject.Inject
import org.eclipse.emf.ecore.util.EcoreUtil.UsageCrossReferencer
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.validation.Check

import static extension cern.plcverif.plc.step7.util.DataTypeUtil.*
import static extension cern.plcverif.plc.step7.util.Step7LanguageHelper.*
import cern.plcverif.plc.step7.step7Language.ProgramUnit
import cern.plcverif.base.common.emf.EmfHelper
import com.google.common.base.Preconditions
import org.eclipse.emf.ecore.EObject
import cern.plcverif.plc.step7.step7Language.StlTransferStatement
import cern.plcverif.plc.step7.step7Language.StlAssignStatement

class CodesmellValidator extends AbstractStep7ComposedValidator {
	val MAXIMUM_ALLOWED_NESTING_LEVELS = 3; // T
	val MAXIMUM_ALLOWED_NEGATION_LEVELS = 3;

	@Inject
	Step7CachedTypeProvider typeProvider;

	@Check
	def checkDirectNamedRefCapitalization(DirectNamedRef e) {
		if (e.ref.name === null) {
			// Nothing to do here.
			return;
		}
		
		val node = NodeModelUtils.getNode(e);
		var tokenText = NodeModelUtils.getTokenText(node);

		// Sanitize token text	
		val usageName = sanitizeVarName(tokenText);
		val definitionName = sanitizeVarName(e.ref.name);

		if (!usageName.equals(definitionName)) {
			info('''Inconsistent name usage: used as '«tokenText»', but defined as '«definitionName»'.''', e, null);
		}
	}
	
	/**
	 * Removes # prefix and quotation marks (if present) from a variable name
	 * and .%Xxx postfix
	 */
	private def String sanitizeVarName(String originalName) {
		var ret = originalName;
		if (ret.startsWith("#")) {
			ret = ret.substring(1);
		}
		if (ret.startsWith("\"") && ret.endsWith("\"") && ret.length > 2) {
			ret = ret.substring(1, ret.length - 1);
		}
		var indexBitAccess = ret.toLowerCase.lastIndexOf(".%x");
		if (indexBitAccess > 0){
			ret = ret.substring( 0, indexBitAccess)
		}
		
		return ret;
	}

	@Check
	def checkExpressionComplexity(SclIfStatement e) {
		checkExpressionComplexityInternal(e.condition, 10);
		if (e.elsifBranches !== null) {
			for (elsifBranch : e.elsifBranches) {
				checkExpressionComplexityInternal(elsifBranch.condition, 8);
			}
		}
	}

	@Check
	def checkExpressionComplexity(SclAssignmentStatement e) {
		checkExpressionComplexityInternal(e.rightValue, 10);
	}

	@Check
	def checkExpressionComplexity(SclWhileStatement e) {
		checkExpressionComplexityInternal(e.entryCondition, 3);
	}

	@Check
	def checkExpressionComplexity(SclRepeatStatement e) {
		checkExpressionComplexityInternal(e.exitCondition, 3);
	}

	@Check
	def checkExpressionComplexity(SclForStatement e) {
		if (e.by) {
			checkExpressionComplexityInternal(e.increment, 2);
		}
		checkExpressionComplexityInternal(e.finalValue, 2);
		checkExpressionComplexityInternal(e.initialStatement.rightValue, 3);
	}

	private def checkExpressionComplexityInternal(Expression rootExpr, int maxComplexity) {
		val complexityScore = ExpressionComplexity.complexityMetric(rootExpr);
		if (complexityScore > maxComplexity) {
			info('''Too complex expression. (Score: «complexityScore», permitted maximum: «maxComplexity»)''', rootExpr,
				null);
		}
	}

	@Check
	def checkParenthesesInNestedLogicExpressions(OrExpression e) {
		for (nestedExpr : #{e.left, e.right}) {
			if (nestedExpr instanceof AndExpression || nestedExpr instanceof XorExpression) {
				val xtextNode = NodeModelUtils.findActualNodeFor(nestedExpr);
				val tokenText = NodeModelUtils.getTokenText(xtextNode);
				if (!isWholeExpressionInParentheses(tokenText.trim())) {
					warning('''Missing parentheses around «IF nestedExpr instanceof AndExpression»AND«ELSE»XOR«ENDIF» expression nested in OR: '«tokenText»'.''',
						nestedExpr, null);
				}
			}
		}
	}

	@Check
	def checkParenthesesInNestedLogicExpressions(XorExpression e) {
		for (nestedExpr : #{e.left, e.right}) {
			if (nestedExpr instanceof AndExpression) {
				val xtextNode = NodeModelUtils.findActualNodeFor(nestedExpr);
				val tokenText = NodeModelUtils.getTokenText(xtextNode);
				if (!isWholeExpressionInParentheses(tokenText.trim())) {
					warning('''Missing parentheses around XOR expression nested in AND: '«tokenText»'.''', nestedExpr,
						null);
				}
			}
		}
	}
	
	private def boolean isWholeExpressionInParentheses(String expr) {
		if (!expr.startsWith("(") || !expr.endsWith(")")) {
			return false;
		}

		val char LEFT_PAR = '(';
		val char RIGHT_PAR = ')';

		var parLevel = 0;
		for (i : 0 .. expr.length - 1) {
			val char currentChar = expr.charAt(i).charValue;
			if (currentChar === LEFT_PAR) {
				parLevel++;
			} else if (currentChar === RIGHT_PAR) {
				parLevel--;
			}

			if (parLevel == 0 && i < expr.length - 1) {
				// we just got out of the scope of the first parenthesis
				return false;
			}
		}

		return (parLevel == 0);
	}

	@Check(NORMAL)
	def checkUnusedVariables(Variable v) {
		if (Boolean.TRUE.equals(context.get(Step7LanguageValidator.SKIP_EXPENSIVE_NONFATAL_VALIDATION))) {
			return;
		}

		// Ignore variables of built-in blocks
		val definingBlock = EcoreUtil2.getContainerOfType(v, ProgramUnit);
		// definingBlock is null for global variables
		
		if (definingBlock !== null && definingBlock.isBuiltInBlock) {
			return;
		}

		if (v.isReference || EcoreUtil2.getContainerOfType(v, VariableDeclarationLine).hasContainingReferenceVariable) {
			// Skip views
			return;
		}

		// Skip storage variables of variable views
		if (definingBlock !== null && isPartOfVariableViewAsStorage(v, definingBlock)) {
			return;
		}

		if (EcoreUtil2.getContainerOfType(v, UserDefinedDataType) !== null) {
			// don't check UDTs
			return;
		}

		// PERF performance can be significantly improved
		val usages = UsageCrossReferencer.find(v, EcoreUtil2.getContainerOfType(v, ProgramFile));
		if (usages.size == 0) {
			warning('Unused variable.', v, null);
		}
	}

	private def boolean isPartOfVariableViewAsStorage(Variable v, ProgramUnit definingBlock) {
		Preconditions.checkNotNull(definingBlock);
		
		// PERF The performance could be improved here, isReferredByAnyRefVariable is rather wasteful
		var currentVarDeclLine = EmfHelper.getContainerOfType(v, VariableDeclarationLine);
		while (currentVarDeclLine !== null) {
			for (Variable v2 : currentVarDeclLine.variables) {
				if (v2.isReferredByAnyRefVariable(definingBlock)) {
					return true;
				}
			}
			currentVarDeclLine = EmfHelper.getContainerOfType(currentVarDeclLine, VariableDeclarationLine);
		}

		return false;
	}

	private def boolean isReferredByAnyRefVariable(Variable v, ProgramUnit definingBlock) {
		return !definingBlock.allDefinedVariables.filter[it|it.isReference && it.ref == v].isEmpty;
	}

	private def boolean hasContainingReferenceVariable(VariableDeclarationLine line) {
		if (line.variables.exists[it|it.isReference]) {
			return true;
		}

		val enclosingDeclarationLine = EcoreUtil2.getContainerOfType(line.eContainer, VariableDeclarationLine);
		if (enclosingDeclarationLine === null) {
			return false;
		} else {
			return hasContainingReferenceVariable(enclosingDeclarationLine);
		}
	}

	@Check
	def checkEqualityCheckOnFloat(EqualityExpression e) {
		val leftType = typeProvider.getTypeDescriptorFor(e.left);
		val rightType = typeProvider.getTypeDescriptorFor(e.right);

		if (leftType.nominalDT.real) {
			warning("Strict equality operation on a floating-point number.", e.left, null);
		} else if (rightType.nominalDT.real) {
			warning("Strict equality operation on a floating-point number.", e.right, null);
		}
	}

	@Check
	def checkNegationCount(LeftValue e) {
		var int count = EmfHelper.countContainersOfTypeRecursively(e, UnaryExpression, [it|it.op == UnaryOperator.NOT]);
		if (count > MAXIMUM_ALLOWED_NEGATION_LEVELS) {
			error('''Too complex Boolean expression: «count» negations for the value «e».''', e, null);
		}
	}

	@Check
	def checkStatementNestingLevel(SclStatement e) {
		if (e instanceof SclSubroutineCall || e instanceof SclAssignmentStatement) {
			return; // experimental
		}

		val int nestingCount = EmfHelper.countContainersOfTypeRecursively(e, SclStatement, [it|it.isRealStatement]);
		if (nestingCount >= MAXIMUM_ALLOWED_NESTING_LEVELS) {
			warning('''Too deeply nested «representingKeyword(e)» statement.''', e, null);
		}
	}

	def String representingKeyword(SclStatement statement) {
		// move to some helper
		switch (statement) {
			SclIfStatement: return 'IF'
			SclCaseStatement: return 'CASE'
			SclRepeatStatement: return 'REPEAT'
			SclForStatement: return "FOR"
			SclWhileStatement: return "WHILE"
			SclAssignmentStatement: return "assignment"
			SclSubroutineCall: return "CALL"
		}

		return statement.class.name;
	}

	@Check
	def checkWritingInputVariables(SclAssignmentStatement e) {
		if (e.leftValue === null) {
			// Partial or invalid AST.
			return;
		}
		
		val writtenVar = Step7LanguageHelper.getReferredVariable(e.leftValue);
		if (writtenVar.isPresent && isVarInputInContext(writtenVar.get, e)) {
			warning('''The input variable '«writtenVar.get.name»' should not be written in its declaring block.''',
					e.leftValue, null);
		}
	}
	
	@Check
	def checkWritingInputVariables(StlTransferStatement e) {
		if (e.arg === null) {
			// Partial or invalid AST.
			return;
		}
		
		if (e.arg instanceof LeftValue) {
			val writtenVar = Step7LanguageHelper.getReferredVariable(e.arg as LeftValue);
			if (writtenVar.isPresent && isVarInputInContext(writtenVar.get, e)) {
				warning('''The input variable '«writtenVar.get.name»' should not be written in its declaring block.''',
						e.arg, null);
			}
		}
	}
	
	@Check
	def checkWritingInputVariables(StlAssignStatement e) {
		if (e.arg === null) {
			// Partial or invalid AST.
			return;
		}
		
		if (e.arg instanceof LeftValue) {
			val writtenVar = Step7LanguageHelper.getReferredVariable(e.arg as LeftValue);
			if (writtenVar.isPresent && isVarInputInContext(writtenVar.get, e)) {
				warning('''The input variable '«writtenVar.get.name»' should not be written in its declaring block.''',
						e.arg, null);
			}
		}
	}
	
	/**
	 * Returns true iff the given variable is an input (that shall not be written) in the given context.
	 */
	private def boolean isVarInputInContext(Variable writtenVar, EObject context) {
		if (writtenVar === null) {
			return false;
		}
		
		val direction = Step7LanguageHelper.getVariableDirection(writtenVar);

		// Is this variable declared in the block of the assignment statement?
		if (EmfHelper.getContainerOfType(context, ProgramUnit) !==
			EmfHelper.getContainerOfType(writtenVar, ProgramUnit)) {
			return false;
		}

		if (direction == VariableDeclarationDirection.INPUT) {
			return true;
		}
		
		return false;
	}
}
