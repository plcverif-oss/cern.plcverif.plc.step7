/******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.validation

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.plc.step7.step7Language.ArrayDT
import cern.plcverif.plc.step7.step7Language.ArrayDimensionRange
import cern.plcverif.plc.step7.step7Language.ArrayRef
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.Expression
import cern.plcverif.plc.step7.step7Language.ImpliesExpression
import cern.plcverif.plc.step7.step7Language.MemoryAddress
import cern.plcverif.plc.step7.step7Language.NamedConstantDeclaration
import cern.plcverif.plc.step7.step7Language.StringDT
import cern.plcverif.plc.step7.step7Language.Variable
import cern.plcverif.plc.step7.step7Language.VerificationAssertion
import cern.plcverif.plc.step7.util.SimpleExpressionUtil
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import org.eclipse.xtext.validation.Check

import static cern.plcverif.plc.step7.validation.Step7LanguageValidator.*

import static extension cern.plcverif.plc.step7.util.DataTypeUtil.*
import static extension cern.plcverif.plc.step7.util.Step7LanguageHelper.isVariable
import cern.plcverif.plc.step7.step7Language.UserDefinedDataType

class ExpressionValidator extends AbstractStep7ComposedValidator {
	@Check
	def checkSimpleExpression(NamedConstantDeclaration e) {
		checkSimpleExpression(e.value);
	}

	@Check
	def checkSimpleExpression(StringDT e) {
		if (e.dimension !== null) {
			checkSimpleExpression(e.dimension);
		}
	}

	@Check
	def checkSimpleExpression(ArrayDimensionRange e) {
		checkSimpleExpression(e.from);
		checkSimpleExpression(e.to);
	}

	private def checkSimpleExpression(Expression expr) {
		if (!SimpleExpressionUtil.isSimpleExpression(expr)) {
			error("Invalid simple expression.", expr, null, INVALID_SIMPLE_EXPRESSION);
		}
	}

	@Check
	def checkImpliesOnlyInAssertion(ImpliesExpression expr) {
		if (!EmfHelper.isContainedInAny(expr, VerificationAssertion)) {
			error("Implication is only permitted in verification assertions.", expr, null, INVALID_USE_OF_IMPLICATION);
		}
	}

	/**
	 * Only arrays can be indexed.
	 */
	@Check
	def checkIndexing(ArrayRef arrayRef) {
		val prefix = arrayRef.ref;
		if (prefix instanceof DirectNamedRef) {
			if (prefix.ref.isVariable) {
				val varType = Step7LanguageHelper.getDataType(prefix.ref as Variable);
				if (!(varType.isArrayType)) {
					warning('''Only arrays can be indexed, but variable '«prefix.ref.name»' is not an array.''',
						arrayRef, null, INVALID_INDEXING);
				} else {
					// Valid.
					return;
				}
			} else {
				error('''Only variables can be indexed, but '«prefix.ref.name»' is not a variable.''', arrayRef, null,
					INVALID_INDEXING);
			}
		} else {
			error('''Only variables can be indexed.''', arrayRef, null, INVALID_INDEXING);
		}
	}

	@Check
	def checkAddressValidity(MemoryAddress address) {
		val s7Address = address.value;
		if (!s7Address.isSyntacticallyValid) {
			error('''Invalid address: '«s7Address.originalStringRepresentation»'.''', address, null, INVALID_ADDRESS);
		}
	}
	
	@Check
	def checkArrayIndexValidity(ArrayRef e) {
		val arrayVar = Step7LanguageHelper.getReferredVariable(e.ref);
		if (!arrayVar.isPresent) {
			// Array variable not found. Maybe a non-array variable is indexed. In that case error will be raised by another rule.
			return;
		}
		
		val arrayType = Step7LanguageHelper.getDataType(arrayVar.get);
		if (arrayType instanceof ArrayDT) {
			if (e.index.size == 0) {
				return; // Partially parsed or invalid model
			}
			for (i : 0..Math.min(arrayType.dimensions.size - 1, e.index.size - 1)) {
				val currentDimDef = arrayType.dimensions.get(i);
				val currentIndex = e.index.get(i);
				
				if (SimpleExpressionUtil.isSimpleExpression(currentIndex)) {
					val idx = SimpleExpressionUtil.evaluateSimpleExpression(currentIndex);
					val lower = currentDimDef.arrayRangeLower;
					val upper = currentDimDef.arrayRangeUpper;

					if (lower != 0 || upper != 0) {
						// The special case with [0..0] is avoided here (lower = upper = 0).
						// This is used for addresses, such as I[123, 1] for example.
						if (idx < lower || idx > upper) {
							error(
								'''The index «i+1» of array '«arrayVar.get.name»' is invalid. The value of the index is «idx» and it is expected to be within «lower»..«upper».''',
								currentIndex,
								null,
								INVALID_ARRAYREF_INDEX
							);
						}
					}

				} else {
					// skip, we cannot determine the value of the index
				}
			}
		} else {
			// The data type is not an array type (ArrayDT), thus we cannot determine the valid ranges.
			// Probably a non-array variable is indexed. In that case error will be raised by another rule.
		}	
	}
	
	/**
	 * Raises ERROR if a direct named reference refers to an unsupported named element.
	 * Reference to a UDT by a direct named reference cannot be valid.
	 */
	@Check
	def checkInvalidNamedRef(DirectNamedRef e) {
		if (e.ref instanceof UserDefinedDataType) {
			error('''Reference to the '«e.ref.name»' UDT is invalid. UDTs cannot be used in expressions.''', e, null, INVALID_REFERENCE);
		}
	}
}
