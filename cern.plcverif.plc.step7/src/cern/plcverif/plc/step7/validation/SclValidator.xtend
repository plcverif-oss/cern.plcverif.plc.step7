/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.validation

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.plc.step7.datatypes.S7AddressMemoryIdentifier
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit
import cern.plcverif.plc.step7.step7Language.IntConstant
import cern.plcverif.plc.step7.step7Language.NamedElement
import cern.plcverif.plc.step7.step7Language.RetValCallParameterItem
import cern.plcverif.plc.step7.step7Language.RetValRef
import cern.plcverif.plc.step7.step7Language.SclStatementList
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall
import cern.plcverif.plc.step7.step7Language.StwBitRef
import cern.plcverif.plc.step7.util.SclSyntaxHelper
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.validation.Check

import static cern.plcverif.plc.step7.validation.Step7LanguageValidator.ILLEGAL_SCL_ELEMENT
import cern.plcverif.plc.step7.step7Language.MemoryAddress

class SclValidator extends AbstractStep7ComposedValidator {
	private static def isSclCode(EObject e) {
		return EmfHelper.isContainedInAny(e, SclStatementList);
	}
	
	private static def isDeclarationInSclBlock(EObject e) {
		val programUnit = EmfHelper.getContainerOfType(e, ExecutableProgramUnit);
		return programUnit.statements instanceof SclStatementList;
	}

	@Check
	def void checkRetValCallParam(RetValCallParameterItem e) {
		if (EmfHelper.isContainedInAny(e, SclSubroutineCall)) {
			error('The RET_VAL call parameter item is forbidden in SCL.', e, null, ILLEGAL_SCL_ELEMENT);
		}
	}
	
	@Check 
	def void checkRetValRef(RetValRef e) {
		if (EmfHelper.isContainedInAny(e, SclStatementList)) {
			error('The use of RET_VAL is forbidden in SCL.', e, null, ILLEGAL_SCL_ELEMENT);
		}
	}
	
	@Check
	def void checkLAddressPrefix(MemoryAddress e) {
		// L prefix is only permitted in STL
		if (e.value.memoryIdentifier == S7AddressMemoryIdentifier.L) {
			if (isSclCode(e)) {
				error('The L memory location prefix is forbidden in SCL.', e, null, ILLEGAL_SCL_ELEMENT);
			}
		}
	}
	
	@Check
	def void checkNoWhitespaceInAddresss(MemoryAddress e) {
		if (isSclCode(e)) {
			if (e.value.originalStringRepresentation.matches(".+\\s.+")) {
				error('The I/O address should not contain any whitespace in SCL.', e, null, ILLEGAL_SCL_ELEMENT);	
			}
		}
	} 
	
	@Check
	def void checkIntConstPrefix(IntConstant e) {
		val stringRep = e.value.originalStringRepresentation.toUpperCase().trim();
		
		if (stringRep.startsWith("L#") && isSclCode(e)) {
			error('The L# constant prefix is not permitted in SCL. Use DINT# instead.', e, null, ILLEGAL_SCL_ELEMENT);
		}
	}
	
	/**
	 * Raises ERROR if a status word reference (OV, OS, BR) is used in SCL.
	 */
	@Check
	def void checkStwBitRef(StwBitRef e) {
		if (isSclCode(e)) {
			error('''The reference to the status word '«e.mnemonic.literal»' is only permitted in STL.''', e, null, ILLEGAL_SCL_ELEMENT);
		}
	}
	
	/**
	 * Raises ERROR if an SCL-specific keyword (e.g., CONTINUE, EXIT) is used in SCL as an identifier.
	 */
	@Check
	def void checkSclKeywordAsIdentifier(NamedElement e) {
		if (e.name !== null && SclSyntaxHelper.isSclOnlyKeyword(e.name) &&
			!Step7LanguageHelper.namedElementAsInSourceCode(e).startsWith("#") &&
			(isSclCode(e) || isDeclarationInSclBlock(e))) {
			error(''''«e.name.toUpperCase»' is a keyword in SCL, thus it cannot be used as an identifier in SCL programs.''',
				e, null, ILLEGAL_SCL_ELEMENT);
		}
	}
	
	/**
	 * Raises ERROR if an SCL-specific keyword (e.g., CONTINUE, EXIT) is used in SCL as an identifier.
	 */
	@Check
	def void checkSclKeywordAsIdentifier(DirectNamedRef e) {
		if (e.ref.name !== null && SclSyntaxHelper.isSclOnlyKeyword(e.ref.name) &&
			!Step7LanguageHelper.namedReferenceAsInSourceCode(e).startsWith("#") &&
			(isSclCode(e) || isDeclarationInSclBlock(e))) {
			error(''''«e.ref.name.toUpperCase»' is a keyword in SCL, thus it cannot be used as an identifier in SCL programs.''',
				e, null, ILLEGAL_SCL_ELEMENT);
		}
	}
}
