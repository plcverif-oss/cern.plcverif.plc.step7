/******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Jean-Charles Tournier - Nov 2019 - Add support for DirectBitAccessRef
 *   Xaver Fink - August 2023 - Add support for BYTE datatype for DirectBitAccessRef
 *****************************************************************************/
package cern.plcverif.plc.step7.validation

import cern.plcverif.plc.step7.step7Language.ArrayDT
import cern.plcverif.plc.step7.step7Language.ArrayDimension
import cern.plcverif.plc.step7.step7Language.ElementaryDT
import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum
import cern.plcverif.plc.step7.step7Language.FbOrUdtDT
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.FunctionBlock
import cern.plcverif.plc.step7.step7Language.ParameterDT
import cern.plcverif.plc.step7.step7Language.ProgramUnit
import cern.plcverif.plc.step7.step7Language.Step7LanguagePackage
import cern.plcverif.plc.step7.step7Language.StringDT
import cern.plcverif.plc.step7.step7Language.UnnamedConstant
import cern.plcverif.plc.step7.step7Language.UserDefinedDataType
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine
import cern.plcverif.plc.step7.util.DataTypeUtil
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.validation.Check

import static cern.plcverif.plc.step7.validation.Step7LanguageValidator.*

import static extension cern.plcverif.plc.step7.util.DataTypeUtil.*
import static extension cern.plcverif.plc.step7.util.Step7LanguageHelper.*
import cern.plcverif.plc.step7.step7Language.StructDT
import cern.plcverif.plc.step7.step7Language.DirectBitAccessRef
import cern.plcverif.plc.step7.util.Step7LanguageHelper

class DataTypeValidator extends AbstractStep7ComposedValidator {
	@Check
	def checkInvalidUnnamedConstants(UnnamedConstant c) {
		val value = c.unnamedConstantValue();
		if (value.isSyntacticallyValid == false) {
			error('''Invalid constant (unable to parse as «c.class.name»): «value.originalStringRepresentation»''', c,
				null, UNPARSABLE_STEP7_CONSTANT);
		}
	}

	@Check(NORMAL)
	def checkArrayInitalizationLength(VariableDeclarationLine varDeclLine) {
		if (varDeclLine.isInitialized && varDeclLine.type instanceof ArrayDT) {
			val arrayDT = varDeclLine.type as ArrayDT;

			var arraySize = 1;
			for (dim : arrayDT.dimensions) {
				arraySize *= DataTypeUtil.arrayDimensionLength(dim);
			}

			val initList = DataTypeUtil.flattenArrayInitialization(varDeclLine.initialization);
			val initListSize = initList.size;

			if (arraySize != initListSize) {
				error('''Invalid array initialization. The size of the array is «arraySize», but the size of the initialization list is «initListSize».''',
					varDeclLine, Step7LanguagePackage.eINSTANCE.variableDeclarationLine_Initialization,
					INVALID_ARRAY_INIT_SIZE);
				}
			}
		}

		@Check
		def void checkArrayRangeValidity(ArrayDimension arrayDimension) {
			val rangeLower = arrayDimension.arrayRangeLower
			val rangeUpper = arrayDimension.arrayRangeUpper
			if (rangeLower < -32768 || rangeUpper > 32767) {
				error('''The array range '«rangeLower»..«rangeUpper»' is not within -32768..32767.''',
					arrayDimension, null, INVALID_ARRAY_RANGE);
			}

			if (rangeLower > rangeUpper) {
				error('''The array range '«rangeLower»..«rangeUpper»' is invalid.''', arrayDimension, null,
					INVALID_ARRAY_RANGE);
			}
		}
		
		
		@Check 
		def void checkDirectBitAccessIndexValidity(DirectBitAccessRef directBitAccessRef){
			val optionalVar = Step7LanguageHelper.getReferredVariable(directBitAccessRef.ref)
			val referredVar = optionalVar.get
			val varDt = Step7LanguageHelper.getDataType(referredVar)
			var type = ElementaryTypeEnum.VOID
			if (varDt instanceof ElementaryDT){
					type = varDt.type;
			}
			
			if (varDt instanceof ArrayDT){
				if (varDt.baseType instanceof ElementaryDT){
					type = (varDt.baseType as ElementaryDT).type
				}
			}
			if (isDirectBitAccessInRange(directBitAccessRef.index, type) === false){
				error('''Direct bit access is only available for WORD and BYTE - Index can not be greater than 15''', directBitAccessRef, null,
					INVALID_ARRAY_RANGE);
			}
		}
		
		def private dispatch isDirectBitAccessInRange(String index, ElementaryTypeEnum type){
			val dataTypeWidth = DataTypeUtil.getBitWidth(type)
			if ( java.lang.Long.valueOf(index) > (dataTypeWidth - 1)){
				return false;
			}	
			return true;
		}
		
		def private dispatch isDirectBitAccessInRange(Void index, ElementaryTypeEnum type){
			//if index is null it means it is not a directBitAccess, so ok
			return true;
		}
		
		/**
		 * Raises ERROR (INVALID_ARRAY_TYPE) if the array data type has 
		 * a parameter type as base type that is invalid according to [SCL] Sec. 7.3.3.
		 */
		@Check
		def void checkArrayBaseType(ArrayDT array) {
			if (array.baseType.isParamType) {
				error('''Parameter types must not be used as the element type for an array.''',
					array.baseType,
					null,
					INVALID_ARRAY_TYPE
				);
			}
		}

		@Check
		def void checkElementaryDataTypeUsage(ElementaryDT dt) {
			if (dt.type == ElementaryTypeEnum.VOID) {
				// VOID is only permitted as the return value of a function
				if (!(dt.eContainer instanceof Function)) {
					error('VOID data type is only permitted as the return type of a function.', dt, null,
						ILLEGAL_USE_OF_VOID);
				}
			}
		}

		@Check
		def void checkArrayDimensionCount(ArrayDT arrayDt) {
			if (arrayDt.dimensions.size > MAX_ARRAY_DIMENSIONS) {
				error('''The array has too many dimensions («arrayDt.dimensions.size» instead of the maximal «MAX_ARRAY_DIMENSIONS»).''',
					arrayDt, null, TOO_MANY_ARRAY_DIMENSIONS);
			}
		}

		@Check
		def void checkParamDtUsage(ParameterDT paramDt) {
			// ParameterDT can only be used as data type in FB's or FC's VAR_INPUT block or in FB's VAR_IN_OUT block
			// TODO: check; I guess it can be return value or VAR_OUT too, in certain cases at least. 
			val containingVariableBlock = EcoreUtil2.getContainerOfType(paramDt, VariableDeclarationBlock);
			if (containingVariableBlock !== null) {
				val containingProgramUnit = EcoreUtil2.getContainerOfType(containingVariableBlock, ProgramUnit);

				if (!(containingProgramUnit instanceof FunctionBlock) && !(containingProgramUnit instanceof Function)) {
					error('''The parameter type «paramDt.type» can only be used in functions and function blocks.''',
						paramDt, null, INVALID_PARAMDT_USAGE);
					return;
				}
				//if (containingVariableBlock.direction != VariableDeclarationDirection.INPUT &&
				//	containingVariableBlock.direction != VariableDeclarationDirection.INOUT) {
					// error('''The parameter type «paramDt.type» can only be used in VAR_INPUT or VAR_IN_OUT blocks.''', paramDt, null, INVALID_PARAMDT_USAGE);
					// This contradicts the example in Section 7.6.1, p. 7-20
				//}
			}
			
			if (paramDt.anyBit) {
				error('ANY_BIT should not be used in the code.', paramDt, null, INVALID_PARAMDT_USAGE);
			}
		}

		@Check
		def void checkStringDTDimension(StringDT stringDt) {
			val dim = stringDt.dimensionInt;
			if (dim < MIN_STRING_DT_DIMENSION || dim > MAX_STRING_DT_DIMENSION) {
				error('''The dimension of the string type («dim») is not within the permitted range «MIN_STRING_DT_DIMENSION»..«MAX_STRING_DT_DIMENSION».''',
					stringDt, null, INVALID_STRINGDT_DIMENSION);
			}
		}

		@Check
		def checkInvalidFbOrUdtType(FbOrUdtDT e) {
			if (e.type !== null && !(e.type instanceof FunctionBlock || e.type instanceof UserDefinedDataType)) {
				error('Invalid type reference. It is expected to be an FB or UDT.', e, null, INVALID_FBORUDT_TYPE);
			}
		}
		
		@Check
		def checkEmptyStruct(StructDT e) {
			if (e.members.nullOrEmpty) {
				error('A STRUCT must contain at least one member variable.', e, null, EMPTY_STRUCT);
			}	
		} 
		
		@Check(NORMAL)
		def checkDirectBitAccessRefOnWordOnly( DirectBitAccessRef e){
			val optionalVar = Step7LanguageHelper.getReferredVariable(e.ref)
			val referredVar = optionalVar.get
			
			val varDt = Step7LanguageHelper.getDataType(referredVar)
			var type = ElementaryTypeEnum.VOID
			if (varDt instanceof ElementaryDT){
					type = varDt.type;
			}
			
			if (varDt instanceof ArrayDT){
				if (varDt.baseType instanceof ElementaryDT){
					type = (varDt.baseType as ElementaryDT).type
				}
			}	
			
			if (type === ElementaryTypeEnum.WORD || type === ElementaryTypeEnum.BYTE){
				return;
			}
			
			error('DirectBitAcces can only be applied to WORD or BYTE variables', e, null, INVALID_DIRECTBITACCESS_TYPE);
		}	 

	}
	