/******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 * 	 Jean-Charles Tournier - November 2019 - Add support for DirectBitAccessRef
 *****************************************************************************/
package cern.plcverif.plc.step7.validation

import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.DirectRef
import cern.plcverif.plc.step7.step7Language.ElementaryDT
import cern.plcverif.plc.step7.step7Language.NamedValueRef
import cern.plcverif.plc.step7.step7Language.QualifiedRef
import cern.plcverif.plc.step7.util.DataTypeUtil
import cern.plcverif.plc.step7.util.Step7CachedFeaturesProvider
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import com.google.inject.Inject
import org.eclipse.xtext.validation.Check

import static cern.plcverif.plc.step7.validation.Step7LanguageValidator.*

import static extension cern.plcverif.plc.step7.util.Step7LanguageHelper.*

class TiaValidator extends AbstractStep7ComposedValidator {

	@Inject
	Step7CachedFeaturesProvider featureSetProvider;

	//@TODO Add validation for DirectBitAccess to make sure it is not used in S7 only in TIA 
	
	//@TODO add validation to check that the variable is of type word

	@Check
	def void checkTiaLocalVariableUses(NamedValueRef namedRef) {
		if (namedRef instanceof DirectRef && !(namedRef instanceof DirectNamedRef)) {
			// These are not variables
			return;
		}
		
		// skip parts of the qualified references 
		if (namedRef.eContainer instanceof QualifiedRef == false) { 
		
			if (!getFirstPrefixOfReference(namedRef).isVariable) {
				// not a variable, no need for #
				return;
			}
			
			if (!Step7LanguageHelper.namedReferenceAsInSourceCode(namedRef).startsWith("#")) {
				val compatilityLevel = featureSetProvider.getCompatibilityLevel(namedRef);
				if (compatilityLevel.isTia==true) {
					warning('''Uses of local variables shall be prefixed with '#' in TIA Portal.''', namedRef, null,
						INVALID_TIA_LOCAL_VARIABLE_REF);
				}
			}
		}
	}

	@Check
	def void checkTiaElementaryTypes(ElementaryDT elementaryDt) {
		val type = elementaryDt.type;

		if (!DataTypeUtil.isStep7SupportedDt(type)) {
			val compatibilityLevel = featureSetProvider.getCompatibilityLevel(elementaryDt);
			if (!DataTypeUtil.isSupportedDt(type, compatibilityLevel)) {
				warning('''Data type «type» is not supported in «compatibilityLevel» files.''', elementaryDt, null,
					INVALID_ELEMENTARY_TYPE);
			}
		}
	}
}
