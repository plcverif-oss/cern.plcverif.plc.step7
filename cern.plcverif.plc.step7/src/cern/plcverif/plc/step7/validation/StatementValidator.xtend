/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.validation

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.plc.step7.step7Language.AssignmentOperator
import cern.plcverif.plc.step7.step7Language.DataBlockInitialAssignment
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.EnCallParameterItem
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.FunctionBlock
import cern.plcverif.plc.step7.step7Language.Label
import cern.plcverif.plc.step7.step7Language.LabeledStlStatement
import cern.plcverif.plc.step7.step7Language.LeftValue
import cern.plcverif.plc.step7.step7Language.NamedCallParameterItem
import cern.plcverif.plc.step7.step7Language.NamedConstantDeclaration
import cern.plcverif.plc.step7.step7Language.OrganizationBlock
import cern.plcverif.plc.step7.step7Language.ProgramUnit
import cern.plcverif.plc.step7.step7Language.SclAssignmentStatement
import cern.plcverif.plc.step7.step7Language.SclContinueStatement
import cern.plcverif.plc.step7.step7Language.SclExitStatement
import cern.plcverif.plc.step7.step7Language.SclForStatement
import cern.plcverif.plc.step7.step7Language.SclGotoStatement
import cern.plcverif.plc.step7.step7Language.SclStatement
import cern.plcverif.plc.step7.step7Language.SclStatementList
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall
import cern.plcverif.plc.step7.step7Language.SingleCallParameter
import cern.plcverif.plc.step7.step7Language.Step7LanguagePackage
import cern.plcverif.plc.step7.step7Language.StlJumpStatement
import cern.plcverif.plc.step7.step7Language.StlStatementList
import cern.plcverif.plc.step7.step7Language.SubroutineCall
import cern.plcverif.plc.step7.step7Language.VariableDeclarationDirection
import cern.plcverif.plc.step7.util.BasicExpressionUtil
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import java.util.Collections
import java.util.List
import java.util.Optional
import java.util.Set
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.validation.Check

import static cern.plcverif.plc.step7.validation.Step7LanguageValidator.*

import static extension cern.plcverif.plc.step7.util.DataTypeUtil.*
import static extension cern.plcverif.plc.step7.util.Step7LanguageHelper.*
import cern.plcverif.plc.step7.step7Language.CallParameterList
import cern.plcverif.plc.step7.step7Language.RangeCaseElementValue
import cern.plcverif.plc.step7.step7Language.CaseElementValue
import cern.plcverif.plc.step7.util.SimpleExpressionUtil
import cern.plcverif.plc.step7.step7Language.RangeCaseElementString
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock
import cern.plcverif.plc.step7.step7Language.NamedValueRef
import cern.plcverif.plc.step7.step7Language.VerificationAssertion
import cern.plcverif.plc.step7.step7Language.DataBlock

class StatementValidator extends AbstractStep7ComposedValidator {
	@Check
	def void checkDuplicateAndUnusedLabels(ExecutableProgramUnit e) {
		if (e.statements instanceof SclStatementList) {
			// In SCL, the labels have to be defined in a 'LABEL' declaration block
			checkDuplicateAndUnusedLabels(Step7LanguageHelper.allDefinedLabelsInDeclarationSection(e.declarationSection),
				e.statements as SclStatementList);
		} else if (e.statements instanceof StlStatementList) {
			// STL labels are not pre-defined
			checkDuplicateAndUnusedLabels(e.statements as StlStatementList);
		}
	}

	private def void checkDuplicateAndUnusedLabels(Iterable<? extends Label> labels, SclStatementList statements) {
		if (statements === null) {
			// no statements to check
			return;
		}
		
		val labelsAndTargets = Step7LanguageHelper.findAllTargetSclStatements(statements);
		for (item : labelsAndTargets.entrySet) {
			if (item.value.size > 1) {
				for (occurrence : item.value) {
					error('''Duplicate use of label '«item.key.name»'.''', occurrence, null, DUPLICATE_LABEL);
				}
			}
		}

		for (label : labels) {
			if (labelsAndTargets.getOrDefault(label, Collections.EMPTY_LIST).size == 0) {
				warning('''Unused label: '«label.name»'.''', label, null, UNUSED_LABEL);
			}
		}
	}
	
	private def void checkDuplicateAndUnusedLabels(StlStatementList statements) {
		val labeledStmts = EmfHelper.getAllContentsOfType(statements, LabeledStlStatement, false);
		val labels = labeledStmts.map[it | it.label].toList;
		
		val jumps = EmfHelper.getAllContentsOfType(statements, StlJumpStatement, false);
		val jumpTargets = jumps.map[it | it.targetLabel].toSet;
		
		checkDuplicateStlLabels(labels);
		checkUnusedStlLabels(labels, jumpTargets);
	}
	
	private def checkDuplicateStlLabels(List<Label> labels) {
		val seenNames = newHashSet();
		for (label : labels) {
			if (seenNames.contains(label.name.toLowerCase)) {
				error('''The label '«label.name»' has already been defined.''', label, null, DUPLICATE_LABEL);
			}
			seenNames.add(label.name.toLowerCase);
		}
	}
	
	private def checkUnusedStlLabels(List<Label> definedLabels, Set<Label> jumpTargets) {
		for (label : definedLabels) {
			if (!jumpTargets.contains(label)) {
				warning('''Unused label: '«label.name»'.''', label, null, UNUSED_LABEL);
			}		
		}
	}
	

	@Check
	def void checkCallValidity(SubroutineCall call) {
		if (!call.isFbCall && !call.isFcCall) {
			// the calledUnit is not FB (or instance FB var) or FC
			error(
				'''The called unit '«call.calledUnit.name»' is not a valid function or function block.''',
				call,
				Step7LanguagePackage.eINSTANCE.subroutineCall_CalledUnit,
				INVALID_SUBROUTINE_CALL
			);
			return;
		}

		if (call.isFcCall || (call.isFbCall && call.calledUnit.isVariable) || (call.isFbCall && call.calledUnit.isDataBlock && (call.calledUnit as DataBlock).isInstanceDb)) {
			// DB must not be specified
			if (call.hasExplicitDataBlock) {
				error(
					'''The called unit '«call.calledUnit.name»' does not allow to specify an explicit data block '«call.dataBlock.name»' for this call.''',
					call,
					Step7LanguagePackage.eINSTANCE.subroutineCall_DataBlock,
					INVALID_SUBROUTINE_CALL
				);
			}
		} else {
			// for a non-instance FB call explicit data block is mandatory
			if (!call.hasExplicitDataBlock) {
				error(
					'''Missing data block for the call of '«call.calledUnit.name»'.''',
					call,
					Step7LanguagePackage.eINSTANCE.subroutineCall_CalledUnit,
					INVALID_SUBROUTINE_CALL
				);
			} else {
				// if there is a DB specified, its type should match the called unit
				val typeOfDb = call.dataBlock.structure;
				if (typeOfDb == null){
					error(
						'''The type of data block '«call.dataBlock.name»' does not match the called unit '«call.calledUnit.name»'.''',
						call,
						Step7LanguagePackage.eINSTANCE.subroutineCall_DataBlock,
						INVALID_FB_CALL_DB
					);
				}
				else {
					val Optional<FunctionBlock> fbTypeOfDb = typeOfDb.getRepresentedFb;
					if (!fbTypeOfDb.present || call.calledUnit !== fbTypeOfDb.get) {
						error(
							'''The type of data block '«call.dataBlock.name»' does not match the called unit '«call.calledUnit.name»'.''',
							call,
							Step7LanguagePackage.eINSTANCE.subroutineCall_DataBlock,
							INVALID_FB_CALL_DB
						);
					}
				}
			}
		}
	}

	/**
	 * Raises ERROR if a parameterless call 
	 * ({@link SubroutineCall#getCallParameter()} is {@code null}) calls a function
	 * that has (input, in-out or output) parameters.
	 */
	@Check
	def void checkFcCallParametersOfParameterlessCall(SubroutineCall call) {
		if (call.isFcCall && call.callParameter === null) {
			val callee = Step7LanguageHelper.getCalledProgramUnit(call);
			val paramCount = Step7LanguageHelper.getBlockParameters(callee).size;
			if (paramCount != 0) {
				error(
					'''The called function has parameters, thus parameterless calls are not permitted.''',
					call,
					null,
					INVALID_SUBROUTINE_CALL
				);
			}
		}
	}
	
	/**
	 * Raises ERROR if a named call parameter item uses a non-assignable value for an output or in-out parameter.
	 */
	@Check
	def void checkFcOutputParameterAssignability(NamedCallParameterItem e) {
		val definingVarBlock = EmfHelper.getContainerOfType(e.parameter, VariableDeclarationBlock);
		if (definingVarBlock.direction == VariableDeclarationDirection.OUTPUT ||
			definingVarBlock.direction == VariableDeclarationDirection.INOUT) {
			// The parameter is output or in-out
			val valid = e.value instanceof LeftValue &&
				!(e.value instanceof NamedValueRef && isDirectNamedConstRef(e.value as NamedValueRef));
			if (!valid) {
				error('''The right-hand side of this parameter assignment is invalid for a «definingVarBlock.direction.literal» parameter.''',
					e, null, INVALID_OUTPUT_PARAMETER);
			}
		}
	}
	
	/**
	 * Raises ERROR if the call assigns a parameter multiple times (e.g. `foo(a := true, a := true, b := false);`,
	 * or if in case of an FC call any of the parameters were not assigned. 
	 */
	@Check
	def void checkCallParameters(CallParameterList callParamList) {
		val call = callParamList.callOfParam;
		if (call === null || call.calledUnit === null || call.calledUnit.name === null) {
			return;
		}
		
		val callee = Step7LanguageHelper.getCalledProgramUnit(call);
		if (callee instanceof ExecutableProgramUnit) {
			val callParams = callParamList.parameters.filter(NamedCallParameterItem).toList;
			
			// Check if call parameters are assigned multiple times
			var alreadySeenVars = newHashSet();
			for (callParam : callParams) {
				if (alreadySeenVars.contains(callParam.parameter)) {
					error('''The parameter '«callParam.parameter.name»' has already been assigned in this call.''',
						callParam,
						Step7LanguagePackage.eINSTANCE.namedCallParameterItem_Parameter,
						INVALID_CALL_PARAMETERS
					);
				} 
				alreadySeenVars.add(callParam.parameter);
			}
			
			// Check if there is any parameter that was not assigned in an FC
			if (call.isFcCall) {
				val missingCalleeParams = Step7LanguageHelper.getBlockParameters(callee).toSet;
				missingCalleeParams.removeAll(callParams.map[it | it.parameter]);
				
				if (!missingCalleeParams.isEmpty) {
					val message = '''The following parameters need to be assigned: «FOR p : missingCalleeParams SEPARATOR ', '»«p.name»«ENDFOR».'''
					error(message, call, null, INVALID_CALL_PARAMETERS);
				}
			}
		} else {
			System.err.println('''Strange case in checkCallParameters.''')
		}
	}
	
	/**
	 * Raises ERROR if a variable view is assigned as named call parameter item.
	 * 
	 * E.g. if the variable {@code v} is defined as {@code v AT w : WORD;} in the function {@code foo},
	 * it is invalid to call {@code foo(v := 123);}.
	 */
	@Check
	def void checkCallParameterVarView(NamedCallParameterItem e) {
		if (e === null || e.parameter === null || e.parameter.eContainer === null) {
			// Nothing to do. Most probably the AST is incomplete or contains parsing errors.
			return;
		}
		
		if (e.parameter.isReference) {
			error('''The parameter '«e.parameter.name»' is a variable view that is not visible outside of its defining block.''',
				e, Step7LanguagePackage.eINSTANCE.namedCallParameterItem_Parameter, INVALID_CALL_PARAMETERS
			);
		}
	}
	
	/**
	 * Raises ERROR if a named call parameter item assigns a variable defined in VAR or VAR_TEMP.
	 */
	@Check
	def void checkCallParameterTempLocal(NamedCallParameterItem e) {
		if (e === null || e.parameter === null || e.parameter.eContainer === null) {
			// Nothing to do. Most probably the AST is incomplete or contains parsing errors.
			return;
		}
		
		val direction = e.parameter.variableDirection;
		if (direction == VariableDeclarationDirection.STATIC || direction == VariableDeclarationDirection.TEMP) {
			error('Only input, output and input-output variables (parameters) can be referenced in a call.',
				e, Step7LanguagePackage.eINSTANCE.namedCallParameterItem_Parameter, INVALID_CALL_PARAMETERS
			);
		} 
	}
	
	@Check
	def void checkContinueLocationValidity(SclContinueStatement e) {
		warnIfNotWithinLoop(e, "CONTINUE");
	}

	@Check
	def void checkExitLocationValidity(SclExitStatement e) {
		warnIfNotWithinLoop(e, "EXIT");
	}

	private def void warnIfNotWithinLoop(SclStatement e, String statementType) {
		val Optional<SclStatement> containingLoop = Step7LanguageHelper.getContainingLoop(e);
		if (!containingLoop.present) {
			// CONTINUE used not within a loop
			warning('''«statementType» is used outside of loops.''', e, null, INVALID_LOOP_STATEMENT);
		}
	}

	@Check
	def void checkGotoTarget(SclGotoStatement e) {
		val labelTarget = Step7LanguageHelper.findFirstTargetStatement(e.label);

		if (!labelTarget.present) {
			error('''The target label '«e.label.name»' is not found.''', e, null, INVALID_GOTO_STATEMENT);
		} else {
			val loopOfTarget = Step7LanguageHelper.getContainingLoop(labelTarget.get);
			val loopOfGoto = Step7LanguageHelper.getContainingLoop(e);
			if (loopOfTarget.present && (!loopOfGoto.present || loopOfGoto.get != loopOfTarget.get)) {
				// GOTO l is valid if l is not inside a loop or 'GOTO l' and 'l' are inside the same loop 
				error('''It is forbidden to jump into a loop using GOTO.''', e, null, INVALID_GOTO_STATEMENT);
			}
		}
	}

	@Check(NORMAL)
	def void checkForStatement(SclForStatement e) {
		// 'initialStatement's 'rightValue' shall be a basic expression
		if (!BasicExpressionUtil.isBasicExpression(e.initialStatement.rightValue)) {
			error("The initial value of a FOR loop shall be a basic expression.", e.initialStatement,
				Step7LanguagePackage.eINSTANCE.sclAssignmentStatement_RightValue, INVALID_FOR_STATEMENT);
		}

		// 'finalValue' shall be a basic expression (p. 15-53)
		if (!BasicExpressionUtil.isBasicExpression(e.finalValue)) {
			error("The target value of a FOR loop shall be a basic expression.", e.finalValue, null,
				INVALID_FOR_STATEMENT);
		}

		// 'increment' shall be a basic expression (p. 15-53)
		if (e.increment !== null && !BasicExpressionUtil.isBasicExpression(e.increment)) {
			error("The increment value of a FOR loop shall be a basic expression.", e.increment, null,
				INVALID_FOR_STATEMENT);
		}
	}

	@Check
	def void checkCallParameters(SingleCallParameter singleCallParam) {
		// using this is only allowed if the called function has a single input parameter
		val callOfParam = singleCallParam.callOfParam;
		if (callOfParam.isFcCall) {
			val calledPou = callOfParam.calledProgramUnit as Function;
			val inputVars = calledPou.declarationSection.allDefinedVariablesInDeclarationSection(
				VariableDeclarationDirection.INPUT);
			val paramCount = Step7LanguageHelper.getBlockParameters(calledPou).size;
			if (inputVars.size != 1 || paramCount != 1) {
				error('''Unnamed call parameters are only if the called function contains exactly one input variable. The function «calledPou.name» contains «inputVars.size» variables: «FOR v : inputVars SEPARATOR ', '»«v.name»«ENDFOR».''',
					singleCallParam, null, INVALID_CALL_PARAMETERS);
			}
		} else {
			error('''Unnamed call parameters are only allowed for function calls.''', singleCallParam, null,
				INVALID_CALL_PARAMETERS);
		}
	}

	@Check
	def void checkReturnValueValidity(SclAssignmentStatement assignment) {
		if (!(assignment.leftValue instanceof DirectNamedRef)) {
			return;
		}
		val assignedElement = (assignment.leftValue as DirectNamedRef).ref;

		switch (assignedElement) {
			OrganizationBlock:
				error('''Assignment of the organization block '«assignedElement.name»' is not valid.''',
					assignment.leftValue, null, INVALID_RETVAL_ASSIGNMENT)
			FunctionBlock:
				error('''Assignment of the function block '«assignedElement.name»' is not valid.''',
					assignment.leftValue, null, INVALID_RETVAL_ASSIGNMENT)
			Function: {
				val containingProgramUnit = EcoreUtil2.getContainerOfType(assignment, ProgramUnit);
				if (assignedElement != containingProgramUnit) {
					error('''Invalid return value assignment (expected left value: '«containingProgramUnit.name»').''',
						assignment.leftValue, null, INVALID_RETVAL_ASSIGNMENT);
					return;
				}
				if (assignedElement.returnType.isVoid) {
					error('''Return value assignment is not valid in VOID functions.''', assignment.leftValue, null,
						INVALID_RETVAL_ASSIGNMENT);
					return;
				}
			}
		}
	}

	@Check
	def void checkParameterAssignmentValidity(NamedCallParameterItem parameterItem) {
		val containingCall = EcoreUtil2.getContainerOfType(parameterItem, SclSubroutineCall);
		if (containingCall === null) {
			return;
		}
		if (containingCall.isFbCall) {
			// in FB call it is forbidden to assign output variables
			if (parameterItem.parameter.variableDirection == VariableDeclarationDirection.OUTPUT) {
				warning('''Forbidden to assign output variables in an FB call.''', parameterItem, null,
					INVALID_FB_OUTPUT_ASSIGNMENT);
			}

			if (parameterItem.assignmentOperator == AssignmentOperator.FC_OUTPUT) {
				warning(
					'''The assignment operator '=>' is only allowed for in-out and output variables of FC calls.''',
					parameterItem,
					Step7LanguagePackage.eINSTANCE.callParameterItem_AssignmentOperator,
					INVALID_CALL_ASSIGNMENT_OPERATOR
				);
			}
		}

		if (containingCall.isFcCall && parameterItem.assignmentOperator == AssignmentOperator.FC_OUTPUT) {
			val direction = parameterItem.parameter.variableDirection;
			if (direction != VariableDeclarationDirection.INOUT && direction != VariableDeclarationDirection.OUTPUT) {
				warning(
					'''The assignment operator '=>' is only allowed for in-out and output variables of FC calls.''',
					parameterItem,
					Step7LanguagePackage.eINSTANCE.callParameterItem_AssignmentOperator,
					INVALID_CALL_ASSIGNMENT_OPERATOR
				);
			}
		}
	}

	@Check
	def void checkParameterAssignmentValidity(EnCallParameterItem parameterItem) {
		if (parameterItem.assignmentOperator == AssignmentOperator.FC_OUTPUT) {
			warning(
				'''The assignment operator '=>' is only allowed for in-out and output variables of FC calls.''',
				parameterItem,
				Step7LanguagePackage.eINSTANCE.callParameterItem_AssignmentOperator,
				INVALID_CALL_ASSIGNMENT_OPERATOR
			);
		}
	}
	
	/**
	 * Raises ERROR if the left value of a DB initial assignment is a named constant. 
	 */
	@Check
	def void checkNoConstAsLeftValue(DataBlockInitialAssignment e) {
		checkNoConstAsLeftValue(e.leftValue);
	}
	
	/**
	 * Raises ERROR if the left value of a SCL assignment is a named constant. 
	 */
	@Check
	def void checkNoConstAsLeftValue(SclAssignmentStatement e) {
		checkNoConstAsLeftValue(e.leftValue);
	}
	
	/**
	 * Raises ERROR if the given left value is a named constant. 
	 */
	private def void checkNoConstAsLeftValue(LeftValue leftValue) {
		// Note: reference to a constant cannot be qualified
		if (leftValue instanceof DirectNamedRef && (leftValue as DirectNamedRef).ref instanceof NamedConstantDeclaration) {
			error('''It is not possible to re-assign the value of a constant. The '«(leftValue as DirectNamedRef).ref.name»' is a constant.''',
				leftValue,
				null,
				CONST_AS_LEFTVALUE
			);
		}
	}
	
	@Check
	def void checkCaseRange(RangeCaseElementValue e) {
		if (e.lowerBound !== null && e.upperBound !== null) {
			checkCaseRange(e, SimpleExpressionUtil.evaluateSimpleExpression(e.lowerBound),  SimpleExpressionUtil.evaluateSimpleExpression(e.upperBound));
		}
	}
	
	@Check
	def void checkCaseRange(RangeCaseElementString e) {
		checkCaseRange(e, Step7LanguageHelper.caseValueRangeLower(e), Step7LanguageHelper.caseValueRangeUpper(e));
	}
	
	private def void checkCaseRange(CaseElementValue e, long lower, long upper) {
		// INVALID_CASE_RANGE
		if (lower > upper) {
			error('''Invalid case value range: the lower bound («lower») shall not be greater than the upper bound («upper»).''', e, null, INVALID_CASE_RANGE);
		}
	}
	
	/**
	 * Raises ERROR if an assertion contains a call.
	 * The type conversion function calls are exceptions, they are permitted.
	 */
	@Check
	def void checkCallInAssertion(VerificationAssertion e) {
		for (call : EmfHelper.getAllContentsOfTypeIterable(e.expression, SubroutineCall, true)) {
			if (!(call.calledUnit instanceof Function &&
				Step7LanguageHelper.isTypeConversionFunc(call.calledUnit))) {
				error(
					'Invalid assertion: assertions shall not contain calls.',
					call,
					Step7LanguagePackage.eINSTANCE.subroutineCall_CalledUnit,
					INVALID_ASSERTION
				);
			}
		}
	}
}
