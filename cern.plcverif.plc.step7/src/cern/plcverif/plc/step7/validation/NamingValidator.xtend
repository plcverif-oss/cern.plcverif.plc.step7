/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.validation

import cern.plcverif.plc.step7.indexing.Step7LanguageIndex
import cern.plcverif.plc.step7.step7Language.DeclarationSection
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.FunctionBlock
import cern.plcverif.plc.step7.step7Language.NamedElement
import cern.plcverif.plc.step7.step7Language.OrganizationBlock
import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.step7Language.ProgramUnit
import cern.plcverif.plc.step7.step7Language.Step7LanguagePackage
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import com.google.inject.Inject
import java.util.HashSet
import java.util.List
import java.util.Map
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.validation.Check

import static cern.plcverif.plc.step7.validation.Step7LanguageValidator.*

import static extension cern.plcverif.plc.step7.util.Step7LanguageHelper.*

class NamingValidator extends AbstractStep7ComposedValidator {

	@Inject
	Step7LanguageIndex index;

	@Check
	def checkInvalidBlockKeyword(OrganizationBlock block) {
		if (block.name.isBlockIdentifier && !block.name.toUpperCase.startsWith("OB")) {
			error('Invalid block keyword is used for ORGANIZATION_BLOCK.', block,
				Step7LanguagePackage.eINSTANCE.namedElement_Name, INVALID_BLOCK_KEYWORD);
		}
	}

	@Check
	def checkInvalidBlockKeyword(Function block) {
		if (block.name.isBlockIdentifier && !block.name.toUpperCase.startsWith("FC")) {
			error('Invalid block keyword is used for FUNCTION.', block,
				Step7LanguagePackage.eINSTANCE.namedElement_Name, INVALID_BLOCK_KEYWORD);
		}
	}

	@Check
	def checkInvalidBlockKeyword(FunctionBlock block) {
		if (block.name.isBlockIdentifier && !block.name.toUpperCase.startsWith("FB")) {
			error('Invalid block keyword is used for FUNCTION_BLOCK.', block,
				Step7LanguagePackage.eINSTANCE.namedElement_Name, INVALID_BLOCK_KEYWORD);
		}
	}

	@Check
	def void checkIdentifierValidity(NamedElement element) {
		if (!element.name.isSymbol()) {
			checkIdentifierValidity(element.name, element, Step7LanguagePackage.eINSTANCE.namedElement_Name);
		}
	}

	def checkIdentifierValidity(String name, EObject markerAttachmentObject, EAttribute markerAttachmentAttribute) {
		if (name.length > 24) {
			warning(
				'''Too long identifier. («name» is «name.length» characters long.)''',
				markerAttachmentObject,
				markerAttachmentAttribute,
				INVALID_IDENTIFIER
			);
		}

		if (name.contains("__") && !name.equalsIgnoreCase("__GLOBAL_TIME")) {
			warning(
				'Identifier should not contain multiple adjacent underscores.',
				markerAttachmentObject,
				markerAttachmentAttribute,
				INVALID_IDENTIFIER
			);
		}
	}

	@Check(NORMAL)
	def void checkVariableNameUniqueness(DeclarationSection declarationSection) {
		val HashSet<String> names = newHashSet();

		for (v : Step7LanguageHelper.allDefinedVariablesInDeclarationSection(declarationSection)) {
			val unique = names.add(v.name.toLowerCase);
			if (!unique) {
				error('''The name of variable '«v.name»' is not unique within the block.''', v, null,
					NON_UNIQUE_FIELD_NAME);
			}
		}

		for (c : Step7LanguageHelper.allDefinedConstantsInDeclarationSection(declarationSection)) {
			val unique = names.add(c.name.toLowerCase);
			if (!unique) {
				error('''The name of constant '«c.name»' is not unique within the block.''', c,
					Step7LanguagePackage.eINSTANCE.namedElement_Name, NON_UNIQUE_FIELD_NAME);
			}
		}
	}

	@Check(NORMAL)
	def void checkBlockNameUniqueness(ProgramFile file) {
		val Map<String, List<ProgramUnit>> localProgramUnitsByName = file.programUnits
			.filter[it | !it.isBuiltInBlock]
			.groupBy [
			it.name.toLowerCase
		];

		val visibleProgramUnits = index.getVisibleEObjectDescriptions(file, Step7LanguagePackage.eINSTANCE.programUnit);
		val HashSet<String> visibleNames = newHashSet();

		for (unitName : visibleProgramUnits.map[it.name.lastSegment.toLowerCase]) {
			val isUnique = visibleNames.add(unitName);
			if (!isUnique) {
				// try to locate the corresponding local unit
				val collidingLocalProgramUnits = localProgramUnitsByName.get(unitName);
				if (collidingLocalProgramUnits !== null) {
					for (unit : collidingLocalProgramUnits) {
						error('''Duplicate block name: «unit.name».''', unit,
							Step7LanguagePackage.eINSTANCE.namedElement_Name, NON_UNIQUE_BLOCK_NAME);
					}
				} else {
					// println('''Duplicate name «unitName» was found, but none of the definitions is local.''')
				}
			}
		}
	}
}
