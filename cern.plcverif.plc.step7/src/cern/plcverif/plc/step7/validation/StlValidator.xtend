/******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 * 	 Jean-Charles Tournier - November 2019 - Add support for DirectBitAccessRef
 *****************************************************************************/
package cern.plcverif.plc.step7.validation

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit
import cern.plcverif.plc.step7.step7Language.Label
import cern.plcverif.plc.step7.step7Language.StlAccuDecrementStatement
import cern.plcverif.plc.step7.step7Language.StlAccuIncrementStatement
import cern.plcverif.plc.step7.step7Language.StlBldStatement
import cern.plcverif.plc.step7.step7Language.StlComparisonStatement
import cern.plcverif.plc.step7.step7Language.StlNopStatement
import cern.plcverif.plc.step7.step7Language.StlShiftRotateMnemonic
import cern.plcverif.plc.step7.step7Language.StlShiftRotateStatement
import cern.plcverif.plc.step7.step7Language.StlStatement
import cern.plcverif.plc.step7.step7Language.StlUnaryConstantStatement
import cern.plcverif.plc.step7.step7Language.StwBitRef
import cern.plcverif.plc.step7.util.SimpleExpressionUtil
import cern.plcverif.plc.step7.util.SimpleExpressionUtil.NotSimpleExpressionException
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import cern.plcverif.plc.step7.util.StlHelper
import com.google.common.base.Preconditions
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.validation.Check

import static cern.plcverif.plc.step7.validation.Step7LanguageValidator.*
import cern.plcverif.plc.step7.step7Language.StlStatementList
import cern.plcverif.plc.step7.step7Language.StlVerificationAssertion
import cern.plcverif.plc.step7.step7Language.DirectBitAccessRef

class StlValidator extends AbstractStep7ComposedValidator {
	@Check
	def void checkStlComparisonSyntax(StlComparisonStatement e) {
		val stlText = NodeModelUtils.findActualNodeFor(e).text;
		val mnemonic = stlText.replaceAll(";", "").trim();
		if (mnemonic.matches(".+\\s+.+")) {
			// Contains whitespace between (>|<|=|<>|>=|<>) and (I|D|R)
			error('''The STL statement '«mnemonic»' should not contain any whitespace between the comparison sign and the data type mnemonic.''', 
				e, null, ILLEGAL_STL_ELEMENT);
		}
	}
	
	/**
	 * Raises ERROR if there is any LABEL .. END_LABEL block (LabelDeclarationBlock)
	 * or any CONST .. END_CONST block (Constant DeclarationBlock) in a program unit
	 * programmed in STL.
	 */
	@Check
	def void checkNoLabelOrConstBlockInStl(ExecutableProgramUnit e) {
		if (Step7LanguageHelper.isStlBlock(e)) {
			// Check existence of LABEL block
			if (e.declarationSection.labelDeclarations !== null && !e.declarationSection.labelDeclarations.isEmpty) {
				error("STL program units shall not have label declarations ('LABEL' block).",
					e.declarationSection.labelDeclarations.get(0), 
					null,
					ILLEGAL_STL_ELEMENT);
			}
			
			// Check existence of CONST block
			if (e.declarationSection.constantDeclarations !== null && !e.declarationSection.constantDeclarations.isEmpty) {
				warning("STL program units shall not have constant declarations ('CONST' block).",
					e.declarationSection.constantDeclarations.get(0), 
					null,
					ILLEGAL_STL_ELEMENT);
			}
		}
	}
	
	/**
	 * Raises ERROR if a label's name is longer than 4 characters in a program unit programmed in STL.
	 */
	@Check
	def void checkLabelNameLength(Label e) {
		if (Step7LanguageHelper.isStlBlock(EmfHelper.getContainerOfType(e, ExecutableProgramUnit))) {
			if (e.name.length > 4) {
				warning("Labels in STL program units should not have names longer than 4 characters.",
					e, null, ILLEGAL_STL_ELEMENT
				);
			}
		}
	}
	
	/**
	 * Raises ERROR if the argument of an STL statement is out of range.
	 */
	@Check
	def void checkStlArgumentRange(StlUnaryConstantStatement e) {
		try {
			val argValue = SimpleExpressionUtil.evaluateSimpleExpression(e.arg);
		
			switch (e) {
				StlAccuIncrementStatement:
					if (!argValue.isWithin(0, 255)) {
						error("The argument of 'INC' instruction shall be within the range 0..255.", e, null, INVALID_STL_ARGUMENT);
					} 
				StlAccuDecrementStatement:
					if (!argValue.isWithin(0, 255)) {
						error("The argument of 'DEC' instruction shall be within the range 0..255.", e, null, INVALID_STL_ARGUMENT);
					} 
				StlBldStatement:
					if (!argValue.isWithin(0, 255)) {
						error("The argument of 'BLD' instruction shall be within the range 0..255.", e, null, INVALID_STL_ARGUMENT);
					} 
				StlNopStatement:
					if (!argValue.isWithin(0, 1)) {
						error("The argument of 'NOP' instruction shall be within the range 0..1.", e, null, INVALID_STL_ARGUMENT);
					} 
			}
		} catch (NotSimpleExpressionException ex) {
			// This should not happen. If it still does, nothing to do in the validation about it.
		}
	}
	
	/**
	 * Raises ERROR if an RLDA or RRDA statement has an argument.
	 */
	@Check
	def void checkRotateCcArgumentPresence(StlShiftRotateStatement e) {
		if (e.mnemonic == StlShiftRotateMnemonic.ROTATE_LEFT_DWORD_VIA_CC1 || e.mnemonic == StlShiftRotateMnemonic.ROTATE_RIGHT_DWORD_VIA_CC1) {
			if (e.arg !== null) {
				error("The rotation via CC1 instructions (RLDA, RRDA) cannot have any argument.", e, null, INVALID_STL_ARGUMENT);
			} 
		}
	}
	
	/**
	 * Raises ERROR if a status word bit (BR, OS, OV) is being assigned.
	 */
	@Check
	def void checkStwBitRefAssignment(StwBitRef e) {
		if (e.eContainer instanceof StlStatement) {
			val parentStatement = e.eContainer as StlStatement;
			if (StlHelper.isWritingStatement(parentStatement)) {
				error('''The status word bit «e.mnemonic.literal» shall not be written in an STL program.''', e, null, INVALID_STL_ARGUMENT);
			}
		}
	}
	
	 
	 @Check(NORMAL)
	def checkDirectbitAccessInStl(DirectBitAccessRef e) {
		//check if it is a direct bit access in a STL statement (but not an STL assertion...)
		if (	e.index !== null &&
				EmfHelper.isContainedInAny(e, StlStatementList) &&
				EmfHelper.isContainedInAny(e, StlVerificationAssertion) === false
			)
		{
			error('''Direct bit access is not allowed in STL''', e, null, UNSUPPORTED_STL_STATEMENT);			
		}
		
	}
	
	private static def boolean isWithin(long value, long lowerInclusive, long upperInclusive) {
		Preconditions.checkArgument(lowerInclusive < upperInclusive);
		return (value >= lowerInclusive && value <= upperInclusive);	
	}
}
