/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.validation.sa

import cern.plcverif.plc.step7.step7Language.AdditiveExpression
import cern.plcverif.plc.step7.step7Language.AndExpression
import cern.plcverif.plc.step7.step7Language.ComparisonExpression
import cern.plcverif.plc.step7.step7Language.CounterConstant
import cern.plcverif.plc.step7.step7Language.EqualityExpression
import cern.plcverif.plc.step7.step7Language.Expression
import cern.plcverif.plc.step7.step7Language.LeftValue
import cern.plcverif.plc.step7.step7Language.MultiplicativeExpression
import cern.plcverif.plc.step7.step7Language.OrExpression
import cern.plcverif.plc.step7.step7Language.PowerExpression
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall
import cern.plcverif.plc.step7.step7Language.TimerConstant
import cern.plcverif.plc.step7.step7Language.UnaryExpression
import cern.plcverif.plc.step7.step7Language.UnnamedConstant
import cern.plcverif.plc.step7.step7Language.XorExpression
import static extension cern.plcverif.plc.step7.util.Step7LanguageHelper.isLogicExpr
import cern.plcverif.plc.step7.step7Language.SingleCallParameter
import cern.plcverif.plc.step7.step7Language.CallParameterList
import cern.plcverif.plc.step7.step7Language.NamedCallParameterItem

class ExpressionComplexity {
	static def dispatch int complexityMetric(Void e) {
		return -1;
	}
	
	static def dispatch int complexityMetric(Expression e) {
		return 1;
	}

	static def dispatch int complexityMetric(TimerConstant e) {
		return 1;
	}

	static def dispatch int complexityMetric(CounterConstant e) {
		return 1;
	}

	static def dispatch int complexityMetric(OrExpression e) {
		return 1 + complexityMetric(e.left) + complexityMetric(e.right) + nestingExtra(e, e.left) + nestingExtra(e, e.right);
	}

	static def dispatch int complexityMetric(XorExpression e) {
		return 2 + complexityMetric(e.left) + complexityMetric(e.right) + nestingExtra(e, e.left) + nestingExtra(e, e.right);
	}

	static def dispatch int complexityMetric(AndExpression e) {
		return 1 + complexityMetric(e.left) + complexityMetric(e.right) + nestingExtra(e, e.left) + nestingExtra(e, e.right);
	}

	static def dispatch int complexityMetric(EqualityExpression e) {
		return 1 + complexityMetric(e.left) + complexityMetric(e.right);
	}

	static def dispatch int complexityMetric(ComparisonExpression e) {
		return 1 + complexityMetric(e.left) + complexityMetric(e.right);
	}

	static def dispatch int complexityMetric(AdditiveExpression e) {
		return 1 + complexityMetric(e.left) + complexityMetric(e.right);
	}

	static def dispatch int complexityMetric(MultiplicativeExpression e) {
		return 1 + complexityMetric(e.left) + complexityMetric(e.right);
	}

	static def dispatch int complexityMetric(PowerExpression e) {
		return 2 + complexityMetric(e.left) + complexityMetric(e.right);
	}

	static def dispatch int complexityMetric(UnaryExpression e) {
		return complexityMetric(e.expr) + nestingExtra(e, e.expr);
	}

	static def dispatch int complexityMetric(UnnamedConstant e) {
		return 0;
	}

	static def dispatch int complexityMetric(LeftValue e) {
		return 0;
	}

	static def dispatch int complexityMetric(SclSubroutineCall e) {
		if (e.callParameter === null) {
			// void call
			return 2
		}
		
		val callParam = e.callParameter;
		switch (callParam) {
			SingleCallParameter: return 2
			CallParameterList: return 3 + callParam.parameters.map [param |
				if(param instanceof NamedCallParameterItem) complexityMetric(param.value) else 1
			].reduce[a, b|a + b]
		}
		
		return 999;
	}
	
	private def static nestingExtra(Expression parentExpr, Expression childExpr) {
		if (parentExpr.isLogicExpr && childExpr.isLogicExpr && parentExpr.class != childExpr.class) {
			return 1;
		}
		return 0;
	}
}
