/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.validation

import cern.plcverif.plc.step7.step7Language.AuthorAttribute
import cern.plcverif.plc.step7.step7Language.BlockAttribute
import cern.plcverif.plc.step7.step7Language.FamilyAttribute
import cern.plcverif.plc.step7.step7Language.NameAttribute
import cern.plcverif.plc.step7.step7Language.NonRetainAttribute
import cern.plcverif.plc.step7.step7Language.ProgramUnit
import cern.plcverif.plc.step7.step7Language.Step7LanguagePackage
import cern.plcverif.plc.step7.step7Language.VersionAttribute
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.validation.Check

import static cern.plcverif.plc.step7.validation.Step7LanguageValidator.*

import static extension cern.plcverif.plc.step7.util.Step7LanguageHelper.isDataBlock

class BlockAttributeValidator extends AbstractStep7ComposedValidator {
	@Check
	def void checkBlockAttributes(AuthorAttribute e) {
		checkBlockAttributeIdValidity(e.author, e);
	}

	@Check
	def void checkBlockAttributes(NameAttribute e) {
		checkBlockAttributeIdValidity(e.content, e);
	}

	@Check
	def void checkBlockAttributes(FamilyAttribute e) {
		checkBlockAttributeIdValidity(e.family, e);
	}

	private def void checkBlockAttributeIdValidity(String str, BlockAttribute e) {
		if (str !== null && !str.matches("'.*'")) {
			// it is an ID, shall be no longer than 8 chars
			if (str.length > 8) {
				warning('''Data in block attributes shall be given as strings or they should not be longer than 8 characters.''',
					e, null, INVALID_BLOCK_ATTRIBUTE);
			}
		}
	}

	@Check
	def void checkVersionAttribute(VersionAttribute e) {
		if (!e.version.matches("'?([0-9]|1[0-5])\\.([0-9]|1[0-5])'?")) {
			warning('''Invalid version number. Version numbers should be in format x.y, where x and y are in the range of 0..15.''',
				e, Step7LanguagePackage.eINSTANCE.versionAttribute_Version, INVALID_BLOCK_ATTRIBUTE);
		}
	}
	
	@Check
	def void checkNonRetainAttribute(NonRetainAttribute e) {
		val containerBlock = EcoreUtil2.getContainerOfType(e, ProgramUnit);
		if (!containerBlock.isDataBlock) {
			error('The attribute NON_RETAIN is only supported for data blocks.', e, null, INVALID_BLOCK_ATTRIBUTE);
		}
	}
}
