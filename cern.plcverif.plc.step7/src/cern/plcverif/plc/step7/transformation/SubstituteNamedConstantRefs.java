/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.transformation;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.plc.step7.datatypes.S7Integer;
import cern.plcverif.plc.step7.step7Language.ConstDeclarationBlock;
import cern.plcverif.plc.step7.step7Language.DirectNamedRef;
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit;
import cern.plcverif.plc.step7.step7Language.IntConstant;
import cern.plcverif.plc.step7.step7Language.NamedConstantDeclaration;
import cern.plcverif.plc.step7.step7Language.NamedConstantRef;
import cern.plcverif.plc.step7.step7Language.ProgramFile;
import cern.plcverif.plc.step7.step7Language.ProgramUnit;
import cern.plcverif.plc.step7.step7Language.Step7LanguageFactory;
import cern.plcverif.plc.step7.step7Language.UnnamedConstantRef;
import cern.plcverif.plc.step7.util.SimpleExpressionUtil;
import cern.plcverif.plc.step7.util.SimpleExpressionUtil.NotSimpleExpressionException;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * This AST transformation eliminates the references to named constants (both
 * {@link DirectNamedRef}s referring to {@link NamedConstantDeclaration}s and
 * {@link NamedConstantRef}s) and substitutes them with a copy of the expression
 * that defines the given named constant. It also removed all
 * {@link NamedConstantDeclaration}s and {@link ConstDeclarationBlock}s from the
 * AST.
 */
public class SubstituteNamedConstantRefs implements IStep7AstTransformation {

	@Override
	public void transform(Iterable<ProgramFile> files) {
		for (ProgramFile file : files) {
			transform(file);
		}
	}

	@Override
	public void transform(ProgramFile file) {
		for (ProgramUnit unit : file.getProgramUnits()) {
			if (unit instanceof ExecutableProgramUnit) {
				transform((ExecutableProgramUnit) unit);
			}
		}
	}

	private static void transform(ExecutableProgramUnit unit) {
		// (1) Substitute the named constant references with their real value

		// (1a) When it is a `DirectNamedRef` referring to a
		// `NamedConstantDeclaration`, it can be simply replaced with the
		// expression that defines the constant.
		Collection<DirectNamedRef> namedConstRefs = EmfHelper.getAllContentsOfType(unit, DirectNamedRef.class, false,
				it -> it.getRef() instanceof NamedConstantDeclaration);
		for (DirectNamedRef namedConstRef : namedConstRefs) {
			NamedConstantDeclaration referredConstant = (NamedConstantDeclaration) namedConstRef.getRef();
			EcoreUtil.replace(namedConstRef, EcoreUtil.copy(referredConstant.getValue()));
		}

		// (1b) In some special cases, the named constant reference is not a
		// `DirectNamedRef`, but a `NamedConstantRef`. This is the case for CASE
		// items and array initializations.
		// In such cases they have to be substituted by UnnamedConstantRefs,
		// thus their value should be computed.
		for (NamedConstantRef namedConstRef : EmfHelper.getAllContentsOfType(unit, NamedConstantRef.class, false)) {
			NamedConstantDeclaration referredConstant = namedConstRef.getRef();
			try {
				long value = SimpleExpressionUtil.evaluateSimpleExpression(referredConstant.getValue());

				IntConstant replacementConst = Step7LanguageFactory.eINSTANCE.createIntConstant();
				replacementConst.setValue(S7Integer.create(Long.toString(value)));

				UnnamedConstantRef replacementRef = Step7LanguageFactory.eINSTANCE.createUnnamedConstantRef();
				replacementRef.setConstant(replacementConst);

				EcoreUtil.replace(namedConstRef, replacementRef);
			} catch (NotSimpleExpressionException e) {
				throw new UnsupportedOperationException(String.format(
						"The definition of the constant '%s' is expected to be a (computable) simple expression, but it is not.",
						referredConstant.getName()));
			}
		}

		// (2) Remove the constants
		for (ConstDeclarationBlock block : new ArrayList<>(unit.getDeclarationSection().getConstantDeclarations())) {
			EcoreUtil.delete(block);
		}

		// Note: This should happen before the type computation.
	}
}
