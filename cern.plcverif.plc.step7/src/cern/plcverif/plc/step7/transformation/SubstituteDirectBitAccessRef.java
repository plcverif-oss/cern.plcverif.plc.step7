/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jean-Charles Tournier - November 2019 - initial implementation
 *   Xaver Fink - August 2023 - Add support for BYTE datatype
 *******************************************************************************/
package cern.plcverif.plc.step7.transformation;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.emf.textual.EmfModelPrinter;
import cern.plcverif.plc.step7.Step7EObjectToText;
import cern.plcverif.plc.step7.datatypes.S7Integer;
import cern.plcverif.plc.step7.step7Language.AbstractSclStatement;
import cern.plcverif.plc.step7.step7Language.AbstractStlStatement;
import cern.plcverif.plc.step7.step7Language.ArrayDT;
import cern.plcverif.plc.step7.step7Language.ArrayDimensionString;
import cern.plcverif.plc.step7.step7Language.ArrayRef;
import cern.plcverif.plc.step7.step7Language.DataType;
import cern.plcverif.plc.step7.step7Language.DeclarationSection;
import cern.plcverif.plc.step7.step7Language.DirectBitAccessRef;
import cern.plcverif.plc.step7.step7Language.DirectNamedRef;
import cern.plcverif.plc.step7.step7Language.ElementaryDT;
import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum;
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit;
import cern.plcverif.plc.step7.step7Language.IntConstant;
import cern.plcverif.plc.step7.step7Language.NamedValueRef;
import cern.plcverif.plc.step7.step7Language.ProgramFile;
import cern.plcverif.plc.step7.step7Language.ProgramUnit;
import cern.plcverif.plc.step7.step7Language.QualifiedRef;
import cern.plcverif.plc.step7.step7Language.SclAssignmentStatement;
import cern.plcverif.plc.step7.step7Language.SclStatementList;
import cern.plcverif.plc.step7.step7Language.Step7LanguageFactory;
import cern.plcverif.plc.step7.step7Language.StlLoadStatement;
import cern.plcverif.plc.step7.step7Language.StlNetwork;
import cern.plcverif.plc.step7.step7Language.StlTransferStatement;
import cern.plcverif.plc.step7.step7Language.Variable;
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock;
import cern.plcverif.plc.step7.step7Language.VariableDeclarationDirection;
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine;
import cern.plcverif.plc.step7.step7Language.impl.VariableImpl;
import cern.plcverif.plc.step7.util.DataTypeUtil;
import cern.plcverif.plc.step7.util.Step7FactoryHelper;
import cern.plcverif.plc.step7.util.Step7LanguageHelper;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.EcoreUtil2;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

/**
 * This AST transformation aims at creating an ARRAY view for all
 * WORD and BYTE variables which have direct bit access (.%X). The direct bit 
 * access is then replaced by the array variable.
 * As an example, the following code:
 *  FUNCTION_BLOCK "TestBlock"
 *  	VAR_TEMP
 *     W1 : Word;
 *  	END_VAR
 *	BEGIN
 *   	#W1.%X3:=1;
 *	END_FUNCTION_BLOCK
 *	
 * will be replaced by:
 * 
 *	FUNCTION_BLOCK "TestBlock"
 *  	VAR_TEMP
 *     		W1 : Word;
 *     		W1_AS_ELEMENTARY_VAR : Word;
 *     		W1_AS_ARRAY AT W1_AS_ELEMENTARY_VAR: ARRAY [0..15] OF BOOL;
 *  	END_VAR
 *	BEGIN
 *		W1_AS_ELEMENTARY_VAR:=W1;
 *		W1_AS_ARRAY[3]:=true;
 *		W1:=W1_AS_ELEMENTARY_VAR;
 *	END_FUNCTION_BLOCK  
 *
 *  The reason for creating always an elementary variable is to be generic in order to 
 *  handle the cases where the original word variable is inside a UDT
 */
public class SubstituteDirectBitAccessRef implements IStep7AstTransformation {

	private static final String ARRAY_VIEW_POSTFIX = "_AS_ARRAY";
	private static final String ELEMENTARY_VAR_POSTFIX = "_AS_ELEMENTARY_VAR";
	
	private enum INSERT_POSITION{
		BEFORE,
		AFTER
	}
	
	@Override
	public void transform(Iterable<ProgramFile> files) {
		for (ProgramFile file : files) {
			transform(file);
		}
	}

	@Override
	public void transform(ProgramFile file) {
		for (ProgramUnit unit : file.getProgramUnits()) {
			if (unit instanceof ExecutableProgramUnit) {
				transform((ExecutableProgramUnit) unit);
			}
		}
	}

	private static void transform(ExecutableProgramUnit unit) {
		//All DirectNamedRef whose bitIndex is not null are actually direct bit access
		
		
		Collection<DirectBitAccessRef> directBitAccessRefs = EmfHelper.getAllContentsOfType(unit, DirectBitAccessRef.class, false);
		
		for (DirectBitAccessRef directBitAccessRef : directBitAccessRefs) {
			Variable arrayViewVar;
			Variable elementaryVar;
			
			//Get the variables and create them if needed
			try {
				arrayViewVar = Step7LanguageHelper.findVariable(unit, buildArrayViewVarNameFromRef(directBitAccessRef));
				elementaryVar = Step7LanguageHelper.findVariable(unit, buildElementaryVarNameFromRef(directBitAccessRef));
			} catch (Step7AstTransformationException e)
			{
				//Create variables
				arrayViewVar = createVariable(unit, buildArrayViewVarNameFromRef(directBitAccessRef));
				elementaryVar = createVariable(unit, buildElementaryVarNameFromRef(directBitAccessRef));
				
				//Set the arrayViewVar as a view of the elementaryVar
				arrayViewVar.setRef(elementaryVar);
				arrayViewVar.setReference(true);
				
				//Create the declaration lines 
				DataType accessType = Step7LanguageHelper.getBaseDatatype(directBitAccessRef);
				VariableDeclarationLine vdlArrayViewVar = createDeclarationLine(arrayViewVar, createArrayViewForType(((ElementaryDT)accessType).getType()));
				VariableDeclarationLine vdlElementaryVar = createDeclarationLine(elementaryVar, createElementaryType(((ElementaryDT)accessType).getType()));
				
				//Create the declaration block
				VariableDeclarationBlock vdb = createAndSetDirectionVariableDeclarationBlock();
				vdb.getVariables().add(vdlArrayViewVar);
				vdb.getVariables().add(vdlElementaryVar);
				
				//And finally add the declaration block to the declaration section
				DeclarationSection declSection = Step7LanguageHelper.getDeclarationSection(unit);
				declSection.getVariableDeclarations().add(vdb);
			}
			
			//0 - Create the two ref to the two new variables
			DirectNamedRef elementaryVarRef = Step7LanguageFactory.eINSTANCE.createDirectNamedRef();
			elementaryVarRef.setRef(elementaryVar);
			DirectNamedRef arrayViewVarRef = Step7LanguageFactory.eINSTANCE.createDirectNamedRef();
			arrayViewVarRef.setRef(arrayViewVar);
			ArrayRef arrayViewVarArrayRef = Step7LanguageFactory.eINSTANCE.createArrayRef();
			arrayViewVarArrayRef.setRef(arrayViewVarRef);
			
			boolean isSclBlock = Step7LanguageHelper.isSclBlock(unit);
			
			//1 - Update the elementaryVar with the current value of directBitAccessRef before the access to directBitAccessRef
			insertAssignementStatement( elementaryVarRef, directBitAccessRef.getRef(), directBitAccessRef, INSERT_POSITION.BEFORE, isSclBlock);
			
			//2 - Update the value of directBitAccessRef with elementaryVar after the access to directBitAccessRef
			// not necessarily needed for STL (only STL assertion can use the directBit access)
			insertAssignementStatement( directBitAccessRef.getRef(), elementaryVarRef, directBitAccessRef, INSERT_POSITION.AFTER, isSclBlock );
			
			//3 - Replace directBitAccessRef with arrayViewRef with the correct index
			replaceDirectBitAccessRefByArrayViewRef(arrayViewVarArrayRef, arrayViewVar, directBitAccessRef);
			
		}
		
		//printAstToFile(unit, "/tmp/test.txt");
	}
	
	private static void insertAssignementStatement(NamedValueRef leftVarRef, NamedValueRef rightVarRef, 
									DirectBitAccessRef referenceVarRefForInsert, 
									INSERT_POSITION position, boolean isSclBlock) {
			
			if (isSclBlock) {
				insertSclAssignementStatement(leftVarRef, rightVarRef, referenceVarRefForInsert, position);
			} else {
				insertStlAssignementStatement(leftVarRef, rightVarRef, referenceVarRefForInsert, position);
			}
			
	}
	
	private static void insertStlAssignementStatement(NamedValueRef leftVarRef, NamedValueRef rightVarRef,
														DirectBitAccessRef referenceVarRefForInsert, INSERT_POSITION position) {
		
		StlLoadStatement loadStatement = Step7LanguageFactory.eINSTANCE.createStlLoadStatement();
		loadStatement.setArg(EcoreUtil.copy(rightVarRef));
		
		StlTransferStatement transfertStatement = Step7LanguageFactory.eINSTANCE.createStlTransferStatement();
		transfertStatement.setArg(EcoreUtil.copy(leftVarRef));
		
		AbstractStlStatement containerStatement = EcoreUtil2.getContainerOfType(referenceVarRefForInsert.eContainer(), AbstractStlStatement.class);
		EObject insertPoint = EmfHelper.getLastAncestorBeforeContainerOfType(containerStatement, StlNetwork.class);
		
		StlNetwork stlNetwork = EcoreUtil2.getContainerOfType(containerStatement, StlNetwork.class);
		
		List<AbstractStlStatement> newStatements = new ArrayList<>();
		newStatements.add(loadStatement);
		newStatements.add(transfertStatement);
		
		if (position == INSERT_POSITION.BEFORE) {
			insertBefore(stlNetwork.getStatements(), insertPoint, newStatements);
		} else {
			insertAfter(stlNetwork.getStatements(), insertPoint, newStatements);
		}
	}

	private static void insertSclAssignementStatement(NamedValueRef leftContainerRef, NamedValueRef rightContainerRef, 
														DirectBitAccessRef referenceVarRefForInsert, 
														INSERT_POSITION position) {
		SclAssignmentStatement newAssignment = createSclAssignmentStatement(EcoreUtil.copy(leftContainerRef), EcoreUtil.copy(rightContainerRef));
		
		AbstractSclStatement containerStatement = EcoreUtil2.getContainerOfType(referenceVarRefForInsert.eContainer(), AbstractSclStatement.class);
		EObject insertPoint = EmfHelper.getLastAncestorBeforeContainerOfType(containerStatement, SclStatementList.class);
		
		SclStatementList sclStatements = EcoreUtil2.getContainerOfType(containerStatement, SclStatementList.class);
		

		if (position == INSERT_POSITION.BEFORE) {
			insertBefore(sclStatements.getStatements(), insertPoint, Lists.newArrayList(newAssignment));
		} else {
			insertAfter(sclStatements.getStatements(), insertPoint, Lists.newArrayList(newAssignment));
		}
		
	}

	private static void replaceDirectBitAccessRefByArrayViewRef(ArrayRef arrayViewVarArrayRef,
			Variable arrayViewVar, DirectBitAccessRef directBitAccessRef) {
		
		IntConstant index = Step7LanguageFactory.eINSTANCE.createIntConstant();
		//TODO Handle the big/little endian difference between S7 and TIA
		index.setValue(S7Integer.create(directBitAccessRef.getIndex()));
		arrayViewVarArrayRef.getIndex().add(index);
		
		//Create a new named reference	
		DirectNamedRef directNamedRef = Step7LanguageFactory.eINSTANCE.createDirectNamedRef();
		directNamedRef.setRef(arrayViewVar);

		arrayViewVarArrayRef.setRef(directNamedRef);
		
		EcoreUtil.replace(directBitAccessRef, arrayViewVarArrayRef);
		
	}
	
	private static SclAssignmentStatement createSclAssignmentStatement(NamedValueRef leftValueRef, NamedValueRef rightValueRef) {
		SclAssignmentStatement newAssignment = Step7LanguageFactory.eINSTANCE.createSclAssignmentStatement();
		newAssignment.setLeftValue(leftValueRef);
		newAssignment.setRightValue(rightValueRef);
		return newAssignment;
	}
	
	private static DataType createElementaryType(ElementaryTypeEnum type) {
		ElementaryDT elementType = Step7LanguageFactory.eINSTANCE.createElementaryDT();
		elementType.setType(type);
		return elementType;
	}

	private static VariableDeclarationLine createDeclarationLine(Variable var, DataType type) {
		VariableDeclarationLine vdl = Step7LanguageFactory.eINSTANCE.createVariableDeclarationLine();
		vdl.getVariables().add(var); 	
		vdl.setType(type);
		return vdl;
	}

	private static Variable createVariable(ExecutableProgramUnit unit, String name) {
		Variable var = Step7LanguageFactory.eINSTANCE.createVariable();
		var.setName(name);
		return var;
	}
	
	private static ArrayDT createArrayViewForType(ElementaryTypeEnum baseType) {
		final ArrayDT type = Step7LanguageFactory.eINSTANCE.createArrayDT();
	    type.setBaseType(Step7FactoryHelper.createElementaryDT(ElementaryTypeEnum.BOOL));
	    
	    //and the set the dimension
	    final ArrayDimensionString dim = Step7LanguageFactory.eINSTANCE.createArrayDimensionString();
	    dim.setRangeString("0.."+(DataTypeUtil.getBitWidth(baseType)-1));
	    type.getDimensions().add(dim);
	    
	    return type;
	}

	private static <T> void insertAfter(List<T> list, EObject after, List<T> itemsToInsert) {
		Preconditions.checkNotNull(list);
		Preconditions.checkNotNull(after);
		Preconditions.checkNotNull(itemsToInsert);
		
		int itemIndex = list.indexOf(after);
		if (itemIndex == -1) {
			throw new IllegalStateException("Impossible to insert, element not found.");
		}
		itemIndex += 1; //since we want to insert after
		if (itemIndex < list.size()) {
			list.addAll(itemIndex, itemsToInsert);
		} else {
			list.addAll(itemsToInsert);
		}
		
	}
	
	private static <T> void insertBefore(List<T> list, EObject before, List<T> itemsToInsert) {
		Preconditions.checkNotNull(list);
		Preconditions.checkNotNull(before);
		Preconditions.checkNotNull(itemsToInsert);

		int itemIndex = list.indexOf(before);
		if (itemIndex == -1) {
			throw new IllegalStateException("Impossible to insert statement during the direct bit access transformation, element not found.");
		}
		list.addAll(itemIndex, itemsToInsert);
	
	}

	private static String buildArrayViewVarNameFromRef(DirectBitAccessRef directBitAccessRef) {
		Preconditions.checkNotNull(directBitAccessRef);
		return getReferredVariableFullName(directBitAccessRef)+ARRAY_VIEW_POSTFIX;
	}
	
	private static String buildElementaryVarNameFromRef(DirectBitAccessRef directBitAccessRef) {
		Preconditions.checkNotNull(directBitAccessRef);
		return getReferredVariableFullName(directBitAccessRef)+ELEMENTARY_VAR_POSTFIX;
	}
	
	private static String getReferredVariableFullName(DirectBitAccessRef directBitAccessRef) {
		Preconditions.checkNotNull(directBitAccessRef);
		
		String varFullName = Step7LanguageHelper.namedReferenceAsInSourceCode(directBitAccessRef.getRef());
		//replace [ and . and % by _
		varFullName = varFullName.replaceAll("[\\.\\[%]", "_");
		//replace # and ] by nothing
		varFullName = varFullName.replaceAll("[#\\]]", "");
		return varFullName;
	}
	

	private static VariableDeclarationBlock createAndSetDirectionVariableDeclarationBlock() {		
		VariableDeclarationBlock vdb = Step7LanguageFactory.eINSTANCE.createVariableDeclarationBlock();
		vdb.setDirection(VariableDeclarationDirection.TEMP);
		return vdb;
	}

	
	@SuppressWarnings("unused")
	private static void printAstToFile(ExecutableProgramUnit unit, String filePath) {
		CharSequence stringRepresentation = EmfModelPrinter.print(unit, Step7EObjectToText.INSTANCE);
		try {
			FileWriter fw = new FileWriter(filePath);
			fw.write(stringRepresentation.toString());
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
}
