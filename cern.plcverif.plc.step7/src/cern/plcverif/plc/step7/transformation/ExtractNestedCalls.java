/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.transformation;

import static cern.plcverif.plc.step7.util.DataTypeUtil.isVoid;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.EcoreUtil2;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.plc.step7.step7Language.AbstractSclStatement;
import cern.plcverif.plc.step7.step7Language.AbstractStlStatement;
import cern.plcverif.plc.step7.step7Language.DeclarationSection;
import cern.plcverif.plc.step7.step7Language.DirectNamedRef;
import cern.plcverif.plc.step7.step7Language.Expression;
import cern.plcverif.plc.step7.step7Language.Function;
import cern.plcverif.plc.step7.step7Language.LeftValue;
import cern.plcverif.plc.step7.step7Language.ProgramFile;
import cern.plcverif.plc.step7.step7Language.ProgramUnit;
import cern.plcverif.plc.step7.step7Language.SclAssignmentStatement;
import cern.plcverif.plc.step7.step7Language.SclStatementList;
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall;
import cern.plcverif.plc.step7.step7Language.Step7LanguageFactory;
import cern.plcverif.plc.step7.step7Language.Variable;
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock;
import cern.plcverif.plc.step7.step7Language.VariableDeclarationDirection;
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine;
import cern.plcverif.plc.step7.util.Step7LanguageHelper;

/**
 * Class implementing the 'extract nested calls' transformation. The CFA does
 * not support the function calls as expression nodes, therefore the calls
 * nested into expression trees have to be extracted using temporary variables.
 */
public class ExtractNestedCalls implements IStep7AstTransformation {
	private static final String NESTED_RETVAL_VARIABLE_PREFIX = "___nested_ret_val";

	private static final Logger log = LogManager.getLogger(ExtractNestedCalls.class);

	private int nextVarId = 1;

	private final Queue<SclSubroutineCall> callsUsedAsExpression = new ArrayDeque<>();

	/**
	 * Extracts the nested calls in each program unit in each program file in
	 * {@code files}.
	 */
	@Override
	public void transform(Iterable<ProgramFile> files) {
		Preconditions.checkNotNull(files);

		for (ProgramFile file : files) {
			transform(file);
		}
	}

	/**
	 * Extracts the nested calls in each program unit in {@code file}.
	 */
	@Override
	public void transform(ProgramFile file) {
		Preconditions.checkNotNull(file);

		// find all subroutine calls in the program units
		for (ProgramUnit unit : file.getProgramUnits()) {
			TreeIterator<EObject> iter = unit.eAllContents();
			while (iter.hasNext()) {
				EObject item = iter.next();
				if (item instanceof SclSubroutineCall && callUsedAsExpression((SclSubroutineCall) item)) {
					callsUsedAsExpression.add((SclSubroutineCall) item);
				}
			}
		}

		// transform each subroutine call (if needed)
		while (!callsUsedAsExpression.isEmpty()) {
			transform(callsUsedAsExpression.remove());
		}
	}

	private void transform(SclSubroutineCall nestedCall) {
		log.trace(String.format("Transforming call '%s'...", nestedCall));
		Preconditions.checkNotNull(nestedCall);

		// transform each parameter
		Expression rootExprNode = rootOfExprTree(nestedCall);
		AbstractSclStatement containerStatement = EcoreUtil2.getContainerOfType(nestedCall.eContainer(),
				AbstractSclStatement.class);

		if (containerStatement == null && Step7LanguageHelper.isTypeConversionFunc(nestedCall)
				&& EmfHelper.getContainerOfType(nestedCall.eContainer(), AbstractStlStatement.class) != null) {
			// No need for extraction here. This can only be an assertion in
			// an STL block containing a type conversion.
			return;
		}

		extractNestedCallsIfNeeded(rootExprNode, containerStatement);
	}

	private void extractNestedCallsIfNeeded(Expression exprTree, AbstractSclStatement containerStatement) {
		Preconditions.checkNotNull(exprTree);
		Preconditions.checkNotNull(containerStatement,
				"Nested call extraction failed: no container statement provided");

		List<SclSubroutineCall> callsToExtract = new ArrayList<>();
		collectCallsOfExpressionTreeToBeExtracted(exprTree, callsToExtract);
		for (SclSubroutineCall call : callsToExtract) {
			extract(call, containerStatement);
		}
	}

	private void extract(SclSubroutineCall nestedCall, AbstractSclStatement containerStatement) {
		ProgramUnit containingBlock = EcoreUtil2.getContainerOfType(containerStatement, ProgramUnit.class);
		Preconditions.checkNotNull(containingBlock,
				"Unable to find the block containing the statement " + containerStatement);

		Preconditions.checkState(nestedCall.getCalledUnit() instanceof Function, "Only function calls may be nested.");
		Function representedFunction = (Function) nestedCall.getCalledUnit();
		Preconditions.checkState(!isVoid(representedFunction.getReturnType()),
				"Calls to VOID functions cannot be nested.");

		if (Step7LanguageHelper.isTypeConversionFunc(nestedCall.getCalledUnit())) {
			// there is no need to extract the type conversions
			return;
		}

		// create new variable with the same type
		Variable retValVar = createRetvalVariable(containingBlock, representedFunction);

		// replace nesting call with variable
		DirectNamedRef varRef = Step7LanguageFactory.eINSTANCE.createDirectNamedRef();
		varRef.setRef(retValVar);
		EcoreUtil.replace(nestedCall, varRef);

		// create new assignment before the nesting call
		SclStatementList nestingList = EcoreUtil2.getContainerOfType(containerStatement, SclStatementList.class);
		Preconditions.checkNotNull(nestingList,
				"Unable to find the statement list where the extracted assignment shall be placed.");
		EObject before = EmfHelper.getLastAncestorBeforeContainerOfType(containerStatement, SclStatementList.class);
		SclAssignmentStatement newAssignment = createSclAssignmentStatement(EcoreUtil.copy(varRef), nestedCall);
		insertBefore(nestingList.getStatements(), before, newAssignment);

		// potentially this nested call has nested calls too, then it has to be
		// transformed
		if (callUsedAsExpression(nestedCall)) {
			this.callsUsedAsExpression.add(nestedCall);
		}
	}

	private static SclAssignmentStatement createSclAssignmentStatement(LeftValue left, Expression right) {
		EmfHelper.checkNotContained(left);
		EmfHelper.checkNotContained(right);

		SclAssignmentStatement newAssignment = Step7LanguageFactory.eINSTANCE.createSclAssignmentStatement();
		newAssignment.setLeftValue(left);
		newAssignment.setRightValue(right);
		return newAssignment;
	}

	private Variable createRetvalVariable(ProgramUnit containingBlock, Function representedFunction) {
		Preconditions.checkNotNull(containingBlock);
		Preconditions.checkNotNull(representedFunction);

		// create new AST variable (Variable)
		Variable astRetValVar = Step7LanguageFactory.eINSTANCE.createVariable();
		astRetValVar.setName(NESTED_RETVAL_VARIABLE_PREFIX + (nextVarId++));

		// create new variable declaration line
		VariableDeclarationLine vdl = Step7LanguageFactory.eINSTANCE.createVariableDeclarationLine();
		vdl.getVariables().add(astRetValVar);
		vdl.setType(EcoreUtil.copy(representedFunction.getReturnType()));

		// create new (temporary) variable declaration block
		VariableDeclarationBlock vdb = Step7LanguageFactory.eINSTANCE.createVariableDeclarationBlock();
		vdb.setDirection(VariableDeclarationDirection.TEMP);
		vdb.getVariables().add(vdl);

		// insert the new variable declaration block to the declaration section
		// of the containing block
		DeclarationSection declSection = Step7LanguageHelper.getDeclarationSection(containingBlock);
		declSection.getVariableDeclarations().add(vdb);

		return astRetValVar;
	}

	private static Expression rootOfExprTree(Expression expr) {
		return EmfHelper.getFarthestContinousAncestorOfType(expr, Expression.class);
	}

	/**
	 * Inserts {@code itemToInsert} into the list {@code list} before the
	 * element {@before}.
	 */
	private static <T> void insertBefore(List<T> list, EObject before, T itemToInsert) {
		Preconditions.checkNotNull(list);
		Preconditions.checkNotNull(before);
		Preconditions.checkNotNull(itemToInsert);

		int itemIndex = list.indexOf(before);
		if (itemIndex == -1) {
			throw new IllegalStateException("Impossible to insert, element not found.");
		}
		list.add(itemIndex, itemToInsert);
	}

	/**
	 * Collects each subroutine call in the expression tree. It DOES NOT check
	 * the expressions in the contained subroutine calls, i.e., the traversal of
	 * the expression tree will stop at each subroutine call node.
	 */
	private static void collectCallsOfExpressionTreeToBeExtracted(Expression rootExprNode,
			List<SclSubroutineCall> calls) {
		Preconditions.checkNotNull(calls);
		Preconditions.checkNotNull(rootExprNode);

		if (rootExprNode instanceof SclSubroutineCall) {
			calls.add((SclSubroutineCall) rootExprNode);
			// stop here: do not explore the calls nested inside this call,
			// it will be performed later
		} else {
			// generic case for Expression
			for (EObject content : rootExprNode.eContents()) {
				collectCallsOfExpressionTreeToBeExtracted((Expression) content, calls);
			}
		}
	}

	private static boolean callUsedAsExpression(SclSubroutineCall call) {
		if (call.eContainer() instanceof SclStatementList) {
			// e.g. void FC call or FB call
			return false;
		}

		if (call.eContainer() instanceof SclAssignmentStatement) {
			SclAssignmentStatement parentAssignment = (SclAssignmentStatement) call.eContainer();
			return parentAssignment.getRightValue() != call;
		}

		return true;
	}
}
