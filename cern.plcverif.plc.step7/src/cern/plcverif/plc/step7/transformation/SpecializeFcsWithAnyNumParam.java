/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.transformation;

import static cern.plcverif.plc.step7.util.DataTypeUtil.isAnyNum;
import static cern.plcverif.plc.step7.util.Step7LanguageHelper.getContainerProgramFile;
import static cern.plcverif.plc.step7.util.Step7LanguageHelper.getDataType;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.plc.step7.step7Language.CallParameterItem;
import cern.plcverif.plc.step7.step7Language.CallParameterList;
import cern.plcverif.plc.step7.step7Language.DataType;
import cern.plcverif.plc.step7.step7Language.ElementaryDT;
import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum;
import cern.plcverif.plc.step7.step7Language.Function;
import cern.plcverif.plc.step7.step7Language.NamedCallParameterItem;
import cern.plcverif.plc.step7.step7Language.ParameterDT;
import cern.plcverif.plc.step7.step7Language.ProgramFile;
import cern.plcverif.plc.step7.step7Language.SingleCallParameter;
import cern.plcverif.plc.step7.step7Language.Step7LanguageFactory;
import cern.plcverif.plc.step7.step7Language.SubroutineCall;
import cern.plcverif.plc.step7.step7Language.Variable;
import cern.plcverif.plc.step7.typecomputer.IElementaryTypeConversion;
import cern.plcverif.plc.step7.typecomputer.S7300ElementaryTypeConversion;
import cern.plcverif.plc.step7.typecomputer.Step7TypeComputer;
import cern.plcverif.plc.step7.util.Step7LanguageHelper;

/**
 * The STEP 7 languages permit the definition of built-in functions that contain
 * inputs, outputs and/or return values with the type ANY_NUM. For example, ABS
 * can be both called with INT and REAL parameters. In those cases, the return
 * value's type will be determined by the type of the input parameters.
 *
 * This transformation creates duplicates of such function declarations,
 * specialized with the given parameter types. E.g., for ABS called both with
 * INT and REAL parameters, two copies will be created: ABS#INT and ABS#REAL.
 */
public class SpecializeFcsWithAnyNumParam implements IStep7AstTransformation {
	private static final class FunctionTypePair {
		private Function function;
		private ElementaryTypeEnum type;

		public FunctionTypePair(Function function, ElementaryTypeEnum type) {
			this.function = function;
			this.type = type;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof FunctionTypePair) {
				FunctionTypePair other = (FunctionTypePair) obj;
				return other.function == this.function && other.type == this.type;
			}
			return false;
		}

		@Override
		public int hashCode() {
			return Objects.hashCode(function, type);
		}
	}

	private Map<FunctionTypePair, Function> specializedFcCopies = null;
	private Step7TypeComputer astType = null;
	private IElementaryTypeConversion plcTypeConversion = new S7300ElementaryTypeConversion();
	// TODO PLC type should be replaceable

	@Override
	public void transform(ProgramFile file) {
		transform(Arrays.asList(file));
	}

	@Override
	public void transform(Iterable<ProgramFile> files) {
		specializedFcCopies = new HashMap<>();
		astType = Step7TypeComputer.compute(files);

		// (1) Collect the FCs which are using ANY_NUM
		Set<Function> fcsWithAny = new HashSet<>();
		for (ProgramFile file : files) {
			//@formatter:off
			file.getProgramUnits().stream()
				.filter(it -> it instanceof Function)
				.map(it -> (Function)it)
				.filter(it -> hasAnyNumDataType(it))
				.forEach(it -> fcsWithAny.add(it));
			//@formatter:on
		}

		// (2) Specialize calls of these FCs
		for (ProgramFile file : files) {
			for (SubroutineCall anyCall : EmfHelper.getAllContentsOfType(file, SubroutineCall.class, false,
					it -> fcsWithAny.contains(it.getCalledUnit()))) {
				specialize(anyCall);
			}
		}

		// (3) Remove FC declarations containing ANY_NUMs
		for (Function fcWithAny : fcsWithAny) {
			EcoreUtil.delete(fcWithAny);
		}
	}

	private static void replace(SubroutineCall anyCall, Function specializedFc) {
		// Change the called function to 'specializedFc'
		Preconditions.checkState(anyCall.getCalledUnit() instanceof Function);
		anyCall.setCalledUnit(specializedFc);

		// Change all parameters (only NamedCallParameterItems have to be
		// changed)
		for (NamedCallParameterItem paramItem : EmfHelper.getAllContentsOfType(anyCall.getCallParameter(),
				NamedCallParameterItem.class, true)) {
			replace(paramItem, specializedFc);
		}
	}

	private static void replace(NamedCallParameterItem paramItem, Function specializedFc) {
		String varName = paramItem.getParameter().getName();
		Variable correspondingVar = Step7LanguageHelper.findVariable(specializedFc, varName);
		// Here we don't use EcoreUtil.replace -- we are not replacing the
		// Variable, instead we change paramItem.param
		paramItem.setParameter(correspondingVar);
	}

	private ElementaryTypeEnum findTypeReplacingAnyNum(SubroutineCall anyCall) {
		if (anyCall.getCallParameter() == null) {
			throw new Step7AstTransformationException("Unable to infer ANY_NUM type for parameterless call.");
		}

		if (anyCall.getCallParameter() instanceof SingleCallParameter) {
			// In case of 'SingleCallParameter' the only input variable
			// should determine the type to be used
			Optional<Variable> inputVar = Step7LanguageHelper
					.tryGetOnlyInputVariable((Function) anyCall.getCalledUnit());
			Preconditions.checkState(inputVar.isPresent(), "No input variable has been found.");
			Preconditions.checkState(isAnyNum(getDataType(inputVar.get())),
					"The only input variable cannot be used to determine the concrete type for ANY_NUM.");

			// Fetch type for the passed value
			// -- this will replace the ANY_NUM type
			Optional<DataType> paramType = astType
					.tryGetType(((SingleCallParameter) anyCall.getCallParameter()).getParameter());
			Preconditions.checkState(paramType.isPresent());
			Preconditions.checkState(paramType.get() instanceof ElementaryDT,
					"The type of the value to be used to specialize ANY_NUM is not an elementary type.");
			return ((ElementaryDT) paramType.get()).getType();
		} else if (anyCall.getCallParameter() instanceof CallParameterList) {
			// If there are many parameters, each of them referring to an
			// ANY_NUM parameter shall be checked
			CallParameterList paramList = (CallParameterList) anyCall.getCallParameter();
			ElementaryTypeEnum resultType = null;
			for (CallParameterItem paramItem : paramList.getParameters()) {
				if (paramItem instanceof NamedCallParameterItem) {
					NamedCallParameterItem param = (NamedCallParameterItem) paramItem;
					if (isAnyNum(getDataType(param.getParameter()))) {
						// Now we know that 'param' refers to an ANY_NUM
						Optional<DataType> paramType = astType.tryGetType(param.getValue());
						if (paramType.isPresent() && paramType.get() instanceof ElementaryDT) {
							ElementaryTypeEnum paramElemType = ((ElementaryDT) paramType.get()).getType();
							if (resultType == null
									|| plcTypeConversion.implicitlyConvertableTo(resultType, paramElemType)) {
								resultType = paramElemType;
							} else if (resultType == paramElemType
									|| plcTypeConversion.implicitlyConvertableTo(paramElemType, resultType)) {
								// nothing to do
							} else {
								throw new Step7AstTransformationException(
										"Inconsistent types for ANY_NUM: " + paramElemType + " and " + resultType);
							}
						}
					}
				}
			}
			Preconditions.checkState(resultType != null);
			return resultType;
		}

		throw new Step7AstTransformationException("Unable to infer ANY_NUM type for " + anyCall);
	}

	private Function findOrCreateSpecialized(Function function, ElementaryTypeEnum type) {
		FunctionTypePair key = new FunctionTypePair(function, type);
		if (!specializedFcCopies.containsKey(key)) {
			Function specializedFc = createSpecializedCopy(function, type);
			specializedFcCopies.put(key, specializedFc);
		}
		return specializedFcCopies.get(key);
	}

	private void specialize(SubroutineCall anyCall) {
		Preconditions.checkArgument(anyCall.getCalledUnit() instanceof Function,
				"Only FCs can have ANY_NUM parameters.");

		// 1) Find out the type to be used instead of ANY_NUM
		ElementaryTypeEnum typeReplacingAnyNum = findTypeReplacingAnyNum(anyCall);
		System.out.printf("Specializing %s with %s...%n", anyCall.getCalledUnit().getName(),
				typeReplacingAnyNum.getLiteral());

		// 2) Find the replacement of called unit
		Function specializedFc = findOrCreateSpecialized((Function) anyCall.getCalledUnit(), typeReplacingAnyNum);

		// 3) Replace! (including the necessary changes in the call parameters)
		replace(anyCall, specializedFc);
	}

	/**
	 * Creates a copy of the given {@code function} that is identical, except
	 * all occurrences of ANY_NUM are replaced with the given {@code type}.
	 *
	 * @param function
	 *            The function to be specialized (copied).
	 * @param type
	 *            The elementary type that should replace the occurrences of
	 *            ANY_NUM
	 * @return The specialized copy of {@code function} that does not contain
	 *         ANY_NUMs.
	 */
	private static Function createSpecializedCopy(Function function, ElementaryTypeEnum type) {
		// Make a copy in the same program file
		Function copy = EcoreUtil.copy(function);
		Preconditions.checkState(copy.eContainer() == null);
		copy.setName(String.format("%s#%s", function.getName(), type.getLiteral().toUpperCase()));
		getContainerProgramFile(function).getProgramUnits().add(copy);
		assert copy.eContainer() != null;
		Preconditions.checkNotNull(copy.eContainer());

		// Replace all ANY_NUMs with the given type
		Collection<ParameterDT> anyNums = EmfHelper.getAllContentsOfType(copy, ParameterDT.class, false,
				it -> isAnyNum(it));
		Preconditions.checkState(!anyNums.isEmpty(),
				"Error: createSpecializedCopy was called for a function that does not contain ANY_NUMs.");

		ElementaryDT replacementType = Step7LanguageFactory.eINSTANCE.createElementaryDT();
		replacementType.setType(type);

		for (ParameterDT anyNum : anyNums) {
			EcoreUtil.replace(anyNum, EcoreUtil.copy(replacementType));
		}

		return copy;
	}

	private static boolean hasAnyNumDataType(Function function) {
		if (isAnyNum(function.getReturnType())) {
			return true;
		}

		for (ParameterDT paramDt : EmfHelper.getAllContentsOfType(function.getDeclarationSection(), ParameterDT.class,
				false)) {
			if (isAnyNum(paramDt)) {
				return true;
			}
		}

		return false;
	}

}
