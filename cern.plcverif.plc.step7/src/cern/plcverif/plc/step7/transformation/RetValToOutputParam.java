/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.transformation;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.plc.step7.step7Language.AssignmentOperator;
import cern.plcverif.plc.step7.step7Language.CallParameterList;
import cern.plcverif.plc.step7.step7Language.Function;
import cern.plcverif.plc.step7.step7Language.LeftValue;
import cern.plcverif.plc.step7.step7Language.NamedCallParameterItem;
import cern.plcverif.plc.step7.step7Language.ProgramFile;
import cern.plcverif.plc.step7.step7Language.RetValCallParameterItem;
import cern.plcverif.plc.step7.step7Language.SclAssignmentStatement;
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall;
import cern.plcverif.plc.step7.step7Language.SingleCallParameter;
import cern.plcverif.plc.step7.step7Language.Step7LanguageFactory;
import cern.plcverif.plc.step7.util.Step7LanguageHelper;

/**
 * Class to perform the following transformation:
 *
 * {targetVar} := fCall(); ---> {targetVar} := fCall(RET_VAL => {targetVar});
 *
 */
public class RetValToOutputParam implements IStep7AstTransformation {

	@Override
	public void transform(Iterable<ProgramFile> files) {
		for (ProgramFile file : files) {
			transform(file);
		}
	}

	@Override
	public void transform(ProgramFile file) {
		// Find all SCL assignment statements where the right-hand side is a
		// SclSubroutineCall
		List<SclAssignmentStatement> retvalAssignments = new ArrayList<>();
		TreeIterator<EObject> iter = file.eAllContents();
		while (iter.hasNext()) {
			EObject item = iter.next();
			if (item instanceof SclAssignmentStatement
					&& ((SclAssignmentStatement) item).getRightValue() instanceof SclSubroutineCall) {
				SclSubroutineCall call = (SclSubroutineCall) ((SclAssignmentStatement) item).getRightValue();
				if (!Step7LanguageHelper.isTypeConversionFunc(call)) {
					// no need for any modification in case of type conversions
					retvalAssignments.add((SclAssignmentStatement) item);
				}
			}
		}

		for (SclAssignmentStatement retvalAssignment : retvalAssignments) {
			transform(retvalAssignment);
		}
	}

	private static void transform(SclAssignmentStatement retvalAssignment) {
		Preconditions.checkArgument(retvalAssignment.getRightValue() instanceof SclSubroutineCall);

		// {targetVar} := fCall();
		// --->
		// {targetVar} := fCall(RET_VAL => {targetVar});
		LeftValue targetVar = retvalAssignment.getLeftValue();
		SclSubroutineCall fCall = (SclSubroutineCall) retvalAssignment.getRightValue();

		// The fCall may not have named parameters; let's fix this first.
		CallParameterList fCallParams;
		if (fCall.getCallParameter() == null) {
			fCallParams = Step7LanguageFactory.eINSTANCE.createCallParameterList();
			fCall.setCallParameter(fCallParams);
		} else if (fCall.getCallParameter() instanceof SingleCallParameter) {
			Preconditions.checkState(fCall.getCalledUnit() instanceof Function,
					"Single call parameters can only be used in function calls.");
			Function calledFunction = (Function) fCall.getCalledUnit();
			fCallParams = Step7LanguageFactory.eINSTANCE.createCallParameterList();

			NamedCallParameterItem namedParamItem = Step7LanguageFactory.eINSTANCE.createNamedCallParameterItem();
			namedParamItem.setParameter(Step7LanguageHelper.getOnlyInputVariable(calledFunction));
			namedParamItem.setAssignmentOperator(AssignmentOperator.GENERAL);
			namedParamItem.setValue(((SingleCallParameter) fCall.getCallParameter()).getParameter());
			fCallParams.getParameters().add(namedParamItem);

			fCall.setCallParameter(fCallParams);
		} else {
			Preconditions.checkState(fCall.getCallParameter() instanceof CallParameterList,
					"Unknown parameter type: " + fCall.getCallParameter().getClass().getName());
			fCallParams = (CallParameterList) fCall.getCallParameter();
		}

		RetValCallParameterItem retvalParamItem = Step7LanguageFactory.eINSTANCE.createRetValCallParameterItem();
		retvalParamItem.setAssignmentOperator(AssignmentOperator.FC_OUTPUT);
		retvalParamItem.setValue(targetVar);
		fCallParams.getParameters().add(retvalParamItem);

		EcoreUtil.replace(retvalAssignment, fCall);
	}

}
