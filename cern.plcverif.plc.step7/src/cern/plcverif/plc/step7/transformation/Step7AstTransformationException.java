/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.transformation;

public class Step7AstTransformationException extends RuntimeException {
	private static final long serialVersionUID = 1299633109928941961L;

	public Step7AstTransformationException(String message) {
		super(message);
	}

	public Step7AstTransformationException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
