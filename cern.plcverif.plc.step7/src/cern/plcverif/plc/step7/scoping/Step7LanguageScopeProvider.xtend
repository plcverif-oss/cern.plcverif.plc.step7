/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.scoping

import cern.plcverif.plc.step7.step7Language.ArrayDT
import cern.plcverif.plc.step7.step7Language.ArrayRef
import cern.plcverif.plc.step7.step7Language.DataBlock
import cern.plcverif.plc.step7.step7Language.DataBlockInitialAssignment
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.FbOrUdtDT
import cern.plcverif.plc.step7.step7Language.NamedCallParameterItem
import cern.plcverif.plc.step7.step7Language.NamedElement
import cern.plcverif.plc.step7.step7Language.NamedValueRef
import cern.plcverif.plc.step7.step7Language.ProgramUnit
import cern.plcverif.plc.step7.step7Language.QualifiedRef
import cern.plcverif.plc.step7.step7Language.Step7LanguagePackage
import cern.plcverif.plc.step7.step7Language.StructDT
import cern.plcverif.plc.step7.step7Language.SubroutineCall
import cern.plcverif.plc.step7.step7Language.UserDefinedDataType
import cern.plcverif.plc.step7.step7Language.Variable
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.Scopes
import org.eclipse.xtext.scoping.impl.FilteringScope
import org.eclipse.xtext.scoping.impl.SimpleScope
import org.eclipse.xtext.util.SimpleAttributeResolver

import static extension cern.plcverif.plc.step7.util.Step7LanguageHelper.*
import cern.plcverif.plc.step7.step7Language.DirectRef
import cern.plcverif.plc.step7.step7Language.DirectBitAccessRef

//import org.eclipse.xtext.nodemodel.util.NodeModelUtils

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class Step7LanguageScopeProvider extends AbstractStep7LanguageScopeProvider {
	override getScope(EObject context, EReference reference) {
//		if (context instanceof DataBlockInitialAssignment) {
//			if (reference == Step7LanguagePackage.Literals.DATA_BLOCK_INIT IAL_ASSIGNMENT__LEFT_VALUE) {
//				val containingDataBlock = EcoreUtil2.getContainerOfType(context, DataBlock);
//				val scope = scopeOfContainedVariables(containingDataBlock.structure); 
//				return scope
//			}
//		}

//		val str = NodeModelUtils.getTokenText(NodeModelUtils.findActualNodeFor(context))
//		println('''getScope for '«str»' ''')		

		if (context instanceof NamedValueRef) {
			if (context.eContainer instanceof QualifiedRef && (context.eContainer as QualifiedRef).ref == context) {
				val scope = scopeOfContainedVariables((context.eContainer as QualifiedRef).prefix)
				// println(context.toString + " --scope--> " + scope.allElements.map[toString].join(', '))
				return scope
			}

			if (context.eContainer instanceof ArrayRef && (context.eContainer as ArrayRef).index.contains(context)) {
				// scope for array indices (to support e.g. array1[i])
				val scope = getScope(EcoreUtil2.getContainerOfType(context, ProgramUnit), reference);
				return scope
			}
			
			if (context.eContainer instanceof DirectBitAccessRef ) {
				// scope for array indices (to support e.g. array1[i])
				val scope = getScope(EcoreUtil2.getContainerOfType(context, ProgramUnit), reference);
				return scope
			}

			if (context.eContainer instanceof ArrayRef && context.eContainer.eContainer instanceof QualifiedRef &&
				(context.eContainer.eContainer as QualifiedRef).ref == context.eContainer) {
				val scope = scopeOfContainedVariables((context.eContainer.eContainer as QualifiedRef).prefix)
				return scope
			}

			if (context instanceof DirectRef && context.eContainer instanceof QualifiedRef &&
				(context.eContainer as QualifiedRef).prefix === context &&
				EcoreUtil2.getContainerOfType(context, DataBlockInitialAssignment) !== null) {
				// scopes for DB initializations
				val containingDataBlock = EcoreUtil2.getContainerOfType(context, DataBlock);
				val scope = scopeOfContainedVariables(containingDataBlock.structure);
				return scope
			}
			if (context instanceof DirectNamedRef && context.eContainer instanceof ArrayRef &&
				context.eContainer.eContainer instanceof QualifiedRef &&
				(context.eContainer.eContainer as QualifiedRef).prefix === context.eContainer &&
				EcoreUtil2.getContainerOfType(context, DataBlockInitialAssignment) !== null) {
				// scopes for DB initializations
				// ugly quickfix for array initializations
				val containingDataBlock = EcoreUtil2.getContainerOfType(context, DataBlock);
				val scope = scopeOfContainedVariables(containingDataBlock.structure);
				return scope
			}
			if (context.eContainer instanceof DataBlockInitialAssignment) {
				// unqualified DB initialization
				val containingDataBlock = EcoreUtil2.getContainerOfType(context, DataBlock);
				val scope = scopeOfContainedVariables(containingDataBlock.structure);
				return scope
			}
			if (context.eContainer instanceof ArrayRef &&
				context.eContainer.eContainer instanceof DataBlockInitialAssignment) {
				// unqualified array DB initialization
				val containingDataBlock = EcoreUtil2.getContainerOfType(context, DataBlock);
				val scope = scopeOfContainedVariables(containingDataBlock.structure);
				return scope
			}

			// if (context instanceof DirectNamedRef && context.eContainer instanceof )
			// filtering scope is a quickfix to include the variables which are declared inside a STRUCT to be found using a non-qualified name
			return new FilteringScope(super.getScope(context, reference), [x |
				x.EObjectOrProxy.eContainer === null || !(
				x.EObjectOrProxy.eContainer.eContainer instanceof StructDT
			)
			]);
		}

		if (context instanceof NamedCallParameterItem) {
			if (reference == Step7LanguagePackage.eINSTANCE.namedCallParameterItem_Parameter) {
				// both for SCL and STL calls
				val callContainer = EcoreUtil2.getContainerOfType(context, typeof(SubroutineCall))
				if (callContainer !== null) {
					val allVariables = callContainer.allVariables;
					if (allVariables !== null) {
						return Scopes.caseInsensitiveScopeFor(allVariables)
					}
					return IScope.NULLSCOPE
				}
			}
		}

//		if (reference == SclGrammarPackage.Literals.BLOCK_REF__REF && context instanceof NumericBlockRef) {
//			val Model root = EcoreUtil2.getContainerOfType(context, typeof(Model));
//			return getNumericBlockRefScope(root, context as NumericBlockRef);
//		}
//		if (reference == SclGrammarPackage.Literals.BLOCK_REF__REF && context instanceof NumericBlockRefString) {
//			val Model root = EcoreUtil2.getContainerOfType(context, typeof(Model));
//			return getNumericBlockRefScope(root, context as NumericBlockRefString);
//		}
		return super.getScope(context, reference);
	}

	def Iterable<? extends EObject> allVariables(SubroutineCall e) {
		return e.calledUnit.allDefinedVariables
	}
	
	def static IScope scopeOfContainedVariables(EObject e) {
		val vars = containedVariables(e);
		if (e === null || vars === null) {
			return IScope.NULLSCOPE;
		} else {
			return Scopes.caseInsensitiveScopeFor(vars);
		}
	}

	def static dispatch Iterable<? extends Variable> containedVariables(Void e) {
		// println("No specific scoping rule for null.")
		// Here most probably we deal with an incomplete AST.
		return null;
	}

	def static dispatch Iterable<? extends Variable> containedVariables(EObject e) {
		println("No specific scoping rule for " + e.class.name + " " + e)
		return null;
	}
	
	def static dispatch Iterable<? extends Variable> containedVariables(NamedElement e) {
		// Here most probably we deal with an incomplete or invalid AST.
		return null;
	}

	def static dispatch Iterable<? extends Variable> containedVariables(NamedValueRef e) {
		print("Unknown VariableRef" + e.class.name)
		return null;
	}

	def static dispatch Iterable<? extends Variable> containedVariables(QualifiedRef e) {
		return containedVariables(e.ref)
	}

	def static dispatch Iterable<? extends Variable> containedVariables(ArrayRef e) {
		return containedVariables(e.ref)
	}
	
	def static dispatch Iterable<? extends Variable> containedVariables(DirectBitAccessRef e) {
		return containedVariables(e.ref)
	}

	def static dispatch Iterable<? extends Variable> containedVariables(DirectNamedRef e) {
		return containedVariables(e.ref)
	}

	def static dispatch Iterable<? extends Variable> containedVariables(Variable e) {
		return containedVariables(e.dataType)
	}

	def static dispatch Iterable<? extends Variable> containedVariables(StructDT e) {
		return e.containedVariables
	}

	def static dispatch Iterable<? extends Variable> containedVariables(FbOrUdtDT e) {
		return containedVariables(e.type)
	}

	def static dispatch Iterable<? extends Variable> containedVariables(ArrayDT e) {
		return containedVariables(e.baseType)
	}

	def static dispatch Iterable<? extends Variable> containedVariables(UserDefinedDataType e) {
		return containedVariables(e.declaration)
	}

	def static dispatch Iterable<? extends Variable> containedVariables(DataBlock e) {
		return containedVariables(e.structure)
	}
	
	def static dispatch Iterable<? extends Variable> containedVariables(ProgramUnit e) {
		return e.allDefinedVariables
	}
	
	private static def caseInsensitiveScopeFor(Class<Scopes> dummy, Iterable<? extends EObject> iter) {
		return new SimpleScope(IScope.NULLSCOPE,
			Scopes.scopedElementsFor(iter, QualifiedName.wrapper(SimpleAttributeResolver.NAME_RESOLVER)), true); // PERF better name resolver
	}
}
