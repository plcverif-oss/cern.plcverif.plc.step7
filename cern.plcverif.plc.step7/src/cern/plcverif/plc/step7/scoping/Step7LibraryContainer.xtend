/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.scoping

import org.eclipse.xtext.resource.impl.AbstractContainer
import org.eclipse.emf.common.util.URI
//import org.eclipse.xtext.resource.IResourceServiceProvider

class Step7LibraryContainer extends AbstractContainer {
	
	public static final URI BUILTIN_URI = URI.createURI("platform:/plugin/cern.plcverif.plc.step7/cern/plcverif/plc/step7/lib/builtin.scl");
    
	
	override getResourceDescriptions() {
	
	}
	
}