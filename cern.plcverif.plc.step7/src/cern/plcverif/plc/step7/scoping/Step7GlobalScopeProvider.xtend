/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/

package cern.plcverif.plc.step7.scoping

import com.google.common.base.Predicate
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.resource.IEObjectDescription
import org.eclipse.xtext.resource.IResourceDescription
import org.eclipse.xtext.resource.IResourceDescriptions
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.impl.DefaultGlobalScopeProvider
import org.eclipse.xtext.scoping.impl.SelectableBasedScope

//class Step7GlobalScopeProvider extends ImportUriGlobalScopeProvider {
//	public static final URI BUILTIN_URI = URI.createURI("platform:/plugin/cern.plcverif.plc.step7/cern/plcverif/plc/step7/lib/builtin.scl");
//    
//    override protected def LinkedHashSet<URI> getImportedUris(Resource resource)
//    {
//    	// This idea is based on http://www.davehofmann.de/?p=232 (David Hofmann).
//        val LinkedHashSet<URI> importedURIs = super.getImportedUris(resource);
//        importedURIs.addAll(resource.resourceSet.resources.map[r | r.URI]);
//        importedURIs.add(BUILTIN_URI);
//        return importedURIs;
//    }
//}
class Step7GlobalScopeProvider extends DefaultGlobalScopeProvider {
	public static final URI BUILTIN_URI = URI.createURI(
		"platform:/plugin/cern.plcverif.plc.step7/cern/plcverif/plc/step7/lib/builtin.scl");

// 	@Inject
//	private Provider<LoadOnDemandResourceDescriptions> loadOnDemandDescriptions;

	override protected getScope(IScope parent, Resource context, boolean ignoreCase, EClass type, Predicate<IEObjectDescription> filter) {
			val IResourceDescriptions descriptions = this.getResourceDescriptions(context);
			// val scope = createLazyResourceScope(parent, BUILTIN_URI, descriptions, type, filter, ignoreCase);
			
			val IResourceDescription description = descriptions.getResourceDescription(BUILTIN_URI);
			val scope = SelectableBasedScope.createScope(parent, description, filter, type, ignoreCase);
			val scope1 = super.getScope(scope, context, ignoreCase, type, filter);
	
			return scope1;
	}


	override getResourceDescriptions(Resource resource) {
//		val IResourceDescriptions result = super.getResourceDescriptions(resource);
//		
//		val LoadOnDemandResourceDescriptions demandResourceDescriptions = loadOnDemandDescriptions.get();
//		demandResourceDescriptions.initialize(result, #{BUILTIN_URI}, resource);
//		return demandResourceDescriptions;

		if (resource.resourceSet.getResource(BUILTIN_URI, false) === null) {
			val lib = EcoreUtil2.getResource(resource, BUILTIN_URI.toString());
			EcoreUtil.resolveAll(lib);
			resource.resourceSet.resources.add(lib);
		}	
		return super.getResourceDescriptions(resource);
	}
}
