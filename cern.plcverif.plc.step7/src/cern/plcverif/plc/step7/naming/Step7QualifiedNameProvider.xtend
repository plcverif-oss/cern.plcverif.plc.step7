package cern.plcverif.plc.step7.naming

import org.eclipse.xtext.naming.DefaultDeclarativeQualifiedNameProvider
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.emf.ecore.EObject
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine
import cern.plcverif.plc.step7.step7Language.StructDT

class Step7QualifiedNameProvider extends DefaultDeclarativeQualifiedNameProvider implements IQualifiedNameProvider {
	
	override getFullyQualifiedName(EObject obj) {
		// To fix FQNs of variables within structs
		// E.g. a : STRUCT b : BOOL; END_STRUCT inside FB foo
		// foo.b --> foo.a.b
		if (obj instanceof StructDT && obj.eContainer instanceof VariableDeclarationLine) {
			val vdl = obj.eContainer as VariableDeclarationLine;
			return super.getFullyQualifiedName(vdl.variables.get(0));
		}
		
		return super.getFullyQualifiedName(obj);
	}
	
}