/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.util;

import java.util.Optional;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.plc.step7.step7Language.DataType;
import cern.plcverif.plc.step7.step7Language.Variable;
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock;

/**
 * Utility methods related to STEP 7 variable views (e.g., {@code a AT b : type;})
 */
public final class VariableViewUtil {
	private VariableViewUtil() {
		// Utility class.
	}
	
	public static boolean areCompatible(Variable viewerVar, Variable storageVar) {
		// They are compatible if there is no incompatibility diagnosis.
		// PERF can be improved
		return diagnoseIncompatibility(viewerVar, storageVar).isPresent() == false;
	}
	
	public static Optional<String> diagnoseIncompatibility(Variable viewerVar, Variable storageVar) {
		DataType viewerType = Step7LanguageHelper.getDataType(viewerVar);
		DataType storageType = Step7LanguageHelper.getDataType(storageVar);
		
		// view to/from ANY or POINTER is not supported by PLCverif
		if (DataTypeUtil.isParamType(viewerType)) {
			return Optional.of("Parameter types are not supported as viewer variable types in PLCverif.");
		}
		
		if (DataTypeUtil.isParamType(storageType)) {
			return Optional.of("Parameter types are not supported as storage variable types in PLCverif.");
		}
		
		if (DataTypeUtil.isFbDataType(viewerType) || DataTypeUtil.isFbDataType(storageType)) {
			return Optional.of("Function block instances cannot be used in variable views.");
		}
		
		VariableDeclarationBlock viewerBlock = EmfHelper.getContainerOfType(viewerVar, VariableDeclarationBlock.class);
		VariableDeclarationBlock storageBlock = EmfHelper.getContainerOfType(storageVar, VariableDeclarationBlock.class);
		
		// the two variables should be defined in the same block or at least the direction should be the same
		if (viewerBlock != storageBlock && viewerBlock.getDirection() != storageBlock.getDirection()) {
			return Optional.of("The viewer and storage variables need to be defined in variable blocks with the same type.");
		}
		
		// the defining variable blocks should be in the same program blocks
		if (viewerBlock.eContainer() != storageBlock.eContainer()) {
			return Optional.of("The viewer and storage variables need to be defined in the same program unit.");
		}
		
		// viewer needs to be smaller or equal in size (SCL handbook, p. 8-6)
		int viewerNetSize = DataTypeUtil.getNetSizeInBits(viewerType);
		int storageNetSize = DataTypeUtil.getNetSizeInBits(storageType);
		if (viewerNetSize > storageNetSize) {
			return Optional.of("The viewer cannot be larger than the storage variable.");
		}
		
		// TODO in FB's IN_OUT, also in FC's IN, OUT, IN_OUT block only elementary can be mapped to elementary or complex to complex
		
		return Optional.empty();
	}
}
