/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility methods for handling the source files in their textual format.
 * Typically to be used for quick heuristic analysis.
 */
public final class TextualUtil {
	private TextualUtil() {
		// Utility class.
	}

	/**
	 * Returns the given source file without the part of the code in block or
	 * line comments.The comments will be removed without parsing the program.
	 * The PLCverif-specific comments starting with {@code //#} will not be
	 * removed.
	 *
	 * @param content
	 *            Content of the source file
	 * @return Content of the source file without comments
	 */
	public static String contentWithoutComments(String content) {
		// (?s): DOTALL -- dot matches new line chars too
		// (?m): MULTILINE: $ matches the end of each line, not the end of the
		// string
		return content.replaceAll("(?s)\\(\\*.*?\\*\\)", "").replaceAll("(?m)//[^#].*$", "");
	}

	/**
	 * Returns the list of executable unit names present in the given STEP 7
	 * source file. The block and line comments will be discarded. The names
	 * will be determined without parsing the program.
	 *
	 * @param content
	 *            Content of the source file
	 * @return List of executable unit names
	 */
	public static List<String> collectExecutableUnitNames(String content) {
		CharSequence contentWithoutComments = contentWithoutComments(content);
		List<String> ret = new ArrayList<>();

		Pattern execUnitName = Pattern.compile(
				"(?i)\\b(?:(FUNCTION_BLOCK|FUNCTION|ORGANIZATION_BLOCK))\\s+\"?\\s*(?<name>[^\\s:\"]+)",
				Pattern.DOTALL | Pattern.MULTILINE);
		Matcher matcher = execUnitName.matcher(contentWithoutComments);

		while (matcher.find()) {
			ret.add(matcher.group("name"));
		}

		return ret;
	}

	/**
	 * Returns the list of assertion tags present in the given STEP 7 source
	 * file. The block and line comments will be discarded. The names will be
	 * determined without parsing the program.
	 *
	 * @param content
	 *            Content of the source file
	 * @return List of executable unit names
	 */
	public static List<String> collectAssertionTags(String content) {
		List<String> ret = new ArrayList<>();

		Pattern execUnitName = Pattern.compile("\\Q//#ASSERT\\E\\s[^;\n]*?:\\s*(?<tag>[a-zA-Z0-9_]+)\\s*;?\\s*$",
				Pattern.DOTALL | Pattern.MULTILINE);
		Matcher matcher = execUnitName.matcher(contentWithoutComments(content));

		while (matcher.find()) {
			ret.add(matcher.group("tag"));
		}

		return ret;
	}
}
