/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.util

import cern.plcverif.plc.step7.step7Language.DataType
import cern.plcverif.plc.step7.step7Language.ElementaryDT
import cern.plcverif.plc.step7.step7Language.ArrayDT
import static extension cern.plcverif.plc.step7.util.DataTypeUtil.arrayRangeLower
import static extension cern.plcverif.plc.step7.util.DataTypeUtil.arrayRangeUpper
import cern.plcverif.plc.step7.step7Language.StringDT
import cern.plcverif.plc.step7.step7Language.FbOrUdtDT
import cern.plcverif.plc.step7.step7Language.StructDT
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine
import cern.plcverif.plc.step7.step7Language.UserDefinedDataType
import cern.plcverif.plc.step7.step7Language.ParameterDT

class TypeEquals {
	def static dispatch boolean equalTypes(DataType dt1, DataType dt2) {
		return dt1 === dt2;
	}

	def static dispatch boolean equalTypes(Void dt1, Void dt2) {
		return false;
	}

	def static dispatch boolean equalTypes(DataType dt1, Void dt2) {
		return false;
	}

	def static dispatch boolean equalTypes(Void dt1, DataType dt2) {
		return false;
	}

	def static dispatch boolean equalTypes(ElementaryDT dt1, ElementaryDT dt2) {
		return dt1.type == dt2.type;
	}
	
	def static dispatch boolean equalTypes(ParameterDT dt1, ParameterDT dt2) {
		return dt1.type == dt2.type;
	}

	def static dispatch boolean equalTypes(ArrayDT dt1, ArrayDT dt2) {
		if (!equalTypes(dt1.baseType, dt2.baseType)) {
			// different base types
			return false;
		}

		if (dt1.dimensions.size != dt2.dimensions.size) {
			// different number of dimensions
			return false;
		}

		for (i : 0 .. dt1.dimensions.size - 1) {
			if (dt1.dimensions.get(i).arrayRangeLower != dt2.dimensions.get(i).arrayRangeLower ||
				dt1.dimensions.get(i).arrayRangeUpper != dt2.dimensions.get(i).arrayRangeUpper) {
				// different bounds for dimension 'i'
				return false;
			}
		}

		return true;
	}

	def static dispatch boolean equalTypes(StringDT dt1, StringDT dt2) {
		return true;
	}

	def static dispatch boolean equalTypes(FbOrUdtDT dt1, FbOrUdtDT dt2) {
		if (dt1.type === dt2.type) {
			return true;
		}
		
		if (dt1.type instanceof UserDefinedDataType && dt2.type instanceof UserDefinedDataType) {
			val udt1 = dt1.type as UserDefinedDataType;
			val udt2 = dt2.type as UserDefinedDataType;
			
			return equalTypes(udt1.declaration, udt2.declaration);
		}
		
		return false;
	}

	def static dispatch boolean equalTypes(FbOrUdtDT dt1, StructDT dt2) {
		if (dt1.type instanceof UserDefinedDataType) {
			val udt1 = dt1.type as UserDefinedDataType;
			return equalTypes(udt1.declaration, dt2);
		}
		return false;
	}
	
	def static dispatch boolean equalTypes(StructDT dt1, FbOrUdtDT dt2) {
		return equalTypes(dt2, dt1);
	}

	def static dispatch boolean equalTypes(StructDT dt1, StructDT dt2) {
		if (dt1 === dt2) {
			return true;
		}
		
		if (dt1.members.size != dt2.members.size) {
			return false;
		}

		val result = areVariableDeclarationLinesEqual(dt1.members, dt2.members);
		return result;
	}

	private def static boolean areVariableDeclarationLinesEqual(Iterable<VariableDeclarationLine> members1,
		Iterable<VariableDeclarationLine> members2) {
		if (members1.isEmpty && members2.isEmpty) {
			return true;
		}
		if (members1.isEmpty || members2.isEmpty) {
			return false;
		}

		if (members1.head.variables.size != 1 || members2.head.variables.size != 1) {
			return false;
		}

		if (!members1.head.variables.head.name.equalsIgnoreCase(members2.head.variables.head.name)) {
			// variable names are different
			return false;
		}
		
		if (!equalTypes(members1.head.type, members2.head.type)) {
			// variable types are different
			return false;
		}

		return areVariableDeclarationLinesEqual(members1.tail, members2.tail);
	}

}
