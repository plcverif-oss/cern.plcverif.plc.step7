/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.util;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Predicate;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import com.google.common.base.Preconditions;

import cern.plcverif.plc.step7.step7Language.StlBitLogicNestingCloseStatement;
import cern.plcverif.plc.step7.step7Language.StlBitLogicNestingOpenStatement;
import cern.plcverif.plc.step7.step7Language.StlBitLogicOpMnemonic;
import cern.plcverif.plc.step7.step7Language.StlBitLogicStatement;
import cern.plcverif.plc.step7.step7Language.StlComparisonStatement;
import cern.plcverif.plc.step7.step7Language.StlLoadStatement;
import cern.plcverif.plc.step7.step7Language.StlNetwork;
import cern.plcverif.plc.step7.step7Language.StlStatement;
import cern.plcverif.plc.step7.step7Language.StlStatementList;
import cern.plcverif.plc.step7.step7Language.StlTransferStatement;
import cern.plcverif.plc.step7.step7Language.StlUnaryStatement;
import cern.plcverif.plc.step7.step7Language.TitleAttribute;

public final class StlHelper {
	private StlHelper() {
		// Utility class.
	}

	public enum StatusBit {
		RLO, NFC, BR, STA, OR, CC0, CC1, OS, OV
	}

	public enum NestingStackBit {
		RLO, BR, OR, FC0, FC1, FC2
	}

	public static class NestingStackFunctionCode {
		private boolean fc0;
		private boolean fc1;
		private boolean fc2;

		NestingStackFunctionCode(boolean fc0, boolean fc1, boolean fc2) {
			this.fc0 = fc0;
			this.fc1 = fc1;
			this.fc2 = fc2;
		}

		public boolean getFc0() {
			return fc0;
		}

		public boolean getFc1() {
			return fc1;
		}

		public boolean getFc2() {
			return fc2;
		}
	}

	private static Map<StlBitLogicOpMnemonic, NestingStackFunctionCode> functionCodes = null;

	public static NestingStackFunctionCode functionCodeOfNsOp(StlBitLogicOpMnemonic op) {
		if (functionCodes == null) {
			// Lazy initialization
			functionCodes = new EnumMap<>(StlBitLogicOpMnemonic.class);
			functionCodes.put(StlBitLogicOpMnemonic.AND, new NestingStackFunctionCode(false, false, false));
			functionCodes.put(StlBitLogicOpMnemonic.AND_NOT, new NestingStackFunctionCode(true, false, false));
			functionCodes.put(StlBitLogicOpMnemonic.OR, new NestingStackFunctionCode(false, true, false));
			functionCodes.put(StlBitLogicOpMnemonic.OR_NOT, new NestingStackFunctionCode(true, true, false));
			functionCodes.put(StlBitLogicOpMnemonic.XOR, new NestingStackFunctionCode(false, false, true));
			functionCodes.put(StlBitLogicOpMnemonic.XOR_NOT, new NestingStackFunctionCode(true, false, true));
		}

		return functionCodes.get(op);
	}

	public static boolean containsNestingBoolOps(StlStatementList stlStatementList) {
		return containsMatching(stlStatementList, StlHelper::isNestingBoolStatement);
	}

	private static boolean isNestingBoolStatement(EObject e) {
		if (e instanceof StlStatement) {
			return e instanceof StlBitLogicNestingOpenStatement || e instanceof StlBitLogicNestingCloseStatement;
		}
		return false;
	}

	public static boolean containsAccuOperations(StlStatementList stlStatementList) {
		return containsMatching(stlStatementList, StlHelper::isAccuOperation);
	}

	private static boolean isAccuOperation(EObject e) {
		if (e instanceof StlStatement) {
			return e instanceof StlComparisonStatement || e instanceof StlLoadStatement
					|| e instanceof StlTransferStatement;
		}
		return false;
	}

	private static boolean containsMatching(StlStatementList stlStatementList, Predicate<EObject> condition) {
		// PERF
		TreeIterator<EObject> iterator = stlStatementList.eAllContents();
		while (iterator.hasNext()) {
			EObject e = iterator.next();
			if (condition.test(e)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the title of the given network, or empty string if not found.
	 * Handles the special case when the title is parsed as TitleAttributeText,
	 * then it removes the unnecessary 'TITLE=' start.
	 *
	 * @param network
	 *            The network. Must not be null.
	 * @return The title of the network. Never null.
	 */
	public static String getNetworkTitle(StlNetwork network) {
		Preconditions.checkNotNull(network, "network");

		TitleAttribute titleAttrib = network.getTitleAttribute();

		if (titleAttrib == null || titleAttrib.getTitle() == null) {
			return "";
		} else {
			if (titleAttrib.getTitle().startsWith("TITLE=")) {
				return titleAttrib.getTitle().substring("TITLE=".length());
			} else {
				return titleAttrib.getTitle();
			}
		}
	}

	/**
	 * Returns true if the given statement modifies the value of its argument.
	 * @param statement Statement to be checked
	 * @return True if the statement modifies the value of its argument
	 */
	public static boolean isWritingStatement(StlStatement statement) {
	  if (statement instanceof StlUnaryStatement) {
		  return !(statement instanceof StlLoadStatement || statement instanceof StlBitLogicStatement);
	  } else {
		  return false;
	  }
	}
}
