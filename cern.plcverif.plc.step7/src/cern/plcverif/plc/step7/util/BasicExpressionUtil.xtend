/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.util

import cern.plcverif.plc.step7.step7Language.AbstractExpression
import cern.plcverif.plc.step7.step7Language.AdditiveExpression
import cern.plcverif.plc.step7.step7Language.AndExpression
import cern.plcverif.plc.step7.step7Language.ArrayRef
import cern.plcverif.plc.step7.step7Language.ComparisonExpression
import cern.plcverif.plc.step7.step7Language.CounterConstant
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.EqualityExpression
import cern.plcverif.plc.step7.step7Language.FlagRef
import cern.plcverif.plc.step7.step7Language.MultiplicativeExpression
import cern.plcverif.plc.step7.step7Language.NamedConstantRef
import cern.plcverif.plc.step7.step7Language.NegatedUnnamedConstantRef
import cern.plcverif.plc.step7.step7Language.OrExpression
import cern.plcverif.plc.step7.step7Language.PowerExpression
import cern.plcverif.plc.step7.step7Language.QualifiedRef
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall
import cern.plcverif.plc.step7.step7Language.TimerConstant
import cern.plcverif.plc.step7.step7Language.UnaryExpression
import cern.plcverif.plc.step7.step7Language.UnnamedConstant
import cern.plcverif.plc.step7.step7Language.UnnamedConstantRef
import cern.plcverif.plc.step7.step7Language.XorExpression

import static extension cern.plcverif.plc.step7.util.Step7LanguageHelper.*
import static extension cern.plcverif.plc.step7.util.DataTypeUtil.*
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import cern.plcverif.plc.step7.step7Language.UnaryOperator
import cern.plcverif.plc.step7.step7Language.MemoryAddress
import cern.plcverif.plc.step7.step7Language.DirectBitAccessRef

/**
 * Basic expression may contain:
 * - Basic arithmetic operations (+, -, *, /, MOD, DIV; but not ** or logic operators)
 * - Constants
 * - Extended variables: simple variable, IO address, DB field, local variable instance; function call
 */
class BasicExpressionUtil {
	static final Logger log = LogManager.getLogger(BasicExpressionUtil);
	
	static dispatch def boolean isBasicExpression(AbstractExpression e) {
		switch (e) {
			MemoryAddress, QualifiedRef, ArrayRef, DirectNamedRef, DirectBitAccessRef: return true
			UnnamedConstant, NamedConstantRef, UnnamedConstantRef, NegatedUnnamedConstantRef: return true
			UnaryExpression: return isBasicExpression(e.expr) && e.op == UnaryOperator.MINUS
			MultiplicativeExpression: return isBasicExpression(e.left) && isBasicExpression(e.right)
			AdditiveExpression: return isBasicExpression(e.left) && isBasicExpression(e.right)
			SclSubroutineCall: return e.isFcCall && !e.returnType.isVoid
			ComparisonExpression: return false
			EqualityExpression: return false
			OrExpression: return false
			AndExpression: return false
			XorExpression: return false
			TimerConstant: return false
			CounterConstant: return false
			PowerExpression: return false
			FlagRef: return false
			default: { 
				log.info("Unknown expression type in isBasicExpression: {}. Assumption: it is not a basic expression.", e.class.name);
				return false
			}
		}
	}
	
	static dispatch def boolean isBasicExpression(Void e) {
		log.info("Null in isBasicExpression.");
		return false;
	}
}