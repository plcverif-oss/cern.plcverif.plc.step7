/******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 * 	 Jean-Charles Tournier - Nov 2019 - Add support for DirectBitAccessRef
 *****************************************************************************/
package cern.plcverif.plc.step7.util

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.plc.step7.datatypes.AbstractS7DataType
import cern.plcverif.plc.step7.datatypes.S7AddressMemoryIdentifier
import cern.plcverif.plc.step7.datatypes.S7Integer
import cern.plcverif.plc.step7.step7Language.AndExpression
import cern.plcverif.plc.step7.step7Language.ArrayDT
import cern.plcverif.plc.step7.step7Language.ArrayRef
import cern.plcverif.plc.step7.step7Language.BoolConstant
import cern.plcverif.plc.step7.step7Language.CallParameter
import cern.plcverif.plc.step7.step7Language.CharConstant
import cern.plcverif.plc.step7.step7Language.CompatbilityLevelEnum
import cern.plcverif.plc.step7.step7Language.ConstantInitializationElement
import cern.plcverif.plc.step7.step7Language.DataBlock
import cern.plcverif.plc.step7.step7Language.DataBlockStructure
import cern.plcverif.plc.step7.step7Language.DataType
import cern.plcverif.plc.step7.step7Language.DateAndTimeConstant
import cern.plcverif.plc.step7.step7Language.DateConstant
import cern.plcverif.plc.step7.step7Language.DeclarationSection
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit
import cern.plcverif.plc.step7.step7Language.Expression
import cern.plcverif.plc.step7.step7Language.FbOrUdtDT
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.FunctionBlock
import cern.plcverif.plc.step7.step7Language.GlobalVariableBlock
import cern.plcverif.plc.step7.step7Language.IntConstant
import cern.plcverif.plc.step7.step7Language.Label
import cern.plcverif.plc.step7.step7Language.LabeledSclStatement
import cern.plcverif.plc.step7.step7Language.LeftValue
import cern.plcverif.plc.step7.step7Language.NamedConstantDeclaration
import cern.plcverif.plc.step7.step7Language.NamedConstantRef
import cern.plcverif.plc.step7.step7Language.NamedElement
import cern.plcverif.plc.step7.step7Language.NamedOrUnnamedConstantRef
import cern.plcverif.plc.step7.step7Language.NamedValueRef
import cern.plcverif.plc.step7.step7Language.NegatedUnnamedConstantRef
import cern.plcverif.plc.step7.step7Language.OrExpression
import cern.plcverif.plc.step7.step7Language.OrganizationBlock
import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.step7Language.ProgramUnit
import cern.plcverif.plc.step7.step7Language.QualifiedRef
import cern.plcverif.plc.step7.step7Language.RangeCaseElementString
import cern.plcverif.plc.step7.step7Language.RealConstant
import cern.plcverif.plc.step7.step7Language.SclAssignmentStatement
import cern.plcverif.plc.step7.step7Language.SclCaseStatement
import cern.plcverif.plc.step7.step7Language.SclForStatement
import cern.plcverif.plc.step7.step7Language.SclIfStatement
import cern.plcverif.plc.step7.step7Language.SclRepeatStatement
import cern.plcverif.plc.step7.step7Language.SclStatement
import cern.plcverif.plc.step7.step7Language.SclStatementList
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall
import cern.plcverif.plc.step7.step7Language.SclWhileStatement
import cern.plcverif.plc.step7.step7Language.StatementList
import cern.plcverif.plc.step7.step7Language.Step7LanguageFactory
import cern.plcverif.plc.step7.step7Language.StlParameterlessCallStatement
import cern.plcverif.plc.step7.step7Language.StlStatementList
import cern.plcverif.plc.step7.step7Language.StringConstant
import cern.plcverif.plc.step7.step7Language.StringDT
import cern.plcverif.plc.step7.step7Language.StructDT
import cern.plcverif.plc.step7.step7Language.SubroutineCall
import cern.plcverif.plc.step7.step7Language.TimeConstant
import cern.plcverif.plc.step7.step7Language.TimeOfDayConstant
import cern.plcverif.plc.step7.step7Language.UnaryExpression
import cern.plcverif.plc.step7.step7Language.UnaryOperator
import cern.plcverif.plc.step7.step7Language.UnnamedConstant
import cern.plcverif.plc.step7.step7Language.UnnamedConstantRef
import cern.plcverif.plc.step7.step7Language.UserDefinedDataType
import cern.plcverif.plc.step7.step7Language.Variable
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock
import cern.plcverif.plc.step7.step7Language.VariableDeclarationDirection
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine
import cern.plcverif.plc.step7.step7Language.VariableInitialization
import cern.plcverif.plc.step7.step7Language.XorExpression
import cern.plcverif.plc.step7.typecomputer.Step7TypeComputer
import com.google.common.base.Preconditions
import java.util.Collections
import java.util.List
import java.util.Map
import java.util.Optional
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.nodemodel.util.NodeModelUtils

import static com.google.common.base.Preconditions.*
import cern.plcverif.plc.step7.step7Language.DirectRef
import cern.plcverif.plc.step7.transformation.Step7AstTransformationException
import cern.plcverif.plc.step7.step7Language.DirectBitAccessRef
import cern.plcverif.plc.step7.step7Language.ElementaryDT

class Step7LanguageHelper {
	static final Logger log = LogManager.getLogger(Step7LanguageHelper);
	
	/**
	 * Boolean load option for resource sets supporting the in-memory source file parsing.
	 * To be used as {@code resourceSet.addLoadOption(Step7LanguageHelper.OPTION_IN_MEMORY_SUPPORT, Boolean.TRUE);}
	 */
	public static final String OPTION_IN_MEMORY_SUPPORT = "IN_MEMORY_SUPPORT";

	def static getDataType(Variable v) {
		checkNotNull(v, "Unable to get the data type for variable 'null'.");
		checkNotNull(v.eContainer, "Unable to get the data type for variable that is not contained.");
		checkArgument(v.eContainer instanceof VariableDeclarationLine);
		return (v.eContainer as VariableDeclarationLine).type;
	}

	def static List<Variable> getAllTopVariables(VariableDeclarationBlock block) {
		return block.variables.map[it|it.variables].flatten.toList;
	}
	
	def static List<Variable> getAllTopVariables(GlobalVariableBlock block) {
		return block.variables.map[it|it.variables].flatten.toList;
	}

	def static List<Variable> getAllTopVariables(StructDT block) {
		return block.members.map[it|it.variables].flatten.toList;
	}

	def static getDeclarationSection(ProgramUnit programUnit) {
		switch (programUnit) {
			ExecutableProgramUnit: return programUnit.declarationSection
			default: throw new UnsupportedOperationException(
				"The given program unit does not have a declaration section.")
		}
	}

	/**
	 * Returns the statement list defining the given program unit.
	 * The result is {@link Optional#empty} if the given program unit cannot contain any
	 * statement list (e.g. DataBlock), or if the statement list is empty.
	 */
	def static Optional<StatementList> getStatementList(ProgramUnit programUnit) {
		switch (programUnit) {
			OrganizationBlock: return Optional.ofNullable(programUnit.statements)
			FunctionBlock: return Optional.ofNullable(programUnit.statements)
			Function: return Optional.ofNullable(programUnit.statements)
			default: return Optional.empty()
		}
	}

	def static ProgramFile getContainerProgramFile(ProgramUnit unit) {
		Preconditions.checkState(unit.eContainer() instanceof ProgramFile,
			"A program unit can only be contained in a program file.");
		return unit.eContainer() as ProgramFile;
	}

	def dispatch static Iterable<? extends Variable> allDefinedVariables(NamedElement e) {
		return Collections.EMPTY_LIST;
	}

	def dispatch static Iterable<? extends Variable> allDefinedVariables(ProgramUnit e) {
		// println('''allDefinedVariables not defined for the unit '«e.class.name»' (name: «e.name»).''');
		return Collections.EMPTY_LIST;
	}

	def dispatch static Iterable<? extends Variable> allDefinedVariables(ExecutableProgramUnit e) {
		return e.declarationSection.allDefinedVariablesInDeclarationSection;
	}

	def dispatch static Iterable<? extends Variable> allDefinedVariables(UserDefinedDataType e) {
		return e.declaration.containedVariables;
	}

	def dispatch static allDefinedVariables(DataBlock e) {
		val structDef = e.structure;
		switch (structDef) {
			FbOrUdtDT: return structDef.containedVariables
			StructDT: return structDef.containedVariables
			default: throw new IllegalStateException("Unknown structure definition type: " + structDef.class.name)
		}
	}

	def dispatch static Iterable<? extends Variable> allDefinedVariables(Variable e) {
		return e.dataType.containedVariables;
	}

	def dispatch static Iterable<? extends Variable> containedVariables(DataType e) {
		println("Unknown data type: " + e.class.name)
		return Collections.EMPTY_LIST
	}

	def dispatch static Iterable<? extends Variable> containedVariables(FbOrUdtDT e) {
		return e.type.allDefinedVariables
	}

	def dispatch static Iterable<? extends Variable> containedVariables(StructDT e) {
		return e.members.flattenVariableDeclaration
	}
	
	def dispatch static Iterable<? extends Variable> containedVariables(ArrayDT e) {
		return containedVariables(e.baseType)
	}

	def static Iterable<? extends Variable> allDefinedVariablesInDeclarationSection(DeclarationSection e) {
		if (e === null || e.variableDeclarations === null) {
			return Collections.EMPTY_LIST;
		}
		return e.getVariableDeclarations.map[x|x.variables].flatten.map[x|x.variables].flatten;
	}

	def static Iterable<? extends Variable> allDefinedVariablesInDeclarationSection(DeclarationSection e,
		VariableDeclarationDirection direction) {
		if (e === null || e.variableDeclarations === null) {
			return Collections.EMPTY_LIST;
		}
		return e.getVariableDeclarations.filter[it.direction == direction].map[x|x.variables].flatten.
			map[x|x.variables].flatten;
	}

	def static Iterable<? extends NamedConstantDeclaration> allDefinedConstantsInDeclarationSection(
		DeclarationSection e) {
		if (e === null || e.constantDeclarations === null) {
			return Collections.EMPTY_LIST;
		}
		return e.constantDeclarations.map[x|x.constants].flatten;
	}

	def static Iterable<? extends NamedConstantDeclaration> allDefinedConstants(ExecutableProgramUnit e) {
		return allDefinedConstantsInDeclarationSection(e.declarationSection);
	}

	def static Iterable<? extends Label> allDefinedLabelsInDeclarationSection(DeclarationSection e) {
		if (e === null || e.labelDeclarations === null) {
			return Collections.EMPTY_LIST;
		}
		return e.labelDeclarations.map[x|x.labels].flatten;
	}

	def static Iterable<? extends Label> allDefinedLabels(ExecutableProgramUnit e) {
		return allDefinedLabelsInDeclarationSection(e.declarationSection);
	}

	private def static Iterable<? extends Variable> flattenVariableDeclaration(
		EList<VariableDeclarationLine> variableDeclarationLines) {
		return variableDeclarationLines.map[x|x.variables].flatten;
	}

	// Array ranges
	def static dispatch AbstractS7DataType unnamedConstantValue(UnnamedConstant c) {
		throw new RuntimeException("XXX")
	}

	def static dispatch AbstractS7DataType unnamedConstantValue(BoolConstant c) {
		return c.value;
	}

	def static dispatch AbstractS7DataType unnamedConstantValue(IntConstant c) {
		return c.value;
	}

	def static dispatch AbstractS7DataType unnamedConstantValue(RealConstant c) {
		return c.value;
	}

	def static dispatch AbstractS7DataType unnamedConstantValue(CharConstant c) {
		return c.value;
	}

	def static dispatch AbstractS7DataType unnamedConstantValue(DateConstant c) {
		return c.value;
	}

	def static dispatch AbstractS7DataType unnamedConstantValue(DateAndTimeConstant c) {
		return c.value;
	}

	def static dispatch AbstractS7DataType unnamedConstantValue(TimeOfDayConstant c) {
		return c.value;
	}

	def static dispatch AbstractS7DataType unnamedConstantValue(TimeConstant c) {
		return c.value;
	}

	def static dispatch AbstractS7DataType unnamedConstantValue(StringConstant c) {
		return c.value;
	}

	def static boolean hasParameter(SubroutineCall call) {
		return call.callParameter !== null;
	}

	/**
	 * 
	 * 
	 * 
	 * Throws exception if not found.
	 */
	def static getOnlyInputVariable(Function function) {
		// return e.getVariableDeclarations.map[x|x.variables].flatten.map[x|x.variables].flatten
		val inputVars = function.declarationSection.variableDeclarations.filter [
			it.direction == VariableDeclarationDirection.INPUT
		].map[x|x.variables].flatten.map[x|x.variables].flatten;
		if (inputVars.size > 1) {
			throw new IllegalStateException(
				"Invalid: there are more than 1 input variables defined in " + function.name + ": " + inputVars.map [
					it.name
				].join(", "))
		} else if (inputVars.size == 0 && !function.isBuiltIn) {
			throw new IllegalStateException("Invalid: no input variable is defined in " + function.name + ".")
		}
		return inputVars.head;
	}
	
	/**
	 * Returns all parameters (input, output and input-output variables)
	 * of the given executable program unit. A variable view can never be a 
	 * parameter.
	 */
	def static Iterable<Variable> getBlockParameters(ExecutableProgramUnit e) {
		val ret = e.declarationSection.variableDeclarations.filter [
			it.direction == VariableDeclarationDirection.INPUT || 
			it.direction == VariableDeclarationDirection.INOUT || 
			it.direction == VariableDeclarationDirection.OUTPUT
		].map[x|x.variables].flatten.map[x|x.variables].flatten
		.filter[it | !it.isReference];
		return ret;
	}

	def static Optional<Variable> tryGetOnlyInputVariable(Function function) {
		val inputVars = function.declarationSection.
			allDefinedVariablesInDeclarationSection(VariableDeclarationDirection.INPUT);
		if (inputVars.size > 1 || (inputVars.size == 0 && !function.isBuiltIn)) {
			return Optional.empty;
		}
		return Optional.of(inputVars.head);
	}

	def static dispatch Expression unwrapRefExpression(NamedOrUnnamedConstantRef ref, Step7TypeComputer typeComputer) {
	}

	def static dispatch Expression unwrapRefExpression(NamedConstantRef ref, Step7TypeComputer typeComputer) {
		return ref.ref.value
	}

	def static dispatch Expression unwrapRefExpression(UnnamedConstantRef ref, Step7TypeComputer typeComputer) {
		return ref.constant
	}

	def static dispatch Expression unwrapRefExpression(NegatedUnnamedConstantRef ref, Step7TypeComputer typeComputer) {
		val UnnamedConstant baseConstant = ref.negatedConstant;

		switch (baseConstant) {
			IntConstant: {
				val newValue = baseConstant.value.copyAndNegate();
				val ret = Step7FactoryHelper.createIntConstant(newValue);
				typeComputer.copyCreated(ref, ret); // register the type of the newly created constant
				return ret;
			}
			RealConstant: {
				val newValue = baseConstant.value.copyAndNegate();
				val ret = Step7FactoryHelper.createRealConstant(newValue);
				typeComputer.copyCreated(ref, ret);  // register the type of the newly created constant
				return ret;
			}
		}

		throw new UnsupportedOperationException("Unable to unwrap NegatedUnnamedConstantRef, unexpected data type: " +
			baseConstant.class.name);
	}

	def static boolean isBlockIdentifier(String name) {
		return name.toUpperCase().matches("(FB|FC|OB|DB|SFB|SFC|SDB|UDT|VAT)\\d+");
	}

	def static boolean isSymbol(String name) {
		if (name === null) {
			return false;
		}
		return (name.startsWith("\"") && name.endsWith("\""));
	}

	def static int dimensionInt(StringDT stringDt) {
		if (stringDt.dimension === null) {
			return 254;
		} else {
			return SimpleExpressionUtil.evaluateSimpleExpression(stringDt.dimension) as int;
		}
	}

	def static boolean isRetValAssignment(SclAssignmentStatement assignment) {
		return (assignment.leftValue instanceof Function);
	}

	def static boolean isFbCall(SubroutineCall call) {
		if (call.calledUnit instanceof FunctionBlock) {
			return true;
		}
		
		if(call.calledUnit instanceof DataBlock){
			if (isInstanceDb(call.calledUnit as DataBlock)){
				return true;
			}
		}

		if (call.calledUnit instanceof Variable) {
			val variable = call.calledUnit as Variable;
			if (DataTypeUtil.isFbDataType(variable.dataType)) {
				return true;
			}
		}

		return false;
	}

	def static boolean isFcCall(SubroutineCall call) {
		if (call.calledUnit instanceof Function) {
			return true;
		}

		return false;
	}

	/**
	 * Returns the executable program unit called by the given call.
	 * If the callee is an executable program unit, that program unit will be
	 * returned. If the callee is a variable with FB type (i.e., a local
	 * instance of a function block), the type FB will be returned. If none
	 * of these cases is true (notably if the called unit is null in the given
	 * call), an exception is thrown.
	 * @param call Subroutine call
	 * @return The called executable program unit
	 * @throws IllegalStateException if it is not possible to determine the 
	 * called program unit
	 */
	def static ExecutableProgramUnit getCalledProgramUnit(SubroutineCall call) {
		if (call.calledUnit instanceof ExecutableProgramUnit) {
			return call.calledUnit as ExecutableProgramUnit;
		} else if (call.calledUnit instanceof Variable) {
			val variable = call.calledUnit as Variable;
			if (DataTypeUtil.isFbDataType(variable.dataType)) {
				val ret = (variable.dataType as FbOrUdtDT).representedFb;
				if (ret.present) {
					return ret.get;
				}
			}
		} else if (call.calledUnit instanceof DataBlock) {
			val ret = (call.calledUnit as DataBlock).structure.representedFb;
			if (ret.present) {
				return ret.get;
			}
		}

		throw new IllegalStateException("Unknown case in getCalledProgramUnit.");
	}
	
	/**
	 * Returns the executable program unit called by the given call.
	 * If the callee is an executable program unit, that program unit will be
	 * returned. If the callee is a variable with FB type (i.e., a local
	 * instance of a function block), the type FB will be returned. If none
	 * of these cases is true (notably if the called unit is null in the given
	 * call), {@link Optional#empty()} is returned.
	 * @param call Subroutine call
	 * @return The called executable program unit or {@link Optional#empty()}
	 */
	def static Optional<ExecutableProgramUnit> tryGetCalledProgramUnit(SubroutineCall call) {
		try {
			return Optional.of(getCalledProgramUnit(call));
		} catch (Throwable t) {
			return Optional.empty();
		}
	}

	def static boolean isFunctionCallExpression(Expression expr) {
		return (expr instanceof SclSubroutineCall);
	}

	def static boolean isTypeConversionFunc(Expression expr) {
		if (!isFunctionCallExpression(expr)) {
			return false;
		}

		val call = expr as SclSubroutineCall;
		return isTypeConversionFunc(call.calledUnit);
	}

	def static boolean isTypeConversionFunc(NamedElement unit) {
		if (unit instanceof Function) {
			val Function calledFc = unit;
			if (calledFc.isBuiltIn && calledFc.name.toLowerCase.contains("_to_") &&
				calledFc.tryGetOnlyInputVariable.isPresent) {
				return true;
			}
		}

		return false;
	}

	def static VariableDeclarationDirection getVariableDirection(Variable v) {
		val varDeclBlock = EcoreUtil2.getContainerOfType(v, VariableDeclarationBlock);
		if (varDeclBlock === null) {
			if (EmfHelper.isContainedInAny(v, GlobalVariableBlock)) {
				// all global variables are assumed to be static
				return VariableDeclarationDirection.STATIC;
			} else {
				// it may be in a UDT too
				val sharedDataBlock = EcoreUtil2.getContainerOfType(v, DataBlock);
				val udt = EcoreUtil2.getContainerOfType(v, UserDefinedDataType);
				if (sharedDataBlock !== null || udt !== null) {
					// all data block variables are assumed to be static
					return VariableDeclarationDirection.STATIC;
				} else {
					throw new UnsupportedOperationException('''Unknown case at getVariableDirection for variable «v.name».''');
				}
			}
		}

		Preconditions.checkState(varDeclBlock.direction !== null);
		return varDeclBlock.direction;
	}

	def static Optional<VariableInitialization> getVariableInitialization(Variable v) {
		Preconditions.checkNotNull(v);
		Preconditions.checkArgument(v.eContainer instanceof VariableDeclarationLine);

		val varLine = v.eContainer as VariableDeclarationLine;

		return Optional.ofNullable(varLine.initialization);
	}

	def static Optional<Expression> getSingleVariableInitialValue(Variable v, Step7TypeComputer typeComputer) {
		Preconditions.checkNotNull(v);
		Preconditions.checkArgument(DataTypeUtil.isElementaryType(v.dataType));
		Preconditions.checkArgument(v.eContainer instanceof VariableDeclarationLine);

		val varLine = v.eContainer as VariableDeclarationLine;

		if (varLine !== null && varLine.initialization instanceof ConstantInitializationElement) {
			val astVarInitialization = varLine.initialization as ConstantInitializationElement;
			val initConstValue = Step7LanguageHelper.unwrapRefExpression(astVarInitialization.constant, typeComputer);
			return Optional.of(initConstValue);
		}
		return Optional.empty;
	}

	def static boolean isVariable(NamedElement e) {
		return e instanceof Variable;
	}
	
	/**
	 * Returns true iff the given variable is declared as part of a struct.
	 */
	def static boolean isStructMemberVariable(Variable e) {
		return EmfHelper.getContainerOfType(e, StructDT) !== null;
	}

	def static boolean isLogicExpr(Expression expr) {
		return (expr instanceof AndExpression || expr instanceof OrExpression || expr instanceof XorExpression ||
			(expr instanceof UnaryExpression && (expr as UnaryExpression).op == UnaryOperator.NOT));
	}

	def static boolean isExecutableProgramUnit(NamedElement e) {
		return e instanceof ExecutableProgramUnit;
	} 

	def static boolean isFunction(NamedElement e) {
		return e instanceof Function;
	}

	def static boolean isFunctionBlock(NamedElement e) {
		return e instanceof FunctionBlock;
	}

	def static boolean isDataBlock(NamedElement e) {
		return e instanceof DataBlock;
	}
	
	def static boolean isOrganizationBlock(NamedElement e) {
		return e instanceof OrganizationBlock;
	}

	/**
	 * Returns true iff the given DATA_BLOCK is an instance DB of a FB.
	 */
	def static boolean isInstanceDb(DataBlock db) {
		return db.structure.representedFb.present;
	}

	/**
	 * Returns true iff the given DATA_BLOCK is a shared DB (its structure is given as a UDT or locally defined structure).
	 */
	def static boolean isSharedDb(DataBlock db) {
		return DataTypeUtil.isStructDataType(db.structure);
	}

	def static dispatch Optional<FunctionBlock> getRepresentedFb(DataBlockStructure structure) {
		return Optional.empty;
	}

	def static dispatch Optional<FunctionBlock> getRepresentedFb(DataType structure) {
		return Optional.empty;
	}

	def static dispatch Optional<FunctionBlock> getRepresentedFb(FbOrUdtDT structure) {
		if (structure.type instanceof FunctionBlock) {
			return Optional.of(structure.type as FunctionBlock);
		}
		return Optional.empty;
	}

	def static dispatch Optional<StructDT> getStructDefinition(DataBlock dataBlock) {
		return dataBlock.structure.structDefinition;
	}

	def static dispatch Optional<StructDT> getStructDefinition(DataBlockStructure structure) {
		return Optional.empty;
	}

	def static dispatch Optional<StructDT> getStructDefinition(DataType structure) {
		return Optional.empty;
	}

	def static dispatch Optional<StructDT> getStructDefinition(FbOrUdtDT structure) {
		if (structure.type instanceof UserDefinedDataType) {
			return Optional.of((structure.type as UserDefinedDataType).declaration);
		}
		return Optional.empty;
	}

	def static dispatch Optional<StructDT> getStructDefinition(StructDT structure) {
		return Optional.of(structure);
	}

	def static boolean isTia(CompatbilityLevelEnum compatibilityLevel) {
		switch (compatibilityLevel) {
			case STEP7_V55: return false
			case TIA1200: return true
			case TIA1500: return true
			case TIA300: return true
		}
	}

	def static Optional<SclStatement> getContainingLoop(EObject e) {
		var current = e;
		while (current !== null) {
			if (current.isLoopStatement()) {
				return Optional.of(current as SclStatement);
			}
			current = current.eContainer;
		}

		return Optional.empty;
	}

	def static boolean isLoopStatement(EObject e) {
		return (e instanceof SclForStatement || e instanceof SclWhileStatement || e instanceof SclRepeatStatement);
	}

	def static Optional<LabeledSclStatement> findFirstTargetStatement(Label label) {
		val containingProgramUnit = EcoreUtil2.getContainerOfType(label, ProgramUnit);
		Preconditions.checkNotNull(containingProgramUnit);

		// look for all labeled statements
		// PERF improve performance if needed
		val labeledStatements = containingProgramUnit.eAllContents.filter(LabeledSclStatement).filter [
			it.labels.contains(label)
		].toList;
		// println(labeledStatements.map[m | m.labels.map[it.name].join(":")].toList)
		if (labeledStatements.empty) {
			return Optional.empty();
		}

		if (labeledStatements.size > 1) {
			log.debug("Multiple statements with the same label in the same block in findFirstTargetStatement.");
		}

		return Optional.ofNullable(labeledStatements.findFirst[true]);
	}

	def static List<LabeledSclStatement> findAllTargetStatements(Label label) {
		val containingProgramUnit = EcoreUtil2.getContainerOfType(label, ProgramUnit);
		Preconditions.checkNotNull(containingProgramUnit);

		// look for all labelled statements
		// PERF improve performance if needed
		val labeledStatements = containingProgramUnit.eAllContents.filter(LabeledSclStatement).filter [
			it.labels.contains(label)
		].toList;
		return Collections.unmodifiableList(labeledStatements);
	}

	def static Map<Label, List<LabeledSclStatement>> findAllTargetSclStatements(SclStatementList parentStatementList) {
		Preconditions.checkNotNull(parentStatementList);
		val Map<Label, List<LabeledSclStatement>> ret = newHashMap()

		val labeledStatements = parentStatementList.eAllContents.filter(LabeledSclStatement);
		for (stmt : labeledStatements.toIterable) {
			for (label : stmt.labels) {
				if (ret.containsKey(label)) {
					ret.get(label).add(stmt);
				} else {
					ret.put(label, newArrayList(stmt));
				}
			}
		}

		return ret;
	}

	/**
	 * Returns the (SCL or STL) call containing the given parameter.
	 * Returns null if no such object is found.
	 */
	def static SubroutineCall getCallOfParam(CallParameter parameter) {
		return EcoreUtil2.getContainerOfType(parameter, SubroutineCall);
	}

	/**
	 * Returns the defined loop increment value for the given statement, or 1 if not otherwise specified.
	 */
	def static Expression getForLoopIncrementValue(SclForStatement e) {
		if (e.by && e.increment !== null) {
			return e.increment;
		} else {
			val one = Step7LanguageFactory.eINSTANCE.createIntConstant();
			one.value = S7Integer.create("INT#1");
			return one;
		}
	}

	/**
	 * Determines whether the given statement is a real statement, i.e. assignment, call, IF, CASE, FOR, WHILE or REPEAT.
	 * For example, a {@code LabeledSclStatement} does not count as real statement, only its nested SclStatement.
	 */
	def static boolean isRealStatement(SclStatement e) {
		if (e instanceof SclAssignmentStatement) {
			// assignment statement
			return true;
		}

		if (e instanceof SclIfStatement || e instanceof SclCaseStatement) {
			// conditional statements
			return true;
		}

		if (e instanceof SclForStatement || e instanceof SclWhileStatement || e instanceof SclRepeatStatement) {
			// loop statements
			return true;
		}

		if (e instanceof SclSubroutineCall) {
			return true;
		}

		return false;
	}

	/**
	 * Returns the variable which is represented by the given {@code LeftValue}, if applicable.
	 * (I.e., it returns the variable referred by the deepest DirectNamedRef.)
	 * The return value is an empty {@code Optional} value in case the left value represents a 
	 * direct DB address ({@code DbFieldAddressRef}) or a flag ({@code FlagRef}).
	 */
	def static dispatch Optional<Variable> getReferredVariable(LeftValue e) {
		return Optional.empty;
	}

	def static dispatch Optional<Variable> getReferredVariable(Void e) {
		// Partial or invalid AST -- but we don't want an IllegalArgumentException
		return Optional.empty;
	}

	def static dispatch Optional<Variable> getReferredVariable(QualifiedRef e) {
		return e.ref.referredVariable;
	}

	def static dispatch Optional<Variable> getReferredVariable(ArrayRef e) {
		return e.ref.referredVariable;
	}
	
	def static dispatch Optional<Variable> getReferredVariable(DirectBitAccessRef e) {
		return e.ref.referredVariable;
	}

	def static dispatch Optional<Variable> getReferredVariable(DirectNamedRef e) {
		if (e.ref instanceof Variable) {
			return Optional.of(e.ref as Variable);
		}
		return Optional.empty();
	}

	def static boolean isBuiltInBlock(ProgramUnit e) {
		switch (e) {
			OrganizationBlock: return e.isBuiltIn
			FunctionBlock: return e.isBuiltIn
			Function: return e.isBuiltIn
			default: return false
		}
	}

	def static VariableDeclarationDirection memoryToVarDir(S7AddressMemoryIdentifier identifier) {
		switch (identifier) {
			case I: return VariableDeclarationDirection.INPUT
			case M: return VariableDeclarationDirection.STATIC
			case L: return VariableDeclarationDirection.STATIC
			case PI: return VariableDeclarationDirection.INPUT
			case PQ: return VariableDeclarationDirection.OUTPUT
			case Q: return VariableDeclarationDirection.OUTPUT
		}
		throw new IllegalArgumentException("Unknown S7AddressMemoryIdentifier: " + identifier);
	}

	def static isDirectNamedConstRef(NamedValueRef expr) {
		return expr instanceof DirectNamedRef && (expr as DirectNamedRef).ref instanceof NamedConstantDeclaration;
	}
	
	def static asSubroutineCall(StlParameterlessCallStatement call) {
		val ret = Step7LanguageFactory.eINSTANCE.createSubroutineCall();
		ret.calledUnit = call.calledUnit;
		ret.hasExplicitDataBlock = false;
		return ret;
	}
	
	/**
	 * Returns true iff the given executable program unit is programmed in STL
	 * language.
	 * @param e Program unit to be checked.
	 * @returns True if the unit is programmed in STL.
	 */
	def static boolean isStlBlock(ExecutableProgramUnit e) {
		return e.statements instanceof StlStatementList;
	}
	
	/**
	 * Returns true iff the given executable program unit is programmed in SCL
	 * language.
	 * @param e Program unit to be checked.
	 * @returns True if the unit is programmed in STL.
	 */
	def static boolean isSclBlock(ExecutableProgramUnit e) {
		return e.statements instanceof SclStatementList;
	}

	/**
	 * Returns a diagnostic string representing the original token text of the given
	 * EObject created by the parser.
	 * For diagnostic purposes.
	 * @param e The parsed object.
	 * @return Origin token text for diagnostic purposes. Never {@code null}.
	 */
	def static String textInSource(EObject e) {
		val node = NodeModelUtils.getNode(e);
		if (node === null) {
			return "unknown";
		} else {
			val tokenText = NodeModelUtils.getTokenText(node);
			return if (tokenText === null) "unknown" else tokenText;
		}
	}
	
	/**
	 * Returns the line number of the given EObject, or 0 if not available.
	 * For diagnostic purposes.
	 * @param e The parsed object.
	 * @return Origin line number for diagnostic purposes. Never {@code null}.
	 */
	def static int lineInSource(EObject e) {
		val node = NodeModelUtils.getNode(e);
		if (node === null) {
			return 0;
		} else {
			return NodeModelUtils.getLineAndColumn(node, node.offset).line;
		}
	}

	/**
	 * Returns a diagnostic string containing the line and column of the given EObject created by the parser.
	 * For diagnostic purposes.
	 * 
	 * @param e The parsed object.
	 * @return Position description for diagnostic purposes. Never {@code null}.
	 */
	def static String posInSource(EObject e) {
		val node = NodeModelUtils.getNode(e);
		if (node === null) {
			return "unknown";
		} else {
			val lineAndCol = NodeModelUtils.getLineAndColumn(node, 0);
			if (lineAndCol === null) {
				return "unknown"
			} else {
				return '''line: «lineAndCol.line», column: «lineAndCol.column»''';
			}
		}
	}
	
	/**
	 * Determines and returns the lower bound (inclusive) of a RangeCaseElementString.
	 */
	def static long caseValueRangeLower(RangeCaseElementString e) {
		Preconditions.checkNotNull(e);
		Preconditions.checkNotNull(e.rangeString);
		Preconditions.checkArgument(e.rangeString.matches("\\d+\\.\\.\\d+"), "Unexpected case value range string.");
		
		val String[] segments = e.rangeString.split("\\.\\.");
		val segment = Preconditions.checkNotNull(segments.get(0));
		return Long.parseLong(segment);
	}
	
	/**
	 * Determines and returns the upper bound (inclusive) of a RangeCaseElementString.
	 */
	def static long caseValueRangeUpper(RangeCaseElementString e) {
		Preconditions.checkNotNull(e);
		Preconditions.checkNotNull(e.rangeString);
		Preconditions.checkArgument(e.rangeString.matches("\\d+\\.\\.\\d+"), "Unexpected case value range string.");
		
		val String[] segments = e.rangeString.split("\\.\\.");
		val segment = Preconditions.checkNotNull(segments.get(1));
		return Long.parseLong(segment);
	}
	
	/**
	 * Returns the name element's name as it is in the source code (i.e., it contains the # prefix if present).
	 * The returned string is trimmed (does not start or end with whitespace).
	 */
	def static String namedElementAsInSourceCode(NamedElement e) {
		return NodeModelUtils.getTokenText(NodeModelUtils.findActualNodeFor(e)).trim;
	}
	
	/**
	 * Returns the named value reference's name as it is in the source code (i.e., it contains the # prefix if present).
	 * The returned string is trimmed (does not start or end with whitespace).
	 */
	def static String namedReferenceAsInSourceCode(NamedValueRef e) {
		return NodeModelUtils.getTokenText(NodeModelUtils.findActualNodeFor(e)).trim;
	}
	
	/**
	 * Returns the named element pointed by the first segment of a potentially qualified reference.
	 */
	def static dispatch NamedElement getFirstPrefixOfReference(QualifiedRef r) {
		return r.prefix.getFirstPrefixOfReference;
	}
	
	/**
	 * Returns the named element pointed by the first segment of a potentially qualified reference.
	 */
	def static dispatch NamedElement getFirstPrefixOfReference(ArrayRef r) {
		return r.ref.getFirstPrefixOfReference;
	}
	
	def static dispatch NamedElement getFirstPrefixOfReference(DirectBitAccessRef r) {
		return r.ref.getFirstPrefixOfReference;
	}
	
	
	/**
	 * Returns the named element pointed by the first segment of a potentially qualified reference.
	 */
	def static dispatch NamedElement getFirstPrefixOfReference(DirectNamedRef r) {
		return r.ref;
	}
	
	def static dispatch NamedElement getFirstPrefixOfReference(DirectRef r) {
		return null; // ?
	}
	
	
		
	/**
	 * Used to fetch the base datatype of the variable in a DirectBitAccessRef
	 */
	def static dispatch DataType getBaseDatatype(DirectBitAccessRef r){
		return r.ref.getBaseDatatype
	}
	
	def static dispatch DataType getBaseDatatype(QualifiedRef r){
		return r.ref.getBaseDatatype
	}
	
	def static dispatch DataType getBaseDatatype(ArrayRef r){
		return r.ref.getBaseDatatype
	}
	
	def static dispatch DataType getBaseDatatype(DirectNamedRef r){
		return r.ref.getBaseDatatype
	}
	
	def static dispatch DataType getBaseDatatype(Variable r){
		return r.eContainer.getBaseDatatype
	}
	
	def static dispatch DataType getBaseDatatype(VariableDeclarationLine r){
		return r.type.getBaseDatatype
	}
	
	def static dispatch DataType getBaseDatatype(ArrayDT r){
		return r.baseType
	}
	
	def static dispatch DataType getBaseDatatype(ElementaryDT r){
		return r
	}
	
	//def static dispatch DataType getBaseDatatype(ArrayDT r){
	//	return r.baseType
	//}
	
	
	
	/**
	 * Returns the variable with the given name defined in the given executable program unit.
	 * If variable with such name does not exists, it throws an exception.
	 * 
	 * @param programUnit Executable program unit where the variable is searched for
	 * @param varName Name of variable that is searched for
	 * @return The variable with the given name
	 * @throws Step7AstTransformationException if such named variable is not found
	 */
	def static Variable findVariable(ExecutableProgramUnit programUnit, String varName) {
		for (VariableDeclarationBlock vdb : programUnit.getDeclarationSection().getVariableDeclarations()) {
			for (VariableDeclarationLine vdl : vdb.getVariables()) {
				for (Variable v : vdl.getVariables()) {
					if (v.getName().equalsIgnoreCase(varName)) {
						return v;
					}
				}
			}
		}

		throw new Step7AstTransformationException(
				String.format("Variable '%s' has not been found in '%s'.", varName, programUnit.getName()));
	}
	
	/**
	 * Returns the hierarchical name of the given named element,
	 * i.e., the concatenation of all named elements' names in the EMF hierarchy.
	 */
	def static String getHierarchicalName(NamedElement named) {
		val NamedElement parentNamed = EmfHelper.getContainerOfType(named.eContainer(), NamedElement);
		if (parentNamed === null) {
			return named.getName();
		} else {
			return getHierarchicalName(parentNamed) + "/" + named.getName();
		}
	}
	
}

