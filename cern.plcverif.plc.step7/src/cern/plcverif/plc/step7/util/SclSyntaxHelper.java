/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.google.common.base.Preconditions;

/**
 * Utility class to implement parts of the SCL language description. Based on
 * the following document: [SCL] "S7-SCL V5.3 for S7-300/400, A5E00324650-01",
 * https://support.industry.siemens.com/cs/attachments/5581793/SCL_e.pdf
 */
public final class SclSyntaxHelper {
	@SuppressWarnings("boxing")
	private static final HashSet<Character> SPECIAL_CHARACTERS = new HashSet<>(
			Arrays.asList('+', '-', '*', '/', '=', '<', '>', '[', ']', '(', ')', ':', ';', '$', '#',
					'\"', '\'', '{', '}', '%', '.', ','));

	/**
	 * Names of dummy global variables, which should not be directly represented
	 * in the CFA. They are assumed to be upper case.
	 */
	private static final Set<String> IGNORED_GLOBAL_VARIABLES = new HashSet<>(Arrays.asList(
		//@formatter:off
		"I", "IX", "IB", "IW", "ID",
		"Q", "QX", "QB", "QW", "QD",
		"M", "MX", "MB", "MW", "MD",
		"PIB", "PIW", "PID",
		"PQB", "PQW", "PQD"
		//@formatter:on
	));
	
	private static final Set<String> SCL_ONLY_KEYWORDS = new HashSet<>(Arrays.asList("AND", "AT",
			"BY", "CASE", "CONST", "CONTINUE", "DIV", "DO", "ELSE", "ELSIF", "EXIT", "FOR", "GOTO", "IF", "MOD", "NIL",
			"NOT", "OK", "OR", "PROGRAM", "REPEAT", "RETURN", "THEN", "TO", "UNTIL", "WHILE", "XOR"));

	private SclSyntaxHelper() {
		// Utility class.
	}

	/**
	 * Determines whether the given character is a special character in SCL.
	 * Implementation of the rules in [SCL] Sec. 5.2, p. 5-4.
	 */
	@SuppressWarnings("boxing")
	public static boolean isSpecialCharacter(char character) {
		return SPECIAL_CHARACTERS.contains(character);
	}

	/**
	 * Determines whether the given variable is a dummy global variable name.
	 */
	public static boolean isIgnoredGlobalVarName(String name) {
		return IGNORED_GLOBAL_VARIABLES.contains(name.toUpperCase());
	}

	public static boolean isStringConstant(String str) {
		return str.matches("'.*'");
	}
	
	/**
	 * Returns true if the given text is a keyword only in SCL code, i.e., 
	 * it can be used in non-SCL languages as identifier.
	 */
	public static boolean isSclOnlyKeyword(String str) {
		Preconditions.checkNotNull(str);
		return SCL_ONLY_KEYWORDS.contains(str.toUpperCase());
	}
	
	/**
	 * PLC name used for the variable representing the return value of non-void functions.
	 */
	public static final String RETVAL_VARIABLE_NAME = "RET_VAL";
}
