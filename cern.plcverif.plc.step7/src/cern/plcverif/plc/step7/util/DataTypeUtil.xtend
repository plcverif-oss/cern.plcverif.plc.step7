/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.util

import cern.plcverif.plc.step7.datatypes.S7AddressMemorySize
import cern.plcverif.plc.step7.datatypes.S7Integer
import cern.plcverif.plc.step7.datatypes.S7IntegerType
import cern.plcverif.plc.step7.datatypes.S7Real
import cern.plcverif.plc.step7.datatypes.S7RealType
import cern.plcverif.plc.step7.step7Language.ArrayDT
import cern.plcverif.plc.step7.step7Language.ArrayDimension
import cern.plcverif.plc.step7.step7Language.ArrayDimensionRange
import cern.plcverif.plc.step7.step7Language.ArrayDimensionString
import cern.plcverif.plc.step7.step7Language.ArrayInitialization
import cern.plcverif.plc.step7.step7Language.CompatbilityLevelEnum
import cern.plcverif.plc.step7.step7Language.ConstantInitializationElement
import cern.plcverif.plc.step7.step7Language.DataBlockStructure
import cern.plcverif.plc.step7.step7Language.DataType
import cern.plcverif.plc.step7.step7Language.ElementaryDT
import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum
import cern.plcverif.plc.step7.step7Language.FbOrUdtDT
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.FunctionBlock
import cern.plcverif.plc.step7.step7Language.InitializationRepetitionList
import cern.plcverif.plc.step7.step7Language.NamedOrUnnamedConstantRef
import cern.plcverif.plc.step7.step7Language.ParameterDT
import cern.plcverif.plc.step7.step7Language.ParameterTypeEnum
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall
import cern.plcverif.plc.step7.step7Language.Step7LanguageFactory
import cern.plcverif.plc.step7.step7Language.StringDT
import cern.plcverif.plc.step7.step7Language.StructDT
import cern.plcverif.plc.step7.step7Language.UserDefinedDataType
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine
import cern.plcverif.plc.step7.step7Language.VariableInitialization
import com.google.common.base.Preconditions
import java.util.Collections
import java.util.List
import java.util.Set
import java.util.regex.Matcher
import java.util.regex.Pattern
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

import static cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum.*
import static com.google.common.base.Preconditions.*

import static extension cern.plcverif.plc.step7.util.SimpleExpressionUtil.evaluateSimpleExpression
import cern.plcverif.plc.step7.step7Language.LongMemoryAddress
import java.util.Arrays

class DataTypeUtil {
	static final String ARRAY_RANGE_STRING_REGEX = "^(\\d+)\\.\\.(\\d+)$";
	val static Set<ElementaryTypeEnum> STEP7_DT = #{VOID, BOOL, BYTE, WORD, DWORD, CHAR, INT, DINT, REAL, S5TIME, TIME, TIME_OF_DAY, DATE, DATE_AND_TIME};
	val static Set<ElementaryTypeEnum> NEW_TIA1200_DT = #{WCHAR, WSTRING, SINT, USINT, UINT, UDINT, LINT, ULINT, LREAL, DTL};
	// val static Set<ElementaryTypeEnum> NEW_T IA1500_DT = #{LWORD, LTIME, LTIME_OF_DAY};
	
	val static Set<ElementaryTypeEnum> UNSIGNED_TYPES = #{BOOL, BYTE, CHAR, DATE, DATE_AND_TIME, DATE_AND_LTIME, DTL, DWORD, LTIME_OF_DAY, LWORD,
					TIME_OF_DAY, UDINT, ULINT, UINT, USINT, VOID, WCHAR, WORD, WSTRING};
	val static Set<ElementaryTypeEnum> SIGNED_TYPES = #{DINT, INT, LINT, LREAL, LTIME, REAL, S5TIME, SINT, TIME};
			
	
	static final ElementaryDT VOID_DT = Step7FactoryHelper.createElementaryDT(VOID); 
	static final Logger log = LogManager.getLogger(DataTypeUtil);
	
	static class InvalidArrayRangeException extends Exception {
		new(String message) {
			super(message);
		}
	}

	def static dispatch boolean isSyntacticallyValidArrayRange(ArrayDimension range) {
		return false;
	}

	def static dispatch boolean isSyntacticallyValidArrayRange(ArrayDimensionRange range) {
		// syntax check (lower and upper value shall be simple expression)
		try {
			range.from.evaluateSimpleExpression;
			range.to.evaluateSimpleExpression;
			return true;
		} catch (SimpleExpressionUtil.NotSimpleExpressionException ex) {
			return false;
		}
	}

	def static dispatch boolean isSyntacticallyValidArrayRange(ArrayDimensionString range) {
		return range.rangeString.matches(ARRAY_RANGE_STRING_REGEX);
	}

	def static dispatch int arrayRangeLower(ArrayDimension range) {
		throw new InvalidArrayRangeException("Unknown ArrayDimension type.");
	}

	def static dispatch int arrayRangeLower(ArrayDimensionRange range) {
		if (!DataTypeUtil.isSyntacticallyValidArrayRange(range)) {
			throw new InvalidArrayRangeException("Invalid ArrayDimensionRange.");
		}

		return range.from.evaluateSimpleExpression as int;
	}

	def static dispatch int arrayRangeLower(ArrayDimensionString range) {
		return arrayRangeLower(arrayDimStringToRange(range))
	}

	def static dispatch int arrayRangeUpper(ArrayDimensionRange range) {
		if (!DataTypeUtil.isSyntacticallyValidArrayRange(range)) {
			throw new InvalidArrayRangeException("Invalid ArrayDimensionRange.");
		}

		return range.to.evaluateSimpleExpression as int;
	}

	def static dispatch int arrayRangeUpper(ArrayDimensionString range) {
		return arrayRangeUpper(arrayDimStringToRange(range))
	}

	private static def arrayDimStringToRange(ArrayDimensionString dimensionString) {
		if (!DataTypeUtil.isSyntacticallyValidArrayRange(dimensionString)) {
			throw new Exception();
		}

		val Matcher matcher = Pattern.compile(ARRAY_RANGE_STRING_REGEX).matcher(dimensionString.rangeString);
		val success = matcher.find();
		checkState(success);
		val ArrayDimensionRange ret = Step7LanguageFactory.eINSTANCE.createArrayDimensionRange();

		val fromConst = Step7LanguageFactory.eINSTANCE.createIntConstant();
		fromConst.value = S7Integer.create(matcher.group(1));
		ret.from = fromConst;

		val toConst = Step7LanguageFactory.eINSTANCE.createIntConstant();
		toConst.value = S7Integer.create(matcher.group(2));
		ret.to = toConst;
		return ret;
	}
	
	def static arrayDimensionLength(ArrayDimension dimension) {
		val length = dimension.arrayRangeUpper - dimension.arrayRangeLower + 1;
		return length;
	}
	
	/**
	 * Enumerates all valid indices of the given array data type
	 * (which may be multi-dimensional).
	 * @return List of the valid indices 
	 */
	def static List<long[]> enumerateValidArrayIndices(ArrayDT type) {
		val long[] current = newLongArrayOfSize(type.dimensions.size);
		val List<long[]> ret = newArrayList();
		
		// Initialize
		for (int idx : 0..type.dimensions.size-1) {
			current.set(idx, arrayRangeLower(type.dimensions.get(idx))); 
		}
		
		// Enumerate
		while (true) {
			ret.add(Arrays.copyOf(current, current.length));
			
			var break = false;
			for (idx : 0..type.dimensions.size) {
				if (!break) {
					if (idx == type.dimensions.size) {
						// Carry to far -- end of enumeration
						return ret;
					}
					
					if (current.get(idx) == arrayRangeUpper(type.dimensions.get(idx))) {
						// Carry needed
						current.set(idx, arrayRangeLower(type.dimensions.get(idx)))
					} else {
						current.set(idx, current.get(idx) + 1);
						break = true;
					}
				}
			}
		}
	}
	
	def static flattenArrayInitialization(VariableInitialization init) {
		val List<NamedOrUnnamedConstantRef> out = newArrayList();
		flattenArrayInitialization(init, out);
		return Collections.unmodifiableList(out);
	}
	
	private def static dispatch void flattenArrayInitialization(VariableInitialization init, List<NamedOrUnnamedConstantRef> out) {
		log.debug("flattenArrayInitialization called with unsupported init parameter: " + init);
	}
	
	private def static dispatch void flattenArrayInitialization(ArrayInitialization init, List<NamedOrUnnamedConstantRef> out) {
		for (element : init.elements) {
			flattenArrayInitialization(element, out);
		}
	}
	
	private def static dispatch void flattenArrayInitialization(ConstantInitializationElement init, List<NamedOrUnnamedConstantRef> out) {
		out.add(init.constant);
	}
	
	private def static dispatch void flattenArrayInitialization(InitializationRepetitionList init, List<NamedOrUnnamedConstantRef> out) {
		val times = SimpleExpressionUtil.evaluateSimpleExpression(init.times) as int;
		for (int i : 1..times) {
			flattenArrayInitialization(init.toRepeat, out);
		}
	}

	def static boolean isNumeric(DataType type) {
		if (type instanceof ElementaryDT) {
			return type.type.isNumeric;
		}
		return false;
	}

	def static boolean isNumeric(ElementaryTypeEnum type) {
		return type.isInteger || type.isReal;
	}

	def static boolean isReal(DataType type) {
		return (type instanceof ElementaryDT) && (type as ElementaryDT).type.isReal;
	}

	def static boolean isReal(ElementaryTypeEnum type) {
		return type == REAL || type == LREAL;
	}

	def static boolean isInteger(DataType type) {
		if (type instanceof ElementaryDT) {
			return type.type.isInteger;
		}
		return false;
	}

	def static boolean isInteger(ElementaryTypeEnum type) {
		return type.isSignedInteger || type.isUnsignedInteger;
	}

	def static boolean isSignedInteger(ElementaryTypeEnum type) {
		return type == SINT || type == INT || type == DINT || type == LINT;
	} 

	def static boolean isUnsignedInteger(ElementaryTypeEnum type) {
		return type == USINT || type == UINT || type == UDINT || type == ULINT;
	}
	
	def static boolean isTimeOrDateLike(ElementaryTypeEnum type) {
		return type.isTimeLike || type.isDate || type.isDateTime;
	}

	/**
	 * True if the given data type is a time-like type (e.g., TIME, LTIME, S5TIME).
	 * Shorthand for {@link #isTimeLike(ElementaryTypeEnum)}.
	 */
	def static boolean isTimeLike(DataType type) {
		if (type instanceof ElementaryDT) {
			return type.type.isTimeLike;
		}
		return false;
	}

	/**
	 * True if the given data type is a time-like type, i.e., 
	 * it is TIME, LTIME or S5TIME.
	 */
	def static boolean isTimeLike(ElementaryTypeEnum type) {
		return type == S5TIME || type == TIME || type == LTIME;
	}
	
	def static boolean isTime(DataType type) {
		if (type instanceof ElementaryDT) {
			return type.type.isTime;
		}
		return false;
	}

	def static boolean isTime(ElementaryTypeEnum type) {
		return type == TIME || type == LTIME;
	}
	
	def static boolean isDate(DataType type) {
		if (type instanceof ElementaryDT) {
			return type.type.isDate;
		}
		return false;
	}

	def static boolean isDate(ElementaryTypeEnum type) {
		return type == DATE;
	}
	
	def static boolean isDateTime(DataType type) {
		if (type instanceof ElementaryDT) {
			return type.type.isDateTime;
		}
		return false;
	}

	def static boolean isDateTime(ElementaryTypeEnum type) {
		return type == DATE_AND_TIME || type == DATE_AND_LTIME;
	}
	
	def static boolean isTimeOfDay(DataType type) {
		if (type instanceof ElementaryDT) {
			return type.type.isTimeOfDay;
		}
		return false;
	}
	
	def static boolean isTimeOfDay(ElementaryTypeEnum type) {
		return type == TIME_OF_DAY || type == LTIME_OF_DAY;
	}

	def static boolean equalsS5Time(DataType type) {
		if (type instanceof ElementaryDT) {
			return type.type == S5TIME;
		}
		return false;
	}
	
	def static boolean equalsTime(DataType type) {
		if (type instanceof ElementaryDT) {
			return type.type == TIME;
		}
		return false;
	}
	
	/**
	 * True iff the given type is CHAR or WCHAR.
	 */
	def static boolean isChar(DataType type) {
		if (type instanceof ElementaryDT) {
			return type.type.isChar;
		}
		return false;
	}

	/**
	 * True iff the given type is CHAR or WCHAR.
	 */
	def static boolean isChar(ElementaryTypeEnum type) {
		return type == CHAR || type == WCHAR;
	}

	def static ElementaryTypeEnum realDTtoElementaryDT(S7RealType type) {
		switch (type) {
			case REAL: return REAL
			case LREAL: return LREAL
			case Unknown: throw new RuntimeException("XXX")
		}
	}

	/**
	 * Returns true iff the given type is a parameter type, as defined
	 * in [SCL] Section 7.5 (p. 7-15).
	 * According to the documentation, parameter types are the following:
	 * TIMER, COUNTER, BLOCK_[FB|FC|DB\SDB], ANY, POINTER. These types
	 * can only be used as types of block parameters.
	 */
	def static boolean isParamType(DataType type) {
		if (type instanceof ParameterDT) {
			switch (type.type) {
				case ParameterTypeEnum.TIMER,
				case ParameterTypeEnum.COUNTER,
				case ParameterTypeEnum.BLOCK_FB,
				case ParameterTypeEnum.BLOCK_FC,
				case ParameterTypeEnum.BLOCK_DB,
				case ParameterTypeEnum.BLOCK_SDB,
				case ParameterTypeEnum.ANY,
				case ParameterTypeEnum.POINTER:
					return true
				default: 
					return false
			}
		}
		return false;
	}

	def static ElementaryTypeEnum integerDTtoElementaryDT(S7IntegerType type) {
		switch (type) {
			case BOOL: return BOOL
			case BYTE: return BYTE
			case DINT: return DINT
			case DWORD: return DWORD
			case INT: return INT
			case UDINT: return UDINT
			case UINT: return UINT
			case WORD: return WORD
			case LINT: return LINT
			case SINT: return SINT
			case ULINT: return ULINT
			case USINT: return USINT
			case Unknown: throw new RuntimeException("XXX")
		}
	}

	def static S7RealType elementaryDTtoRealDT(ElementaryTypeEnum type) {
		switch (type) {
			case REAL: return S7RealType.REAL
			case LREAL: return S7RealType.LREAL
			default: return S7RealType.Unknown
		}
	}

	def static S7IntegerType elementaryDTtoIntegerDT(ElementaryTypeEnum type) {
		switch (type) {
			case BOOL: return S7IntegerType.BOOL
			case BYTE: return S7IntegerType.BYTE
			case DINT: return S7IntegerType.DINT
			case DWORD: return S7IntegerType.DWORD
			case INT: return S7IntegerType.INT
			case LINT: return S7IntegerType.LINT
			case SINT: return S7IntegerType.SINT
			case UDINT: return S7IntegerType.UDINT
			case UINT: return S7IntegerType.UINT
			case ULINT: return S7IntegerType.ULINT
			case USINT: return S7IntegerType.USINT
			case WORD: return S7IntegerType.WORD
			default: return S7IntegerType.Unknown
		}
	}

	def static boolean isBoolean(ElementaryTypeEnum type) {
		return type === BOOL;
	}

	def static isBooleanElementaryType(DataType type) {
		if (type === null) {
			return false;
		}
		if (type instanceof ElementaryDT) {
			if (type.type == BOOL) {
				return true;
			}
		}

		return false;
	}

	def static dispatch String toString(DataType type) {
		return type.toString();
	}

	def static dispatch String toString(ElementaryDT type) {
		return type.type.toString();
	}

	def static dispatch String toString(ParameterDT type) {
		return type.type.toString();
	}

	def static dispatch String toString(FbOrUdtDT type) {
		return type.type.name;
	}

	def static dispatch String toString(StringDT type) {
		return '''STRING[«if (type.dimension === null) "?" else SimpleExpressionUtil.evaluateSimpleExpression(type.dimension)»]''';
	}
	
	def static dispatch String toString(ArrayDT type) {
		return '''ARRAY[«FOR dim : type.dimensions SEPARATOR ', '»«dim.arrayRangeLower»..«dim.arrayRangeUpper»«ENDFOR»] OF «toString(type.baseType)»''';
	}

	def static dispatch String toString(StructDT type) {
		return '''STRUCT «Step7LanguageHelper.containedVariables(type).map[it.name].join(", ")»''';
	}

	def static dispatch String toString(Void type) {
		return "<Undefined>";
	}

	def static matchesAnyNum(ElementaryTypeEnum type) {
		return isNumeric(type);
	}

	def static matchesAnyBit(ElementaryTypeEnum type) {
		return type == BOOL || type == BYTE || type == WORD || type == DWORD || type == LWORD;
	}
	
	def static isBitArray(DataType type) {
		if (type instanceof ElementaryDT) {
			return type.type.isBitArray;
		}
		return false;
	}
	
	def static isBitArray(ElementaryTypeEnum type) {
		return type == BYTE || type == WORD || type == DWORD || type == LWORD;
	}

	def static boolean isVoid(DataType type) {
		if (type instanceof ElementaryDT) {
			if (type.type == ElementaryTypeEnum.VOID) {
				return true;
			}
		}
		return false;
	}
	
	def static isElementaryType(DataType type) {
		return (type instanceof ElementaryDT);
	}
	
	def static isArrayType(DataType type) {
		return (type instanceof ArrayDT);
	}

	def static boolean isAny(DataType type) {
		if (type instanceof ParameterDT) {
			if (type.type == ParameterTypeEnum.ANY) {
				return true;
			}
		}
		return false;
	}

	def static boolean isAnyBit(DataType type) {
		if (type instanceof ParameterDT) {
			if (type.type == ParameterTypeEnum.ANY_BIT) {
				return true;
			}
		}
		return false;
	}

	def static boolean isAnyNum(DataType type) {
		if (type instanceof ParameterDT) {
			if (type.type == ParameterTypeEnum.ANY_NUM) {
				return true;
			}
		}
		return false;
	}

	def static ElementaryTypeEnum largerBitDataType(ElementaryTypeEnum type1, ElementaryTypeEnum type2) {
		// In STEP 7 the INT->WORD implicit conversion is NOT allowed (checked in V5.6+HF3, 11/10/2018)
		// checkArgument(type1.matchesAnyBit, "%s is not a BIT type.", type1)
		// checkArgument(type2.matchesAnyBit, "%s is not a BIT type.", type2)

		if (type1 == LWORD || type2 == LWORD) {
			return LWORD;
		}
		if (type1 == DWORD || type2 == DWORD) {
			return DWORD;
		}
		if (type1 == WORD || type2 == WORD) {
			return WORD;
		}
		if (type1 == BYTE || type2 == BYTE) {
			return BYTE;
		}
		if (type1 == BOOL && type2 == BOOL) {
			return BOOL;		
		}
		throw new UnsupportedOperationException('''Unable to compute largerBitDataType(«type1.literal», «type2.literal»).''');
	}
	
	def static ElementaryTypeEnum elementaryTypeOfAddress(LongMemoryAddress addr) {
		return elementaryTypeOfMemorySize(addr.value.memorySize);
	}
	
	def static ElementaryTypeEnum elementaryTypeOfMemorySize(S7AddressMemorySize memorySize) {
		switch (memorySize) {
			case Boolean: return ElementaryTypeEnum.BOOL
			case Byte: return ElementaryTypeEnum.BYTE
			case Word: return ElementaryTypeEnum.WORD
			case Dword: return ElementaryTypeEnum.DWORD
		}
		
		throw new IllegalStateException("Unknown type in elementaryTypeOfMemorySize.");
	}
	
	/**
	 * Returns true if the given data type is {@link FbOrUdtDT},
	 * referring to a block of type {@link FunctionBlock}.
	 */
	def static isFbDataType(DataType type) {
		if (type instanceof FbOrUdtDT) {
			if (type.type instanceof FunctionBlock) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns true if the given data type is a UDT or locally defined struct.
	 * (This DOES NOT guarantee that the type is of type StructDT. Use 
	 * {@code Step7LanguageHelper.getStructDefinition} to get the struct definition. 
	 */
	def static isStructDataType(DataType type) {
		if (type instanceof FbOrUdtDT) {
			if (type.type instanceof UserDefinedDataType) {
				return true;
			}
		}
		if (type instanceof StructDT) {
			return true;
		}
		
		return false;
	}
	
	def static isStructDataType(DataBlockStructure type) {
		if (type instanceof DataType) {
			return isStructDataType(type as DataType);
		}
		
		Preconditions.checkState(false, "Impossible scenario.");
	}
	
	/**
	 * Returns the StructDT defining the given data type. The given data type should be 
	 * an anonymous struct ({@link StructDT}) or a UDT ({@link FbOrUdtDT} referring to 
	 * a {@link UserDefinedDataType}).
	 */
	def static StructDT getStructDefinition(DataType structDataType) {
		Preconditions.checkState(isStructDataType(structDataType), "The given argument is not a struct data type.");
		
		if (structDataType instanceof FbOrUdtDT) {
			if (structDataType.type instanceof UserDefinedDataType) {
				return (structDataType.type as UserDefinedDataType).declaration;
			}
		}
		if (structDataType instanceof StructDT) {
			return structDataType;
		}
		
		throw new UnsupportedOperationException("The given argument is not a struct data type.");
	}
	
	
	def static boolean isSupportedDt(ElementaryTypeEnum type, CompatbilityLevelEnum compatibilityLevel) {
		switch (compatibilityLevel) {
			case STEP7_V55: return isStep7SupportedDt(type)
			case TIA1200: return isTia1200SupportedDt(type)
			case TIA1500: return true
			case TIA300: return isStep7SupportedDt(type)
			
		}
	}
	
	def static boolean isStep7SupportedDt(ElementaryTypeEnum type) {
		return STEP7_DT.contains(type);		
	}
	
	def static boolean isTia1200SupportedDt(ElementaryTypeEnum type) {
		return isStep7SupportedDt(type) || NEW_TIA1200_DT.contains(type);		
	}
	
	
	def static DataType returnType(SclSubroutineCall call) {
		if (call.calledUnit instanceof Function) {
			return (call.calledUnit as Function).returnType;
		} else {
			return VOID_DT;	
		}
	}
	
	/**
	 * Returns the width of the given elementary type in bits.
	 *
	 * Based on [SCL], Sec. 7.2--7.3, pp. 7-3--7-6
	 */
	def static int getBitWidth(ElementaryTypeEnum type) {
		switch (type) {
			case BOOL:
				return 1
			case BYTE:
				return S7Integer.getSizeInBits(S7IntegerType.BYTE)
			case CHAR:
				return 8
			// TIA change to S7Char.getSizeInBits(...) once LCHAR is supported
			case DATE:
				return 16
			case DATE_AND_TIME:
				return 64
			case DINT:
				return S7Integer.getSizeInBits(S7IntegerType.DINT)
			case DWORD:
				return S7Integer.getSizeInBits(S7IntegerType.DWORD)
			case INT:
				return S7Integer.getSizeInBits(S7IntegerType.INT)
			case LINT:
				return S7Integer.getSizeInBits(S7IntegerType.LINT)
			case LREAL:
				return S7Real.getSizeInBits(S7RealType.LREAL)
			case REAL:
				return S7Real.getSizeInBits(S7RealType.REAL)
			case S5TIME:
				return 16
			case SINT:
				return S7Integer.getSizeInBits(S7IntegerType.SINT)
			case TIME:
				return 32
			case TIME_OF_DAY:
				return 32
			case UDINT:
				return S7Integer.getSizeInBits(S7IntegerType.UDINT)
			case UINT:
				return S7Integer.getSizeInBits(S7IntegerType.UINT)
			case ULINT:
				return S7Integer.getSizeInBits(S7IntegerType.ULINT)
			case USINT:
				return S7Integer.getSizeInBits(S7IntegerType.USINT)
			case VOID:
				return 0
			case WORD:
				return S7Integer.getSizeInBits(S7IntegerType.WORD)
			case DTL,
			case DATE_AND_LTIME,
			case LTIME,
			case LTIME_OF_DAY,
			case LWORD,
			case WCHAR,
			case WSTRING:
				throw new UnsupportedOperationException("Bitsize is unknown in SclSemanticsHelper.getBitWidth for " + type)
		}

		throw new UnsupportedOperationException("Bitsize is unknown in SclSemanticsHelper.getBitWidth for " + type);
	}

	def static boolean isSigned(ElementaryTypeEnum type) {
		if (UNSIGNED_TYPES.contains(type)) {
			return false;
		} else if (SIGNED_TYPES.contains(type)) {
			return true;
		} else {
			throw new UnsupportedOperationException("Unknown data type: " + type);
		}
	}
	
	/**
	 * Returns the total size of the given data type in bits.
	 * The returned value is the net size, i.e., it does not take into account 
	 * any eventual gaps in the memory layout due to variable alignment.
	 */
	def static dispatch int getNetSizeInBits(DataType type) {
		throw new UnsupportedOperationException("Unknown data type: " + type);	
	}
	
	def static dispatch int getNetSizeInBits(Void type) {
		return 0;	
	}
	
	def static dispatch int getNetSizeInBits(ElementaryDT type) {
		return getBitWidth(type.type);
	}
	
	def static dispatch int getNetSizeInBits(StructDT type) {
		var ret = 0;
		for (line : type.members) {
			ret += line.variables.length * getNetSizeInBits(line.type);	
		}
		return ret;
	}
	
	def static dispatch int getNetSizeInBits(ArrayDT type) {
		var members = 1;
		for (dim : type.dimensions){
			members = members * arrayDimensionLength(dim);
		}
		return members * getNetSizeInBits(type.baseType);
	}
	
	def static dispatch int getNetSizeInBits(FbOrUdtDT type) {
		if (type.type instanceof UserDefinedDataType) {
			val udt = type.type as UserDefinedDataType;
			return getNetSizeInBits(udt.declaration);
		} else if (type.type instanceof FunctionBlock) {
			var ret = 0;
			for (VariableDeclarationBlock block : (type.type as FunctionBlock).declarationSection.variableDeclarations) {
				for (VariableDeclarationLine line : block.variables) {
					ret = ret + line.variables.size * getNetSizeInBits(line.type);
				}
			}
			return ret;
		}
		
		throw new UnsupportedOperationException("Unknown data type: " + type);	
	}
}
