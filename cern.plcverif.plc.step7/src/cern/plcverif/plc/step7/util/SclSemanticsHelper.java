/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.util;

/**
 * Utility class to implement parts of the SCL language description. Based on
 * the following document: [SCL] "S7-SCL V5.3 for S7-300/400, A5E00324650-01",
 * https://support.industry.siemens.com/cs/attachments/5581793/SCL_e.pdf
 */
public final class SclSemanticsHelper {
	private SclSemanticsHelper() {
		// Utility class.
	}

	
}
