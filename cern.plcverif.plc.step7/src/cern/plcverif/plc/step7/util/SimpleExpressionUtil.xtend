/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.util

import cern.plcverif.plc.step7.step7Language.AdditiveExpression
import cern.plcverif.plc.step7.step7Language.AndExpression
import cern.plcverif.plc.step7.step7Language.ArrayRef
import cern.plcverif.plc.step7.step7Language.BoolConstant
import cern.plcverif.plc.step7.step7Language.ComparisonExpression
import cern.plcverif.plc.step7.step7Language.CounterConstant
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.EqualityExpression
import cern.plcverif.plc.step7.step7Language.Expression
import cern.plcverif.plc.step7.step7Language.FlagRef
import cern.plcverif.plc.step7.step7Language.IntConstant
import cern.plcverif.plc.step7.step7Language.MultiplicativeExpression
import cern.plcverif.plc.step7.step7Language.NamedConstantDeclaration
import cern.plcverif.plc.step7.step7Language.OrExpression
import cern.plcverif.plc.step7.step7Language.PowerExpression
import cern.plcverif.plc.step7.step7Language.QualifiedRef
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall
import cern.plcverif.plc.step7.step7Language.TimerConstant
import cern.plcverif.plc.step7.step7Language.UnaryExpression
import cern.plcverif.plc.step7.step7Language.UnnamedConstant
import cern.plcverif.plc.step7.step7Language.XorExpression
import cern.plcverif.plc.step7.step7Language.AbstractExpression
import cern.plcverif.plc.step7.step7Language.NamedConstantRef
import cern.plcverif.plc.step7.step7Language.UnnamedConstantRef
import cern.plcverif.plc.step7.step7Language.NegatedUnnamedConstantRef
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import cern.plcverif.plc.step7.step7Language.UnaryOperator
import cern.plcverif.plc.step7.step7Language.MemoryAddress
import cern.plcverif.plc.step7.step7Language.DirectBitAccessRef

class SimpleExpressionUtil {
	static final Logger log = LogManager.getLogger(SimpleExpressionUtil);
	
	static class NotSimpleExpressionException extends Exception {
		new(String message) {
			super(message);
		}
	}
	
	static dispatch def boolean isSimpleExpression(AbstractExpression e) {
		switch (e) {
			UnaryExpression: return isSimpleExpression(e.expr) && e.op == UnaryOperator.MINUS
			MultiplicativeExpression: return isSimpleExpression(e.left) && isSimpleExpression(e.right)
			AdditiveExpression: return isSimpleExpression(e.left) && isSimpleExpression(e.right)
			ComparisonExpression: return false
			EqualityExpression: return false
			OrExpression: return false
			AndExpression: return false
			XorExpression: return false
			TimerConstant: return false
			CounterConstant: return false
			PowerExpression: return false
			SclSubroutineCall: return false
			MemoryAddress: return false
			QualifiedRef: return false
			FlagRef: return false
			ArrayRef: return false
			DirectBitAccessRef: return false
			NamedConstantRef: return isSimpleExpression(e.ref.value)
			UnnamedConstantRef: return isSimpleExpression(e.constant)
			NegatedUnnamedConstantRef: return isSimpleExpression(e.negatedConstant)
			default: {
				log.info("Unknown expression type in isSimpleExpression: {}. Assumption: it is not a simple expression.", e.class.name);
				return false
			}
		}
	}
	
	static dispatch def boolean isSimpleExpression(Void e) {
		log.info("Null in isSimpleExpression.");
		return false;
	}
	
	static dispatch def boolean isSimpleExpression(DirectNamedRef e) {
		if (e.ref instanceof NamedConstantDeclaration) {
			val declaration = e.ref as NamedConstantDeclaration;
			return isSimpleExpression(declaration.value);
		}
		return false;
	}
	
	static dispatch def boolean isSimpleExpression(UnnamedConstant e) {
		switch (e) {
			BoolConstant: return true
			IntConstant: return true
			default: return false
		}
		
		// Note: if taken literally, the SCL language description permits only 
		// numeric literals in the simple expressions. However, the STEP7 5.6
		// permits Boolean constants in simple expressions.
		
	}
	
	static dispatch def long evaluateSimpleExpression(AbstractExpression e) throws NotSimpleExpressionException {
		throw new NotSimpleExpressionException('''"«e»" is not a simple expression.''');
	}
	
	static dispatch def long evaluateSimpleExpression(NamedConstantRef e) throws NotSimpleExpressionException {
		return evaluateSimpleExpression(e.ref.value);
	}
	
	static dispatch def long evaluateSimpleExpression(UnnamedConstantRef e) throws NotSimpleExpressionException {
		return evaluateSimpleExpression(e.constant);
	}
	
	static dispatch def long evaluateSimpleExpression(NegatedUnnamedConstantRef e) throws NotSimpleExpressionException {
		return -1 * evaluateSimpleExpression(e.negatedConstant);
	}
	
	static dispatch def long evaluateSimpleExpression(Expression e) throws NotSimpleExpressionException {
		throw new NotSimpleExpressionException('''"«e»" is not a simple expression.''');
	}
	
	static dispatch def long evaluateSimpleExpression(Void e) throws NotSimpleExpressionException {
		throw new NotSimpleExpressionException('''The value null cannot be evaluated as a simple expression.''');
	}
	
	static dispatch def long evaluateSimpleExpression(UnaryExpression e) {
		if (e.op == UnaryOperator.MINUS) {
			return -1 * evaluateSimpleExpression(e.expr);
		}
		throw new NotSimpleExpressionException('''"«e»" is not a simple expression.''');
	}
	
	static dispatch def long evaluateSimpleExpression(MultiplicativeExpression e) {
		val left = evaluateSimpleExpression(e.left);
		val right = evaluateSimpleExpression(e.right);
		switch (e.operator) {
			case DIVISION: return left / right
			case INTEGER_DIVISION: return left / right
			case MODULO: return left % right
			case MULTIPLICATION: return left * right
		}
	}
	
	static dispatch def long evaluateSimpleExpression(AdditiveExpression e) {
		val left = evaluateSimpleExpression(e.left);
		val right = evaluateSimpleExpression(e.right);
		switch (e.operator) {
			case PLUS: return left + right
			case MINUS: return left - right
		}
	}
	
	static dispatch def long evaluateSimpleExpression(DirectNamedRef e) {
		if (e.ref instanceof NamedConstantDeclaration) {
			val declaration = e.ref as NamedConstantDeclaration;
			return evaluateSimpleExpression(declaration.value);
		}
		throw new NotSimpleExpressionException('''"«e»" is not a simple expression.''');
	}
	
	static dispatch def long evaluateSimpleExpression(BoolConstant e) {
		return if (e.value.boolValue) 1 else 0;
	}
	
	static dispatch def long evaluateSimpleExpression(IntConstant e) {
		return e.value.intValue
	}
}