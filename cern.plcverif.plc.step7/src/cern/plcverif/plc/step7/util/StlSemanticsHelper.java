/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.util;

import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum;
import cern.plcverif.plc.step7.step7Language.StlWordLogicMnemonic;
import cern.plcverif.plc.step7.step7Language.StlWordLogicStatement;

/**
 * Utility class to implement parts of the STL language description.
 */
public final class StlSemanticsHelper {
	private StlSemanticsHelper() {
		// Utility class.
	}

	/**
	 * Returns the type used for the accumulators.
	 */
	public static final ElementaryTypeEnum ACCU_TYPE = ElementaryTypeEnum.DINT;

	/**
	 * Returns true iff the {@link StlWordLogicStatement} statement with the
	 * given mnemonic expects a WORD argument.
	 * 
	 * @param mnemonic
	 *            Word logic mnemonic.
	 * @return True if the expected data size is WORD.
	 */
	public static boolean isWordSized(StlWordLogicMnemonic mnemonic) {
		switch (mnemonic) {
		case AND_WORD:
		case OR_WORD:
		case XOR_WORD:
			return true;
		default:
			return false;
		}
	}

	/**
	 * Returns true iff the {@link StlWordLogicStatement} statement with the
	 * given mnemonic expects a DWORD argument.
	 * 
	 * @param mnemonic
	 *            Word logic mnemonic.
	 * @return True if the expected data size is DWORD.
	 */
	public static boolean isDwordSized(StlWordLogicMnemonic mnemonic) {
		switch (mnemonic) {
		case AND_DWORD:
		case OR_DWORD:
		case XOR_DWORD:
			return true;
		default:
			return false;
		}
	}

	/**
	 * Returns the expected argument type for the given mnemonic.
	 * <p>
	 * The {@link StlWordLogicStatement}'s expected argument type depends on the
	 * given mnemonic. This method returns what is the expected data type.
	 *
	 * @param mnemonic
	 *            Mnemonic
	 * @return Expected data type for the given mnemonic
	 */
	public static ElementaryTypeEnum getArgumentType(StlWordLogicMnemonic mnemonic) {
		if (isDwordSized(mnemonic)) {
			return ElementaryTypeEnum.DWORD;
		} else if (isWordSized(mnemonic)) {
			return ElementaryTypeEnum.WORD;
		} else {
			throw new UnsupportedOperationException("Unknown argument type for mnemonic " + mnemonic);
		}
	}
}
