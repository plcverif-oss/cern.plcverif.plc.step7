/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.util

import cern.plcverif.plc.step7.step7Language.Step7LanguageFactory
import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum
import cern.plcverif.plc.step7.step7Language.ElementaryDT
import cern.plcverif.plc.step7.step7Language.IntConstant
import cern.plcverif.plc.step7.datatypes.S7Integer
import cern.plcverif.plc.step7.datatypes.S7Real
import cern.plcverif.plc.step7.step7Language.RealConstant

class Step7FactoryHelper {
	static final Step7LanguageFactory FACTORY = Step7LanguageFactory.eINSTANCE;
	new() {}
	
	def static ElementaryDT createElementaryDT(ElementaryTypeEnum type) {
		val ret = FACTORY.createElementaryDT();
		ret.type = type;
		return ret;
	}
	
	def static IntConstant createIntConstant(S7Integer value) {
		val ret = FACTORY.createIntConstant();
		ret.value = value;
		return ret;
	}
	
	def static RealConstant createRealConstant(S7Real value) {
		val ret = FACTORY.createRealConstant();
		ret.value = value;
		return ret;
	}
}
