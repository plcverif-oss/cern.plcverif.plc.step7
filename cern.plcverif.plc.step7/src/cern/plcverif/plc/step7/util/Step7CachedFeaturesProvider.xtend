/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.util

import cern.plcverif.plc.step7.step7Language.CompatbilityLevel
import cern.plcverif.plc.step7.step7Language.CompatbilityLevelEnum
import cern.plcverif.plc.step7.step7Language.ProgramFile
import com.google.common.base.Preconditions
import com.google.inject.Inject
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.util.IResourceScopeCache

class Step7CachedFeaturesProvider {
	static final Logger log = LogManager.getLogger(Step7CachedFeaturesProvider);
	
	public static CompatbilityLevelEnum DEFAULT_COMPATIBILITY_LEVEL = CompatbilityLevelEnum.STEP7_V55;

	@Inject IResourceScopeCache cache;

	def CompatbilityLevelEnum getCompatibilityLevelForFile(ProgramFile program) {
		if (cache === null) {
			log.error("Injection error.");
		}
		Preconditions.checkNotNull(cache, "Cache is null.");
		Preconditions.checkNotNull(program);
		Preconditions.checkNotNull(program.eResource);

		cache.get("COMPATIBILITY_LEVEL" -> program, program.eResource) [
			return noncachedCompatibilityLevel(program);
		];
	}

	private def CompatbilityLevelEnum noncachedCompatibilityLevel(ProgramFile program) {	
		val CompatbilityLevel compLevelDef = program.verificationOptions?.filter(CompatbilityLevel).findLast[true];
		if (compLevelDef === null) {
			return DEFAULT_COMPATIBILITY_LEVEL;
		} else {
			return compLevelDef.style;
		}
	}

	def CompatbilityLevelEnum getCompatibilityLevel(EObject e) {
		val root = EcoreUtil2.getContainerOfType(e, ProgramFile);
		return getCompatibilityLevelForFile(root);
	}
}
