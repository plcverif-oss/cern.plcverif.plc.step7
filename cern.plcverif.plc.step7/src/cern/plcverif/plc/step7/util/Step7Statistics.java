/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import org.eclipse.emf.ecore.EObject;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.plc.step7.step7Language.DataBlock;
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit;
import cern.plcverif.plc.step7.step7Language.GlobalVariableBlock;
import cern.plcverif.plc.step7.step7Language.ProgramFile;
import cern.plcverif.plc.step7.step7Language.ProgramUnit;
import cern.plcverif.plc.step7.step7Language.SclStatement;
import cern.plcverif.plc.step7.step7Language.SclStatementList;
import cern.plcverif.plc.step7.step7Language.StlStatement;
import cern.plcverif.plc.step7.step7Language.StlStatementList;
import cern.plcverif.plc.step7.step7Language.StructDT;
import cern.plcverif.plc.step7.step7Language.UserDefinedDataType;
import cern.plcverif.plc.step7.step7Language.Variable;
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock;

public class Step7Statistics {
	private List<ProgramFile> programFiles;

	public Step7Statistics(Collection<ProgramFile> programFiles) {
		Preconditions.checkNotNull(programFiles);
		this.programFiles = new ArrayList<>(programFiles);
	}

	/**
	 * Returns the count of all program units (FB, FC, OB, DB, UDT) in all the
	 * program files.
	 */
	public int countProgramUnits() {
		return programFiles.stream().mapToInt(it -> countProgramUnits(it)).sum();
	}

	private static int countProgramUnits(ProgramFile file) {
		return file.getProgramUnits().size();
	}

	/**
	 * Returns the count of all executable program units (FB, FC, OB) in all the
	 * program files.
	 */
	public int countExecutableProgramUnits() {
		return programFiles.stream().mapToInt(it -> countExecutableProgramUnits(it)).sum();
	}

	private static int countExecutableProgramUnits(ProgramFile file) {
		return file.getProgramUnits().stream().filter(it -> it instanceof ExecutableProgramUnit).mapToInt(it -> 1)
				.sum();
	}

	/**
	 * Returns the count of all SCL and STL statements in all the program files.
	 */
	public int countStatements() {
		return programFiles.stream().mapToInt(it -> countSclStatements(it) + countStlStatements(it)).sum();
	}

	private static int countSclStatements(ProgramFile file) {
		return count(file, SclStatement.class, it -> it.eContainer() instanceof SclStatementList);
	}

	private static int countStlStatements(ProgramFile file) {
		return count(file, StlStatement.class, it -> it.eContainer() instanceof StlStatementList);
	}

	/**
	 * Returns the count of all top level variables in all the program files. An
	 * array variable will be counted as one. A structure will be counted as
	 * one.
	 */
	public int countTopVariables() {
		return programFiles.stream().mapToInt(it -> countTopVariables(it)).sum();
	}

	private static int countTopVariables(ProgramFile file) {
		int count = 0;

		// Top-level variables in program units
		for (ProgramUnit unit : file.getProgramUnits()) {
			if (unit instanceof ExecutableProgramUnit) {
				for (VariableDeclarationBlock varBlock : ((ExecutableProgramUnit) unit).getDeclarationSection()
						.getVariableDeclarations()) {
					count += Step7LanguageHelper.getAllTopVariables(varBlock).size();
				}
			} else if (unit instanceof UserDefinedDataType) {
				count += Step7LanguageHelper.getAllTopVariables(((UserDefinedDataType) unit).getDeclaration()).size();
			} else if (unit instanceof DataBlock && Step7LanguageHelper.isSharedDb((DataBlock) unit)) {
				count += Step7LanguageHelper.getAllTopVariables((StructDT) ((DataBlock) unit).getStructure()).size();
			}
		}
		
		// Top-level global variables
		for (GlobalVariableBlock varBlock : file.getGlobalVariables()) { 
			count += Step7LanguageHelper.getAllTopVariables(varBlock).size();
		}
		
		return count;
	}

	/**
	 * Returns the count of all defined variable names in all the program files
	 */
	public int countVariableNames() {
		return programFiles.stream().mapToInt(it -> countVariableNames(it)).sum();
	}

	private static int countVariableNames(ProgramFile file) {
		return count(file, Variable.class, it -> true);
	}

	// Helper methods

	private static <T extends EObject> int count(ProgramFile file, Class<T> clazz, Predicate<T> countIfTrue) {
		return EmfHelper.getAllContentsOfType(file, clazz, true, countIfTrue).size();
	}
}
