/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.ide.syntaxcoloring

import cern.plcverif.plc.step7.step7Language.AuthorAttribute
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.FamilyAttribute
import cern.plcverif.plc.step7.step7Language.FbOrUdtDT
import cern.plcverif.plc.step7.step7Language.LabeledSclStatement
import cern.plcverif.plc.step7.step7Language.LabeledStlStatement
import cern.plcverif.plc.step7.step7Language.NameAttribute
import cern.plcverif.plc.step7.step7Language.NamedCallParameterItem
import cern.plcverif.plc.step7.step7Language.NamedElement
import cern.plcverif.plc.step7.step7Language.NamedOrUnnamedConstantRef
import cern.plcverif.plc.step7.step7Language.SclGotoStatement
import cern.plcverif.plc.step7.step7Language.Step7LanguagePackage
import cern.plcverif.plc.step7.step7Language.SubroutineCall
import cern.plcverif.plc.step7.step7Language.TitleAttribute
import cern.plcverif.plc.step7.step7Language.Variable
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.xtext.ide.editor.syntaxcoloring.DefaultSemanticHighlightingCalculator
import org.eclipse.xtext.ide.editor.syntaxcoloring.HighlightingStyles
import org.eclipse.xtext.ide.editor.syntaxcoloring.IHighlightedPositionAcceptor
import org.eclipse.xtext.util.CancelIndicator

class Step7LanguageSemanticHighlightingCalculator extends DefaultSemanticHighlightingCalculator {
	// Based on https://blogs.itemis.com/en/xtext-hint-identifiers-conflicting-with-keywords
	
	private static class ClassFeaturePair {
		Class<?> clazz;
		EStructuralFeature feature;
		new (Class<?> clazz, EStructuralFeature feature) {
			this.clazz = clazz;
			this.feature = feature;
		}
	}
	
	val NAMELIKE_FEATURES =
		#[
			new ClassFeaturePair(NamedElement, Step7LanguagePackage.eINSTANCE.namedElement_Name),
			new ClassFeaturePair(Variable, Step7LanguagePackage.eINSTANCE.variable_Ref),
			new ClassFeaturePair(DirectNamedRef, Step7LanguagePackage.eINSTANCE.directNamedRef_Ref),
			new ClassFeaturePair(FbOrUdtDT, Step7LanguagePackage.eINSTANCE.fbOrUdtDT_Type),
			new ClassFeaturePair(SubroutineCall, Step7LanguagePackage.eINSTANCE.subroutineCall_CalledUnit),
			new ClassFeaturePair(NamedCallParameterItem, Step7LanguagePackage.eINSTANCE.namedCallParameterItem_Parameter),
			new ClassFeaturePair(NamedOrUnnamedConstantRef, Step7LanguagePackage.eINSTANCE.namedConstantRef_Ref),
			new ClassFeaturePair(LabeledSclStatement, Step7LanguagePackage.eINSTANCE.labeledSclStatement_Labels),
			new ClassFeaturePair(LabeledStlStatement, Step7LanguagePackage.eINSTANCE.labeledStlStatement_Label),
			new ClassFeaturePair(SclGotoStatement, Step7LanguagePackage.eINSTANCE.sclGotoStatement_Label),
			new ClassFeaturePair(TitleAttribute, Step7LanguagePackage.eINSTANCE.titleAttribute_Title),
			new ClassFeaturePair(AuthorAttribute, Step7LanguagePackage.eINSTANCE.authorAttribute_Author),
			new ClassFeaturePair(NameAttribute, Step7LanguagePackage.eINSTANCE.nameAttribute_Content),
			new ClassFeaturePair(FamilyAttribute, Step7LanguagePackage.eINSTANCE.familyAttribute_Family)
		];
	
	override protected highlightElement(EObject object, IHighlightedPositionAcceptor acceptor, CancelIndicator cancelIndicator) {
		// Fix syntax highlighting for names that coincide with STL keywords
		
		// PERF this may be too slow
		for (item : NAMELIKE_FEATURES) {
			if (item.clazz.isInstance(object)) {
				highlightFeature(acceptor, object, item.feature, HighlightingStyles.DEFAULT_ID);
			}
		}
		return false;
	}
}
