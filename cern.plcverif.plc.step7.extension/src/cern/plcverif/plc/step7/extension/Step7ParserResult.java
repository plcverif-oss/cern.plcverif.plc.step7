/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.extension;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.validation.FeatureBasedDiagnostic;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.Provider;

import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.common.logging.PlcverifSeverity;
import cern.plcverif.base.common.utils.UiUtils;
import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.interfaces.exceptions.AstParsingException;
import cern.plcverif.base.interfaces.exceptions.AtomParsingException;
import cern.plcverif.base.interfaces.exceptions.CfaGenerationException;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.OriginalDataTypeFieldAnnotation;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.expr.AtomicExpression;
import cern.plcverif.base.models.expr.IntLiteral;
import cern.plcverif.plc.step7.cfa.AtomicParser;
import cern.plcverif.plc.step7.cfa.Step7CodeToCfa;
import cern.plcverif.plc.step7.cfa.Step7CodeToCfa.Step7CodeToCfaResult;
import cern.plcverif.plc.step7.cfa.exceptions.PlcCodeToCfaException;
import cern.plcverif.plc.step7.datatypes.DatatypesPackage;
import cern.plcverif.plc.step7.datatypes.S7AddressMemoryIdentifier;
import cern.plcverif.plc.step7.datatypes.S7Integer;
import cern.plcverif.plc.step7.datatypes.S7IntegerType;
import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum;
import cern.plcverif.plc.step7.step7Language.ProgramFile;
import cern.plcverif.plc.step7.util.Step7LanguageHelper;
import cern.plcverif.plc.step7.validation.Step7LanguageValidator;

public class Step7ParserResult implements IParserLazyResult {
	@Inject
	private Provider<XtextResourceSet> resourceSetProvider;

	private Map<String, String> fileContents = null;

	private List<ProgramFile> programFiles = null;
	private ProgramFile mainProgramFile = null;
	private AtomicParser atomicParser = null;
	private Step7Settings settings = null;
	private JobResult jobResult = null;
	private Map<URI, String> uriFileNameMapping = new HashMap<>();
	
	private static final CfaToString TO_STEP7_STRING = new CfaToString(true) {
		@Override
		protected String segmentSeparator() {
			return ".";
		}
	};

	void setFileContents(Map<String, String> fileContents) {
		// Note: it should be in the constructor, but this class is instantiated
		// using dependency injection.
		this.fileContents = fileContents;
	}

	void setSettings(Step7Settings settings) {
		this.settings = settings;
	}

	@Override
	public Set<String> getFileNames() {
		return fileContents.keySet();
	}

	@Override
	public Step7AstReference getAst() throws AstParsingException {
		loadAllIfNeeded(true);

		return Step7AstReference.create(programFiles, mainProgramFile);
	}

	public Step7AstReference getAstWithoutValidation() throws AstParsingException {
		loadAllIfNeeded(false);

		return Step7AstReference.create(programFiles, mainProgramFile);
	}

	@Override
	public CfaNetworkDeclaration generateCfa(JobResult result) throws AstParsingException, CfaGenerationException {
		Preconditions.checkState(settings != null, "The settings was not loaded for Step7ParserResult.");

		try {
			loadAllIfNeeded(true);

			Step7CodeToCfaResult conversionResult;
			if (settings.isEntryBlockSet()) {
				// if the entry block is set, locate the main program file based
				// on that
				conversionResult = Step7CodeToCfa.convert(result.currentStage(), programFiles,
						settings.getEntryBlock());
			} else {
				// entry block is not known, hopefully there is only one program
				// file, and its first appropriate block will be the entry point
				// (or OB1)
				if (programFiles.size() > 1) {
					result.currentStage().logWarning(
							"No entry block was given and there are multiple program files given. %s was selected to be the main program file, but this may be incorrect.",
							mainProgramFile);
				}
				conversionResult = Step7CodeToCfa.convert(result.currentStage(), mainProgramFile);
			}

			this.atomicParser = conversionResult.getAtomicParser();
			return conversionResult.getCfa();
		} catch (PlcCodeToCfaException e) {
			throw new CfaGenerationException("Unable to parse the given PLC program file(s). " + e.getMessage(), e);
		}
	}

	/**
	 * Loads all program files if they aren't loaded already. Depending on the
	 * value of the given parameter, validation will be performed too.
	 *
	 * @param validate
	 *            If true, the loaded AST will be validated and the errors and
	 *            warnings will be logged
	 * @throws AstParsingException
	 *             if there is any error or warning present while parsing
	 */
	private void loadAllIfNeeded(boolean validate) throws AstParsingException {
		if (programFiles == null || mainProgramFile == null) {
			programFiles = loadAllProgramFiles();
			Preconditions.checkNotNull(programFiles);
			Preconditions.checkState(programFiles.size() >= 1, "Unsuccessful parsing: no program file has been found.");
			mainProgramFile = programFiles.get(0);
			programFiles.forEach(it -> Preconditions.checkState(
					it.eResource().getResourceSet() == mainProgramFile.eResource().getResourceSet(),
					"The loaded program files are not in the same resource set."));

			if (validate) {
				Map<Object, Object> diagnosisOptions = new HashMap<>();
				diagnosisOptions.put(Step7LanguageValidator.SKIP_EXPENSIVE_NONFATAL_VALIDATION, Boolean.TRUE);

				for (ProgramFile programFile : programFiles) {
					Diagnostic diagnostic = Diagnostician.INSTANCE.validate(programFile, diagnosisOptions);
					if (diagnostic.getSeverity() == Diagnostic.ERROR) {
						// There is a problem, log and halt

						for (Diagnostic childDiagnostic : diagnostic.getChildren()) {
							switch (childDiagnostic.getSeverity()) {
							case Diagnostic.ERROR:
								log(PlcverifSeverity.Error,
										childDiagnostic.getMessage() + " " + tryGetLocationInfo(childDiagnostic));
								break;
							case Diagnostic.WARNING:
								log(PlcverifSeverity.Warning,
										childDiagnostic.getMessage() + " " + tryGetLocationInfo(childDiagnostic));
								break;
							default: 
								// We don't care about the other severities. 
								break;
							}
						}

						throw new AstParsingException(
								"One of the source PLC programs contain a syntax error. Check the log for details.");
					}
				}
			}
		}
	}

	private static String tryGetLocationInfo(Diagnostic diagnostic) {
		if (diagnostic instanceof FeatureBasedDiagnostic) {
			ICompositeNode node = NodeModelUtils
					.findActualNodeFor(((FeatureBasedDiagnostic) diagnostic).getSourceEObject());
			return String.format("Line: %s, offset: %s.", node.getStartLine(), node.getOffset());
		} else {
			return "";
		}
	}

	@Override
	public AtomicExpression parseAtom(String atom) throws AtomParsingException {
		if (atom == null) {
			throw new AtomParsingException("It is not possible to parse 'null' as a Step7 variable or literal (atom).");
		}
		Preconditions.checkState(atomicParser != null,
				"The atomicParser of Step7 has not been initialized. It is not possible to parse an atom before calling the generateCfa() method.");
		return atomicParser.parseAtom(atom);
	}

	private List<ProgramFile> loadAllProgramFiles() throws AstParsingException {
		Preconditions.checkNotNull(fileContents, "There is no file to parse, fileContents is null.");
		Preconditions.checkState(!fileContents.isEmpty(), "There is no file to parse, fileContents is empty.");

		// Just to make sure the step7 project is loaded
		Step7LanguageHelper.memoryToVarDir(S7AddressMemoryIdentifier.I);
		DatatypesPackage.eINSTANCE.eClass();

		try {
			List<ProgramFile> result = new ArrayList<>();

			final XtextResourceSet resourceSet = this.resourceSetProvider.get();
			resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.FALSE);
			resourceSet.addLoadOption(Step7LanguageHelper.OPTION_IN_MEMORY_SUPPORT, Boolean.TRUE);

			int i = 0;
			for (Entry<String, String> kvp : fileContents.entrySet()) {
				String filename = kvp.getKey();
				String content = kvp.getValue();
				int id = i++;
				URI uri = URI.createURI("inmemory:/" + id + ".scl");
				Resource resource = resourceSet.createResource(uri);
				uriFileNameMapping.put(uri, filename);
				PlatformLogger
						.logDebug(String.format("Resource %s created for %s.", resource.getURI().toString(), filename));
				InputStream inputStream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
				resource.load(inputStream, resourceSet.getLoadOptions());
				if (!resource.getErrors().isEmpty()) {
					String message = String.format("The file '%s' contains the following errors: %s.", filename,
							errorsToString(resource.getErrors()));
					log(PlcverifSeverity.Error, message);
					throw new AstParsingException(message);
				}

				if (resource.getContents().isEmpty()) {
					log(PlcverifSeverity.Warning, String.format("The resource loaded from %s is empty.", filename));
					// it won't be possible to extract the contents, don't even
					// try
				} else {
					if (!(resource.getContents().get(0) instanceof ProgramFile)) {
						throw new AstParsingException(
								String.format("The loaded %s is not a STEP 7 program file.", filename));
					}
					ProgramFile programFile = (ProgramFile) resource.getContents().get(0);
					result.add(programFile);
				}
			}
			EcoreUtil.resolveAll(resourceSet);

			return result;
		} catch (IOException e) {
			throw new AstParsingException("Unable to parse the given file.", e);
		}
	}

	private static String errorsToString(List<Resource.Diagnostic> errors) {
		StringBuilder sb = new StringBuilder();
		for (Resource.Diagnostic error : errors) {
			sb.append(String.format("%n- %s (%s:%s)", error.getMessage(), error.getLine(), error.getColumn()));
		}
		return sb.toString();
	}

	@Override
	public String serializeAtom(AtomicExpression atom) {
		return TO_STEP7_STRING.toString(atom).toString();
	}
	
	@Override
	public String serializeAtom(AtomicExpression atom, OriginalDataTypeFieldAnnotation hint) {
		if (hint == null || hint.getPlcDataType() == null) {
			return serializeAtom(atom);
		}
		
		if (atom instanceof IntLiteral) {
			if (ElementaryTypeEnum.TIME.getLiteral().equalsIgnoreCase(hint.getPlcDataType())) {
				return String.format("T#%sms", ((IntLiteral) atom).getValue());
			} else if (ElementaryTypeEnum.BYTE.getLiteral().equalsIgnoreCase(hint.getPlcDataType())) {
				long intValue = (int)((IntLiteral) atom).getValue();
				
				String strValue = Strings.padStart(Long.toBinaryString(intValue), S7Integer.getSizeInBits(S7IntegerType.BYTE), '0');
				return String.format("B#2#%s", strValue);
			} else if (ElementaryTypeEnum.WORD.getLiteral().equalsIgnoreCase(hint.getPlcDataType())) {
				long intValue = (int)((IntLiteral) atom).getValue();
				
				String strValue = Strings.padStart(Long.toBinaryString(intValue), S7Integer.getSizeInBits(S7IntegerType.WORD), '0');
				return String.format("W#2#%s", UiUtils.separatedString(strValue, 8, '_'));
			} else if (ElementaryTypeEnum.DWORD.getLiteral().equalsIgnoreCase(hint.getPlcDataType())) {
				long intValue = (int)((IntLiteral) atom).getValue();
				
				String strValue = Strings.padStart(Long.toBinaryString(intValue), S7Integer.getSizeInBits(S7IntegerType.DWORD), '0');
				return String.format("DW#2#%s", UiUtils.separatedString(strValue, 8, '_'));
			} 
		} 
			
		// Fallback: ignore the hint
		return serializeAtom(atom);
	}

	public void setJobResult(JobResult result) {
		this.jobResult = result;
	}

	private void log(PlcverifSeverity severity, String message) {
		if (this.jobResult != null && this.jobResult.currentStage() != null) {
			jobResult.currentStage().log(severity, message);
		} else {
			PlatformLogger.INSTANCE.log(severity, message);
		}
	}

	/**
	 * Returns the origin file name for a given URI that is included in this
	 * parser result.
	 * 
	 * @param uri
	 *            Resource URI to look up
	 * @return The file name that originated the creation of the resource with
	 *         the given URI. Returns {@link Optional#empty()} if the given URI
	 *         is not known in this parser result.
	 */
	public Optional<String> getFilenameForUri(URI uri) {
		Preconditions.checkNotNull(uri, "Null is not a valid URI");
		Preconditions.checkState(uriFileNameMapping != null, "Uninitialized uriFileNameMapping");
		return Optional.ofNullable(uriFileNameMapping.get(uri));
	}
}
