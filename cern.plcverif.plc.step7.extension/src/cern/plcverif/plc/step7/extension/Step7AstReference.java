/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.extension;

import cern.plcverif.base.interfaces.IAstReference;
import cern.plcverif.plc.step7.step7Language.ProgramFile;

import java.util.Collection;

public final class Step7AstReference implements IAstReference<Collection<ProgramFile>> {
	private ProgramFile mainAst;
	private Collection<ProgramFile> allProgramFiles;

	private Step7AstReference(Collection<ProgramFile> allProgramFiles, ProgramFile mainAst) {
		this.mainAst = mainAst;
		this.allProgramFiles = allProgramFiles;
	}

	public static Step7AstReference create(Collection<ProgramFile> allProgramFiles, ProgramFile mainAst) {
		return new Step7AstReference(allProgramFiles, mainAst);
	}

	@Override
	public Collection<ProgramFile> getAst() {
		return allProgramFiles;
	}

	public ProgramFile getMainAst() {
		return mainAst;
	}
}
