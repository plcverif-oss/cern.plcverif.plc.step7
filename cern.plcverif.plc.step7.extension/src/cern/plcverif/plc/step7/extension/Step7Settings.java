/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.extension;

import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;
import cern.plcverif.plc.step7.step7Language.CompatbilityLevelEnum;

public class Step7Settings extends AbstractSpecificSettings {
	public static final String ENTRY_BLOCK = "entry";
	@PlcverifSettingsElement(name = ENTRY_BLOCK, description = "Program block which serves as entry point for the verification. If not defined, a heuristic will be used to find the entry point.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private String entryBlock = null;

	public static final String COMPILER = "compiler";
	@PlcverifSettingsElement(name = COMPILER, description = "Compiler settings to be used. Not taken into account currently.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private CompatbilityLevelEnum compiler = CompatbilityLevelEnum.STEP7_V55;

	public boolean isEntryBlockSet() {
		return entryBlock != null;
	}

	public String getEntryBlock() {
		return entryBlock;
	}

	public CompatbilityLevelEnum getCompiler() {
		return compiler;
	}
}
