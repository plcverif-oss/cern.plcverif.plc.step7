/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.extension;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import com.google.common.base.Preconditions;
import com.google.inject.Injector;

import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.plc.step7.Step7LanguageStandaloneSetup;

public class Step7ExtensionActivator implements BundleActivator {
	private static Injector injector = null;

	public static Injector getInjector() {
		if (injector == null) {
			PlatformLogger.logDebug("A new injector has been created in the STEP 7 extension. This is an error if executed as GUI.");
			injector = new Step7LanguageStandaloneSetup().createInjectorAndDoEMFRegistration();
		}
		return injector;
	}
	
	public static void setInjector(Injector newInjector) {
		if (injector == null) {
			PlatformLogger.logDebug("An injector has been provided for the STEP 7 extension, thus a new one will not be created.");
		} else {
			PlatformLogger.logError("An injector has been provided for the STEP 7 extension, but a standalone injector has already been created. This is an ERROR.");
		}
		Step7ExtensionActivator.injector = Preconditions.checkNotNull(newInjector);
	}

	@Override
	public void start(BundleContext context) throws Exception {
		PlatformLogger.logDebug("Step7ExtensionActivator.start called, injector=" + injector);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// Nothing to do here.

	}

}
