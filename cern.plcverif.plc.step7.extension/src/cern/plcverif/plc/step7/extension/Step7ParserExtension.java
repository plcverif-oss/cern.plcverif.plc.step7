/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.extension;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.interfaces.IParser;
import cern.plcverif.base.interfaces.IParserExtension;

public class Step7ParserExtension implements IParserExtension {
	public static final String CMD_ID = "step7";

	@Override
	public Optional<IParser> getDefaultParser(Map<String, String> files) {
		return Optional.empty();
	}

	@Override
	public Step7Parser createParser() {
		return Step7ExtensionActivator.getInjector().getInstance(Step7Parser.class);
	}

	@Override
	public Step7Parser createParser(SettingsElement settings) throws SettingsParserException {
		Step7Parser ret = createParser();
		ret.setSettings(SpecificSettingsSerializer.parse(settings, Step7Settings.class));
		return ret;
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "STEP 7 language frontend",
				"Provides language frontend (parser and CFA representation) for the STEP 7 programming languages.",
				Step7Settings.class);
		SpecificSettingsSerializer.fillSettingsHelp(help, Step7Settings.class);
	}
	
	@Override
	public List<String> getSupportedExtensions() {
		return Arrays.asList("scl", "awl", "stl");
	}
}
