/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.extension;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.Provider;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.interfaces.IParser;
import cern.plcverif.base.interfaces.data.JobResult;

public class Step7Parser implements IParser {
	@Inject
	Provider<Step7ParserResult> resultProvider;

	private Map<String, String> filesToParse = new HashMap<>();

	private Step7Settings settings;

	/**
	 * Creates a new Step7 parser object without any settings.
	 *
	 * This class relies on injected fields. Make sure that it is not
	 * instantiated directly, instead an injector is used to create the
	 * instances.
	 *
	 * @see Step7ParserExtension#createParser()
	 */
	public Step7Parser() {
		this(null);
	}

	/**
	 * Creates a new Step7 parser object with the given settings.
	 *
	 * This class relies on injected fields. Make sure that it is not
	 * instantiated directly, instead an injector is used to create the
	 * instances.
	 *
	 * @see Step7ParserExtension#createParser()
	 */
	public Step7Parser(Step7Settings settings) {
		this.settings = settings;
	}

	public void setSettings(Step7Settings settings) {
		this.settings = settings;
	}

	@Override
	public void setFiles(Map<String, String> files) {
		this.filesToParse = files;
	}

	@Override
	public Map<String, String> getFiles() {
		return Collections.unmodifiableMap(filesToParse);
	}

	/**
	 * May return null.
	 */
	@Override
	public Settings retrieveSettings() {
		try {
			return SpecificSettingsSerializer.toGenericSettings(settings);
		} catch (SettingsParserException | SettingsSerializerException e) {
			throw new PlcverifPlatformException("Unable to retrieve the configuration for Step7 plugin.", e);
		}
	}

	@Override
	public Step7ParserResult parseCode(JobResult result) {
		Step7ParserResult instance = resultProvider.get();
		// 'result' is not used on purpose
		instance.setFileContents(filesToParse);
		instance.setSettings(settings);
		instance.setJobResult(result);
		return instance;
	}

	/**
	 * For internal use.
	 */
	public Step7ParserResult parseCode() {
		Step7ParserResult instance = resultProvider.get();
		instance.setFileContents(filesToParse);
		return instance;
	}
}
