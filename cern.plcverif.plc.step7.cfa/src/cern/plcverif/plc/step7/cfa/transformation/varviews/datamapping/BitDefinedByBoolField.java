/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.cfa.transformation.varviews.datamapping;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.utils.TypeUtils;
import cern.plcverif.plc.step7.cfa.transformation.varviews.AstVarView;

/**
 * Data mapping that defines a one-bit-long slice of an elementary field, based
 * on a Boolean field.
 */
public class BitDefinedByBoolField extends BitDefinedByBoolExpr {
	private DataRef definingBoolVar;

	public BitDefinedByBoolField(AstVarView origin, DataRef definedData, int definedDataBit, DataRef definingBoolVar) {
		super(origin, definedData, definedDataBit);

		this.definingBoolVar = definingBoolVar;

		Preconditions.checkArgument(TypeUtils.hasBoolType(definingBoolVar));
	}

	@Override
	public boolean isInvalidatedBy(DataRef modifiedData) {
		return InvalidationHelper.isChangedBy(definingBoolVar, modifiedData);
	}

	@Override
	public Expression getDefiningBoolExpr() {
		return definingBoolVar;
	}
}
