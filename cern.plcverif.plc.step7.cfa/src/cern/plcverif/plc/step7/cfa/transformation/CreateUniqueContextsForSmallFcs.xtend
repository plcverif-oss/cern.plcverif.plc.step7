/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.transformation

import cern.plcverif.base.common.emf.EObjectSet
import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.Call
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import java.util.Map
import org.eclipse.emf.ecore.util.EcoreUtil
import cern.plcverif.base.models.cfa.utils.EquatableDataRef

class CreateUniqueContextsForSmallFcs {
	CfaNetworkDeclaration cfaNetwork;

	new(CfaNetworkDeclaration cfaNetwork) {
		this.cfaNetwork = cfaNetwork;
	}
	
	Map<AutomatonDeclaration, Integer> callCounts;
	int counter = 1;
	EObjectSet<DataRef> calleeContexts = new EObjectSet(EquatableDataRef.FACTORY);
	EObjectSet<DataRef> forbiddenCalleeContexts = new EObjectSet(EquatableDataRef.FACTORY);

	def void execute() {
		// count calls
		callCounts = newHashMap();
		cfaNetwork.automata.forEach[it | callCounts.put(it, 0)];
		for (Call call : EmfHelper.getAllContentsOfType(cfaNetwork, Call, false)) {
			callCounts.put(call.calledAutomaton, callCounts.get(call.calledAutomaton) + 1);
			calleeContexts.add(call.calleeContext);
		}

		// count the number of uses for each callee context. A callee context 
		// that is used outside of calls correspond to a stateful automaton that shall not be inlined.
		for (DataRef ref : EmfHelper.getAllContentsOfType(cfaNetwork, DataRef, false)) {
			if (calleeContexts.contains(ref) && !EmfHelper.isContainedInAny(ref, Call)) {
				forbiddenCalleeContexts.add(ref);
			}
		}
 		
 		// make contexts unique
		for (call : EmfHelper.getAllContentsOfType(cfaNetwork, Call, false)) {
			execute(call);
		}
	}
	
	private def void execute(Call call) {
		if (call.calledAutomaton.locations.size > 10) {
			// too big, don't make its context unique
			return;
		}
		
		if (callCounts.get(call.calledAutomaton) == 1) {
			// unique call, no need to change anything
			return;
		}
		
		if (forbiddenCalleeContexts.contains(call.calleeContext)) {
			//System.err.println('''Inlining shall not be performed for «call» (called automaton: «call.calledAutomaton.name»), as its context is accessed outside of this call.''');
			return;
		}
		
		if (call.calleeContext instanceof FieldRef) {
			// only FieldRefs are supported for the moment
			val Field newContext = EcoreUtil.copy((call.calleeContext as FieldRef).field);
			// TODO make it more robust, ensuring name uniqueness (or is it already ensured?)
			newContext.name = '''«newContext.name»_inlined_«counter»''';
			newContext.displayName = '''«newContext.displayName»_inlined_«counter»''';
			counter++;
			cfaNetwork.rootDataStructure.fields.add(newContext);
			call.calleeContext = CfaDeclarationSafeFactory.INSTANCE.createFieldRef(newContext);
		}
	}
}