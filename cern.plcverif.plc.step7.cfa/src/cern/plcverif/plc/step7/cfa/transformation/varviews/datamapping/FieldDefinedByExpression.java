/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.cfa.transformation.varviews.datamapping;

import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.plc.step7.cfa.transformation.varviews.AstVarView;

import java.util.Collection;
import java.util.Collections;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

/**
 * Data mapping that defines a whole elementary field, based on some expression.
 */
public abstract class FieldDefinedByExpression extends CfaDataMapping {
	private DataRef definedData;

	public FieldDefinedByExpression(AstVarView origin, DataRef definedData) {
		super(origin);
		this.definedData = definedData;
	}

	public DataRef getDefinedData() {
		return definedData;
	}

	public abstract Expression getDefiningExpression();

	@Override
	public Collection<DataRef> represent(Location sourceLoc, Location targetLoc) {
		Preconditions.checkNotNull(sourceLoc, "sourceLoc");
		Preconditions.checkNotNull(targetLoc, "targetLoc");
		Preconditions.checkArgument(sourceLoc.getParentAutomaton() == targetLoc.getParentAutomaton(),
				"sourceLoc and targetLoc are from different automata");
		Preconditions.checkArgument(sourceLoc.getParentAutomaton() instanceof AutomatonDeclaration,
				"parentAutomaton should be AutomatonDeclaration");

		AssignmentTransition transition = factory.createAssignmentTransition(NEW_LOC_NAME,
				(AutomatonDeclaration) sourceLoc.getParentAutomaton(), sourceLoc, targetLoc, factory.trueLiteral());

		// 'definedData' := 'definingExpression'
		factory.createAssignment(transition, EcoreUtil.copy(getDefinedData()), EcoreUtil.copy(getDefiningExpression()));

		return Collections.singleton(definedData);
	}

	@Override
	public boolean isInScopeOf(AutomatonDeclaration automaton) {
		return isDeclaredInAutomatonLocalStructure(CfaDeclarationUtils.getReferredField(definedData), automaton);
	}
}
