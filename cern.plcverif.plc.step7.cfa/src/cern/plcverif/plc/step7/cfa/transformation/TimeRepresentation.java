/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.transformation;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfabase.DataDirection;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.DirectionFieldAnnotation;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef;
import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.base.models.expr.IntType;
import cern.plcverif.base.models.expr.Type;
import cern.plcverif.plc.step7.cfa.impl.Step7TypeToExprType;
import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum;
import cern.plcverif.plc.step7.transformation.Step7AstTransformationException;

/**
 * This class is responsible for representing time with accuracy of one cycle in
 * the CFA declaration.
 *
 * <p>
 * It will perform the following steps:
 * <ul>
 * <li>It makes sure that there is a global field {@code __GLOBAL_TIME} with
 * type {@code TIME} and initial value {@code T#0s}.
 * <li>It makes sure that there is a global <i>input</i> field {@code T_CYCLE}
 * with type {@code TIME}. (The initial value does not matter as it will be
 * non-deterministically initialized at the beginning of each cycle.)
 * <li>It adds an assignment {@code __GLOBAL_TIME := __GLOBAL_TIME + T_CYCLE;}
 * to the <b>end of the main automaton</b>.
 * </ul>
 */
public final class TimeRepresentation {
	private static final String T_CYCLE_NAME = "T_CYCLE";
	private static final String GLOBAL_TIME_NAME = "__GLOBAL_TIME";

	private static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;
	private CfaNetworkDeclaration cfaNetwork;

	public TimeRepresentation(CfaNetworkDeclaration cfaNetwork) {
		this.cfaNetwork = cfaNetwork;
	}

	public void execute() {
		representTime(cfaNetwork);
	}

	public static void representTime(CfaNetworkDeclaration cfaNetwork) {
		Optional<Field> tCycle = findGlobalField(cfaNetwork, T_CYCLE_NAME);
		Optional<Field> globalTime = findGlobalField(cfaNetwork, GLOBAL_TIME_NAME);

		if (!globalTime.isPresent()) {
			// There is no need to represent global time if it is not used.
			return;
		}

		Preconditions.checkState(globalTime.isPresent());

		// Create T_CYCLE if it does not exist
		if (!tCycle.isPresent()) {
			tCycle = Optional.of(createTcycleField(cfaNetwork));
		} else {
			ensureCorrectTcycleDef(tCycle.get());
		}
		Preconditions.checkState(tCycle.isPresent());
		ensureCorrectGlobalTimeDef(globalTime.get());

		// HACK reduce the type of T_CYCLE (8-bit, unsigned)
		shrinkTcycleType(cfaNetwork, tCycle.get(), FACTORY.createIntType(true, 32));
		// TODO improve once restricted non-deterministic values are supported

		//addGlobalTimeIncrementAssignment(cfaNetwork.getMainAutomaton(), globalTime.get(), tCycle.get());
	}

	private static void shrinkTcycleType(CfaNetworkDeclaration cfaNetwork, Field tCycleField, IntType newType) {
		EmfHelper.checkNotContained(newType, "newType has already an eContainer.");

		if (tCycleField.getType().dataTypeEquals(newType)) {
			// no change, nothing to do
			return;
		}

		// (1) Replace the type of the tCycle field
		Type oldType = tCycleField.getType();
		tCycleField.setType(newType);

		// (2) Replace its initial value's type
		tCycleField.getInitialAssignments().clear();
		FACTORY.createInitialAssignment(tCycleField, FACTORY.createIntLiteral(0, EcoreUtil.copy(newType)));

		// (3) Replace each reference to the T_CYCLE field with type conversion
		for (FieldRef ref : EmfHelper.getAllContentsOfType(cfaNetwork, FieldRef.class, false,
				it -> it.getField() == tCycleField)) {
			if (ref.eContainer() instanceof VariableAssignment
					&& ((VariableAssignment) ref.eContainer()).getLeftValue() == ref) {
				// skip (we cannot replace the LHS)
				continue;
			}
			EcoreUtil.replace(ref, FACTORY.createTypeConversion(EcoreUtil.copy(ref), EcoreUtil.copy(oldType)));
		}
	}

	private static Field createTcycleField(CfaNetworkDeclaration cfaNetwork) {
		Field tCycleField = FACTORY.createField(T_CYCLE_NAME, cfaNetwork.getRootDataStructure(),
				Step7TypeToExprType.s7typeToExprType(ElementaryTypeEnum.TIME));
		FACTORY.createInitialAssignment(tCycleField, Step7TypeToExprType.defaultCfaValue(ElementaryTypeEnum.TIME));
		FACTORY.createDirectionFieldAnnotation(tCycleField, DataDirection.INPUT);
		return tCycleField;
	}

	private static void ensureCorrectTcycleDef(Field tCycleField) {
		if (!(tCycleField.getType() instanceof IntType)) {
			throw new Step7AstTransformationException(T_CYCLE_NAME + " has a non-integer type in the CFA.");
		}

		// Make sure it has INPUT direction annotation
		ensureDirectionAnnotation(tCycleField, DataDirection.INPUT);
	}

	private static void ensureCorrectGlobalTimeDef(Field globalTimeField) {
		if (!(globalTimeField.getType() instanceof IntType)) {
			throw new Step7AstTransformationException(GLOBAL_TIME_NAME + " has a non-integer type in the CFA.");
		}

		// Make sure it has LOCAL direction annotation
		ensureDirectionAnnotation(globalTimeField, DataDirection.LOCAL);
	}

	private static void ensureDirectionAnnotation(Field field, DataDirection requiredDirection) {
		List<DirectionFieldAnnotation> directionAnnotations = CfaUtils.getAllAnnotationsOfType(field,
				DirectionFieldAnnotation.class);
		if (directionAnnotations.size() > 1) {
			throw new Step7AstTransformationException(
					String.format("Multiple direction annotations on the '%s' field.", field.getName()));
		}

		if (directionAnnotations.size() == 1) {
			if (directionAnnotations.get(0).getDirection() != requiredDirection) {
				directionAnnotations.get(0).setDirection(requiredDirection);
			}
		} else {
			Preconditions.checkState(directionAnnotations.isEmpty());
			FACTORY.createDirectionFieldAnnotation(field, requiredDirection);
		}
	}

	private static Optional<Field> findGlobalField(CfaNetworkDeclaration cfaNetwork, String fieldName) {
		List<Field> tCycleFields = cfaNetwork.getRootDataStructure().getFields().stream()
				.filter(it -> it.getName().equalsIgnoreCase(fieldName)).collect(Collectors.toList());
		if (tCycleFields.isEmpty()) {
			return Optional.empty();
		} else if (tCycleFields.size() > 1) {
			throw new Step7AstTransformationException(
					String.format("Multiple '%s' variables have been found.", fieldName));
		} else {
			Preconditions.checkState(tCycleFields.size() == 1);
			return Optional.of(tCycleFields.get(0));
		}
	}

//	private static void addGlobalTimeIncrementAssignment(AutomatonDeclaration mainAutomaton, Field globalTime,
//			Field tCycle) {
//		Location newLoc = FACTORY.createLocation("prepare_globaltime", mainAutomaton);
//		// Move all incoming transitions and annotations of end location to the
//		// new location
//		newLoc.getIncoming().addAll(mainAutomaton.getEndLocation().getIncoming());
//		newLoc.getAnnotations().addAll(mainAutomaton.getEndLocation().getAnnotations());
//
//		// Create a new assignment transition (newLoc->endLoc) to represent:
//		// `globalTime := globalTime + tCycle`
//		AssignmentTransition incrementTime = FACTORY.createAssignmentTransition("increment_time", mainAutomaton, newLoc,
//				mainAutomaton.getEndLocation(), FACTORY.trueLiteral());
//		//@formatter:off
//		FACTORY.createAssignment(incrementTime,
//				FACTORY.createFieldRef(globalTime),
//				FACTORY.plus(
//						FACTORY.createFieldRef(globalTime),
//						FACTORY.withTypeConversionIfNeeded(FACTORY.createFieldRef(tCycle), globalTime.getType())
//					)
//			);
//		//@formatter:on
//	}
}
