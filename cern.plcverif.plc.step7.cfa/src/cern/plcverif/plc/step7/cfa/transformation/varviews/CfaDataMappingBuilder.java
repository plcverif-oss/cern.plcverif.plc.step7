/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.transformation.varviews;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils;
import cern.plcverif.base.models.expr.ElementaryType;
import cern.plcverif.base.models.expr.utils.TypeUtils;
import cern.plcverif.plc.step7.cfa.trace.AstCfaVarTrace;
import cern.plcverif.plc.step7.cfa.transformation.varviews.datamapping.CfaDataMapping;
import cern.plcverif.plc.step7.cfa.transformation.varviews.datamapping.Slice;
import cern.plcverif.plc.step7.step7Language.Variable;
import cern.plcverif.plc.step7.util.Step7LanguageHelper;

/**
 * Collection of methods to build CFA data constraint hierarchy to represent the
 * given AST variable views.
 */
public final class CfaDataMappingBuilder {
	public static class CfaDataBiMappings {
		Collection<CfaDataMapping> viewersDefinedByStorages = new ArrayList<>();
		Collection<CfaDataMapping> storagesDefinedByViewers = new ArrayList<>();

		public Collection<CfaDataMapping> getViewersDefinedByStorages() {
			return Collections.unmodifiableCollection(viewersDefinedByStorages);
		}

		public Collection<CfaDataMapping> getStoragesDefinedByViewers() {
			return Collections.unmodifiableCollection(storagesDefinedByViewers);
		}

		public List<CfaDataMapping> getAll() {
			List<CfaDataMapping> ret = new ArrayList<>();
			ret.addAll(viewersDefinedByStorages);
			ret.addAll(storagesDefinedByViewers);
			return ret;
		}
	}

	/**
	 * Represents a memory area occupied by a given elementary field, starting
	 * at position {@code startBit}.
	 */
	private static class MemoryArea {
		private DataRef dataRef;
		private int startBit;
		private int dataLength;

		public MemoryArea(DataRef dataRef, int startBit) {
			this.dataRef = dataRef;
			this.startBit = startBit;

			Preconditions.checkArgument(dataRef.getType() instanceof ElementaryType, "dataRef.type is not elementary.");
			this.dataLength = TypeUtils.getBitLength((ElementaryType) dataRef.getType());
		}

		public DataRef getDataRef() {
			return dataRef;
		}

		/**
		 * Start position (bit index) of the memory area occupied by the
		 * elementary field {@link #getDataRef()}.
		 */
		public int getStartBit() {
			return startBit;
		}

		/**
		 * End position (bit index) of the memory area occupied by the
		 * elementary field {@link #getDataRef()}.
		 */
		public int getEndBit() {
			return this.startBit + this.dataLength - 1;
		}

		@Override
		public String toString() {
			return String.format("%s (from bit %s to bit %s)", CfaToString.toDiagString(dataRef),
					Integer.toString(startBit), Integer.toString(getEndBit()));
		}
	}

	private CfaDataMappingBuilder() {
	}

	public static CfaDataBiMappings build(List<AstVarView> varViews, AstCfaVarTrace varTrace, IPlcverifLogger logger) {
		Preconditions.checkNotNull(varViews, "varViews");
		Preconditions.checkNotNull(varTrace, "varTrace");

		CfaDataBiMappings ret = new CfaDataBiMappings();
		for (AstVarView vv : varViews) {
			Preconditions.checkState(vv.getStorage() != vv.getViewer());
			ret.storagesDefinedByViewers.addAll(createMappings(vv, vv.getStorage(), vv.getViewer(), varTrace, logger));
			ret.viewersDefinedByStorages.addAll(createMappings(vv, vv.getViewer(), vv.getStorage(), varTrace, logger));
		}
		return ret;
	}

	/**
	 * Creates CFA data mappings to represent the mapping from the elementary
	 * variables of {@code definingAstVar} to the corresponding parts of
	 * {@code definedAstVar}.
	 *
	 * @param origin
	 *            The variable view in the AST that originates this mapping.
	 *            Note that {@code origin} represents a bidirectional mapping,
	 *            while the returned CFA data mappings will be unidirectional
	 *            that defines the elements of {@code definedAstVar} based on
	 *            {@code definingAstVar}, but not in the opposite direction.
	 * @param definedAstVar
	 *            The AST variable that is being defined.
	 * @param definingAstVar
	 *            The AST variable that is being used to define
	 *            {@code definedAstVar}.
	 * @param varTrace
	 *            Variable trace to locate the CFA variables corresponding to
	 *            the AST variables.
	 *        @param logger Logger to be used.
	 * @return CFA data mappings to represent the expressed mappings.
	 */
	private static Collection<CfaDataMapping> createMappings(AstVarView origin, Variable definedAstVar,
			Variable definingAstVar, AstCfaVarTrace varTrace, IPlcverifLogger logger) {
		Preconditions.checkNotNull(definedAstVar, "definedAstVar");
		Preconditions.checkNotNull(definingAstVar, "definingAstVar");
		Preconditions.checkNotNull(varTrace, "varTrace");

		// Find CFA field corresponding to the given AST variables
		if (!varTrace.containsVariableToField(definedAstVar)) {
			String message = String.format(
					"Not possible to represent the '%s -> %s' variable view. The variable '%s' does not have a corresponding representation in the CFA. This is not a problem if the missing variable is out of the verification scope.",
					Step7LanguageHelper.getHierarchicalName(definingAstVar),
					Step7LanguageHelper.getHierarchicalName(definedAstVar),
					Step7LanguageHelper.getHierarchicalName(definedAstVar));
			logger.logWarning(message);
			return Collections.emptyList();
		}
		if (!varTrace.containsVariableToField(definingAstVar)) {
			String message = String.format(
					"Not possible to represent the '%s -> %s' variable view. The variable '%s' does not have a corresponding representation in the CFA. This is not a problem if the missing variable is out of the verification scope.",
					Step7LanguageHelper.getHierarchicalName(definingAstVar),
					Step7LanguageHelper.getHierarchicalName(definedAstVar),
					Step7LanguageHelper.getHierarchicalName(definingAstVar));
			logger.logWarning(message);
			return Collections.emptyList();
		}

		Field definedField = varTrace.get(definedAstVar);
		Field definingField = varTrace.get(definingAstVar);
		Preconditions.checkNotNull(definedField,
				String.format("Field representing the variable '%s' is not found.", definedField.getName()));
		Preconditions.checkNotNull(definingField,
				String.format("Field representing the variable '%s' is not found.", definingField.getName()));

		return createElementaryMappings(origin, definedField, definingField);
	}

	/**
	 * Creates CFA data mappings to represent the mapping from the elementary
	 * fields of {@code definingField} to the corresponding parts of
	 * {@code definedField}.
	 *
	 * @param origin
	 *            The variable view in the AST that originates this mapping.
	 * @param definedField
	 *            The CFA field that is being defined.
	 * @param definingField
	 *            The CFA field that is used to define {@code definedField}.
	 * @return CFA data mappings to represent the expressed mappings.
	 */
	private static List<CfaDataMapping> createElementaryMappings(AstVarView origin, Field definedField,
			Field definingField) {
		FieldRef definedFieldRef = CfaDeclarationSafeFactory.INSTANCE.createFieldRef(definedField);
		FieldRef definingFieldRef = CfaDeclarationSafeFactory.INSTANCE.createFieldRef(definingField);

		List<DataRef> definedElements = CfaDeclarationUtils.getAllElementaryDataElements(definedFieldRef);
		List<DataRef> definingElements = CfaDeclarationUtils.getAllElementaryDataElements(definingFieldRef);

		List<MemoryArea> definedMA = elementsToMemoryAreas(definedElements);
		List<MemoryArea> definingMA = elementsToMemoryAreas(definingElements);

		//@formatter:off
		int[] endBits = Stream.concat(definedMA.stream(), definingMA.stream())
				.mapToInt(it -> it.getEndBit())
				.distinct()
				.sorted()
				.toArray();
		//@formatter:on

		List<CfaDataMapping> mappings = new ArrayList<>();
		int rangeStart = 0;
		for (int item : endBits) {
			// for each range 'rangeStart..rangeEnd'
			int rangeEnd = item;

			// Fetch elements in this range.
			MemoryArea definedElem = findSingleElementAtPosition(definedMA, rangeStart, rangeEnd);
			MemoryArea definingElem = findSingleElementAtPosition(definingMA, rangeStart, rangeEnd);
			mappings.addAll(representMapping(origin, definedElem, definingElem, rangeStart, rangeEnd));

			// preparation for the next range
			rangeStart = rangeEnd + 1;
		}

		return mappings;
	}

	/**
	 * Finds the single memory area in the given list that occupies the range
	 * {@code rangeStart}..{@code rangeEnd}.
	 *
	 * @throws IllegalArgumentException
	 *             if no such memory area is found, or if multiple such memory
	 *             areas are found.
	 */
	private static MemoryArea findSingleElementAtPosition(List<MemoryArea> memoryAreas, int rangeStart, int rangeEnd) {
		List<MemoryArea> candidates = memoryAreas.stream()
				.filter(it -> it.getStartBit() <= rangeStart && it.getEndBit() >= rangeEnd)
				.collect(Collectors.toList());
		if (candidates.isEmpty()) {
			throw new IllegalArgumentException("No memory area found in the given range.");
		} else if (candidates.size() > 1) {
			throw new IllegalArgumentException("Multiple memory areas found in the given range: "
					+ candidates.stream().map(it -> it.toString()).collect(Collectors.joining(", ")));
		} else {
			return candidates.get(0);
		}
	}

	/**
	 * Creates CFA data mappings to represent the definition of a slice of the
	 * memory area {@code definedElem} based on a slice of the memory area.
	 * <p>
	 * The {@code rangeStart} and {@code rangeEnd} parameters determine which
	 * slices are to be mapped (using global bit indexes, like the ones returned
	 * by {@link MemoryArea#getStartBit()} and {@link MemoryArea#getEndBit()}).
	 * The ranges should be within the two memory areas.
	 *
	 * @param origin
	 *            AST variable view to be represented
	 * @param definedElem
	 *            Memory area of the defined element
	 * @param definingElem
	 *            Memory area of the defining element
	 * @param rangeStart
	 *            Start bit of slice (global bit index)
	 * @param rangeEnd
	 *            End bit of slice (global bit index)
	 * @return CFA data mappings to represent the expressed mappings.
	 */
	private static Collection<CfaDataMapping> representMapping(AstVarView origin, MemoryArea definedElem,
			MemoryArea definingElem, int rangeStart, int rangeEnd) {
		Slice definedSlice = new Slice(definedElem.getDataRef(), (rangeStart - definedElem.getStartBit()),
				(rangeEnd - definedElem.getStartBit()));
		Slice definingSlice = new Slice(definingElem.getDataRef(), (rangeStart - definingElem.getStartBit()),
				(rangeEnd - definingElem.getStartBit()));

		return CfaDataMapping.create(origin, definedSlice, definingSlice);
	}

	/**
	 * Creates a memory representation for the given list of elementary fields.
	 * <p>
	 * It is assumed that the given elements follow each other consecutively, in
	 * the given order, without any gap.
	 */
	private static List<MemoryArea> elementsToMemoryAreas(List<DataRef> elements) {
		int nextAreaStartBit = 0;
		List<MemoryArea> memoryAreas = new ArrayList<>();
		for (DataRef it : elements) {
			MemoryArea ma = new MemoryArea(it, nextAreaStartBit);
			memoryAreas.add(ma);
			nextAreaStartBit = ma.getEndBit() + 1;
		}
		return memoryAreas;
	}
}
