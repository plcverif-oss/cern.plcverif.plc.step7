/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.cfa.transformation.varviews.datamapping;

import static cern.plcverif.plc.step7.cfa.util.SiemensWordIndexingUtils.wordBitSelection;

import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.IntType;
import cern.plcverif.plc.step7.cfa.transformation.varviews.AstVarView;

import java.util.Collection;
import java.util.Collections;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * Data mapping that defines a one-bit-long slice of an elementary field, based
 * on some (Boolean) expression.
 */
// definedData[definedDataBit] <- definingData (BoolType)
public abstract class BitDefinedByBoolExpr extends CfaDataMapping {
	private DataRef definedData;
	private int definedDataBit;

	public BitDefinedByBoolExpr(AstVarView origin, DataRef definedData, int definedDataBit) {
		super(origin);
		this.definedData = definedData;
		this.definedDataBit = definedDataBit;

		checkCanBeSliced(definedData);
	}

	public abstract Expression getDefiningBoolExpr();

	public DataRef getDefinedData() {
		return definedData;
	}

	/**
	 * Principle of representation: {@code
	 * IF definingData THEN
	 *   definedData := definedData OR 0..010..0;
	 * ELSE
	 *   definedData := definedData AND 1..101..1;
	 * END_IF;
	 * }
	 */
	@Override
	public Collection<DataRef> represent(Location sourceLoc, Location targetLoc) {
		// 'then' branch (when 'definingData' is TRUE)
		AssignmentTransition thenTr = factory.createAssignmentTransition(NEW_LOC_NAME,
				(AutomatonDeclaration) sourceLoc.getParentAutomaton(), sourceLoc, targetLoc,
				EcoreUtil.copy(getDefiningBoolExpr()));

		// definedData := definedData OR 00..0100..0
		IntType wordType = (IntType) getDefinedData().getType();
		factory.createAssignment(thenTr, EcoreUtil.copy(getDefinedData()),
				factory.bwOr(EcoreUtil.copy(getDefinedData()), wordBitSelection(false, definedDataBit, wordType)));

		// 'else' branch
		AssignmentTransition elseTr = factory.createAssignmentTransition(NEW_LOC_NAME,
				(AutomatonDeclaration) sourceLoc.getParentAutomaton(), sourceLoc, targetLoc,
				factory.createElseExpression());
		// definedData := definedData AND 11..1011..1
		factory.createAssignment(elseTr, EcoreUtil.copy(getDefinedData()),
				factory.bwAnd(EcoreUtil.copy(getDefinedData()), wordBitSelection(true, definedDataBit, wordType)));

		return Collections.singleton(definedData);
	}

	@Override
	public boolean isInScopeOf(AutomatonDeclaration automaton) {
		return isDeclaredInAutomatonLocalStructure(CfaDeclarationUtils.getReferredField(definedData), automaton);
	}

}
