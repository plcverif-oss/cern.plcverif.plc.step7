/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.cfa.transformation.varviews.datamapping;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.expr.ElementaryType;
import cern.plcverif.base.models.expr.utils.TypeUtils;

/**
 * Represents a slice of a data element.
 *
 * A slice is a certain part (or the whole) of a field referred by
 * {@code dataRef}. The slice starts from bit {@code startBit} to bit
 * {@code endBit} of {@code dataRef}.
 *
 */
public class Slice {
	private DataRef dataRef;
	private int startBit;
	private int endBit;

	/**
	 * Bit length of the {@code dataRef}. This is NOT necessarily the length of
	 * the current slice!
	 */
	private int dataLength;

	public Slice(DataRef dataRef, int startBit, int endBit) {
		this.dataRef = checkNotNull(dataRef);
		checkNotNull(dataRef.getType());
		checkArgument(dataRef.getType() instanceof ElementaryType);
		this.dataLength = TypeUtils.getBitLength((ElementaryType) dataRef.getType());
		checkArgument(dataLength >= 1, "Data length must be at least 1.");

		checkArgument(startBit >= 0, "startBit must not be negative.");
		this.startBit = startBit;

		checkArgument(endBit >= startBit, "endBit cannot be less than startBit.");
		checkArgument(endBit < this.dataLength, "endBit cannot be longer the the data length.");
		this.endBit = endBit;
	}

	/**
	 * Returns the base data element of the represented slice.
	 *
	 * @return The base data element. This object represents a slice of this
	 *         data element, starting at {@link #getStartBit()} and ending at
	 *         {@link #getEndBit()}.
	 */
	public DataRef getDataRef() {
		return dataRef;
	}

	/**
	 * Returns the start bit of the represented slice.
	 *
	 * @return The start bit. This object represents a slice of the data element
	 *         {@link #getStartBit()}, starting at {@link #getStartBit()} and
	 *         ending at {@link #getEndBit()}.
	 */
	public int getStartBit() {
		return startBit;
	}

	/**
	 * Returns the end bit of the represented slice.
	 *
	 * @return The end bit. This object represents a slice of the data element
	 *         {@link #getStartBit()}, starting at {@link #getStartBit()} and
	 *         ending at {@link #getEndBit()}.
	 */
	public int getEndBit() {
		return endBit;
	}

	/**
	 * Returns true, iff the given slice is a complete field, i.e., the slice
	 * starts at the 0th bit and ends at the last bit of the
	 * {@link #getDataRef()}.
	 */
	public boolean isCompleteField() {
		return (startBit == 0) && (endBit == dataLength - 1);
	}

	/**
	 * Determines whether the given slice is a one bit long.
	 *
	 * @return True iff the slice is 1 bit long.
	 */
	public boolean isBooleanSlice() {
		return (getSliceLength() == 1);
	}

	/**
	 * Returns the length of the defined slice, i.e. the length between
	 * {@code startBit} and {@code endBit}.
	 *
	 * @return Length of the slice (in bits).
	 */
	public int getSliceLength() {
		return endBit - startBit + 1;
	}
}
