/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.cfa.transformation.varviews.datamapping;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.IntType;
import cern.plcverif.base.models.expr.utils.TypeUtils;
import cern.plcverif.plc.step7.cfa.transformation.varviews.AstVarView;
import cern.plcverif.plc.step7.cfa.util.SiemensWordIndexingUtils;

/**
 * Data mapping that defines a (whole) Boolean elementary field, based on a
 * given bit of an elementary field.
 */
public class BoolFieldDefinedByBit extends FieldDefinedByExpression {
	private DataRef definingData;
	private int definingDataBit;

	public BoolFieldDefinedByBit(AstVarView origin, DataRef definedData, DataRef definingData, int definingDataBit) {
		super(origin, definedData);

		Preconditions.checkArgument(TypeUtils.hasBoolType(definedData));
		checkCanBeSliced(definingData);
		this.definingData = definingData;

		this.definingDataBit = definingDataBit;
	}

	@Override
	public Expression getDefiningExpression() {
		// neq(bwAnd('definingData', 2#00..0100..0), 0)
		IntType wordType = (IntType) definingData.getType();
		return factory.neq(
				factory.bwAnd(EcoreUtil.copy(definingData),
						SiemensWordIndexingUtils.wordBitSelection(false, definingDataBit, wordType)),
				factory.createIntLiteral(0L, EcoreUtil.copy(wordType)));
	}

	@Override
	public boolean isInvalidatedBy(DataRef modifiedData) {
		return InvalidationHelper.isChangedBy(definingData, modifiedData);
	}
}
