/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.cfa.transformation.varviews.datamapping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EObjectSet;
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.expr.IntType;
import cern.plcverif.base.models.expr.utils.TypeUtils;
import cern.plcverif.plc.step7.cfa.transformation.varviews.AstVarView;

/**
 * Generic class to represent data mappings in one direction. The defined data
 * can be a whole elementary field or a part of a field.
 */
public abstract class CfaDataMapping {
	protected CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;
	protected static final String NEW_LOC_NAME = "varview_refresh";

	/**
	 * The AST variable mapping that is being (partially) represented by this
	 * CFA data mapping.
	 */
	private AstVarView origin;

	/**
	 * Creates a new CFA data mapping that (partially) represents the given AST
	 * variable mapping.
	 *
	 * @param origin
	 *            Origin of this data mapping
	 */
	public CfaDataMapping(AstVarView origin) {
		this.origin = origin;
	}

	/**
	 * Generates locations and transitions which are necessary to re-establish
	 * the mapping to the data element that is defined by this object. The
	 * representation shall be placed between the locations {@code sourceLoc}
	 * and {@code targetLoc}.
	 *
	 * @param sourceLoc
	 *            The source location for the representation.
	 * @param targetLoc
	 *            The target location for the representation.
	 * @return The collection of modified data references. (If a given data
	 *         reference was modified multiple times, it is enough to return
	 *         once.)
	 */
	public abstract Collection<DataRef> represent(Location sourceLoc, Location targetLoc);

	/**
	 * Returns true iff the modification of {@code modifiedData} necessitates
	 * refreshing this mapping. I.e., returns true iff the modification of
	 * {@code modifiedData} changes the value of the defining expression used to
	 * define the defined variable.
	 *
	 * @param modifiedData
	 *            The modified data.
	 * @return True, iff modification of {@code modifiedData} makes refreshing
	 *         this mapping necessary.
	 *
	 * @see InvalidationHelper#isChangedBy(DataRef, DataRef)
	 */
	public abstract boolean isInvalidatedBy(DataRef modifiedData);

	/**
	 * Returns true iff the execution of the variable assignment
	 * {@code assignment} necessitates refreshing this mapping. I.e., returns
	 * true iff the execution of the variable assignment {@code assignment}
	 * changes the value of the defining expression used to define the defined
	 * variable.
	 *
	 * @param assignment
	 *            Variable assignment to be executed.
	 * @return True, iff execution of {@code assignment} makes refreshing this
	 *         mapping necessary.
	 * @see CfaDataMapping#isInvalidatedBy(DataRef)
	 */
	public final boolean isInvalidatedBy(VariableAssignment assignment) {
		return this.isInvalidatedBy(assignment.getLeftValue());
	}

	/**
	 * Returns true iff the modification of any data in {@code modifiedData}
	 * necessitates refreshing this mapping. I.e., returns true iff the
	 * modification of {@code modifiedData} changes the value of the defining
	 * expression used to define the defined variable.
	 *
	 * @param modifiedData
	 *            The collection of modified data.
	 * @return True, iff modification of {@code modifiedData} makes refreshing
	 *         this mapping necessary.
	 *
	 * @see InvalidationHelper#isChangedBy(DataRef, DataRef)
	 */
	public final boolean isInvalidatedByAny(Collection<DataRef> modifiedData) {
		for (DataRef dataRef : modifiedData) {
			if (this.isInvalidatedBy(dataRef)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Returns true if the defined variable is in the scope of the given
	 * automaton, i.e., this data mapping has to be enforced at the beginning of
	 * the given automaton to initialize the defined variables.
	 *
	 * @param automaton
	 *            Automaton.
	 * @return True, iff it is in the scope.
	 */
	public abstract boolean isInScopeOf(AutomatonDeclaration automaton);

	/**
	 * Returns the AST variable view that is (partially) represented by this
	 * mapping.
	 *
	 * @return Origin AST variable view.
	 */
	public final AstVarView getOrigin() {
		return origin;
	}

	/**
	 * Creates CFA data mapping(s) to represent the definition of slice
	 * {@code definedSlice} based on the slice {@definingSlice}.
	 *
	 * @param origin
	 *            AST variable view originating the CFA data mapping.
	 * @param definedSlice
	 *            Slice being defined.
	 * @param definingSlice
	 *            Slice used for the definition of {@code definedSlice}.
	 * @return The CFA data mapping(s)
	 */
	public static Collection<CfaDataMapping> create(AstVarView origin, Slice definedSlice, Slice definingSlice) {
		Preconditions.checkArgument(definedSlice.getSliceLength() == definingSlice.getSliceLength(),
				"It is not possible to create mapping between two slices with different lengths.");

		if (definedSlice.isCompleteField()) {
			// The 'definedData' is a complete variable, not a slice of a
			// variable.

			DataRef definedData = definedSlice.getDataRef();
			if (definingSlice.isCompleteField()) {
				// VarDefinedByVar
				return Collections
						.singletonList(new FieldDefinedByField(origin, definedData, definingSlice.getDataRef()));
			} else {
				// VarDefinedBySlice
				if (definedSlice.isBooleanSlice() && definingSlice.isBooleanSlice()) {
					// Special case: BoolVarDefinedByBit
					return Collections.singletonList(new BoolFieldDefinedByBit(origin, definedData,
							definingSlice.getDataRef(), definingSlice.getStartBit()));
				} else {
					// fallback to 'BitDefinedByBit's, one for each bit
					return createBitByBitMapping(origin, definedSlice, definingSlice);
				}
			}
		} else {
			// The 'definedData' is NOT a complete variable,
			// but a slice of a variable.

			if (definedSlice.isBooleanSlice() && definingSlice.isBooleanSlice() && definingSlice.isCompleteField()) {
				// Special case: BitDefinedByBoolVar
				return Collections.singletonList(new BitDefinedByBoolField(origin, definedSlice.getDataRef(),
						definedSlice.getStartBit(), definingSlice.getDataRef()));
			}

			// General case: fallback to 'BitDefinedByBit's, one for each bit
			return createBitByBitMapping(origin, definedSlice, definingSlice);
		}
	}

	/**
	 * Represents the refreshment of all given mapping between the given
	 * locations.
	 *
	 * @param sourceLoc
	 *            The location where the representation starts.
	 * @param targetLoc
	 *            The location where the representation ends.
	 * @param mappingsToRepresent
	 *            The mappings whose refreshments are to be represented.
	 * @return References to data elements that are modified by the
	 *         refreshments.
	 */
	public static Collection<DataRef> representMany(Location sourceLoc, Location targetLoc,
			List<CfaDataMapping> mappingsToRepresent) {
		return representManyInvalidated(null, sourceLoc, targetLoc, mappingsToRepresent);
	}

	/**
	 * Represents the refreshment of all given mapping between the given
	 * locations which are invalidated by the modification of the data elements
	 * in {@code assignedRefs}.
	 *
	 * @param assignedRefs
	 *            The data elements that were modified.
	 * @param sourceLoc
	 *            The location where the representation starts.
	 * @param targetLoc
	 *            The location where the representation ends.
	 * @param mappingsToRepresent
	 *            The mappings whose refreshments are to be represented. Only
	 *            those mappings will be represented which are invalidated by
	 *            the modification of the {@code assignedRefs}.
	 * @return References to data elements that are modified by the
	 *         refreshments.
	 */
	public static Collection<DataRef> representManyInvalidated(Collection<DataRef> assignedRefs, Location sourceLoc,
			Location targetLoc, List<CfaDataMapping> mappingsToRepresent) {
		Location currentSource = sourceLoc;

		// Exclude the non-invalidated mappings
		List<CfaDataMapping> mappings;
		if (assignedRefs == null) {
			// assigned refs not known, everything to be refreshed
			mappings = mappingsToRepresent;
		} else {
			mappings = mappingsToRepresent.stream().filter(it -> it.isInvalidatedByAny(assignedRefs))
					.collect(Collectors.toList());
			Preconditions.checkNotNull(mappings);
		}

		EObjectSet<DataRef> modifiedDataRefs = new EObjectSet<>();
		for (int i = 0; i < mappings.size(); i++) {
			CfaDataMapping mappingToRepresent = mappings.get(i);
			Location target = (i == mappings.size() - 1) ? targetLoc
					: CfaDeclarationSafeFactory.INSTANCE.createLocation(NEW_LOC_NAME, sourceLoc.getParentAutomaton());
			modifiedDataRefs.addAll(mappingToRepresent.represent(currentSource, target));
			currentSource = target;
		}

		return modifiedDataRefs;
	}

	protected boolean isDeclaredInAutomatonLocalStructure(Field field, AutomatonDeclaration automaton) {
		// FIXME This is not necessarily correct!
		return isDeclaredInAutomatonLocalStructure(field.getEnclosingType(), automaton);
	}

	private static boolean isDeclaredInAutomatonLocalStructure(DataStructure dataStructure, AutomatonDeclaration automaton) {
		if (dataStructure == null) {
			return false;
		}
		if (dataStructure == automaton.getLocalDataStructure().getDefinition()) {
			return true;
		}

		return isDeclaredInAutomatonLocalStructure(dataStructure.getEnclosingType(), automaton);
	}

	/**
	 * Helper method to create bit-by-bit mapping from {@code definingSlice} to
	 * {@code definedSlice}.
	 *
	 * @param origin
	 *            AST variable view to be represented
	 * @param definedSlice
	 *            Defined slice
	 * @param definingSlice
	 *            Defining slice
	 * @return CFA data mappings representing the bit-by-bit mapping.
	 */
	private static Collection<CfaDataMapping> createBitByBitMapping(AstVarView origin, Slice definedSlice,
			Slice definingSlice) {
		Preconditions.checkArgument(definedSlice.getSliceLength() == definingSlice.getSliceLength(),
				"It is not possible to create mapping between two slices with different lengths.");

		List<CfaDataMapping> ret = new ArrayList<>();

		int definingIdx = definingSlice.getStartBit();
		for (int definedIdx = definedSlice.getStartBit(); definedIdx <= definedSlice.getEndBit(); definedIdx++) {
			ret.add(new BitDefinedByBit(origin, definedSlice.getDataRef(), definedIdx, definingSlice.getDataRef(),
					definingIdx));
			definingIdx++;
		}

		return ret;
	}

	/**
	 * Returns true iff the given data element can be sliced, i.e., a given bit
	 * of it can be indexed.
	 * <p>
	 * Practically, it returns true iff the given data reference has type
	 * {@link IntType}, as only integers can be used in the bitwise CFA
	 * operations.
	 */
	protected final void checkCanBeSliced(DataRef dataRef) {
		Preconditions.checkArgument(TypeUtils.hasIntType(dataRef),
				String.format("Only IntType fields can be sliced. '%s' has type '%s'.",
						CfaToString.toDiagString(dataRef), CfaToString.toDiagString(dataRef.getType())));
	}
}
