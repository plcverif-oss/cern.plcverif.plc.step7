/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.cfa.transformation.varviews.datamapping;

import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.plc.step7.cfa.transformation.varviews.AstVarView;

/**
 * Data mapping that defines a whole elementary field, based on another whole
 * elementary field.
 */
public class FieldDefinedByField extends FieldDefinedByExpression {
	private DataRef definingData;
	private Expression definingDataWithTypeConversion;

	public FieldDefinedByField(AstVarView origin, DataRef definedData, DataRef definingData) {
		super(origin, definedData);
		// Type conversion may be needed!
		this.definingData = definingData;
		this.definingDataWithTypeConversion = CfaDeclarationSafeFactory.INSTANCE.createTypeConversionIfNeeded(definingData, definedData.getType());
	}

	@Override
	public Expression getDefiningExpression() {
		// The constructor takes care of the type conversion
		return definingDataWithTypeConversion;
	}

	@Override
	public boolean isInvalidatedBy(DataRef modifiedData) {
		return InvalidationHelper.isChangedBy(definingData, modifiedData);
	}
}
