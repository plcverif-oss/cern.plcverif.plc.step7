/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.cfa.transformation.varviews.datamapping;

import org.eclipse.emf.ecore.util.EcoreUtil;

import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.IntType;
import cern.plcverif.plc.step7.cfa.transformation.varviews.AstVarView;
import cern.plcverif.plc.step7.cfa.util.SiemensWordIndexingUtils;

/**
 * Data mapping that defines a one-bit-long slice of an elementary field, based
 * on a given bit of an elementary field.
 */
public class BitDefinedByBit extends BitDefinedByBoolExpr {
	private DataRef definingData;
	private int definingDataBit;

	public BitDefinedByBit(AstVarView origin, DataRef definedData, int definedDataBit, DataRef definingData,
			int definingDataBit) {
		super(origin, definedData, definedDataBit);

		this.definingData = definingData;
		this.definingDataBit = definingDataBit;

		checkCanBeSliced(definedData);
	}

	@Override
	public boolean isInvalidatedBy(DataRef modifiedData) {
		return InvalidationHelper.isChangedBy(definingData, modifiedData);
	}

	@Override
	public Expression getDefiningBoolExpr() {
		// neq(bwAnd('definingData', 2#00..0100..0), 0)
		IntType wordType = (IntType) definingData.getType();
		return factory.neq(
				factory.bwAnd(EcoreUtil.copy(definingData),
						SiemensWordIndexingUtils.wordBitSelection(false, definingDataBit, wordType)),
				factory.createIntLiteral(0L, EcoreUtil.copy(wordType)));
	}
}
