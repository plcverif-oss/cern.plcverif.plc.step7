/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.transformation.varviews.datamapping

import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.Indexing
import java.util.List
import org.eclipse.emf.ecore.util.EcoreUtil

import static cern.plcverif.base.models.expr.utils.ExprUtils.isComputableConstant
import static cern.plcverif.base.models.expr.utils.ExprUtils.toLong

final class InvalidationHelper {
	private new() {
	}

	/**
	 * Returns true iff the value {@code data} could have changed following the modification of
	 * {@code modifiedData}.
	 * <p>
	 * For example, if struct1.member1[0] was modified, then struct1 or struct1.member1 changes,
	 * however, struct1.member1[5] or struct1.member2 not.
	 */
	static def boolean isChangedBy(DataRef data, DataRef modifiedData) {
		val result = isInvalidatedByInternal(data, modifiedData);
//		if (result) {
//			println(''''«CfaToString.toDiagString(data)»' was invalidated by writing '«CfaToString.toDiagString(modifiedData)»'.''')
//		}

		return result;
	}

	private static def boolean isInvalidatedByInternal(DataRef data, DataRef modifiedData) {
		val dataList = hierarchicalDataRefToList(data);
		val modifiedDataList = hierarchicalDataRefToList(modifiedData);

		for (int i : 0 .. (modifiedDataList.size - 1)) {
			if (dataList.size <= i) {
				// The dataList is shorter than modifiedDataList.
				// If there was no difference so far, the 'modifiedData' 
				// is a part of 'data', thus shall be invalidated.
				return true;
			}

			val dataItem = dataList.get(i);
			val modifiedItem = modifiedDataList.get(i);

			if (dataItem instanceof Indexing && modifiedItem instanceof Indexing) {
				// If at least one of them is not a constant, then we cannot be
				// sure whether they are different. It is safer to consider them equal.
				if (isComputableConstant((dataItem as Indexing).index) &&
					isComputableConstant((modifiedItem as Indexing).index)) {
					// compute the indices
					val long idx1 = toLong((dataItem as Indexing).index);
					val long idx2 = toLong((modifiedItem as Indexing).index);

					// If they are different, we found a difference that makes the invalidation unnecessary.
					if (idx1 != idx2) {
						return false;
					}
				}
			// we cannot be sure that the two indices are different; continue
			} else if (EcoreUtil.equals(dataItem, modifiedItem) == false) {
				// Now we know that the compared items are not Indexings.
				// There is a difference, thus they are not in conflict.
				// E.g. struct1.member[0] and struct1.member[1], OR
				// struct1.memberA and struct1.memberB.
				return false;
			}
		}

		// 'modifiedData' is a prefix of 'data', thus the modification invalidates 'data'.
		return true;
	}

	private static def List<DataRef> hierarchicalDataRefToList(DataRef dataRef) {
		val List<DataRef> ret = newArrayList();

		var current = dataRef;
		while (current !== null) {
			ret.add(current);
			current = current.prefix;
		}

		return ret.reverse();
	}
}
