/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.transformation

import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.CallTransition
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment
import cern.plcverif.plc.step7.cfa.trace.AstCfaVarTrace
import cern.plcverif.plc.step7.cfa.transformation.varviews.AstVarView
import cern.plcverif.plc.step7.cfa.transformation.varviews.datamapping.CfaDataMapping
import java.util.ArrayList
import java.util.Collection
import java.util.List
import java.util.stream.Collectors
import cern.plcverif.plc.step7.cfa.transformation.varviews.CfaDataMappingBuilder
import cern.plcverif.plc.step7.cfa.transformation.varviews.CfaDataMappingBuilder.CfaDataBiMappings
import cern.plcverif.base.common.logging.IPlcverifLogger

/**
 * Class to represent AST variable views in the CFA.
 */
class RepresentVariableViews {
	CfaNetworkDeclaration cfaNetwork;
	AstCfaVarTrace varTrace;

	/**
	 * Top-level variable views in the AST.
	 */
	List<AstVarView> varViews;

	/**
	 * CFD data mappings, representing the AST variable views.
	 */
	CfaDataBiMappings cfaDataMappings;

	new(CfaNetworkDeclaration cfaNetwork, AstCfaVarTrace varTrace, List<AstVarView> varViews, IPlcverifLogger logger) {
		this.cfaNetwork = cfaNetwork;
		this.varTrace = varTrace;
		this.varViews = varViews;

		this.cfaDataMappings = CfaDataMappingBuilder.build(varViews, varTrace, logger);
	}

	/**
	 * Adds the necessary assignments to each automaton of the CFD
	 * to represent the defined variable views. 
	 */
	def void execute() {
		for (automaton : cfaNetwork.automata) {
			execute(automaton);
		}
	}

	private def void execute(AutomatonDeclaration automaton) {
		val transitionsToCheck = new ArrayList(automaton.transitions);

		// (1) Re-establish mappings at the beginning of the automaton
		val varViewToInit = newArrayList();
		for (vv : cfaDataMappings.getViewersDefinedByStorages) {
			if (vv.isInScopeOf(automaton)) {
				// in the local scope
				varViewToInit.add(vv);
			}
		}
		if (!varViewToInit.isEmpty) {
			// create new location
			val newLoc = CfaDeclarationSafeFactory.INSTANCE.createLocation("x", automaton);

			// This will represent the view refreshments:
			// WAS: initLoc ==>
			// NEW: initLoc --> (refreshments) --> newLoc ==>
			newLoc.outgoing.addAll(automaton.initialLocation.outgoing);

			CfaDataMapping.representMany(automaton.initialLocation, newLoc, varViewToInit);
		}

		// (2) After each modification, re-establish the mappings which are needed to
		// (this does not include the newly created transitions)
		for (transition : transitionsToCheck) {
			checkAndReestablish(transition);
		}
	}

	private def dispatch checkAndReestablish(Transition transition) {
		throw new UnsupportedOperationException("Unhandled transition type: " + transition.name);
	}

	private def dispatch checkAndReestablish(CallTransition transition) {
		val List<VariableAssignment> outputAmts = newArrayList();
		for (call : transition.calls) {
			outputAmts.addAll(call.outputAssignments);
		}
		checkAndReestablish(transition, outputAmts);
	}

	private def dispatch checkAndReestablish(AssignmentTransition transition) {
		checkAndReestablish(transition, transition.assignments)
	}

	private def void checkAndReestablish(Transition transition, Collection<VariableAssignment> assignments) {
		var invalidated = false;
		for (mapping : cfaDataMappings.all) {
			for (amt : assignments) {
				invalidated = invalidated || mapping.isInvalidatedBy(amt);
			}
		}
		if (invalidated) {
			// create new location
			val newLoc = CfaDeclarationSafeFactory.INSTANCE.createLocation("x", transition.parentAutomaton);

			// This will represent the view refreshments:
			// WAS: transition.source ---> transition.target==oldTarget ==>
			// NEW: transition.source ---> newLoc  (refreshments) ---> oldTarget ==>
			val oldTarget = transition.target;
			transition.target = newLoc;

			val writtenByRefreshments = CfaDataMapping.representManyInvalidated(assignments.map[it|it.leftValue].toList,
				newLoc, oldTarget, cfaDataMappings.getAll());

			// It may happen that due to the change of viewer1, storage was updated here.
			// However, if the storage had other viewers, they have to be updated too.
			// First, let's do a cheaper check.
			val anyViewerMayNeedRefreshment = cfaDataMappings.getViewersDefinedByStorages.stream.anyMatch(
				[it|it.isInvalidatedByAny(writtenByRefreshments)]);
			if (anyViewerMayNeedRefreshment) {
				// Check which variable views were previously refreshed. They do not have to be repeated.
				val previouslyRefreshedVarViews = cfaDataMappings.all.stream.filter([ it |
					it.isInvalidatedByAny(assignments.map[it|it.leftValue].toList)
				]).map[it|it.origin].collect(Collectors.toSet);
				val anyViewerNeedsRefreshment = cfaDataMappings.getViewersDefinedByStorages.stream.filter([ it |
					!previouslyRefreshedVarViews.contains(it.origin)
				]).anyMatch([it|it.isInvalidatedByAny(writtenByRefreshments)]);

				if (anyViewerNeedsRefreshment) {
					// We need to inject more refreshments at 'oldTarget'.
					// These refreshments will be between 'oldTarget' and 'loc'.
					val loc = CfaDeclarationSafeFactory.INSTANCE.createLocation("reverseRefresh_end",
						transition.parentAutomaton);
					loc.outgoing.addAll(oldTarget.outgoing);

					CfaDataMapping.representManyInvalidated(writtenByRefreshments, oldTarget, loc,
						newArrayList(cfaDataMappings.getViewersDefinedByStorages().filter([ it |
							!previouslyRefreshedVarViews.contains(it.origin)
						])));
				}
			}
		}
	}
}
