/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.transformation.varviews;

import static cern.plcverif.plc.step7.util.Step7LanguageHelper.getHierarchicalName;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;

import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit;
import cern.plcverif.plc.step7.step7Language.ProgramFile;
import cern.plcverif.plc.step7.step7Language.ProgramUnit;
import cern.plcverif.plc.step7.step7Language.Variable;
import cern.plcverif.plc.step7.step7Language.VariableDeclarationBlock;
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine;
import cern.plcverif.plc.step7.util.VariableViewUtil;

/**
 * Describes a variable view ({@code viewer AT storage}) on the AST level.
 */
public class AstVarView {
	private Variable viewer;
	private Variable storage;

	public AstVarView(Variable viewer, Variable storage) {
		this.viewer = viewer;
		this.storage = storage;
	}

	@Override
	public String toString() {
		return String.format("%s ~AT~> %s", getHierarchicalName(viewer), getHierarchicalName(storage));
	}

	public Variable getViewer() {
		return viewer;
	}

	public Variable getStorage() {
		return storage;
	}

	/**
	 * Creates an {@link AstVarView} object representing the variable view
	 * defined in the given variable declaration line.
	 *
	 * @param varDeclLine
	 *            Variable declaration line. It is expected to contain a
	 *            variable view ('AT').
	 * @return The {@link AstVarView} object representing the variable view in
	 *         the given declaration line.
	 *
	 * @throws IllegalArgumentException
	 *             if the given variable declaration line does not describe a
	 *             variable view.
	 */
	public static AstVarView createView(VariableDeclarationLine varDeclLine) {
		Preconditions.checkArgument(varDeclLine.getVariables().size() == 1);
		Variable viewerVariable = varDeclLine.getVariables().get(0);

		Preconditions.checkArgument(viewerVariable.isReference());
		Preconditions.checkNotNull(viewerVariable.getRef());
		Variable storageVariable = viewerVariable.getRef();

		Preconditions.checkState(VariableViewUtil.areCompatible(viewerVariable, storageVariable));
		return new AstVarView(viewerVariable, storageVariable);
	}

	// Collection methods
	/**
	 * Collects and returns all variable views in the given program files.
	 */
	public static List<AstVarView> collect(Iterable<ProgramFile> files) {
		List<AstVarView> varViews = new ArrayList<>();

		for (ProgramFile file : files) {
			collect(file, varViews);
		}
		return varViews;
	}

	/**
	 * Collects and returns all variable views in the given program file.
	 */
	private static void collect(ProgramFile file, List<AstVarView> varViews) {
		for (ProgramUnit unit : file.getProgramUnits()) {
			if (unit instanceof ExecutableProgramUnit) {
				for (VariableDeclarationBlock vdBlock : ((ExecutableProgramUnit) unit).getDeclarationSection()
						.getVariableDeclarations()) {
					for (VariableDeclarationLine vdl : vdBlock.getVariables()) {
						if (vdl.getVariables().size() == 1 && vdl.getVariables().get(0).isReference()) {
							varViews.add(AstVarView.createView(vdl));
						}
					}

				}
			}
		}
	}
}
