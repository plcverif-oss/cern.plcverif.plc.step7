/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.trace

import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.plc.step7.datatypes.S7MemoryAddress
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.Variable
import com.google.common.collect.BiMap
import com.google.common.collect.HashBiMap
import java.util.Map
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit
import com.google.common.base.Preconditions
import cern.plcverif.plc.step7.datatypes.S7AddressMemoryIdentifier
import java.util.Objects
import cern.plcverif.plc.step7.cfa.trace.AstCfaVarTrace.LocalAddress

class AstCfaVarTrace {
	/**
	 * Internal name used for the variable representing the return value of non-void functions.
	 */
	public static final String RETVAL_VARIABLE_NAME = "RET_VAL";
	
	/**
	 * Represents a local memory address in a given executable program unit.
	 */
	private static final class LocalAddress {
		S7MemoryAddress memoryAddress;
		ExecutableProgramUnit programUnit;
		
		new(ExecutableProgramUnit programUnit, S7MemoryAddress memoryAddress) {
			Preconditions.checkNotNull(programUnit);
			Preconditions.checkNotNull(memoryAddress);
			Preconditions.checkArgument(memoryAddress.memoryIdentifier == S7AddressMemoryIdentifier.L, "Local address shall be in the L area.");
			
			this.memoryAddress = memoryAddress;
			this.programUnit = programUnit;
		}
		
		def getMemoryAddress() {
			return memoryAddress;
		}
		
		def getProgramUnit() {
			return programUnit;
		}
		
		override hashCode() {
			return Objects.hash(programUnit, memoryAddress);
		}
		
		override equals(Object other) {
			if (other instanceof LocalAddress) {
				return memoryAddress.equals(other.memoryAddress) && programUnit.equals(other.programUnit);
			}
			
			return false; 
		}
	} 

	Map<Variable, Field> variableFieldMapping = newHashMap();
	Map<Function, Field> retvalFieldMapping = newHashMap();
	BiMap<S7MemoryAddress, Field> globalAddressFieldMapping = HashBiMap.create();
	BiMap<LocalAddress, Field> localAddressFieldMapping = HashBiMap.create();

	def add(Variable v, Field f) {
		variableFieldMapping.put(v, f);
	}

	def Field get(Variable v) {
		val result = variableFieldMapping.get(v); 
		if (result === null) {
			throw new IllegalStateException("No field has been found for the variable " + v);
		}
		return result; 
	}
	
	def boolean containsVariableToField(Variable v) {
		return variableFieldMapping.containsKey(v);
	}

	def addRetval(Function function, Field field) {
		retvalFieldMapping.put(function, field);
	}

	def Field getRetval(Function function) {
		return retvalFieldMapping.get(function);
	}
	
	def void addGlobalAddress(S7MemoryAddress address, Field field) {
		Preconditions.checkNotNull(address, "address");
		Preconditions.checkArgument(address.memoryIdentifier != S7AddressMemoryIdentifier.L, "Non-local addresses shall not be in the L area.");
		Preconditions.checkNotNull(field, "field");
		
		globalAddressFieldMapping.put(address, field);
	}
	
	def Field getFieldForGlobalAddress(S7MemoryAddress address) {
		val result = globalAddressFieldMapping.get(address);
		if (result === null) {
			throw new IllegalStateException('''Internal error: no field has been found for «address».''');
		}
		return result;
	}
	
	def void addLocalAddress(ExecutableProgramUnit programUnit, S7MemoryAddress address, Field field) {
		Preconditions.checkNotNull(programUnit, "programUnit");
		Preconditions.checkNotNull(address, "address");
		Preconditions.checkArgument(address.memoryIdentifier == S7AddressMemoryIdentifier.L, "Local addresses shall be in the L area.");
		Preconditions.checkNotNull(field, "field");
		
		localAddressFieldMapping.put(new LocalAddress(programUnit, address), field);
	}
	
	def Field getFieldForLocalAddress(ExecutableProgramUnit programUnit, S7MemoryAddress address) {
		Preconditions.checkNotNull(address, "address");
		Preconditions.checkArgument(address.memoryIdentifier == S7AddressMemoryIdentifier.L, "Local addresses shall be in the L area.");
		
		val result = localAddressFieldMapping.get(new LocalAddress(programUnit, address));
		if (result === null) {
			throw new IllegalStateException('''Internal error: no field has been found for «address» in «programUnit.name».''');
		}
		return result;
	}
	
	def Iterable<Variable> getVariables() {
		return variableFieldMapping.keySet().sortBy[it | it.name];
	}
	
	// Replaces all uses of 'f1' with 'f2'.
	def replace(Field field1, Field field2) {
		replaceMapValue(variableFieldMapping, field1, field2);
		replaceMapValue(retvalFieldMapping, field1, field2);
		replaceMapValue(globalAddressFieldMapping, field1, field2);
		replaceMapValue(localAddressFieldMapping, field1, field2);
	}
	
	private def <K,V> void replaceMapValue(Map<K, V> map, V oldValue, V newValue) {
		for (kvp : map.entrySet.filter[value == oldValue].toList) {
			map.remove(kvp.key);
			map.put(kvp.key, newValue);
		}
	}
}
