/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.trace

import cern.plcverif.plc.step7.step7Language.ProgramUnit
import com.google.common.collect.BiMap
import com.google.common.collect.HashBiMap
import java.util.Map
import cern.plcverif.plc.step7.cfa.util.ListMap
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration

class StructuralAstCfaTrace {
	final BiMap<ProgramUnit, DataStructure> programUnitToType = HashBiMap.create();
	final Map<ProgramUnit, Field> singletonProgramUnitToField = newHashMap();
	final Map<ProgramUnit, AutomatonDeclaration> programUnitToAutomaton = newHashMap();
	final Map<Field, AutomatonDeclaration> fieldToAutomaton = new ListMap();
	
	def getProgramUnitToType() {
		return programUnitToType;
	}
	
	def getSingletonProgramUnitToField() {
		return singletonProgramUnitToField;
	}
	
	def getProgramUnitToAutomaton() {
		return programUnitToAutomaton;
	}
	
	def getFieldToAutomaton() {
		return fieldToAutomaton;
	}
	
}
