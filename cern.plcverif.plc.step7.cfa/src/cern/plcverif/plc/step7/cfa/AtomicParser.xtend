/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa

import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils
import cern.plcverif.base.models.expr.AtomicExpression
import cern.plcverif.base.models.expr.ExprFactory
import cern.plcverif.base.models.expr.Type
import cern.plcverif.plc.step7.cfa.impl.Step7TypeToExprType
import cern.plcverif.plc.step7.cfa.trace.AstCfaVarTrace
import cern.plcverif.plc.step7.cfa.trace.StructuralAstCfaTrace
import cern.plcverif.plc.step7.datatypes.S7Bool
import cern.plcverif.plc.step7.datatypes.S7Integer
import cern.plcverif.plc.step7.datatypes.S7IntegerType
import cern.plcverif.plc.step7.datatypes.S7MemoryAddress
import cern.plcverif.plc.step7.datatypes.S7Real
import cern.plcverif.plc.step7.datatypes.S7RealType
import cern.plcverif.plc.step7.datatypes.S7String
import cern.plcverif.plc.step7.datatypes.S7TimePeriod
import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum
import java.util.Optional
import cern.plcverif.base.interfaces.exceptions.AtomParsingException

/**
 * Class that is responsible to parse atomic expressions (i.e. references and literals) from string in the given CFA.
 */
class AtomicParser {
	static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;
	static final ExprFactory UNSAFE_EXPR_FACTORY = ExprFactory.eINSTANCE;

	StructuralAstCfaTrace structuralTrace;
	AstCfaVarTrace varTrace;
	CfaNetworkDeclaration cfa;

	new(StructuralAstCfaTrace structuralTrace, AstCfaVarTrace varTrace, CfaNetworkDeclaration cfa) {
		this.structuralTrace = structuralTrace;
		this.varTrace = varTrace;
		this.cfa = cfa;
	}

	/**
	 * Parses the given atomic expression string into {@link AtomicExpression}.
	 */
	def AtomicExpression parseAtom(String atom) throws AtomParsingException {
		// First try to parse as (hierarchical) named element 
		try {
			return parseAtomAsNamedElement(atom);
		} catch (AtomParsingException e) {
			// If unable, try to parse as literal (e.g. INT#1)
			var Optional<AtomicExpression> result;
			result = tryParseAtomAsLiteral(atom);
			if (result.isPresent()) {
				return result.get();
			}
			throw e;
		} catch (Exception e) {
			throw new AtomParsingException(String.format("Unable to parse the atom '%s'. %s", atom, e.message), e);
		}
	}

	private def Optional<AtomicExpression> tryParseAtomAsLiteral(String atomStr) {
		val isIntLiteral = S7Integer.isValid(atomStr);

		// NOTE This parsing is a lightweight variant only.
		if (!isIntLiteral && S7Bool.isValid(atomStr)) {
			val boolValue = S7Bool.create(atomStr);
			return Optional.of(FACTORY.createBoolLiteral(boolValue.boolValue));
		} else if (!isIntLiteral && S7Real.isValid(atomStr)) {
			val realValue = S7Real.create(atomStr);
			val ret = UNSAFE_EXPR_FACTORY.createFloatLiteral();
			ret.value = realValue.doubleValue;
			if (realValue.type == S7RealType.Unknown) {
				ret.type = FACTORY.createUnknownType();
			} else {
				ret.type = Step7TypeToExprType.s7RealTypeToCfaType(realValue.type);
			}

			return Optional.of(ret);
		} else if (S7String.isValid(atomStr)) {
			throw new UnsupportedOperationException("S7Strings are not supported yet in AtomicParser.");
		} else {
			// We hope that it is a type represented as INT
			var long intValue;
			var Type type = FACTORY.createUnknownType();
			if (S7Integer.isValid(atomStr)) {
				val s7value = S7Integer.create(atomStr)
				intValue = s7value.intValue;
				if (s7value.type !== S7IntegerType.Unknown) {
					type = Step7TypeToExprType.s7IntTypeToCfaType(s7value.type);
				}
			} else if (S7TimePeriod.isValid(atomStr)) {
				val s7value = S7TimePeriod.create(atomStr)
				intValue = s7value.totalMilliseconds;
				type = Step7TypeToExprType.s7typeToExprType(ElementaryTypeEnum.TIME);
			} else {
				// Unable to parse.
				return Optional.empty();
			}

			val ret = UNSAFE_EXPR_FACTORY.createIntLiteral();
			ret.value = intValue;
			ret.type = type;
			return Optional.of(ret);
		}
	}

	/**
	 * Parses the given atom into a {@link AtomicExpression}. 
	 * <p>
	 * It is assumed that the different segments of the given textual reference are separated by {@code .} characters. The different segments shall be the {@code displayName}s of fields.
	 * @param atom The textual reference to be parsed.
	 * @return If the atom was resolved, the result is the resolved {@link AtomicExpression} object. If the resolution is unsuccessful, exception is thrown. Null value is never returned.
	 * @throws AtomParsingException If the given atom cannot be parsed.
	 */
	private def AtomicExpression parseAtomAsNamedElement(String atom) throws AtomParsingException {
		// Try to handle it as S7 address first, as this is not handled by CfaDeclarationUtils.parseHierarchicalDataRef. (E.g., IX0.0)
		if (S7MemoryAddress.isValid(atom)) {
			// canonize S7 IO addresses
			val memoryAddressName = S7MemoryAddress.create(atom).toString();
			val exactFieldMatch = cfa.rootDataStructure.fields.findFirst[it|it.displayName.equalsIgnoreCase(memoryAddressName)];
			if (exactFieldMatch !== null) {
				return FACTORY.createFieldRef(exactFieldMatch);
			} // if not, continue parsing
		}
		
		// If not, S7 address, use the built-in support
		try {
			val DataRef parsedRef = CfaDeclarationUtils.parseHierarchicalDataRef(atom, cfa, [field | field.displayName]);
			return parsedRef;
		}
		catch (IllegalArgumentException ex) {
			// 'parseHierarchicalDataRef' was unable to parse the given text, re-throw exception as PlcverifPlatformException
			throw new AtomParsingException(ex.message);  
		}
	}
}
