/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink - new features and improvements
 *****************************************************************************/

package cern.plcverif.plc.step7.cfa

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.common.logging.IPlcverifLogger
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.cfa.builder.TransitionBuilder
import cern.plcverif.base.models.cfa.cfabase.DataDirection
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructureRef
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils
import cern.plcverif.base.models.cfa.utils.CfaUtils
import cern.plcverif.base.models.expr.ElementaryType
import cern.plcverif.base.models.expr.InitialValue
import cern.plcverif.base.models.expr.Type
import cern.plcverif.plc.step7.cfa.exceptions.PlcCodeToCfaException
import cern.plcverif.plc.step7.cfa.exceptions.PlcCodeToCfaRuntimeException
import cern.plcverif.plc.step7.cfa.impl.LibraryFunctionsToCfa
import cern.plcverif.plc.step7.cfa.impl.ProgramConverter
import cern.plcverif.plc.step7.cfa.impl.Step7ExprToCfaExpr
import cern.plcverif.plc.step7.cfa.impl.Step7TypeToExprType
import cern.plcverif.plc.step7.cfa.impl.TranslationScope
import cern.plcverif.plc.step7.cfa.trace.AstCfaVarTrace
import cern.plcverif.plc.step7.cfa.trace.StructuralAstCfaTrace
import cern.plcverif.plc.step7.cfa.transformation.CreateUniqueContextsForSmallFcs
import cern.plcverif.plc.step7.cfa.transformation.RepresentVariableViews
import cern.plcverif.plc.step7.cfa.transformation.TimeRepresentation
import cern.plcverif.plc.step7.cfa.transformation.varviews.AstVarView
import cern.plcverif.plc.step7.datatypes.S7AddressMemoryIdentifier
import cern.plcverif.plc.step7.step7Language.ArrayDT
import cern.plcverif.plc.step7.step7Language.DataBlock
import cern.plcverif.plc.step7.step7Language.DataType
import cern.plcverif.plc.step7.step7Language.DeclarationSection
import cern.plcverif.plc.step7.step7Language.ElementaryDT
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit
import cern.plcverif.plc.step7.step7Language.FbOrUdtDT
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.FunctionBlock
import cern.plcverif.plc.step7.step7Language.GlobalVariableBlock
import cern.plcverif.plc.step7.step7Language.OrganizationBlock
import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.step7Language.ProgramUnit
import cern.plcverif.plc.step7.step7Language.Step7LanguageFactory
import cern.plcverif.plc.step7.step7Language.StructDT
import cern.plcverif.plc.step7.step7Language.UserDefinedDataType
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine
import cern.plcverif.plc.step7.transformation.ExtractNestedCalls
import cern.plcverif.plc.step7.transformation.IStep7AstTransformation
import cern.plcverif.plc.step7.transformation.RetValToOutputParam
import cern.plcverif.plc.step7.transformation.SpecializeFcsWithAnyNumParam
import cern.plcverif.plc.step7.transformation.SubstituteNamedConstantRefs
import cern.plcverif.plc.step7.typecomputer.Step7TypeComputer
import cern.plcverif.plc.step7.util.DataTypeUtil
import cern.plcverif.plc.step7.util.SclSyntaxHelper
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import com.google.common.base.Preconditions
import java.util.ArrayList
import java.util.List
import java.util.Set
import java.util.stream.Collectors
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.xtend.lib.annotations.Data

import static extension cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils.refersTo
import cern.plcverif.plc.step7.datatypes.S7MemoryAddress
import cern.plcverif.plc.step7.transformation.SubstituteDirectBitAccessRef
import cern.plcverif.plc.step7.step7Language.impl.SclStatementListImpl
import cern.plcverif.plc.step7.step7Language.SclAssignmentStatement
import cern.plcverif.plc.step7.step7Language.Variable
import cern.plcverif.plc.step7.step7Language.impl.VariableImpl
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.impl.DirectNamedRefImpl
import cern.plcverif.plc.step7.step7Language.NamedElement
import cern.plcverif.plc.step7.step7Language.VariableDeclarationDirection

/**
 * Class representing the main entry point for STEP 7 PLC code to CFA (Control Flow Automaton) generation.
 */
class Step7CodeToCfa {
	@Data
	static class Step7CodeToCfaResult {
		CfaNetworkDeclaration cfa;
		AtomicParser atomicParser; 
	}	
	
	/**
	 * Types assigned to the expressions of the STEP 7 PLC code AST.
	 */
	Step7TypeComputer astTypes;

	/**
	 * The scope of the CFA representation, i.e., the blocks and variables that 
	 * have to be represented in the CFA.
	 */
	TranslationScope translationScope;

	CfaNetworkDeclaration cfaNetwork = null;
	AstCfaVarTrace astCfaVarTrace = new AstCfaVarTrace();
	StructuralAstCfaTrace structuralTrace = new StructuralAstCfaTrace();
	CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;
	Step7ExprToCfaExpr exprConverter;
	ProgramFile mainProgramFile;
	List<AstVarView> varViews = null;
	final IPlcverifLogger logger;
	
	
	/**
	 * AST transformations to be performed before type computation.
	 */
	List<IStep7AstTransformation> astTransformations = #[
		new SpecializeFcsWithAnyNumParam(),
		new ExtractNestedCalls(),
		new RetValToOutputParam(),
		new SubstituteNamedConstantRefs(),
		new SubstituteDirectBitAccessRef()
	];

	private new(ProgramFile programFile, IPlcverifLogger logger) {
		this.mainProgramFile = programFile;
		this.translationScope = TranslationScope.collect(programFile.programUnits);
		this.logger = logger;

	// AST types and ExprConverter will be computed in precomputationAfterTransformations
	// this.astTypes = Step7TypeComputer.compute(translationScope.programFilesInScope);
	// exprConverter = new Step7ExprToCfaExpr(astCfaVarTrace, astTypes, structuralTrace);
	}

	def static Step7CodeToCfaResult convert(IPlcverifLogger log, ProgramFile file) throws PlcCodeToCfaException {
		// try to find the main program unit
		val ob1 = file.programUnits.filter[it|it instanceof OrganizationBlock && it.name.toUpperCase().endsWith("OB1")].
			findFirst[true];
		if (ob1 !== null) {
			return convert(log, file, ob1);
		}

		// No OB found; let's use the first block
		Preconditions.checkState(file.programUnits.size >= 1, "It is not possible to convert an empty file to CFA.");
		// Shared DBs and UDTs are not valid main blocks
		val firstBlock = file.programUnits.findFirst[it|canBeMainBlock(it)];
		Preconditions.checkNotNull(firstBlock, "No main unit candidate has been found.");
		return convert(log, file, firstBlock);
	}
	
	def static Step7CodeToCfaResult convert(IPlcverifLogger log, ProgramFile file, String entryBlockName) throws PlcCodeToCfaException {
		val ProgramUnit entryBlock = file.programUnits.findFirst[it | it.name.equalsIgnoreCase(entryBlockName)];
		if (entryBlock === null) {
			throw new PlcCodeToCfaException('''The block «entryBlockName» is not found.''');
		} else if (!canBeMainBlock(entryBlock)) {
			throw new PlcCodeToCfaException('''The block «entryBlock.name» cannot be used as an entry block.''');
		}
		return convert(log, file, entryBlock);
	}
	
	def static Step7CodeToCfaResult convert(IPlcverifLogger log, List<ProgramFile> files, String entryBlockName) throws PlcCodeToCfaException {
		// We need to chose the main program file too.
		for (file : files) {
			if (file.programUnits.filter[it | it.name.equalsIgnoreCase(entryBlockName)].isEmpty == false) {
				return convert(log, file, entryBlockName);
			}
		}
		throw new PlcCodeToCfaException('''The block «entryBlockName» is not found.''');
	}

	private def static boolean canBeMainBlock(ProgramUnit programUnit) {
		if (programUnit instanceof DataBlock) {
			return Step7LanguageHelper.isInstanceDb(programUnit);
		}
		return !(programUnit instanceof UserDefinedDataType);
	}

	/**
	 * Converts the given program file and all its dependencies into to a CFA.
	 * 
	 * @throws PlcCodeToCfaException If a non-recoverable error occurred during the conversion.
	 */
	def static Step7CodeToCfaResult convert(IPlcverifLogger log, ProgramFile file, ProgramUnit mainUnit) throws PlcCodeToCfaException {
		try {
			val instance = new Step7CodeToCfa(file, log);
			instance.doTransformations();
			instance.precomputationAfterTransformations();
			return instance.representFilesInScope(mainUnit);
		} catch (PlcCodeToCfaRuntimeException e) {
			// changing the type (this is in order to avoid polluting the internal implementation with 'throws' declarations)
			throw new PlcCodeToCfaException(e.message, e);
		} catch (UnsupportedOperationException e) {
			throw new PlcCodeToCfaException('''Unsupported feature was found during the automaton generation. «e.message»''', e);
		}
	}


	private def doTransformations() {
		// AST transformations
		// These transformations are performed before type computation.
		for (transformation : astTransformations) {
			transformation.transform(translationScope.programFilesInScope);
		}
		
		varViews = AstVarView.collect(translationScope.programFilesInScope);
	}

	def precomputationAfterTransformations() {
		// TODO: now translationScope is recalculated after AST transformations 
		// due to eventual changes, but this is not a nice solution. For the 
		// transformations a more lightweight scope computation would be enough 
		// (that collects all referred ProgramFiles).
		this.translationScope = TranslationScope.collect(mainProgramFile.programUnits);
		this.astTypes = Step7TypeComputer.compute(translationScope.programFilesInScope);
		this.exprConverter = new Step7ExprToCfaExpr(astCfaVarTrace, astTypes, structuralTrace);
	}

	private def Step7CodeToCfaResult representFilesInScope(ProgramUnit mainUnit) {
		return representFilesInScope("network", mainUnit);
	}

	private def Step7CodeToCfaResult representFilesInScope(String networkName, ProgramUnit mainUnit) {
		// create CfaNetwork
		cfaNetwork = factory.createCfaNetworkDeclaration(networkName);

		createMemoryAddressInstances();
		createGlobalVariableInstances();

		// block structure
		for (block : translationScope.programUnitsInScope) {
			representBlockStructure(block, cfaNetwork);
		}

		setMainContextAndAutomaton(mainUnit);
		cleanupComplexTypeDefs();
		
		val ProgramConverter programConverter = new ProgramConverter(cfaNetwork, astCfaVarTrace, astTypes, structuralTrace, logger);

		// fill automata with locations and transitions
		for (kvp : structuralTrace.programUnitToAutomaton.entrySet()) {
			val programUnit = kvp.key;
			val automaton = kvp.value;
			
			var statementList = Step7LanguageHelper.getStatementList(programUnit);
			if (statementList.isPresent) {
				// statement list may not be present if the block body is empty
				programConverter.convertBlock(statementList.get, automaton);
			} else {
				// create a dummy automaton
				val init = factory.createInitialLocation("init", automaton);
				val end = factory.createEndLocation("end", automaton);
				TransitionBuilder.builderBetween(init, end).name("t").condition(factory.trueLiteral()).build();
			}
		}
		
		// Prune varviews: remove views out of scope
		pruneViews(varViews, translationScope);
		(new RepresentVariableViews(cfaNetwork, astCfaVarTrace, varViews, logger)).execute();
		
		(new CreateUniqueContextsForSmallFcs(cfaNetwork)).execute();
		(new TimeRepresentation(cfaNetwork)).execute();

		val atomicParser = new AtomicParser(structuralTrace, astCfaVarTrace, cfaNetwork);
		return new Step7CodeToCfaResult(cfaNetwork, atomicParser);
	}
	
	/**
	 * Removes all variable views from the given list whose variables are not
	 * contained in program units in the translation scope.
	 */
	private static def pruneViews(List<AstVarView> varViews, TranslationScope scope) {
		varViews.removeIf([it | 
			val viewersUnit = EmfHelper.getContainerOfType(it.viewer, ProgramUnit);
			val storagesUnit = EmfHelper.getContainerOfType(it.storage, ProgramUnit);
			return viewersUnit !== null && storagesUnit !== null &&
				!scope.programUnitsInScope.contains(viewersUnit) && !scope.programUnitsInScope.contains(storagesUnit);
		]);
	}

	private def setMainContextAndAutomaton(ProgramUnit mainUnit) {
		if (structuralTrace.singletonProgramUnitToField.containsKey(mainUnit)) {
			// It is a singleton program unit (OB, FC, DB)
			var AutomatonDeclaration mainUnitAutomaton;
			if (mainUnit instanceof DataBlock) {
				Preconditions.checkState(Step7LanguageHelper.isInstanceDb(mainUnit),
					"Only instance DBs can be set as main block.");
				val structureFb = Step7LanguageHelper.getRepresentedFb(mainUnit.structure);
				Preconditions.checkState(structureFb.isPresent);
				mainUnitAutomaton = structuralTrace.programUnitToAutomaton.get(structureFb.get);
			} else {
				mainUnitAutomaton = structuralTrace.programUnitToAutomaton.get(mainUnit);
			}
			val mainUnitField = structuralTrace.singletonProgramUnitToField.get(mainUnit);

			Preconditions.checkNotNull(mainUnitAutomaton, "mainUnitAutomaton");
			Preconditions.checkNotNull(mainUnitField, "mainUnitField");

			cfaNetwork.mainAutomaton = mainUnitAutomaton;
			cfaNetwork.mainContext = factory.createFieldRef(mainUnitField);
		} else {
			// It is a program unit to be instantiated (FB without instance)
			Preconditions.checkState(mainUnit instanceof FunctionBlock,
				"Implicit instantiation is only possible for FB.");

			// create a new instance of the FB
			val implicitInstDbType = Step7LanguageFactory.eINSTANCE.createFbOrUdtDT();
			implicitInstDbType.setType(mainUnit);
			val implicitInstanceField = factory.createField("instance", cfaNetwork.rootDataStructure,
				representType(implicitInstDbType, cfaNetwork.rootDataStructure));
			logger.logInfo("Implicit instance created for FB '%s'.", mainUnit.name);
			cfaNetwork.mainAutomaton = structuralTrace.programUnitToAutomaton.get(mainUnit);
			cfaNetwork.mainContext = factory.createFieldRef(implicitInstanceField);

			Preconditions.checkNotNull(cfaNetwork.mainAutomaton, "cfaNetwork.mainAutomaton");
			Preconditions.checkNotNull(cfaNetwork.mainContext, "cfaNetwork.mainContext");

		}
	}

	private def createMemoryAddressInstances() {
		// Global memory addresses
		val addresses = translationScope.getMemoryAddressesInScope();
		for (address : addresses) {
			val cfaField = createFieldForAddress(address, cfaNetwork.rootDataStructure);
			astCfaVarTrace.addGlobalAddress(address, cfaField);
		}
	}
	
	/**
	 * Creates a field for the given memory address in the given data structure.
	 * Note: this method does NOT register the created CFA field in the variable trace!
	 */
	private def Field createFieldForAddress(S7MemoryAddress address, DataStructure containerDataStructure) {
		val exprType = Step7TypeToExprType.s7MemorySizeToExprType(address.memorySize);
		val cfaField = factory.createField(address.toString(), containerDataStructure, exprType);
		//astCfaVarTrace.addAddress(address, cfaField);

		factory.createDirectionFieldAnnotation(
			cfaField,
			Step7TypeToExprType.astToCfaDirection(address.memoryIdentifier)
		)

		if (exprType instanceof ElementaryType) {
			factory.createInitialAssignment(cfaField, Step7ExprToCfaExpr.defaultLiteral(exprType));
		}
		
		// For L memory areas add 'generated' annotation (as it is not accessible typically in reality)
		if (address.memoryIdentifier == S7AddressMemoryIdentifier.L) {
			factory.createInternalGeneratedFieldAnnotation(cfaField);
		}
		
		return cfaField;
	}

	private def createGlobalVariableInstances() {
		for (globalVar : translationScope.globalVarsInScope) {
			val globalVarBlock = EmfHelper.getContainerOfType(globalVar, GlobalVariableBlock);
			Preconditions.checkNotNull(globalVarBlock,
				"The declaring block of a global variable should not be null. Maybe it is not a global variable?");

			if (!SclSyntaxHelper.isIgnoredGlobalVarName(globalVar.name)) {
				val plcType = Step7LanguageHelper.getDataType(globalVar);
				val cfaType = representType(plcType, cfaNetwork.rootDataStructure);
				val cfaField = factory.createField(globalVar.name, cfaNetwork.rootDataStructure, cfaType);
				astCfaVarTrace.add(globalVar, cfaField);

				factory.createDirectionFieldAnnotation(cfaField,
					Step7TypeToExprType.astToCfaDirection(globalVarBlock.direction));
				if (plcType instanceof ElementaryDT) {
					factory.createOriginalDataTypeFieldAnnotation(cfaField, plcType.type.literal);
				}

				val vdl = EmfHelper.getContainerOfType(globalVar, VariableDeclarationLine);
				exprConverter.representInitialization(cfaField, vdl, false);
			}
		}
	}

	private def dispatch void representBlockStructure(ProgramUnit unit, CfaNetworkDeclaration parent) {
		throw new UnsupportedOperationException("Unknown program unit: " + unit);
	}

	private def dispatch void representBlockStructure(Function unit, CfaNetworkDeclaration parent) {
		if (structuralTrace.programUnitToType.containsKey(unit)) {
			return;
		}
		
		if (Step7LanguageHelper.isTypeConversionFunc(unit)) {
			// Special handling: skip data type conversion functions (e.g.,
			// REAL_TO_INT), don't create instance descriptor for them
			return;
		}
		
		if (unit.isBuiltIn && LibraryFunctionsToCfa.isHandledBuiltinBlock(unit)) {
			logger.logInfo("The function '%s' will not be represented by an automaton as it seems to be a built-in block.", unit.name);
			return;
		}

		representExecutableBlockStructure(unit, unit.declarationSection, parent)
		createSingletonForProgramUnit(unit);

		// represent RET_VAL
		if (!DataTypeUtil.isVoid(unit.returnType)) {
			val retvalType = representType(unit.returnType, parent.rootDataStructure);
			val retvalField = factory.createField(AstCfaVarTrace.RETVAL_VARIABLE_NAME,
				structuralTrace.programUnitToType.get(unit), retvalType);
			astCfaVarTrace.addRetval(unit, retvalField);
			if (retvalType instanceof ElementaryType) {
				factory.createInitialAssignment(retvalField, Step7ExprToCfaExpr.defaultLiteral(retvalType))
			} else {
				throw new UnsupportedOperationException(
					"Default literal for composite return values is not supported yet.");
			}
		}
	}

	private def dispatch void representBlockStructure(OrganizationBlock unit, CfaNetworkDeclaration parent) {
		if (structuralTrace.programUnitToType.containsKey(unit)) {
			return;
		}

		representExecutableBlockStructure(unit, unit.declarationSection, parent)
		createSingletonForProgramUnit(unit);
	}

	private def dispatch void representBlockStructure(FunctionBlock unit, CfaNetworkDeclaration parent) {
		if (structuralTrace.programUnitToType.containsKey(unit)) {
			return;
		}

		representExecutableBlockStructure(unit, unit.declarationSection, parent)
	}

	private def dispatch void representBlockStructure(DataBlock unit, CfaNetworkDeclaration parent) {
		val unitStructure = unit.structure;
		var Type fieldType;
		if (unitStructure instanceof FbOrUdtDT) {
			fieldType = representType(unitStructure, parent.rootDataStructure);
		} else if (unitStructure instanceof StructDT) {
			fieldType = representType(unitStructure, parent.rootDataStructure);
		} else {
			throw new UnsupportedOperationException();
		}

		val field = factory.createField(unit.name, parent.rootDataStructure, fieldType);
		structuralTrace.singletonProgramUnitToField.put(unit, field);

		if (unitStructure instanceof FbOrUdtDT && DataTypeUtil.isFbDataType(unitStructure as FbOrUdtDT)) {
			structuralTrace.fieldToAutomaton.put(field,
				structuralTrace.programUnitToAutomaton.get((unitStructure as FbOrUdtDT).type));
		}

		// Represent DB initial values 
		for (initAmt : unit.initialAssignments) {
			val relativeLeftRef = exprConverter.convertLeftValue(initAmt.leftValue);
			val fullLeftRef = CfaDeclarationUtils.concatenateDataRefs(factory.createFieldRef(field), relativeLeftRef);
			val rightValue = exprConverter.convertRefToCfaExpression(initAmt.constant);
			Preconditions.checkState(rightValue instanceof InitialValue, "Unexpected initial value: " + rightValue)
			factory.createComplexInitialAssignment(field, fullLeftRef, rightValue as InitialValue);
		}
	}

	private def dispatch void representBlockStructure(UserDefinedDataType unit, CfaNetworkDeclaration parent) {
		if (structuralTrace.programUnitToType.containsKey(unit)) {
			return;
		}

		val DataStructureRef type = _representType(unit.declaration, parent.rootDataStructure);
		type.definition.setDisplayName(type.definition.getDisplayName() + " from " + unit.name);
		structuralTrace.programUnitToType.put(unit, type.definition);
	}

	private def void representExecutableBlockStructure(ExecutableProgramUnit unit, DeclarationSection declSection,
		CfaNetworkDeclaration parent) {
		// Issue warning when an automaton is created for an empty built-in block
		if (unit !== null) {
			if (unit.isBuiltIn && unit.statements === null) {
				logger.
					logWarning('''The block '«unit.name»' to be represented is an empty built-in block. It is typically unnecessary to create an automaton for an empty built-in block, thus this may be a bug in PLCverif or the built-in block library.''');
			}
		}
			
		// type representing the function
		val functionDS = factory.createDataStructure(unit.name, parent.rootDataStructure);
		structuralTrace.programUnitToType.put(unit, functionDS);

		// automaton for program unit
		val automaton = factory.createAutomatonDeclaration(unit.name, parent, functionDS);
		structuralTrace.programUnitToAutomaton.put(unit, automaton);
		
		// Automaton annotations
		factory.createBlockAutomatonAnnotation(automaton, unit.eClass.name, unit.name, Step7LanguageHelper.isFunctionBlock(unit));

		// Calculate the set of variables which need to be initialized nondeterministically
		val nondeterminedVariables = calculateUsagesBeforeAssignment(unit)
		
		// automaton.localDataStructure is a reference!
		// automaton.localDataStructure.definition === functionDS
		// Representing the variables of the block
		for (varDeclBlock : declSection.variableDeclarations) {
			for (varDecl : varDeclBlock.variables) {
				for (variable : varDecl.variables) {
					val plcType = Step7LanguageHelper.getDataType(variable);
					val cfaFieldType = representType(plcType, functionDS);
					val cfaField = factory.createField(variable.name, functionDS, cfaFieldType);
					astCfaVarTrace.add(variable, cfaField);

					// Initialization
					exprConverter.representInitialization(cfaField, varDecl, nondeterminedVariables.contains(variable.name) && varDeclBlock.retain && !(varDeclBlock.direction.value == VariableDeclarationDirection.INPUT_VALUE));
					exprConverter.representBounds(cfaField, varDecl);

					// annotations
					if (variable.isReference) {
						// a reference variable is always a TEMP 
						factory.createDirectionFieldAnnotation(cfaField,
							DataDirection.TEMP);
					} else {
						factory.createDirectionFieldAnnotation(cfaField,
							Step7TypeToExprType.astToCfaDirection(varDeclBlock.direction));
					}
					if (plcType instanceof ElementaryDT) {
						factory.createOriginalDataTypeFieldAnnotation(cfaField, plcType.type.literal);
					}
				}
			}
		}
		
		// Local addresses
		for (address : translationScope.getLocalMemoryAddressesInProgramUnit(unit)) {
			val cfaField = createFieldForAddress(address, functionDS);
			astCfaVarTrace.addLocalAddress(unit, address, cfaField);
		}
	}
	
	/*
	 * Input: Executable program unit with statements
	 * Output: The set of variables for which holds: They are used before being assigned a value in the program
	 * 
	 * This is used for calculating which variables with the RETAIN keyword (persistent memory) need to be initialized nondeterministically. 
	 * This needs to be done if they are accessed before being written as in this case the state on startup is unknown.
	 */
	private def List<String> calculateUsagesBeforeAssignment(ExecutableProgramUnit unit){
		val invalidVarList = newArrayList;
		val assignedVars = newArrayList;
		if (unit.statements instanceof SclStatementListImpl) {
			for (statement : (unit.statements as SclStatementListImpl).statements) {
				val assignments = statement.eAllContents.filter(SclAssignmentStatement).toList
				if (statement instanceof SclAssignmentStatement){
					assignments.add(statement)
				}
				for (assignment : assignments){
					val List<DirectNamedRef> variablesRightSide = newArrayList
					if (assignment.rightValue instanceof DirectNamedRef){
						variablesRightSide.add(assignment.rightValue as DirectNamedRef)
					} else {
						variablesRightSide.addAll(assignment.rightValue.eAllContents.filter(DirectNamedRef).toList)
					}
					variablesRightSide.forEach[e | {
						if (!assignedVars.contains(e.getRef.name)){
							invalidVarList.add(e.getRef.name);
						}
					}]
					
					val List<DirectNamedRef> variableLeftSide = newArrayList
					if (assignment.leftValue instanceof DirectNamedRef){
						variableLeftSide.add(assignment.leftValue as DirectNamedRef)
					} else {
						variableLeftSide.addAll(assignment.leftValue.eAllContents.filter(DirectNamedRef).toList)
					}
					if (!variableLeftSide.empty){
						assignedVars.add(variableLeftSide.get(0).getRef.name)
					}
				}
			}
		}
		return invalidVarList
	}
	
	private def void createSingletonForProgramUnit(ProgramUnit unit) {
		Preconditions.checkArgument(!(unit instanceof DataBlock));

		val unitDS = structuralTrace.programUnitToType.get(unit);
		// field representing the singleton function
		val singletonField = factory.createField(unit.name, cfaNetwork.rootDataStructure,
			factory.createDataStructureRef(unitDS));
		structuralTrace.singletonProgramUnitToField.put(unit, singletonField);

		val correspondingProgramUnit = structuralTrace.programUnitToAutomaton.get(unit);
		Preconditions.checkState(correspondingProgramUnit !== null);
		structuralTrace.fieldToAutomaton.put(singletonField, correspondingProgramUnit);
	}

	private def dispatch Type representType(DataType plcType, DataStructure parent) {
		throw new UnsupportedOperationException("Unknown PLC data type: " + plcType);
	}

	private def dispatch Type representType(ElementaryDT plcType, DataStructure parent) {
		return Step7TypeToExprType.s7typeToExprType(plcType);
	}

	private def dispatch Type representType(ArrayDT plcType, DataStructure parent) {
		val elementType = representType(plcType.baseType, parent);

		// handling multiple dimensions
		var innerType = elementType;
		for (dim : plcType.dimensions.stream.collect(Collectors.toList()).reverse) {
			// iteration inside out
			innerType = factory.createArrayType(innerType, DataTypeUtil.arrayRangeLower(dim),
				DataTypeUtil.arrayRangeUpper(dim));
		}

		return innerType;
	}

	private def dispatch DataStructureRef representType(StructDT plcType, DataStructure parent) {
		var typeName = "anonymous type";
		if (plcType.eContainer instanceof UserDefinedDataType) {
			// This is a named type
			typeName = (plcType.eContainer as UserDefinedDataType).name;
		}
		val anonDataStructure = factory.createDataStructure(typeName, parent);

		for (varDeclLine : plcType.members) {
			for (variable : varDeclLine.variables) {
				// TODO unify with previous usages
				val varPlcType = Step7LanguageHelper.getDataType(variable);
				val cfaVarType = representType(varPlcType, anonDataStructure);
				val cfaField = factory.createField(variable.name, anonDataStructure, cfaVarType);
				astCfaVarTrace.add(variable, cfaField);
				
				if (varPlcType instanceof ElementaryDT) {
					factory.createOriginalDataTypeFieldAnnotation(cfaField, varPlcType.type.literal);
				}
				// We omit annotating with direction annotation

				// Initialization
				exprConverter.representInitialization(cfaField, varDeclLine, false);
			}
		}

		return factory.createDataStructureRef(anonDataStructure);
	}

	private def dispatch DataStructureRef representType(FbOrUdtDT plcType, DataStructure parent) {
		val referredProgramUnit = plcType.type;
		val cfaDataStructure = getDataStructureForProgramUnit(referredProgramUnit);
		return factory.createDataStructureRef(cfaDataStructure);
	}

	private def getDataStructureForProgramUnit(ProgramUnit unit) {
		// lazy
		if (!structuralTrace.programUnitToType.containsKey(unit)) {
			representBlockStructure(unit, cfaNetwork);
			Preconditions.checkState(structuralTrace.programUnitToType.containsKey(unit), "Unable to represent the block structure of a program unit.");
		}
		return structuralTrace.programUnitToType.get(unit);
	}

	/**
	 * Removes the unnecessary type definition duplications.
	 */
	private def void cleanupComplexTypeDefs() {
		val automatonDataStructures = cfaNetwork.automata.map[it | it.localDataStructure.definition].toSet;
		cleanupComplexTypeDefs(cfaNetwork.rootDataStructure.complexTypes, cfaNetwork.rootDataStructure.complexTypes, automatonDataStructures);
	}

	/**
	 * Removes the unnecessary type definition duplications.
	 * Merges identical complex types defined in the same data 
	 * structure. Also, it can reuse top-level complex data types 
	 * instead of locally-defined (anonymous) data types if they 
	 * are identical.
	 * 
	 * The data structures in {@code dsToIgnore} will not be modified or
	 * considered for the unification.
	 */
	private def void cleanupComplexTypeDefs(List<DataStructure> complexTypes, 
		List<DataStructure> topLevelComplexTypes, Set<DataStructure> dsToIgnore
	) {
		for (ds : complexTypes) {
			cleanupComplexTypeDefs(ds.complexTypes, topLevelComplexTypes, dsToIgnore);	
		}
		
		val candidates = new ArrayList(complexTypes);
		candidates.addAll(topLevelComplexTypes);
		
		for (ds1 : candidates.filter[it | !dsToIgnore.contains(it)]) {				
			for (ds2 : new ArrayList(complexTypes)) {
				if (ds1 !== ds2 && ds1.eContainer !== null && ds2.eContainer !== null && !dsToIgnore.contains(ds2)) {
					// println('''Checking if «ds1.name» and «ds2.name» can be unified...''');
					if (canBeUnified(ds1, ds2)) {
						// replace ds2 with ds1
						// If exactly one of them is a top level complex type, it will be 'ds1'.
						unifyIdenticalTypes(ds2, ds1);
					}
				}
			}
		}
	}

	/**
	 * Determines whether the two given data structures can be unified.
	 * 
	 * Two data structures can be unified if they:
	 * 	<li>Are not equivalent,
	 * 	<li>Have the same amount of fields,
	 * 	<li>Each of their fields can be unified.
	 */
	private def boolean canBeUnified(DataStructure ds1, DataStructure ds2) {
		if (ds1 === ds2) {
			return false;
		}

		if (ds1.fields.size != ds2.fields.size) {
			return false;
		}

		for (field1 : ds1.fields) {
			val field2 = ds2.fields.findFirst[it|it.name == field1.name];
			if (field2 === null || canBeUnified(field1, field2) == false) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Determines whether the two given fields can be unified.
	 * 
	 * Two fields can be unified if they:
	 * 	<li>Have the same name,
	 * 	<li>Have the corresponding initial assignments,
	 * 	<li>Have corresponding data types (equivalent data types or data types that can be unified)
	 */
	private def boolean canBeUnified(Field f1, Field f2) {
		if (f1.name != f2.name) {
			return false;
		}

		if (f1.initialAssignments.size !== f2.initialAssignments.size) {
			return false;
		}
		for (var i = 0; i < f1.initialAssignments.size; i++) {
			val amt1 = f1.initialAssignments.get(i);
			val amt2 = f2.initialAssignments.get(i);

			if (!amt1.leftValue.refersTo(f1) || !amt2.leftValue.refersTo(f2) ||
				!EcoreUtil.equals(amt1.rightValue, amt2.rightValue)) {
				// FIXME this is far too restrictive
				return false;
			}
		}

		if (f1.type.dataTypeEquals(f2.type)) {
			return true;
		}

		// maybe the data types are not equal, but can be unified, that's also fine
		if (f1.type instanceof DataStructureRef && f2.type instanceof DataStructureRef) {
			if (canBeUnified((f1.type as DataStructureRef).definition, (f2.type as DataStructureRef).definition)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Replaces all uses of 'ds1' with 'ds2'.
	 */
	private def void unifyIdenticalTypes(DataStructure ds1, DataStructure ds2) {
		Preconditions.checkArgument(cfaNetwork.rootDataStructure !== ds1, "The root data structure cannot be merged.");
		Preconditions.checkArgument(cfaNetwork.rootDataStructure !== ds2, "The root data structure cannot be merged.");

		// fix all DataStructureRefs pointing to ds1 to now point to ds2
		for (DataStructureRef dsRef : EmfHelper.getAllContentsOfType(cfaNetwork, DataStructureRef, false, [it |
			it.definition == ds1
		])) {
			dsRef.definition = ds2;
		}

		for (Field f1 : new ArrayList(ds1.fields)) {
			val Field f2 = ds2.fields.findFirst[it|it.name == f1.name];
			Preconditions.checkNotNull(f2);
			unifyIdenticalTypes(f1, f2);
		}

		CfaUtils.delete(ds1);
	}

	/**
	 * Replaces all uses of 'f1' with 'f2'.
	 */
	private def void unifyIdenticalTypes(Field f1, Field f2) {
		// fix astCfaVarTrace
		astCfaVarTrace.replace(f1, f2);

		// fix all fieldrefs pointing to f1 to now point to f2
		for (FieldRef fieldRef : EmfHelper.getAllContentsOfType(cfaNetwork, FieldRef, false, [it|it.field == f1])) {
			fieldRef.field = f2;
		}

		CfaUtils.delete(f1);
	}
}
