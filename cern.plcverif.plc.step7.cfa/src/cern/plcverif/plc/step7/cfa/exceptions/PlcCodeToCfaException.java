/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.cfa.exceptions;

/**
 * Represents a non-recoverable error happening during the conversion of Step7
 * PLC code into CFA.
 */
public class PlcCodeToCfaException extends Exception {
	private static final long serialVersionUID = -2924952068891544823L;

	public PlcCodeToCfaException() {
		super();
	}

	public PlcCodeToCfaException(String message) {
		super(message);
	}

	public PlcCodeToCfaException(String message, Throwable exception) {
		super(message, exception);
	}
}
