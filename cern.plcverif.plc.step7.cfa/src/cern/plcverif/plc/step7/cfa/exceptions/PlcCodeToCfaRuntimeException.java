/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.cfa.exceptions;

import cern.plcverif.plc.step7.cfa.Step7CodeToCfa;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Represents a non-recoverable error happening during the conversion of Step7
 * PLC code into CFA. This exception shall only be used internally. The
 * {@link Step7CodeToCfa} class will wrap the exception in a
 * {@link PlcCodeToCfaException} object.
 *
 * These exceptions are automatically logged.
 */
public class PlcCodeToCfaRuntimeException extends RuntimeException {
	private static final long serialVersionUID = -8481229162972617255L;

	private static final Logger log = LogManager.getLogger(PlcCodeToCfaRuntimeException.class);

	public PlcCodeToCfaRuntimeException() {
		super();
	}

	public PlcCodeToCfaRuntimeException(String message) {
		super(message);
		log.debug(String.format("PlcCodeToCfaException occurred, message: %s", message));
	}

	public PlcCodeToCfaRuntimeException(String format, Object... args) {
		super(String.format(format, args));
		log.debug(String.format("PlcCodeToCfaException occurred, message: %s", super.getMessage()));
	}
}
