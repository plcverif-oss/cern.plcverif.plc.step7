package cern.plcverif.plc.step7.cfa.impl

import cern.plcverif.base.common.logging.IPlcverifLogger
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.plc.step7.cfa.exceptions.PlcCodeToCfaRuntimeException
import cern.plcverif.plc.step7.cfa.trace.AstCfaVarTrace
import cern.plcverif.plc.step7.cfa.trace.StructuralAstCfaTrace
import cern.plcverif.plc.step7.step7Language.SclStatementList
import cern.plcverif.plc.step7.step7Language.StatementList
import cern.plcverif.plc.step7.step7Language.StlStatementList
import cern.plcverif.plc.step7.typecomputer.Step7TypeComputer
import com.google.common.base.Preconditions
import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor

@FinalFieldsConstructor
class ProgramConverter {
	final CfaNetworkDeclaration automataNetwork;
	final AstCfaVarTrace astCfaVarTrace;
	final Step7TypeComputer astTypes;
	final StructuralAstCfaTrace structuralTrace;
	final IPlcverifLogger logger;
	
	StlGlobalData stlGlobalData = null;
	
	
	/**
	 * Depending on the implementation language of the given statement list ({@code e}), it creates an instance of the appropriate
	 * BlockToCfa subclass, and it generates the transitions and locations required to represent the given statement list {@code e} in the CFA. 
	 */
	def BlockToCfa convertBlock(StatementList e, AutomatonDeclaration representingAutomaton) {
		if (e instanceof SclStatementList || e === null) {
			return new SclBlockToCfa(e as SclStatementList, representingAutomaton, astCfaVarTrace, astTypes, structuralTrace, logger).convert();
		} else if (e instanceof StlStatementList) {
			createStlGlobalDataIfNeeded();
			return new StlBlockToCfa(e, representingAutomaton, astCfaVarTrace, astTypes, structuralTrace, stlGlobalData, logger).convert();
		} else {
			throw new PlcCodeToCfaRuntimeException("Unsupported language used in the statement list '%s' that cannot be represented as CFA (yet).", e.class.name);
		}
	}
	
	private def createStlGlobalDataIfNeeded() {
		if (stlGlobalData === null) {
			stlGlobalData = new StlGlobalData(automataNetwork);
		}
		
		Preconditions.checkState(stlGlobalData !== null);
	}
	
}