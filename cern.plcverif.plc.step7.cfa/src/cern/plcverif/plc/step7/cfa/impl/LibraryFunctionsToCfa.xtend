package cern.plcverif.plc.step7.cfa.impl

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.cfa.builder.VariableAssignmentSkeleton
import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.FloatWidth
import cern.plcverif.base.models.expr.LibraryFunction
import cern.plcverif.base.models.expr.LibraryFunctions
import cern.plcverif.plc.step7.step7Language.CallParameter
import cern.plcverif.plc.step7.step7Language.CallParameterItem
import cern.plcverif.plc.step7.step7Language.CallParameterList
import cern.plcverif.plc.step7.step7Language.EnCallParameterItem
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.LeftValue
import cern.plcverif.plc.step7.step7Language.NamedCallParameterItem
import cern.plcverif.plc.step7.step7Language.RetValCallParameterItem
import cern.plcverif.plc.step7.step7Language.SingleCallParameter
import cern.plcverif.plc.step7.step7Language.SubroutineCall
import cern.plcverif.plc.step7.step7Language.Variable
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import com.google.common.base.Preconditions
import java.util.ArrayList
import java.util.Collections
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.Optional
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor

/**
 * Class providing representation to built-in functions which shall be 
 * represented by library functions ({@link LibraryFunction}) in the CFA declaration.
 */
final class LibraryFunctionsToCfa {
	// Representation instances
	val static LOG = new FloatLibraryFunctionRepresentation("LOG", LibraryFunctions.LOG);
	val static LN = new FloatLibraryFunctionRepresentation("LN", LibraryFunctions.LN);
	val static SIN = new FloatLibraryFunctionRepresentation("SIN", LibraryFunctions.SIN);
	val static COS = new FloatLibraryFunctionRepresentation("COS", LibraryFunctions.COS);
	val static TAN = new FloatLibraryFunctionRepresentation("TAN", LibraryFunctions.TAN);
	val static ASIN = new FloatLibraryFunctionRepresentation("ASIN", LibraryFunctions.ASIN);
	val static ACOS = new FloatLibraryFunctionRepresentation("ACOS", LibraryFunctions.ACOS);
	val static ATAN = new FloatLibraryFunctionRepresentation("ATAN", LibraryFunctions.ATAN);
	val static EXP = new FloatLibraryFunctionRepresentation("EXP", LibraryFunctions.EXP);
	val static SQRT = new FloatLibraryFunctionRepresentation("SQRT", LibraryFunctions.SQRT);

	/**
	 * All known built-in function representations
	 */
	val static BUILTIN_FUNCTION_REPRESENTATIONS = #[LOG, LN, SIN, COS, TAN, ASIN, ACOS, ATAN, EXP, SQRT];

	/**
	 * Lookup table for the {@link #BUILTIN_BLOCK_REPRESENTATIONS},
	 * where the key is the name ({@link BuiltInBlockRepresentation#getBlockName()}) in upper case.
	 */
	val static Map<String, BuiltInBlockRepresentation> LOOKUP_TABLE = {
		val ret = new HashMap<String, BuiltInBlockRepresentation>();
		for (representation : LibraryFunctionsToCfa.BUILTIN_FUNCTION_REPRESENTATIONS) {
			ret.put(representation.getBlockName().toUpperCase, representation);
		}
		return ret;
	}

	static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;

	private new() {
		// Utility class.
	}

	/**
	 * Returns true if the given subroutine call is a call of a built-in function call
	 * that needs to be represented by a library function in the CFA.
	 * I.e., it returns true if this class can handle the given subroutine call.
	 * 
	 * The check is based on the name of the called function only. In addition, 
	 * it is checked whether the call target is a function (and not e.g., a function block). 
	 */
	def static boolean isHandledBuiltinFunctionCall(SubroutineCall e) {
		return Step7LanguageHelper.isFcCall(e) && isKnownName(e.calledUnit.name);
	}
	
	/**
	 * Returns true if the given block is a built-in function that needs to be represented 
	 * by a library function in the CFA.
	 * 
	 * The check is based on the name of the called function only. 
	 */
	def static boolean isHandledBuiltinBlock(Function e) {
		return isKnownName(e.name);
	}

	private def static boolean isKnownName(String blockName) {
		return LOOKUP_TABLE.containsKey(blockName.toUpperCase);
	}

	private def static BuiltInBlockRepresentation get(String blockName) {
		return LOOKUP_TABLE.get(blockName.toUpperCase);
	}

	/**
	 * Returns true if the given call can be represented as a call. If the returned value is true, 
	 * the {@link #representAsCall(SubroutineCall, Location, Step7ExprToCfaExpr)} can be used to create
	 * the representation.
	 * 
	 * @return True if the given call can be represented as a call. If the name of the called block
	 *         does not match the block name of this, the return value shall always be false.
	 * @see #representAsCall(SubroutineCall, Location, Step7ExprToCfaExpr)
	 */
	def static boolean canRepresentAsCall(SubroutineCall e) {
		Preconditions.checkNotNull(e, "The call to be represented cannot be null.");
		Preconditions.checkState(isHandledBuiltinFunctionCall(e), "The callee is not known.");

		return get(e.calledUnit.name).canRepresentAsCall(e);
	}

	/**
	 * Represents the given call as a CFA fragment, starting from the given location.
	 * 
	 * @param e The call to be represented. It is expected to be representable as a call, 
	 * 	  i.e., it is expected that the {@link #canRepresentAsCall(SubroutineCall)} was already called
	 *    for this call and it returned true.
	 * @param start Start location for the CFA fragment representation
	 * @param exprConverter STEP7 to CFA expression converter to be used for the conversion of the 
	 *    call parameters.
	 * @return End location of the generated CFA fragment. The next statement will be represented 
	 *    starting from this location.
	 * @see #canRepresentAsCall(SubroutineCall)
	 */
	def static Location representAsCall(SubroutineCall e, Location start, Step7ExprToCfaExpr exprConverter) {
		Preconditions.checkNotNull(e, "The call to be represented cannot be null.");
		Preconditions.checkState(isHandledBuiltinFunctionCall(e), "The callee is not known.");

		return get(e.calledUnit.name).representAsCall(e, start, exprConverter);
	}

	/**
	 * Returns true if the given call can be represented as an expression. If the returned
	 * value is true, the {@link #representAsExpression(SubroutineCall, Step7ExprToCfaExpr)} can be
	 * used to create the representation.
	 * 
	 * @return True if the given call can be represented as an expression.
	 *         If the name of the called block does not match the block name of this, the return value
	 *         shall always be false.
	 * @see #representAsExpression(SubroutineCall, Step7ExprToCfaExpr)
	 */
	def static boolean canRepresentAsExpression(SubroutineCall e) {
		Preconditions.checkNotNull(e, "The call to be represented cannot be null.");
		Preconditions.checkState(isHandledBuiltinFunctionCall(e), "The callee is not known.");

		return get(e.calledUnit.name).canRepresentAsExpression(e);
	}

	/**
	 * Represents the given call as a CFA declaration expression.
	 * 
	 * @param e The call to be represented. It is expected to be representable as an expression, 
	 * 	  i.e., it is expected that the {@link #canRepresentAsExpression(SubroutineCall)} was already called
	 *    for this call and it returned true.
	 * @param exprConverter STEP7 to CFA expression converter to be used for the conversion of the 
	 *    call parameters.
	 * @return The corresponding CFA declaration expression.
	 * @see #canRepresentAsExpression(SubroutineCall)
	 */
	def static Expression representAsExpression(SubroutineCall e, Step7ExprToCfaExpr exprConverter) {
		Preconditions.checkNotNull(e, "The call to be represented cannot be null.");
		Preconditions.checkState(isHandledBuiltinFunctionCall(e), "The callee is not known.");

		return get(e.calledUnit.name).representAsExpression(e, exprConverter);
	}

	/**
	 * Interface for a built-in block representation.
	 * A built-in block can be represented in two different situations: 
	 * <ul>
	 * 	<li>As a <b>call</b>, in which case a CFA fragment (some transitions and locations from
	 * 		a given start location) is expected to be generated,
	 *  <li>As an <b>expression</b>, in which case a CFA expression is expected to be generated.
	 * </ul>
	 * Certain built-in blocks may not be represented as calls or as expressions, depending on their details.
	 */
	interface BuiltInBlockRepresentation {
		/**
		 * Returns the name of the represented block (i.e., the name of the Siemens block, as used in SCL).
		 * 
		 * Comparison of this name should be case insensitive.
		 */
		def String getBlockName();

		/**
		 * Returns true if the given call can be represented as a call by this. If the returned value is true, 
		 * the {@link #representAsCall(SubroutineCall, Location, Step7ExprToCfaExpr)} can be used to create
		 * the representation.
		 * 
		 * @return True if the given call can be represented as a call by this. If the name of the called block
		 *         does not match the block name of this, the return value shall always be false.
		 * @see #representAsCall(SubroutineCall, Location, Step7ExprToCfaExpr)
		 */
		def boolean canRepresentAsCall(SubroutineCall e);

		/**
		 * Represents the given call as a CFA fragment, starting from the given location.
		 * 
		 * @param e The call to be represented. It is expected to be representable as a call by this, 
		 * 	  i.e., it is expected that the {@link #canRepresentAsCall(SubroutineCall)} was already called
		 *    for this call and it returned true.
		 * @param start Start location for the CFA fragment representation
		 * @param exprConverter STEP7 to CFA expression converter to be used for the conversion of the 
		 *    call parameters.
		 * @return End location of the generated CFA fragment. The next statement will be represented 
		 *    starting from this location.
		 * @see #canRepresentAsCall(SubroutineCall)
		 */
		def Location representAsCall(SubroutineCall e, Location start, Step7ExprToCfaExpr exprConverter);

		/**
		 * Returns true if the given call can be represented as an expression by this. If the returned
		 * value is true, the {@link #representAsExpression(SubroutineCall, Step7ExprToCfaExpr)} can be
		 * used to create the representation.
		 * 
		 * @return True if the given call can be represented as an expression by this.
		 *         If the name of the called block does not match the block name of this, the return value
		 *         shall always be false.
		 * @see #representAsExpression(SubroutineCall, Step7ExprToCfaExpr)
		 */
		def boolean canRepresentAsExpression(SubroutineCall e);

		/**
		 * Represents the given call as a CFA declaration expression.
		 * 
		 * @param e The call to be represented. It is expected to be representable as an expression by this, 
		 * 	  i.e., it is expected that the {@link #canRepresentAsExpression(SubroutineCall)} was already called
		 *    for this call and it returned true.
		 * @param exprConverter STEP7 to CFA expression converter to be used for the conversion of the 
		 *    call parameters.
		 * @return The corresponding CFA declaration expression.
		 * @see #canRepresentAsExpression(SubroutineCall)
		 */
		def Expression representAsExpression(SubroutineCall e, Step7ExprToCfaExpr exprConverter);
	}

	/**
	 * Abstract implementation of {@link BuiltInBlockRepresentation} for built-in functions.
	 * The subclasses of this class are expected to implement the representation of expression,
	 * but this class contains a default implementation for the representation as call based on
	 * {@link #representAsExpression(SubroutineCall, Step7ExprToCfaExpr)}. It will fetch the 
	 * return value call parameter and create an assigment transition based on the expression
	 * representation.
	 * 
	 * It is assumed that the call to be represented does only have input parameters (i.e., no
	 * in-out or output parameters).
	 */
	@FinalFieldsConstructor
	static abstract class BuiltInFunctionRepresentation implements BuiltInBlockRepresentation {
		final String blockName;

		override String getBlockName() {
			return blockName;
		}

		protected def boolean calledBlockNameMatches(SubroutineCall e) {
			return blockName !== null && blockName.equalsIgnoreCase(e.calledUnit.name) &&
				!(e.calledUnit instanceof Variable);
		}

		override boolean canRepresentAsCall(SubroutineCall e) {
			return calledBlockNameMatches(e) && Step7LanguageHelper.isFcCall(e) && tryFindRetValParam(e).isPresent &&
				canRepresentAsExpression(e);
		}

		override Location representAsCall(SubroutineCall e, Location start, Step7ExprToCfaExpr exprConverter) {
			val retValParam = tryFindRetValParam(e);
			Preconditions.checkState(retValParam.isPresent,
				"Unable to represent as call with the default implementation, RET_VAL parameter is not found.");
			val callParamLeftSide = exprConverter.convertLeftValue(retValParam.get.value as LeftValue);
			val amt = VariableAssignmentSkeleton.create(EcoreUtil.copy(callParamLeftSide),
				representAsExpression(e, exprConverter));

			return createSingleAssignmentTransition(start, amt);
		}

		override boolean canRepresentAsExpression(SubroutineCall e) {
			return calledBlockNameMatches(e) && Step7LanguageHelper.isFcCall(e);
		}

		override abstract Expression representAsExpression(SubroutineCall e, Step7ExprToCfaExpr exprConverter);
	}

	/**
	 * Default implementation of {@link BuiltInFunctionRepresentation} for library
	 * functions which have a single parameter (and that is input) and return 32 bit float.
	 */
	static class FloatLibraryFunctionRepresentation extends BuiltInFunctionRepresentation {
		LibraryFunctions libraryFunction;
		final CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;

		/**
		 * Creates a new representation for a float32-returning library function.
		 * @param blockName Name of the PLC block to be represented
		 * @param libraryFunction The represented library function enum
		 */
		new(String blockName, LibraryFunctions libraryFunction) {
			super(blockName);
			this.libraryFunction = libraryFunction;
		}

		override representAsExpression(SubroutineCall e, Step7ExprToCfaExpr exprConverter) {
			val libraryFunParams = toLibraryParameterList(e.callParameter, exprConverter);
			Preconditions.checkState(libraryFunParams.size == 1,
				"This library function representation expects to have exactly one parameter.");
			return factory.createLibraryFunction(libraryFunParams, libraryFunction,
				factory.createFloatType(FloatWidth.B32));
		}
	}

	// HELPERS
	/**
	 * Returns the call parameter item that assigns the return value. If not found, {@link Optional#empty()} is returned.
	 * It is assumed that at most one return value call parameter item can be found for the given call.
	 */
	private def static Optional<RetValCallParameterItem> tryFindRetValParam(SubroutineCall e) {
		// TODO_LOWPRI Move to Step7LanguageHelper
		for (item : EmfHelper.toIterable(e.eAllContents)) {
			if (item instanceof RetValCallParameterItem) {
				return Optional.of(item);
			}
		}

		return Optional.empty();
	}

	/**
	 * Returns the list of call parameters in the given {@link CallParameter} object.
	 * If there are multiple call parameters defined, they will be returned in the order of presence in the call.
	 * Return value parameter items ({@code RET_VAL := ...} with type {@link RetValCallParameterItem}) and 
	 * EN parameter items ({@code EN := ...} with type {@link EnCallParameterItem}) will be excluded.
	 */
	private def static dispatch List<Expression> toLibraryParameterList(CallParameter callParams,
		Step7ExprToCfaExpr exprConverter) {
		throw new UnsupportedOperationException("Unhandled call parameter type: " + callParams);
	}

	private def static dispatch List<Expression> toLibraryParameterList(Void callParams,
		Step7ExprToCfaExpr exprConverter) {
		return Collections.emptyList();
	}

	private def static dispatch List<Expression> toLibraryParameterList(SingleCallParameter callParam,
		Step7ExprToCfaExpr exprConverter) {
		return Collections.singletonList(exprConverter.convertExpression(callParam));
	}

	private def static dispatch List<Expression> toLibraryParameterList(CallParameterItem callParam,
		Step7ExprToCfaExpr exprConverter) {
		throw new UnsupportedOperationException("Unhandled call parameter item type: " + callParam);
	}

	private def static dispatch List<Expression> toLibraryParameterList(NamedCallParameterItem callParam,
		Step7ExprToCfaExpr exprConverter) {
		return Collections.singletonList(exprConverter.convertExpression(callParam.value));
	}

	private def static dispatch List<Expression> toLibraryParameterList(RetValCallParameterItem callParam,
		Step7ExprToCfaExpr exprConverter) {
		// Don't represent the return value 
		return Collections.emptyList();
	}

	private def static dispatch List<Expression> toLibraryParameterList(EnCallParameterItem callParam,
		Step7ExprToCfaExpr exprConverter) {
		// Don't represent EN
		return Collections.emptyList();
	}

	private def static dispatch List<Expression> toLibraryParameterList(CallParameterList callParams,
		Step7ExprToCfaExpr exprConverter) {
		val ret = new ArrayList<Expression>();
		for (callParam : callParams.parameters) {
			ret.addAll(toLibraryParameterList(callParam, exprConverter));
		}
		return ret;
	}

	/**
	 * Creates a new assignment transition from the given start location with the given assignment.
	 * It returns the newly created target location of the created transition.
	 */
	private def static Location createSingleAssignmentTransition(Location start, VariableAssignmentSkeleton amt) {
		val automaton = start.parentAutomaton as AutomatonDeclaration;
		val end = FACTORY.createLocation("builtinblockL", automaton);
		val transition = FACTORY.createAssignmentTransition("builtinblockT", automaton, start, end,
			FACTORY.trueLiteral());
		amt.createVariableAssignments(transition);
		return end;
	}
}
