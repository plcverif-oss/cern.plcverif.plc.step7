/******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.impl

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.cfa.cfabase.ArrayType
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils
import cern.plcverif.base.models.expr.BinaryArithmeticOperator
import cern.plcverif.base.models.expr.BoolType
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.expr.ElementaryType
import cern.plcverif.base.models.expr.ExplicitlyTyped
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.FloatType
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.Literal
import cern.plcverif.base.models.expr.StringType
import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.TypeConversion
import cern.plcverif.base.models.expr.Typed
import cern.plcverif.base.models.expr.UnaryArithmeticOperator
import cern.plcverif.base.models.expr.UnaryLogicOperator
import cern.plcverif.base.models.expr.UnknownType
import cern.plcverif.base.models.expr.utils.ExprTypeCache
import cern.plcverif.plc.step7.cfa.exceptions.PlcCodeToCfaRuntimeException
import cern.plcverif.plc.step7.cfa.trace.AstCfaVarTrace
import cern.plcverif.plc.step7.cfa.trace.StructuralAstCfaTrace
import cern.plcverif.plc.step7.datatypes.S7IntegerType
import cern.plcverif.plc.step7.datatypes.S7RealType
import cern.plcverif.plc.step7.step7Language.AdditionOperator
import cern.plcverif.plc.step7.step7Language.AdditiveExpression
import cern.plcverif.plc.step7.step7Language.AndExpression
import cern.plcverif.plc.step7.step7Language.ArrayInitialization
import cern.plcverif.plc.step7.step7Language.ArrayRef
import cern.plcverif.plc.step7.step7Language.BoolConstant
import cern.plcverif.plc.step7.step7Language.CharConstant
import cern.plcverif.plc.step7.step7Language.ComparisonExpression
import cern.plcverif.plc.step7.step7Language.ConstantInitializationElement
import cern.plcverif.plc.step7.step7Language.DataBlock
import cern.plcverif.plc.step7.step7Language.DataType
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.ElementaryDT
import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum
import cern.plcverif.plc.step7.step7Language.EqualityExpression
import cern.plcverif.plc.step7.step7Language.EqualityOperator
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.ImpliesExpression
import cern.plcverif.plc.step7.step7Language.InitializationRepetitionList
import cern.plcverif.plc.step7.step7Language.IntConstant
import cern.plcverif.plc.step7.step7Language.LeftValue
import cern.plcverif.plc.step7.step7Language.MultiplicationOperator
import cern.plcverif.plc.step7.step7Language.MultiplicativeExpression
import cern.plcverif.plc.step7.step7Language.NamedConstantDeclaration
import cern.plcverif.plc.step7.step7Language.NamedOrUnnamedConstantRef
import cern.plcverif.plc.step7.step7Language.OrExpression
import cern.plcverif.plc.step7.step7Language.PowerExpression
import cern.plcverif.plc.step7.step7Language.QualifiedRef
import cern.plcverif.plc.step7.step7Language.RealConstant
import cern.plcverif.plc.step7.step7Language.RetValRef
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall
import cern.plcverif.plc.step7.step7Language.SingleCallParameter
import cern.plcverif.plc.step7.step7Language.TimeConstant
import cern.plcverif.plc.step7.step7Language.TimeOfDayConstant
import cern.plcverif.plc.step7.step7Language.UnaryExpression
import cern.plcverif.plc.step7.step7Language.UnnamedConstant
import cern.plcverif.plc.step7.step7Language.Variable
import cern.plcverif.plc.step7.step7Language.VariableDeclarationLine
import cern.plcverif.plc.step7.step7Language.VariableInitialization
import cern.plcverif.plc.step7.step7Language.XorExpression
import cern.plcverif.plc.step7.typecomputer.Step7TypeComputer
import cern.plcverif.plc.step7.util.DataTypeUtil
import cern.plcverif.plc.step7.util.SimpleExpressionUtil
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import com.google.common.base.Preconditions
import java.util.List
import java.util.Optional
import java.util.Stack
import java.util.function.Consumer
import org.eclipse.emf.ecore.util.EcoreUtil

import static extension cern.plcverif.plc.step7.util.DataTypeUtil.isBooleanElementaryType
import cern.plcverif.plc.step7.step7Language.MemoryAddress
import cern.plcverif.plc.step7.cfa.util.MemoryAddressArrayUtil
import cern.plcverif.plc.step7.datatypes.S7AddressMemoryIdentifier
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit
import cern.plcverif.plc.step7.step7Language.UserDefinedDataType
import java.math.BigInteger
import cern.plcverif.base.models.expr.InitialValue

final class Step7ExprToCfaExpr {
	
	protected AstCfaVarTrace astCfaVarTrace;
	
	protected Step7TypeComputer astTypes;
	
	protected StructuralAstCfaTrace structuralTrace;
	
	protected static final CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;
	
	ExprTypeCache typeCache = new ExprTypeCache();
	
	new(AstCfaVarTrace astCfaVarTrace, Step7TypeComputer astTypes, StructuralAstCfaTrace structuralTrace) {
		this.astCfaVarTrace = astCfaVarTrace;
		this.astTypes = astTypes;
		this.structuralTrace = structuralTrace;
	}
	
	def getAstTypes() {
		return astTypes;
	}
	
	def getAstCfaVarTrace() {
		return astCfaVarTrace;
	}
	
	private def Type cachedType(Typed typed) {
		if (typed instanceof ExplicitlyTyped) {
			return typed.type;
		}
		
		return typeCache.getType(typed);
	}
	
	// EXPRESSION CONVERSION METHODS
	def Expression convertExpressionWithTypeConv(cern.plcverif.plc.step7.step7Language.Expression astExpr) {
		Preconditions.checkNotNull(astExpr, "Unable to convert null in 'convertExpressionWithTypeConv'.");
		
		val astType = astTypes.tryGetExpectedType(astExpr);
		if (astType.isPresent) {
			if (astType.get() instanceof ElementaryDT) {
				val cfaExpectedType = Step7TypeToExprType.s7typeToExprType(astType.get() as ElementaryDT);
				val cfaExpr = convertExpression(astExpr);
				if (!cfaExpr.cachedType.dataTypeEquals(cfaExpectedType)) {
					return factory.createTypeConversion(cfaExpr, cfaExpectedType);
				} else {
					return cfaExpr;
				}
			}
		}
		
		return convertExpression(astExpr);
	}
	
	def Expression convertExpressionWithTypeConv(cern.plcverif.plc.step7.step7Language.Expression astExpr, Type targetCfaType) {
		val cfaExpr = convertExpression(astExpr);
		return convertExpressionWithTypeConv(cfaExpr, targetCfaType);
//		Preconditions.checkState(cfaExpr.type !== null, "Impossible to generate implicit type conversions for an expression with unknown type.");
//		if (!cfaExpr.type.dataTypeEquals(targetCfaType)) {
//			return createTypeConversion(cfaExpr, EcoreUtil.copy(targetCfaType));
//		} else {
//			return cfaExpr;
//		}
	}
	
	def Expression convertExpressionWithTypeConv(Expression cfaExpr, Type targetCfaType) {
		Preconditions.checkState(cfaExpr.cachedType !== null, "Impossible to generate implicit type conversions for an expression with unknown type.");
		if (!cfaExpr.cachedType.dataTypeEquals(targetCfaType)) {
			return factory.createTypeConversion(cfaExpr, EcoreUtil.copy(targetCfaType));
		} else {
			return cfaExpr;
		}
	}
	
	def dispatch Expression convertExpression(cern.plcverif.plc.step7.step7Language.Expression expression) {
		throw new UnsupportedOperationException("Unknown STEP 7 AST expression type: " + expression)
	}
	
	def dispatch Expression convertExpression(UnnamedConstant astExpr) {
		throw new UnsupportedOperationException("Unknown UnnamedConstant subclass: " + astExpr.class.name);
	}
	
	def dispatch Expression convertExpression(BoolConstant e) {
		return factory.createBoolLiteral(e.value.boolValue);
	}
	
	def dispatch Expression convertExpression(IntConstant e) {
		val astType = astTypes.tryGetExpectedType(e);
	
		if (astType.isPresent && astType.get instanceof ElementaryDT) {
			val cfaType = representType(astType.get as ElementaryDT);
			if (cfaType instanceof FloatType) {
				// a float can be represented as STEP 7 IntConstant
				return factory.createFloatLiteral(e.value.doubleValue, cfaType);
			} else if (cfaType instanceof BoolType) {
				// a Boolean can be represented as STEP 7 IntConstant
				var boolean value = false;
				if (e.value.intValue == 0) {
					value = false;
				} else if (e.value.intValue == 1) {
					value = true;
				} else {
					throw new PlcCodeToCfaRuntimeException("Unable to convert the value %s to a BoolLiteral.", e.value.intValue);
				}
				return factory.createBoolLiteral(value, cfaType);
			}
			
			// We handled the special cases, now we expect to have an IntType
			
			Preconditions.checkState(cfaType instanceof IntType, "The type of an IntConstant should be IntType. Instead it is " + cfaType.class.name)
			// uint64->int64 conversion here if needed -- FIXME test and review the solution
			var BigInteger intVal = BigInteger.valueOf(e.value.intValue);
			val cfaIntType = cfaType as IntType;
			if (cfaIntType.signed && intVal >= BigInteger.ONE.shiftLeft(cfaIntType.bits - 1)) {
			        intVal = intVal - BigInteger.ONE.shiftLeft(cfaIntType.bits); 
			}
			return factory.createIntLiteral(intVal.longValue, cfaType as IntType);
		} else {
			// last hope: this constant is not in the scope of the type computer, but has an explicit type
			if (e.value.type !== S7IntegerType.Unknown) {
				return factory.createIntLiteral(e.value.intValue, Step7TypeToExprType.s7IntTypeToCfaType(e.value.type));
			}
			
			if (astType.isPresent && DataTypeUtil.isAnyNum(astType.get)) {
				// ANY_NUM --> assume smallest int literal
				return factory.createIntLiteralWithMinimalType(e.value.intValue);
			}
		
			throw new PlcCodeToCfaRuntimeException("IntConstant ('%s') with non-elementary data type '%s'. Inferred data type: %s.", e.value.originalStringRepresentation, e.value.type, astType);
		}
	}
	
	def dispatch Expression convertExpression(CharConstant e) {
		val astType = astTypes.tryGetExpectedType(e);
		if (astType.isPresent && astType.get instanceof ElementaryDT) {
			val cfaType = representType(astType.get as ElementaryDT);
			var long intVal = e.value.integerValue;
			val cfaIntType = cfaType as IntType;
			Preconditions.checkState(!cfaIntType.signed, "Unexpected: signed char type.");
			return factory.createIntLiteral(intVal, cfaIntType);
		}
		
		throw new PlcCodeToCfaRuntimeException("CharConstant ('%s') with unexpected data type '%s'.",
			e.value.originalStringRepresentation, astType);
	}
	
	def dispatch Expression convertExpression(RealConstant e) {
		val astType = astTypes.tryGetExpectedType(e);
	
		if (astType.isPresent && astType.get instanceof ElementaryDT) {
			val cfaType = representType(astType.get as ElementaryDT);
			Preconditions.checkState(cfaType instanceof FloatType, "The type of an RealConstant should be FloatType.")
			return factory.createFloatLiteral(e.value.doubleValue, cfaType as FloatType);
		} else {
			// last hope: this constant is not in the scope of the type computer, but has an explicit type
			if (e.value.type !== S7RealType.Unknown) {
				return factory.createFloatLiteral(e.value.doubleValue, Step7TypeToExprType.s7RealTypeToCfaType(e.value.type));
			}
			
			throw new PlcCodeToCfaRuntimeException("RealConstant with non-elementary data type.");
		}
	}
	
	private def ElementaryType representType(ElementaryDT plcType) {
		return Step7TypeToExprType.s7typeToExprType(plcType);
	}
	
	def dispatch Expression convertExpression(TimeConstant e) {
		val cfaType = tryGetElementaryCfaType(e).orElse(Step7TypeToExprType.s7typeToExprType(ElementaryTypeEnum.TIME));
		Preconditions.checkState(cfaType instanceof IntType, "TimeConstant with non-integer type.");			
		return factory.createIntLiteral(e.value.totalMilliseconds, cfaType as IntType);
	}
	
	def dispatch Expression convertExpression(TimeOfDayConstant e) {
		val cfaType = tryGetElementaryCfaType(e).orElse(Step7TypeToExprType.s7typeToExprType(ElementaryTypeEnum.TIME));
		Preconditions.checkState(cfaType instanceof IntType, "TimeConstant with non-integer type.");			
		return factory.createIntLiteral(e.value.totalMilliseconds, cfaType as IntType);
	}

	def dispatch Expression convertExpression(ImpliesExpression e) {
		return factory.impl(convertExpressionWithTypeConv(e.left), convertExpressionWithTypeConv(e.right));
	}

	def dispatch Expression convertExpression(OrExpression e) {
		val leftAstType = astTypes.getType(e.left);
		val rightAstType = astTypes.getType(e.right);

		if (leftAstType.isBooleanElementaryType && rightAstType.isBooleanElementaryType) {
			// Both operands are Booleans --> logic OR expression
			return factory.or(convertExpressionWithTypeConv(e.left), convertExpressionWithTypeConv(e.right));
		} else if (!leftAstType.isBooleanElementaryType && !rightAstType.isBooleanElementaryType) {
			// None of the operands is Boolean --> bitwise OR expression
			return factory.createBinaryArithmeticExpression(
				convertExpressionWithTypeConv(e.left),
				convertExpressionWithTypeConv(e.right),
				BinaryArithmeticOperator.BITWISE_OR
			);
		}
		throw new PlcCodeToCfaRuntimeException(
			"Unable to decide whether the following OR expression is a logic or bitwise operation: " + e);
	}

	def dispatch Expression convertExpression(AndExpression e) {
		val leftAstType = astTypes.getType(e.left);
		val rightAstType = astTypes.getType(e.right);

		if (leftAstType.isBooleanElementaryType && rightAstType.isBooleanElementaryType) {
			// Both operands are Booleans --> logic AND expression
			return factory.and(convertExpressionWithTypeConv(e.left), convertExpressionWithTypeConv(e.right));
		} else if (!leftAstType.isBooleanElementaryType && !rightAstType.isBooleanElementaryType) {
			// None of the operands is Boolean --> bitwise AND expression
			return factory.createBinaryArithmeticExpression(
				convertExpressionWithTypeConv(e.left),
				convertExpressionWithTypeConv(e.right),
				BinaryArithmeticOperator.BITWISE_AND
			);
		}
		throw new PlcCodeToCfaRuntimeException(
			"Unable to decide whether the following AND expression is a logic or bitwise operation: " + e);
	}

	def dispatch Expression convertExpression(XorExpression e) {
		val leftAstType = astTypes.getType(e.left);
		val rightAstType = astTypes.getType(e.right);

		if (leftAstType.isBooleanElementaryType && rightAstType.isBooleanElementaryType) {
			// Both operands are Booleans --> logic XOR expression
			return factory.xor(convertExpressionWithTypeConv(e.left), convertExpressionWithTypeConv(e.right));
		} else if (!leftAstType.isBooleanElementaryType && !rightAstType.isBooleanElementaryType) {
			// None of the operands is Boolean --> bitwise XOR expression
			return factory.createBinaryArithmeticExpression(
				convertExpressionWithTypeConv(e.left),
				convertExpressionWithTypeConv(e.right),
				BinaryArithmeticOperator.BITWISE_XOR
			);
		}
		throw new PlcCodeToCfaRuntimeException(
			"Unable to decide whether the following XOR expression is a logic or bitwise operation: " + e);
	}
	
	def dispatch Expression convertExpression(EqualityExpression e) {
		return factory.createComparisonExpression(convertExpressionWithTypeConv(e.left), convertExpressionWithTypeConv(e.right), astToCfaComparisonOperator(e.operator))
	}
	
	def dispatch Expression convertExpression(ComparisonExpression e) {
		return factory.createComparisonExpression(
			convertExpressionWithTypeConv(e.left), 
			convertExpressionWithTypeConv(e.right), 
			astToCfaComparisonOperator(e.operator)
		);
	}

	def dispatch Expression convertExpression(AdditiveExpression e) {
		return factory.createBinaryArithmeticExpression(
			convertExpressionWithTypeConv(e.left),
			convertExpressionWithTypeConv(e.right),
			convertAdditiveOperator(e.operator)
		);
	}

	def dispatch Expression convertExpression(MultiplicativeExpression e) {
		return factory.createBinaryArithmeticExpression(
			convertExpressionWithTypeConv(e.left),
			convertExpressionWithTypeConv(e.right),
			convertMultiplicativeOperator(e.operator)
		);	
	}
	
	def dispatch Expression convertExpression(PowerExpression e) {
		return factory.pow(convertExpressionWithTypeConv(e.left), convertExpressionWithTypeConv(e.right))	
	}
	
	def dispatch Expression convertExpression(UnaryExpression e) {
		switch (e.op) {
			case NOT: {
				val astType = astTypes.tryGetType(e.expr);
				if (astType.isPresent()) {
					if (DataTypeUtil.isBooleanElementaryType(astType.get())) {
						// Logic not
						return factory.createUnaryLogicExpression(convertExpressionWithTypeConv(e.expr),
							UnaryLogicOperator.NEG)
					} else {
						// Bitwise not
						return factory.createUnaryArithmeticExpression(convertExpressionWithTypeConv(e.expr),
							UnaryArithmeticOperator.BITWISE_NOT)
					}
				} else {
					// Unknown type -- assume logic not
					return factory.createUnaryLogicExpression(convertExpressionWithTypeConv(e.expr),
						UnaryLogicOperator.NEG)
				}
			} 
			case MINUS:
				return factory.createUnaryArithmeticExpression(convertExpressionWithTypeConv(e.expr), UnaryArithmeticOperator.MINUS) 
			case PLUS: 
				return convertExpressionWithTypeConv(e.expr)
			default:
				throw new UnsupportedOperationException("Unhandled STEP 7 unary operator type: " + e.op)
		}
	}
	
	def dispatch Expression convertExpression(QualifiedRef e) {
		val prefixExpr = convertExpressionWithTypeConv(e.prefix);
		Preconditions.checkState(prefixExpr instanceof DataRef, "prefixExpr must be a DataRef");
		val prefixRef = prefixExpr as DataRef;
		
		val suffixExpr = convertExpression(e.ref); // TODO no type conversion here! (check if it can cause problems)
		Preconditions.checkState(suffixExpr instanceof DataRef, "suffixExpr must be a DataRef; problem with QualifiedRef " + e + "; its type is " + suffixExpr);
		val suffixRef = suffixExpr as DataRef;
		
		return CfaDeclarationUtils.concatenateDataRefs(prefixRef, suffixRef);
	}
	
	def dispatch Expression convertExpression(ArrayRef e) {
		if (MemoryAddressArrayUtil.isMemoryAddressArray(e)) {
			// Special handling for `M[0,1]`-like arrays.
			// Note that `M[var1, var2+3]` would NOT be treated by this special case as 
			// the indices cannot be evaluated to constants in compile time
			val field = astCfaVarTrace.getFieldForGlobalAddress(MemoryAddressArrayUtil.toMemoryAddress(e));
			return factory.createFieldRef(field);
		}
		
		val prefixExpr = convertExpressionWithTypeConv(e.ref);
		Preconditions.checkState(prefixExpr instanceof DataRef, "prefixExpr must be a DataRef");
		val prefixRef = prefixExpr as DataRef;
		
		var currentRef = prefixRef;
		for (astIndexExpr : e.index) {
			currentRef = factory.appendIndexing(currentRef, convertExpressionWithTypeConv(astIndexExpr));
		}
		
		return currentRef; 
	}
	
	def dispatch Expression convertExpression(DirectNamedRef e) {
		val referredElement = e.ref;
		switch (referredElement) {
			NamedConstantDeclaration: return convertExpressionWithTypeConv(referredElement.value) // add type here! (AST->CFA type conversion)
			Function: return factory.createFieldRef(astCfaVarTrace.getRetval(referredElement))
			Variable: return factory.createFieldRef(astCfaVarTrace.get(referredElement))
			DataBlock: return factory.createFieldRef(structuralTrace.singletonProgramUnitToField.get(referredElement))
			UserDefinedDataType: throw new UnsupportedOperationException(String.format("The reference to the UDT '%s' is illegal. A UDT cannot be used in a reference.", referredElement.name))
		}
		
		throw new UnsupportedOperationException("Unknown DirectNamedRef.ref: " + referredElement);
	}
	
	def dispatch Expression convertExpression(RetValRef e) {
		val enclosingFunction = EmfHelper.getContainerOfType(e, Function);
		if (enclosingFunction === null || DataTypeUtil.isVoid(enclosingFunction.returnType)) {
			throw new PlcCodeToCfaRuntimeException("Illegal use of RET_VAL: it is not within a non-VOID function.");
		} else {
			return factory.createFieldRef(astCfaVarTrace.getRetval(enclosingFunction));
		}
	}
	
	def DataRef convertLeftValue(LeftValue value) {
		val result = convertExpression(value);
		Preconditions.checkState(result instanceof DataRef, "Unexpected result: a left-value resulted in a something else than a data reference.");		
		return result as DataRef;
	}
		
	def dispatch Expression convertExpression(MemoryAddress e) {
		if (e.value.memoryIdentifier == S7AddressMemoryIdentifier.L) {
			// Local address -- local field
			val programUnit = EmfHelper.getContainerOfType(e, ExecutableProgramUnit);
			val field = astCfaVarTrace.getFieldForLocalAddress(programUnit, e.value);
			return factory.createFieldRef(field);
		} else {
			// Global field
			val field = astCfaVarTrace.getFieldForGlobalAddress(e.value);
			return factory.createFieldRef(field);
		}
	}
	
	def dispatch Expression convertExpression(NamedConstantDeclaration e) {
		// Named constant is just simply substituted with its value.
		return convertExpressionWithTypeConv(e.value);
	}
	
	def dispatch Expression convertExpression(SclSubroutineCall astExpr) {
		// special case: DT conversion
		if (Step7LanguageHelper.isTypeConversionFunc(astExpr) && astExpr.callParameter instanceof SingleCallParameter) {
			Preconditions.checkState(astExpr.calledUnit.name.toUpperCase.contains("_TO_"), "Suspicious conversion function: " + astExpr.calledUnit.name);
			return convertTypeConversionCall(astExpr, astExpr.callParameter as SingleCallParameter);
		}
		
		// We do not expect to have library function calls here. They were transformed already.
		
		throw new UnsupportedOperationException("Unable to treat a SclSubroutineCall as expression: " + astExpr.calledUnit.name);
	}
	
	private def TypeConversion convertTypeConversionCall(SclSubroutineCall typeConversionCall, SingleCallParameter singleParameter) {
		Preconditions.checkState(typeConversionCall.calledUnit.name.toUpperCase.contains("_TO_"));

		// try to determine the target type
		val astTargetType = astTypes.getType(typeConversionCall);
		Preconditions.checkNotNull(astTargetType, "Type conversion to an unknown target type.");
		Preconditions.checkState(astTargetType instanceof ElementaryDT, "Impossible to convert an expression to a non-elementary type.");
		val cfaTargetType = Step7TypeToExprType.s7typeToExprType(astTargetType as ElementaryDT);
		return factory.createTypeConversion(convertExpression(singleParameter.parameter), cfaTargetType);
	}
	
	private def Optional<Type> tryGetElementaryCfaType(cern.plcverif.plc.step7.step7Language.Expression astExpr) {
		val computedType = astTypes.tryGetType(astExpr);
		// This will return the nominal type, if exists (e.g. INT#123); or the expected type if there is no nominal type inferred (e.g. 123).
		
		if (!computedType.isPresent || DataTypeUtil.isAny(computedType.get) || DataTypeUtil.isAnyNum(computedType.get)) {
			// TODO check what to do with ANY or ANY_NUM
			return Optional.empty();
		}
		
		if (!(computedType.get instanceof ElementaryDT)) {
			throw new PlcCodeToCfaRuntimeException("It is expected to have an elementary AST data type in tryGetElementaryCfaType.");
		}

		val cfaType = Step7TypeToExprType.s7typeToExprType(computedType.get as ElementaryDT);
		return Optional.of(cfaType);
	}
	
	// Conversion helper methods
	def Expression convertRefToCfaExpression(NamedOrUnnamedConstantRef astRef) {
		val expr = Step7LanguageHelper.unwrapRefExpression(astRef, astTypes);
		return convertExpressionWithTypeConv(expr);
	}	
	
	
	static def BinaryArithmeticOperator convertAdditiveOperator(AdditionOperator operator) {
			switch (operator) {
			case PLUS: return BinaryArithmeticOperator.PLUS
			case MINUS: return BinaryArithmeticOperator.MINUS	
			default: throw new UnsupportedOperationException("Unknown addition operator.")
		}
	}
	
	def static BinaryArithmeticOperator convertMultiplicativeOperator(MultiplicationOperator operator) {
		switch (operator) {
			case MULTIPLICATION: return BinaryArithmeticOperator.MULTIPLICATION 
			case DIVISION: return BinaryArithmeticOperator.DIVISION
			case INTEGER_DIVISION: return BinaryArithmeticOperator.INTEGER_DIVISION	
			case MODULO: return BinaryArithmeticOperator.MODULO
			default: throw new UnsupportedOperationException("Unknown MultiplicationOperator.")
		}
	}
	
	def static ComparisonOperator astToCfaComparisonOperator(cern.plcverif.plc.step7.step7Language.ComparisonOperator astOp) {
		switch (astOp) {
			case GREATER_EQ: return ComparisonOperator.GREATER_EQ
			case GREATER_THAN: return ComparisonOperator.GREATER_THAN
			case LESS_EQ: return ComparisonOperator.LESS_EQ
			case LESS_THAN: return ComparisonOperator.LESS_THAN
			default: throw new UnsupportedOperationException("Unknown ComparisonOperator.")
		}
	}
	
	static def ComparisonOperator astToCfaComparisonOperator(EqualityOperator astOp) {
		switch (astOp) {
			case EQUALS: return ComparisonOperator.EQUALS
			case NOT_EQUALS: return ComparisonOperator.NOT_EQUALS
			default: throw new UnsupportedOperationException("Unknown EqualityOperator.")
		}
	}
	
	def representBounds(Field cfaField, VariableDeclarationLine varDecl) {
		if (varDecl.isBounded){
			Preconditions.checkState(cfaField.type instanceof ElementaryType)
			val lowerBoundLiteral = toLiteralList(varDecl.lowerBound, cfaField.type as ElementaryType)
			Preconditions.checkNotNull(lowerBoundLiteral)
			Preconditions.checkState(lowerBoundLiteral.size == 1,
				"An elementary field cannot be initialized with a list of initial values.")
			val lowerBound = lowerBoundLiteral.get(0)
			factory.createFieldBound(cfaField, lowerBound, ComparisonOperator.GREATER_EQ)
			
			val upperBoundLiteral = toLiteralList(varDecl.upperBound, cfaField.type as ElementaryType)
			Preconditions.checkNotNull(upperBoundLiteral)
			Preconditions.checkState(upperBoundLiteral.size == 1,
				"An elementary field cannot be initialized with a list of initial values.")
			val upperBound = upperBoundLiteral.get(0)
			factory.createFieldBound(cfaField, upperBound, ComparisonOperator.LESS_EQ)
		}
	}
	
	def representInitialization(Field cfaField, VariableDeclarationLine varDecl, boolean isNondet) {
		val cfaFieldType = cfaField.type;
		switch (cfaFieldType) {
			ElementaryType: representInitialization(cfaField, varDecl, cfaFieldType, isNondet)
			ArrayType: representInitialization(cfaField, varDecl, cfaFieldType, isNondet)
			DataType: Preconditions.checkState(!varDecl.isInitialized)
		}
	}

	def representInitialization(Field cfaField, VariableDeclarationLine varDecl, ElementaryType type, boolean isNondet) {
		// cfaField is an elementary field 
		if (varDecl.isInitialized) {
			// explicit initialization
			val initLiterals = toLiteralList(varDecl.initialization, type);
			Preconditions.checkNotNull(initLiterals);
			Preconditions.checkState(initLiterals.size == 1,
				"An elementary field cannot be initialized with a list of initial values.");
			val initLiteral = initLiterals.get(0);
			factory.createInitialAssignment(cfaField, initLiteral);
		} else {
			// implicit initialization
			val initialValue = if (isNondet) factory.createNondeterministic(EcoreUtil.copy(type)) else defaultLiteral(type);
			factory.createInitialAssignment(cfaField, initialValue);
		}
	}

	def static defaultLiteral(ElementaryType type) {
		switch (type) {
			BoolType: return factory.createBoolLiteral(false)
			IntType: return factory.createIntLiteral(0, EcoreUtil.copy(type))
			FloatType: return factory.createFloatLiteral(0, EcoreUtil.copy(type))
			StringType: return factory.createStringLiteral("", EcoreUtil.copy(type))
			UnknownType: throw new UnsupportedOperationException("No default literal for UnknownType.")
			default: throw new UnsupportedOperationException("Not implemented yet: " + type)
		}
	}

	def representInitialization(Field cfaField, VariableDeclarationLine varDecl, ArrayType type, boolean isNondet) {
		// cfaField is an array
		val baseType = getBaseType(type);
		if (baseType instanceof ElementaryType == false) {
			Preconditions.checkState(!varDecl.isInitialized, "Complex types cannot be initialized.");
			return;
		}

		val List<Literal> initLiterals = newArrayList();
		if (varDecl.isInitialized) {
			// explicit initialization
			initLiterals.addAll(toLiteralList(varDecl.initialization, baseType as ElementaryType));
			Preconditions.checkNotNull(initLiterals);
		} else {
			// implicit initialization
		}

		val List<DataRef> dataRefList = newArrayList();
		indexIteration(
			type,
			[indices|createIndexElementInitialization(cfaField, indices, dataRefList)]
		);

		for (var i = 0; i < dataRefList.size; i++) {
			val dataRef = dataRefList.get(i);
			var InitialValue initLiteral;
			if (initLiterals.size > i) {
				initLiteral = initLiterals.get(i);
			} else {
				initLiteral = if (isNondet) factory.createNondeterministic(EcoreUtil.copy(baseType as ElementaryType)) else defaultLiteral(baseType as ElementaryType);
			}
			factory.createComplexInitialAssignment(cfaField, dataRef, initLiteral);
		}
	}

	def createIndexElementInitialization(Field field, List<Integer> indices, List<DataRef> resultList) {
		var DataRef ref = factory.createFieldRef(field);
		Preconditions.checkState(indices.size > 0, "No index in createIndexElementInitialization");
		for (index : indices) {
			ref = factory.appendIndexing(ref, index);
		}
		resultList.add(ref);
	}

	private def void indexIteration(ArrayType type, Consumer<List<Integer>> consumer) {
		indexIteration(type, new Stack<Integer>(), consumer);
	}

	private def void indexIteration(ArrayType type, Stack<Integer> indices, Consumer<List<Integer>> consumer) {
		val elementType = type.elementType;
		if (elementType instanceof ArrayType) {
			for (i : type.dimension.lowerIndex .. type.dimension.upperIndex) {
				indices.push(i);
				indexIteration(elementType, indices, consumer);
				indices.pop();
			}
		} else {
			for (i : type.dimension.lowerIndex .. type.dimension.upperIndex) {
				indices.push(i);
				consumer.accept(indices);
				indices.pop();
			}
		}
	}

	def Type getBaseType(ArrayType type) {
		val elementType = type.elementType;
		switch (elementType) {
			ArrayType: return getBaseType(elementType)
			default: return elementType
		}
	}

	private def dispatch List<Literal> toLiteralList(VariableInitialization init, ElementaryType elementType) {
		throw new UnsupportedOperationException();
	}

	private def dispatch List<Literal> toLiteralList(ConstantInitializationElement init, ElementaryType elementType) {
		val List<Literal> ret = newArrayList();
		val initValue = Step7LanguageHelper.unwrapRefExpression(init.constant, this.astTypes);
		val expr = this.convertExpression(initValue);
		// TODO handle unknown types? + check for incompatible types
		Preconditions.checkState(expr instanceof Literal)
		ret.add(expr as Literal);
		return ret;
	}

	private def dispatch List<Literal> toLiteralList(InitializationRepetitionList init, ElementaryType elementType) {
		val List<Literal> ret = newArrayList();
		val times = SimpleExpressionUtil.evaluateSimpleExpression(init.times) as int;
		for (i : 1..times) {
			ret.addAll(toLiteralList(init.toRepeat, elementType));
		}
		return ret;
	}

	private def dispatch List<Literal> toLiteralList(ArrayInitialization init, ElementaryType elementType) {
		val List<Literal> ret = newArrayList();
		for (element : init.getElements()) {
			ret.addAll(toLiteralList(element, elementType));
		}
		return ret;
	}
}