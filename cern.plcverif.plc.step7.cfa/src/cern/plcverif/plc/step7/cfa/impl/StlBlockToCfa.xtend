/******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 * 	 Jean-Charles Tournier - Add support for shift operations	
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.impl

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.common.logging.IPlcverifLogger
import cern.plcverif.base.models.cfa.builder.SequentialAssignments
import cern.plcverif.base.models.cfa.builder.TransitionBuilder
import cern.plcverif.base.models.cfa.builder.VariableAssignmentSkeleton
import cern.plcverif.base.models.cfa.cfabase.DataDirection
import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.expr.BinaryArithmeticOperator
import cern.plcverif.base.models.expr.BoolLiteral
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.FloatType
import cern.plcverif.base.models.expr.IntLiteral
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.LibraryFunctions
import cern.plcverif.base.models.expr.Type
import cern.plcverif.plc.step7.cfa.exceptions.PlcCodeToCfaRuntimeException
import cern.plcverif.plc.step7.cfa.impl.BlockToCfa.PostponedTransitionCreation
import cern.plcverif.plc.step7.cfa.trace.AstCfaVarTrace
import cern.plcverif.plc.step7.cfa.trace.StructuralAstCfaTrace
import cern.plcverif.plc.step7.cfa.util.SiemensWordIndexingUtils
import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum
import cern.plcverif.plc.step7.step7Language.Label
import cern.plcverif.plc.step7.step7Language.LabeledStlStatement
import cern.plcverif.plc.step7.step7Language.LeftValue
import cern.plcverif.plc.step7.step7Language.NamedOrUnnamedConstantRef
import cern.plcverif.plc.step7.step7Language.OrganizationBlock
import cern.plcverif.plc.step7.step7Language.StatementList
import cern.plcverif.plc.step7.step7Language.StlAccuArglessStatement
import cern.plcverif.plc.step7.step7Language.StlAccuDecrementStatement
import cern.plcverif.plc.step7.step7Language.StlAccuIncrementStatement
import cern.plcverif.plc.step7.step7Language.StlAndBeforeOr
import cern.plcverif.plc.step7.step7Language.StlArithmeticMnemonic
import cern.plcverif.plc.step7.step7Language.StlArithmeticStatement
import cern.plcverif.plc.step7.step7Language.StlAssignStatement
import cern.plcverif.plc.step7.step7Language.StlBitLogicNestingCloseStatement
import cern.plcverif.plc.step7.step7Language.StlBitLogicNestingOpenStatement
import cern.plcverif.plc.step7.step7Language.StlBitLogicOpMnemonic
import cern.plcverif.plc.step7.step7Language.StlBitLogicStatement
import cern.plcverif.plc.step7.step7Language.StlBldStatement
import cern.plcverif.plc.step7.step7Language.StlBlockEndStatement
import cern.plcverif.plc.step7.step7Language.StlClrRloStatement
import cern.plcverif.plc.step7.step7Language.StlComparisonDataType
import cern.plcverif.plc.step7.step7Language.StlComparisonMnemonic
import cern.plcverif.plc.step7.step7Language.StlComparisonStatement
import cern.plcverif.plc.step7.step7Language.StlConversionStatement
import cern.plcverif.plc.step7.step7Language.StlFloatMathFunction
import cern.plcverif.plc.step7.step7Language.StlFloatMathFunctionMnemonic
import cern.plcverif.plc.step7.step7Language.StlFnStatement
import cern.plcverif.plc.step7.step7Language.StlFpStatement
import cern.plcverif.plc.step7.step7Language.StlJumpStatement
import cern.plcverif.plc.step7.step7Language.StlLoadStatement
import cern.plcverif.plc.step7.step7Language.StlLoadStwStatement
import cern.plcverif.plc.step7.step7Language.StlMcrStatement
import cern.plcverif.plc.step7.step7Language.StlModStatement
import cern.plcverif.plc.step7.step7Language.StlNegateRloStatement
import cern.plcverif.plc.step7.step7Language.StlNetwork
import cern.plcverif.plc.step7.step7Language.StlNopStatement
import cern.plcverif.plc.step7.step7Language.StlParameterlessCallMnemonic
import cern.plcverif.plc.step7.step7Language.StlParameterlessCallStatement
import cern.plcverif.plc.step7.step7Language.StlResetStatement
import cern.plcverif.plc.step7.step7Language.StlSaveRloStatement
import cern.plcverif.plc.step7.step7Language.StlSetRloStatement
import cern.plcverif.plc.step7.step7Language.StlSetStatement
import cern.plcverif.plc.step7.step7Language.StlStatement
import cern.plcverif.plc.step7.step7Language.StlStatementList
import cern.plcverif.plc.step7.step7Language.StlSubroutineCall
import cern.plcverif.plc.step7.step7Language.StlTransferStatement
import cern.plcverif.plc.step7.step7Language.StlTransferStwStatement
import cern.plcverif.plc.step7.step7Language.StlVerificationAssertion
import cern.plcverif.plc.step7.step7Language.StlWordLogicArglessStatement
import cern.plcverif.plc.step7.step7Language.StwBitRef
import cern.plcverif.plc.step7.step7Language.SubroutineCall
import cern.plcverif.plc.step7.typecomputer.Step7TypeComputer
import cern.plcverif.plc.step7.util.SimpleExpressionUtil
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import cern.plcverif.plc.step7.util.StlHelper
import cern.plcverif.plc.step7.util.StlHelper.NestingStackBit
import cern.plcverif.plc.step7.util.StlHelper.NestingStackFunctionCode
import cern.plcverif.plc.step7.util.StlSemanticsHelper
import com.google.common.base.Preconditions
import java.util.Collections
import java.util.List
import java.util.Map
import java.util.Objects

import static cern.plcverif.plc.step7.util.StlHelper.StatusBit.*
import static org.eclipse.emf.ecore.util.EcoreUtil.copy
import cern.plcverif.plc.step7.step7Language.StlJumpMnemonic
import cern.plcverif.plc.step7.step7Language.StlConversionMnemonic
import cern.plcverif.plc.step7.step7Language.StlBlockEndMnemonic
import cern.plcverif.plc.step7.step7Language.StlShiftRotateStatement
import cern.plcverif.plc.step7.step7Language.StlJumpList
import cern.plcverif.plc.step7.step7Language.CallParameterList
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.util.DataTypeUtil

/**
 * Class for the STL statement list representation in CFA.
 */
class StlBlockToCfa extends BlockToCfa {
	/**
	 * The STL statements to be represented by this object in the {@link BlockToCfa#getAutomaton()}.
	 * Equals to {@link BlockToCfa#statementsOfBlock}, only their types are different.
	 */
	StlStatementList stlStatementsOfBlock;
	
	Map<NestingStackEntry, Field> nestingStackInCfa;
	
	StlGlobalData stlGlobalData;
	
	static class NestingStackEntry {
		StlHelper.NestingStackBit bit;
		int depth;
		
		public static int TOPMOST_ENTRY_INDEX = 1;
		public static int NESTING_STACK_DEPTH = 7;
		public static int DEEPEST_ENTRY_INDEX = TOPMOST_ENTRY_INDEX + NESTING_STACK_DEPTH - 1;
		
		private new(StlHelper.NestingStackBit bit, int depth) {
			this.bit = bit;
			this.depth = depth;
		}
		
		static def NestingStackEntry of(StlHelper.NestingStackBit bit, int depth) {
			return new NestingStackEntry(bit, depth);
		}  
		
		static def NestingStackEntry top(StlHelper.NestingStackBit bit) {
			return new NestingStackEntry(bit, TOPMOST_ENTRY_INDEX);
		}
		
		override equals(Object obj) {
			if (obj instanceof NestingStackEntry) {
				return obj.bit == this.bit && obj.depth == this.depth;
			}
			return false;
		}
		
		override hashCode() {
			return Objects.hash(bit, depth);
		}
	}
	
	new(StlStatementList statementsOfBlock, AutomatonDeclaration representingAutomaton, AstCfaVarTrace astCfaVarTrace, Step7TypeComputer astTypes, StructuralAstCfaTrace structuralTrace, StlGlobalData stlGlobalData, IPlcverifLogger logger) {
		super(statementsOfBlock, representingAutomaton, astCfaVarTrace, astTypes, structuralTrace, logger);
		this.stlGlobalData = stlGlobalData;
		this.stlStatementsOfBlock = statementsOfBlock;
	}
	
	protected override beforeConversion() {
		super.beforeConversion();
		
		// Create variables for nesting stack (if there is any nesting statement)
		if (StlHelper.containsNestingBoolOps(stlStatementsOfBlock)) {
			this.nestingStackInCfa = newHashMap();
			for (bit : StlHelper.NestingStackBit.values) {
				for (depth : NestingStackEntry.TOPMOST_ENTRY_INDEX .. NestingStackEntry.DEEPEST_ENTRY_INDEX) {
					val cfaFieldName = "__" + bit.name + "_" + depth; // TODO improve naming (ensure uniqueness)
					val cfaType = factory.createBoolType();
					val cfaField = factory.createField(cfaFieldName, representingAutomaton.localDataStructure.definition,
						cfaType);
					factory.createDirectionFieldAnnotation(cfaField, DataDirection.TEMP);
					factory.createInitialAssignment(cfaField, Step7ExprToCfaExpr.defaultLiteral(cfaType));
					this.nestingStackInCfa.put(NestingStackEntry.of(bit, depth), cfaField);
				}
			}
		}
	}

	/**
	 * Converts the given STL statement list into locations and transitions, starting from the location {@code start}.
	 */
	protected def dispatch Location convertList(StatementList e, Location start) {
		throw new UnsupportedOperationException('''Unhandled statement list type: '«e.class.simpleName»'.''');
	}

	protected def dispatch Location convertList(Void e, Location start) {
		// to support empty statement lists
		return start;
	}

	/**
	 * Converts the given STL statement list into locations and transitions, starting from the location {@code start}.
	 */
	protected def dispatch Location convertList(StlStatementList e, Location start) {
		var previousLoc = start;
		
		if (EmfHelper.getContainerOfType(e, OrganizationBlock) !== null) {
			// In an OB --> clear STW
			previousLoc = sequentialAssignmentsFrom(start)
				.independentAssignments(
					amt(OS.ref, falseLiteral()),
					amt(OV.ref, falseLiteral()),
					amt(OR.ref, falseLiteral()),
					amt(STA.ref, falseLiteral()),
					amt(RLO.ref, falseLiteral()),
					amt(CC0.ref, falseLiteral()),
					amt(CC1.ref, falseLiteral()),
					amt(BR.ref, falseLiteral()),
					amt(NFC.ref, falseLiteral())  
				)
				.endLocation();
		}
		
		for (network : e.networks) {
			// Network initialization
			previousLoc = convertNetworkInit(network, previousLoc);
			
			// Network content handling
			for (statement : network.statements) {
				previousLoc = convert(statement, previousLoc);
				Preconditions.checkState(previousLoc !== null);
			}
		}
		return previousLoc;
	}
	
	private def Location convertNetworkInit(StlNetwork network, Location start) {
		// As checked on simulator, nothing is modified at NETWORK
		return sequentialAssignmentsFrom(start)
//			.independentAssignments(
//				amt(OR.ref, falseLiteral()),
//				amt(STA.ref, trueLiteral()),
//				amt(RLO.ref, trueLiteral()),
//				amt(CC0.ref, falseLiteral()),
//				amt(CC1.ref, falseLiteral()),
//				amt(BR.ref, falseLiteral()),
//				amt(NFC.ref, falseLiteral())  
//			)
			.nestingStackEntriesToFalse()
				// This is not necessary, but helps the reductions 
			.endLocation();
	}

	/**
	 * Generates CFA representation for the given {@link LabeledStlStatement} labeled statement.
	 * The given start location will be registered for its label for future use. It will also be
	 * annotated with {@link cern.plcverif.base.models.cfa.cfabase.LabelledLocationAnnotation}.
	 * The STL statement that is labeled will be represented according to its type. 
	 */
	protected def dispatch Location convert(LabeledStlStatement e, Location start) {
		// save label that labels Location 'start'
		registerLabelForLocation(e.label, start);

		// create annotation
		factory.createLabelledLocationAnnotation(start, e.label.name);

		// handle generic case
		val endLoc = convert(e.statement, start);

		return endLoc;
	}
	

	/**
	 * Method to handle the conversion of unsupported STL statement types.
	 * 
	 * Certain STL statements cannot be handled by PLCverif currently, but instead of breaking the CFA generation, they will be skipped and warnings will be generated.
	 * 
	 * It provides specific diagnostic information for the unknown or unhandled {@link StlStatement} type. 
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlStatement e, Location start) {
		switch (e) {
			StlMcrStatement: {
				handleUnsupportedStatement(e, e.mnemonic.literal);
				return start;
			}
			StlBlockEndStatement: {
				handleUnsupportedStatement(e, e.mnemonic.literal);
				return start;
			}
			StlAccuArglessStatement: {
				handleUnsupportedStatement(e, e.mnemonic.literal);
				return start;
			}
			StlLoadStwStatement: {
				handleUnsupportedStatement(e, "L STW");
				return start;
			}
			StlTransferStwStatement: {
				handleUnsupportedStatement(e, "T STW");
				return start;
			}
		}
		
		throw new PlcCodeToCfaRuntimeException("Unhandled STL statement type: '%s'.", e.class.name);
	}
	
	protected def void handleUnsupportedStatement(StlStatement e, String statementName) {
		log.logWarning('''The unsupported «statementName» STL statement has been skipped.''');
	}
	
	
	/**
	 * Representation of the {@code SSI, SSD, SLW, SRW, SLD, SRD, RLD, RRD, RLDA, RRDA} STL statements.
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert( StlShiftRotateStatement e, Location start)
	{
		switch (e.mnemonic) {
			case SHIFT_RIGHT_SIGN_INT: { //SSI
				return convertSsi(e, start);		
			}
			case SHIFT_RIGHT_SIGN_DINT: { //SSD
				return convertSsd(e, start);
			}
			case SHIFT_LEFT_WORD: { //SLW
				return convertSlw(e, start);
			}
			case SHIFT_RIGHT_WORD: { //SRW
				return convertSrw(e, start);
			}
			case SHIFT_LEFT_DWORD: { //SLD
				return convertSld(e, start);
			}
			case SHIFT_RIGHT_DWORD: { //SRD
				return convertSrd(e, start);
			}
			case ROTATE_LEFT_DWORD: { //RLD
				return convertRld(e, start);
			}
			case ROTATE_RIGHT_DWORD: { //RRD
				convertRrd(e, start);
			}
			case ROTATE_LEFT_DWORD_VIA_CC1: { //RLDA
				convertRlda(e, start);
			}
			case ROTATE_RIGHT_DWORD_VIA_CC1: { //RRDA
				convertRrda(e, start);
			}
			default: {
				throw new PlcCodeToCfaRuntimeException("Unhandled STL Shift/Rotate statement: '%s'.", e.mnemonic.literal);
			}
		}
	}
	
	private def Location convertSsi(StlShiftRotateStatement statement, Location location) {
		throw new PlcCodeToCfaRuntimeException("Unhandled STL Shift/Rotate statement: '%s'.", statement.mnemonic.literal);
	}
	
	private def Location convertSsd(StlShiftRotateStatement statement, Location location) {
		throw new PlcCodeToCfaRuntimeException("Unhandled STL Shift/Rotate statement: '%s'.", statement.mnemonic.literal);
	}
	
	/**
	 * Representation of the the {@code SLW} STL statement ({@link StlSlwStatement}).
	 * 
	 * Depending of the presence or no of an argument to SLW, the max shifting is either 15 or 16
	 * 
	 * @param statement STL statement to be represented
	 * @param location Start location
	 * @return The end of the representation where the succeeding statement should start.
	 *
	 */
	private def Location convertSlw(StlShiftRotateStatement statement, Location location) {
		var long maxShift;
		var Expression arg;
		if ( statement.arg !== null)
		{
			 arg = factory.createIntLiteral(SimpleExpressionUtil.evaluateSimpleExpression(statement.arg), copy(accu1.type) as IntType);
			 maxShift = 15;
		} else
		{
			arg = factory.bwAnd(accu2.ref, accuTypeIntLiteral(StlGlobalData.ACCU_LOWLOW_MASK));
			maxShift = 16;
		}
		return convertShiftGeneric( arg, location, maxShift, 
					getSlwImplRepresentationForCc1(arg), getSlwImplRepresentationForAccu1(arg), bwAnd(accu1.ref, accuTypeIntLiteral(StlGlobalData.ACCU_HIGH_MASK))
		);
	}
	
	
	/**
	 * Generic representation of the shift operation 
	 * 
	 * Equivalent to:
	 * <pre>
	 * 	IF {ARG} = 0 THEN RETURN NOP
	 *  IF {ARG} <= {MAXSHIFT} THEN
	 * 		{ACCU1} := {ACCU1} SHIFT {ARG}
	 * 		{CC1} := getLastShiftedBit()
	 *		{CC0}, {OV} := FALSE
	 * 	ELSE
	 * 		{ACCU1} := 0
	 *		{CC1}, {CC0}, {OV} := FALSE
	 *  ENDIF
	 * </pre>
	 * Note that this is not exactly what is specified in the documentation
	 * as in the case of a shift statement with a literal, it is not specified 
	 * what should be done in case the literal is greater than the allowed value.
	 * We are treating it here the same as the statement w/o argument
	 *
	 * @param arg Expression: this is either the literal passed as argument to the shift operator or ACCU2-L-L if no argument is present
	 * @param location Location: this is the start location
	 * @param maxShift Long: maximal number allowed for shifting (typically 15 or 16 for SLW and SRW)
	 * @param cc1Exp Expression: BinaryArithmeticExpression representing the updated value of CC1
	 * @param accu1Exp Expression: BinaryArithmeticExpression representing the updated value of ACCU1
	 * @param accu1Reset Expression: Expression representing the updated value of ACCU1	 in case of shift higher than maxShift
	 * <p>
	 */
	private def Location convertShiftGeneric(Expression arg, Location location, long maxShift, Expression cc1Exp, Expression accu1Exp, Expression accu1Reset){
		val endLoc = createUniqueLocation(representingAutomaton);
		val thenStartTest0 = createUniqueLocation(representingAutomaton);
		val elseStartTest0 = createUniqueLocation(representingAutomaton);
		val thenStartTestMaxShift = createUniqueLocation(representingAutomaton);
		val elseStartTestMaxShift = createUniqueLocation(representingAutomaton);
		
	 	//Create the first if-then-else to check if the shift is equal to 0
		val Transition ifZeroThen = createIfElseTransitions(location, eq(arg, factory.createIntLiteral(0, copy(accu1.type as IntType))), thenStartTest0, elseStartTest0);
		
		//Create the second if-then-else to check if the shift is greater than maxShift
		val Transition ifLessThanMaxThen = createIfElseTransitions(elseStartTest0, lt(arg, maxShift), thenStartTestMaxShift, elseStartTestMaxShift);
	
		// then branch (thenStartTest0 --> endLoc) => regarded as a NOP operation
		TransitionBuilder.builderBetween(thenStartTest0, endLoc)
			.name(nextTransName())
			.condition(trueLiteral())
			.build();
		
		// Annotate the transitions with IF location annotations.
		factory.createIfLocationAnnotation(location, endLoc, ifZeroThen);
		factory.createIfLocationAnnotation(elseStartTest0, endLoc, ifLessThanMaxThen);
		
		// else branch (thenStartTestMaxShift) --> (endLoc)) => do the actual shifting  
		TransitionBuilder.builderBetween(thenStartTestMaxShift, endLoc)
			.name(nextTransName())
			.condition(trueLiteral())
			.addAssignment(amt(accu1.ref, accu1Exp))
			.addAssignment(amt(CC1.ref, cc1Exp))
			.addAssignment(amt(OV.ref, falseLiteral))
			.addAssignment(amt(CC0.ref, falseLiteral))
			.build();
			
		// else branch (elseStartTestMaxShift) --> (endLoc)) => update the status bit if shifting is more than maxShift  
		TransitionBuilder.builderBetween(elseStartTestMaxShift, endLoc)
			.name(nextTransName())
			.condition(trueLiteral())
			.addAssignment(amt(accu1.ref, accu1Reset)) 
			.addAssignment(amt(OV.ref, falseLiteral))
			.addAssignment(amt(CC0.ref, falseLiteral))
			.addAssignment(amt(CC1.ref, falseLiteral))
			.build();
			
		return endLoc;	
	}
	
	/**
	* Give the actual implementation representation of CC1 for the SLW statement
	*
	*	@param arg Expression: expression representing the shift number either as an literal or as ACCU2
	* @return The BinaryArithmeticExpression to extract the last shifted bit
	*/
	private def Expression getSlwImplRepresentationForCc1(Expression arg)
	{
		// represent accuLow=DW#0000_FFFF, accuBit1=DW#0000_0001
		val accuLow = accuTypeIntLiteral(StlGlobalData.ACCU_LOW_MASK);
	 	val accuBit1 = accuTypeIntLiteral(StlGlobalData.ACCU_BIT1_MASK);
	 	
	 	// CC1 = (((ACCU1 & ACCU_LOW_MASK) << arg) >> 16 ) & ACCU_LOW_MASK_BIT1
	 	return bwAnd(
					factory.bwShiftRight(
						factory.bwShiftLeft( 
							bwAnd(accu1.ref, accuLow) , 
								arg
						)
						, factory.createIntLiteral(16, copy(accu1.type) as IntType)
					)				
					, accuBit1
				);
	}
	
	/**
	* Give the actual implementation representation of ACCU1 for the SLW statement
	*
	*	@param arg Expression: expression representing the shift number either as an literal or as ACCU2
	* @return The BinaryArithmeticExpression to shift ACCU1-L
	*/
	private def Expression getSlwImplRepresentationForAccu1(Expression arg)
	{
		// represent accuLow=DW#0000_FFFF, accuHigh=DW#FFFF_0000
		val accuLow = accuTypeIntLiteral(StlGlobalData.ACCU_LOW_MASK);
	 	val accuHigh = accuTypeIntLiteral(StlGlobalData.ACCU_HIGH_MASK);
	 		 	
	 	// ACCU1 = (ACCU1 & ACCU_HIGH_MASK) | ( ( (ACCU1 & ACCU_LOW_MASK) << arg) & ACCU_LOW_MASK )
	 	return bwOr(
					bwAnd(accu1.ref, accuHigh),
					bwAnd(
							factory.bwShiftLeft( 
								bwAnd(accu1.ref, accuLow) , 
								arg
							) 
						, accuLow)
				);
	}
	
	/**
	 * Representation of the the {@code SRW} STL statement ({@link StlSrwStatement}).
	 * 
	 * Depending of the presence or no of an argument to SLW, the max shifting is either 15 or 16
	 * 
	 * @param statement STL statement to be represented
	 * @param location Start location
	 * @return The end of the representation where the succeeding statement should start.
	 *
	 */
	private def Location convertSrw(StlShiftRotateStatement statement, Location location) {
		var long maxShift;
		var Expression arg;
		if ( statement.arg !== null)
		{
			 arg = factory.createIntLiteral(SimpleExpressionUtil.evaluateSimpleExpression(statement.arg), copy(accu1.type) as IntType);
			 maxShift = 15;
		} else
		{
			arg = factory.bwAnd(accu2.ref, accuTypeIntLiteral(StlGlobalData.ACCU_LOWLOW_MASK));
			maxShift = 16;
		}
		return convertShiftGeneric( arg, location, maxShift, 
					getSrwImplRepresentationForCc1(arg), getSrwImplRepresentationForAccu1(arg), bwAnd(accu1.ref, accuTypeIntLiteral(StlGlobalData.ACCU_HIGH_MASK)));
	}
	
	/**
	* Give the actual implementation representation of CC1 for the SRW statement
	*
	*	@param arg Expression: expression representing the shift number either as an literal or as ACCU2
	* @return The BinaryArithmeticExpression to extract the last shifted bit
	*/
	private def Expression getSrwImplRepresentationForCc1(Expression arg)
	{
		// represent accuLow=DW#0000_FFFF, accuBit1=DW#0000_0001
		val accuLow = accuTypeIntLiteral(StlGlobalData.ACCU_LOW_MASK);
	 	val accuBit1 = accuTypeIntLiteral(StlGlobalData.ACCU_BIT1_MASK);
	 	
	 	// CC1 = (((ACCU1 & ACCU_LOW_MASK) >> (arg-1))) & ACCU_LOW_MASK_BIT1
	 	return bwAnd(
					factory.bwShiftRight( 
						bwAnd(accu1.ref, accuLow)
						,  
						factory.minus(arg,factory.createIntLiteral(1, copy(accu1.type) as IntType))
					)
					,										
					accuBit1
				);
	}
	
	/**
	* Give the actual implementation representation of ACCU1 for the SRW statement
	*
	*	@param arg Expression: expression representing the shift number either as an literal or as ACCU2
	* @return The BinaryArithmeticExpression to shift ACCU1-L
	*/
	private def Expression getSrwImplRepresentationForAccu1(Expression arg)
	{
		// represent accuLow=DW#0000_FFFF, accuHigh=DW#FFFF_0000
		val accuLow = accuTypeIntLiteral(StlGlobalData.ACCU_LOW_MASK);
	 	val accuHigh = accuTypeIntLiteral(StlGlobalData.ACCU_HIGH_MASK);
	 		 	
	 	// ACCU1 = (ACCU1 & ACCU_HIGH_MASK) | ( ( (ACCU1 & ACCU_LOW_MASK) >> arg) & ACCU_LOW_MASK )
	 	return bwOr(
					bwAnd(accu1.ref, accuHigh),
					bwAnd( factory.bwShiftRight( 
								bwAnd(accu1.ref, accuLow), 
								arg) 
						, accuLow)
					);
	}
	
	/**
	 * Representation of the the {@code SLD} STL statement ({@link StlSldStatement}).
	 * 
	 * @param statement STL statement to be represented
	 * @param location Start location
	 * @return The end of the representation where the succeeding statement should start.
	 *
	 */
	private def Location convertSld(StlShiftRotateStatement statement, Location location) {
		val long maxShift = 32;
		var Expression arg;
		if ( statement.arg !== null)
		{
			 arg = factory.createIntLiteral(SimpleExpressionUtil.evaluateSimpleExpression(statement.arg), copy(accu1.type) as IntType);
		} else
		{
			arg = factory.bwAnd(accu2.ref, accuTypeIntLiteral(StlGlobalData.ACCU_LOWLOW_MASK));
		}
		return convertShiftGeneric( arg, location, maxShift, 
					getSldImplRepresentationForCc1(arg), getSldImplRepresentationForAccu1(arg), factory.createIntLiteral(0x00000000, copy(accu1.type) as IntType));
	}
	
	/**
	* Give the actual implementation representation of CC1 for the SLD statement
	*
	* @param arg Expression: expression representing the shift number either as an literal or as ACCU2
	* @return The BinaryArithmeticExpression to extract the last shifted bit
	*/
	private def Expression getSldImplRepresentationForCc1(Expression arg)
	{
		// accuBit1=DW#0000_0001
	 	val accuBit1 = accuTypeIntLiteral(StlGlobalData.ACCU_BIT1_MASK);
	 	
	 	// CC1 = ((ACCU1 << (arg-1)) >> 31 ) & ACCU_LOW_MASK_BIT1
	 	// Equivalent to CC1 = (ACCU1 >> (32-arg1)) & ACCU_LOW_MASK_BIT1
	 	return bwAnd(
					factory.bwShiftRight( accu1.ref, 
						factory.minus( factory.createIntLiteral(32, copy(accu1.type) as IntType), arg)
							)
					, accuBit1
				);
	}
	
	/**
	* Give the actual implementation representation of ACCU1 for the SLD statement
	*
	*@param arg Expression: expression representing the shift number either as an literal or as ACCU2
	* @return The BinaryArithmeticExpression to shift ACCU1
	*/
	private def Expression getSldImplRepresentationForAccu1(Expression arg)
	{
	 	// ACCU1 = (ACCU1  << arg) 
	 	return factory.bwShiftLeft( accu1.ref, arg);
	}
	
	/**
	 * Representation of the the {@code SRD} STL statement ({@link StlSrdStatement}).
	 * 
	 * @param statement STL statement to be represented
	 * @param location Start location
	 * @return The end of the representation where the succeeding statement should start.
	 *
	 */
	private def Location convertSrd(StlShiftRotateStatement statement, Location location) {
		val long maxShift = 32;
		var Expression arg;
		if ( statement.arg !== null)
		{
			 arg = factory.createIntLiteral(SimpleExpressionUtil.evaluateSimpleExpression(statement.arg), copy(accu1.type) as IntType);
		} else
		{
			arg = factory.bwAnd(accu2.ref, accuTypeIntLiteral(StlGlobalData.ACCU_LOWLOW_MASK));
		}
		return convertShiftGeneric( arg, location, maxShift, 
					getSrdImplRepresentationForCc1(arg), getSrdImplRepresentationForAccu1(arg), factory.createIntLiteral(0x0, copy(accu1.type) as IntType));
	}
	
	/**
	* Give the actual implementation representation of CC1 for the SLD statement
	*
	* @param arg Expression: expression representing the shift number either as an literal or as ACCU2
	* @return The BinaryArithmeticExpression to extract the last shifted bit
	*/
	private def Expression getSrdImplRepresentationForCc1(Expression arg)
	{
		// accuBit1=DW#0000_0001
	 	val accuBit1 = accuTypeIntLiteral(StlGlobalData.ACCU_BIT1_MASK);
	 	
	 	// CC1 = ((ACCU1 >> (arg-1))) & ACCU_LOW_MASK_BIT1
	 	return bwAnd(
					factory.bwShiftRight(
								accu1.ref, 
								factory.minus(arg, factory.createIntLiteral(1, copy(accu1.type) as IntType))
							), 
					accuBit1
				);
	}
	
	/**
	* Give the actual implementation representation of ACCU1 for the SRD statement
	*
	*@param arg Expression: expression representing the shift number either as an literal or as ACCU2
	* @return The BinaryArithmeticExpression to shift ACCU1
	*/
	private def Expression getSrdImplRepresentationForAccu1(Expression arg)
	{
	 	// ACCU1 = (ACCU1  >> arg) 
	 	return factory.bwShiftRight(accu1.ref, arg);
	}
	
	private def Location convertRld(StlShiftRotateStatement statement, Location location) {
		throw new PlcCodeToCfaRuntimeException("Unhandled STL Shift/Rotate statement: '%s'.", statement.mnemonic.literal);
	}
	
	private def Location convertRrd(StlShiftRotateStatement statement, Location location) {
		throw new PlcCodeToCfaRuntimeException("Unhandled STL Shift/Rotate statement: '%s'.", statement.mnemonic.literal);
	}
	
	private def Location convertRlda(StlShiftRotateStatement statement, Location location) {
		throw new PlcCodeToCfaRuntimeException("Unhandled STL Shift/Rotate statement: '%s'.", statement.mnemonic.literal);
	}
	
	private def Location convertRrda(StlShiftRotateStatement statement, Location location) {
		throw new PlcCodeToCfaRuntimeException("Unhandled STL Shift/Rotate statement: '%s'.", statement.mnemonic.literal);
	}
	
	/**
	 * Representation of the {@code SET} STL statement.
	 * Equivalent to:
	 * <pre>
	 *   {RLO} := TRUE; 
	 *   {STA} := TRUE;
	 *   {OR} := FALSE;
	 *   {nFC} := FALSE;
	 * </pre>
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlSetRloStatement e, Location start) {
		return sequentialAssignmentsFrom(start)
			.independentAssignments(
				amt(RLO.ref, trueLiteral()),
				amt(STA.ref, trueLiteral()),
				amt(OR.ref, falseLiteral()),
				amt(NFC.ref, falseLiteral())
			)
			.endLocation();
	}
	
	/**
	 * Representation of the {@code CLR} STL statement.
	 * Equivalent to:
	 * <pre>
	 *   {RLO} := FALSE; 
	 *   {STA} := FALSE;
	 *   {OR} := FALSE;
	 *   {nFC} := FALSE;
	 * </pre>
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlClrRloStatement e, Location start) {
		return sequentialAssignmentsFrom(start)
			.independentAssignments(
				amt(RLO.ref, falseLiteral()),
				amt(STA.ref, falseLiteral()),
				amt(OR.ref, falseLiteral()),
				amt(NFC.ref, falseLiteral())
			)
			.endLocation();
	}
	
	/**
	 * Representation of the {@code SAVE} STL statement.
	 * Equivalent to:
	 * <pre>
	 *   {BR} := {RLO}; 
	 * </pre>
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlSaveRloStatement e, Location start) {
		return sequentialAssignmentsFrom(start)
			.assignment(BR.ref, RLO.ref)
			.endLocation();
	}
	
	/**
	 * Representation of the {@code NOT} STL statement.
	 * Equivalent to:
	 * <pre>
	 *   {RLO} := NOT {RLO};
	 *   {STA} := TRUE;
	 * </pre>
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlNegateRloStatement e, Location start) {
		return sequentialAssignmentsFrom(start)
			.independentAssignments(
				amt(RLO.ref, not(RLO.ref)),
				amt(STA.ref, trueLiteral)
			)
			.endLocation();
	}
	
	/**
	 * Representation of the {@code # argument} STL statement, where # can be A, AN, O, ON, X, or XOR.
	 * 
	 * Equivalent to:
	 * <br>
	 * For {@code #}={@code A}:
	 * <pre>
	 *  {RLO} := (NOT {nFC} OR {RLO}) AND (<i>argument</i> OR {OR});
	 *  {OR} := {OR} AND {nFC};
	 *  {STA} := <i>argument</i>;
	 *  {nFC} := TRUE;
	 * </pre>
	 * 
	 * For {@code #}={@code AN}:
	 * <pre>
	 *  {RLO} := (NOT {nFC} OR {RLO}) AND (NOT <i>argument</i> OR {OR});
	 *  {OR} := {OR} AND {nFC};
	 *  {STA} := <i>argument</i>;
	 *  {nFC} := TRUE;
	 * </pre>
	 * 
	 * For {@code #}={@code O}:
	 * <pre>
	 *  {RLO} := ({nFC} AND {RLO}) OR <i>argument</i>;
	 *  {OR} := FALSE;
	 *  {STA} := <i>argument</i>;
	 *  {nFC} := TRUE;
	 * </pre>
	 *  
	 * For {@code #}={@code ON}:
	 * <pre>
	 *  {RLO} := ({nFC} AND {RLO}) OR NOT <i>argument</i>;
	 *  {OR} := FALSE;
	 *  {STA} := <i>argument</i>;
	 *  {nFC} := TRUE;
	 * </pre>
	 *  
	 * For {@code #}={@code X}:
	 * <pre>
	 *  {RLO} := ({nFC} AND {RLO}) XOR <i>argument</i>;
	 *  {OR} := FALSE;
	 *  {STA} := <i>argument</i>;
	 *  {nFC} := TRUE;
	 * </pre>
	 *   
	 * For {@code #}={@code XN}:
	 * <pre>
	 *  {RLO} := ({nFC} AND {RLO}) XOR NOT <i>argument</i>;
	 *  {OR} := FALSE;
	 *  {STA} := <i>argument</i>;
	 *  {nFC} := TRUE;
	 * </pre>
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlBitLogicStatement e, Location start) {
		val Expression argRef = convertStlExpression(e.arg);

		var Expression newRloValue = null;		
		var Expression newOrValue = null;
		
		switch (e.mnemonic) {
			case AND: {
				// RLO := [not NFC or RLO] AND [argument or OR];
				newRloValue = factory.and(
					factory.or(not(NFC.ref), RLO.ref), 
					factory.or(argRef, OR.ref)
				);
				// OR := OR and NFC
				newOrValue = and(OR.ref, NFC.ref);
			}
			case AND_NOT: {
				// RLO := [not NFC or RLO] AND [not argument or OR];
				newRloValue = and(
					or(not(NFC.ref), RLO.ref), 
					or(not(argRef), OR.ref)
				);
				// OR := OR and NFC
				newOrValue = and(OR.ref, NFC.ref);
			}
			case OR: {
				// RLO := [NFC and RLO] or [argument];
				newRloValue = or(
					and(NFC.ref, RLO.ref), 
					argRef
				);
				// OR := FALSE
				newOrValue = falseLiteral();
			}
			case OR_NOT: {
				// RLO := [NFC and RLO] or [not argument];
				newRloValue = or(
					and(NFC.ref, RLO.ref),
					not(argRef)
				);
				// OR := FALSE
				newOrValue = falseLiteral();
			}
			case XOR: {
				// RLO := [NFC and RLO] xor [argument];
				newRloValue = xor(
					and(NFC.ref, RLO.ref),
					argRef
				);
				// OR := FALSE
				newOrValue = falseLiteral();
			}
			case XOR_NOT: {
				// RLO := [NFC and RLO] xor [not argument];
				newRloValue = xor(
					and(NFC.ref, RLO.ref),
					not(argRef)
				);
				// OR := FALSE
				newOrValue = falseLiteral();
			}
		}

		return sequentialAssignmentsFrom(start)
			.assignment(RLO.ref, newRloValue)
			.assignment(OR.ref, newOrValue)
			.independentAssignments(
				amt(STA.ref, copy(argRef)),
				amt(NFC.ref, trueLiteral())
			)
			.endLocation();
	}	
	
	/**
	 * Representation of the {@code O} (AND before OR) STL statement.
	 * Equivalent to:
	 * <pre>
	 *   {STA} := TRUE;
	 *   {OR} := {NFC} AND ({OR} OR {RLO});
	 *   {NFC} := {RLO} and {NFC};
	 * </pre>
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlAndBeforeOr e, Location start) {
		return sequentialAssignmentsFrom(start)
			.assignment(STA.ref, trueLiteral())
			.assignment(OR.ref, and(NFC.ref, or(OR.ref, RLO.ref)))
			.assignment(NFC.ref, and(RLO.ref, NFC.ref))
			.endLocation();
	}
	
	/**
	 * Representation of the {@code #(} STL statement, where # can be A, AN, O, ON, X, or XOR.
	 * Equivalent to:
	 * <pre>
	 *   <i>Push down nesting stack depending on #,</i>
	 *   {OR} := FALSE;
	 *   {STA} := TRUE;
	 *   {NFC} := FALSE;
	 * </pre>
	 * 
	 * The new stack entry will have the following values:
	 * <ul>
	 *   <li>{@code FC0, FC1, FC2} according to the operation,</li>
	 *   <li>RLO: <code>{RLO} OR NOT {nFC}</code> for {@code A} and {@code AN}; <code>{RLO} AND {nFC}</code> for the other mnemonics,</li>
	 *   <li>OR: <code>{OR} AND {nFC}</code> for {@code A} and {@code AN}; <code>FALSE</code> for the other mnemonics.</li>
	 * </ul>
	 * 
	 * TODO: BR should not be pushed (at least on S7-300)
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlBitLogicNestingOpenStatement e, Location start) {
		var Expression rloVal = null;
		var Expression orVal = null;
		var Expression brVal = BR.ref;
		
		switch (e.mnemonic) {
			case AND: {
				rloVal = or(RLO.ref, not(NFC.ref));
				orVal = and(OR.ref, NFC.ref);
			}
			case AND_NOT: {
				rloVal = or(RLO.ref, not(NFC.ref));
				orVal = and(OR.ref, NFC.ref);
			}
			case OR: {
				rloVal = and(RLO.ref, NFC.ref);
				orVal = falseLiteral();
			}
			case OR_NOT: {
				rloVal = and(RLO.ref, NFC.ref);
				orVal = falseLiteral();
			}
			case XOR: {
				rloVal = and(RLO.ref, NFC.ref);
				orVal = falseLiteral();
			}
			case XOR_NOT: {
				rloVal = and(RLO.ref, NFC.ref);
				orVal = falseLiteral();
			}
		}
		
		val fc = StlHelper.functionCodeOfNsOp(e.mnemonic);
		
		return sequentialAssignmentsFrom(start)
			.pushDownNestingStack()
			.independentAssignments(
				amt(NestingStackEntry.top(NestingStackBit.RLO).ref, rloVal), 
				amt(NestingStackEntry.top(NestingStackBit.OR).ref, orVal),
				amt(NestingStackEntry.top(NestingStackBit.BR).ref, brVal),
				amt(NestingStackEntry.top(NestingStackBit.FC0).ref, factory.createBoolLiteral(fc.fc0)),
				amt(NestingStackEntry.top(NestingStackBit.FC1).ref, factory.createBoolLiteral(fc.fc1)),
				amt(NestingStackEntry.top(NestingStackBit.FC2).ref, factory.createBoolLiteral(fc.fc2))
			)
			.independentAssignments(
				amt(OR.ref, falseLiteral()),
				amt(STA.ref, trueLiteral()),
				amt(NFC.ref, falseLiteral())
			)
			.endLocation();
	}
	
	/**
	 * Representation of the {@code )} STL statement.
	 * 
	 * Equivalent to:
	 * <pre>
	 *   <i>Pop the nesting stack</i>
	 *   {OR} := {nsOR};
	 *   {nFC} := TRUE;
	 *   {STA} := TRUE;
	 *   {RLO} := <i>depending on the <code>{nsRLO}</code> popped from the stack</i>;
	 * </pre>
	 * where <code>{nsOR}</code> is the popped OR bit from the nesting stack, <code>{nsRLO}</code> is the popped RLO bit from the nesting stack.
	 * 
	 * The new value of <code>{RLO}</code> will be determined based on the popped function code:
	 * <ul>
	 * 	<li>If the popped function code corresponds to {@code A}: <code>{RLO} := ({nsRLO} AND {RLO}) OR {nsOR};</code>.</li>
	 *	<li>If the popped function code corresponds to {@code AN}: <code>{RLO} := ({nsRLO} AND NOT {RLO}) OR {nsOR};</code>.</li>
	 * 	<li>If the popped function code corresponds to {@code O}: <code>{RLO} := {nsRLO} OR {RLO};</code>.</li>
	 * 	<li>If the popped function code corresponds to {@code ON}: <code>{RLO} := {nsRLO} OR NOT {RLO};</code>.</li>
	 * 	<li>If the popped function code corresponds to {@code X}: <code>{RLO} := {nsRLO} XOR {RLO};</code>.</li>
	 * 	<li>If the popped function code corresponds to {@code XN}: <code>{RLO} := {nsRLO} XOR NOT {RLO};</code>.</li>
	 * <ul>
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlBitLogicNestingCloseStatement e, Location start) {
		val loc1 = sequentialAssignmentsFrom(start)
			.assignment(OR.ref, NestingStackEntry.top(NestingStackBit.OR).ref)
			.assignment(NFC.ref, trueLiteral())
			.assignment(STA.ref, trueLiteral())
			.assignment(BR.ref, NestingStackEntry.top(NestingStackBit.BR).ref)
			.endLocation();
			
		val loc2 = createUniqueLocation(representingAutomaton);
		
		// new RLO values based on the stack content
		conditionalRloAmt(loc1, loc2, StlHelper.functionCodeOfNsOp(StlBitLogicOpMnemonic.AND), 
			or(and(NestingStackEntry.top(NestingStackBit.RLO).ref, RLO.ref), NestingStackEntry.top(NestingStackBit.OR).ref)
		);
		conditionalRloAmt(loc1, loc2, StlHelper.functionCodeOfNsOp(StlBitLogicOpMnemonic.AND_NOT), 
			or(and(NestingStackEntry.top(NestingStackBit.RLO).ref, not(RLO.ref)), NestingStackEntry.top(NestingStackBit.OR).ref)
		);
		conditionalRloAmt(loc1, loc2, StlHelper.functionCodeOfNsOp(StlBitLogicOpMnemonic.OR), 
			or(NestingStackEntry.top(NestingStackBit.RLO).ref, RLO.ref)
		);
		conditionalRloAmt(loc1, loc2, StlHelper.functionCodeOfNsOp(StlBitLogicOpMnemonic.OR_NOT), 
			or(NestingStackEntry.top(NestingStackBit.RLO).ref, not(RLO.ref))
		);
		conditionalRloAmt(loc1, loc2, StlHelper.functionCodeOfNsOp(StlBitLogicOpMnemonic.XOR), 
			factory.xor(NestingStackEntry.top(NestingStackBit.RLO).ref, RLO.ref)
		);
		conditionalRloAmt(loc1, loc2, StlHelper.functionCodeOfNsOp(StlBitLogicOpMnemonic.XOR), 
			factory.xor(NestingStackEntry.top(NestingStackBit.RLO).ref, not(RLO.ref))
		);	
			
		return sequentialAssignmentsFrom(loc2)
			.popNestingStack()
			.endLocation();
	}

	private def conditionalRloAmt(Location source, Location target, NestingStackFunctionCode fc, Expression rloRightVal) {
		Preconditions.checkState(source.parentAutomaton instanceof AutomatonDeclaration);
		val amtTransition = factory.createAssignmentTransition(
			nextTransName(),
			source.parentAutomaton as AutomatonDeclaration,
			source, target,
			and(
				eq(NestingStackEntry.top(NestingStackBit.FC0).ref, factory.createBoolLiteral(fc.fc0)), 
				eq(NestingStackEntry.top(NestingStackBit.FC1).ref, factory.createBoolLiteral(fc.fc1)),
				eq(NestingStackEntry.top(NestingStackBit.FC2).ref, factory.createBoolLiteral(fc.fc2))
			)
		);
		factory.createAssignment(amtTransition, RLO.ref, rloRightVal);
		return amtTransition;
	}
	
	private def SequentialAssignments nestingStackEntriesToFalse(SequentialAssignments context) {
		if (nestingStackInCfa === null) {
			// there is no need for nesting stacks
			return context;
		}
		
		val List<VariableAssignmentSkeleton> assignments = newArrayList();
		for (i : NestingStackEntry.TOPMOST_ENTRY_INDEX .. NestingStackEntry.DEEPEST_ENTRY_INDEX) {
			for (bit : StlHelper.NestingStackBit.values) {
				// BIT[i] := false
				assignments.add(amt(NestingStackEntry.of(bit, i).ref, falseLiteral()));
			}
		}
		context.independentAssignments(assignments);
		return context;
	}
	
	private def SequentialAssignments pushDownNestingStack(SequentialAssignments context) {
		Preconditions.checkNotNull(context);
		
		var current = context;
		Preconditions.checkState(NestingStackEntry.DEEPEST_ENTRY_INDEX > NestingStackEntry.TOPMOST_ENTRY_INDEX);
		for (i : NestingStackEntry.DEEPEST_ENTRY_INDEX .. NestingStackEntry.TOPMOST_ENTRY_INDEX + 1) {
			val List<VariableAssignmentSkeleton> assignments = newArrayList();
			for (bit : StlHelper.NestingStackBit.values) {
				// BIT[i] := BIT[i-1]
				assignments.add(amt(NestingStackEntry.of(bit, i).ref, NestingStackEntry.of(bit, i - 1).ref));
			}
			current = current.independentAssignments(assignments);
		}
		return current;
	}
	
	private def SequentialAssignments popNestingStack(SequentialAssignments context) {
		Preconditions.checkNotNull(context);
		
		var current = context;
		Preconditions.checkState(NestingStackEntry.DEEPEST_ENTRY_INDEX > NestingStackEntry.TOPMOST_ENTRY_INDEX);
		for (i : NestingStackEntry.TOPMOST_ENTRY_INDEX .. NestingStackEntry.DEEPEST_ENTRY_INDEX - 1) {
			val List<VariableAssignmentSkeleton> assignments = newArrayList();
			for (bit : StlHelper.NestingStackBit.values) {
				// BIT[i] := BIT[i+1]
				assignments.add(amt(NestingStackEntry.of(bit, i).ref, NestingStackEntry.of(bit, i + 1).ref));
			}
			current = current.independentAssignments(assignments);
		}
		
		val List<VariableAssignmentSkeleton> assignments = newArrayList();
		for (bit : StlHelper.NestingStackBit.values) {
			// BIT[DEEPEST] := false
			assignments.add(amt(NestingStackEntry.of(bit, NestingStackEntry.DEEPEST_ENTRY_INDEX).ref, falseLiteral()));
		}
		current = current.independentAssignments(assignments);
		return current;
	}
	
	/**
	 * Representation of the <code>= <i>argument</i></code> STL statement.
	 * <pre>
	 * Equivalent to:
	 *   <i>argument</i> := {RLO};
	 *   {OR} := FALSE;
	 *   {STA} := RLO;
	 *   {NFC} := FALSE;
	 * </pre>
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlAssignStatement e, Location start) {
		return sequentialAssignmentsFrom(start)
			.assignment(exprConverter.convertLeftValue(e.arg as LeftValue), RLO.ref)
			.assignment(OR.ref, falseLiteral())
			.assignment(STA.ref, RLO.ref)
			.assignment(NFC.ref, falseLiteral())
			.endLocation();
	}
	
	/**
	 * Representation of the <code>S <i>argument</i></code> STL statement.
	 * Equivalent to:
	 * <pre>
	 *   <i>argument</i> := <i>argument</i> OR {RLO};
	 *   {OR} := FALSE;
	 *   {STA} := <i>argument</i>;
	 *   {NFC} := FALSE;
	 * </pre>
	 * 
	 * TODO_LOWPRI handle MCR
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlSetStatement e, Location start) {
		return sequentialAssignmentsFrom(start)
			.assignment(exprConverter.convertLeftValue(e.arg as LeftValue), or(convertStlExpression(e.arg), RLO.ref))
			.assignment(OR.ref, falseLiteral())
			.assignment(STA.ref, convertStlExpression(e.arg))
			.assignment(NFC.ref, falseLiteral())
			.endLocation();
	}
	
	/**
	 * Representation of the {@code R argument} STL statement.
	 * Equivalent to:
	 * <pre>
	 *   <i>argument</i> := <i>argument</i> AND NOT {RLO};
	 *   {OR} := FALSE;
	 *   {STA} := <i>argument</i>;
	 *   {NFC} := FALSE;
	 * </pre>
	 * 
	 * TODO_LOWPRI handle MCR
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlResetStatement e, Location start) {
		return sequentialAssignmentsFrom(start)
			.assignment(exprConverter.convertLeftValue(e.arg as LeftValue), and(convertStlExpression(e.arg), not(RLO.ref)))
			.assignment(OR.ref, falseLiteral())
			.assignment(STA.ref, convertStlExpression(e.arg))
			.assignment(NFC.ref, falseLiteral())
			.endLocation();
	}
	
	/**
	 * Representation of the {@code FP argument} STL statement.
	 * Equivalent to:
	 * <pre>
	 *   {STA} := {RLO};
	 *   {OR} := FALSE;
	 *   {NFC} := TRUE;
	 *   IF NOT <i>argument</i> AND {RLO} THEN
	 *       <i>argument</i> := {RLO}; {RLO} := TRUE;
	 *   ELSE
	 *       <i>argument</i> := {RLO}; {RLO} := FALSE;
	 *   END_IF;
	 * </pre>
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlFpStatement e, Location start) {
		return convertStlEdgeStatement(e.arg, start, EdgeStatementType.FP);
	}
	
	/**
	 * Representation of the {@code FN argument} STL statement.
	 * Equivalent to:
	 * <pre>
	 *   {STA} := {RLO};
	 *   {OR} := FALSE;
	 *   {NFC} := TRUE;
	 *   IF <i>argument</i> AND NOT {RLO} THEN
	 *       <i>argument</i> := {RLO}; {RLO} := TRUE;
	 *   ELSE
	 *       <i>argument</i> := {RLO}; {RLO} := FALSE;
	 *   END_IF;
	 * </pre>
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlFnStatement e, Location start) {
		return convertStlEdgeStatement(e.arg, start, EdgeStatementType.FN);
	}
	
	private enum EdgeStatementType { FP, FN }
	private def Location convertStlEdgeStatement(cern.plcverif.plc.step7.step7Language.Expression argument, Location start, EdgeStatementType edgeType) {
		// TODO refactor, simplify (make SeqAssignmentsBuilder better?)
		val intermediateLoc = sequentialAssignmentsFrom(start)
			.assignment(OR.ref, falseLiteral())
			.assignment(STA.ref, RLO.ref)
			.assignment(NFC.ref, trueLiteral())
			.endLocation();
		
		val Expression argRef = convertStlExpression(argument);
		val parentAutomaton = intermediateLoc.parentAutomaton as AutomatonDeclaration;
		
		// if FP: [NOT arg AND RLO] arg := RLO; RLO := TRUE
		// if FN: [arg AND NOT RLO] arg := RLO; RLO := TRUE
		// (only the guard is different between those two)
		// Branch 1 represents the case when an edge (positive or negative, depending on 'edgeType' is detected
		val branch1 = createUniqueLocation(representingAutomaton);
		var Expression condition = null;
		switch (edgeType) {
			case FP: condition = and(not(argRef), RLO.ref)
			case FN: condition = and(argRef, not(RLO.ref))
			default: throw new UnsupportedOperationException('''Unknown edge detection STL instruction: '«edgeType»'.''')
		}
		factory.createAssignmentTransition(nextTransName, parentAutomaton, intermediateLoc, branch1, 
			condition);
		val branch1End = sequentialAssignmentsFrom(branch1)
			.assignment(exprConverter.convertLeftValue(argument as LeftValue), RLO.ref)
			.assignment(RLO.ref, trueLiteral())
			.endLocation();
			
		// [else] arg := RLO; RLO := TRUE
		// Branch 1 represents the case when the 'edgeType' edge is not detected
		val branch2 = createUniqueLocation(representingAutomaton);
		factory.createAssignmentTransition(nextTransName, parentAutomaton, intermediateLoc, branch2, 
			factory.createElseExpression()
		);
		val branch2End = sequentialAssignmentsFrom(branch2)
			.assignment(exprConverter.convertLeftValue(argument as LeftValue), RLO.ref)
			.assignment(RLO.ref, falseLiteral())
			.endLocation();
		
		// merge the two branches
		val endLoc = createUniqueLocation(representingAutomaton);
		factory.createAssignmentTransition(nextTransName, parentAutomaton, branch1End, endLoc, trueLiteral());
		factory.createAssignmentTransition(nextTransName, parentAutomaton, branch2End, endLoc, trueLiteral());
		return endLoc;
	}
	
	/**
	 * Representation of the {@code NOP argument} STL statement.
	 * Equivalent to: no operation.
	 * 
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlNopStatement e, Location start) {
		return start;
	}
	
	/**
	 * Representation of the {@code BLD argument} STL statement.
	 * Equivalent to: no operation.
	 * 
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlBldStatement e, Location start) {
		return start;
	}
	
	/**
	 * Representation of the {@code L argument} STL statement.
	 * Equivalent to:
	 * <pre>
	 *   {ACCU2} := {ACCU1};
	 *   {ACCU1} := <i>argument</i>;  
	 * </pre>
	 * <p>
	 * Status bits are not altered.
	 * 
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlLoadStatement e, Location start) {
		return sequentialAssignmentsFrom(start)
			.assignment(accu2.ref, accu1.ref)
			.assignment(accu1.ref, convertStlExpressionWithTypeConv(e.arg, Step7TypeToExprType.s7typeToExprType(StlSemanticsHelper.ACCU_TYPE)))
			.endLocation();
		// No STW change needed.
		// LOWPRI Add float support -- adapt for FloatType expressions
		// LOWPRI MCR support
	}
	
	/**
	 * Representation of the {@code T argument} STL statement.
	 * Equivalent to:
	 * <pre>
	 *   <i>argument</i> := {ACCU1};
	 * </pre>
	 * <p>
	 * Status bits are not altered.
	 * 
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlTransferStatement e, Location start) {
		Preconditions.checkArgument(e.arg instanceof LeftValue);
		return sequentialAssignmentsFrom(start)
			.assignment(exprConverter.convertLeftValue(e.arg as LeftValue), accu1.ref)
			.endLocation();
		// No STW change needed.
		// LOWPRI Add float support -- adapt for FloatType exp
	}
	
	/**
	 * Representation of the {@code #*} comparison STL statement, where # is from {>, >=, <, <=, ==}; * is from {I, D}.
	 * Equivalent to:
	 * <pre>
	 *   {RLO} := {ACCU2} # {ACCU1};
	 *   {CC0} := {ACCU1} > {ACCU2};
	 *   {CC1} := {ACCU1} < {ACCU2};
	 *   {OR} := FALSE;
	 *   {NFC} := TRUE;
	 *   {OV} := FALSE;
	 *   {STA} := {RLO};
	 * </pre>
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlComparisonStatement e, Location start) {
		// LOWPRI float not supported
		return sequentialAssignmentsFrom(start)
			.assignment(RLO.ref, factory.createComparisonExpression(convertIfNeeded(accu2.ref, e.dataType), convertIfNeeded(accu1.ref, e.dataType), stlComparisonMnemonicToExprComp(e.comparison)))
			.independentAssignments(
				amt(CC0.ref, factory.createComparisonExpression(convertIfNeeded(accu1.ref, e.dataType), convertIfNeeded(accu2.ref, e.dataType), ComparisonOperator.GREATER_THAN)),
				amt(CC1.ref, factory.createComparisonExpression(convertIfNeeded(accu1.ref, e.dataType), convertIfNeeded(accu2.ref, e.dataType), ComparisonOperator.LESS_THAN)),
				amt(OR.ref, falseLiteral()),
				amt(NFC.ref, trueLiteral()),
				amt(OV.ref, falseLiteral()),
				amt(STA.ref, RLO.ref) // TODO check semantics on real PLC
			)
			.endLocation();
	}
	
	private def convertIfNeeded(FieldRef ref, StlComparisonDataType type) {
		switch (type) {
			case INT:
				return convertIfNeeded(ref, Step7TypeToExprType.s7typeToExprType(ElementaryTypeEnum.INT))
			case DINT:
				return convertIfNeeded(ref, Step7TypeToExprType.s7typeToExprType(ElementaryTypeEnum.DINT))
			default:
				throw new UnsupportedOperationException("Unsupported comparison data type: " + type)
		}
	}
	
	private def convertIfNeeded(FieldRef ref, Type type) {
		if (type.dataTypeEquals(ref.type)) {
			return ref;
		} else {
			return exprConverter.convertExpressionWithTypeConv(ref, type);
		}
	}
	
	private def static ComparisonOperator stlComparisonMnemonicToExprComp(StlComparisonMnemonic mnemonic) {
		switch (mnemonic) {
			case EQ:  
				return ComparisonOperator.EQUALS
			case NEQ:  
				return ComparisonOperator.NOT_EQUALS
			case GE:
				return ComparisonOperator.GREATER_EQ
			case GT: 
				return ComparisonOperator.GREATER_THAN
			case LE:
				return ComparisonOperator.LESS_EQ
			case LT:
				return ComparisonOperator.LESS_THAN
		}
	} 
	
	/**
	 * Representation for various STL block end statements.
	 * 
	 * @see #convertUnconditionalReturn(Location)
	 * @see #convertConditionalReturn(Location)
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */ 
	protected def dispatch Location convert(StlBlockEndStatement e, Location start) {
		Preconditions.checkNotNull(start);
		
		if (e.mnemonic == StlBlockEndMnemonic.BLOCK_END_UNCONDITIONAL || e.mnemonic == StlBlockEndMnemonic.BLOCK_END) {
			return convertUnconditionalReturn(start);
		} else if (e.mnemonic == StlBlockEndMnemonic.BLOCK_END_CONDITIONAL) {
			return convertConditionalReturn(start);
		} else {
			throw new UnsupportedOperationException('''Unhandled STL block end instruction: '«e.mnemonic.literal»'.''')	
		}
	}
	
	/**
	 * Representation of the {@code BEU} and {@code BE} STL block end statements.
	 * Equivalent to:
	 * <pre>
	 *   {STA} := TRUE;
	 *   {OS} := FALSE;
	 *   {OR} := FALSE;
	 *   {NFC} := FALSE;
	 * 	 jump to the end of the block;
	 * </pre>
	 * 
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 * 	       It is always a newly created location with no incoming transition.
	 */
	 private def Location convertUnconditionalReturn(Location start) {
	 	// this operation has to be postponed, as the block is still being processed
		postpone(
			new PostponedTransitionCreation(representingAutomaton, start, nextTransName("BEU")) {
				override Location getTargetLocation() {
					if (representingAutomaton.endLocation === null) {
						throw new PlcCodeToCfaRuntimeException(
							"Target of BEU statement (end location of automaton) cannot be found.");
					}
					return representingAutomaton.endLocation;
				}

				override createTransitionAnnotations(Transition transition) {
					factory.createBlockReturnTransitionAnnotation(transition);
				}

				override createTransition() {
					val transition = super.createTransition();
					if (transition instanceof AssignmentTransition) {
						amt(STA.ref, trueLiteral()).createVariableAssignments(transition);
						amt(OS.ref, falseLiteral()).createVariableAssignments(transition);
						amt(OR.ref, falseLiteral()).createVariableAssignments(transition);
						amt(NFC.ref, falseLiteral()).createVariableAssignments(transition);
					} else {
						throw new PlcCodeToCfaRuntimeException(
							"Unexpected transition type, not possible to represent the BEU statement's register modifications.");
					}

					return transition;
				}
			}
		);
		
		return createUniqueLocation(representingAutomaton);
	 }
	 
	 /**
	 * Representation of the {@code BEC} STL block end statement.
	 * Equivalent to:
	 * <pre>
	 *   {STA} := TRUE;
	 *   {OR} := FALSE;
	 *   {NFC} := FALSE;
	 *   IF {RLO} THEN
	 *       {OS} := FALSE;
	 * 	     jump to the end of the block;
	 *   ELSE
	 *       {RLO} := TRUE;
	 *   END_IF;
	 * </pre>
	 * 
	 * Note that the represented behavior of the OS bit is just based on an assumption.
	 * 
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 * 	       It is always a newly created location with no incoming transition.
	 */
	 private def Location convertConditionalReturn(Location start) {
	 	val loc1 = sequentialAssignmentsFrom(start)
			.independentAssignments(
				amt(STA.ref, trueLiteral()),
				amt(OR.ref, falseLiteral()),
				amt(NFC.ref, falseLiteral())
			)
			.endLocation();
			
		val nextLoc = createUniqueLocation(representingAutomaton);
		// Branch with [NOT RLO]
		TransitionBuilder.builderBetween(loc1, nextLoc)
			.condition(not(RLO.ref))
			.addAssignment(amt(RLO.ref, trueLiteral()))
			.build();
	 	
	 	// Branch with [RLO]
		postpone(
			new PostponedTransitionCreation(representingAutomaton, loc1, nextTransName("BEC")) {
				override Location getTargetLocation() {
					if (representingAutomaton.endLocation === null) {
						throw new PlcCodeToCfaRuntimeException(
							"Target of BEC statement (end location of automaton) cannot be found.");
					}
					return representingAutomaton.endLocation;
				}

				override createTransitionAnnotations(Transition transition) {
					factory.createBlockReturnTransitionAnnotation(transition);
				}

				override createTransition() {
					val transition = super.createTransition();
					transition.setCondition(RLO.ref);		
					if (transition instanceof AssignmentTransition) {
						amt(OS.ref, falseLiteral()).createVariableAssignments(transition);
					} else {
						throw new PlcCodeToCfaRuntimeException(
							"Unexpected transition type, not possible to represent the BEC statement's register modifications.");
					}
					return transition;
				}
			}
		);
		
		return nextLoc;
	 }
	 
	 
	  /**
	 * Representation of the {@code JL} STL jump list statement.
	 * It is described by the syntax
	 * JL	{default}
	 * JU	{jump_1}
	 * ...
	 * JU	{jump_n}
	 * {default}	...;
	 * 
	 * with semantic: Load x:=ACCU1 and jump to {jump_x} if x<=n, else jump to {default}\
	 * 
	 * which is implemented as
	 * 
	 * IF ACCU1 == 0 
	 * 	jump to {jump_1}
	 * ELSE IF ACCU1 == 1
	 * 	jump to {jump_2}
	 * ...
	 * ELSE
	 * 	jump to {default}
	 * 	
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 * 	       It is always a newly created location with no incoming transition.
	 */
	 
	 protected def dispatch Location convert(StlJumpList e, Location start) {
	 	var previousLoc = start;
		Preconditions.checkNotNull(start);
		var i = 0;
		for (jump : e.jumps) {
			val condition = eq(accu1.ref, factory.createIntLiteral(i, copy(accu1.type as IntType)));
			previousLoc = convertJumpConditional(previousLoc, jump.targetLabel, condition);
			Preconditions.checkState(previousLoc !== null);
			i += 1;
		}
		previousLoc = convert(e.closingLabel, previousLoc)
		Preconditions.checkState(previousLoc !== null);
		return previousLoc;
	}
	 
	 
	
	/**
	 * Representation for various STL jump statements.
	 * 
	 * The jump condition (transition guard) will be the following, depending on the mnemonic:
	 * <ul>
	 * <li>For {@link StlJumpMnemonic#JUMP_UNCONDITIONAL} ({@code JU}): <code>[TRUE]</code>,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_RLO_TRUE} ({@code JC}): <code>[{RLO}]</code>*,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_RLO_FALSE} ({@code JCN}): <code>[NOT {RLO}]</code>*,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_RLO_TRUE_WITH_BR} ({@code JCB}): <code>[ todo ]</code>*,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_RLO_FALSE_WITH_BR} ({@code JNB}): <code>[ todo ]</code>*,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_NOT_ZERO} ({@code JN}): <code>[({CC1}=FALSE AND {CC0}=TRUE) OR ({CC1}=TRUE AND {CC0}=FALSE)]</code>,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_ZERO} ({@code JZ}): <code>[{CC1}=FALSE AND {CC0}=FALSE]</code>,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_PLUS} ({@code JP}): <code>[{CC1}=TRUE AND {CC0}=FALSE]</code>,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_MINUS} ({@code JM}): <code>[{CC1}=FALSE AND {CC0}=TRUE]</code>,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_PLUS_OR_ZERO} ({@code JPZ}): <code>[{CC0}=FALSE]</code>,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_MINUS_OR_ZERO} ({@code JMZ}): <code>[{CC1}=FALSE]</code>,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_UNORDERED} ({@code JUO}): <code>[{CC1}=TRUE AND {CC0}=TRUE]</code>,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_BR_FALSE} ({@code JNBI}): <code>[{BR}=FALSE]</code>*,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_BR_TRUE} ({@code JBI}): <code>[{BR}=FALSE]</code>*,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_OS_TRUE} ({@code JOS}): <code>[{OS}]</code>*,
	 * <li>For {@link StlJumpMnemonic#JUMP_IF_OV_TRUE} ({@code JO}): <code>[{OV}]</code>.
	 * </ul>
	 * 
	 * <p>
	 * In addition, for {@code JC, JCB, JCN, JNB}, the following assignments will be performed
	 * before the jump (even if the jump condition is evaluated to false thus there will
	 * not be a jump): <code>{OR} := FALSE; {STA} := TRUE; {nFC} := FALSE;</code>. Independently from 
	 * the result of the jump condition evaluation, <code>{RLO} := TRUE;</code> will be performed too,
	 * but not before the jump condition evaluation.  
	 * 
	 * In addition, for {@code JCB, JNB}, the <code>BR := RLO;</code> assignment is performed before everything.
	 * 
	 * In addition, for {@code JBI, JNBI}, the following assignments will be performed
	 * before the jump (even if the jump condition is evaluated to false thus there will
	 * not be a jump): <code>{OR} := FALSE; {STA} := TRUE; {nFC} := FALSE;</code>.
	 * 
	 * In addition, for {@code JOS}, the {@code OS} status bit will be set to false if the jump needs 
	 * to be performed. (If the jump does not need to be performed, the {@code OS} bit is already false.)
	 * 
	 * <p>
	 * Besides the transition with the above condition representing a jump to the target label,
	 * another transition will be created with {@link cern.plcverif.base.models.cfa.cfabase.ElseExpression} 
	 * as guard to jump to a newly created location. This latter location will be returned by the method.
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlJumpStatement e, Location start) {
		Preconditions.checkNotNull(start);
		switch (e.mnemonic) {
			case JUMP_UNCONDITIONAL: return convertJumpUnconditional(e.targetLabel, start) // JU
			case JUMP_IF_RLO_TRUE: return convertJumpIfRlo(e.targetLabel, start, true, false) // JC			
			case JUMP_IF_RLO_FALSE: return convertJumpIfRlo(e.targetLabel, start, false, false) // JCN
			case JUMP_IF_RLO_TRUE_WITH_BR:return convertJumpIfRlo(e.targetLabel, start, true, true) // JCB
			case JUMP_IF_RLO_FALSE_WITH_BR:return convertJumpIfRlo(e.targetLabel, start, false, true) // JNB
			case JUMP_IF_NOT_ZERO: return convertJumpIfCcDifferent(e.targetLabel, start) // JN
			case JUMP_IF_ZERO: return convertJumpIfCc(e.targetLabel, start, false, false) // JZ
			case JUMP_IF_PLUS: return convertJumpIfCc(e.targetLabel, start, true, false) // JP
			case JUMP_IF_MINUS: return convertJumpIfCc(e.targetLabel, start, false, true) // JM
			case JUMP_IF_PLUS_OR_ZERO: return convertJumpIfCc0(e.targetLabel, start, false) // JPZ
			case JUMP_IF_MINUS_OR_ZERO: return convertJumpIfCc1(e.targetLabel, start, false) // JMZ
			case JUMP_IF_UNORDERED: return convertJumpIfCc(e.targetLabel, start, true, true) // JUO
			case JUMP_IF_BR_FALSE: return convertJumpIfBr(e.targetLabel, start, false) // JNBI
			case JUMP_IF_BR_TRUE: return convertJumpIfBr(e.targetLabel, start, true) // JBI
			case JUMP_IF_OS_TRUE: return convertJumpIfOs(e.targetLabel, start) // JOS
			case JUMP_IF_OV_TRUE: return convertJumpIfOv(e.targetLabel, start) // JO
			
			default: 
				throw new UnsupportedOperationException('''Unhandled STL jump instruction: '«e.mnemonic.literal»'.''')			
		}
	}
	
	private def Location convertJumpUnconditional(Label target, Location start) {
		createJump(start, target);
		
		return createUniqueLocation(representingAutomaton);
	}
	
	/**
	 * Representation for
	 * {@code JZ} ({@code cc1=cc0=FALSE}),
	 * {@code JP} ({@code cc1=TRUE}, {@code cc0=FALSE}),
	 * {@code JM} ({@code cc1=FALSE}, {@code cc0=TRUE}),
	 * {@code JUO} ({@code cc1=cc0=TRUE}).
	 */
	private def Location convertJumpIfCc(Label targetLabel, Location start, boolean cc1, boolean cc0) {
		val cc1ref = if (cc1) CC1.ref else not(CC1.ref);
		val cc0ref = if (cc0) CC0.ref else not(CC0.ref);
		return convertJumpConditional(start, targetLabel, and(cc1ref, cc0ref));
	}
	
	/**
	 * Representation for
	 * {@code JPZ} ({@code cc0=FALSE}).
	 */
	private def Location convertJumpIfCc0(Label targetLabel, Location start, boolean cc0) {
		val condition = if (cc0) CC0.ref else not(CC0.ref);
		return convertJumpConditional(start, targetLabel, condition);
	}
	
	/**
	 * Representation for
	 * {@code JMZ} ({@code cc1=FALSE}).
	 */
	private def Location convertJumpIfCc1(Label targetLabel, Location start, boolean cc1) {
		val condition = if (cc1) CC1.ref else not(CC1.ref);
		return convertJumpConditional(start, targetLabel, condition);
	}
	
	/**
	 * Representation for
	 * {@code JBI} ({@code br=TRUE}) and
	 * {@code JNBI} ({@code br=FALSE}).
	 * <p>
	 * Besides the conditional jump, it sets {@code OR:=0, STA:=1, nFC:=0;} (before the jump).
	 */
	private def Location convertJumpIfBr(Label targetLabel, Location start, boolean br) {
		val loc1 = setOr0Sta1Nfc0(start);
		
		val condition = if (br) BR.ref else not(BR.ref);
		return convertJumpConditional(loc1, targetLabel, condition);
	}
	
	/**
	 * Representation for {@code JOS}.
	 */
	private def Location convertJumpIfOs(Label targetLabel, Location start) {
		val onTrue = createUniqueLocation(representingAutomaton);
		val onFalse = createUniqueLocation(representingAutomaton);
		
		val ifJump = createIfElseTransitions(start, OS.ref, onTrue, onFalse);		
		factory.createIfLocationAnnotation(start, onFalse, ifJump);
		
		val onTrue2 = createUniqueLocation(representingAutomaton);
		// set OS to false if jump
		TransitionBuilder.builderBetween(onTrue, onTrue2).addAssignment(amt(OS.ref, falseLiteral)).build();
		
		// if OS _was_ true, jump to target
		createJump(onTrue2, targetLabel);

		// if 'condition'=false, continue the normal execution
		// (OS was false, no need to set it to false)
		return onFalse;
	}
	
	/**
	 * Representation for {@code JO}.
	 */
	private def Location convertJumpIfOv(Label targetLabel, Location start) {
		val condition = OV.ref;
		return convertJumpConditional(start, targetLabel, condition);
	}
	
	/**
	 * Representation for {@code JN}.
	 */
	private def Location convertJumpIfCcDifferent(Label targetLabel, Location start) {
		val condition = or(and(CC0.ref, not(CC1.ref)), and(not(CC0.ref), CC1.ref))
		return convertJumpConditional(start, targetLabel, condition);
	}
		
	/**
	 * Representation for
	 * {@code JC} ({@code withBr=FALSE, rlo=TRUE}),
	 * {@code JCB} ({@code withBr=TRUE, rlo=TRUE}),
	 * {@code JCN} ({@code withBr=FALSE, rlo=FALSE}) and 
	 * {@code JNB} ({@code withBr=TRUE, rlo=FALSE}).
	 * <p>
	 * It sets first {@code BR:=RLO} if {@code withBr=TRUE}.
	 * Besides the conditional jump, it sets {@code OR:=0, STA:=1, nFC:=0;} (before the jump).
	 * It sets {@code RLO:=1} too, after the jump condition was evaluated. 
	 */
	private def Location convertJumpIfRlo(Label targetLabel, Location start, boolean rlo, boolean withBr) {
		var loc1 = start;
		if (withBr) {
			// If 'withBr = TRUE', an additional 'BR := RLO' assignment has to be executed 
			loc1 = setBrToRlo(start);
		}
		
		// OR:=FALSE, STA:=TRUE, nFC:=FALSE
		val loc2 = setOr0Sta1Nfc0(loc1);
		
		val condition = if (rlo) RLO.ref else not(RLO.ref); 
		val endLoc = convertJumpConditional(loc2, targetLabel, condition);
		
		// Add the RLO:=1 to both outgoing transitions of loc2
		// (Cannot be done earlier as original value is needed to evaluate 'condition'!) 
		Preconditions.checkState(loc2.outgoing.size == 2); // then and else transitions
		for (Transition t : loc2.outgoing) {
			if (t instanceof AssignmentTransition) {
				factory.createAssignment(t, RLO.ref, trueLiteral);
			} else {
				throw new UnsupportedOperationException("Unexpected case in convertJumpIfRloFalse.");
			}
		}
		
		return endLoc;
	}
	
	/**
	 * Creates an assignment transition performing {@code BR:=RLO}.
	 * @returns Newly created target location of the transition.
	 */
	private def Location setBrToRlo(Location start) {
		return sequentialAssignmentsFrom(start)
				.assignment(BR.ref, RLO.ref)
				.endLocation();
	}
	
	/**
	 * Creates an assignment transition performing {@code OR:=0, STA:=1, RLO:=1, nFC:=0;}.
	 * @returns Newly created target location of the transition.
	 */
	private def Location setOr0Sta1Nfc0(Location start) {
		return sequentialAssignmentsFrom(start)			
			.independentAssignments(
				amt(OR.ref, falseLiteral()),
				amt(STA.ref, trueLiteral()),
				amt(NFC.ref, falseLiteral())
			).endLocation();
	}
	
	
	private def Location convertJumpConditional(Location source, Label target, Expression condition) {
		val onTrue = createUniqueLocation(representingAutomaton);
		val onFalse = createUniqueLocation(representingAutomaton);
		val ifJump = createIfElseTransitions(source, condition, onTrue, onFalse);		
		
		// if 'condition'=true, jump to target
		createJump(onTrue, target);
		
		factory.createIfLocationAnnotation(source, onFalse, ifJump);

		// if 'condition'=false, continue the normal execution
		return onFalse;
	}
	
	/**
	 * Creates an if-else transition pair starting from a given location.
	 * 
	 * @return The 'then' transition of the if-else pair.
	 */
	private def Transition createIfElseTransitions(Location start, Expression condition, Location onTrue, Location onFalse) {
		Preconditions.checkNotNull(start);
		Preconditions.checkNotNull(condition);
		Preconditions.checkNotNull(onTrue);
		Preconditions.checkNotNull(onFalse);
		EmfHelper.checkNotContained(condition);
		
		Preconditions.checkState(start.parentAutomaton instanceof AutomatonDeclaration);
		val Transition then = TransitionBuilder.builderBetween(start, onTrue).name(nextTransName()).condition(condition).build();
		TransitionBuilder.builderBetween(start, onFalse).name(nextTransName()).condition(not(condition)).build();
		
		return then;
	}
	
	private def void createJump(Location from, Label target) {
		postpone(new PostponedTransitionCreation(representingAutomaton, from, nextTransName("jump")) {
			override getTargetLocation() {
				return getRegisteredLocationOfLabel(target);
			}			
			
			override createTransitionAnnotations(Transition transition) {
				factory.createGotoTransitionAnnotation(transition, target.name);
			}
		});
	}
	
	/**
	 * Represents {@code CALL} instruction.
	 * After calling the FC/FB, it will set the following status bits:
	 * <pre>
	 *   {OS} := FALSE;
	 *   {OR} := FALSE;
	 *   {STA} := TRUE;
	 *   {nFC} := FALSE;
	 * </pre> 
	 *
	 * FIXME: if the called instance is locally declared, {RLO} may have to be altered too.
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlSubroutineCall e, Location start) {
		// If this is bool then the {RLO} settings might be wrong, if this occurs something has to be implemented or changed
		if (e.calledUnit instanceof Function){
			Preconditions.checkState(!DataTypeUtil.isBooleanElementaryType((e.calledUnit as Function).returnType))
		}	
		
		convertUnconditionalCall(e, start);
	}
	
	/**
	 * Represents {@code UC} and {@code CC} instructions.
	 * <p>
	 * In case of {@code UC}, the everything is the same as for {@code CALL},
	 * thus {@link #convertUnconditionalCall} is used.
	 * <p>
	 * In case of {@code CC}, the call is only executed if {@code RLO}=true.
	 * Whether the call is executed or not in this case, the status bits will be set as follows:
	 * <pre>
	 *   {OS} := FALSE;
	 *   {OR} := FALSE;
	 *   {STA} := TRUE;
	 *   {nFC} := FALSE;
	 *   {RLO} := TRUE;
	 * </pre>
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlParameterlessCallStatement e, Location start) {
		if (e.mnemonic == StlParameterlessCallMnemonic.UNCONDITIONAL_CALL) {
			return convertUnconditionalCall(Step7LanguageHelper.asSubroutineCall(e), start);
		} else if (e.mnemonic == StlParameterlessCallMnemonic.CONDITIONAL_CALL) {
			val parent = start.parentAutomaton as AutomatonDeclaration;
			val ifTrue = createUniqueLocation(parent);
			TransitionBuilder.builderBetween(start, ifTrue).condition(RLO.ref).build;
			val mergeLoc = convertCallStatement(Step7LanguageHelper.asSubroutineCall(e), ifTrue);
			TransitionBuilder.builderBetween(start, mergeLoc).condition(not(RLO.ref)).addAssignment(amt(RLO.ref, trueLiteral())).build;
			
			return sequentialAssignmentsFrom(mergeLoc)
					.independentAssignments(
					amt(OS.ref, falseLiteral()),
					amt(OR.ref, falseLiteral()),
					amt(STA.ref, trueLiteral()),
					amt(NFC.ref, falseLiteral())
					//amt(RLO.ref, trueLiteral())
				)
				.endLocation();
				// Note: RLO is not set to true after the call. If the callee clears the RLO,
				// it will remain false for the caller's succeeding instruction.
				// Checked on real PLC on 08/11/2018 with STEP 7 v5.6. 
				// It seems that the rest of the bits are set after executing the call.
		} else {
			throw new UnsupportedOperationException('''Unknown parameterless STL call mnemonic: '«e.mnemonic.literal»'.''');
		}
	}
	
	/**
	 * Common representation for {@code CALL} and {@code UC}.
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	private def Location convertUnconditionalCall(SubroutineCall e, Location start) {
		val endOfCall = convertCallStatement(e, start);
		return sequentialAssignmentsFrom(endOfCall)
			.independentAssignments(
				amt(OS.ref, falseLiteral()),
				amt(OR.ref, falseLiteral()),
				amt(STA.ref, trueLiteral()),
				amt(NFC.ref, falseLiteral())
			)
			.endLocation();
	}
	
	
	protected def dispatch Location convert(StlFloatMathFunction e, Location start) {
		// ABS and SQR need to be treated specifically, as they are not CFA library functions.
		if (e.mnemonic == StlFloatMathFunctionMnemonic.ABS) {
			return convertAbs(start);
		} else if (e.mnemonic == StlFloatMathFunctionMnemonic.SQR) {
			return convertSqr(start);
		}  
		
		val argument = convertStlExpressionWithTypeConv(accu1.ref, realType());
		val libraryFunction = toCfaLibraryFunction(e.mnemonic);
		val libraryExpr = factory.createLibraryFunction(Collections.singletonList(argument), libraryFunction, realType());
		
		// NOTE at the conversion from REAL to DINT we lose the digits after decimal point!! 
			
		return sequentialAssignmentsFrom(start)
			.assignment(accu1.ref, convertStlExpressionWithTypeConv(libraryExpr, 
				Step7TypeToExprType.s7typeToExprType(StlSemanticsHelper.ACCU_TYPE)
			))
			.endLocation();
	}
	
	private def Location convertAbs(Location start) {
		val endLocation = createUniqueLocation(representingAutomaton);
		
		// If ACCU1 >= 0: nothing to do
		TransitionBuilder.builderBetween(start, endLocation).name(nextTransName()).condition(factory.geq(accu1.ref, factory.createIntLiteral(0, copy(accu1.type) as IntType))).build();
		
		// IF ACCU1<0: ACCU1 := ACCU1 * -1;
		TransitionBuilder.builderBetween(start, endLocation).name(nextTransName()).condition(factory.createElseExpression).
		addAssignment(amt(accu1.ref, factory.multiply(accu1.ref, factory.createIntLiteral(-1, copy(accu1.type) as IntType)))).build();
		
		// NOTE This does not take into account that floating-point numbers should be treated here.
		
		// Does not alter any of the status bits.
		return endLocation;
	}
	
	private def Location convertSqr(Location start) {
		// TODO_LOWPRI The status bits are not modified here (CC1, CC0, OV, OS).
		// NOTE This does not take into account that floating-point numbers should be treated here.
		return sequentialAssignmentsFrom(start).assignment(accu1.ref, factory.multiply(accu1.ref, accu1.ref)).endLocation;
	}
	
	 /**
	 * Representation of the {@link StlArithmeticStatement} arithmetic operation STL statements (e.g., {@code +D, *I}).
	 * Equivalent to:
	 * <pre>
	 *   {ACCU1} := {ACCU1} # {ACCU2}; // where # is +, - or * depending on the mnemonic
	 * </pre>
	 * See the code for the assignments of <code>{CC0}</code>, <code>{CC1}</code>, <code>{OV}</code>, <code>{OS}</code>
	 * as they differ based on both the operation and the data type.
	 *
	 * <p>
	 * The floating-point number handling here is incomplete.
	 *
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlArithmeticStatement e, Location start) {
			 
		val List<VariableAssignmentSkeleton> amts = newArrayList();
		val List<VariableAssignmentSkeleton> preAmts = newArrayList();
		
		var VariableAssignmentSkeleton accuAmt;		
		// Calculate new ACCU value
		if (e.mnemonic == StlArithmeticMnemonic.PLUS) {
			accuAmt = amt(accu1.ref, factory.plus(accu1.ref, accu2.ref))
		} else if (e.mnemonic == StlArithmeticMnemonic.MINUS) {
			accuAmt = amt(accu1.ref, factory.minus(accu2.ref, accu1.ref))
		} else if (e.mnemonic == StlArithmeticMnemonic.MULTIPLICATION) {
			accuAmt = amt(accu1.ref, factory.multiply(accu1.ref, accu2.ref))
		} else if (e.mnemonic == StlArithmeticMnemonic.DIVISON) {
			accuAmt = amt(accu1.ref, factory.divide(accu2.ref, accu1.ref)) //Note that ACCU2 is divided by ACCU1
		} 
		else {
			throw new UnsupportedOperationException('''Unknown STL arithmetic instruction: '«e.mnemonic.literal»'.''');
		}
		
		
		if (e.dataType == StlComparisonDataType.DINT) {
			// Calculate OV, OS (same for PLUS, MINUS, MULTIPLICATION)
			preAmts.add(amt(OV.ref, not(betweenIncl(accu1.ref, -214783648L, 214783647L))))
			preAmts.add(amt(OS.ref, or(OS.ref, not(betweenIncl(accu1.ref, -214783648L, 214783647L)))))
			
			// Calculate CC0, CC1
			// Assuming that ACCU1 is a DINT
			if (e.mnemonic == StlArithmeticMnemonic.PLUS || e.mnemonic == StlArithmeticMnemonic.MINUS) {
				amts.add(amt(CC0.ref, or(
					betweenIncl(accu1.ref, -214783648L, -1L),
					gt(accu1.ref, 214783647L)
				)))
				amts.add(amt(CC1.ref, or(
					betweenIncl(accu1.ref, 1L, 2147483647L),
					lt(accu1.ref, -2147483648L) // will never be satisfied! // why wouldnt it? Both added parts can be negative
				)))
			} else if (e.mnemonic == StlArithmeticMnemonic.MULTIPLICATION) {
				amts.add(amt(CC0.ref, lt(accu1.ref, 0)))
				amts.add(amt(CC1.ref, gt(accu1.ref, 0)))
			}
		} else if (e.dataType == StlComparisonDataType.INT) {
			// Calculate OV, OS (same for PLUS, MINUS, MULTIPLICATION)
			amts.add(amt(OV.ref, 
				not(betweenIncl(accu1.ref, -32768L, 32767L))
			));
			amts.add(amt(OS.ref, or(OS.ref, not(betweenIncl(accu1.ref, -32768L, 32767L)))));
			
			if (e.mnemonic == StlArithmeticMnemonic.PLUS || e.mnemonic == StlArithmeticMnemonic.MINUS) {
				amts.add(amt(CC0.ref, or(
					betweenIncl(accu1.ref, -32768, -1L),
					betweenIncl(accu1.ref, 32768L, 65535L)
				)))
				amts.add(amt(CC1.ref, or(
					betweenIncl(accu1.ref, 1, 32767L),
					betweenIncl(accu1.ref, -65535L, -32769L)
				)))
			} else if (e.mnemonic == StlArithmeticMnemonic.MULTIPLICATION) {
				amts.add(amt(CC0.ref, lt(accu1.ref, 0)))
				amts.add(amt(CC1.ref, gt(accu1.ref, 0)))
			} else if (e.mnemonic == StlArithmeticMnemonic.DIVISON) {
				return convertDivision(e, start)
			}
			
		} else if  (e.dataType == StlComparisonDataType.REAL) {
			// TODO_LOWPRI incomplete implementation of REAL StlArithmeticStatement, floats are not supported yet
			amts.add(amt(OV.ref, falseLiteral()));
			// OS skipped on purpose
			amts.add(amt(CC0.ref, falseLiteral()));
			amts.add(amt(CC1.ref, falseLiteral()));
		}
		
		return sequentialAssignmentsFrom(start)
			.independentAssignments(preAmts)
			.assignmentIfPresent(accuAmt)
			.independentAssignments(amts)
			.endLocation();
	}
	
	/**
	 * Representation of the StlArithmeticStatement arithmetic operation STL statements with Division mnemonic.
	 * Equivalent to:
	 * <pre>
	 *   {ACCU1} := {ACCU1} / {ACCU2}; 
	 * </pre>
	 * See the code for the assignments of <code>{CC0}</code>, <code>{CC1}</code>, <code>{OV}</code>, <code>{OS}</code>
	 *
	 * <p>
	 * This specific case for division is needed because of handling special cases like overflow and division through zero
	 *
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def Location convertDivision(StlArithmeticStatement e, Location start){
		val endLoc = createUniqueLocation(representingAutomaton);
		
		val thenStart = createUniqueLocation(representingAutomaton);
		val elseStart = createUniqueLocation(representingAutomaton);
		
		val Transition ifZeroOrOverflowThen = createIfElseTransitions(start, or(eq(accu1.ref, 0), eq(factory.divide(accu2.ref, accu1.ref), 32768L)), thenStart, elseStart);
		
		factory.createIfLocationAnnotation(start, endLoc, ifZeroOrOverflowThen);
		
		var VariableAssignmentSkeleton accuAmt;	
		
		// then branch (thenStart --> endLoc)
		accuAmt = amt(accu1.ref, factory.divide(accu2.ref, accu1.ref))
		val List<VariableAssignmentSkeleton> preAmts = newArrayList();
		preAmts.add(amt(CC0.ref, eq(accu1.ref, 0)))
		preAmts.add(amt(CC1.ref, or(
			eq(factory.divide(accu2.ref, accu1.ref), 32768L),
			eq(accu1.ref, 0)
		)))
		preAmts.add(amt(OV.ref, or(
			eq(factory.divide(accu2.ref, accu1.ref), 32768L),
			eq(accu1.ref, 0)
		)))
		preAmts.add(amt(OS.ref, or(or(
			OS.ref,
			eq(factory.divide(accu2.ref, accu1.ref), 32768L)),
			eq(accu1.ref, 0)
		)))
		
		TransitionBuilder.builderBetween(thenStart, endLoc)
			.name(nextTransName())
			.condition(trueLiteral())
			.addAssignments(preAmts)
			.addAssignment(accuAmt)
			.build();
		
		// else branch elseStart --> endLoc
		accuAmt = amt(accu1.ref, factory.divide(accu2.ref, accu1.ref))
		val List<VariableAssignmentSkeleton> postAmts = newArrayList();
		postAmts.add(amt(CC0.ref, lt(accu1.ref, 0)))
		postAmts.add(amt(CC1.ref, gt(accu1.ref, 0)))		
		postAmts.add(amt(OV.ref, falseLiteral))
		accuAmt = amt(accu1.ref, factory.divide(accu2.ref, accu1.ref))
		
		TransitionBuilder.builderBetween(elseStart, endLoc)
			.name(nextTransName())
			.condition(trueLiteral())
			.addAssignment(accuAmt)
			.addAssignments(postAmts)
			.build();
		
		return endLoc;		
	}
	
	
	/**
	 * Representation of the {@code MOD} STL statement ({@link StlModStatement}).
	 * Equivalent to:
	 * <pre>
	 *   IF {ACCU1} = 0 THEN
	 *     {OS}, {OV}, {CC0}, {CC1} := TRUE;
	 *   ELSE
	 *     {ACCU1} := {ACCU2} MOD {ACCU1};
	 *     // elseLoc1
	 *     {OV} := FALSE;
	 *     {CC0} := {ACCU1} < 0;
	 *     {CC1} := {ACCU1} > 0;
	 *   END_IF;
	 * </pre>
	 * <p>
	 * It is assumed that there are only two ACCUs. The behavior
	 * is slightly different if there are four ACCUs.
	 * 
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlModStatement e, Location start) {
		val endLoc = createUniqueLocation(representingAutomaton);
		
		val thenStart = createUniqueLocation(representingAutomaton);
		val elseStart = createUniqueLocation(representingAutomaton);
		val elseLoc1 = createUniqueLocation(representingAutomaton);
		
		val Transition ifZeroThen = createIfElseTransitions(start, eq(accu1.ref, factory.createIntLiteral(0, copy(accu1.type as IntType))), thenStart, elseStart);
		
		factory.createIfLocationAnnotation(start, endLoc, ifZeroThen);
		
		// then branch (thenStart --> endLoc)
		TransitionBuilder.builderBetween(thenStart, endLoc)
			.name(nextTransName())
			.condition(trueLiteral())
			.addAssignment(amt(OS.ref, trueLiteral))
			.addAssignment(amt(OV.ref, trueLiteral))
			.addAssignment(amt(CC0.ref, trueLiteral))
			.addAssignment(amt(CC1.ref, trueLiteral))
			.build();
		
		// else branch
		// t1: elseStart --> elseLoc1
		val t1 = factory.createAssignmentTransition(nextTransName(), representingAutomaton, elseStart, elseLoc1, trueLiteral());
		factory.createAssignment(t1, accu1.ref, factory.createBinaryArithmeticExpression(accu2.ref, accu1.ref, BinaryArithmeticOperator.MODULO));
		
		// t2: elseLoc1 --> endLoc
		TransitionBuilder.builderBetween(elseLoc1, endLoc)
			.name(nextTransName())
			.condition(trueLiteral())
			.addAssignment(amt(OV.ref, falseLiteral))
			.addAssignment(amt(CC0.ref, lt(accu1.ref, 0)))
			.addAssignment(amt(CC1.ref, gt(accu1.ref, 0)))
			.build();
		
		return endLoc;
	}
	
	
	/**
	 * Representation of the <i>argumentless</i> STL word logic instructions ({@code AW}, {@code OW}, {@code XOW}, {@code AD}, {@code OD}, {@code XOD}).
	 * Equivalent to (for DWORD):
	 * <pre>
	 *   ACCU1 := ACCU1 {op} ACCU2;
	 *   {CC0}, {OV} := FALSE;
	 *   {CC1} := (ACCU1 = DW#0);
	 * </pre>
	 * <p>
	 * Equivalent to (for WORD):
	 * <pre>
	 *   ACCU1 := ((ACCU1 AND DW#0000_FFFF) {op} (ACCU2 AND DW#0000_FFFF)) OR (ACCU1 AND DW#FFFF_0000);
	 *   {CC0}, {OV} := FALSE;
	 *   {CC1} := ((ACCU1  AND DW#0000_FFFF) = DW#0);
	 * </pre>
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlWordLogicArglessStatement e, Location start) {
		val binaryOp = switch(e.mnemonic) {
			case AND_WORD, case AND_DWORD: BinaryArithmeticOperator.BITWISE_AND
			case OR_WORD, case OR_DWORD: BinaryArithmeticOperator.BITWISE_OR
			case XOR_WORD, case XOR_DWORD: BinaryArithmeticOperator.BITWISE_XOR
			default: throw new UnsupportedOperationException('''Unknown STL word logic instruction: '«e.mnemonic.literal»'.''') 
		};
		
		if (StlSemanticsHelper.isDwordSized(e.mnemonic)) {
			// AD, OD, XOD
			return sequentialAssignmentsFrom(start)
				.assignment(accu1.ref, factory.createBinaryArithmeticExpression(accu1.ref, accu2.ref, binaryOp))
				.independentAssignments(
					amt(CC0.ref, falseLiteral()),
					amt(OV.ref, falseLiteral()),
					amt(CC1.ref, eq(accu1.ref, factory.createIntLiteral(0, copy(accu1.type) as IntType)))
				).endLocation;
		} else {
			// AW, OW, XOW 
			Preconditions.checkState(StlSemanticsHelper.isWordSized(e.mnemonic))
			
	 		// represent accuLow=DW#0000_FFFF, accuHigh=DW#FFFF_0000
	 		val accuLow = accuTypeIntLiteral(StlGlobalData.ACCU_LOW_MASK);
	 		val accuHigh = accuTypeIntLiteral(StlGlobalData.ACCU_HIGH_MASK);
			
			return sequentialAssignmentsFrom(start)
				.assignment(accu1.ref, bwOr(
					factory.createBinaryArithmeticExpression(bwAnd(accu1.ref, accuLow), bwAnd(accu2.ref, accuLow), binaryOp),
					bwAnd(accu1.ref, accuHigh)
					))
				.independentAssignments(
					amt(CC0.ref, falseLiteral()),
					amt(OV.ref, falseLiteral()),
					amt(CC1.ref, eq(bwAnd(accu1.ref, accuLow), factory.createIntLiteral(0, copy(accu1.type) as IntType)))
				).endLocation;
		}
	}
	
	/**
	 * Representation of the {@link StlAccuIncrementStatement} STL accumulator increment statement.
	 * Equivalent to:
	 * <pre>
	 *   {ACCU1} := ({ACCU1} & 2#11111111) | (({ACCU1} + <i>argument</a>) & 2#11111111);
	 * </pre>
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlAccuIncrementStatement e, Location start) {
		return convertIncDecStatement(e.arg, BinaryArithmeticOperator.PLUS, start);
	}
	
	/**
	 * Representation of the {@link StlAccuDecrementStatement} STL accumulator decrement statement.
	 * Equivalent to:
	 * <pre>
	 *   {ACCU1} := ({ACCU1} & 2#11111111) | (({ACCU1} - <i>argument</a>) & 2#11111111);
	 * </pre>
     *	 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlAccuDecrementStatement e, Location start) {
		return convertIncDecStatement(e.arg, BinaryArithmeticOperator.MINUS, start);
	}
	
	/**
	 * Unified handling of StlAccuIncrementStatement and StlAccuDecrementStatement.
	 */
	private def Location convertIncDecStatement(NamedOrUnnamedConstantRef argument, BinaryArithmeticOperator operator, Location start) {
		// (ACCU1 & ACCU_LL_MASK_INV) | ((ACCU1 {op} arg) & ACCU_LL_MASK)
		val long argValue = SimpleExpressionUtil.evaluateSimpleExpression(argument);
		val newAccuValue = bwOr(
			bwAnd(accu1.ref, accuTypeIntLiteral(StlGlobalData.ACCU_LOWLOW_MASK_INV)),
			bwAnd(
				factory.createBinaryArithmeticExpression(accu1.ref, accuTypeIntLiteral(argValue), operator), 
				accuTypeIntLiteral(StlGlobalData.ACCU_LOWLOW_MASK)
			)
		);
		
		return sequentialAssignmentsFrom(start)
			.assignment(accu1.ref, newAccuValue)
			.endLocation();
	}
	
	
	/**
	 * Representation of the {@link StlConversionStatement} STL conversion statements.
	 * <p>
	 * Not supported for the following mnemonics:
	 * {@link StlConversionMnemonic#BCD_TO_DINT},
	 * {@link StlConversionMnemonic#BCD_TO_INT},
	 * {@link StlConversionMnemonic#CAD},
	 * {@link StlConversionMnemonic#CAW},
	 * {@link StlConversionMnemonic#DINT_TO_BCD},
	 * {@link StlConversionMnemonic#INT_TO_BCD},
	 * {@link StlConversionMnemonic#REAL_NEGATE},
	 * {@link StlConversionMnemonic#DINT_TO_REAL}. In these cases the STL statement is skipped.
	 *
	 * Check the code for the exact implementation of the rest of the conversion statements.
	 * 
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlConversionStatement e, Location start) {
		switch (e.mnemonic) {
			case BCD_TO_DINT,
			case BCD_TO_INT,
			case CAD,
			case CAW,
			case DINT_TO_BCD,
			case INT_TO_BCD: {
				handleUnsupportedStatement(e, e.mnemonic.literal);
				return start;
			}
			case DINT_ONES_COMPLEMENT: {
				// intentionally no STW modification
				return sequentialAssignmentsFrom(start)
					.assignment(accu1.ref, factory.bwXor(
						accu1.ref,
						convertStlExpressionWithTypeConv(
							SiemensWordIndexingUtils.wordWithAllOnes(
								Step7TypeToExprType.s7typeToExprType(ElementaryTypeEnum.DINT) as IntType),
							accu1.type
						)
					)).endLocation();
			}
			case INT_ONES_COMPLEMENT: {
				// intentionally no STW modification
				return sequentialAssignmentsFrom(start).assignment(accu1.ref, factory.bwXor(
					accu1.ref,
					convertStlExpressionWithTypeConv(
						SiemensWordIndexingUtils.wordWithAllOnes(
							Step7TypeToExprType.s7typeToExprType(ElementaryTypeEnum.INT) as IntType),
						accu1.type
					)
				)).endLocation();
			}
			case INT_TO_DINT: {
				// nothing to do with the current ACCU representation
				// TODO zero out the upper 16 bits (or "one out" if negative?) (or MOD INT16_MAX?)
				return start;
			}
			case REAL_NEGATE: {
				// Floating-point ACCU operations are not supported at the moment
			}
			case DINT_TO_REAL: {
				// Floating-point ACCU operations are not supported at the moment
			}
			case DINT_TWOS_COMPLEMENT: {
				log.logWarning("The NEGD STL instruction is not represented accurately.");
				// LOWPRI NEGD representation improve
				return sequentialAssignmentsFrom(start)
					.assignment(accu1.ref, factory.multiply(
							accuTypeIntLiteral(-1),
							accu1.ref
						))
					.independentAssignments(
						amt(OV.ref, eq(accu1.ref, accuTypeIntLiteral(-2_147_483_648L))),
						amt(OS.ref, eq(accu1.ref, accuTypeIntLiteral(-2_147_483_648L))),
						amt(CC0.ref, lt(accu1.ref, 0)),
						amt(CC1.ref, gt(accu1.ref, 0))
					)
					.endLocation();
			}
			case INT_TWOS_COMPLEMENT: {
				log.logWarning("The NEGI STL instruction is not represented accurately. It does not take into account that ACCU1-H may be non-zero. Also, overflow handling is not done precisely.");
				// LOWPRI NEGI representation improve
				
				val locUnderflow = createUniqueLocation(start.parentAutomaton as AutomatonDeclaration);
				val locNormal = createUniqueLocation(start.parentAutomaton as AutomatonDeclaration);
				createIfElseTransitions(start, eq(accu1.ref, accuTypeIntLiteral(-32768)), locUnderflow, locNormal);
				
				val locMerge = createUniqueLocation(start.parentAutomaton as AutomatonDeclaration);
				TransitionBuilder.builderBetween(locUnderflow, locMerge).addAssignment(amt(accu1.ref, accuTypeIntLiteral(-32768))).build();
				TransitionBuilder.builderBetween(locNormal, locMerge).addAssignment(amt(accu1.ref, factory.multiply(
							accuTypeIntLiteral(-1),
							accu1.ref
						))).build();
				
				return sequentialAssignmentsFrom(locMerge)
					.independentAssignments(
						amt(OV.ref, eq(accu1.ref, accuTypeIntLiteral(-32768))),
						amt(OS.ref, eq(accu1.ref, accuTypeIntLiteral(-32768))),
						amt(CC0.ref, lt(accu1.ref, 0)),
						amt(CC1.ref, gt(accu1.ref, 0))
					)
					.endLocation();
					
			}
			case RND_MINUS: {
				// Floating-point ACCU operations are not supported at the moment
			}
			case RND_PLUS: {
				// Floating-point ACCU operations are not supported at the moment
			}
			case ROUND: { 
				// Floating-point ACCU operations are not supported at the moment 
			}
			case TRUNCATE: {
				// Floating-point ACCU operations are not supported at the moment
			}	
		}
		
		handleUnsupportedStatement(e, e.mnemonic.literal);
		return start;
	}
	
	/**
	 * Representation of a verification assertion.
	 * 
	 * @param e STL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convert(StlVerificationAssertion e, Location start) {
		return this.convertVerificationAssertion(e, start);
	}

	// SPECIFIC HELPER METHODS
	private def LibraryFunctions toCfaLibraryFunction(StlFloatMathFunctionMnemonic mnemonic) {
		switch (mnemonic) {
			case ACOS: return LibraryFunctions.ACOS
			case ASIN: return LibraryFunctions.ASIN
			case ATAN: return LibraryFunctions.ATAN
			case COS: return LibraryFunctions.COS
			case EXP: return LibraryFunctions.EXP
			case LN: return LibraryFunctions.LN
			case SIN: return LibraryFunctions.SIN
			case SQRT: return LibraryFunctions.SQRT
			case TAN: return LibraryFunctions.TAN
			//case ABS:
			//case SQR: 
			default: 
				throw new UnsupportedOperationException('''Unsupported STL floating-point mathematical instruction: '«mnemonic.literal»'.''')
		}
	}
	
	private def Field getAccu1() {
		return stlGlobalData.accu1Field;
	}
	
	private def Field getAccu2() {
		return stlGlobalData.accu2Field;
	}
	
	private def FieldRef ref(StlHelper.StatusBit bit) {
		val cfaField = stlGlobalData.getStatusBitField(bit);
		return factory.createFieldRef(cfaField);
	}
	
	private def FieldRef ref(NestingStackEntry nestingStackEntry) {
		val cfaField = nestingStackInCfa.get(nestingStackEntry);
		Preconditions.checkNotNull(cfaField);
		return factory.createFieldRef(cfaField);
	}
	
	private def FieldRef ref(Field field) {
		Preconditions.checkNotNull(field);
		return factory.createFieldRef(field);
	}

	private def SequentialAssignments sequentialAssignmentsFrom(Location start) {
		return SequentialAssignments.builder([createUniqueLocation(representingAutomaton)], [nextTransName]).startLocation(start);
	}
	
	private def Expression not(Expression arg) {
		return factory.neg(arg);
	}
	
	private def Expression and(Expression lhs, Expression rhs) {
		return factory.and(lhs, rhs);
	}
	
	private def Expression and(Expression... exprs) {
		return factory.and(exprs);
	}
	
	private def Expression bwAnd(Expression lhs, Expression rhs) {
		return factory.bwAnd(lhs, rhs);
	}
	
	private def Expression or(Expression lhs, Expression rhs) {
		return factory.or(lhs, rhs);
	}
	
	private def Expression bwOr(Expression lhs, Expression rhs) {
		return factory.bwOr(lhs, rhs);
	}
	
	private def Expression xor(Expression lhs, Expression rhs) {
		return factory.xor(lhs, rhs);
	}
	
	private def Expression eq(Expression lhs, Expression rhs) {
		return factory.eq(lhs, rhs);
	}
	
	private def Expression geq(Expression lhs, Expression rhs) {
		return factory.geq(lhs, rhs);
	}
	
	/*private def Expression pow(Expression lhs, Expression rhs){
		return factory.pow(lhs, rhs);
	}*/
	
	/**
	 * Simple shorthand for {@link VariableAssignmentSkeleton#create()}.
	 */
	private def VariableAssignmentSkeleton amt(FieldRef leftValue, Expression rightValue) {
		// Actually, type conversion is not needed here, will be performed upon creation.
		// return VariableAssignmentSkeleton.create(leftValue, convertStlExpressionWithTypeConv(rightValue, leftValue.type));
		return VariableAssignmentSkeleton.create(leftValue, rightValue);
	}
	
	private def BoolLiteral falseLiteral() {
		return factory.falseLiteral();
	}
	
	private def BoolLiteral trueLiteral() {
		return factory.trueLiteral();
	}
	
	private def Type realType() {
		return Step7TypeToExprType.s7typeToExprType(ElementaryTypeEnum.REAL);
	}
	
	/**
	 * Returns expression representing {@code low <= expr <= high}.
	 */
	private def Expression betweenIncl(Expression expr, Expression low, Expression high) {
		Preconditions.checkNotNull(expr);
		Preconditions.checkNotNull(low);
		Preconditions.checkNotNull(high);
		Preconditions.checkArgument(expr.type.dataTypeEquals(low.type), "expr and low has different types");
		Preconditions.checkArgument(expr.type.dataTypeEquals(high.type), "expr and high has different types");
		
		// (low <= expr) and (expr <= high)
		return and(
			factory.leq(low, expr),
			factory.leq(expr, high)
		);
	}
	
	private def Expression betweenIncl(Expression expr, long low, long high) {
		Preconditions.checkNotNull(expr);
		Preconditions.checkArgument(expr.type instanceof IntType, "'expr's type is not IntType");
		Preconditions.checkArgument(low < high, "low < high is not respected");
		return betweenIncl(expr, 
			factory.createIntLiteral(low, copy(expr.type) as IntType),
			factory.createIntLiteral(high, copy(expr.type) as IntType)
		);
	}
	
	private def Expression gt(Expression expr, long value) {
		Preconditions.checkNotNull(expr);
		Preconditions.checkArgument(expr.type instanceof IntType || expr.type instanceof FloatType,
			"'expr' needs to have int or float type");
		if (expr.type instanceof IntType) {
			return factory.gt(
				expr,
				factory.createIntLiteral(value, copy(expr.type) as IntType)
			)
		} else {
			return factory.gt(
				expr,
				factory.createFloatLiteral(value, copy(expr.type) as FloatType)
			)
		}
	}
	
	private def Expression lt(Expression expr, long value) {
		Preconditions.checkNotNull(expr);
		Preconditions.checkArgument(expr.type instanceof IntType || expr.type instanceof FloatType,
			"'expr' needs to have int or float type");
		if (expr.type instanceof IntType) {
			return factory.lt(
				expr,
				factory.createIntLiteral(value, copy(expr.type) as IntType)
			)
		} else {
			return factory.lt(
				expr,
				factory.createFloatLiteral(value, copy(expr.type) as FloatType)
			)
		}
	}
	
	private def Expression eq(Expression expr, long value) {
		Preconditions.checkNotNull(expr);
		Preconditions.checkArgument(expr.type instanceof IntType || expr.type instanceof FloatType,
			"'expr' needs to have int or float type");
		if (expr.type instanceof IntType) {
			return factory.eq(
				expr,
				factory.createIntLiteral(value, copy(expr.type) as IntType)
			)
		} else {
			return factory.eq(
				expr,
				factory.createFloatLiteral(value, copy(expr.type) as FloatType)
			)
		}
	}

	/**
	 * Creates a new integer literal with the given value, using
	 * the ACCU1's type as type.
	 */
	private def IntLiteral accuTypeIntLiteral(long value) {
		Preconditions.checkState(accu1.type instanceof IntType);
		return factory.createIntLiteral(value, copy(accu1.type as IntType));
	}
	
	private def Expression convertStlExpression(cern.plcverif.plc.step7.step7Language.Expression expr) {
		if (expr instanceof StwBitRef) {
			switch (expr.mnemonic) {
				case OVERFLOW: return OV.ref
				case OVERFLOW_STORED: return OS.ref
				case BIT_RESULT: return BR.ref
				default: throw new PlcCodeToCfaRuntimeException("Unknown STW bit reference: " + expr.mnemonic.literal)
			}
		}
		
		return exprConverter.convertExpression(expr);
	}
	
	private def convertStlExpressionWithTypeConv(cern.plcverif.plc.step7.step7Language.Expression expr, Type targetType) {
		return exprConverter.convertExpressionWithTypeConv(expr, targetType);
	}
	
	private def convertStlExpressionWithTypeConv(Expression cfaExpr, Type targetType) {
		return exprConverter.convertExpressionWithTypeConv(cfaExpr, targetType);
	}
}
