/******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Martijn Potters - add support for SCL regions in TIA Portal
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.impl

import cern.plcverif.base.models.cfa.builder.TransitionBuilder
import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfabase.LoopLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.ForLoopLocationAnnotation
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.plc.step7.cfa.exceptions.PlcCodeToCfaRuntimeException
import cern.plcverif.plc.step7.cfa.impl.BlockToCfa.PostponedTransitionCreation
import cern.plcverif.plc.step7.cfa.trace.AstCfaVarTrace
import cern.plcverif.plc.step7.cfa.trace.StructuralAstCfaTrace
import cern.plcverif.plc.step7.step7Language.CaseElementValue
import cern.plcverif.plc.step7.step7Language.LabeledSclStatement
import cern.plcverif.plc.step7.step7Language.RangeCaseElementValue
import cern.plcverif.plc.step7.step7Language.SclAssignmentStatement
import cern.plcverif.plc.step7.step7Language.SclCaseStatement
import cern.plcverif.plc.step7.step7Language.SclContinueStatement
import cern.plcverif.plc.step7.step7Language.SclExitStatement
import cern.plcverif.plc.step7.step7Language.SclForStatement
import cern.plcverif.plc.step7.step7Language.SclGotoStatement
import cern.plcverif.plc.step7.step7Language.SclIfStatement
import cern.plcverif.plc.step7.step7Language.SclNullStatement
import cern.plcverif.plc.step7.step7Language.SclRepeatStatement
import cern.plcverif.plc.step7.step7Language.SclReturnStatement
import cern.plcverif.plc.step7.step7Language.SclStatement
import cern.plcverif.plc.step7.step7Language.SclStatementList
import cern.plcverif.plc.step7.step7Language.SclSubroutineCall
import cern.plcverif.plc.step7.step7Language.SclWhileStatement
import cern.plcverif.plc.step7.step7Language.SingleCaseElementValue
import cern.plcverif.plc.step7.step7Language.StatementList
import cern.plcverif.plc.step7.step7Language.SclRegion
import cern.plcverif.plc.step7.typecomputer.Step7TypeComputer
import cern.plcverif.plc.step7.util.SimpleExpressionUtil
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import com.google.common.base.Preconditions
import java.util.List
import java.util.Map

import static org.eclipse.emf.ecore.util.EcoreUtil.copy
import cern.plcverif.plc.step7.step7Language.SclVerificationAssertion
import cern.plcverif.base.common.logging.IPlcverifLogger
import cern.plcverif.plc.step7.step7Language.RangeCaseElementString
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.cfa.cfabase.IfLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.ElseExpression
import cern.plcverif.base.models.cfa.cfabase.RepeatLoopLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.LabelledLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.AssertionAnnotation

/**
 * Class for the SCL statement list representation in CFA.
 */
class SclBlockToCfa extends BlockToCfa {
	/**
	 * The SCL statements to be represented by this object in the {@link BlockToCfa#getAutomaton()}.
	 * Equals to {@link BlockToCfa#statementsOfBlock}, only their types are different.
	 */
	SclStatementList sclStatementsOfBlock;
	
	/**
	 * This map stores the loop annotations for the loop SCL statements.  
	 */
	Map<SclStatement, LoopLocationAnnotation> loopAnnotation = newHashMap();
	
	
	new(SclStatementList statementsOfBlock, AutomatonDeclaration representingAutomaton, AstCfaVarTrace astCfaVarTrace, Step7TypeComputer astTypes, StructuralAstCfaTrace structuralTrace, IPlcverifLogger logger) {
		super(statementsOfBlock, representingAutomaton, astCfaVarTrace, astTypes, structuralTrace, logger);
		this.sclStatementsOfBlock = statementsOfBlock;
	}
	

	
	/**
	 * Converts the given SCL statement list into locations and transitions, starting from the location {@code start}.
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertList(StatementList e, Location start) {
		throw new UnsupportedOperationException('''Impossible generic case. Unknown SCL statement list type: '«e.class.simpleName»'.''');
	}
	
	protected def dispatch Location convertList(Void e, Location start) {
		// to support empty statement lists
		return start;
	}

	/**
	 * Converts the given SCL statement list into locations and transitions, starting from the location {@code start}.
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertList(SclStatementList e, Location start) {
		var previousLoc = start;
		for (statement : e.statements) {
			previousLoc = convertStatement(statement, previousLoc);
			Preconditions.checkState(previousLoc !== null);
		}
		return previousLoc;
	}
	
	/**
	 * Generates CFA representation for the given {@link LabeledSclStatement} labeled statement.
	 * The given start location will be registered for its labels for future use. It will also be annotated with {@link LabelledLocationAnnotation}, one annotation will be created for each label.
	 * The SCL statement that is labeled will be represented according to its type. 
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertStatement(LabeledSclStatement e, Location start) {
		// Save each label that labels the Location 'start'
		for (label : e.labels) {
			registerLabelForLocation(label, start);
			
			// create annotation for the location
			factory.createLabelledLocationAnnotation(start, label.name);
		}
		
		// handle generic case
		val endLoc = convertStatement(e.statement, start);

		return endLoc;
	}

	/**
	 * Generates CFA representation for the given {@link SclNullStatement} null statement (empty semicolon).
	 * <br>
	 * <b>Representation.</b> The given start location will be returned as end location, thus no new location will be created for this statement.
	 * <br>
	 * <b>Annotations.</b> No annotation will be created.
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertStatement(SclNullStatement e, Location start) {
		Preconditions.checkNotNull(start);
		
		// there is nothing to do in this case
		return start;
	}
	
	/**
	 * Generates CFA representation for the given {@link SclAssignmentStatement} variable assignment statement.
	 * <br>
	 * <b>Representation.</b> A new location will be created (returned as end location). Between the new location and the start location, a new assignment transition (with guard <i>true</i>) will be created, which will contain a single variable assignment representing the assignment statement.
	 * <br>
	 * <b>Annotations.</b> No annotation will be created.
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertStatement(SclAssignmentStatement e, Location start) {
		Preconditions.checkNotNull(start);
		
		annotateWithLineNumber(start, e);
		
		val end = factory.createLocation(nextLocName(), representingAutomaton);
		val transition = factory.createAssignmentTransition(nextTransName(), representingAutomaton, start, end, factory.trueLiteral());

		val DataRef leftRef = exprConverter.convertLeftValue(e.leftValue);
		val Expression rightExpr = exprConverter.convertExpressionWithTypeConv(e.rightValue, leftRef.type);
		factory.createAssignment(transition, leftRef, rightExpr);

		return end;
	}
	
	/**
	 * Generates CFA representation for the given {@link SclIfStatement} conditional (IF) statement.
	 * <br>
	 * <b>Representation.</b>
	 * <ul>
	 * 	<li>A new location will be created for each branch of the IF statement (for then branch,
	 *      for each elsif branch if present, and for the else branch, even if it is implicit).</li>
	 *  <li>For each of these locations, a new transition will be created from the start location.
	 *      The condition of the transition represents the branch condition. The branch conditions 
	 *      will be mutually exclusive (i.e., with any variable valuation, exactly one of them will
	 *      be satisfied). The condition of the else branch will always be {@link ElseExpression}.</li>
	 *  <li>A new sink location will be created.</li>
	 *  <li>The content (statement list) of each branch will be represented between the corresponding
	 *      branch location (created in step 1) and the sink location. 
	 * </ul>
	 * <br>
	 * <b>Annotations.</b>
	 * <ul>
	 *   <li>The start location will be annotated with {@link IfLocationAnnotation}.</li>
	 *   <li>The start location will be annotated with line number annotation, see {@link #annotateWithLineNumber(Location, EObject)}.</li>
	 * </ul>
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertStatement(SclIfStatement e, Location start) {
				Preconditions.checkNotNull(start);
		val branchEndLocs = newArrayList();
		
		annotateWithLineNumber(start, e);

		// THEN branch
		Preconditions.checkNotNull(e.condition, "The IF's block condition cannot be null.");
		val thenCondition = exprConverter.convertExpression(e.condition);
		branchEndLocs.add(createConditionalBranch(thenCondition, e.thenBranch, start));
		
		val conditions = newArrayList(thenCondition);
		
		// ELSIF branches
		Preconditions.checkNotNull(e.elsifBranches);
		for (elsifBranch : e.elsifBranches) {
			val branchCondition = exprConverter.convertExpression(elsifBranch.condition);
		
			// cummulativeCondition = NOT(cond[0]) AND NOT(cond[1]) AND NOT ...
			// PERF improve performance
			val cummulativeCondition = factory.and(conditions.map[it | factory.neg(it)]); 
		
			branchEndLocs.add(createConditionalBranch(factory.and(cummulativeCondition, branchCondition), elsifBranch.elsifBranch, start));
			conditions.add(branchCondition);
		}
		
		// ELSE branch
		val elseCondition = factory.createElseExpression();
		branchEndLocs.add(createConditionalBranch(elseCondition, e.elseBranch, start));

		// create sink location for merging conditional branches
		val endLoc = createUniqueLocation(representingAutomaton);
		for (branchEndLoc : branchEndLocs) {
			TransitionBuilder.builderBetween(branchEndLoc, endLoc).name(nextTransName()).build();
			
		}

		// Annotation
		factory.createIfLocationAnnotation(start, endLoc, start.outgoing.get(0));

		return endLoc;

	}
	
	/**
	 * Generates CFA representation for the given {@link SclCaseStatement} conditional (CASE) statement.
	 * <br>
	 * <b>Representation.</b>
	 * <ul>
	 * 	<li>Two new locations will be created for each case element of the CASE statement, preserving the
	 *      original order, the <i>element true</i> location and the <i>element false</i> location.
	 *  <li>From the start location, two transitions are created. A transition to the first case element's
	 *      <i>element true</i> location with the first case element's condition; and a transition with
	 *      condition {@link ElseExpression} to the to the first case element's
	 *      <i>element false</i> location.</li>
	 *  <li>For each case element's <i>element false</i> location (except for the last one), two new
	 *      transitions are created. One transition to the next case element's
	 *      <i>element true</i> location with the next case element's condition; and a transition with
	 *      condition {@link ElseExpression} to the to the next case element's
	 *      <i>element false</i> location.</li>
	 *  <li>A new sink location will be created. This will be returned as end location.</li>
	 *  <li>The content (statement list) of each case element will be represented between the corresponding
	 *      <i>element true</i> location (created in step 1) and the sink location.</li>
	 *  <li>An unconditional transition is created from the last case element's <i>element false</i> location
	 *      to the sink location.</li> 
	 * </ul>
	 * <br>
	 * <b>Annotations.</b>
	 * <ul>
	 *   <li>The start location will be annotated with {@link cern.plcverif.base.models.cfa.cfabase.SwitchLocationAnnotation}.</li>
	 *   <li>Each case element start location will be annotated with {@link cern.plcverif.base.models.cfa.cfabase.CaseLocationAnnotation}.</li>
	 *   <li>The start location will be annotated with line number annotation, see {@link #annotateWithLineNumber(Location, EObject)}.</li>
	 * </ul>
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertStatement(SclCaseStatement e, Location start) {
		Preconditions.checkNotNull(start);
		
		annotateWithLineNumber(start, e);
		
		var Location previousLoc = start;
		
		val Location end = createUniqueLocation(representingAutomaton);
		val selectionExpr = exprConverter.convertExpression(e.selection);
		
		// Switch annotation
		val switchAnnotation = factory.createSwitchLocationAnnotation(start, end, selectionExpr);
		
		for (caseElement : e.elements) {
			val caseTrueLoc = createUniqueLocation(representingAutomaton);
			val caseFalseLoc = createUniqueLocation(representingAutomaton);
			
			// Transition: previousLoc --[caseElement.condition]--> caseTrueLoc
			val caseElementCondition = caseValuesToCondition(selectionExpr, caseElement.values);
			TransitionBuilder.builder()
				.name(nextTransName())
				.source(previousLoc)
				.target(caseTrueLoc)
				.condition(caseElementCondition).build();
			
			// Transition: previousLoc --[NOT caseElement.condition === ELSE]--> caseFalseLoc
			TransitionBuilder.builder()
				.name(nextTransName())
				.source(previousLoc)
				.target(caseFalseLoc)
				.condition(factory.createElseExpression()).build();
				
			// Represent statements of case
			val endOfStatements = convertList(caseElement.statements, caseTrueLoc);
			
			// Transition: endOfStatements ---> end
			TransitionBuilder.builder()
				.name(nextTransName())
				.source(endOfStatements)
				.target(end).build();
					
			previousLoc = caseFalseLoc;
			
			// Annotation
			// caseAnnotation:
			factory.createCaseLocationAnnotation(caseTrueLoc, switchAnnotation);
		}		
		
		if (e.elseStatements !== null) {
			// Annotation
			val annotation = factory.createCaseLocationAnnotation(previousLoc, switchAnnotation);
			switchAnnotation.elseCase = annotation;
			
			// Convert statements
			previousLoc = convertList(e.elseStatements, previousLoc);
		}

		// Transition: previousLoc ---> end
		TransitionBuilder.builder()
				.name(nextTransName())
				.source(previousLoc)
				.target(end).build();

		return end;
	}

	/**
	 * Generates CFA representation for the given {@link SclForStatement} FOR loop statement.
	 * <br>
	 * <b>Representation.</b>
	 * <pre>
	 *                                                 +--------------------------+
	 *                                                 |          start           |
	 *                                                 +--------------------------+
	 *                                                   |
	 *                                                   | loopVariableInit
	 *                                                   | LOOP_VAR := INIT_VALUE;
	 *                                                   v
	 *                 nextCycle                       +--------------------------+
	 *   +-------------------------------------------> |       initialized        | -+
	 *   |                                             +--------------------------+  |
	 *   |                                               |                           |
	 *   |                                               | entry                     |
	 *   |                                               | [LOOP_VAR <= MAX]         |
	 *   |                                               v                           |
	 *   |                                             +--------------------------+  |
	 *   |                                             |        bodyStart         |  |
	 *   |                                             +--------------------------+  |
	 *   |                                               :                           |
	 *   |                                               :                           |
	 *   |                                               v                           |
	 *   |                                             +--------------------------+  | loopExit
	 *   |                                             |    e.getStatements()     |  | [NOT (LOOP_VAR <= MAX)]
	 *   |                                             +--------------------------+  |
	 *   |                                               :                           |
	 *   |                                               :                           |
	 *   |                                               v                           |
	 * +-------------+  LOOP_VAR := LOOP_VAR + STEP;   +--------------------------+  |
	 * | incremented | <------------------------------ |         bodyEnd          |  |
	 * +-------------+                                 +--------------------------+  |
	 *                                                                               |
	 *                                                                               |
	 *                                                                               |
	 *                                                 +--------------------------+  |
	 *                                                 |           end            | <+
	 *                                                 +--------------------------+
	 * </pre>
	 * <br>
	 * <b>Annotations.</b>
	 * <ul>
	 *   <li>The start location will be annotated with {@link ForLoopLocationAnnotation}.</li>
	 *   <li>The start location will be annotated with line number annotation, see {@link #annotateWithLineNumber(Location, EObject)}.</li>
	 * </ul>
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertStatement(SclForStatement e, Location start) {	
		Preconditions.checkNotNull(start);
		
		annotateWithLineNumber(start, e);
		
		// Loop body will be represented between bodyStart and bodyEnd
		val bodyStart = createUniqueLocation(representingAutomaton);
		val bodyEnd = convertList(e.statements, bodyStart);
		
		// Last location
		val end = createUniqueLocation(representingAutomaton);
		
		// Loop variable
		val loopVarRef = exprConverter.convertLeftValue(e.initialStatement.leftValue);
//		Preconditions.checkState(loopVarRef instanceof CfaSingleVariableRef, "The loop variable in a FOR loop must be a single variable.")
//		val loopVar = (loopVarRef as CfaSingleVariableRef).cfaVariable;
		
	
		
		// Increment value
		val incrementValue = exprConverter.convertExpression(Step7LanguageHelper.getForLoopIncrementValue(e));
		// FIXME currently assumed that increment value is a simple expression, but in reality it is a basic expression (may throw exception)
		val incrementValueLong = SimpleExpressionUtil.evaluateSimpleExpression(Step7LanguageHelper.getForLoopIncrementValue(e));
		
		// Represent initial assignment
		val initialized = convertStatement(e.initialStatement, start);
		Preconditions.checkState(initialized.incoming.size == 1);
		val Transition loopVarInit = initialized.incoming.get(0);
		
		// TODO if the 'TO' or 'BY' value is not constant, their current value needs to be stored in a temp variable and used later in the conditions (at least for SCL 5.6)
		if (!SimpleExpressionUtil.isSimpleExpression(e.finalValue)) {
			log.logWarning("If a FOR loop's final value (TO) is altered while the loop is executed, the real execution results may be different from the one in the PLCverif representation.");
		}
		if (e.by && !SimpleExpressionUtil.isSimpleExpression(e.increment)) {
			log.logWarning("If a FOR loop's increment value (BY) is altered while the loop is executed, the real execution results may be different from the one in the PLCverif representation.");
		}
		
		// Transition 'entry': initialized --[condition]--> bodyStart
		// Step value may be negative!
		var sign = ComparisonOperator.LESS_EQ;
		if (incrementValueLong < 0) {
			sign = ComparisonOperator.GREATER_EQ;
		}
		
		val condition = factory.createComparisonExpression(loopVarRef, exprConverter.convertExpression(e.finalValue), sign);
		val loopBodyEntry = TransitionBuilder.builder()
				.name(nextTransName("entry"))
				.source(initialized)
				.target(bodyStart)
				.condition(condition).build();
		
		// Transition 'loopExit': initialized --[NOT condition]--> end		
		val loopExit = TransitionBuilder.builder()
				.name(nextTransName("loopExit"))
				.source(initialized)
				.target(end)
				.condition(factory.neg(condition)).build();		
		
		// Transition to increment the loop variable ('loopVarIncr'):
		// bodyEnd --[TRUE] loopVar := loopVar + incrementValue-->incremented
		val incremented = createUniqueLocation(representingAutomaton);
		
		
		val loopVarIncr = TransitionBuilder.builder()
			.name(nextTransName())
			.source(bodyEnd)
			.target(incremented)
			//.addAssignment(incrementAssignment)
			.build();
		Preconditions.checkState(loopVarIncr instanceof AssignmentTransition);
			
		// incrementAssignment:
		factory.createAssignment(loopVarIncr as AssignmentTransition, copy(loopVarRef), 
			factory.plus(copy(loopVarRef), incrementValue));
		
		// Transition 'nextCycle': incremented --[TRUE]--> initialized
		val loopNextCycle = TransitionBuilder.builder()
				.name(nextTransName("nextCycle"))
				.source(incremented)
				.target(initialized).build();
		
		// Annotation
		val annotation = factory.createForLoopLocationAnnotation(start, loopBodyEntry, loopNextCycle, loopExit, copy(loopVarRef), loopVarInit, loopVarIncr);
		loopAnnotation.put(e, annotation);
		
		return end;
	}
	
	/**
	 * Generates CFA representation for the given {@link SclWhileStatement} WHILE loop statement.
	 * <br>
	 * <b>Representation.</b>
	 * <pre>
	 *               +-------------------+
	 *   +---------> |       start       | -+
	 *   |           +-------------------+  |
	 *   |             |                    |
	 *   |             | entry              |
	 *   |             | [cond]             |
	 *   |             v                    |
	 *   |           +-------------------+  |
	 *   |           |     bodyStart     |  |
	 *   |           +-------------------+  |
	 *   |             :                    |
	 *   | nextCycle   :                    |
	 *   |             v                    |
	 *   |           +-------------------+  | loopExit
	 *   |           | e.getStatements() |  | [NOT cond]
	 *   |           +-------------------+  |
	 *   |             :                    |
	 *   |             :                    |
	 *   |             v                    |
	 *   |           +-------------------+  |
	 *   +---------- |      bodyEnd      |  |
	 *               +-------------------+  |
	 *                                      |
	 *                                      |
	 *                                      |
	 *               +-------------------+  |
	 *               |        end        | <+
	 *               +-------------------+
	 *  
	 * </pre>
	 * <br>
	 * <b>Annotations.</b>
	 * <ul>
	 *   <li>The start location will be annotated with {@link cern.plcverif.base.models.cfa.cfabase.WhileLoopLocationAnnotation}.</li>
	 *   <li>The start location will be annotated with line number annotation, see {@link #annotateWithLineNumber(Location, EObject)}.</li>
	 * </ul>
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertStatement(SclWhileStatement e, Location start) {
		Preconditions.checkNotNull(start);
		 
		annotateWithLineNumber(start, e);
		 
		// Loop body will be represented between bodyStart and bodyEnd
		val bodyStart = createUniqueLocation(representingAutomaton);
		val bodyEnd = convertList(e.statements, bodyStart);
		
		// Last location
		val end = createUniqueLocation(representingAutomaton);
		
		// Transition 'entry': start --[condition]--> bodyStart
		val condition = exprConverter.convertExpression(e.entryCondition);
		val loopBodyEntry = TransitionBuilder.builder()
				.name(nextTransName("entry"))
				.source(start)
				.target(bodyStart)
				.condition(condition).build();
		
		// Transition 'loopExit': start --[NOT condition]--> end		
		val loopExit = TransitionBuilder.builder()
				.name(nextTransName("loopExit"))
				.source(start)
				.target(end)
				.condition(factory.neg(condition)).build();		
		
		// Transition 'nextCycle': bodyEnd --[TRUE]--> start
		val loopNextCycle = TransitionBuilder.builder()
				.name(nextTransName("nextCycle"))
				.source(bodyEnd)
				.target(start).build();
		
		// Annotation
		val annotation = factory.createWhileLoopLocationAnnotation(start, loopBodyEntry, loopNextCycle, loopExit);
		loopAnnotation.put(e, annotation);
		
		return end;
	}
	
	/**
	 * Generates CFA representation for the given {@link SclRepeatStatement} REPEAT loop statement.
	 * <br>
	 * <b>Representation.</b>
	 * <pre>
	 * +-------------------+
	 * |       start       | <+
	 * +-------------------+  |
	 *   |                    |
	 *   | entry              |
	 *   v                    |
	 * +-------------------+  |
	 * |     bodyStart     |  |
	 * +-------------------+  |
	 *   :                    |
	 *   :                    | nextCycle
	 *   v                    | [NOT exitCondition]
	 * +-------------------+  |
	 * | e.getStatements() |  |
	 * +-------------------+  |
	 *   :                    |
	 *   :                    |
	 *   v                    |
	 * +-------------------+  |
	 * |      bodyEnd      | -+
	 * +-------------------+
	 *   |
	 *   | loopExit
	 *   | [exitCondition]
	 *   v
	 * +-------------------+
	 * |        end        |
	 * +-------------------+
	 * </pre>
	 * <br>
	 * <b>Annotations.</b>
	 * <ul>
	 *   <li>The start location will be annotated with {@link RepeatLoopLocationAnnotation}.</li>
	 *   <li>The start location will be annotated with line number annotation, see {@link #annotateWithLineNumber(Location, EObject)}.</li>
	 * </ul>
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertStatement(SclRepeatStatement e, Location start) {
		Preconditions.checkNotNull(start);
		
		annotateWithLineNumber(start, e);
		 
		// Loop body will be represented between bodyStart and bodyEnd
		val bodyStart = createUniqueLocation(representingAutomaton);
		val bodyEnd = convertList(e.statements, bodyStart);
		
		// Last location
		val end = createUniqueLocation(representingAutomaton);
		
		// Transition 'entry': start --[TRUE]--> bodyStart
		val loopBodyEntry = TransitionBuilder.builder()
				.name(nextTransName("entry"))
				.source(start)
				.target(bodyStart).build();
		
		// Transition 'loopExit': bodyEnd --[terminationCondition]--> end		
		val condition = exprConverter.convertExpression(e.exitCondition);
		val loopExit = TransitionBuilder.builder()
				.name(nextTransName("loopExit"))
				.source(bodyEnd)
				.target(end)
				.condition(condition).build();		
		
		// Transition 'nextCycle': bodyEnd --[NOT terminationCondition]--> start
		val loopNextCycle = TransitionBuilder.builder()
				.name(nextTransName("nextCycle"))
				.source(bodyEnd)
				.target(start)
				.condition(factory.neg(condition)).build();
		
		// Annotation
		val annotation = factory.createRepeatLoopLocationAnnotation(start, loopBodyEntry, loopNextCycle, loopExit);
		loopAnnotation.put(e, annotation);
		
		return end;
	}
	
	/**
	 * Generates CFA representation for the given {@link SclSubroutineCall} call statement.
	 * It is handled by the generic {@link BlockToCfa#conconvertCallStatement(SclSubroutineCall, Location)}.
	 * 
	 * @see BlockToCfa#conconvertCallStatement(SclSubroutineCall, Location)
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertStatement(SclSubroutineCall e, Location start) {
		annotateWithLineNumber(start, e);
		return convertCallStatement(e, start);
	}
	
	/**
	 * Generates CFA representation for the given {@link SclReturnStatement} RETURN statement.
	 * <br>
	 * <b>Representation.</b>
	 * It registers a postponed transition creation from the given start location to the end location of the representing automaton.
	 * A newly created location without any incoming transition will be returned as end location.
	 * <br>
	 * <b>Annotations.</b>
	 * <ul>
	 *   <li>The transition will be annotated with {@link cern.plcverif.base.models.cfa.cfabase.BlockReturnTransitionAnnotation} (once created).</li>
	 *   <li>The start location will be annotated with line number annotation, see {@link #annotateWithLineNumber(Location, EObject)}.</li>
	 * </ul>
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertStatement(SclReturnStatement e, Location start) {
		annotateWithLineNumber(start, e);
		
		// this operation has to be postponed, as the block is still being processed
		postpone(new PostponedTransitionCreation(representingAutomaton, start, nextTransName("exit")) {
			override Location getTargetLocation() {
				if (representingAutomaton.endLocation === null) {
					throw new PlcCodeToCfaRuntimeException("Target of RETURN statement (end location of automaton) cannot be found.");
				}
				return representingAutomaton.endLocation;
			}
			
			override createTransitionAnnotations(Transition transition) {
				factory.createBlockReturnTransitionAnnotation(transition);
			}
		}
		);
		
		return createUniqueLocation(representingAutomaton);
	}
	
	/**
	 * Generates CFA representation for the given {@link SclContinueStatement} CONTINUE statement.
	 * <br>
	 * <b>Representation.</b>
	 * It registers a postponed transition creation from the given start location to the start of the next cycle in the containing loop. If the containing loop is a FOR loop, the transition will lead to the location preceding the increment of the loop counter. 
	 * A newly created location without any incoming transition will be returned as end location.
	 * <br>
	 * <b>Annotations.</b>
	 * <ul>
	 *   <li>The transition will be annotated with {@link cern.plcverif.base.models.cfa.cfabase.ContinueTransitionAnnotation} (once created).</li>
	 *   <li>The start location will be annotated with line number annotation, see {@link #annotateWithLineNumber(Location, EObject)}.</li>
	 * </ul>
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertStatement(SclContinueStatement e, Location start) {
		Preconditions.checkNotNull(start);
		
		annotateWithLineNumber(start, e);
		
		val enclosingLoop = Step7LanguageHelper.getContainingLoop(e);
		if (!enclosingLoop.isPresent) {
			throw new PlcCodeToCfaRuntimeException("Invalid CONTINUE statement: no enclosing loop has been found.");
		}
		
		// this operation has to be postponed, as the loop is still being processed
		postpone(new PostponedTransitionCreation(representingAutomaton, start, nextTransName("continue")) {
			override Location getTargetLocation() {
				val loopAnnotation = loopAnnotation.get(enclosingLoop.get());
				if (loopAnnotation === null || loopAnnotation.loopExit === null) {
					throw new PlcCodeToCfaRuntimeException('''Target of CONTINUE statement (line: «Step7LanguageHelper.lineInSource(e)») cannot be found.''');
				}
				
				if (loopAnnotation instanceof ForLoopLocationAnnotation) {
					// the FOR's loop counter needs to be incremented even if there is a CONTINUE statement
					return loopAnnotation.loopVariableIncrement.source;
				} else {
					return loopAnnotation.loopNextCycle.target;
				}
			}	
					
			override createTransitionAnnotations(Transition transition) {
				factory.createContinueTransitionAnnotation(transition);
			}
		});
		
		return createUniqueLocation(representingAutomaton);
	}
	
	/**
	 * Generates CFA representation for the given {@link SclExitStatement} EXIT statement.
	 * <br>
	 * <b>Representation.</b>
	 * It registers a postponed transition creation from the given start location to the end location of the containing loop. 
	 * A newly created location without any incoming transition will be returned as end location.
	 * <br>
	 * <b>Annotations.</b>
	 * <ul>
	 *   <li>The transition will be annotated with {@link cern.plcverif.base.models.cfa.cfabase.ExitTransitionAnnotation} (once created).</li>
	 *   <li>The start location will be annotated with line number annotation, see {@link #annotateWithLineNumber(Location, EObject)}.</li>
	 * </ul>
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */	
	protected def dispatch Location convertStatement(SclExitStatement e, Location start) {
		annotateWithLineNumber(start, e);
		
		val enclosingLoop = Step7LanguageHelper.getContainingLoop(e);
		if (!enclosingLoop.isPresent) {
			throw new PlcCodeToCfaRuntimeException('''Invalid EXIT statement (line: «Step7LanguageHelper.lineInSource(e)»): no enclosing loop has been found.''');
		}
		
		// this operation has to be postponed, as the loop is still being processed
		postpone(new PostponedTransitionCreation(representingAutomaton, start, nextTransName("exit")) {
			override Location getTargetLocation() {
				val loopAnnotation = loopAnnotation.get(enclosingLoop.get());
				if (loopAnnotation === null || loopAnnotation.loopExit === null) {
					throw new PlcCodeToCfaRuntimeException('''Target of EXIT statement (line: «Step7LanguageHelper.lineInSource(e)») cannot be found.''');
				}
				return loopAnnotation.loopExit.target;
			}
			
			override createTransitionAnnotations(Transition transition) {
				factory.createExitTransitionAnnotation(transition);
			}
		});
		
		return createUniqueLocation(representingAutomaton);
	}
	
	/**
	 * Generates CFA representation for the given {@link SclGotoStatement} GOTO statement.
	 * <br>
	 * <b>Representation.</b>
	 * It registers a postponed transition creation from the given start location to the location having the given label in the same automaton declaration. 
	 * A newly created location without any incoming transition will be returned as end location.
	 * <br>
	 * <b>Annotations.</b>
	 * <ul>
	 *   <li>The transition will be annotated with {@link cern.plcverif.base.models.cfa.cfabase.GotoTransitionAnnotation} (once created).</li>
	 *   <li>The start location will be annotated with line number annotation, see {@link #annotateWithLineNumber(Location, EObject)}.</li>
	 * </ul>
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */	
	protected def dispatch Location convertStatement(SclGotoStatement e, Location start) {
		Preconditions.checkNotNull(start);
		
		annotateWithLineNumber(start, e);
		
		postpone(new PostponedTransitionCreation(representingAutomaton, start, nextTransName("jump")) {
			override getTargetLocation() {
				return getRegisteredLocationOfLabel(e.label);
			}		
				
			override createTransitionAnnotations(Transition transition) {
				factory.createGotoTransitionAnnotation(transition, e.label.name);
			}
		});
		
		return createUniqueLocation(representingAutomaton);
	}
	
	/**
	 * Generates CFA representation for the given {@link SclVerificationAssertion} verification assertion.
	 * <br>
	 * <b>Representation.</b> The given start location will be returned as end location, thus no new location will be created for this statement.
	 * <br>
	 * <b>Annotations.</b>
	 * <ul>
	 *   <li>The location will be annotated with {@link AssertionAnnotation}.</li>
	 *   <li>The start location will be annotated with line number annotation, see {@link #annotateWithLineNumber(Location, EObject)}.</li>
	 * </ul>
	 * 
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertStatement(SclVerificationAssertion e, Location start) {
		annotateWithLineNumber(start, e);
		return this.convertVerificationAssertion(e, start);
	}
	
	/**
	 * Generates CFA representation for the given {@link SclRegion} statement.
	 * <br>
	 * <b>Representation.</b> The body of an SCL region will be converted as a {@link SclStatementList}.
	 * No new location will be created for the region statement itself.
	 * <br>
	 * <b>Annotations.</b> None.
	 * @param e SCL statement to be represented
	 * @param start Start location
	 * @return The end of the representation where the succeeding statement should start.
	 */
	protected def dispatch Location convertStatement(SclRegion e, Location start) {
		return convertList(e.body, start);
		
	}
	
	// HELPER METHODS
	
	// IF statements
	private def createConditionalBranch(Expression condition, SclStatementList statementList, Location start) {
		// statementList may be null if the branch is empty
		// create new location which will be after the conditional transition
		val afterConditionLoc = createUniqueLocation(representingAutomaton);

		// create conditional transition
		val Expression conditionOrTrue = if (condition === null) factory.trueLiteral() else condition;
		factory.createAssignmentTransition(nextTransName(), representingAutomaton, start, afterConditionLoc, conditionOrTrue);
		
		// represent the content of the branch
		if (statementList === null) {
			// statementList may be empty e.g. if the ELSE branch is empty
			return afterConditionLoc;
		} else {
			return convertList(statementList, afterConditionLoc);
		}
	}
	
	// CASE statements
	private def Expression caseValuesToCondition(Expression selection, List<CaseElementValue> list) {
		return factory.or(list.map[it | caseValueToCondition(selection, it)].toList);
	}
	
	private def dispatch Expression caseValueToCondition(Expression selection, CaseElementValue caseElemVal) {
		throw new UnsupportedOperationException('''Impossible generic case in caseValueToCondition. Missing dedicated support for type '«caseElemVal.class.simpleName»'.'''); 
	}
	
	private def dispatch Expression caseValueToCondition(Expression selection, SingleCaseElementValue caseElemVal) {
		return factory.eq(
			copy(selection),
			exprConverter.convertRefToCfaExpression(caseElemVal.value)
		);
	}
	
	private def dispatch Expression caseValueToCondition(Expression selection, RangeCaseElementValue caseElemVal) {
		return caseValueRangeToCondition(selection, 
			exprConverter.convertExpression(Step7LanguageHelper.unwrapRefExpression(caseElemVal.lowerBound, exprConverter.getAstTypes())),
			exprConverter.convertExpression(Step7LanguageHelper.unwrapRefExpression(caseElemVal.upperBound, exprConverter.getAstTypes()))
		);
	}
	
	private def dispatch Expression caseValueToCondition(Expression selection, RangeCaseElementString caseElemVal) {
		Preconditions.checkArgument(selection.type instanceof IntType, "The selection expression of a CASE must be an integer type.");
		
		val selectionType = selection.type as IntType;
		return caseValueRangeToCondition(selection, 
			factory.createIntLiteral(Step7LanguageHelper.caseValueRangeLower(caseElemVal), copy(selectionType)),
			factory.createIntLiteral(Step7LanguageHelper.caseValueRangeUpper(caseElemVal), copy(selectionType))
		);
	}
	
	private def Expression caseValueRangeToCondition(Expression selection, Expression lowerBound, Expression upperBound) {
		val lowerConstraint = factory.createComparisonExpression(
			copy(selection),
			lowerBound,
			ComparisonOperator.GREATER_EQ
		);

		val upperConstraint = factory.createComparisonExpression(
			copy(selection),
			upperBound,
			ComparisonOperator.LESS_EQ
		);

		return factory.and(lowerConstraint, upperConstraint);
	}
}
