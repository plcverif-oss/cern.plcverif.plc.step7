/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.cfa.impl;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.plc.step7.cfa.util.MemoryAddressArrayUtil;
import cern.plcverif.plc.step7.datatypes.S7AddressMemoryIdentifier;
import cern.plcverif.plc.step7.datatypes.S7MemoryAddress;
import cern.plcverif.plc.step7.step7Language.ArrayRef;
import cern.plcverif.plc.step7.step7Language.DataBlock;
import cern.plcverif.plc.step7.step7Language.DirectNamedRef;
import cern.plcverif.plc.step7.step7Language.ExecutableProgramUnit;
import cern.plcverif.plc.step7.step7Language.FbOrUdtDT;
import cern.plcverif.plc.step7.step7Language.GlobalVariableBlock;
import cern.plcverif.plc.step7.step7Language.MemoryAddress;
import cern.plcverif.plc.step7.step7Language.ProgramFile;
import cern.plcverif.plc.step7.step7Language.ProgramUnit;
import cern.plcverif.plc.step7.step7Language.SubroutineCall;
import cern.plcverif.plc.step7.step7Language.Variable;
import cern.plcverif.plc.step7.step7Language.util.Step7LanguageSwitch;
import cern.plcverif.plc.step7.util.Step7LanguageHelper;

/**
 * Class to collect the scope of the STEP 7 PLC code to CFA translation. It takes
 * an initial scope of one or more program files (initial scope), then
 * recursively explores the dependencies. It collects:
 * <ul>
 * <li>The program units which are present in the initial scope, or transitively
 * called / used as data type from the initial scope,
 * <li>The IO addresses used in any of the program units.
 * </ul>
 */
public final class TranslationScope {
	private class TranslationScopeCollectorSwitch extends Step7LanguageSwitch<Void> {
		private final String contextName;

		public TranslationScopeCollectorSwitch(String contextName) {
			super();
			this.contextName = contextName;
		}

		@Override
		public Void caseSubroutineCall(SubroutineCall e) {
			if (e.getCalledUnit() instanceof ProgramUnit) {
				foundBlock((ProgramUnit) e.getCalledUnit(), contextName);
			}
			if (e.getDataBlock() != null) {
				foundBlock(e.getDataBlock(), contextName);
			}
			return null;
		}

		@Override
		public Void caseFbOrUdtDT(FbOrUdtDT e) {
			foundBlock(e.getType(), contextName);
			return null;
		}

		@Override
		public Void caseMemoryAddress(MemoryAddress e) {
			if (e.getValue().getMemoryIdentifier() == S7AddressMemoryIdentifier.L) {
				ExecutableProgramUnit programUnit = EmfHelper.getContainerOfType(e, ExecutableProgramUnit.class);
				foundLocalMemoryAddress(programUnit, e.getValue(), contextName);
			} else {
				foundMemoryAddress(e.getValue(), contextName);
			}
			return null;
		}

		@Override
		public Void caseArrayRef(ArrayRef e) {
			if (MemoryAddressArrayUtil.isMemoryAddressArray(e)) {
				// An array reference can act like a memory address, e.g. `M[0,1]` is the same as `%M0.1`
				foundMemoryAddress(MemoryAddressArrayUtil.toMemoryAddress(e), contextName);
			}
			return null;
		}

		@Override
		public Void caseDirectNamedRef(DirectNamedRef e) {
			if (e.getRef() instanceof Variable
					&& EmfHelper.countContainersOfTypeRecursively(e.getRef(), GlobalVariableBlock.class) >= 1) {
				// It refers to a variable declared in a global variable block.
				Variable variable = (Variable) e.getRef();
				if (!Step7LanguageHelper.isStructMemberVariable(variable)) {
					// Variables which are part of structs are skipped, they
					// don't need to be directly represented.
					foundGlobalVar(variable, contextName);
				}
			} else if (e.getRef() instanceof DataBlock) {
				foundBlock((DataBlock) e.getRef(), contextName);
			}
			return null;
		}
	}

	/**
	 * Remaining program units to be analyzed.
	 */
	private final Queue<ProgramUnit> unitsToAnalyze = new ArrayDeque<>();

	/**
	 * Program units within the scope. Kept in the order of their exploration.
	 */
	private final List<ProgramUnit> unitsInScope = new ArrayList<>();

	/**
	 * Program files within the scope.
	 */
	private Set<ProgramFile> programFilesInScope = null;

	/**
	 * Memory addresses used in any of the units in the scope
	 * ({@code unitsInScope}).
	 */
	private final List<S7MemoryAddress> memoryAddressesInScope = new ArrayList<>();
	
	/**
	 * Local memory addresses used in any of the units in the scope
	 * ({@code unitsInScope}).
	 */
	private final Map<ExecutableProgramUnit, List<S7MemoryAddress>> localMemoryAddressesInScope = new HashMap<>();

	/**
	 * Global variables used in any of the units in the scope
	 * ({@code unitsInScope}).
	 */
	private final List<Variable> globalVarsInScope = new ArrayList<>();

	private TranslationScope(ProgramUnit initialScope) {
		Preconditions.checkNotNull(initialScope);
		foundBlock(initialScope, "(constructor)");
	}

	private TranslationScope(Iterable<ProgramUnit> initialScope) {
		Preconditions.checkNotNull(initialScope);

		for (ProgramUnit unit : initialScope) {
			foundBlock(unit, "(constructor)");
		}
	}

	/**
	 * Collects the translation scope for the given program unit, i.e., it
	 * recursively collects all required ProgramUnits, and the memory addresses
	 * used in them.
	 */
	public static TranslationScope collect(ProgramUnit initialScope) {
		TranslationScope collector = new TranslationScope(initialScope);
		collector.collect();
		return collector;
	}

	/**
	 * Collects the translation scope for the given program units, i.e., it
	 * recursively collects all required ProgramUnits, and the memory addresses
	 * used in them.
	 */
	public static TranslationScope collect(Iterable<ProgramUnit> initialScope) {
		TranslationScope collector = new TranslationScope(initialScope);
		collector.collect();
		return collector;
	}

	/**
	 * The program units contained in the scope.
	 */
	public List<ProgramUnit> getProgramUnitsInScope() {
		return ImmutableList.copyOf(unitsInScope);
	}

	/**
	 * The program files in the scope.
	 *
	 * This set is computed every time it is accessed based on the program units
	 * in the scope.
	 */
	public Set<ProgramFile> getProgramFilesInScope() {
		return programFilesInScope;
	}

	/**
	 * The memory addresses in the scope (i.e., memory addresses used in any of
	 * the program units within the scope).
	 */
	public List<S7MemoryAddress> getMemoryAddressesInScope() {
		return ImmutableList.copyOf(memoryAddressesInScope);
	}
	
	/**
	 * The local memory addresses in the given program unit.
	 */
	public List<S7MemoryAddress> getLocalMemoryAddressesInProgramUnit(ExecutableProgramUnit programUnit) {
		return ImmutableList.copyOf(localMemoryAddressesInScope.getOrDefault(programUnit, Collections.emptyList()));
	}

	/**
	 * The global variables in the scope (i.e., global variables used in any of
	 * the program units within the scope).
	 */
	public List<Variable> getGlobalVarsInScope() {
		return ImmutableList.copyOf(globalVarsInScope);
	}

	// Internal implementation

	private void collect() {
		while (!unitsToAnalyze.isEmpty()) {
			analyze(unitsToAnalyze.remove());
		}

		this.programFilesInScope = ImmutableSet.copyOf(unitsInScope.stream()
				.map(it -> EmfHelper.getContainerOfType(it, ProgramFile.class)).collect(Collectors.toSet()));
	}

	private void analyze(ProgramUnit unitToAnalyze) {
		Preconditions.checkNotNull(unitToAnalyze);
		Step7LanguageSwitch<Void> scopeCollectorSwitch = new TranslationScopeCollectorSwitch(unitToAnalyze.getName());

		for (EObject item : IteratorExtensions.toIterable(unitToAnalyze.eAllContents())) {
			scopeCollectorSwitch.doSwitch(item);
		}
	}

	private void foundBlock(ProgramUnit unit, String contextName) {
		// contextName is used only for logging
		Preconditions.checkNotNull(unit);

		// PERF if it is a significant performance overhead, keep a copy of
		// requiredUnits as HashSet too
		if (!unitsInScope.contains(unit)) {
			this.unitsInScope.add(unit);
			this.unitsToAnalyze.add(unit);
		}
	}

	private void foundMemoryAddress(S7MemoryAddress address, String contextName) {
		// contextName is used only for logging
		Preconditions.checkNotNull(address);
		Preconditions.checkArgument(address.getMemoryIdentifier() != S7AddressMemoryIdentifier.L);

		// PERF if it is a significant performance overhead, keep a copy of
		// memoryAddressesInScope as HashSet too
		if (!memoryAddressesInScope.contains(address)) {
			memoryAddressesInScope.add(address);
		}
	}
	
	private void foundLocalMemoryAddress(ExecutableProgramUnit programUnit, S7MemoryAddress address, String contextName) {
		// contextName is used only for logging
		Preconditions.checkNotNull(programUnit);
		Preconditions.checkNotNull(address);
		Preconditions.checkArgument(address.getMemoryIdentifier() == S7AddressMemoryIdentifier.L);

		if (!localMemoryAddressesInScope.containsKey(programUnit)) {
			localMemoryAddressesInScope.put(programUnit, new ArrayList<>());
		}
		
		List<S7MemoryAddress> list = localMemoryAddressesInScope.get(programUnit);
		// PERF if it is a significant performance overhead, improve
		if (!list.contains(address)) {
			list.add(address);
		}
	}

	private void foundGlobalVar(Variable globalVar, String contextName) {
		// contextName is used only for logging
		Preconditions.checkNotNull(globalVar);

		// PERF if it is a significant performance overhead, keep a copy of
		// globalVarsInScope as HashSet too
		if (!globalVarsInScope.contains(globalVar)) {
			globalVarsInScope.add(globalVar);
		}
	}
}