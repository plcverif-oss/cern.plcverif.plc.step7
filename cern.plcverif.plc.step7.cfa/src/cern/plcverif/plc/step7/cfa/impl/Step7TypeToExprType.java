/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.cfa.impl;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfabase.DataDirection;
import cern.plcverif.base.models.expr.BoolType;
import cern.plcverif.base.models.expr.ElementaryType;
import cern.plcverif.base.models.expr.FloatType;
import cern.plcverif.base.models.expr.FloatWidth;
import cern.plcverif.base.models.expr.InitialValue;
import cern.plcverif.base.models.expr.IntType;
import cern.plcverif.base.models.expr.Type;
import cern.plcverif.plc.step7.cfa.exceptions.PlcCodeToCfaRuntimeException;
import cern.plcverif.plc.step7.datatypes.S7AddressMemoryIdentifier;
import cern.plcverif.plc.step7.datatypes.S7AddressMemorySize;
import cern.plcverif.plc.step7.datatypes.S7IntegerType;
import cern.plcverif.plc.step7.datatypes.S7RealType;
import cern.plcverif.plc.step7.step7Language.ElementaryDT;
import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum;
import cern.plcverif.plc.step7.step7Language.GlobalVariableDeclarationDirection;
import cern.plcverif.plc.step7.step7Language.VariableDeclarationDirection;
import cern.plcverif.plc.step7.util.DataTypeUtil;
import cern.plcverif.plc.step7.util.Step7LanguageHelper;

/**
 * Utility class for data type conversion between STEP 7 PLC types (e.g., BOOL,
 * INT) and Expr ({@code cern.plcverif.base.models.expr}) metamodel types.
 */
public final class Step7TypeToExprType {
	private static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;

	private Step7TypeToExprType() {
		// Utility class.
	}
	
	public static ElementaryType s7typeToExprType(ElementaryDT astType) {
		Preconditions.checkNotNull(astType);
		return s7typeToExprType(astType.getType());
	}

	public static Type s7typeToExprType(String type) {
		ElementaryTypeEnum typeEnum = ElementaryTypeEnum.valueOf(type.toUpperCase());
		if (typeEnum == null) {
			throw new PlcCodeToCfaRuntimeException("Unknown type in type conversion: " + type);
		}
		return s7typeToExprType(typeEnum);
	}

	public static boolean isSigned(ElementaryTypeEnum type) {
		// PERF Unnecessary to create a new type object here
		Type cfaType = s7typeToExprType(type);
		if (cfaType instanceof IntType) {
			return ((IntType) cfaType).isSigned();
		} else if (cfaType instanceof BoolType) {
			return false;
		} else if (cfaType instanceof FloatType) {
			return true;
		} else {
			throw new UnsupportedOperationException("Unable to determine if signed: " + type);
		}
	}

	public static ElementaryType s7typeToExprType(ElementaryTypeEnum type) {
		if (DataTypeUtil.isBoolean(type)) {
			return FACTORY.createBoolType();
		}

		int bitWidth = DataTypeUtil.getBitWidth(type);

		if (DataTypeUtil.isInteger(type) || DataTypeUtil.isTimeLike(type) || DataTypeUtil.isDate(type)
				|| DataTypeUtil.isDateTime(type) || DataTypeUtil.isBitArray(type) || DataTypeUtil.isTimeOfDay(type)
				|| DataTypeUtil.isChar(type)) {
			return FACTORY.createIntType(DataTypeUtil.isSigned(type), bitWidth);
		}

		if (DataTypeUtil.isReal(type)) {
			return FACTORY.createFloatType(FloatWidth.get(bitWidth));
		}

		throw new PlcCodeToCfaRuntimeException("Unsupported data type in Step7TypeToExprType.s7typeToExprType: '%s'.",
				type);
	}

	public static IntType s7IntTypeToCfaType(S7IntegerType type) {
		switch (type) {
		case BYTE:
			return (IntType) s7typeToExprType(ElementaryTypeEnum.BYTE);
		case DINT:
			return (IntType) s7typeToExprType(ElementaryTypeEnum.DINT);
		case DWORD:
			return (IntType) s7typeToExprType(ElementaryTypeEnum.DWORD);
		case INT:
			return (IntType) s7typeToExprType(ElementaryTypeEnum.INT);
		case UDINT:
			return (IntType) s7typeToExprType(ElementaryTypeEnum.UDINT);
		case UINT:
			return (IntType) s7typeToExprType(ElementaryTypeEnum.UINT);
		case WORD:
			return (IntType) s7typeToExprType(ElementaryTypeEnum.WORD);
		case BOOL:
		case Unknown:
		default:
			throw new UnsupportedOperationException("Unable to convert the following S7IntegerType: " + type);
		}
	}

	public static FloatType s7RealTypeToCfaType(S7RealType type) {
		switch (type) {
		case REAL:
			return (FloatType) s7typeToExprType(ElementaryTypeEnum.REAL);
		case LREAL:
			return (FloatType) s7typeToExprType(ElementaryTypeEnum.LREAL);
		case Unknown:
		default:
			throw new UnsupportedOperationException("Unable to convert the following S7RealType: " + type);
		}
	}

	public static Type s7MemorySizeToExprType(S7AddressMemorySize size) {
		switch (size) {
		case Boolean:
			return FACTORY.createBoolType();
		case Byte:
			return s7typeToExprType(ElementaryTypeEnum.BYTE);
		case Dword:
			return s7typeToExprType(ElementaryTypeEnum.DWORD);
		case Word:
			return s7typeToExprType(ElementaryTypeEnum.WORD);
		default:
			break;
		}

		throw new PlcCodeToCfaRuntimeException(
				"Unsupported memory size in Step7TypeToExprType.s7MemorySizeToExprType: '%s'.", size);
	}

	public static ElementaryTypeEnum s7MemorySizeToS7VarType(S7AddressMemorySize size) {
		switch (size) {
		case Boolean:
			return ElementaryTypeEnum.BOOL;
		case Byte:
			return ElementaryTypeEnum.BYTE;
		case Dword:
			return ElementaryTypeEnum.DWORD;
		case Word:
			return ElementaryTypeEnum.WORD;
		default:
			break;
		}

		throw new PlcCodeToCfaRuntimeException(
				"Unsupported memory size in Step7TypeToExprType.s7MemorySizeToS7VarType: '%s'.", size);
	}

	public static VariableDeclarationDirection toLocalVarDir(GlobalVariableDeclarationDirection globalDir) {
		switch (globalDir) {
		case INPUT:
			return VariableDeclarationDirection.INPUT;
		case OUTPUT:
			return VariableDeclarationDirection.OUTPUT;
		case STATIC:
			return VariableDeclarationDirection.STATIC;
		default:
			throw new UnsupportedOperationException("Unexpected global variable declaration direction: " + globalDir);
		}
	}

	public static InitialValue defaultCfaValue(ElementaryTypeEnum type) {
		Type cfaType = s7typeToExprType(type);

		if (type == ElementaryTypeEnum.BOOL) {
			Preconditions.checkState(cfaType instanceof BoolType);
			return FACTORY.createBoolLiteral(false, (BoolType) cfaType);
		}

		if (DataTypeUtil.isInteger(type) || DataTypeUtil.isTimeOrDateLike(type)) {
			Preconditions.checkState(cfaType instanceof IntType);
			return FACTORY.createIntLiteral(0, (IntType) cfaType);
		}

		if (DataTypeUtil.isReal(type)) {
			Preconditions.checkState(cfaType instanceof FloatType);
			return FACTORY.createFloatLiteral(0.0, (FloatType) cfaType);
		}

		if (DataTypeUtil.isBitArray(type)) {
			Preconditions.checkState(cfaType instanceof IntType);
			return FACTORY.createIntLiteral(0, (IntType) cfaType);
		}

		throw new PlcCodeToCfaRuntimeException("Unsupported data type in Step7TypeToExprType.defaultValue: '%s'.",
				type);
	}

	public static DataDirection astToCfaDirection(GlobalVariableDeclarationDirection astDirection) {
		return astToCfaDirection(toLocalVarDir(astDirection));
	}

	public static DataDirection astToCfaDirection(S7AddressMemoryIdentifier astDirection) {
		return astToCfaDirection(Step7LanguageHelper.memoryToVarDir(astDirection));
	}

	public static DataDirection astToCfaDirection(VariableDeclarationDirection astDirection) {
		switch (astDirection) {
		case INPUT:
			return DataDirection.INPUT;
		case OUTPUT:
			return DataDirection.OUTPUT;
		case INOUT:
			return DataDirection.INOUT;
		case STATIC:
			return DataDirection.LOCAL;
		case TEMP:
			return DataDirection.TEMP;
		default:
			throw new PlcCodeToCfaRuntimeException(
					"Unsupported data type in Step7TypeToExprType.astToCfaDirection: '%s'.", astDirection);
		}
	}

}
