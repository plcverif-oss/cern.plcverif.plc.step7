/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.impl

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.common.logging.IPlcverifLogger
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.cfa.builder.TransitionBuilder
import cern.plcverif.base.models.cfa.cfabase.ArrayType
import cern.plcverif.base.models.cfa.cfabase.AssertionAnnotation
import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.Call
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructureRef
import cern.plcverif.base.models.expr.ElementaryType
import cern.plcverif.plc.step7.cfa.exceptions.PlcCodeToCfaRuntimeException
import cern.plcverif.plc.step7.cfa.trace.AstCfaVarTrace
import cern.plcverif.plc.step7.cfa.trace.StructuralAstCfaTrace
import cern.plcverif.plc.step7.step7Language.CallParameter
import cern.plcverif.plc.step7.step7Language.CallParameterList
import cern.plcverif.plc.step7.step7Language.DirectNamedRef
import cern.plcverif.plc.step7.step7Language.EnCallParameterItem
import cern.plcverif.plc.step7.step7Language.Expression
import cern.plcverif.plc.step7.step7Language.Function
import cern.plcverif.plc.step7.step7Language.FunctionBlock
import cern.plcverif.plc.step7.step7Language.Label
import cern.plcverif.plc.step7.step7Language.LeftValue
import cern.plcverif.plc.step7.step7Language.NamedCallParameterItem
import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.step7Language.ProgramUnit
import cern.plcverif.plc.step7.step7Language.RetValCallParameterItem
import cern.plcverif.plc.step7.step7Language.SingleCallParameter
import cern.plcverif.plc.step7.step7Language.StatementList
import cern.plcverif.plc.step7.step7Language.Step7LanguageFactory
import cern.plcverif.plc.step7.step7Language.SubroutineCall
import cern.plcverif.plc.step7.step7Language.Variable
import cern.plcverif.plc.step7.step7Language.VariableDeclarationDirection
import cern.plcverif.plc.step7.step7Language.VerificationAssertion
import cern.plcverif.plc.step7.typecomputer.Step7TypeComputer
import cern.plcverif.plc.step7.util.Step7LanguageHelper
import com.google.common.base.Preconditions
import java.util.List
import java.util.Map
import java.util.Optional
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil
import cern.plcverif.plc.step7.step7Language.DataBlock

abstract class BlockToCfa {
	/**
	 * Class to represent a transition that has to be created after each
	 * location is created in the target automaton.
	 * 
	 * The source location is identified by the field {@link PostponedTransitionCreation#sourceLocation}
	 * (as it is known when the {@code PostponedTransitionCreation} object is created).
	 * The target location is identified by the method {@link PostponedTransitionCreation#getTargetLocation()}
	 * (as it is not known yet when the {@code PostponedTransitionCreation} object is created).
	 * 
	 * Once the location creation phase is over, the transitions described in {@code PostponedTransitionCreation}
	 * objects will be created. Optionally they can be annotated too, if the method
	 * {@link PostponedTransitionCreation#createTransitionAnnotations} is overridden. 
	 */
	protected static abstract class PostponedTransitionCreation {
		AutomatonDeclaration containerAutomaton;
		Location sourceLocation; 
		String name;
		
		/**
		 * @param containerAutomaton The automaton which will contain the transition once it is created.
		 * @param sourceLocation The source of the transition.
		 * @param name The name of the transition to be created.
		 */
		new(AutomatonDeclaration containerAutomaton, Location sourceLocation, String name) {
			this.containerAutomaton = containerAutomaton;
			this.sourceLocation = sourceLocation;
			this.name = name;
		}
		
		def abstract Location getTargetLocation();
		
		def void createTransitionAnnotations(Transition transition) {
			// by default do nothing
		}
		
		def Transition createTransition() {
			val transition = TransitionBuilder.builder()
				.name(name)
				.source(sourceLocation)
				.target(getTargetLocation()).build();
			createTransitionAnnotations(transition);
			return transition;
		}	
	}
	
	protected final IPlcverifLogger log;
	
	/**
	 * The statements of the block to be represented in CFA.
	 */
	StatementList statementsOfBlock;
	
	/**
	 * Counter to ensure the transition name uniqueness.
	 */
	int nextTransId = 1;
	
	/**
	 * Counter to ensure the location name uniqueness.
	 */
	int nextLocId = 1;
	
	protected StructuralAstCfaTrace structuralTrace;
	
	protected final CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;
	
	protected Step7ExprToCfaExpr exprConverter;
	
	/**
	 * The automaton in which the statements will be represented.
	 */	
	protected AutomatonDeclaration representingAutomaton;
	
	/**
	 * Mapping of STEP 7 labels to their representing locations.
	 */
	Map<Label, Location> labelToLocation = newHashMap();
	
	/**
	 * Collection of transitions that were identified during CFA generation, 
	 * but which shall be created strictly after each location is created.
	 * (If there is a forward jump in the code, the target location may not exist yet.)
	 */
	List<PostponedTransitionCreation> postponedTransitionCreations = newArrayList();
	
	
	protected new(StatementList statementsOfBlock, AutomatonDeclaration representingAutomaton, AstCfaVarTrace astCfaVarTrace, Step7TypeComputer astTypes, StructuralAstCfaTrace structuralTrace, IPlcverifLogger logger) {
		this.log = logger;
		if (statementsOfBlock === null) {
			log.logDebug("It is suspicious to generate an automaton representing a null statement list. It is fine if the source program unit is intentionally empty.");
		}
		this.statementsOfBlock = statementsOfBlock;
		this.representingAutomaton = representingAutomaton;
		this.structuralTrace = structuralTrace;
		
		this.exprConverter = new Step7ExprToCfaExpr(astCfaVarTrace, astTypes, structuralTrace); 
	}

	/**
	 * Generic conversion strategy description:
	 * - Creation of the initial location
	 * - Call {@code beforeConversion()}
	 * - Representation of the main statement list using the {@code convertList} method
	 * - Creation of the postponed jumps 
	 */
	protected def convert() {
		// Creation of the initial location
		val initLoc = factory.createInitialLocation("init", representingAutomaton);
		
		// Reinitialize TEMP variables (they should not retain their values)
		val beginLoc = reinitializeTempVariables(initLoc, representingAutomaton);
		
		// Pre-conversion initialization if needed
		beforeConversion();
		
		// Representation of the structure without jumps
		val endLoc = convertList(statementsOfBlock, beginLoc);
		if (endLoc.annotations.filter(AssertionAnnotation).empty == false) {
			// don't let the end location have assertion annotation
			val newEndLoc = factory.createEndLocation("end", representingAutomaton);
			factory.createAssignmentTransition(nextTransName(), representingAutomaton, endLoc, newEndLoc, factory.trueLiteral());
		} else {
			representingAutomaton.setEndLocation(endLoc);
		}
		
		// Note: input variables are not initialized here (as it has to be done only for the main automaton)
		// TODO set TEMP variables to default at the end?
		
		// Adding postponed transitions (e.g. representations of GOTO, EXIT, CONTINUE)
		representPostponedTransitions();
		
		return this;
	}
		
	protected def getAstCfaVarTrace() {
		return this.exprConverter.getAstCfaVarTrace();
	}
	
	private def Location reinitializeTempVariables(Location start, AutomatonDeclaration parentAutomaton) {
		val tempVars = getAstCfaVarTrace().variables.filter[
			it | Step7LanguageHelper.getVariableDirection(it) == VariableDeclarationDirection.TEMP 
				&& it.isReference == false // no need to zero out the variable views
		];
		val tempFields = tempVars.map[it | getAstCfaVarTrace().get(it)];
		val localTempFields = tempFields.filter[it | EmfHelper.getContainerOfType(it, DataStructure) == parentAutomaton.localDataStructure.definition]; 
		if (localTempFields.isEmpty) {
			// no TEMP field -- nothing to reinitialize -- no need for new location
			return start;
		}
		
		// new location and new transition
		val retLoc = createUniqueLocation(parentAutomaton);
		val transition = factory.createAssignmentTransition(nextTransName("temp_reinit"), parentAutomaton, start, retLoc, factory.trueLiteral());
		
		// reinitialize TEMP fields
		for (field : localTempFields) {
			createZeroAssignments(transition, factory.createFieldRef(field));
		}
		
		return retLoc;
	}
	
	private def void createZeroAssignments(AssignmentTransition transition, DataRef prefix) {
		val type = prefix.type;
		switch (type) {
			ElementaryType: factory.createAssignment(transition, prefix, Step7ExprToCfaExpr.defaultLiteral(type))
			ArrayType: 
				for (idx : type.dimension.lowerIndex..type.dimension.upperIndex) {
					val ref = factory.appendIndexing(EcoreUtil.copy(prefix), idx);
					createZeroAssignments(transition, ref);
				}
			DataStructureRef: 
				for (field : type.definition.fields) {
					val ref = factory.appendFieldRef(EcoreUtil.copy(prefix), field);
					createZeroAssignments(transition, ref);
				}
		}
	}

	/**
	 * Placeholder for operations that need to be done before the statement conversion starts.
	 */
	protected def void beforeConversion() {}

	/**
	 * Entry point for the translation of the statement list contents.
	 * To be implemented by the specific subclasses for each language.
	 */
	protected def abstract Location convertList(StatementList e, Location start);
	
	// Label- and jump-related helpers
	/**
	 * Registers the given label for the given location.
	 * 
	 * @throws PlcCodeToCfaRuntimeException If the label was already registered
	 */
	protected def void registerLabelForLocation(Label label, Location location) {
		if (labelToLocation.containsKey(label)) {
			// duplicated label in the current block
			throw new PlcCodeToCfaRuntimeException("Duplicate definition of label '%s' in automaton '%s'.", label.name,
				representingAutomaton.name);
		}
		labelToLocation.put(label, location);
	}
	
	 /**
	 * Returns the location registered for the given label. It is never null.
	 * 
	 * @throws PlcCodeToCfaRuntimeException If the label is not found
	 */
	protected def Location getRegisteredLocationOfLabel(Label label) {
		val ret = labelToLocation.get(label);
		if (ret === null) {
			throw new PlcCodeToCfaRuntimeException("No location was found for label '%s' in automaton '%s.", label.name,
				representingAutomaton.name);
		}
		
		return ret;
	}

	 /**
	 * Registers a transition that has to be created after all locations of the given automaton were created.
	 */
	protected def void postpone(PostponedTransitionCreation jumpCreation) {
		postponedTransitionCreations.add(jumpCreation);
	}
	
	private def void representPostponedTransitions() {
		for (transitionCreation : postponedTransitionCreations) {
			transitionCreation.createTransition();
		}
	}
	
	// CFA building helper methods
	protected def createUniqueLocation(AutomatonDeclaration containerAutomaton) {
		return factory.createLocation(nextLocName(), containerAutomaton); 
	}

	protected def String nextLocName() {
		return String.format("l%s", (nextLocId++));
	}
	
	protected def String nextTransName() {
		return String.format("t%s", (nextTransId++));
	}
	
	protected def String nextTransName(String prefix) {
		return prefix + (nextTransId++);
	}
	
	
	
	protected def Location convertVerificationAssertion(VerificationAssertion e, Location start) {
		Preconditions.checkNotNull(start);
		
		val expression = exprConverter.convertExpression(e.expression);
		factory.createAssertionAnnotation(start, expression, e.tag); 
		return start;
	}
	
	protected def Location convertCallStatement(SubroutineCall e, Location start) {
		// SPECIAL CASE for built-in functions (CFA libary functions)
		if (LibraryFunctionsToCfa.isHandledBuiltinFunctionCall(e) && LibraryFunctionsToCfa.canRepresentAsCall(e)) {
			return LibraryFunctionsToCfa.representAsCall(e, start, exprConverter);
		}
		
		val end = factory.createLocation(nextLocName(), representingAutomaton);
		val transition = factory.createCallTransition(nextTransName(), representingAutomaton, start, end, factory.trueLiteral());
		
		var ProgramUnit calledProgramUnit;
		var DataRef calleeContext;
		if (Step7LanguageHelper.isFcCall(e)) {
			calledProgramUnit = e.calledUnit as Function;
			calleeContext = factory.createFieldRef(structuralTrace.singletonProgramUnitToField.get(calledProgramUnit));
			
		} else if (Step7LanguageHelper.isFbCall(e)) {
			calledProgramUnit = Step7LanguageHelper.getCalledProgramUnit(e);
			
			if (e.calledUnit instanceof Variable) {
				calleeContext = factory.createFieldRef(astCfaVarTrace.get(e.calledUnit as Variable));
			} else if (e.calledUnit instanceof FunctionBlock) {
				calleeContext = factory.createFieldRef(structuralTrace.singletonProgramUnitToField.get(e.dataBlock));
			} else if (e.calledUnit instanceof DataBlock) {
				calleeContext = factory.createFieldRef(structuralTrace.singletonProgramUnitToField.get(e.calledUnit));
			} else {
				throw new UnsupportedOperationException();
			} 
		} else {
			Preconditions.checkState(!e.calledUnit.eIsProxy, String.format("The call %s calls a block that cannot be resolved (is a proxy).", e));
			throw new IllegalStateException(String.format("The call %s is not an FC nor an FB call.", e));
		}
		
		Preconditions.checkState(calleeContext !== null, "Unknown (null) calleeContext for the following call: " + e);
		Preconditions.checkState(calleeContext.type !== null, "Unknown (null) calleeContext.type for the following call: " + e);
		val calledAutomaton = structuralTrace.programUnitToAutomaton.get(calledProgramUnit);
		val call = factory.createCall(transition, calledAutomaton, calleeContext);
		representCallParameter(e.callParameter, call);
		
		return end;
	}
	
	protected def dispatch void representCallParameter(CallParameter param, Call targetCall) {
		throw new PlcCodeToCfaRuntimeException("Unknown generic case in representCallParameter");
	}
	
	protected def dispatch void representCallParameter(Void param, Call targetCall) {
		// nothing to do here (parameterless call)
	}
	
	protected def dispatch void representCallParameter(SingleCallParameter param, Call targetCall) {
		val enclosingCall = EmfHelper.getContainerOfType(param, SubroutineCall);
		if (Step7LanguageHelper.isFunction(enclosingCall.calledUnit)) {
			val inputVar = Step7LanguageHelper.tryGetOnlyInputVariable(enclosingCall.calledUnit as Function);
			if (inputVar.isPresent) {
				val varRef = Step7LanguageFactory.eINSTANCE.createDirectNamedRef() => [ref = inputVar.get];
				createInputOutputParam(VariableDeclarationDirection.INPUT, varRef, param.parameter, targetCall); 
			} else {
				throw new PlcCodeToCfaRuntimeException("The function '%s' is called with unnamed parameter, however it contains multiple input variables.", enclosingCall.calledUnit.name);
			}
		} else {
			throw new PlcCodeToCfaRuntimeException("Function call with unnamed parameter cannot be used for '%s' as it is not a function.", enclosingCall.calledUnit.name);
		}
	}
	
	protected def dispatch void representCallParameter(CallParameterList e, Call targetCall) {
		for (callParam : e.parameters) {
			representCallParameter(callParam, targetCall);
		}		
	}
	
	protected def dispatch void representCallParameter(NamedCallParameterItem e, Call call) {
		val varRef = Step7LanguageFactory.eINSTANCE.createDirectNamedRef();
		varRef.ref = e.parameter;
		
		val dir = Step7LanguageHelper.getVariableDirection(e.parameter);
		createInputOutputParam(dir, varRef, e.value, call); 
	}
	
	protected def dispatch void representCallParameter(RetValCallParameterItem e, Call call) {
		val calledUnit = EmfHelper.getContainerOfType(e, SubroutineCall).calledUnit;
		Preconditions.checkArgument(calledUnit instanceof Function, "Only a function may have a return value.");
		Preconditions.checkArgument(e.value instanceof LeftValue, "The return value must be assigned to a left value.");
		
		val calledFunction = calledUnit as Function;
		val callParamLeftSide = exprConverter.convertLeftValue(e.value as LeftValue);
		val callParamRightSide = exprConverter.convertExpressionWithTypeConv(factory.createFieldRef(astCfaVarTrace.getRetval(calledFunction)), callParamLeftSide.type); 
		factory.createOutputAssignment(call, 
			callParamLeftSide,
			callParamRightSide);
	}
	
	protected def dispatch void representCallParameter(EnCallParameterItem e, Call call) {
		throw new UnsupportedOperationException("Unhandled feature: EN call parameter.");
	}
	
	private def createInputOutputParam(VariableDeclarationDirection direction, DirectNamedRef paramLeft, Expression paramRight, Call targetCall) {
		if (direction == VariableDeclarationDirection.INPUT || direction == VariableDeclarationDirection.INOUT) {
			val assignmentLeft = exprConverter.convertLeftValue(paramLeft);
			factory.createInputAssignment(targetCall, assignmentLeft, exprConverter.convertExpressionWithTypeConv(paramRight, assignmentLeft.type));
		}
		
		if (direction == VariableDeclarationDirection.OUTPUT || direction == VariableDeclarationDirection.INOUT) {
			if (!(paramRight instanceof LeftValue)) {
				throw new PlcCodeToCfaRuntimeException(
						String.format("The right value of output parameters should be a variable and not '%s'. " +
							"(%s %s := %s, located at %s)", paramRight.class.simpleName,
							direction.literal, 
							Step7LanguageHelper.textInSource(paramLeft),
							Step7LanguageHelper.textInSource(paramRight),
							Step7LanguageHelper.posInSource(paramLeft))
					);
			}
			
			Preconditions.checkArgument(paramRight instanceof LeftValue, "The right value of output parameters should be a variable and not " + paramRight.class.name);
			val assignmentLeft = exprConverter.convertLeftValue(paramRight as LeftValue);
			factory.createOutputAssignment(targetCall, assignmentLeft, exprConverter.convertExpressionWithTypeConv(paramLeft, assignmentLeft.type));
		}
	}
	
	private def void annotateWithLineNumber(Location location, String filename, int lineNumber) {
		factory.createLineNumberAnnotation(location, filename, lineNumber);
	}
	
	protected def void annotateWithLineNumber(Location location, EObject origin) {
		val int lineNumber = Step7LanguageHelper.lineInSource(origin);
		val ProgramFile p = EmfHelper.getContainerOfType(origin, ProgramFile);
		val filename = Optional.ofNullable(p.eResource.URI.lastSegment).orElse(""); 
		annotateWithLineNumber(location, filename, lineNumber);
	}
}
	