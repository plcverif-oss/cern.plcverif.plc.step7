package cern.plcverif.plc.step7.cfa.impl

import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.cfa.cfabase.DataDirection
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.plc.step7.util.StlHelper
import cern.plcverif.plc.step7.util.StlHelper.StatusBit
import cern.plcverif.plc.step7.util.StlSemanticsHelper
import com.google.common.base.Preconditions
import java.util.Map

import static cern.plcverif.plc.step7.util.DataTypeUtil.*

class StlGlobalData {
	public static final boolean ACCU_SIGNED = Step7TypeToExprType.isSigned(StlSemanticsHelper.ACCU_TYPE);
	public static final int ACCU_BITS = (Step7TypeToExprType.s7typeToExprType(StlSemanticsHelper.ACCU_TYPE) as IntType).bits;
	public static final long ACCU_LOW_MASK = 0xFFFF;
	public static final long ACCU_HIGH_MASK = invertAccuMask(ACCU_LOW_MASK);
	public static final long ACCU_LOWLOW_MASK = 0xFF;
	public static final long ACCU_LOWLOW_MASK_INV = invertAccuMask(ACCU_LOWLOW_MASK);
	public static final long ACCU_BIT1_MASK = 0x1;
	
	protected final CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;
	
	final CfaNetworkDeclaration automatonNetwork;
	
	Field accu1 = null;
	Field accu2 = null;
	
	/**
	 * CFA variable representation of status bits.
	 * Solution to be improved (e.g. support for accumulators needed).
	 */
	Map<StlHelper.StatusBit, Field> statusBitInCfa;
	
	
	new(CfaNetworkDeclaration automatonNetwork) {
		this.automatonNetwork = automatonNetwork;
		
		createStatusBits();
		
		// ACCUs are created lazily
	}
	
	def Field getStatusBitField(StatusBit bit) {
		return Preconditions.checkNotNull(statusBitInCfa.get(bit));
	}
	
	def Field getAccu1Field() {
		if (accu1 === null) {
			createAccus();
		}
		
		return accu1;
	}
	
	def Field getAccu2Field() {
		if (accu2 === null) {
			createAccus();
		}
		
		return accu2;
	}
	
	
	private def createStatusBits() {
		// Create variables for the registers
		this.statusBitInCfa = newHashMap();
		for (statusBit : StlHelper.StatusBit.values) {
			val cfaFieldName = "__" + statusBit.name; // TODO improve naming (ensure uniqueness)
			val cfaType = factory.createBoolType();
			val cfaField = factory.createField(cfaFieldName, automatonNetwork.rootDataStructure, 
				cfaType);
			factory.createDirectionFieldAnnotation(cfaField, DataDirection.LOCAL);
			factory.createInitialAssignment(cfaField, Step7ExprToCfaExpr.defaultLiteral(cfaType));
			factory.createInternalGeneratedFieldAnnotation(cfaField);
			this.statusBitInCfa.put(statusBit, cfaField);
		}
	}
	
	private def createAccus() {
		// Create variables for the accumulators
			val accu1CfaVarName = "__ACCU1";
			val accu1Type = factory.createIntType(isSigned(StlSemanticsHelper.ACCU_TYPE), getBitWidth(StlSemanticsHelper.ACCU_TYPE));
			accu1 = factory.createField(accu1CfaVarName, automatonNetwork.rootDataStructure,
						accu1Type);
			factory.createDirectionFieldAnnotation(accu1, DataDirection.LOCAL);
			factory.createInitialAssignment(accu1, Step7ExprToCfaExpr.defaultLiteral(accu1Type));
			factory.createInternalGeneratedFieldAnnotation(accu1);
					
			val accu2CfaVarName = "__ACCU2";
			val accu2Type = factory.createIntType(isSigned(StlSemanticsHelper.ACCU_TYPE), getBitWidth(StlSemanticsHelper.ACCU_TYPE));
			accu2 = factory.createField(accu2CfaVarName, automatonNetwork.rootDataStructure,
						accu2Type);
			factory.createDirectionFieldAnnotation(accu2, DataDirection.LOCAL);
			factory.createInitialAssignment(accu2, Step7ExprToCfaExpr.defaultLiteral(accu2Type));
			factory.createInternalGeneratedFieldAnnotation(accu2);
	}
	
	
	private static def long invertAccuMask(long maskToInvert) {
		if (ACCU_SIGNED) {
			return -maskToInvert - 1;
		} else {
			return (1L << ACCU_BITS) - 1 - maskToInvert;
		}
	} 
	
}