/*******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.cfa.util;

import java.util.HashSet;
import java.util.Set;

import com.google.common.base.Preconditions;

import cern.plcverif.plc.step7.datatypes.S7AddressMemoryIdentifier;
import cern.plcverif.plc.step7.datatypes.S7AddressMemorySize;
import cern.plcverif.plc.step7.datatypes.S7MemoryAddress;
import cern.plcverif.plc.step7.step7Language.ArrayRef;
import cern.plcverif.plc.step7.step7Language.DirectNamedRef;
import cern.plcverif.plc.step7.util.SimpleExpressionUtil;
import cern.plcverif.plc.step7.util.SimpleExpressionUtil.NotSimpleExpressionException;

/**
 * Utility class to represent array-like memory accesses (e.g., {@code I[0,1]})
 * to memory address-based accesses (e.g., {@code I0.1}).
 * 
 * Non-constant array-like memory accesses (e.g., {@code IB[var + 3]}) are not
 * supported here.
 */
public final class MemoryAddressArrayUtil {
	private MemoryAddressArrayUtil() {
		// Utility class.
	}

	// Should contain MB, MW, MD, IB, IW, ID, ...
	private static final Set<String> MEMORY_ADDRESS_BYTE_PREFIXES = new HashSet<>();

	// Should contain M, MX, I, IX, ...
	private static final Set<String> MEMORY_ADDRESS_BIT_PREFIXES = new HashSet<>();

	static {
		// Initialize prefix sets
		for (S7AddressMemoryIdentifier area : S7AddressMemoryIdentifier.values()) {
			for (S7AddressMemorySize size : S7AddressMemorySize.values()) {
				if (size.getSizeInBits() == 1) {
					MEMORY_ADDRESS_BIT_PREFIXES
							.add(area.name().toUpperCase() + size.getIdentifierString().toUpperCase());
				} else {
					MEMORY_ADDRESS_BYTE_PREFIXES
							.add(area.name().toUpperCase() + size.getIdentifierString().toUpperCase());
				}
			}
			MEMORY_ADDRESS_BIT_PREFIXES.add(area.name().toUpperCase());
		}
	}

	/**
	 * Returns true if the given array reference can be represented as a memory
	 * address. It returns false if the array reference refers to a memory area,
	 * but the index expressions are not constants.
	 */
	public static boolean isMemoryAddressArray(ArrayRef arrayRef) {
		return isBitMemoryAddressArray(arrayRef) || isByteMemoryAddressArray(arrayRef);
	}

	/**
	 * Returns true if the given array reference can be represented as a bit
	 * memory address. For this the following requirements are checked:
	 * <ul>
	 * <li>The referred array's name is a valid prefix (as defined in
	 * {@link #MEMORY_ADDRESS_BIT_PREFIXES}, e.g., {@code M}, {@code MX},
	 * {@code I}),
	 * <li>The array reference contains exactly two indices,
	 * <li>The array reference indices can be evaluated, i.e., they are integer
	 * literals or constant expressions (e.g., 2+3).
	 * </ul>
	 */
	public static boolean isBitMemoryAddressArray(ArrayRef arrayRef) {
		if (arrayRef.getRef() instanceof DirectNamedRef) {
			String varName = ((DirectNamedRef) arrayRef.getRef()).getRef().getName().toUpperCase();
			return (MEMORY_ADDRESS_BIT_PREFIXES.contains(varName) && arrayRef.getIndex().size() == 2
					&& SimpleExpressionUtil.isSimpleExpression(arrayRef.getIndex().get(0))
					&& SimpleExpressionUtil.isSimpleExpression(arrayRef.getIndex().get(1)));
		}

		return false;
	}

	/**
	 * Returns true if the given array reference can be represented as a byte
	 * memory address. For this the following requirements are checked:
	 * <ul>
	 * <li>The referred array's name is a valid prefix (as defined in
	 * {@link #MEMORY_ADDRESS_BYTE_PREFIXES}, e.g., {@code MB}, {@code MW},
	 * {@code IB}),
	 * <li>The array reference contains exactly one index,
	 * <li>The array reference index can be evaluated, i.e., it is an integer
	 * literal or constant expression (e.g., 2+3).
	 * </ul>
	 */
	public static boolean isByteMemoryAddressArray(ArrayRef arrayRef) {
		if (arrayRef.getRef() instanceof DirectNamedRef) {
			String varName = ((DirectNamedRef) arrayRef.getRef()).getRef().getName().toUpperCase();
			return (MEMORY_ADDRESS_BYTE_PREFIXES.contains(varName) && arrayRef.getIndex().size() == 1
					&& SimpleExpressionUtil.isSimpleExpression(arrayRef.getIndex().get(0)));
		}

		return false;
	}

	/**
	 * Represents the given array reference as memory address. If it is not
	 * possible, it throws {@link IllegalArgumentException}. Use
	 * {@link #isMemoryAddressArray(ArrayRef)} to determine whether an array
	 * reference can be represented as memory address.
	 * 
	 * @param arrayRef
	 *            Array reference to represent as memory address
	 * @return Representing memory address
	 * @throws IllegalArgumentException
	 *             if it is not possible to represent the given array reference
	 *             as memory address
	 * 
	 * @see #isBitMemoryAddressArray(ArrayRef)
	 * @see #isByteMemoryAddressArray(ArrayRef)
	 * @see #isMemoryAddressArray(ArrayRef)
	 */
	public static S7MemoryAddress toMemoryAddress(ArrayRef arrayRef) {
		if (arrayRef.getRef() instanceof DirectNamedRef) {
			String varName = ((DirectNamedRef) arrayRef.getRef()).getRef().getName().toUpperCase();

			if (arrayRef.getIndex().stream().anyMatch(it -> !(SimpleExpressionUtil.isSimpleExpression(it)))) {
				throw new IllegalArgumentException(
						"Unable to translate it to a memory address. The indexing is not constant.");
			}

			try {
				if (isByteMemoryAddressArray(arrayRef)) {
					Preconditions.checkArgument(arrayRef.getIndex().size() == 1,
							"Byte memory address must have one index.");
					long index = SimpleExpressionUtil.evaluateSimpleExpression(arrayRef.getIndex().get(0));
					return S7MemoryAddress.create(String.format("%s%s", varName, index));
				} else if (isBitMemoryAddressArray(arrayRef)) {
					Preconditions.checkArgument(arrayRef.getIndex().size() == 2,
							"Bit memory address must have two indices.");
					long index1 = SimpleExpressionUtil.evaluateSimpleExpression(arrayRef.getIndex().get(0));
					long index2 = SimpleExpressionUtil.evaluateSimpleExpression(arrayRef.getIndex().get(1));
					return S7MemoryAddress.create(String.format("%s%s.%s", varName, index1, index2));
				}
			} catch (NotSimpleExpressionException ex) {
				throw new IllegalArgumentException(
						"Unable to translate it to a memory address. The indexing is not constant.", ex);
			}
		}

		throw new IllegalArgumentException("Not an array that represents a memory address.");
	}
}
