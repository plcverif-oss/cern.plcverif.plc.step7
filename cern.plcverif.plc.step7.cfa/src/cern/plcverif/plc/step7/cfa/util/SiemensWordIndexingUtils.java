/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.expr.IntLiteral;
import cern.plcverif.base.models.expr.IntType;

/**
 * This class provides utility methods to calculate bit indexes in Siemens PLC
 * words.
 *
 * While arrays are indexed in a linear order, Siemens PLCs index words in a
 * different order, using Little-endian encoding for each two bytes.
 */
public final class SiemensWordIndexingUtils {
	private static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;

	private SiemensWordIndexingUtils() {
		// Utility class.
	}
	
	/**
	 * Returns an integer literal that can be used to select the bit with the
	 * given index (together with bitwise AND and OR operations).
	 *
	 * If the selection is {@code inverted}, the result will contain a 0 at the
	 * needed index, all other bits will be 1. If {@code inverted=false}, the
	 * result will contain a 1 at the needed index, all other bits will be 0.
	 *
	 * This is not Siemens-specific.
	 *
	 * @param inverted
	 *            If not inverted, the single 1 in the binary representation of
	 *            the returned integer is where the selected bit is.
	 * @param arrayIndex
	 *            The (zero-based) position index where the indexing bit should
	 *            be placed.
	 * @param requiredType
	 *            The type of the returned literal. This {@link EObject} will
	 *            not be used for the created expressions, thus it can be
	 *            contained.
	 * @return Bit indexer literal.
	 */
	public static IntLiteral wordBitSelection(boolean inverted, long arrayIndex, IntType requiredType) {
		long wordIndex = arrayIndexToWordIndex(arrayIndex, requiredType);

		if (inverted) {
			return wordWithSingleZero(wordIndex, requiredType);
		} else {
			return wordWithSingleOne(wordIndex, requiredType);
		}
	}

	/**
	 * We handle the Siemens-specific WORD indexing here. The given
	 * {@code arrayIndex} is a zero-based linear bit indexing from the right, as
	 * used for arrays.
	 *
	 * For 8-bit words, the Siemens indexing is as follows:
	 *
	 * <pre>
	 * +---------------+
	 * |7 6 5 4 3 2 1 0|
	 * +---------------+
	 * </pre>
	 *
	 * For 16-bit words, the Siemens indexing is as follows:
	 *
	 * <pre>
	 * +---------------+---------------+
	 * |               |1 1 1 1 1 1    |
	 * |7 6 5 4 3 2 1 0|5 4 3 2 1 0 9 8|
	 * +---------------+---------------+
	 * </pre>
	 *
	 * Therefore wordIndex = {@code (arrayIndex + 8) % 16} in this case.
	 *
	 * For 32-bit words, the Siemens indexing is as follows:
	 *
	 * <pre>
	 * +---------------+---------------+---------------+---------------+
	 * |               |1 1 1 1 1 1    |2 2 2 2 1 1 1 1|3 3 2 2 2 2 2 2|
	 * |7 6 5 4 3 2 1 0|5 4 3 2 1 0 9 8|3 2 1 0 9 8 7 6|1 0 9 8 7 6 5 4|
	 * +---------------+---------------+---------------+---------------+
	 * </pre>
	 *
	 * Therefore wordIndex =
	 * {@code 16 + (arrayIndex + 8) % 16 - 16*(arrayIndex DIV 16)} in this case.
	 *
	 *
	 */
	public static long arrayIndexToWordIndex(long arrayIndex, IntType wordType) {
		long wordIndex;
		int wordLength = wordType.getBits();

		if (wordLength == 8) {
			wordIndex = arrayIndex;
		} else if (wordLength == 16) {
			wordIndex = (arrayIndex + 8) % 16;
		} else if (wordLength == 32) {
			wordIndex = 16 + (arrayIndex + 8) % 16 - 16 * (arrayIndex / 16);
		} else {
			throw new UnsupportedOperationException("Unsupported Siemens WORD size: " + wordLength);
		}
		return wordIndex;
	}

	/**
	 * Returns an {@code IntLiteral} representing the binary 00..0100..0 value,
	 * where 1 is at the position {@code idx} from the right (zero-based).
	 *
	 * Example: wordWithSingleOne(2, {signed=false, bits=8}) --> binary
	 * 0000_0100
	 */
	public static IntLiteral wordWithSingleOne(long idx, IntType type) {
		return FACTORY.createIntLiteral(1L << idx, EcoreUtil.copy(type));
	}

	/**
	 * Returns an {@code IntLiteral} representing the binary 1..1111 value, with
	 * length {@code type.bits}.
	 *
	 * Example: wordWithAllOnes(2, {signed=false, bits=8}) --> binary 1111_1111
	 * with type {signed=false, bits=8}
	 */
	public static IntLiteral wordWithAllOnes(IntType type) {
		long value = 0;
		for (long i = 0; i < type.getBits(); i++) {
			value += 1L << i;
		}

		return FACTORY.createIntLiteral(value, EcoreUtil.copy(type));
	}

	/**
	 * Returns an {@code IntLiteral} representing the binary 11..1011..1 value,
	 * where 0 is at the position {@code idx} from the right (zero-based).
	 *
	 * Example: wordWithSingleZero(2, {signed=false, bits=8}) --> binary
	 * 1111_1011
	 */
	public static IntLiteral wordWithSingleZero(long idx, IntType type) {
		long result = 0;
		// make result a type-long string of bit 1s
		for (int i = 0; i < type.getBits(); i++) {
			// shifting in '1' for 'type.bits' times
			result = (result << 1) | 1;
		}

		// erase the bit at 'idx'
		result = result ^ (1L << idx);

		return FACTORY.createIntLiteral(result, EcoreUtil.copy(type));
	}
}
