/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class ListMap<K, V> implements Map<K, V> {
	private class ListMapEntry implements java.util.Map.Entry<K, V> {
		private K key;
		private V value;

		public ListMapEntry(K key, V value) {
			this.key = key;
			this.value = value;
		}

		@Override
		public K getKey() {
			return key;
		}

		@Override
		public V getValue() {
			return value;
		}

		@Override
		public V setValue(V value) {
			V oldValue = this.value;
			this.value = value;
			return oldValue;
		}
	}

	private List<ListMapEntry> entries = new ArrayList<>();

	@Override
	public void clear() {
		entries.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		return entries.stream().anyMatch(it -> it.key.equals(key));
	}

	@Override
	public boolean containsValue(Object value) {
		return entries.stream().anyMatch(it -> it.value.equals(value));
	}

	@Override
	public Set<Map.Entry<K, V>> entrySet() {
		return new HashSet<Map.Entry<K, V>>(this.entries);
	}

	@Override
	public V get(Object key) {
		Optional<ListMapEntry> match = entries.stream().filter(it -> it.key.equals(key)).findAny();
		if (match.isPresent()) {
			return match.get().getValue();
		} else {
			return null;
		}
	}

	@Override
	public boolean isEmpty() {
		return entries.isEmpty();
	}

	@Override
	public Set<K> keySet() {
		return entries.stream().map(it -> it.getKey()).collect(Collectors.toSet());
	}

	@Override
	public V put(K key, V value) {
		V ret = remove(key);

		entries.add(new ListMapEntry(key, value));

		return ret;
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		throw new UnsupportedOperationException("Not implemented yet");
	}

	@Override
	public V remove(Object key) {
		V result = null;
		int i = 0;
		while (entries.size() > i) {
			if (entries.get(i).getKey().equals(key)) {
				result = entries.remove(i).value;
			} else {
				i++;
			}
		}

		return result;
	}

	@Override
	public int size() {
		return entries.size();
	}

	@Override
	public Collection<V> values() {
		return entries.stream().map(it -> it.getValue()).collect(Collectors.toList());
	}
}
