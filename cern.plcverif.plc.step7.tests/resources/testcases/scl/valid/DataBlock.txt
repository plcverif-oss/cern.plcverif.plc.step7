{ProgramFile}
 ├──verificationOptions: <empty>
 ├──globalVariables: <empty>
 └──programUnits: 
    {DataBlock name=shared_db1}
     ├──attributes: <null>
     ├──structure: 
     │  {StructDT}
     │   └──members: 
     │      {VariableDeclarationLine initialized=false bounded=false}
     │       ├──variables: 
     │       │  {Variable name=a reference=false ref:<null>}
     │       ├──type: 
     │       │  {ElementaryDT type=BOOL}
     │       ├──initialization: <null>
     │       ├──lowerBound: <null>
     │       └──upperBound: <null>
     └──initialAssignments: 
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {DirectNamedRef ref:{NamedElement name=a}}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {BoolConstant value=TRUE}
    {DataBlock name=shared_db2}
     ├──attributes: <null>
     ├──structure: 
     │  {StructDT}
     │   └──members: 
     │      {VariableDeclarationLine initialized=false bounded=false}
     │       ├──variables: 
     │       │  {Variable name=a reference=false ref:<null>}
     │       ├──type: 
     │       │  {StructDT}
     │       │   └──members: 
     │       │      {VariableDeclarationLine initialized=false bounded=false}
     │       │       ├──variables: 
     │       │       │  {Variable name=b reference=false ref:<null>}
     │       │       ├──type: 
     │       │       │  {ElementaryDT type=INT}
     │       │       ├──initialization: <null>
     │       │       ├──lowerBound: <null>
     │       │       └──upperBound: <null>
     │       ├──initialization: <null>
     │       ├──lowerBound: <null>
     │       └──upperBound: <null>
     └──initialAssignments: 
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {QualifiedRef}
         │   ├──prefix: 
         │   │  {DirectNamedRef ref:{NamedElement name=a}}
         │   └──ref: 
         │      {DirectNamedRef ref:{NamedElement name=b}}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {IntConstant value=1}
    {DataBlock name=shared_db3}
     ├──attributes: <null>
     ├──structure: 
     │  {StructDT}
     │   └──members: 
     │      {VariableDeclarationLine initialized=false bounded=false}
     │       ├──variables: 
     │       │  {Variable name=a reference=false ref:<null>}
     │       ├──type: 
     │       │  {StructDT}
     │       │   └──members: 
     │       │      {VariableDeclarationLine initialized=false bounded=false}
     │       │       ├──variables: 
     │       │       │  {Variable name=b reference=false ref:<null>}
     │       │       ├──type: 
     │       │       │  {StructDT}
     │       │       │   └──members: 
     │       │       │      {VariableDeclarationLine initialized=false bounded=false}
     │       │       │       ├──variables: 
     │       │       │       │  {Variable name=c reference=false ref:<null>}
     │       │       │       ├──type: 
     │       │       │       │  {ElementaryDT type=INT}
     │       │       │       ├──initialization: <null>
     │       │       │       ├──lowerBound: <null>
     │       │       │       └──upperBound: <null>
     │       │       ├──initialization: <null>
     │       │       ├──lowerBound: <null>
     │       │       └──upperBound: <null>
     │       ├──initialization: <null>
     │       ├──lowerBound: <null>
     │       └──upperBound: <null>
     └──initialAssignments: 
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {QualifiedRef}
         │   ├──prefix: 
         │   │  {QualifiedRef}
         │   │   ├──prefix: 
         │   │   │  {DirectNamedRef ref:{NamedElement name=a}}
         │   │   └──ref: 
         │   │      {DirectNamedRef ref:{NamedElement name=b}}
         │   └──ref: 
         │      {DirectNamedRef ref:{NamedElement name=c}}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {IntConstant value=1}
    {DataBlock name=shared_db4}
     ├──attributes: <null>
     ├──structure: 
     │  {StructDT}
     │   └──members: 
     │      {VariableDeclarationLine initialized=false bounded=false}
     │       ├──variables: 
     │       │  {Variable name=a reference=false ref:<null>}
     │       ├──type: 
     │       │  {StructDT}
     │       │   └──members: 
     │       │      {VariableDeclarationLine initialized=false bounded=false}
     │       │       ├──variables: 
     │       │       │  {Variable name=barr reference=false ref:<null>}
     │       │       ├──type: 
     │       │       │  {ArrayDT}
     │       │       │   ├──dimensions: 
     │       │       │   │  {ArrayDimensionString rangeString=1..2}
     │       │       │   └──baseType: 
     │       │       │      {StructDT}
     │       │       │       └──members: 
     │       │       │          {VariableDeclarationLine initialized=false bounded=false}
     │       │       │           ├──variables: 
     │       │       │           │  {Variable name=c reference=false ref:<null>}
     │       │       │           ├──type: 
     │       │       │           │  {ElementaryDT type=INT}
     │       │       │           ├──initialization: <null>
     │       │       │           ├──lowerBound: <null>
     │       │       │           └──upperBound: <null>
     │       │       ├──initialization: <null>
     │       │       ├──lowerBound: <null>
     │       │       └──upperBound: <null>
     │       ├──initialization: <null>
     │       ├──lowerBound: <null>
     │       └──upperBound: <null>
     └──initialAssignments: 
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {QualifiedRef}
         │   ├──prefix: 
         │   │  {QualifiedRef}
         │   │   ├──prefix: 
         │   │   │  {DirectNamedRef ref:{NamedElement name=a}}
         │   │   └──ref: 
         │   │      {ArrayRef}
         │   │       ├──ref: 
         │   │       │  {DirectNamedRef ref:{NamedElement name=barr}}
         │   │       └──index: 
         │   │          {IntConstant value=1}
         │   └──ref: 
         │      {DirectNamedRef ref:{NamedElement name=c}}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {IntConstant value=1}
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {QualifiedRef}
         │   ├──prefix: 
         │   │  {QualifiedRef}
         │   │   ├──prefix: 
         │   │   │  {DirectNamedRef ref:{NamedElement name=a}}
         │   │   └──ref: 
         │   │      {ArrayRef}
         │   │       ├──ref: 
         │   │       │  {DirectNamedRef ref:{NamedElement name=barr}}
         │   │       └──index: 
         │   │          {IntConstant value=2}
         │   └──ref: 
         │      {DirectNamedRef ref:{NamedElement name=c}}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {IntConstant value=1}
    {DataBlock name=shared_db5}
     ├──attributes: <null>
     ├──structure: 
     │  {StructDT}
     │   └──members: 
     │      {VariableDeclarationLine initialized=false bounded=false}
     │       ├──variables: 
     │       │  {Variable name=aarr reference=false ref:<null>}
     │       ├──type: 
     │       │  {ArrayDT}
     │       │   ├──dimensions: 
     │       │   │  {ArrayDimensionString rangeString=5..6}
     │       │   └──baseType: 
     │       │      {StructDT}
     │       │       └──members: 
     │       │          {VariableDeclarationLine initialized=false bounded=false}
     │       │           ├──variables: 
     │       │           │  {Variable name=barr reference=false ref:<null>}
     │       │           ├──type: 
     │       │           │  {ArrayDT}
     │       │           │   ├──dimensions: 
     │       │           │   │  {ArrayDimensionString rangeString=1..2}
     │       │           │   └──baseType: 
     │       │           │      {StructDT}
     │       │           │       └──members: 
     │       │           │          {VariableDeclarationLine initialized=false bounded=false}
     │       │           │           ├──variables: 
     │       │           │           │  {Variable name=c reference=false ref:<null>}
     │       │           │           ├──type: 
     │       │           │           │  {ElementaryDT type=INT}
     │       │           │           ├──initialization: <null>
     │       │           │           ├──lowerBound: <null>
     │       │           │           └──upperBound: <null>
     │       │           ├──initialization: <null>
     │       │           ├──lowerBound: <null>
     │       │           └──upperBound: <null>
     │       ├──initialization: <null>
     │       ├──lowerBound: <null>
     │       └──upperBound: <null>
     └──initialAssignments: 
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {QualifiedRef}
         │   ├──prefix: 
         │   │  {QualifiedRef}
         │   │   ├──prefix: 
         │   │   │  {ArrayRef}
         │   │   │   ├──ref: 
         │   │   │   │  {DirectNamedRef ref:{NamedElement name=aarr}}
         │   │   │   └──index: 
         │   │   │      {IntConstant value=5}
         │   │   └──ref: 
         │   │      {ArrayRef}
         │   │       ├──ref: 
         │   │       │  {DirectNamedRef ref:{NamedElement name=barr}}
         │   │       └──index: 
         │   │          {IntConstant value=1}
         │   └──ref: 
         │      {DirectNamedRef ref:{NamedElement name=c}}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {IntConstant value=1}
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {QualifiedRef}
         │   ├──prefix: 
         │   │  {QualifiedRef}
         │   │   ├──prefix: 
         │   │   │  {ArrayRef}
         │   │   │   ├──ref: 
         │   │   │   │  {DirectNamedRef ref:{NamedElement name=aarr}}
         │   │   │   └──index: 
         │   │   │      {IntConstant value=6}
         │   │   └──ref: 
         │   │      {ArrayRef}
         │   │       ├──ref: 
         │   │       │  {DirectNamedRef ref:{NamedElement name=barr}}
         │   │       └──index: 
         │   │          {IntConstant value=2}
         │   └──ref: 
         │      {DirectNamedRef ref:{NamedElement name=c}}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {IntConstant value=1}
    {DataBlock name=shared_db6}
     ├──attributes: <null>
     ├──structure: 
     │  {StructDT}
     │   └──members: 
     │      {VariableDeclarationLine initialized=false bounded=false}
     │       ├──variables: 
     │       │  {Variable name=aarr reference=false ref:<null>}
     │       ├──type: 
     │       │  {ArrayDT}
     │       │   ├──dimensions: 
     │       │   │  {ArrayDimensionString rangeString=5..6}
     │       │   └──baseType: 
     │       │      {StructDT}
     │       │       └──members: 
     │       │          {VariableDeclarationLine initialized=false bounded=false}
     │       │           ├──variables: 
     │       │           │  {Variable name=barr reference=false ref:<null>}
     │       │           ├──type: 
     │       │           │  {ArrayDT}
     │       │           │   ├──dimensions: 
     │       │           │   │  {ArrayDimensionString rangeString=1..2}
     │       │           │   └──baseType: 
     │       │           │      {StructDT}
     │       │           │       └──members: 
     │       │           │          {VariableDeclarationLine initialized=false bounded=false}
     │       │           │           ├──variables: 
     │       │           │           │  {Variable name=carr reference=false ref:<null>}
     │       │           │           ├──type: 
     │       │           │           │  {ArrayDT}
     │       │           │           │   ├──dimensions: 
     │       │           │           │   │  {ArrayDimensionString rangeString=9..10}
     │       │           │           │   └──baseType: 
     │       │           │           │      {ElementaryDT type=INT}
     │       │           │           ├──initialization: <null>
     │       │           │           ├──lowerBound: <null>
     │       │           │           └──upperBound: <null>
     │       │           ├──initialization: <null>
     │       │           ├──lowerBound: <null>
     │       │           └──upperBound: <null>
     │       ├──initialization: <null>
     │       ├──lowerBound: <null>
     │       └──upperBound: <null>
     └──initialAssignments: 
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {QualifiedRef}
         │   ├──prefix: 
         │   │  {QualifiedRef}
         │   │   ├──prefix: 
         │   │   │  {ArrayRef}
         │   │   │   ├──ref: 
         │   │   │   │  {DirectNamedRef ref:{NamedElement name=aarr}}
         │   │   │   └──index: 
         │   │   │      {IntConstant value=5}
         │   │   └──ref: 
         │   │      {ArrayRef}
         │   │       ├──ref: 
         │   │       │  {DirectNamedRef ref:{NamedElement name=barr}}
         │   │       └──index: 
         │   │          {IntConstant value=1}
         │   └──ref: 
         │      {ArrayRef}
         │       ├──ref: 
         │       │  {DirectNamedRef ref:{NamedElement name=carr}}
         │       └──index: 
         │          {IntConstant value=9}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {IntConstant value=1}
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {QualifiedRef}
         │   ├──prefix: 
         │   │  {QualifiedRef}
         │   │   ├──prefix: 
         │   │   │  {ArrayRef}
         │   │   │   ├──ref: 
         │   │   │   │  {DirectNamedRef ref:{NamedElement name=aarr}}
         │   │   │   └──index: 
         │   │   │      {IntConstant value=6}
         │   │   └──ref: 
         │   │      {ArrayRef}
         │   │       ├──ref: 
         │   │       │  {DirectNamedRef ref:{NamedElement name=barr}}
         │   │       └──index: 
         │   │          {IntConstant value=2}
         │   └──ref: 
         │      {ArrayRef}
         │       ├──ref: 
         │       │  {DirectNamedRef ref:{NamedElement name=carr}}
         │       └──index: 
         │          {IntConstant value=10}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {IntConstant value=1}
    {UserDefinedDataType name=test013_udt1}
     ├──attributes: <null>
     └──declaration: 
        {StructDT}
         └──members: 
            {VariableDeclarationLine initialized=false bounded=false}
             ├──variables: 
             │  {Variable name=t1 reference=false ref:<null>}
             ├──type: 
             │  {ElementaryDT type=INT}
             ├──initialization: <null>
             ├──lowerBound: <null>
             └──upperBound: <null>
            {VariableDeclarationLine initialized=false bounded=false}
             ├──variables: 
             │  {Variable name=t2 reference=false ref:<null>}
             ├──type: 
             │  {ElementaryDT type=BOOL}
             ├──initialization: <null>
             ├──lowerBound: <null>
             └──upperBound: <null>
    {UserDefinedDataType name=test013_udt2}
     ├──attributes: <null>
     └──declaration: 
        {StructDT}
         └──members: 
            {VariableDeclarationLine initialized=false bounded=false}
             ├──variables: 
             │  {Variable name=t3 reference=false ref:<null>}
             ├──type: 
             │  {ArrayDT}
             │   ├──dimensions: 
             │   │  {ArrayDimensionString rangeString=1..5}
             │   └──baseType: 
             │      {ElementaryDT type=INT}
             ├──initialization: <null>
             ├──lowerBound: <null>
             └──upperBound: <null>
            {VariableDeclarationLine initialized=false bounded=false}
             ├──variables: 
             │  {Variable name=t4 reference=false ref:<null>}
             ├──type: 
             │  {StructDT}
             │   └──members: 
             │      {VariableDeclarationLine initialized=false bounded=false}
             │       ├──variables: 
             │       │  {Variable name=x reference=false ref:<null>}
             │       ├──type: 
             │       │  {ElementaryDT type=INT}
             │       ├──initialization: <null>
             │       ├──lowerBound: <null>
             │       └──upperBound: <null>
             │      {VariableDeclarationLine initialized=false bounded=false}
             │       ├──variables: 
             │       │  {Variable name=z reference=false ref:<null>}
             │       ├──type: 
             │       │  {ElementaryDT type=BOOL}
             │       ├──initialization: <null>
             │       ├──lowerBound: <null>
             │       └──upperBound: <null>
             ├──initialization: <null>
             ├──lowerBound: <null>
             └──upperBound: <null>
    {DataBlock name=shared_db7}
     ├──attributes: <null>
     ├──structure: 
     │  {FbOrUdtDT type:{NamedElement name=test013_udt1}}
     └──initialAssignments: <empty>
    {DataBlock name=shared_db8}
     ├──attributes: <null>
     ├──structure: 
     │  {FbOrUdtDT type:{NamedElement name=test013_udt1}}
     └──initialAssignments: 
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {DirectNamedRef ref:{NamedElement name=t1}}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {IntConstant value=10}
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {DirectNamedRef ref:{NamedElement name=t2}}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {BoolConstant value=TRUE}
    {DataBlock name=shared_db9}
     ├──attributes: <null>
     ├──structure: 
     │  {FbOrUdtDT type:{NamedElement name=test013_udt2}}
     └──initialAssignments: <empty>
    {DataBlock name=shared_db10}
     ├──attributes: <null>
     ├──structure: 
     │  {FbOrUdtDT type:{NamedElement name=test013_udt2}}
     └──initialAssignments: 
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {ArrayRef}
         │   ├──ref: 
         │   │  {DirectNamedRef ref:{NamedElement name=t3}}
         │   └──index: 
         │      {IntConstant value=1}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {IntConstant value=1}
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {ArrayRef}
         │   ├──ref: 
         │   │  {DirectNamedRef ref:{NamedElement name=t3}}
         │   └──index: 
         │      {IntConstant value=2}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {IntConstant value=2}
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {QualifiedRef}
         │   ├──prefix: 
         │   │  {DirectNamedRef ref:{NamedElement name=t4}}
         │   └──ref: 
         │      {DirectNamedRef ref:{NamedElement name=x}}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {IntConstant value=10}
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {QualifiedRef}
         │   ├──prefix: 
         │   │  {DirectNamedRef ref:{NamedElement name=t4}}
         │   └──ref: 
         │      {DirectNamedRef ref:{NamedElement name=z}}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {BoolConstant value=true}
    {FunctionBlock name=test013_fb1 builtIn=false}
     ├──attributes: <null>
     ├──declarationSection: 
     │  {DeclarationSection}
     │   ├──variableDeclarations: 
     │   │  {VariableDeclarationBlock direction=VAR_INPUT retain=false}
     │   │   └──variables: 
     │   │      {VariableDeclarationLine initialized=false bounded=false}
     │   │       ├──variables: 
     │   │       │  {Variable name=in1 reference=false ref:<null>}
     │   │       ├──type: 
     │   │       │  {ElementaryDT type=INT}
     │   │       ├──initialization: <null>
     │   │       ├──lowerBound: <null>
     │   │       └──upperBound: <null>
     │   │  {VariableDeclarationBlock direction=VAR_OUTPUT retain=false}
     │   │   └──variables: 
     │   │      {VariableDeclarationLine initialized=false bounded=false}
     │   │       ├──variables: 
     │   │       │  {Variable name=out1 reference=false ref:<null>}
     │   │       ├──type: 
     │   │       │  {ElementaryDT type=BOOL}
     │   │       ├──initialization: <null>
     │   │       ├──lowerBound: <null>
     │   │       └──upperBound: <null>
     │   ├──constantDeclarations: <empty>
     │   └──labelDeclarations: <empty>
     └──statements: <null>
    {DataBlock name=instance_db11}
     ├──attributes: <null>
     ├──structure: 
     │  {FbOrUdtDT type:{NamedElement name=test013_fb1}}
     └──initialAssignments: <empty>
    {DataBlock name=instance_db12}
     ├──attributes: <null>
     ├──structure: 
     │  {FbOrUdtDT type:{NamedElement name=test013_fb1}}
     └──initialAssignments: 
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {DirectNamedRef ref:{NamedElement name=in1}}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {IntConstant value=10}
        {DataBlockInitialAssignment}
         ├──leftValue: 
         │  {DirectNamedRef ref:{NamedElement name=out1}}
         └──constant: 
            {UnnamedConstantRef}
             └──constant: 
                {BoolConstant value=false}
