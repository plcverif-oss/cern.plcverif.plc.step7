{ProgramFile}
 ├──verificationOptions: <empty>
 ├──globalVariables: <empty>
 └──programUnits: 
    {FunctionBlock name=fb3 builtIn=false}
     ├──attributes: <null>
     ├──declarationSection: 
     │  {DeclarationSection}
     │   ├──variableDeclarations: 
     │   │  {VariableDeclarationBlock direction=VAR retain=false}
     │   │   └──variables: 
     │   │      {VariableDeclarationLine initialized=false bounded=false}
     │   │       ├──variables: 
     │   │       │  {Variable name=i reference=false ref:<null>}
     │   │       │  {Variable name=j reference=false ref:<null>}
     │   │       ├──type: 
     │   │       │  {ElementaryDT type=INT}
     │   │       ├──initialization: <null>
     │   │       ├──lowerBound: <null>
     │   │       └──upperBound: <null>
     │   │  {VariableDeclarationBlock direction=VAR_TEMP retain=false}
     │   │   └──variables: 
     │   │      {VariableDeclarationLine initialized=false bounded=false}
     │   │       ├──variables: 
     │   │       │  {Variable name=___nested_ret_val4 reference=false ref:<null>}
     │   │       ├──type: 
     │   │       │  {ElementaryDT type=INT}
     │   │       ├──initialization: <null>
     │   │       ├──lowerBound: <null>
     │   │       └──upperBound: <null>
     │   │  {VariableDeclarationBlock direction=VAR_TEMP retain=false}
     │   │   └──variables: 
     │   │      {VariableDeclarationLine initialized=false bounded=false}
     │   │       ├──variables: 
     │   │       │  {Variable name=___nested_ret_val5 reference=false ref:<null>}
     │   │       ├──type: 
     │   │       │  {ElementaryDT type=INT}
     │   │       ├──initialization: <null>
     │   │       ├──lowerBound: <null>
     │   │       └──upperBound: <null>
     │   ├──constantDeclarations: <empty>
     │   └──labelDeclarations: <empty>
     └──statements: 
        {SclStatementList}
         └──statements: 
            {SclAssignmentStatement}
             ├──leftValue: 
             │  {DirectNamedRef ref:___nested_ret_val4}
             └──rightValue: 
                {SclSubroutineCall hasExplicitDataBlock=false calledUnit:MAX3 dataBlock:<null>}
                 └──callParameter: 
                    {CallParameterList}
                     └──parameters: 
                        {NamedCallParameterItem assignmentOperator=:= parameter:in1}
                         └──value: 
                            {IntConstant value=11}
                        {NamedCallParameterItem assignmentOperator=:= parameter:in2}
                         └──value: 
                            {IntConstant value=22}
            {SclAssignmentStatement}
             ├──leftValue: 
             │  {DirectNamedRef ref:___nested_ret_val5}
             └──rightValue: 
                {SclSubroutineCall hasExplicitDataBlock=false calledUnit:MAX3 dataBlock:<null>}
                 └──callParameter: 
                    {CallParameterList}
                     └──parameters: 
                        {NamedCallParameterItem assignmentOperator=:= parameter:in1}
                         └──value: 
                            {IntConstant value=33}
                        {NamedCallParameterItem assignmentOperator=:= parameter:in2}
                         └──value: 
                            {IntConstant value=44}
            {SclAssignmentStatement}
             ├──leftValue: 
             │  {DirectNamedRef ref:j}
             └──rightValue: 
                {SclSubroutineCall hasExplicitDataBlock=false calledUnit:MAX3 dataBlock:<null>}
                 └──callParameter: 
                    {CallParameterList}
                     └──parameters: 
                        {NamedCallParameterItem assignmentOperator=:= parameter:in1}
                         └──value: 
                            {DirectNamedRef ref:___nested_ret_val4}
                        {NamedCallParameterItem assignmentOperator=:= parameter:in2}
                         └──value: 
                            {DirectNamedRef ref:___nested_ret_val5}
    {Function name=MAX3 builtIn=false}
     ├──attributes: <null>
     ├──declarationSection: 
     │  {DeclarationSection}
     │   ├──variableDeclarations: 
     │   │  {VariableDeclarationBlock direction=VAR_INPUT retain=false}
     │   │   └──variables: 
     │   │      {VariableDeclarationLine initialized=false bounded=false}
     │   │       ├──variables: 
     │   │       │  {Variable name=in1 reference=false ref:<null>}
     │   │       │  {Variable name=in2 reference=false ref:<null>}
     │   │       ├──type: 
     │   │       │  {ElementaryDT type=INT}
     │   │       ├──initialization: <null>
     │   │       ├──lowerBound: <null>
     │   │       └──upperBound: <null>
     │   ├──constantDeclarations: <empty>
     │   └──labelDeclarations: <empty>
     ├──statements: 
     │  {SclStatementList}
     │   └──statements: 
     │      {SclIfStatement else=true}
     │       ├──condition: 
     │       │  {ComparisonExpression operator=>}
     │       │   ├──left: 
     │       │   │  {DirectNamedRef ref:in1}
     │       │   └──right: 
     │       │      {DirectNamedRef ref:in2}
     │       ├──thenBranch: 
     │       │  {SclStatementList}
     │       │   └──statements: 
     │       │      {SclAssignmentStatement}
     │       │       ├──leftValue: 
     │       │       │  {DirectNamedRef ref:MAX3}
     │       │       └──rightValue: 
     │       │          {DirectNamedRef ref:in1}
     │       ├──elsifBranches: <empty>
     │       └──elseBranch: 
     │          {SclStatementList}
     │           └──statements: 
     │              {SclAssignmentStatement}
     │               ├──leftValue: 
     │               │  {DirectNamedRef ref:MAX3}
     │               └──rightValue: 
     │                  {DirectNamedRef ref:in2}
     └──returnType: 
        {ElementaryDT type=INT}
