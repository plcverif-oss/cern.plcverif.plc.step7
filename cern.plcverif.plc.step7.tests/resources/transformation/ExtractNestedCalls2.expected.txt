{ProgramFile}
 ├──verificationOptions: <empty>
 ├──globalVariables: <empty>
 └──programUnits: 
    {FunctionBlock name=fb2 builtIn=false}
     ├──attributes: <null>
     ├──declarationSection: 
     │  {DeclarationSection}
     │   ├──variableDeclarations: 
     │   │  {VariableDeclarationBlock direction=VAR retain=false}
     │   │   └──variables: 
     │   │      {VariableDeclarationLine initialized=false bounded=false}
     │   │       ├──variables: 
     │   │       │  {Variable name=i reference=false ref:<null>}
     │   │       │  {Variable name=j reference=false ref:<null>}
     │   │       ├──type: 
     │   │       │  {ElementaryDT type=INT}
     │   │       ├──initialization: <null>
     │   │       ├──lowerBound: <null>
     │   │       └──upperBound: <null>
     │   │  {VariableDeclarationBlock direction=VAR_TEMP retain=false}
     │   │   └──variables: 
     │   │      {VariableDeclarationLine initialized=false bounded=false}
     │   │       ├──variables: 
     │   │       │  {Variable name=___nested_ret_val2 reference=false ref:<null>}
     │   │       ├──type: 
     │   │       │  {ElementaryDT type=INT}
     │   │       ├──initialization: <null>
     │   │       ├──lowerBound: <null>
     │   │       └──upperBound: <null>
     │   │  {VariableDeclarationBlock direction=VAR_TEMP retain=false}
     │   │   └──variables: 
     │   │      {VariableDeclarationLine initialized=false bounded=false}
     │   │       ├──variables: 
     │   │       │  {Variable name=___nested_ret_val3 reference=false ref:<null>}
     │   │       ├──type: 
     │   │       │  {ElementaryDT type=INT}
     │   │       ├──initialization: <null>
     │   │       ├──lowerBound: <null>
     │   │       └──upperBound: <null>
     │   ├──constantDeclarations: <empty>
     │   └──labelDeclarations: <empty>
     └──statements: 
        {SclStatementList}
         └──statements: 
            {SclAssignmentStatement}
             ├──leftValue: 
             │  {DirectNamedRef ref:___nested_ret_val3}
             └──rightValue: 
                {SclSubroutineCall hasExplicitDataBlock=false calledUnit:MIN dataBlock:<null>}
                 └──callParameter: 
                    {CallParameterList}
                     └──parameters: 
                        {NamedCallParameterItem assignmentOperator=:= parameter:in1}
                         └──value: 
                            {IntConstant value=3}
                        {NamedCallParameterItem assignmentOperator=:= parameter:in2}
                         └──value: 
                            {IntConstant value=4}
            {SclAssignmentStatement}
             ├──leftValue: 
             │  {DirectNamedRef ref:___nested_ret_val2}
             └──rightValue: 
                {SclSubroutineCall hasExplicitDataBlock=false calledUnit:MIN dataBlock:<null>}
                 └──callParameter: 
                    {CallParameterList}
                     └──parameters: 
                        {NamedCallParameterItem assignmentOperator=:= parameter:in1}
                         └──value: 
                            {IntConstant value=2}
                        {NamedCallParameterItem assignmentOperator=:= parameter:in2}
                         └──value: 
                            {AdditiveExpression operator=+}
                             ├──left: 
                             │  {MultiplicativeExpression operator=*}
                             │   ├──left: 
                             │   │  {IntConstant value=2}
                             │   └──right: 
                             │      {DirectNamedRef ref:___nested_ret_val3}
                             └──right: 
                                {IntConstant value=6}
            {SclAssignmentStatement}
             ├──leftValue: 
             │  {DirectNamedRef ref:j}
             └──rightValue: 
                {SclSubroutineCall hasExplicitDataBlock=false calledUnit:MIN dataBlock:<null>}
                 └──callParameter: 
                    {CallParameterList}
                     └──parameters: 
                        {NamedCallParameterItem assignmentOperator=:= parameter:in1}
                         └──value: 
                            {IntConstant value=1}
                        {NamedCallParameterItem assignmentOperator=:= parameter:in2}
                         └──value: 
                            {AdditiveExpression operator=+}
                             ├──left: 
                             │  {DirectNamedRef ref:___nested_ret_val2}
                             └──right: 
                                {IntConstant value=5}
    {Function name=MIN builtIn=false}
     ├──attributes: <null>
     ├──declarationSection: 
     │  {DeclarationSection}
     │   ├──variableDeclarations: 
     │   │  {VariableDeclarationBlock direction=VAR_INPUT retain=false}
     │   │   └──variables: 
     │   │      {VariableDeclarationLine initialized=false bounded=false}
     │   │       ├──variables: 
     │   │       │  {Variable name=in1 reference=false ref:<null>}
     │   │       │  {Variable name=in2 reference=false ref:<null>}
     │   │       ├──type: 
     │   │       │  {ElementaryDT type=INT}
     │   │       ├──initialization: <null>
     │   │       ├──lowerBound: <null>
     │   │       └──upperBound: <null>
     │   ├──constantDeclarations: <empty>
     │   └──labelDeclarations: <empty>
     ├──statements: 
     │  {SclStatementList}
     │   └──statements: 
     │      {SclIfStatement else=true}
     │       ├──condition: 
     │       │  {ComparisonExpression operator=>}
     │       │   ├──left: 
     │       │   │  {DirectNamedRef ref:in1}
     │       │   └──right: 
     │       │      {DirectNamedRef ref:in2}
     │       ├──thenBranch: 
     │       │  {SclStatementList}
     │       │   └──statements: 
     │       │      {SclAssignmentStatement}
     │       │       ├──leftValue: 
     │       │       │  {DirectNamedRef ref:MIN}
     │       │       └──rightValue: 
     │       │          {DirectNamedRef ref:in1}
     │       ├──elsifBranches: <empty>
     │       └──elseBranch: 
     │          {SclStatementList}
     │           └──statements: 
     │              {SclAssignmentStatement}
     │               ├──leftValue: 
     │               │  {DirectNamedRef ref:MIN}
     │               └──rightValue: 
     │                  {DirectNamedRef ref:in2}
     └──returnType: 
        {ElementaryDT type=INT}
