/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.symboltable

import org.junit.Test
import java.util.Collections
import org.junit.Assert
import java.util.List
import cern.plcverif.plc.step7.symboltable.SymbolTableParser.PortalVersion

class SymbolsToGlobalVarsTest {
	@Test
	def void test1() {
		val symbolTable = '''"var1","I0.0","BOOL","Comment"'''
		
		val symbols = Collections.singletonList(SymbolTableParser.parse(symbolTable, PortalVersion.STEP7).get);
		val result = SymbolsToGlobalVars.represent(symbols).trim;
		
		val expected = '''
			VAR_GLOBAL_INPUT
				var1 : BOOL; // Comment (I0.0)
			END_VAR
		'''.toString.trim;
		
		Assert.assertEquals(expected.replace(" ",""), result.replace(" ", ""));
	}
	
	@Test
	def void test2() {
		val symbolTable = '''"var1","%I0.0","Bool","True","True","False","Comment","","True"'''
		
		val symbols = Collections.singletonList(SymbolTableParser.parse(symbolTable, PortalVersion.TIA).get);
		val result = SymbolsToGlobalVars.represent(symbols).trim;
		
		val expected = '''
			VAR_GLOBAL_INPUT
				var1 : Bool; // Comment (%I0.0)
			END_VAR
		'''.toString.trim;
		
		Assert.assertEquals(expected.replace(" ",""), result.replace(" ", ""));
	}
	
	@Test
	def void test3() {
		val symbolTable = 
			'''"outvar1","Q  0.0","BOOL","Comment1"
			   "invar1","IW0","INT","Comment2"
			   "var1","M   0.0","BOOL","Comment3"
			   "instance1","DB   124","FB   123","Comment4"
			   "fb1","FB   123","FB   123","Comment5"
			'''.toString.split("\\n");
		
		val List<Symbol> symbols =  symbolTable.map[it | SymbolTableParser.parse(it, PortalVersion.STEP7)].filter[it | it.present].map[it | it.get].toList;
		val result = SymbolsToGlobalVars.represent(symbols).trim;
		
		val expected = '''
			VAR_GLOBAL_INPUT
				invar1 : INT; // Comment2 (IW0)
			END_VAR
			
			VAR_GLOBAL_OUTPUT
				outvar1 : BOOL; // Comment1 (Q0.0)
			END_VAR
			
			VAR_GLOBAL
				var1 : BOOL; // Comment3 (M0.0)
			END_VAR
			
			DATA_BLOCK instance1 fb1 BEGIN END_DATA_BLOCK // Comment4 (DB124)
		'''.toString.trim;
		
		Assert.assertEquals(expected.replace(" ",""), result.replace(" ", ""));
	}
	
	@Test
	def void test4() {
		val symbolTable = 
			'''"outvar1","Q  0.0","BOOL","True","True","False","Comment1","","True"
			   "invar1","IW0","INT","True","True","False","Comment2","","True"
			   "var1","M   0.0","BOOL","True","True","False","Comment3","","True"
			   "instance1","DB   124","FB   123","True","True","False","Comment4","","True"
			   "fb1","FB   123","FB   123","True","True","False","Comment5","","True"
			'''.toString.split("\\n");
		
		val List<Symbol> symbols =  symbolTable.map[it | SymbolTableParser.parse(it, PortalVersion.TIA)].filter[it | it.present].map[it | it.get].toList;
		val result = SymbolsToGlobalVars.represent(symbols).trim;
		
		val expected = '''
			VAR_GLOBAL_INPUT
				invar1 : INT; // Comment2 (IW0)
			END_VAR
			
			VAR_GLOBAL_OUTPUT
				outvar1 : BOOL; // Comment1 (Q0.0)
			END_VAR
			
			VAR_GLOBAL
				var1 : BOOL; // Comment3 (M0.0)
			END_VAR
			
			DATA_BLOCK instance1 fb1 BEGIN END_DATA_BLOCK // Comment4 (DB124)
		'''.toString.trim;
		
		Assert.assertEquals(expected.replace(" ",""), result.replace(" ", ""));
	}
}