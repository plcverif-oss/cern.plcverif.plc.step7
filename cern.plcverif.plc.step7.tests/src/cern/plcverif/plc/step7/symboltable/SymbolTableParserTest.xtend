/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.symboltable

import org.junit.Test

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertTrue
import static org.junit.Assert.assertFalse
import cern.plcverif.plc.step7.symboltable.SymbolTableParser.PortalVersion

class SymbolTableParserTest {
	@Test
	def void parseMemorySymbol() {
		// Arrange
		val input = '''"apple  ","M    1452.7 ","BOOL      ","This is a long description                                           "''';

		// Act
		val output = SymbolTableParser.parse(input, PortalVersion.STEP7);

		// Assert
		assertTrue(output.isPresent());
		assertTrue(output.get() instanceof MemorySymbol);
		val outputMemorySymbol = output.get() as MemorySymbol;
		assertEquals("apple", outputMemorySymbol.name);
		assertEquals("BOOL", outputMemorySymbol.type);
		assertEquals("This is a long description", outputMemorySymbol.comment);
		assertTrue(outputMemorySymbol.address.syntacticallyValid);
		assertEquals("M    1452.7", outputMemorySymbol.address.originalStringRepresentation);
	}

	@Test
	def void parseFbSymbol() {
		// Arrange
		val input = '''"AVG                     ","FB    301   ","FB    301 ","FB to calculate average                   "''';

		// Act
		val output = SymbolTableParser.parse(input, PortalVersion.STEP7);

		// Assert
		assertTrue(output.isPresent());
		assertTrue(output.get() instanceof FbSymbol);
		val outputFbSymbol = output.get() as FbSymbol;
		assertEquals("AVG", outputFbSymbol.name);
		assertEquals(301, outputFbSymbol.fbNumber);
		assertEquals("FB to calculate average", outputFbSymbol.comment);
	}

	@Test
	def void parseInstanceSymbol() {
		// Arrange
		val input = '''"InstanceName             ","DB    364   ","FB    348 ","DB instance of FB348                                                      "''';

		// Act
		val output = SymbolTableParser.parse(input, PortalVersion.STEP7);

		// Assert
		assertTrue(output.isPresent());
		assertTrue(output.get() instanceof InstanceSymbol);
		val outputInstSymbol = output.get() as InstanceSymbol;
		assertEquals("InstanceName", outputInstSymbol.name);
		assertEquals(348, outputInstSymbol.fbNumber);
		assertEquals("DB    364", outputInstSymbol.address);
		assertEquals("DB instance of FB348", outputInstSymbol.comment);
	}

	@Test
	def void parseUdtSymbol() {
		// Arrange
		val input = '''"UdtName             ","UDT    364   ","UDT    364 ","UDT comment                                                      "''';

		// Act
		val output = SymbolTableParser.parse(input, PortalVersion.STEP7);

		// Assert
		assertFalse(output.isPresent());
	}

	@Test
	def void parseUnsuccessful1() {
		// Arrange
		val input = '''"Name             ","DB    364   ","FB    364 "''';

		// Act
		val output = SymbolTableParser.parse(input, PortalVersion.STEP7);

		// Assert
		assertFalse(output.isPresent());
	}

	@Test
	def void parseUnsuccessful2() {
		// Arrange
		val input = '''abcdefg''';

		// Act
		val output = SymbolTableParser.parse(input, PortalVersion.STEP7);

		// Assert
		assertFalse(output.isPresent());
	}
}
