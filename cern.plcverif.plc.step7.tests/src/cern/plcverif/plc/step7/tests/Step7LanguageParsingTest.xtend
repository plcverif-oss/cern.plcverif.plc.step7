/******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 * 	 Jean-Charles Tournier - November 2019 - add support for DirectBitAccessRef
 *****************************************************************************/
package cern.plcverif.plc.step7.tests

import cern.plcverif.base.common.emf.textual.EmfModelPrinter
import cern.plcverif.base.common.utils.IoUtils
import cern.plcverif.plc.step7.Step7EObjectToText
import cern.plcverif.plc.step7.generator.Step7LanguageGenerator
import cern.plcverif.plc.step7.step7Language.ProgramFile
import com.google.inject.Inject
import java.util.HashSet
import java.util.Set
import org.eclipse.xtext.diagnostics.Severity
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.eclipse.xtext.util.CancelIndicator
import org.eclipse.xtext.validation.CheckMode
import org.eclipse.xtext.validation.IResourceValidator
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import java.io.File
import java.nio.file.Paths
import java.nio.file.Files

@RunWith(XtextRunner)
@InjectWith(Step7LanguageInjectorProvider)
class Step7LanguageParsingTest {
	@Inject
	ParseHelper<ProgramFile> parseHelper

	@Inject extension ParseHelper<ProgramFile>
	//@Inject extension ValidationTestHelper
	@Inject ValidationTestHelper validationTester
	@Inject IResourceValidator resourceValidator
	
	public static final boolean CONTINUE_ON_ASSERT_VIOLATION = false; // NOPMD
	public static boolean writeActualOutputs = CONTINUE_ON_ASSERT_VIOLATION;


	@Test
	def void smokeTest() {
		val result = parseHelper.parse('''
			FUNCTION_BLOCK fb1
				VAR x : BOOL; END_VAR
			BEGIN 
				x := TRUE;
			END_FUNCTION_BLOCK 
		''');
		validationTester.assertNoIssues(result);
		Assert.assertNotNull(result);
	}

	@Test
	def void smokeTest2() {
		val result = '''
			FUNCTION_BLOCK fb1
				VAR x : BOOL; END_VAR
			BEGIN 
				x := TRUE;
			END_FUNCTION_BLOCK 
		'''.parse;
		validationTester.assertNoIssues(result);
		Assert.assertNotNull(result);
	}

	@Test
	def void testSclValid() {
		// This should be a parameterized test, but it is not properly supported by Xtext. 
		testValid("/testcases/scl/valid/Function");
		testValid("/testcases/scl/valid/FunctionBlock");
		testValid("/testcases/scl/valid/OrganizationBlock");
		testValid("/testcases/scl/valid/DataTypes");
		testValid("/testcases/scl/valid/SclStatements");
		testValid("/testcases/scl/valid/ExpressionTyping");
		testValid("/testcases/scl/valid/Constants");	
		
		testValid("/testcases/scl/valid/SiemensExamples-Chapter6");
		testValid("/testcases/scl/valid/SiemensExamples-Chapter7");
		testValid("/testcases/scl/valid/SiemensExamples-Chapter8");
		testValid("/testcases/scl/valid/SiemensExamples-Chapter12");
		testValid("/testcases/scl/valid/Metadata");
		testValid("/testcases/scl/valid/VariableInitialization");
		testValid("/testcases/scl/valid/DataBlock");
		
		System.err.println("testSclValid: ALL OK");
	}
	
	@Test
	def void testStlValid() {
		testValid("/testcases/stl/valid/StlApostropheNetwork");
		testValid("/testcases/stl/valid/StlBasicLogicStatements");
		testValid("/testcases/stl/valid/StlComparisonStatements");
		testValid("/testcases/stl/valid/StlIoAddresses");
		testValid("/testcases/stl/valid/StlJumps");
		testValid("/testcases/stl/valid/StlMath");
		testValid("/testcases/stl/valid/StlNestingStack");
		testValid("/testcases/stl/valid/StlSubroutineCall");
		testValid("/testcases/stl/valid/StlShiftInstructions");
		
		System.err.println("testStlValid: ALL OK");
	}
	
	@Test
	def void testStlInvalid() {
		testInvalid("/testcases/stl/invalid/StlJumpsInvalid");
		
		System.err.println("testStlInvalid: ALL OK");
	}
	
	@Test
	def void testSclInvalid() {
		testInvalid("/testcases/scl/invalid/BlockNameUniqueness");
		testInvalid("/testcases/scl/invalid/CallValidation");
		testInvalid("/testcases/scl/invalid/DataTypeValidation");
		testInvalid("/testcases/scl/invalid/FieldNameUniqueness");
		testInvalid("/testcases/scl/invalid/InvalidExpressionTyping");
		testInvalid("/testcases/scl/invalid/MissingNameInExpression");
		testInvalid("/testcases/scl/invalid/TiaValidation");
		testInvalid("/testcases/scl/invalid/Typing");
		testInvalid("/testcases/scl/invalid/ValidationRules");
		testInvalid("/testcases/scl/invalid/VariableDeclValidation");
		testInvalid("/testcases/scl/invalid/NonSclElements");
		testInvalid("/testcases/scl/invalid/VarViewValidation");
		testInvalid("/testcases/scl/invalid/UnsupportedFeatures");
	}
	
	@Test
	def void testSclInvalidDirectBitAccessRef() {
		testInvalid("/testcases/scl/invalid/DirectBitAccessNotWord");
		testInvalid("/testcases/scl/invalid/DirectBitAccessOutOfBound");
		
		System.err.println("testSclInvalid: ALL OK");
	}
	
	@Test
	def void testStlInvalidDirectBitAccessRef() {
		testInvalid("/testcases/stl/invalid/DirectBitAccess");
		
		System.err.println("testSclInvalid: ALL OK");
	}

	def void testValid(String pathBase) {
		print('''Testing «pathBase»... ''');

		// Load SCL file and reference text representation
		val sclFile = readResourceFile(pathBase + ".scl");
		Assert.assertNotNull(sclFile);
		var txtFile = ""
		if(!CONTINUE_ON_ASSERT_VIOLATION){
			txtFile = readResourceFile(pathBase + ".txt");
			Assert.assertNotNull(txtFile);
		}

		// Parse
		val parsedScl = parseHelper.parse(sclFile);
		Assert.assertNotNull(parsedScl);
		// force validation for NORMAL rules too
		//val validationIssues = parsedScl.validate;
		val validationIssues = resourceValidator.validate(parsedScl.eResource, CheckMode.ALL, CancelIndicator.NullImpl); 
		val validationErrors = validationIssues.filter[it.severity == Severity.ERROR];
		validationErrors.forEach[System.err.println('''Validation error in «pathBase»: «it»''')]; 
		Assert.assertTrue('''The SCL file '«pathBase».scl' contains errors.''', validationErrors.isEmpty);

		// Compare textual representations
		val parsedSclInText = EmfModelPrinter.print(parsedScl, Step7EObjectToText.INSTANCE);
		
		if (writeActualOutputs) {
			val writeFile = new File(Files.createDirectories(Paths.get("temp_resources",pathBase.split("/").get(1),pathBase.split("/").get(2),pathBase.split("/").get(3))).toString, String.format("%s.txt" , pathBase.split("/").get(4)));
			IoUtils.writeAllContent(writeFile, parsedSclInText.toString);
		}
		
		if(!CONTINUE_ON_ASSERT_VIOLATION){
			Assert.assertEquals('''The AST of SCL file '«pathBase».scl' does not match the expectations.''',txtFile.trim, parsedSclInText.toString.trim);
			println("OK");
		}
	}

	def void testInvalid(String pathBase) {
		print('''Testing «pathBase» (invalid)... ''');

		// Load SCL file and reference text representation
		val sclFile = readResourceFile(pathBase + ".scl");
		val txtFile = readResourceFile(pathBase + ".txt");
		Assert.assertNotNull(sclFile);
		Assert.assertNotNull(txtFile);

		// Parse
		val parsedScl = parseHelper.parse(sclFile);
		Assert.assertNotNull(parsedScl);
		
		val validationIssues = resourceValidator.validate(parsedScl.eResource, CheckMode.ALL, CancelIndicator.NullImpl);
		val validationErrors = validationIssues.filter[it.severity == Severity.ERROR];
		Assert.assertFalse("The SCL file does not contain errors.", validationErrors.isEmpty);
		val validationErrorsWarnings = validationIssues.filter[it.severity == Severity.ERROR || it.severity == Severity.WARNING];

		// Compare textual representations (change in the order of lines permitted)
		//  -- sometimes the order of certain errors is not the same (if they are at the exact same location in the file)
		// Note: currently the severities are not compared (i.e., test is insensitive to changing the severity from WARN to ERROR)
		val errorsInText = Step7LanguageGenerator.errorsAndWarningsToString(validationErrorsWarnings);
		
		val expectedLinesSet = txtFile.linesAsSet;
		val actualLinesSet = errorsInText.toString.linesAsSet;
		if (!expectedLinesSet.equals(actualLinesSet)) {
			// try to diagnose
			printLineDiffDiagnosis(expectedLinesSet, actualLinesSet);
		}
		
		if (writeActualOutputs) {
			val writeFile = new File(Files.createDirectories(Paths.get("temp_resources",pathBase.split("/").get(1),pathBase.split("/").get(2),pathBase.split("/").get(3))).toString, String.format("%s.txt" , pathBase.split("/").get(4)));
			IoUtils.writeAllContent(writeFile, errorsInText.toString);
		}
		
		if(!CONTINUE_ON_ASSERT_VIOLATION){
			Assert.assertEquals(String.join("\n", expectedLinesSet.toList.sort), String.join("\n", actualLinesSet.toList.sort));
			println("OK");
		}
	}
	
	def printLineDiffDiagnosis(Set<String> expectedLinesSet, Set<String> actualLinesSet) {
		println();
		println("Lines missing from actual output:");
		val set = new HashSet(expectedLinesSet);
		set.removeAll(actualLinesSet);
		set.forEach[it | println(it)];
		
		println("Lines present in actual output additionally compared to expected:");
		val set2 = new HashSet(actualLinesSet);
		set2.removeAll(expectedLinesSet);
		set2.forEach[it | println(it)];
		
		if (set.isEmpty && set2.isEmpty) {
			System.err.println("NOTHING TO DIAGNOSE.");
		}
	}
	
	private def Set<String> linesAsSet(String text) {
		return text.replaceAll("(" + System.lineSeparator() + ")+",System.lineSeparator()).trim.split(System.lineSeparator()).toSet;
	}

	private def readResourceFile(String path) {
		return IoUtils.readResourceFile(path, this.class.classLoader);
	}
	
	@Test
	def void sentinel() {
		Assert.assertFalse(CONTINUE_ON_ASSERT_VIOLATION);
	}
	
}
