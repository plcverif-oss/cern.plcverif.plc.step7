/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.tests;

import org.junit.Assert;
import org.junit.Test;

import cern.plcverif.plc.step7.util.Step7LanguageHelper;

public class Step7LanguageHelperTest {
	@SuppressWarnings("boxing")
	@Test
	public void testIsBlockIdentifier() {
		Assert.assertEquals(true, Step7LanguageHelper.isBlockIdentifier("fb123"));
		Assert.assertEquals(true, Step7LanguageHelper.isBlockIdentifier("fc123"));
		Assert.assertEquals(true, Step7LanguageHelper.isBlockIdentifier("ob1234"));
		Assert.assertEquals(true, Step7LanguageHelper.isBlockIdentifier("sfb123"));
		Assert.assertEquals(true, Step7LanguageHelper.isBlockIdentifier("sfc12"));
		Assert.assertEquals(true, Step7LanguageHelper.isBlockIdentifier("UDT1"));

		Assert.assertEquals(false, Step7LanguageHelper.isBlockIdentifier("test"));
		Assert.assertEquals(false, Step7LanguageHelper.isBlockIdentifier("test123"));
	}
}
