/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.tests

import cern.plcverif.plc.step7.step7Language.Expression
import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.step7Language.SclAssignmentStatement
import cern.plcverif.plc.step7.util.SimpleExpressionUtil
import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.eclipse.xtext.diagnostics.Severity

@RunWith(XtextRunner)
@InjectWith(Step7LanguageInjectorProvider)
class SimpleExpressionTest {
	@Inject extension ParseHelper<ProgramFile>
	@Inject extension ValidationTestHelper

	private def Expression parseExpression(String expression) {
		Assert.assertFalse(expression.contains(";"));
		val dummyProgram = '''
			FUNCTION_BLOCK test
				CONST
					c_ten := 10;
					c_twotimesten := 2 * 10;
				END_CONST
				VAR
					varint1 : INT;
					varint2 : INT;
					res : REAL;
				END_VAR
			BEGIN
				res := «expression»;
			END_FUNCTION_BLOCK
		'''
		val program = dummyProgram.parse
		val errors = program.validate.filter[it.severity == Severity.ERROR];
		errors.forEach[item|println('''«item.message» at line «item.lineNumber»''')];
		Assert.assertTrue("The test program is not error free.", errors.size == 0);
		val assignment = program.eAllContents.filter(typeof(SclAssignmentStatement)).findFirst[true]
		Assert.assertNotNull(assignment)
		return assignment.rightValue
	}

	@Test
	def void testValid() {
		val validSimpleExpressions = #[
			"12" -> 12,
			"1 + 2" -> 3,
			"(1+2)" -> 3,
			"(1+2)*3" -> 9,
			"c_ten" -> 10,
			"c_twotimesten" -> 20,
			"2 * c_ten" -> 20,
			"100 / c_ten" -> 10,
			"99 MOD c_ten" -> 9
		];

		for (item : validSimpleExpressions) {
			print('''Testing «item.key» --> «item.value»...   ''')
			val parsedExpression = parseExpression(item.key);
			Assert.assertTrue(SimpleExpressionUtil.isSimpleExpression(parsedExpression));
			print("isSimpleExpression OK    ")
			Assert.assertEquals(item.value, SimpleExpressionUtil.evaluateSimpleExpression(parsedExpression));
			println("evaluateSimpleExpression OK    ")
		}
		println('testValid() done.');
	}

	@Test
	def void testInvalid() {
		val invalidSimpleExpressions = newArrayList("varint1", "2**5", "2 * varint1"); // "'a'", "'abcdef'"

		for (item : invalidSimpleExpressions) {
			println('''Testing «item» --> <invalid>...''')
			val parsedExpression = parseExpression(item);
			Assert.assertFalse(SimpleExpressionUtil.isSimpleExpression(parsedExpression));
		}
		println('testInvalid() done.');
	}
}
