/******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.formatter.FormatterTestHelper
import org.junit.Test
import org.junit.runner.RunWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.InjectWith
import cern.plcverif.base.common.utils.IoUtils

@RunWith(XtextRunner)
@InjectWith(Step7LanguageInjectorProvider)
class FormatterTest {
	@Inject
	FormatterTestHelper helper;
	
	@Test	
	def smokeTest() {
		helper.assertFormatted[
			toBeFormatted = '''organization_block    ob1   begin  end_organization_block'''
			expectation = 
			'''
			ORGANIZATION_BLOCK ob1
			BEGIN
			END_ORGANIZATION_BLOCK''' 
		];
	}
	
	@Test
	def sclFormatTest1() {
		test('SclFormatterTest1');
	}
	
	private def test(String fileNameBase) {
		val inputText = IoUtils.readResourceFile('''/formattertest/«fileNameBase»-unformatted.scl''', this.class.classLoader);
		val expectedText = IoUtils.readResourceFile('''/formattertest/«fileNameBase»-formatted.scl''', this.class.classLoader);
		
		helper.assertFormatted[
			toBeFormatted = inputText
			expectation = expectedText 
			useNodeModel = false
		];
	}
}