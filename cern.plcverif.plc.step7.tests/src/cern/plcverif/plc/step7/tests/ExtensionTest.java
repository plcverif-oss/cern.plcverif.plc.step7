// package cern.plcverif.plc.step7.tests;
//
// import static org.junit.Assert.assertNotNull;
// import static org.junit.Assert.assertTrue;
//
// import cern.plcverif.base.interfaces.IParserLazyResult;
// import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
// import cern.plcverif.plc.step7.extension.Step7Parser;
// import cern.plcverif.plc.step7.extension.Step7ParserExtension;
//
// import java.util.HashMap;
// import java.util.Map;
//
// import org.junit.Test;
//
// public class ExtensionTest {
// @Test
// public void smokeTest() {
// Step7ParserExtension parserExtension = new Step7ParserExtension();
// Step7Parser parser = parserExtension.createParser();
//
// Map<String, String> files = new HashMap<>();
// files.put("dummy.SCL", "FUNCTION foo : VOID VAR a : BOOL; END_VAR BEGIN
// a:=TRUE; END_FUNCTION");
// parser.setFiles(files);
//
// IParserLazyResult result = parser.parseCode(null);
// CfaNetworkDeclaration cfa = result.generateCfa(null);
//
// assertNotNull(cfa);
// assertTrue(true);
// }
// }
