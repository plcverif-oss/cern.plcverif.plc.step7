/******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 * 	 Jean-Charles Tournier - Nov 2019 - Add test for DirectBitAccessRef transformation
 *****************************************************************************/
package cern.plcverif.plc.step7.tests.transformation

import cern.plcverif.base.common.emf.textual.EmfModelPrinter
import cern.plcverif.base.common.emf.textual.SimpleEObjectToText
import cern.plcverif.base.common.utils.IoUtils
import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.tests.Step7LanguageInjectorProvider
import cern.plcverif.plc.step7.transformation.ExtractNestedCalls
import cern.plcverif.plc.step7.transformation.IStep7AstTransformation
import cern.plcverif.plc.step7.transformation.SpecializeFcsWithAnyNumParam
import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import cern.plcverif.plc.step7.transformation.RetValToOutputParam
import cern.plcverif.plc.step7.transformation.SubstituteNamedConstantRefs
import cern.plcverif.plc.step7.transformation.SubstituteDirectBitAccessRef
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

/**
 * Regression tests for the AST transformations (implementing {@link IStep7AstTransformation}).
 */
@RunWith(XtextRunner)
@InjectWith(Step7LanguageInjectorProvider)
class AstTransformationRegressionTest {
	@Inject
	ParseHelper<ProgramFile> parseHelper
	
	public static final boolean CONTINUE_ON_ASSERT_VIOLATION = false; // NOPMD
	public static boolean writeActualOutputs = CONTINUE_ON_ASSERT_VIOLATION;

	@Test
	def extractNestedCallsTest() {
		val transformation = new ExtractNestedCalls();
		for (i : 1 .. 4) {
			test('''transformation/ExtractNestedCalls«i».scl''',
				transformation, '''transformation/ExtractNestedCalls«i».expected.txt''');
		}
	}

	@Test
	def retValToOutputParamTest() {
		val transformation = new RetValToOutputParam();
		for (i : 1 .. 3) {
			test('''transformation/RetValToOutputParam«i».scl''',
				transformation, '''transformation/RetValToOutputParam«i».expected.txt''');
		}
	}

	@Test
	def specializeFcsAnynumTest() {
		val transformation = new SpecializeFcsWithAnyNumParam();
		for (i : 1 .. 1) {
			test('''transformation/SpecializeFcsWithAnyNumParam«i».scl''',
				transformation, '''transformation/SpecializeFcsWithAnyNumParam«i».expected.txt''');
		}
	}

	@Test
	def substituteNamedConstantRefsTest() {
		val transformation = new SubstituteNamedConstantRefs();
		for (i : 1 .. 1) {
			test('''transformation/SubstituteNamedConstantRefs«i».scl''',
				transformation, '''transformation/SubstituteNamedConstantRefs«i».expected.txt''');
		}
	}
	
	@Test
	def substituteDirectBitAccessRefTest(){
		val transformation = new SubstituteDirectBitAccessRef();
		for (i : 1 .. 1) {
			test('''transformation/SubstituteDirectBitAccessRefs«i».scl''',
				transformation, '''transformation/SubstituteDirectBitAccessRefs«i».expected.txt''');
		}
	}

	private def test(String pathOfOriginalScl, IStep7AstTransformation transformation, String pathOfExpectedTxt) {
		println('''Checking «transformation.class.simpleName» on «pathOfOriginalScl»...''')

		// load PLC program and parse
		val originalScl = IoUtils.readResourceFile(pathOfOriginalScl, this.class.classLoader);
		val ast = parseHelper.parse(originalScl);
		Assert.assertNotNull("Unable to parse " + pathOfOriginalScl, ast);

		// perform AST transformation
		transformation.transform(ast);

		// load expected text and create actual text
		val expectedTxt = IoUtils.readResourceFile(pathOfExpectedTxt, this.class.classLoader);
		val actualTxt = EmfModelPrinter.print(ast, SimpleEObjectToText.INSTANCE);
		
		if (writeActualOutputs) {
			val writeFile = new File(Files.createDirectories(Paths.get("temp_resources","transformation")).toString, String.format("%s" , pathOfExpectedTxt.split("/").get(1)));
			IoUtils.writeAllContent(writeFile, actualTxt.toString);
		}
		
		// compare
		if(!CONTINUE_ON_ASSERT_VIOLATION){
			Assert.assertEquals(expectedTxt.trim, actualTxt.toString().trim);
		}
	}
	
	@Test
	def void sentinel() {
		Assert.assertFalse(CONTINUE_ON_ASSERT_VIOLATION);
	}
}
