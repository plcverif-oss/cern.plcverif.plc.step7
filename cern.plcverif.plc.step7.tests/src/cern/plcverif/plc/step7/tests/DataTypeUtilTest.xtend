/******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.tests

import cern.plcverif.plc.step7.step7Language.ArrayDT
import cern.plcverif.plc.step7.step7Language.ElementaryTypeEnum
import cern.plcverif.plc.step7.step7Language.Step7LanguageFactory
import cern.plcverif.plc.step7.util.DataTypeUtil
import cern.plcverif.plc.step7.util.Step7FactoryHelper
import org.junit.Assert
import org.junit.Test
import java.util.List

class DataTypeUtilTest {
	val static BOOL_TYPE = Step7FactoryHelper.createElementaryDT(ElementaryTypeEnum.BOOL);
	val static FACTORY = Step7LanguageFactory.eINSTANCE;
	 	
	private static def createArrayDT(String... dimStrings) {
		val ArrayDT type = FACTORY.createArrayDT();
		type.baseType = BOOL_TYPE;
		
		for (dimString : dimStrings) {
			val dim = FACTORY.createArrayDimensionString();
			dim.rangeString = dimString;
			type.dimensions.add(dim);
		}
		
		return type;
	}
	
	private static def String toString(List<long[]> list) {
		return '''[«FOR listItem : list SEPARATOR '; '»[«FOR idx : listItem SEPARATOR ','»«idx»«ENDFOR»]«ENDFOR»]''';
	}
	 	
	@Test
	def void enumerateValidArrayIndicesTest1() {
		// One dimension, 1..5
		val ArrayDT type = createArrayDT("1..5");
		
		val actual = DataTypeUtil.enumerateValidArrayIndices(type);
		Assert.assertEquals(5, actual.size); // 5 elements
		actual.forEach[it | Assert.assertEquals(1, it.length)]; // each element with 1 item
		
		Assert.assertEquals("[[1]; [2]; [3]; [4]; [5]]", toString(actual));
	}
	
	@Test
	def void enumerateValidArrayIndicesTest2() {
		// Two dimensions, 1..3, 2..4 (9 element)
		val ArrayDT type = createArrayDT("1..3", "2..4");
		
		val actual = DataTypeUtil.enumerateValidArrayIndices(type);
		Assert.assertEquals(9, actual.size); // 9 elements
		actual.forEach[it | Assert.assertEquals(2, it.length)]; // each element with 2 items
		
		Assert.assertEquals("[[1,2]; [2,2]; [3,2]; [1,3]; [2,3]; [3,3]; [1,4]; [2,4]; [3,4]]", toString(actual));
	}
	
	@Test
	def void enumerateValidArrayIndicesTest3() {
		// Two dimensions, 1..1, 2..4 (3 element)
		val ArrayDT type = createArrayDT("1..1", "2..4");
		
		val actual = DataTypeUtil.enumerateValidArrayIndices(type);
		Assert.assertEquals(3, actual.size); // 3 elements
		actual.forEach[it | Assert.assertEquals(2, it.length)]; // each element with 2 items
		
		Assert.assertEquals("[[1,2]; [1,3]; [1,4]]", toString(actual));
	}
}