/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.util

import org.junit.Test
import org.junit.Assert

class TextualUtilTests {
	@Test
	def removeCommentsTest0() {
		// If there are no comments, no change is expected.
		val input = '''
			a := b;
			c := d * e;
			e := f;
		''';

		Assert.assertEquals(input, TextualUtil.contentWithoutComments(input));
	}

	@Test
	def removeCommentsTest1() {
		val input = '''
			a := b; // line comment
			c := d; (* block comment *)
			e := f; (*
			g := h;
			*)
		''';
		val expected = '''
			a := b; 
			c := d; 
			e := f; 
		''';

		Assert.assertEquals(expected, TextualUtil.contentWithoutComments(input));
	}

	@Test
	def collectExecNamesTest1() {
		val input = '''
			FUNCTION foo : INT
				bar;
			END_FUNCTION
		''';
		val expected = #['foo'];

		Assert.assertEquals(expected, TextualUtil.collectExecutableUnitNames(input));
	}

	@Test
	def collectExecNamesTest2() {
		val input = '''
			FUNCTION_BLOCK foo
				bar(5);
			END_FUNCTION_BLOCK
			 
			FUNCTION bar:INT
				;
			END_FUNCTION
			
			ORGANIZATION_BLOCK ob1
			END_ORGANIZATION_BLOCK
			
			DATA_BLOCK db1 foo
			BEGIN
			END_DATA_BLOCK
		''';
		val expected = #['foo', 'bar', 'ob1'];

		Assert.assertEquals(expected, TextualUtil.collectExecutableUnitNames(input));
	}

	@Test
	def collectExecNamesTest3() {
		val input = '''
			FUNCTION_BLOCK (*bof*) foo
				bar(5);
			END_FUNCTION_BLOCK
			
			(*
			FUNCTION bar:INT
				;
			END_FUNCTION
			*)
			
			ORGANIZATION_BLOCK // ob99
				 ob1
			END_ORGANIZATION_BLOCK
			
			DATA_BLOCK db1 foo
			BEGIN
			END_DATA_BLOCK
		''';
		val expected = #['foo', 'ob1'];

		Assert.assertEquals(expected, TextualUtil.collectExecutableUnitNames(input));
	}

	@Test
	def collectExecNamesTest4() {
		val input = '''
			FUNCTION "foo" : INT
				bar;
			END_FUNCTION
		''';
		val expected = #['foo'];

		Assert.assertEquals(expected, TextualUtil.collectExecutableUnitNames(input));
	}

	@Test
	def collectAssertTagsTest1() {
		val input = '''
			FUNCTION "foo" : INT
				bar;
				//#ASSERT a = b
			END_FUNCTION
		''';
		val expected = #[];

		Assert.assertEquals(expected, TextualUtil.collectAssertionTags(input));
	}

	@Test
	def collectAssertTagsTest2() {
		val input = '''
			FUNCTION "foo" : INT
				bar;
				//#ASSERT a = b : tag1
			END_FUNCTION
		''';
		val expected = #['tag1'];

		Assert.assertEquals(expected, TextualUtil.collectAssertionTags(input));
	}

	@Test
	def collectAssertTagsTest3() {
		val input = '''
			FUNCTION "foo" : INT
				bar;
				//#ASSERT a = b : tag1
				//#ASSERT a = b : tag1
			END_FUNCTION
		''';
		val expected = #['tag1', 'tag1'];

		Assert.assertEquals(expected, TextualUtil.collectAssertionTags(input));
	}

	@Test
	def collectAssertTagsTest4() {
		val input = '''
			FUNCTION "foo" : INT
				bar;
				//#ASSERT a = b : tag1
				//#ASSERT b = c
				//#ASSERT a = b : tag2;
			END_FUNCTION
		''';
		val expected = #['tag1', 'tag2'];

		Assert.assertEquals(expected, TextualUtil.collectAssertionTags(input));
	}
	
	@Test
	def collectAssertTagsTest5() {
		val input = '''
		//#ASSERT a = b OR c : tag1 // Some comment more
		//#ASSERT a = b OR c : tag2 // Some comment more
		''';
		val expected = #['tag1', 'tag2'];

		Assert.assertEquals(expected, TextualUtil.collectAssertionTags(input));
	}
	
	@Test
	def collectAssertTagsTest6() {
		val input = '''
			FUNCTION "foo" : INT
				NETWORK
					//#ASSERT a = b;
			END_FUNCTION
			FUNCTION bar : VOID
			END_FUNCTION
		''';
		val expected = #[];

		Assert.assertEquals(expected, TextualUtil.collectAssertionTags(input));
	}
	
	@Test
	def collectAssertTagsTest7() {
		val input = '''
			FUNCTION "foo" : INT
				bar;
				//#ASSERT a = b
			END_FUNCTION
			FUNCTION bar : VOID
			END_FUNCTION
		''';
		val expected = #[];

		Assert.assertEquals(expected, TextualUtil.collectAssertionTags(input));
	}
}
