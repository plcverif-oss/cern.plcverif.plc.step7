/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class S7TimeOfDay extends AbstractS7DataType {
	private static final Logger log = LogManager.getLogger(AbstractS7DataType.class);
	private static final Pattern PATTERN_TOD = Pattern.compile(
			"^(?:(TOD|TIME_OF_DAY)#)([\\d_]+):([\\d_]+):([\\d_]+)(?:\\.([\\d_]+))?$", Pattern.CASE_INSENSITIVE);

	private int hours;
	private int minutes;
	private int seconds;
	private int milliseconds;

	private S7TimeOfDay(String stringRepresentation) {
		super(stringRepresentation, false);
	}

	private S7TimeOfDay(String stringRepresentation, int hours, int minutes, int seconds, int milliseconds) {
		super(stringRepresentation, true);
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
		this.milliseconds = milliseconds;
	}

	public static boolean isValid(String str) {
		return PATTERN_TOD.matcher(str).find();
	}

	public static S7TimeOfDay create(String str) {
		if (isValid(str)) {
			Matcher matcher = PATTERN_TOD.matcher(str);
			if (matcher.find()) {
				try {
					String hoursStr = matcher.group(2);
					String minutesStr = matcher.group(3);
					String secondsStr = matcher.group(4);
					String millisecondsStr = matcher.group(5);

					int hours = Integer.parseInt(hoursStr.replaceAll("_", ""));
					int minutes = Integer.parseInt(minutesStr.replaceAll("_", ""));
					int seconds = Integer.parseInt(secondsStr.replaceAll("_", ""));
					int milliseconds = millisecondsStr == null ? 0
							: Integer.parseInt(millisecondsStr.replaceAll("_", ""));

					return new S7TimeOfDay(str, hours, minutes, seconds, milliseconds);
				} catch (NumberFormatException ex) {
					log.info(String.format("Unable to parse as TOD#: '%s'. (invalid number format)", str), ex);
					return new S7TimeOfDay(str);
				}
			}
		}

		// parsing is not possible
		log.info("Unable to parse as TOD#: '{}'. (invalid pattern)", str);
		return new S7TimeOfDay(str);
	}

	public int getHours() {
		return hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public int getSeconds() {
		return seconds;
	}

	public int getMilliseconds() {
		return milliseconds;
	}

	public long getTotalMilliseconds() {
		return milliseconds + seconds * 1000L + minutes * 1000L * 60L + hours * 1000L * 60L * 60L;
	}

	@Override
	public int getSizeInBits() {
		return 32;
	}

	@Override
	public boolean isWithinRange() {
		return isWithin(hours, 0, 23) && isWithin(minutes, 0, 59) && isWithin(seconds, 0, 59)
				&& isWithin(milliseconds, 0, 999);
	}
}
