/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

public enum S7IntegerType {
	// @formatter:off
	BOOL(0, 1),
	INT(-32_768, 32_767),
	DINT(-2_147_483_648, 2_147_483_647),
	UINT(0, 65_535),
	UDINT(0, 4_294_967_295L),
	BYTE(0,255),
	WORD(0, 65_535),
	DWORD(0, 4_294_967_295L),
	// non-standard types
	SINT(-128, 127),
	USINT(0, 255),
	LINT(-9_223_372_036_854_775_808L, 9_223_372_036_854_775_807L),
	ULINT(0, 9_223_372_036_854_775_807L), // actually LINT supports bigger numbers, but Java doesn't.
	Unknown(0, -1);
	// @formatter:on

	private long lowerBound;
	private long upperBound;

	private S7IntegerType(long lowerBound, long upperBound) {
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}

	public long getLowerBound() {
		return lowerBound;
	}

	public long getUpperBound() {
		return upperBound;
	}

	public boolean isWithinRange(long number) {
		return number >= lowerBound && number <= upperBound;
	}

	public boolean isSigned() {
		return lowerBound < 0;
	}
}
