/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class S7Timer extends AbstractS7DataType {
	private static final Logger log = LogManager.getLogger(AbstractS7DataType.class);
	private static final Pattern PATTERN_TIMER = Pattern.compile("^T(\\d+)$", Pattern.CASE_INSENSITIVE);

	private int timerId;

	private S7Timer(String stringRepresentation) {
		super(stringRepresentation, false);
	}

	private S7Timer(String stringRepresentation, int timerId) {
		super(stringRepresentation, true);
		this.timerId = timerId;
	}

	public static boolean isValid(String str) {
		return PATTERN_TIMER.matcher(str).find();
	}

	public static S7Timer create(String str) {
		if (isValid(str)) {
			Matcher matcher = PATTERN_TIMER.matcher(str.toUpperCase());
			if (matcher.find()) {
				String valueStr = matcher.group(1);
				int value = Integer.parseInt(valueStr);
				return new S7Timer(str, value);
			}
		}

		// parsing is not possible
		log.info("Unable to parse as timer: '{}'.", str);
		return new S7Timer(str);
	}

	@Override
	public int getSizeInBits() {
		return 0;
	}

	public int getTimerId() {
		return timerId;
	}

	@Override
	public boolean isWithinRange() {
		return true;
	}
}
