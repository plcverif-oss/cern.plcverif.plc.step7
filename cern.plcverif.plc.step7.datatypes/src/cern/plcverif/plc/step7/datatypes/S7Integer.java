/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import static cern.plcverif.plc.step7.datatypes.S7DataTypeUtil.stringToLong;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Representation for INT, DINT, UINT, UDINT S7 data types.
 */
public final class S7Integer extends AbstractS7DataType implements IS7IntegerDataType {
	private static final Logger log = LogManager.getLogger(AbstractS7DataType.class);
	private static final Pattern PATTERN_INT = Pattern.compile(
			"^(?:(U?D?INT|D?WORD|BYTE|BOOL|D?W|B|L)#)?(\\-)?(?:(2|8|10|16)#)?([+-]?[a-f\\d_]+)$",
			Pattern.CASE_INSENSITIVE);

	private S7IntegerType type = S7IntegerType.Unknown;
	private S7IntegerBase base = S7IntegerBase.Decimal;
	private long value;

	private S7Integer(String stringRepresentation) {
		super(stringRepresentation, false);
	}

	private S7Integer(String stringRepresentation, S7IntegerType type, S7IntegerBase base, long value) {
		super(stringRepresentation, true);

		this.type = type;
		this.base = base;
		this.value = value;
	}

	public static boolean isValid(String str) {
		return PATTERN_INT.matcher(str).find();
	}

	public static S7Integer create(String str) {
		if (isValid(str)) {
			Matcher matcher = PATTERN_INT.matcher(str.toUpperCase());
			if (matcher.find()) {
				try {
					String typeStr = matcher.group(1);
					String signStr = matcher.group(2);
					String baseStr = matcher.group(3);
					String valueStr = matcher.group(4);

					// resolve abbreviations
					if (typeStr != null) {
						switch (typeStr.toUpperCase()) {
						case "B":
							typeStr = "BYTE";
							break;
						case "W":
							typeStr = "WORD";
							break;
						case "L":
							typeStr = "DINT";
							break;
						case "DW":
							typeStr = "DWORD";
							break;
						default:
							break; // don't have to change if not abbreviation
						}
					}

					S7IntegerType type = typeStr == null ? S7IntegerType.Unknown
							: S7IntegerType.valueOf(typeStr.toUpperCase());
					S7IntegerBase base = baseStr == null ? S7IntegerBase.Decimal : S7IntegerBase.fromString(baseStr);
					long value = stringToLong(valueStr.replaceAll("_", ""), base.getBase());
					if ("-".equals(signStr)) {
						value = -value;
					}

					return new S7Integer(str, type, base, value);
				} catch (NumberFormatException ex) {
					return new S7Integer(str);
				}
			}
		}

		// parsing is not possible
		log.info("Unable to parse as INT#: '{}'.", str);
		return new S7Integer(str);
	}

	public S7Integer copy() {
		return new S7Integer(this.getOriginalStringRepresentation(), this.type, this.base, this.value);
	}

	public S7Integer copyAndNegate() {
		return new S7Integer("-" + this.getOriginalStringRepresentation(), this.type, this.base, -1 * this.value);
	}

	@Override
	public boolean isSigned() {
		switch (type) {
		case INT:
		case DINT:
			return true;
		case UINT:
		case UDINT:
		case BYTE:
		case WORD:
		case DWORD:
			return false;
		default:
			throw new IllegalStateException("Unsupported case at S7Integer.isSigned: " + type);
		}
	}

	@Override
	public double doubleValue() {
		return value;
	}

	@Override
	public int getSizeInBits() {
		return getSizeInBits(type);
	}

	public static int getSizeInBits(S7IntegerType type) {
		switch (type) {
		case BYTE:
		case SINT:
		case USINT:
			return 8;
		case INT:
		case UINT:
		case WORD:
			return 16;
		case DINT:
		case UDINT:
		case DWORD:
			return 32;
		case LINT:
		case ULINT:
			return 64;
		default:
			throw new IllegalStateException("Unsupported case at S7Integer.getSizeInBits: " + type);
		}
	}

	@Override
	public long intValue() {
		return value;
	}

	@Override
	public S7IntegerBase getBase() {
		return base;
	}

	public S7IntegerType getType() {
		return type;
	}

	/**
	 * Returns the collection of all potential types of the integer constant.
	 * E.g.
	 * <li>INT#123 --> Int</li>
	 * <li>123 --> Int, Dint, Uint, ...</li>
	 * <li>0 --> Bool, Int, ...</li>
	 */
	public Collection<S7IntegerType> getPotentialTypes() {
		if (this.type == S7IntegerType.Unknown) {
			return Arrays.stream(S7IntegerType.values()).filter(t -> t.isWithinRange(intValue()))
					.collect(Collectors.toList());
		} else {
			return Collections.singletonList(type);
		}
	}

	public S7IntegerType getSmallestPotentialSignedType() {
		// Heuristics to find out the type of a constant if everything else
		// fails
		if (this.type == S7IntegerType.Unknown) {
			return getPotentialTypes().stream().filter(it -> it.isSigned())
					.sorted((it1, it2) -> Long.compare(it1.getUpperBound(), it2.getUpperBound())).findFirst()
					.orElse(null);
		} else {
			return type;
		}
	}

	@Override
	public boolean isWithinRange() {
		return isWithinRange(type);
	}

	public boolean isWithinRange(S7IntegerType asType) {
		if (asType == S7IntegerType.Unknown) {
			return false;
		}

		return asType.isWithinRange(value);
	}
}
