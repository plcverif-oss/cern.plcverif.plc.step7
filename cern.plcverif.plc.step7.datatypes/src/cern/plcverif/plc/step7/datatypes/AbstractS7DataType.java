/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

public abstract class AbstractS7DataType implements IS7DataType {
	private final boolean valid;
	private final String stringRepresentation;

	@Override
	public boolean isSyntacticallyValid() {
		return valid;
	}

	protected AbstractS7DataType(String stringReprentation, boolean valid) {
		this.stringRepresentation = stringReprentation;
		this.valid = valid;
	}

	public String getOriginalStringRepresentation() {
		return stringRepresentation;
	}

	@Override
	public String toString() {
		return stringRepresentation;
	}

	protected boolean isWithin(int value, int lowerBound, int upperBound) {
		return (value >= lowerBound && value <= upperBound);
	}
}
