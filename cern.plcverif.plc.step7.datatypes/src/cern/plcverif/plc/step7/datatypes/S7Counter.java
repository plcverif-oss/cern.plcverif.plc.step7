/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class S7Counter extends AbstractS7DataType {
	private static final Logger log = LogManager.getLogger(AbstractS7DataType.class);
	private static final Pattern PATTERN_COUNTER = Pattern.compile("^C(\\d+)$", Pattern.CASE_INSENSITIVE);

	private int counterId;

	private S7Counter(String stringRepresentation) {
		super(stringRepresentation, false);
	}

	private S7Counter(String stringRepresentation, int counterId) {
		super(stringRepresentation, true);
		this.counterId = counterId;
	}

	public static boolean isValid(String str) {
		return PATTERN_COUNTER.matcher(str).find();
	}

	public static S7Counter create(String str) {
		if (isValid(str)) {
			Matcher matcher = PATTERN_COUNTER.matcher(str.toUpperCase());
			if (matcher.find()) {
				String valueStr = matcher.group(1);
				int value = Integer.parseInt(valueStr);
				return new S7Counter(str, value);
			}
		}

		// parsing is not possible
		log.info("Unable to parse as counter: '{}'.", str);
		return new S7Counter(str);
	}

	@Override
	public int getSizeInBits() {
		return 0;
	}

	public int getCounterId() {
		return counterId;
	}

	@Override
	public boolean isWithinRange() {
		return true;
	}
}
