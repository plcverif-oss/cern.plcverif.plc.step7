/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import static cern.plcverif.plc.step7.datatypes.S7DataTypeUtil.stringToDouble;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class S7TimePeriod extends AbstractS7DataType {
	private static final Logger log = LogManager.getLogger(AbstractS7DataType.class);
	private static final Pattern PATTERN_TIMEPERIOD = Pattern.compile(
			//@formatter:off
			"^(?:T|TIME|S5T|S5TIME)#" + 
				// T# or TIME# prefix is obligatory (or S5TIME in STL)
			"(?:(\\d+(?:\\.\\d+)?)D)?_?" +
			"(?:(\\d+(?:\\.\\d+)?)H)?_?" +
			"(?:(\\d+(?:\\.\\d+)?)M)?_?" +
			"(?:(\\d+(?:\\.\\d+)?)S)?_?" +
			"(?:(\\d+(?:\\.\\d+)?)MS)?_?$",
			//@formatter:on
			Pattern.CASE_INSENSITIVE);
	// The format of S5TIME constants in STL is S5T#value or S5TIME#value, cf. [SCL] p. 9-5

	private double days;
	private double hours;
	private double minutes;
	private double seconds;
	private double milliseconds;

	private S7TimePeriod(String stringRepresentation, double days, double hours, double minutes, double seconds,
			double milliseconds) {
		super(stringRepresentation, true);

		this.days = days;
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
		this.milliseconds = milliseconds;
	}

	private S7TimePeriod(String stringRepresentation) {
		super(stringRepresentation, false);
	}

	public static boolean isValid(String str) {
		return !str.toUpperCase().matches("#$") && PATTERN_TIMEPERIOD.matcher(str).find();
	}

	public static S7TimePeriod create(String str) {
		if (isValid(str)) {
			Matcher matcher = PATTERN_TIMEPERIOD.matcher(str.toUpperCase());
			if (matcher.find()) {
				double days = stringToDouble(matcher.group(1));
				double hours = stringToDouble(matcher.group(2));
				double minutes = stringToDouble(matcher.group(3));
				double seconds = stringToDouble(matcher.group(4));
				double milliseconds = stringToDouble(matcher.group(5));

				return new S7TimePeriod(str, days, hours, minutes, seconds, milliseconds);
			}
		}

		// parsing is not possible
		log.info("Unable to parse as TIME#: '{}'.", str);
		return new S7TimePeriod(str);
	}

	@Override
	public int getSizeInBits() {
		return 32;
	}

	public double getDays() {
		return days;
	}

	public double getHours() {
		return hours;
	}

	public double getMinutes() {
		return minutes;
	}

	public double getSeconds() {
		return seconds;
	}

	public double getMilliseconds() {
		return milliseconds;
	}

	public long getTotalMilliseconds() {
		return (long) (((((days * 24 + hours) * 60 + minutes) * 60 + seconds) * 1000) + milliseconds);
	}

	@Override
	public boolean isWithinRange() {
		// TIA support S5TIME, LTIME
		return isWithinRange(S7TimePeriodType.TIME);
	}

	public boolean isWithinRange(S7TimePeriodType asType) {
		return asType.isWithinRange(this);
	}
}
