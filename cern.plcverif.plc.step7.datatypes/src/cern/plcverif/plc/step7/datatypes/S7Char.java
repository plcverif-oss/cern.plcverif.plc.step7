/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cern.plcverif.plc.step7.datatypes.exceptions.SilentValueParseException;

public final class S7Char extends AbstractS7DataType {
	private static final Logger log = LogManager.getLogger(AbstractS7DataType.class);
	private static final Pattern PATTERN_CHAR = Pattern
			.compile("^(?:CHAR#)?('(.)'|'\\$(.)'|'\\$([0-9A-F][0-9A-F])'|([\\d_]+))$", Pattern.CASE_INSENSITIVE);

	private char value;

	private S7Char(String stringRepresentation) {
		super(stringRepresentation, false);
	}

	private S7Char(String stringRepresentation, char value) {
		super(stringRepresentation, true);
		this.value = value;
	}

	public static boolean isValid(String str) {
		return PATTERN_CHAR.matcher(str).find();
	}

	public static S7Char create(String str) {
		if (isValid(str)) {

			Matcher matcher = PATTERN_CHAR.matcher(str);
			if (matcher.find()) {
				try {
					char value = '?';
					if (matcher.group(2) != null) {
						// formal 'x'
						assert matcher.group(2).length() == 1;
						value = matcher.group(2).charAt(0);
					} else if (matcher.group(3) != null) {
						// format '$x'
						assert matcher.group(3).length() == 1;
						value = specialCharacter(matcher.group(3).charAt(0));
					} else if (matcher.group(4) != null) {
						// format '$yy' where y is a hexa digit
						assert matcher.group(4).length() == 2;
						value = Character.toChars(Integer.parseInt(matcher.group(4), 16))[0];
					} else if (matcher.group(5) != null) {
						// format 'zzz' where zzz is a decimal digit
						assert matcher.group(5).length() >= 1;
						value = Character.toChars(Integer.parseInt(matcher.group(5), 10))[0];
					} else {
						throw new SilentValueParseException("Unknown case.");
					}

					return new S7Char(str, value);
				} catch (SilentValueParseException e) {
					log.info("Unable to parse as CHAR#: '{}'. {}", str, e.getMessage());
					return new S7Char(str);
				}
			}
		}

		// parsing is not possible
		log.info("Unable to parse as CHAR#: '{}'.", str);
		return new S7Char(str);
	}

	private static char specialCharacter(char specialChar) throws SilentValueParseException {
		char lowerSpecialChar = Character.toLowerCase(specialChar);
		switch (lowerSpecialChar) {
		// case 'p': return '\p';
		// case 'l': return '\l';
		case 'r':
			return '\r';
		case 't':
			return '\t';
		case 'n':
			return '\n';
		case '\'':
			return '\'';
		case '$':
			return '$';
		default:
			throw new SilentValueParseException("Unknown special character.");
		}
	}

	@Override
	public int getSizeInBits() {
		return 8;
	}

	public char charValue() {
		return value;
	}

	public long integerValue() {
		return value;
	}

	@Override
	public boolean isWithinRange() {
		return true;
	}
}
