/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

public enum S7AddressMemorySize {
	Boolean("X", 1), Byte("B", 8), Word("W", 16), Dword("D", 32);

	private String identifier;
	private int sizeInBits;

	S7AddressMemorySize(String identifier, int sizeInBits) {
		this.identifier = identifier;
		this.sizeInBits = sizeInBits;
	}

	public String getIdentifierString() {
		return identifier;
	}

	public int getSizeInBits() {
		return sizeInBits;
	}

	public static S7AddressMemorySize fromString(String str) {
		for (S7AddressMemorySize item : S7AddressMemorySize.values()) {
			if (item.identifier.equalsIgnoreCase(str)) {
				return item;
			}
		}
		throw new IllegalArgumentException(String.format("Unknown S7AddressMemorySize constant: '%s'", str));
	}
}
