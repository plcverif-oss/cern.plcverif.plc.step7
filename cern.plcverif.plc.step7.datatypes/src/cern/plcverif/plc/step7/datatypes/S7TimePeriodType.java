/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.step7.datatypes;

public enum S7TimePeriodType {
	S5TIME(10, 9_990_000L), // 0H_0M_0S_10MS..2H_46M_30S_0MS
	TIME(0, 2_147_483_647L), // -24D_20H_31M_23S_647MS..24D_20H_31M_23S_647MS
	LTIME(0, 0);

	private long lowerMs;
	private long upperMs;

	private S7TimePeriodType(long lowerMs, long upperMs) {
		this.lowerMs = lowerMs;
		this.upperMs = upperMs;
	}

	public boolean isWithinRange(long valueInMs) {
		return (valueInMs >= lowerMs && valueInMs <= upperMs);
	}

	public boolean isWithinRange(S7TimePeriod value) {
		return (value.getTotalMilliseconds() > lowerMs - 1 && value.getTotalMilliseconds() < upperMs + 1);
	}
}
