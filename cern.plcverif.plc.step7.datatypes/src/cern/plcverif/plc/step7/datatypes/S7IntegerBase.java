/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

public enum S7IntegerBase {
	Decimal(10), Binary(2), Octal(8), Hexadecimal(16);

	private int base;

	private S7IntegerBase(int base) {
		this.base = base;
	}

	public static S7IntegerBase fromString(String str) {
		for (S7IntegerBase item : S7IntegerBase.values()) {
			if (Integer.toString(item.base).equalsIgnoreCase(str)) {
				return item;
			}
		}
		throw new IllegalArgumentException(String.format("Unknown S7IntegerBase constant: '%s'.", str));
	}

	public int getBase() {
		return base;
	}
}
