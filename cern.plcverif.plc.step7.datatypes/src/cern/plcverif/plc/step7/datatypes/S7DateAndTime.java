/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class S7DateAndTime extends AbstractS7DataType {
	private static final Logger log = LogManager.getLogger(AbstractS7DataType.class);
	private static final Pattern PATTERN_DT = Pattern.compile(
			// @formatter:off
			"^(?:(DT|DATE_AND_TIME)#)" + // prefix
			"([\\d_]+)-([\\d_]+)-([\\d_]+)" + // date
			"-" +
			"([\\d_]+):([\\d_]+):([\\d_]+)(?:\\.([\\d_]+))?$" // time of day
			, Pattern.CASE_INSENSITIVE);
			// @formatter:on

	private int year;
	private int month;
	private int day;
	private int hours;
	private int minutes;
	private int seconds;
	private int milliseconds;

	private S7DateAndTime(String stringRepresentation) {
		super(stringRepresentation, false);
	}

	private S7DateAndTime(String stringRepresentation, int year, int month, int date, int hours, int minutes,
			int seconds, int milliseconds) {
		super(stringRepresentation, true);
		this.year = year;
		this.month = month;
		this.day = date;
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
		this.milliseconds = milliseconds;
	}

	public static boolean isValid(String str) {
		return PATTERN_DT.matcher(str).find();
	}

	public static S7DateAndTime create(String str) {
		if (isValid(str)) {
			Matcher matcher = PATTERN_DT.matcher(str);
			if (matcher.find()) {
				try {
					String yearStr = matcher.group(2).replaceAll("_", "");
					String monthStr = matcher.group(3).replaceAll("_", "");
					String dayStr = matcher.group(4).replaceAll("_", "");
					String hoursStr = matcher.group(5);
					String minutesStr = matcher.group(6);
					String secondsStr = matcher.group(7);
					String millisecondsStr = matcher.group(8);

					int year = Integer.parseInt(yearStr.replaceAll("_", ""));
					int month = Integer.parseInt(monthStr.replaceAll("_", ""));
					int day = Integer.parseInt(dayStr.replaceAll("_", ""));
					int hours = Integer.parseInt(hoursStr.replaceAll("_", ""));
					int minutes = Integer.parseInt(minutesStr.replaceAll("_", ""));
					int seconds = Integer.parseInt(secondsStr.replaceAll("_", ""));
					int milliseconds = millisecondsStr == null ? 0
							: Integer.parseInt(millisecondsStr.replaceAll("_", ""));

					return new S7DateAndTime(str, year, month, day, hours, minutes, seconds, milliseconds);
				} catch (NumberFormatException ex) {
					log.info(String.format("Unable to parse as DT#: '%s'. (invalid number format)", str), ex);
					return new S7DateAndTime(str);
				}
			}
		}

		// parsing is not possible
		log.info("Unable to parse as DT#: '{}'. (invalid pattern)", str);
		return new S7DateAndTime(str);
	}

	public int getYear() {
		return year;
	}

	public int getMonth() {
		return month;
	}

	public int getDay() {
		return day;
	}

	public int getHours() {
		return hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public int getSeconds() {
		return seconds;
	}

	public int getMilliseconds() {
		return milliseconds;
	}

	@Override
	public int getSizeInBits() {
		return 64;
	}

	@Override
	public boolean isWithinRange() {
		// TIA handle DLT and LDT
		// @formatter:off
		return isWithin(year, 1990, 2089) &&
				isWithin(month, 1, 12) &&
				isWithin(day, 1, 31) &&
				isWithin(hours, 0, 23) &&
				isWithin(minutes, 0, 59) &&
				isWithin(seconds, 0, 59) &&
				isWithin(milliseconds, 0, 999);
		// @formatter:on
	}
}
