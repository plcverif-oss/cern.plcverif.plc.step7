/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class S7Date extends AbstractS7DataType {
	private static final Logger log = LogManager.getLogger(AbstractS7DataType.class);
	private static final Pattern PATTERN_DATE = Pattern.compile("^(?:(D|DATE)#)?([\\d_]+)-([\\d_]+)-([\\d_]+)$",
			Pattern.CASE_INSENSITIVE);

	private int year;
	private int month;
	private int day;

	private S7Date(String stringRepresentation) {
		super(stringRepresentation, false);
	}

	private S7Date(String stringRepresentation, int year, int month, int day) {
		super(stringRepresentation, true);
		this.year = year;
		this.month = month;
		this.day = day;
	}

	public static boolean isValid(String str) {
		return PATTERN_DATE.matcher(str).find();
	}

	public static S7Date create(String str) {
		if (isValid(str)) {
			Matcher matcher = PATTERN_DATE.matcher(str.toUpperCase());
			if (matcher.find()) {
				try {
					// group(1) is the type (D or DATE)
					String yearStr = matcher.group(2).replaceAll("_", "");
					String monthStr = matcher.group(3).replaceAll("_", "");
					String dayStr = matcher.group(4).replaceAll("_", "");

					return new S7Date(str, Integer.parseInt(yearStr), Integer.parseInt(monthStr),
							Integer.parseInt(dayStr));
				} catch (NumberFormatException ex) {
					log.info("Unable to parse as DATE#: '{}'. Invalid number format detected.", str);
					return new S7Date(str);
				}
			}
		}

		// parsing is not possible
		log.info("Unable to parse as DATE#: '{}'.", str);
		return new S7Date(str);
	}

	public int getYear() {
		return year;
	}

	public int getMonth() {
		return month;
	}

	public int getDate() {
		return day;
	}

	@Override
	public int getSizeInBits() {
		return 16;
	}

	@Override
	public boolean isWithinRange() {
		return isWithin(year, 1990, 2168) && isWithin(month, 1, 12) && isWithin(day, 1, 12);
	}
}
