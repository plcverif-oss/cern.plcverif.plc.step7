/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cern.plcverif.plc.step7.datatypes.exceptions.SilentValueParseException;

public final class S7MemoryAddress extends AbstractS7DataType {
	private static final Logger log = LogManager.getLogger(AbstractS7DataType.class);
	private static final Pattern PATTERN_MEMORYADDRESS = Pattern
			.compile("^%?(I|Q|M|PI|PQ|L)(X|B|W|D)?\\s*(\\d+)(?:\\.([0-7]))?$", Pattern.CASE_INSENSITIVE);

	private S7AddressMemoryIdentifier memoryIdentifier;
	private S7AddressMemorySize memorySize;
	private int byteNumber;
	private int bitNumber;

	private S7MemoryAddress(String stringRepresentation) {
		super(stringRepresentation, false);
	}

	private S7MemoryAddress(String stringRepresentation, S7AddressMemoryIdentifier memoryIdentifier,
			S7AddressMemorySize memorySize, int byteNumber, int bitNumber) {
		super(stringRepresentation, true);
		this.memoryIdentifier = memoryIdentifier;
		this.memorySize = memorySize;
		this.byteNumber = byteNumber;
		this.bitNumber = bitNumber;
	}

	public static boolean isValid(String str) {
		return PATTERN_MEMORYADDRESS.matcher(str).find();
	}

	public static S7MemoryAddress create(String str) {
		try {
			if (isValid(str)) {
				Matcher matcher = PATTERN_MEMORYADDRESS.matcher(str.toUpperCase());
				if (matcher.find()) {
					String memoryIdentifierStr = matcher.group(1);
					String memorySizeStr = matcher.group(2);
					String byteNumberStr = matcher.group(3); // NOPMD
					String bitNumberStr = matcher.group(4); // NOPMD

					S7AddressMemoryIdentifier memoryIdentifier = S7AddressMemoryIdentifier
							.valueOf(memoryIdentifierStr.toUpperCase());
					S7AddressMemorySize memorySize = memorySizeStr == null ? S7AddressMemorySize.Boolean
							: S7AddressMemorySize.fromString(memorySizeStr);

					if (memorySize == S7AddressMemorySize.Boolean) {
						if (bitNumberStr == null) {
							throw new SilentValueParseException("Bit number is mandatory for Boolean addresses.");
						}
					} else {
						if (bitNumberStr != null) {
							throw new SilentValueParseException("Bit number is forbidden for non-Boolean addresses.");
						}
					}

					int byteNumber = S7DataTypeUtil.stringToInt(byteNumberStr);
					int bitNumber = S7DataTypeUtil.stringToInt(bitNumberStr);

					return new S7MemoryAddress(str, memoryIdentifier, memorySize, byteNumber, bitNumber);
				}
			}

			// parsing is not possible
			log.info("Unable to parse as memory address: '{}'.", str);
			return new S7MemoryAddress(str);
		} catch (SilentValueParseException ex) {
			// parsing is not possible
			log.info("Unable to parse as memory address: '{}'. {}", str, ex.getMessage());
			return new S7MemoryAddress(str);
		}

	}

	public S7AddressMemoryIdentifier getMemoryIdentifier() {
		return memoryIdentifier;
	}

	public S7AddressMemorySize getMemorySize() {
		return memorySize;
	}

	public int getByteNumber() {
		return byteNumber;
	}

	public int getBitNumber() {
		return bitNumber;
	}

	@Override
	public int getSizeInBits() {
		return this.getMemorySize().getSizeInBits();
	}

	@Override
	public boolean isWithinRange() {
		return true;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof S7MemoryAddress) {
			S7MemoryAddress other = (S7MemoryAddress) obj;
			return (other.bitNumber == this.bitNumber && other.byteNumber == this.byteNumber
					&& other.memoryIdentifier == this.memoryIdentifier && other.memorySize == this.memorySize);
		}
		return false;
	}

	@SuppressWarnings("boxing")
	@Override
	public int hashCode() {
		return Objects.hash(memoryIdentifier, memorySize, byteNumber, bitNumber);
	}

	@SuppressWarnings("boxing")
	@Override
	public String toString() {
		if (memorySize == S7AddressMemorySize.Boolean) {
			return String.format("%s%s%s.%s", memoryIdentifier, memorySize.getIdentifierString(), byteNumber,
					bitNumber);
		} else {
			return String.format("%s%s%s", memoryIdentifier,
					memorySize == null ? "?" : memorySize.getIdentifierString(), byteNumber);
		}
	}
}
