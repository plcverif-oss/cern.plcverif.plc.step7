/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class S7Bool extends AbstractS7DataType {
	private static final Logger log = LogManager.getLogger(AbstractS7DataType.class);
	private static final Pattern PATTERN_BOOL = Pattern.compile("^(?:BOOL#)?(TRUE|FALSE)$", Pattern.CASE_INSENSITIVE);

	private boolean value;

	private S7Bool(String stringRepresentation) {
		super(stringRepresentation, false);
	}

	private S7Bool(String stringRepresentation, boolean value) {
		super(stringRepresentation, true);
		this.value = value;
	}

	public static boolean isValid(String str) {
		return PATTERN_BOOL.matcher(str).find();
	}

	public static S7Bool create(String str) {
		if (isValid(str)) {
			Matcher matcher = PATTERN_BOOL.matcher(str.toUpperCase());
			if (matcher.find()) {
				String valueStr = matcher.group(1);

				boolean value = false;
				if ("true".equalsIgnoreCase(valueStr)) {
					value = true;
				}

				return new S7Bool(str, value);
			}
		}

		// parsing is not possible

		log.info("Unable to parse as BOOL#: '{}'.", str);
		return new S7Bool(str);
	}

	@Override
	public int getSizeInBits() {
		return 1;
	}

	public boolean boolValue() {
		return value;
	}

	@Override
	public boolean isWithinRange() {
		return true;
	}
}
