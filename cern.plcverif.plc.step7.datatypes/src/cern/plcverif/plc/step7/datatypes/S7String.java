/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class S7String extends AbstractS7DataType {
	private static final Logger log = LogManager.getLogger(AbstractS7DataType.class);
	private static final Pattern PATTERN_STRING = Pattern.compile("^'(.*)'$", Pattern.CASE_INSENSITIVE);

	private String value;

	private S7String(String stringRepresentation) {
		super(stringRepresentation, false);
	}

	private S7String(String stringRepresentation, String value) {
		super(stringRepresentation, true);
		this.value = value;
	}

	public static boolean isValid(String str) {
		return PATTERN_STRING.matcher(str).find();
	}

	public static S7String create(String str) {
		if (isValid(str)) {
			Matcher matcher = PATTERN_STRING.matcher(str);
			if (matcher.find()) {
				String value = matcher.group(1);
				// simplified handling
				// TODO_LOWPRI handle special characters of string constants

				return new S7String(str, value);
			}
		}

		// parsing is not possible
		log.info("Unable to parse as STRING: '{}'.", str);
		return new S7String(str);
	}

	@Override
	public int getSizeInBits() {
		return -1;
	}

	public String stringValue() {
		return this.value;
	}

	@Override
	public boolean isWithinRange() {
		return true;
	}
}
