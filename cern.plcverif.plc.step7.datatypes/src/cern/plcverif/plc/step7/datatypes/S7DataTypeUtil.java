/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

// Non-public on purpose.
final class S7DataTypeUtil {
	private S7DataTypeUtil() {
		// Utility class.
	}

	public static double stringToDouble(String str) {
		if (str == null) {
			return 0;
		}
		return Double.parseDouble(str);
	}

	public static long stringToLong(String str) {
		return stringToLong(str, 10);
	}

	public static long stringToLong(String str, int radix) {
		if (str == null) {
			return 0;
		}
		return Long.parseLong(str, radix);
	}

	public static int stringToInt(String str) {
		if (str == null) {
			return 0;
		}
		return Integer.parseInt(str);
	}
}
