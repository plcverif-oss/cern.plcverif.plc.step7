/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import static cern.plcverif.plc.step7.datatypes.S7DataTypeUtil.stringToDouble;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class S7Real extends AbstractS7DataType implements IS7NumericDataType {
	private static final Logger log = LogManager.getLogger(AbstractS7DataType.class);
	private static final Pattern PATTERN_REAL = Pattern
			.compile("^(?:(L?REAL)#)?([+-]?\\d+(?:\\.\\d+)?(?:e[+-]?\\d+)?)$", Pattern.CASE_INSENSITIVE);

	private S7RealType type = S7RealType.Unknown;
	private double value;

	private S7Real(String stringRepresentation) {
		super(stringRepresentation, false);
	}

	private S7Real(String stringRepresentation, S7RealType type, double value) {
		super(stringRepresentation, true);
		this.type = type;
		this.value = value;
	}

	public static boolean isValid(String str) {
		return PATTERN_REAL.matcher(str).find();
	}

	public static S7Real create(String str) {
		if (isValid(str)) {
			Matcher matcher = PATTERN_REAL.matcher(str.toUpperCase());
			if (matcher.find()) {
				String typeStr = matcher.group(1);
				String valueStr = matcher.group(2);

				S7RealType type = typeStr == null ? S7RealType.Unknown : S7RealType.valueOf(typeStr.toUpperCase());
				double value = stringToDouble(valueStr.replaceAll("_", ""));

				return new S7Real(str, type, value);
			}
		}

		// parsing is not possible
		log.info("Unable to parse as REAL#: '{}'.", str);
		return new S7Real(str);
	}

	public S7Real copy() {
		return new S7Real(this.getOriginalStringRepresentation(), this.type, this.value);
	}

	public S7Real copyAndNegate() {
		return new S7Real("-" + this.getOriginalStringRepresentation(), this.type, -1 * this.value);
	}

	public S7RealType getType() {
		return type;
	}

	@Override
	public int getSizeInBits() {
		return getSizeInBits(type);
	}

	public static int getSizeInBits(S7RealType type) {
		switch (type) {
		case REAL:
			return 32;
		case LREAL:
			return 64;
		default:
			throw new IllegalStateException("Unsupported case at S7Real.getSizeInBits: " + type);
		}
	}

	@Override
	public double doubleValue() {
		return value;
	}

	@Override
	public boolean isSigned() {
		return true;
	}

	@Override
	public boolean isWithinRange() {
		// TIA support LREAL
		// NOTE this is a really weak, preliminary check
		return Math.abs(value) < 3.41e38;
	}
}
