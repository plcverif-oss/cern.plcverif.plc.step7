/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class S7DatablockFieldAddress extends AbstractS7DataType {
	private static final Logger log = LogManager.getLogger(AbstractS7DataType.class);
	private static final Pattern PATTERN_DBFIELD_BIT = Pattern.compile("^DB?X?(\\d+)\\.([0-7])$",
			Pattern.CASE_INSENSITIVE);
	private static final Pattern PATTERN_DBFIELD_NONBIT = Pattern.compile("^DB?([B|W|D])(\\d+)$",
			Pattern.CASE_INSENSITIVE);

	private S7AddressMemorySize memorySize;
	private int byteNumber;
	private int bitNumber;

	private S7DatablockFieldAddress(String stringRepresentation) {
		super(stringRepresentation, false);
	}

	private S7DatablockFieldAddress(String stringRepresentation, S7AddressMemorySize memorySize, int byteNumber,
			int bitNumber) {
		super(stringRepresentation, true);
		this.memorySize = memorySize;
		this.byteNumber = byteNumber;
		this.bitNumber = bitNumber;
	}

	public static boolean isValid(String str) {
		return PATTERN_DBFIELD_BIT.matcher(str).find() || PATTERN_DBFIELD_NONBIT.matcher(str).find();
	}

	public static S7DatablockFieldAddress create(String str) {
		if (isValid(str)) {
			S7AddressMemorySize memorySize;
			int byteNumber;
			int bitNumber;

			Matcher bitMatcher = PATTERN_DBFIELD_BIT.matcher(str);
			if (bitMatcher.find()) {
				// bit value
				String byteNumberStr = bitMatcher.group(1);
				String bitNumberStr = bitMatcher.group(2);

				memorySize = S7AddressMemorySize.Boolean;
				byteNumber = S7DataTypeUtil.stringToInt(byteNumberStr);
				bitNumber = S7DataTypeUtil.stringToInt(bitNumberStr);
			} else {
				Matcher nonbitMatcher = PATTERN_DBFIELD_NONBIT.matcher(str);
				if (!nonbitMatcher.find()) {
					log.info("Unable to parse as datablock field address: '{}'.", str);
					return new S7DatablockFieldAddress(str);
				}

				// non-bit value
				String memorySizeStr = nonbitMatcher.group(1);
				String byteNumberStr = nonbitMatcher.group(2);

				memorySize = S7AddressMemorySize.fromString(memorySizeStr);
				byteNumber = S7DataTypeUtil.stringToInt(byteNumberStr);
				bitNumber = 0;
			}

			return new S7DatablockFieldAddress(str, memorySize, byteNumber, bitNumber);
		}

		// parsing is not possible
		log.info("Unable to parse as datablock field address: '{}'.", str);
		return new S7DatablockFieldAddress(str);
	}

	public S7AddressMemorySize getMemorySize() {
		return memorySize;
	}

	public int getByteNumber() {
		return byteNumber;
	}

	public int getBitNumber() {
		return bitNumber;
	}

	@Override
	public int getSizeInBits() {
		return this.getMemorySize().getSizeInBits();
	}

	@Override
	public boolean isWithinRange() {
		return true;
	}
}
