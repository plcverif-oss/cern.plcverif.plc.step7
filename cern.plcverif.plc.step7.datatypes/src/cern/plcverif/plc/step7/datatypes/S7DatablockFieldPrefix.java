/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class S7DatablockFieldPrefix extends AbstractS7DataType {
	private static final Logger log = LogManager.getLogger(AbstractS7DataType.class);
	private static final Pattern PATTERN_DBFIELD_PREFIX = Pattern.compile("^D([X|B|W|D])\\[$",
			Pattern.CASE_INSENSITIVE);

	private S7AddressMemorySize memorySize;

	private S7DatablockFieldPrefix(String stringRepresentation) {
		super(stringRepresentation, false);
	}

	private S7DatablockFieldPrefix(String stringRepresentation, S7AddressMemorySize memorySize) {
		super(stringRepresentation, true);
		this.memorySize = memorySize;
	}

	public static boolean isValid(String str) {
		return PATTERN_DBFIELD_PREFIX.matcher(str).find();
	}

	public static S7DatablockFieldPrefix create(String str) {
		if (isValid(str)) {
			Matcher matcher = PATTERN_DBFIELD_PREFIX.matcher(str);
			if (matcher.find()) {
				String memorySizeStr = matcher.group(1);
				S7AddressMemorySize memorySize = S7AddressMemorySize.fromString(memorySizeStr);

				return new S7DatablockFieldPrefix(str, memorySize);
			}
		}

		// parsing is not possible
		log.info("Unable to parse as datablock field prefix: '{}'.", str);
		return new S7DatablockFieldPrefix(str);
	}

	public S7AddressMemorySize getMemorySize() {
		return memorySize;
	}

	@Override
	public int getSizeInBits() {
		return this.getMemorySize().getSizeInBits();
	}

	@Override
	public boolean isWithinRange() {
		return true;
	}
}
