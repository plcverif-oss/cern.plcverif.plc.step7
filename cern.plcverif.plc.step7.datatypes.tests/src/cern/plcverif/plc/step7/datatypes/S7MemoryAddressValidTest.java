/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class S7MemoryAddressValidTest {
	@SuppressWarnings("boxing")
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			// @formatter:off
			{ "I0.0", S7AddressMemoryIdentifier.I, S7AddressMemorySize.Boolean, 0, 0 },
			{ "I0.1", S7AddressMemoryIdentifier.I, S7AddressMemorySize.Boolean, 0, 1 },
			{ "IX0.6", S7AddressMemoryIdentifier.I, S7AddressMemorySize.Boolean, 0, 6 },
			{ "%I0.7", S7AddressMemoryIdentifier.I, S7AddressMemorySize.Boolean, 0, 7 },
			{ "IB11", S7AddressMemoryIdentifier.I, S7AddressMemorySize.Byte, 11, 0 },
			{ "IW12", S7AddressMemoryIdentifier.I, S7AddressMemorySize.Word, 12, 0 },
			{ "ID13", S7AddressMemoryIdentifier.I, S7AddressMemorySize.Dword, 13, 0 },
			{ "MW14", S7AddressMemoryIdentifier.M, S7AddressMemorySize.Word, 14, 0 },
			{ "QW15", S7AddressMemoryIdentifier.Q, S7AddressMemorySize.Word, 15, 0 },
			{ "PIW16", S7AddressMemoryIdentifier.PI, S7AddressMemorySize.Word, 16, 0 },
			{ "PQW17", S7AddressMemoryIdentifier.PQ, S7AddressMemorySize.Word, 17, 0 },
			// @formatter:on
		});
	}

	private final String stringRepresentation;
	private final S7AddressMemoryIdentifier expectedMemoryIdentifier;
	private final S7AddressMemorySize expectedMemorySize;
	private final int expectedByteNumber;
	private final int expectedBitNumber;

	public S7MemoryAddressValidTest(String stringRepresentation, S7AddressMemoryIdentifier expectedMemoryIdentifier,
			S7AddressMemorySize expectedMemorySize, int expectedByteNumber, int expectedBitNumber) {
		this.stringRepresentation = stringRepresentation;
		this.expectedMemoryIdentifier = expectedMemoryIdentifier;
		this.expectedMemorySize = expectedMemorySize;
		this.expectedByteNumber = expectedByteNumber;
		this.expectedBitNumber = expectedBitNumber;
	}

	@Test
	public void test() {
		S7MemoryAddress parsedData = S7MemoryAddress.create(stringRepresentation);

		Assert.assertTrue(parsedData.isSyntacticallyValid());
		Assert.assertEquals(stringRepresentation, parsedData.getOriginalStringRepresentation());
		Assert.assertEquals(expectedMemoryIdentifier, parsedData.getMemoryIdentifier());
		Assert.assertEquals(expectedMemorySize, parsedData.getMemorySize());
		Assert.assertEquals(expectedByteNumber, parsedData.getByteNumber());
		Assert.assertEquals(expectedBitNumber, parsedData.getBitNumber());
	}
}
