/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import org.junit.Assert;
import org.junit.Test;

public class S7BoolTest {
	private static void testBoolValid(String str, boolean expectedValue) {
		S7Bool data = S7Bool.create(str);
		Assert.assertTrue(data.isSyntacticallyValid());
		Assert.assertTrue(data.boolValue() == expectedValue);
	}

	@Test
	public void testBoolValid1() {
		testBoolValid("BOOL#true", true);
	}

	@Test
	public void testBoolValid2() {
		testBoolValid("bool#FALSE", false);
	}

	@Test
	public void testBoolValid3() {
		testBoolValid("True", true);
	}

	@Test
	public void testBoolValid4() {
		testBoolValid("false", false);
	}

	@Test
	public void testBoolInvalid1() {
		S7Bool data = S7Bool.create("bool#0");
		Assert.assertFalse(data.isSyntacticallyValid());
	}

	@Test
	public void testBoolInvalid2() {
		S7Bool data = S7Bool.create("abc");
		Assert.assertFalse(data.isSyntacticallyValid());
	}
}
