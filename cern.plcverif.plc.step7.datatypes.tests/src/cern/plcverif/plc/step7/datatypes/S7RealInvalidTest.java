/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import org.junit.Assert;
import org.junit.Test;

public class S7RealInvalidTest {
	@Test
	public void testRealInvalid() {
		String[] invalidStrings = { "abc", "", "REAL#a", "TIME#123", "REAL#12.3e12.3", "XREAL#5",
				// Following are from the Siemens SCL V5.3 documentation:
				"1.", // There must be a digit on both sides of the decimal
						// point.
				"1,000.0", // Integers must not contain commas.
				".333", // There must be a digit on both sides of the decimal
						// point.
				"3.E+10", // There must be a digit on both sides of the decimal
							// point.
				"8e2.3", // The exponent must be an integer.
				".333e-3", // There must be a digit on both sides of the decimal
							// point.
				"30 E10", // Integers must not contain spaces.
		};

		for (String str : invalidStrings) {
			S7Real data = S7Real.create(str);
			Assert.assertFalse(data.isSyntacticallyValid());
		}
	}
}
