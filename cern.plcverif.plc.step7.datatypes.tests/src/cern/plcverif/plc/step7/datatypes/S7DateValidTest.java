/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class S7DateValidTest {
	@SuppressWarnings("boxing")
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			// @formatter:off
			{ "1990-03-01", 1990, 3, 1 },
			{ "1234-567-89", 1234, 567, 89 },
			{ "D#1990-1-2", 1990, 1, 2 },
			{ "date#2017-12-24", 2017, 12, 24 },
			{ "D#1_9_5_4-10-20", 1954, 10, 20 },
			// @formatter:on
		});
	}

	private final String stringRepresentation;
	private final int expectedYear;
	private final int expectedMonth;
	private final int expectedDay;

	public S7DateValidTest(String stringRepresentation, int expectedYear, int expectedMonth, int expectedDay) {
		this.stringRepresentation = stringRepresentation;
		this.expectedYear = expectedYear;
		this.expectedMonth = expectedMonth;
		this.expectedDay = expectedDay;
	}

	@Test
	public void test() {
		S7Date parsedData = S7Date.create(stringRepresentation);

		Assert.assertTrue(parsedData.isSyntacticallyValid());
		Assert.assertEquals(stringRepresentation, parsedData.getOriginalStringRepresentation());
		Assert.assertEquals(expectedYear, parsedData.getYear());
		Assert.assertEquals(expectedMonth, parsedData.getMonth());
		Assert.assertEquals(expectedDay, parsedData.getDate());
	}
}
