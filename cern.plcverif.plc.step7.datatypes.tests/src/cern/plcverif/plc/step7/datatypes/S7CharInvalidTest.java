/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import org.junit.Assert;
import org.junit.Test;

public class S7CharInvalidTest {
	@Test
	public void testCharInvalid() {
		String[] invalidStrings = { "abc", "", "CHAR#abc", "TIME#123", "$N", "CHAR#$N", "'$X'", "'$ABC'", "CHAR#'64'",
				"'XY'" };

		for (String str : invalidStrings) {
			S7Char data = S7Char.create(str);
			Assert.assertFalse(data.isSyntacticallyValid());
		}
	}
}
