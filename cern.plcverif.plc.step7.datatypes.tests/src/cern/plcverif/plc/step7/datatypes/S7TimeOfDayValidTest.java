/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class S7TimeOfDayValidTest {
	@SuppressWarnings("boxing")
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			// @formatter:off
			{ "TOD#12:01:30.123", 12, 1, 30, 123 },
			{ "TOD#12:01:30", 12, 1, 30, 0 },
			{ "TIME_OF_DAY#18:15:10", 18, 15, 10, 0 },
			{ "TOD#1_2:0_1:3_0.1_2_3", 12, 1, 30, 123 },
			// @formatter:on
		});
	}

	private final String stringRepresentation;
	private final int expectedHours;
	private final int expectedMinutes;
	private final int expectedSeconds;
	private final int expectedMilliseconds;

	public S7TimeOfDayValidTest(String stringRepresentation, int expectedHours, int expectedMinutes,
			int expectedSeconds, int expectedMilliseconds) {
		this.stringRepresentation = stringRepresentation;
		this.expectedHours = expectedHours;
		this.expectedMinutes = expectedMinutes;
		this.expectedSeconds = expectedSeconds;
		this.expectedMilliseconds = expectedMilliseconds;
	}

	@Test
	public void test() {
		S7TimeOfDay parsedData = S7TimeOfDay.create(stringRepresentation);

		Assert.assertTrue(parsedData.isSyntacticallyValid());
		Assert.assertEquals(stringRepresentation, parsedData.getOriginalStringRepresentation());
		Assert.assertEquals(expectedHours, parsedData.getHours());
		Assert.assertEquals(expectedMinutes, parsedData.getMinutes());
		Assert.assertEquals(expectedSeconds, parsedData.getSeconds());
		Assert.assertEquals(expectedMilliseconds, parsedData.getMilliseconds());
	}
}
