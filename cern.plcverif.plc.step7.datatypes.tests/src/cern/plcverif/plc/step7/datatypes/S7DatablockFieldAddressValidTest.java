/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class S7DatablockFieldAddressValidTest {
	@SuppressWarnings("boxing")
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			// @formatter:off
			{ "DBB123", S7AddressMemorySize.Byte, 123, 0 },
			{ "DB124", S7AddressMemorySize.Byte, 124, 0 },
			{ "DBW125", S7AddressMemorySize.Word, 125, 0 },
			{ "DW126", S7AddressMemorySize.Word, 126, 0 },
			{ "DBD127", S7AddressMemorySize.Dword, 127, 0 },
			{ "DD128", S7AddressMemorySize.Dword, 128, 0 },
			{ "DB10.1", S7AddressMemorySize.Boolean, 10, 1 },
			{ "DBX10.2", S7AddressMemorySize.Boolean, 10, 2 },
			{ "DX10.3", S7AddressMemorySize.Boolean, 10, 3 },
			{ "D10.7", S7AddressMemorySize.Boolean, 10, 7 },
			// @formatter:on
		});
	}

	private final String stringRepresentation;
	private final S7AddressMemorySize expectedMemorySize;
	private final int expectedByteNumber;
	private final int expectedBitNumber;

	public S7DatablockFieldAddressValidTest(String stringRepresentation, S7AddressMemorySize expectedMemorySize,
			int expectedByteNumber, int expectedBitNumber) {
		this.stringRepresentation = stringRepresentation;
		this.expectedMemorySize = expectedMemorySize;
		this.expectedByteNumber = expectedByteNumber;
		this.expectedBitNumber = expectedBitNumber;
	}

	@Test
	public void test() {
		S7DatablockFieldAddress parsedData = S7DatablockFieldAddress.create(stringRepresentation);

		Assert.assertTrue(parsedData.isSyntacticallyValid());
		Assert.assertEquals(stringRepresentation, parsedData.getOriginalStringRepresentation());
		Assert.assertEquals(expectedMemorySize, parsedData.getMemorySize());
		Assert.assertEquals(expectedByteNumber, parsedData.getByteNumber());
		Assert.assertEquals(expectedBitNumber, parsedData.getBitNumber());
	}
}
