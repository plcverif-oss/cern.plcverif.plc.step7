/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class S7CharValidTest {
	@SuppressWarnings("boxing")
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			// @formatter:off
			{ "'a'", 'a' },
			{ "' '", ' ' },
			{ "CHAR#'b'", 'b' },
			{ "'$N'", '\n' },
			{ "char#'$n'", '\n' },
			{ "CHAR#64", '@' },
			{ "CHAR#'$40'", '@' },
			{ "'$40'", '@' },
			{ "char#'$t'", '\t' },
			{ "char#'$r'", '\r' },
			{ "char#'$''", '\'' },
			{ "char#'$$'", '$' },
			// @formatter:on
		});
	}

	private final String stringRepresentation;
	private final char expectedCharValue;

	public S7CharValidTest(String stringRepresentation, char expectedCharValue) {
		this.stringRepresentation = stringRepresentation;
		this.expectedCharValue = expectedCharValue;
	}

	@Test
	public void test() {
		S7Char parsedData = S7Char.create(stringRepresentation);

		Assert.assertTrue(parsedData.isSyntacticallyValid());
		Assert.assertEquals(stringRepresentation, parsedData.getOriginalStringRepresentation());
		Assert.assertEquals(expectedCharValue, parsedData.charValue());
	}
}
