/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import org.junit.Assert;
import org.junit.Test;

public class S7CounterTest {
	private static void testCounterValid(String str, int expectedValue) {
		S7Counter data = S7Counter.create(str);
		Assert.assertTrue(data.isSyntacticallyValid());
		Assert.assertTrue(data.getCounterId() == expectedValue);
	}

	@Test
	public void testCounterValid1() {
		testCounterValid("C123", 123);
	}

	@Test
	public void testCounterValid2() {
		testCounterValid("C1", 1);
	}

	@Test
	public void testCounterInvalid1() {
		S7Counter data = S7Counter.create("0");
		Assert.assertFalse(data.isSyntacticallyValid());
	}

	@Test
	public void testCounterInvalid2() {
		S7Counter data = S7Counter.create("CX1");
		Assert.assertFalse(data.isSyntacticallyValid());
	}
}
