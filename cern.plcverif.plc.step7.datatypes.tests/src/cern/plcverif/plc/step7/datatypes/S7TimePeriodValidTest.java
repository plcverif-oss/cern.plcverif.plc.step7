/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class S7TimePeriodValidTest {
	private static final double DELTA = 0.0001;

	@SuppressWarnings("boxing")
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			// @formatter:off
			{ "T#1D2H3M4S567MS", 1, 2, 3, 4, 567 },
			{ "T#1D_3H_5M_7S_987MS", 1, 3, 5, 7, 987 },
			{ "T#123456MS", 0, 0, 0, 0, 123456 },
			{ "T#1H", 0, 1, 0, 0, 0 },
			{ "T#1H30M", 0, 1, 30, 0, 0 },
			{ "T#1H30S", 0, 1, 0, 30, 0 },
			{ "T#1H30.1S", 0, 1, 0, 30.1, 0 },
			{ "T#1H_30.1S", 0, 1, 0, 30.1, 0 },
			{ "T#1.567H", 0, 1.567, 0, 0, 0 },
			{ "TIME#1D_3H_5M_7S_987MS", 1, 3, 5, 7, 987 },
			{ "time#1d_3h_5m_7s_987ms", 1, 3, 5, 7, 987 },
			// @formatter:on
		});
	}

	private final String stringRepresentation;
	private final double expectedDays;
	private final double expectedHours;
	private final double expectedMinutes;
	private final double expectedSeconds;
	private final double expectedMilliseconds;

	public S7TimePeriodValidTest(String stringRepresentation, double expectedDays, double expectedHours,
			double expectedMinutes, double expectedSeconds, double expectedMilliseconds) {
		this.stringRepresentation = stringRepresentation;
		this.expectedDays = expectedDays;
		this.expectedHours = expectedHours;
		this.expectedMinutes = expectedMinutes;
		this.expectedSeconds = expectedSeconds;
		this.expectedMilliseconds = expectedMilliseconds;
	}

	@Test
	public void test() {
		S7TimePeriod parsedData = S7TimePeriod.create(stringRepresentation);

		Assert.assertTrue(parsedData.isSyntacticallyValid());
		Assert.assertEquals(stringRepresentation, parsedData.getOriginalStringRepresentation());
		Assert.assertEquals(expectedDays, parsedData.getDays(), DELTA);
		Assert.assertEquals(expectedHours, parsedData.getHours(), DELTA);
		Assert.assertEquals(expectedMinutes, parsedData.getMinutes(), DELTA);
		Assert.assertEquals(expectedSeconds, parsedData.getSeconds(), DELTA);
		Assert.assertEquals(expectedMilliseconds, parsedData.getMilliseconds(), DELTA);
	}
}
