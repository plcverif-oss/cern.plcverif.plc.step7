/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import org.junit.Assert;
import org.junit.Test;

public class S7TimerTest {
	private static void testTimerValid(String str, int expectedValue) {
		S7Timer data = S7Timer.create(str);
		Assert.assertTrue(data.isSyntacticallyValid());
		Assert.assertTrue(data.getTimerId() == expectedValue);
	}

	@Test
	public void testTimerValid1() {
		testTimerValid("T123", 123);
	}

	@Test
	public void testTimerValid2() {
		testTimerValid("T1", 1);
	}

	@Test
	public void testTimerInvalid1() {
		S7Timer data = S7Timer.create("0");
		Assert.assertFalse(data.isSyntacticallyValid());
	}

	@Test
	public void testTimerInvalid2() {
		S7Timer data = S7Timer.create("TX1");
		Assert.assertFalse(data.isSyntacticallyValid());
	}
}
