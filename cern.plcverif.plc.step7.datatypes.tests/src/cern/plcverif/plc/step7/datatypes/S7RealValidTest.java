/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class S7RealValidTest {
	private static final double DELTA = 0.0001;

	@SuppressWarnings("boxing")
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			// @formatter:off
			{ "10.5", S7RealType.Unknown, 10.5 },
			{ "0", S7RealType.Unknown, 0 },
			{ "real#123.45", S7RealType.REAL,  123.45 },
			{ "LREAL#-100", S7RealType.LREAL, -100},
			{ "REAL#1.23e10", S7RealType.REAL,  1.23e10 },
			{ "REAL#1.23e-2", S7RealType.REAL,  1.23e-2 },
			{ "1e10", S7RealType.Unknown,  1e10 },

			// following are from Siemens SCL V5.3 definition, Section 9.1.3, p. 9-4 (A5E00324650-01)
			{ "123.4567", S7RealType.Unknown, 123.4567 },
			{ "REAL#1", S7RealType.REAL, 1 },
			{ "real#1.5", S7RealType.REAL, 1.5 },
			{ "real#2e4", S7RealType.REAL,  20000 },
			{ "+1.234567E+02", S7RealType.Unknown,  123.4567 },

			// following are from Siemens SCL V5.3 definition, Section 5.10, p. 5-11 (A5E00324650-01)
			{ "3.0E+10", S7RealType.Unknown, 3e10 },
			{ "3.0E10", S7RealType.Unknown, 3e10 },
			{ "3e+10", S7RealType.Unknown, 3e10 },
			{ "3E10", S7RealType.Unknown, 3e10 },
			{ "0.3E+11", S7RealType.Unknown, 3e10 },
			{ "0.3e11", S7RealType.Unknown, 3e10 },
			{ "30.0E+9", S7RealType.Unknown, 3e10 },
			{ "30e9", S7RealType.Unknown, 3e10 },
			// @formatter:on
		});
	}

	private final String stringRepresentation;
	private final S7RealType expectedType;
	private final double expectedDoubleValue;

	public S7RealValidTest(String stringRepresentation, S7RealType expectedType, double expectedDoubleValue) {
		this.stringRepresentation = stringRepresentation;
		this.expectedType = expectedType;
		this.expectedDoubleValue = expectedDoubleValue;
	}

	@Test
	public void test() {
		S7Real parsedData = S7Real.create(stringRepresentation);

		Assert.assertTrue(parsedData.isSyntacticallyValid());
		Assert.assertEquals(stringRepresentation, parsedData.getOriginalStringRepresentation());
		Assert.assertEquals(expectedType, parsedData.getType());
		Assert.assertEquals(expectedDoubleValue, parsedData.doubleValue(), DELTA);
	}
}
