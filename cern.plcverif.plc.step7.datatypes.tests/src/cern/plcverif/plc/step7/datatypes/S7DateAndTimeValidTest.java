/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class S7DateAndTimeValidTest {
	@SuppressWarnings("boxing")
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			// @formatter:off
			{ "DT#1990-01-02-03:04:05.678", 1990, 1, 2, 3, 4, 5, 678 },
			{ "DATE_AND_TIME#2017-02-23-15:52:24.123", 2017, 2, 23, 15, 52, 24, 123 },
			{ "DT#2_0_1_7-0_2-2_3-1_5:5_2:2_4.1_2_3", 2017, 2, 23, 15, 52, 24, 123 },
			// @formatter:on
		});
	}

	private final String stringRepresentation;

	private final int expectedYear;
	private final int expectedMonth;
	private final int expectedDay;
	private final int expectedHours;
	private final int expectedMinutes;
	private final int expectedSeconds;
	private final int expectedMilliseconds;

	public S7DateAndTimeValidTest(String stringRepresentation, int expectedYear, int expectedMonth, int expectedDay,
			int expectedHours, int expectedMinutes, int expectedSeconds, int expectedMilliseconds) {
		this.stringRepresentation = stringRepresentation;
		this.expectedYear = expectedYear;
		this.expectedMonth = expectedMonth;
		this.expectedDay = expectedDay;
		this.expectedHours = expectedHours;
		this.expectedMinutes = expectedMinutes;
		this.expectedSeconds = expectedSeconds;
		this.expectedMilliseconds = expectedMilliseconds;
	}

	@Test
	public void test() {
		S7DateAndTime parsedData = S7DateAndTime.create(stringRepresentation);

		Assert.assertTrue(parsedData.isSyntacticallyValid());
		Assert.assertEquals(stringRepresentation, parsedData.getOriginalStringRepresentation());
		Assert.assertEquals(expectedYear, parsedData.getYear());
		Assert.assertEquals(expectedMonth, parsedData.getMonth());
		Assert.assertEquals(expectedDay, parsedData.getDay());
		Assert.assertEquals(expectedHours, parsedData.getHours());
		Assert.assertEquals(expectedMinutes, parsedData.getMinutes());
		Assert.assertEquals(expectedSeconds, parsedData.getSeconds());
		Assert.assertEquals(expectedMilliseconds, parsedData.getMilliseconds());
	}
}
