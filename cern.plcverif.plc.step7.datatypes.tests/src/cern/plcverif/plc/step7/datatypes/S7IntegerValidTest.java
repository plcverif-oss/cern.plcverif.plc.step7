/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class S7IntegerValidTest {
	private static final double DELTA = 0.0001;

	@SuppressWarnings("boxing")
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			// @formatter:off
			{ "int#16#ff", S7IntegerType.INT, S7IntegerBase.Hexadecimal, 255 },
			{ "123", S7IntegerType.Unknown, S7IntegerBase.Decimal, 123 },
			{ "uint#8#123", S7IntegerType.UINT, S7IntegerBase.Octal, 83},
			{ "dint#2#0001_001", S7IntegerType.DINT, S7IntegerBase.Binary, 9},
			{ "INT#100", S7IntegerType.INT, S7IntegerBase.Decimal, 100},
			{ "10#101", S7IntegerType.Unknown, S7IntegerBase.Decimal, 101},
			{ "2#1010", S7IntegerType.Unknown, S7IntegerBase.Binary, 10},
			{ "WORD#2#1010", S7IntegerType.WORD, S7IntegerBase.Binary, 10},
			{ "DWORD#0333", S7IntegerType.DWORD, S7IntegerBase.Decimal, 333},
			{ "byte#1", S7IntegerType.BYTE, S7IntegerBase.Decimal, 1},

			// following are from Siemens SCL V5.3 definition, Section 9.1.3, p. 9-4 (A5E00324650-01)
			{ "B#16#00", S7IntegerType.BYTE, S7IntegerBase.Hexadecimal, 0},
			{ "B#16#FF", S7IntegerType.BYTE, S7IntegerBase.Hexadecimal, 255},
			{ "BYTE#0", S7IntegerType.BYTE, S7IntegerBase.Decimal, 0},
			{ "B#2#101", S7IntegerType.BYTE, S7IntegerBase.Binary, 5},
			//{ "Byte#'�'", S7IntegerType.BYTE, S7IntegerBase.Decimal, 0},
			{ "b#16#f", S7IntegerType.BYTE, S7IntegerBase.Hexadecimal, 15},

			{ "W#16#0000", S7IntegerType.WORD, S7IntegerBase.Hexadecimal, 0},
			{ "W#16#FFFF", S7IntegerType.WORD, S7IntegerBase.Hexadecimal, 65535},
			{ "word#16#f", S7IntegerType.WORD, S7IntegerBase.Hexadecimal, 15},
			{ "WORD#8#177777", S7IntegerType.WORD, S7IntegerBase.Octal, 65535},
			{ "8#177777", S7IntegerType.Unknown, S7IntegerBase.Octal, 65535},
			{ "W#2#1001_0100", S7IntegerType.WORD, S7IntegerBase.Binary, 148},
			{ "WORD#32768", S7IntegerType.WORD, S7IntegerBase.Decimal, 32768},

			{ "DW#16#0000_0000", S7IntegerType.DWORD, S7IntegerBase.Hexadecimal, 0},
			{ "DW#16#FFFF_FFFF", S7IntegerType.DWORD, S7IntegerBase.Hexadecimal, 4294967295L},
			{ "Dword#8#37777777777", S7IntegerType.DWORD, S7IntegerBase.Octal, 4294967295L},
			{ "8#37777777777", S7IntegerType.Unknown, S7IntegerBase.Octal, 4294967295L},
			{ "DW#2#1111_0000_1111_0000", S7IntegerType.DWORD, S7IntegerBase.Binary, 61680},
			{ "dword#32768", S7IntegerType.DWORD, S7IntegerBase.Decimal, 32768},

			{ "-32768", S7IntegerType.Unknown, S7IntegerBase.Decimal, -32768},
			{ "+32767", S7IntegerType.Unknown, S7IntegerBase.Decimal, 32767},
			{ "INT#16#3f_ff", S7IntegerType.INT, S7IntegerBase.Hexadecimal, 16383},
			{ "int#-32768", S7IntegerType.INT, S7IntegerBase.Decimal, -32768},
			{ "Int#2#1111_0000", S7IntegerType.INT, S7IntegerBase.Binary, 240},
			{ "Int#-2#1111_0000", S7IntegerType.INT, S7IntegerBase.Binary, -240},
			{ "inT#8#77777", S7IntegerType.INT, S7IntegerBase.Octal, 32767},

			{ "-2147483648", S7IntegerType.Unknown, S7IntegerBase.Decimal, -2147483648},
			{ "+2147483647", S7IntegerType.Unknown, S7IntegerBase.Decimal, 2147483647},
			{ "DINT#16#3fff_ffff", S7IntegerType.DINT, S7IntegerBase.Hexadecimal, 1073741823},
			{ "dint#-65000", S7IntegerType.DINT, S7IntegerBase.Decimal, -65000},
			{ "Dint#2#1111_0000", S7IntegerType.DINT, S7IntegerBase.Binary, 240},
			{ "Dint#-2#1111_0000", S7IntegerType.DINT, S7IntegerBase.Binary, -240},
			{ "dinT#8#17777777777", S7IntegerType.DINT, S7IntegerBase.Octal, 2147483647},
			// @formatter:on
		});
	}

	private final String stringRepresentation;
	private final S7IntegerType expectedType;
	private final S7IntegerBase expectedBase;
	private final long expectedIntValue;

	public S7IntegerValidTest(String stringRepresentation, S7IntegerType expectedType, S7IntegerBase expectedBase,
			long expectedIntValue) {
		this.stringRepresentation = stringRepresentation;
		this.expectedType = expectedType;
		this.expectedBase = expectedBase;
		this.expectedIntValue = expectedIntValue;
	}

	@Test
	public void test() {
		S7Integer parsedData = S7Integer.create(stringRepresentation);

		Assert.assertTrue(parsedData.isSyntacticallyValid());
		Assert.assertEquals(stringRepresentation, parsedData.getOriginalStringRepresentation());
		Assert.assertEquals(expectedType, parsedData.getType());
		Assert.assertEquals(expectedBase, parsedData.getBase());
		Assert.assertEquals(expectedIntValue, parsedData.intValue());
		Assert.assertEquals(expectedIntValue, parsedData.doubleValue(), DELTA);
	}
}
