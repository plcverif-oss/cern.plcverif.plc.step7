/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 *
 * Based on: Section 7.2
 *
 */
@RunWith(Parameterized.class)
public class BitSizeTest {
	@SuppressWarnings("boxing")
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			// @formatter:off
			{ S7Bool.create("true"), 1 },
			{ S7Char.create("'a'"), 8 },
			{ S7Integer.create("int#1"), 16 },
			{ S7Integer.create("uint#1"), 16 },
			{ S7Integer.create("dint#1"), 32 },
			{ S7Integer.create("udint#1"), 32 },
			{ S7Integer.create("byte#1"), 8 },
			{ S7Integer.create("word#1"), 16 },
			{ S7Integer.create("dword#1"), 32 },
			{ S7Real.create("real#1.0"), 32 },
			{ S7Date.create("date#1990-01-01"), 16 },
			{ S7DateAndTime.create("dt#1990-01-01-12:30:00"), 64 },
			{ S7TimeOfDay.create("tod#12:30:00"), 32 },
			{ S7TimePeriod.create("t#1d"), 32 },
			// { S5Time.create("T#0H_0M_0S_10MS"), 16 },
			// @formatter:on
		});
	}

	private final IS7DataType dataValue;
	private final int expectedSizeInBits;

	public BitSizeTest(IS7DataType data, int expectedSizeInBits) {
		this.dataValue = data;
		this.expectedSizeInBits = expectedSizeInBits;
	}

	@Test
	public void test() {
		Assert.assertTrue(dataValue.isSyntacticallyValid());
		Assert.assertEquals(expectedSizeInBits, dataValue.getSizeInBits());
	}
}
