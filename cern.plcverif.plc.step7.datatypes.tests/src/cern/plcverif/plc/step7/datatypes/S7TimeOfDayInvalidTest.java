/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.step7.datatypes;

import org.junit.Assert;
import org.junit.Test;

public class S7TimeOfDayInvalidTest {
	@Test
	public void testTimePeriodInvalid() {
		String[] invalidStrings = { "abc", "", "TOD#abc", "TOD#123", "12:30:00", "TOD#12:30", "TOD#12" };

		for (String str : invalidStrings) {
			S7TimeOfDay data = S7TimeOfDay.create(str);
			Assert.assertFalse(data.isSyntacticallyValid());
		}
	}
}
