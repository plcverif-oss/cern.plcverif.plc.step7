/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.codegen

import cern.plcverif.plc.tia.fbd.BitLogicInstruction
import cern.plcverif.plc.tia.fbd.BitLogicOperator
import cern.plcverif.plc.tia.fbd.CoilKind
import cern.plcverif.plc.tia.fbd.CompareOperator
import cern.plcverif.plc.tia.fbd.Connectable
import cern.plcverif.plc.tia.fbd.Constant
import cern.plcverif.plc.tia.fbd.MathDataType
import cern.plcverif.plc.tia.fbd.NetworkElementFactory
import cern.plcverif.plc.tia.fbd.Port
import cern.plcverif.plc.tia.fbd.ReturnKind
import cern.plcverif.plc.tia.fbd.Symbol
import cern.plcverif.plc.tia.fbd.UnknownPart
import cern.plcverif.plc.tia.fbd.Value
import org.junit.Before
import org.junit.Test

import static cern.plcverif.plc.tia.fbd.utils.CodeAssert.*;

class StlCodeGeneratorTest {
	NetworkElementFactory factory;
	UnknownPartTranslator unknownPartTranslator;
	
	StlCodeGenerator generator;
	
	Symbol a;
	Symbol b;
	Symbol c;
	Symbol d;
	
	Symbol x;
	Symbol y;
	Symbol z;
	
	Symbol w1;
	Symbol w2;
	Symbol w3;
	
	Constant cBool;
	Constant cInt;
	Constant cWord;
	Constant cFloat;
	
	private static class UnknownPartTranslatorMock implements UnknownPartTranslator
	{		
		override setNetworkElementTranslator(NetworkElementTranslator translator) {}
		
		override translate(UnknownPart part, String rloValue) {}
	}
	
	@Before
	def void setUp() {
		this.factory = NetworkElementFactory.create();
		//this.unknownPartTranslator = Mockito.mock(UnknownPartTranslator);
		this.unknownPartTranslator = new UnknownPartTranslatorMock();
		
		this.generator = new StlCodeGenerator(this.unknownPartTranslator);
		
		// While A, B, and X are not explicitly enclosed in quotes here,
		// they should be put in quotes automatically by the generator.
		
		a = this.factory.createSymbol("#A");
		b = this.factory.createSymbol("#B");
		c = this.factory.createSymbol("#\"C\"");
		d = this.factory.createSymbol("#\"D\"");
		
		x = this.factory.createSymbol("#X");
		y = this.factory.createSymbol("#\"Y\"");		
		z = this.factory.createSymbol("#\"Z\"");
				
		w1 = this.factory.createSymbol("#\"W1\"");
		w2 = this.factory.createSymbol("#\"W2\"");
		w3 = this.factory.createSymbol("#\"W3\"");
		
		cBool = this.factory.createConstant("true");
		cInt = this.factory.createConstant("1234");
		cWord = this.factory.createConstant("16#FAFA");
		cFloat = this.factory.createConstant("3.14"); 
	}
	
	def void checkTranslateCoil(CoilKind kind, Symbol operand, Connectable input, boolean inputNegated, boolean outputNegated, CharSequence expected) {
		val coil = this.factory.createCoil(kind);
		coil.inPort.connection = input;
		coil.operand = operand;
		
		coil.inPort.isNegated = inputNegated;
		coil.outPort.isNegated = outputNegated;
		
		assertCodeMatches(expected, generator.translate(coil));		
	}
	
	def void checkTranslateCompare(CompareOperator operator, MathDataType type, Value in1, Value in2, boolean outputNegated, CharSequence expected) {
		val compare = this.factory.createCompare(operator, type);
		compare.first = in1;
		compare.second = in2;
		
		compare.outPort.isNegated = outputNegated;
		
		assertCodeMatches(expected, generator.translate(compare));
	}
	
	def void checkTranslateBitLogic(BitLogicOperator operator, boolean in1Neg, boolean in2Neg, boolean outNeg, CharSequence expected) {
		val bl = this.createBitOperation(operator, this.a, this.b);
		
		bl.getInPort(1).isNegated = in1Neg;
		bl.getInPort(2).isNegated = in2Neg;
		bl.outPort.isNegated = outNeg;		
		
		assertCodeMatches(expected, generator.translate(bl));
	}
	
	@Test
	def void testComplexTranslateBitLogic() {
		// XOR(AND(A, B), C)
		val xor = this.factory.createBitLogicInstruction(BitLogicOperator.XOR, 2);
		xor.getInPort(2).connection = this.c; 
		
		this.createBitOperation(BitLogicOperator.AND, this.a, this.b, xor.getInPort(1));
		
		assertCodeMatches('''
			X(;
				A #"A";
				A #"B";
			);
			X #"C";
		''', generator.translate(xor));		
	}
	
	@Test
	def void testMultiaryBitLogic() {
		val or = this.factory.createBitLogicInstruction(BitLogicOperator.OR, 3);
		or.getInPort(1).connection = this.a; 
		or.getInPort(2).connection = this.b; 
		or.getInPort(3).connection = this.c; 
		
		assertCodeMatches('''
			O #"A";
			O #"B";
			O #"C";
		''', generator.translate(or));
	}
	
	@Test
	def void testTranslateMove() {
		var move = this.factory.createMoveInstruction(1);		
		move.inputPort.connection = this.w1;		
		move.outputPorts.get(0).connection = this.w2;
		
		assertCodeMatches('''
			L #"W1";
			T #"W2";
		''', generator.translate(move));
		
		move.en.connection = a;		
		assertCodeMatches('''
			A #"A";
			JCN « GeneratorState.LABEL_NAME_PREFIX »0;
			L #"W1";
			T #"W2";
			« GeneratorState.LABEL_NAME_PREFIX »0: NOP 0;
		''', generator.translate(move));
		
		generator.state.reset();
		
		move.en.isNegated = true
		assertCodeMatches('''
			AN #"A";
			JCN « GeneratorState.LABEL_NAME_PREFIX »0;
			L #"W1";
			T #"W2";
			« GeneratorState.LABEL_NAME_PREFIX »0: NOP 0;
		''', generator.translate(move));
		
		move = this.factory.createMoveInstruction(3);
		move.inputPort.connection = this.cWord;
		move.outputPorts.get(0).connection = this.w1;
		move.outputPorts.get(1).connection = this.w2;
		move.outputPorts.get(2).connection = this.w3;
		
		assertCodeMatches('''
			L 16#FAFA;
			T #"W1";
			T #"W2";
			T #"W3";
		''', generator.translate(move));
	}
	
	
	@Test
	def void testTranslateReturn() {
		val ret = this.factory.createReturn(ReturnKind.TRUE);
		ret.inPort.connection = this.a; 

		assertCodeMatches('''
			A #"A";
			SAVE;
			BEC;
		''', this.generator.translate(ret));	
				
		ret.kind = ReturnKind.TRUE
		assertCodeMatches('''
			A #"A";
			SAVE;
			BEC;
		''', this.generator.translate(ret));
		
		ret.kind = ReturnKind.FALSE;
		assertCodeMatches('''
			A #"A";
			NOT;
			SAVE;
			NOT;
			BEC;
		''', this.generator.translate(ret));				
	}
	
	@Test
	def void testTranslateUnknownPart() {
		val unknownPart = this.factory.createUnknownPart("TEST_PART");
		
		generator.translate(unknownPart);
		//Mockito.verify(this.unknownPartTranslator, Mockito.times(1)).translate(unknownPart, "");
	}
	
	@Test
	def void testBranch() {
		val bl = this.createBitOperation(BitLogicOperator.AND, this.a, this.b);
		
		val coil1 = this.factory.createCoil(CoilKind.ASSIGNMENT);
		coil1.operand = this.x;
		
		val coil2 = this.factory.createCoil(CoilKind.ASSIGNMENT);
		coil2.operand = this.y;
		
		val branch = this.factory.createBranch(2);
		
		this.factory.createWire(branch.outputPorts.get(0), coil1.inPort);
		this.factory.createWire(branch.outputPorts.get(1), coil2.inPort);
		
		this.factory.createWire(bl.outPort, branch.inputPort);
		
		assertCodeMatches('''
			A #"A";
			A #"B";
			= %L0.0;
			A %L0.0;
		''', this.generator.translate(branch));
		
		assertCodeMatches('''
			A %L0.0;
			= #"X";
		''', this.generator.translate(coil1));
		
		assertCodeMatches('''
			A %L0.0;
			= #"Y";
		''', this.generator.translate(coil2));
	}
	
	@Test
	def testTranslateCoil() {
		// Coil { CoilKind kind, Symbol operand, Connectable input, boolean inputNegated, boolean outputNegated, String expected }
		this.checkTranslateCoil(CoilKind.ASSIGNMENT, this.x, this.a, /*inputNegated=*/false, /*outputNegated=*/false, '''
			A #"A";
			= #"X";
		''' );
		this.checkTranslateCoil( CoilKind.ASSIGNMENT, this.x, this.a, /*inputNegated=*/false, /*outputNegated=*/true, '''
			A #"A";
			= #"X";
			NOT;
		''');
		this.checkTranslateCoil( CoilKind.ASSIGNMENT, this.x, this.a, /*inputNegated=*/true, /*outputNegated=*/false, '''
			AN #"A";
			= #"X";
		''');
		this.checkTranslateCoil( CoilKind.NEGATED_ASSIGNMENT, this.x, this.a, /*inputNegated=*/false, /*outputNegated=*/false, '''
			A #"A";
			NOT;
			= #"X";
			NOT;
		''');
		this.checkTranslateCoil(CoilKind.NEGATED_ASSIGNMENT, this.x, this.a, /*inputNegated=*/false, /*outputNegated=*/true, '''
			A #"A";
			NOT;
			= #"X";
			NOT;
			NOT;
		''');
		this.checkTranslateCoil(CoilKind.SET, this.x, this.a, /*inputNegated=*/false, /*outputNegated=*/false, '''
			A #"A";
			S #"X";
		''');
		this.checkTranslateCoil(CoilKind.RESET, this.x, this.a, /*inputNegated=*/false, /*outputNegated=*/false, '''
			A #"A";
			R #"X";
		''');
	}
	
	@Test
	def testTranslateCompare() {
		// CompareOperator operator, MathDataType type, Value first, Value second, boolean outputNegated, CharSequence expected
		this.checkTranslateCompare(CompareOperator.EQ,  MathDataType.DINT, this.w1, this.w2, /*outNegated=*/false, '''
			L #"W1";
			L #"W2";
			==D;
		''');
		this.checkTranslateCompare(CompareOperator.NE,  MathDataType.INT, this.cInt, this.w2, /*outNegated=*/true, '''
			L 1234;
			L #"W2";
			<>I;
			NOT;
		''');
		this.checkTranslateCompare(CompareOperator.GE,  MathDataType.REAL, this.w1, this.w2, /*outNegated=*/false, '''
			L #"W1";
			L #"W2";
			>=R;
		''');
		this.checkTranslateCompare(CompareOperator.GT,  MathDataType.DINT, this.w1, this.w2, /*outNegated=*/true, '''
			L #"W1";
			L #"W2";
			>D;
			NOT;
		''');
		this.checkTranslateCompare(CompareOperator.LE,  MathDataType.INT, this.w1, this.w2, /*outNegated=*/false, '''
			L #"W1";
			L #"W2";
			<=I;
		''');
		this.checkTranslateCompare(CompareOperator.LT,  MathDataType.REAL, this.cFloat, this.w2, /*outNegated=*/true, '''
			L 3.14;
			L #"W2";
			<R;
			NOT;
		''');
	}
	
	@Test
	def testTranslateSimpleBitLogic() {
		this.checkTranslateBitLogic(BitLogicOperator.AND, /*in1Negated=*/false, /*in2Negated=*/false, /*outNegated=*/false, '''
			A #"A";
			A #"B";
		''');
		this.checkTranslateBitLogic(BitLogicOperator.AND, /*in1Negated=*/true,  /*in2Negated=*/false, /*outNegated=*/false, '''
			AN #"A";
			A #"B";
		''');
		this.checkTranslateBitLogic(BitLogicOperator.AND, /*in1Negated=*/true,  /*in2Negated=*/true, /*outNegated=*/false, '''
			AN #"A";
			AN #"B";
		''');
		this.checkTranslateBitLogic(BitLogicOperator.AND, /*in1Negated=*/true,  /*in2Negated=*/true, /*outNegated=*/true, '''
			AN #"A";
			AN #"B"; 
			NOT;
		''');
		this.checkTranslateBitLogic(BitLogicOperator.OR, /*in1Negated=*/false, /*in2Negated=*/false, /*outNegated=*/false, '''
			O #"A";
			O #"B";
		''');
		this.checkTranslateBitLogic(BitLogicOperator.XOR, /*in1Negated=*/false, /*in2Negated=*/false, /*outNegated=*/false, '''
			X #"A";
			X #"B";
		''');
		this.checkTranslateBitLogic(BitLogicOperator.XOR, /*in1Negated=*/true, /*in2Negated=*/false, /*outNegated=*/false, '''
			XN #"A";
			X #"B";
		''');
		this.checkTranslateBitLogic(BitLogicOperator.XOR, /*in1Negated=*/true, /*in2Negated=*/false, /*outNegated=*/false, '''
			XN #"A";
			X #"B";
		''');
	}
	
	private def BitLogicInstruction createBitOperation(BitLogicOperator operator, Connectable in1, Connectable in2, Port connectedTo) {
		val inst = this.createBitOperation(operator, in1, in2);
		this.factory.createWire(inst.outPort, connectedTo);				
		
		return inst;
	}
	
	private def BitLogicInstruction createBitOperation(BitLogicOperator operator, Connectable in1, Connectable in2) {		
		val inst = this.factory.createBitLogicInstruction(operator, 2);
		inst.getInPort(1).connection = in1;
		inst.getInPort(2).connection = in2;
		
		return inst;
	}
}