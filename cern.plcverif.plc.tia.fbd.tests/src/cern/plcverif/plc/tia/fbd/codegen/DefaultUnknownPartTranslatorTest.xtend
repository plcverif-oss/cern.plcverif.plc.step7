/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.codegen

import cern.plcverif.plc.tia.fbd.BitLogicOperator
import cern.plcverif.plc.tia.fbd.NetworkElementFactory
import cern.plcverif.plc.tia.fbd.PortDirection
import cern.plcverif.plc.tia.fbd.Symbol
import org.junit.Before
import org.junit.Test

import static cern.plcverif.plc.tia.fbd.utils.CodeAssert.*
import cern.plcverif.plc.tia.fbd.InstanceScope

class DefaultUnknownPartTranslatorTest {
	
	NetworkElementFactory factory;
	DefaultUnknownPartTranslator unknownPartTranslator;
	StlCodeGenerator generator;	
	
	Symbol a;
	Symbol b;
	Symbol c;
	Symbol d;
	
	@Before
	def void setUp() {
		this.factory = NetworkElementFactory.create();
		this.unknownPartTranslator = new DefaultUnknownPartTranslator();
		this.generator = new StlCodeGenerator(this.unknownPartTranslator);
				
		this.a = this.factory.createSymbol("#\"A\"");
		this.b = this.factory.createSymbol("#\"B\"");
		this.c = this.factory.createSymbol("#\"C\"");
		this.d = this.factory.createSymbol("#\"D\"");
	}
	
	@Test
	def void testTranslateUnknownPartToCall() {
		val in1 = this.factory.createBitLogicInstruction(BitLogicOperator.AND, 2);
		in1.getInPort(1).connection = this.a;
		in1.getInPort(2).connection = this.b;
		
		val in2 = this.factory.createBitLogicInstruction(BitLogicOperator.OR, 2);
		in2.getInPort(1).connection = this.c;
		in2.getInPort(2).connection = this.d;
		
		val in3 = this.factory.createBitLogicInstruction(BitLogicOperator.XOR, 3);
		in3.getInPort(1).connection = this.a;
		in3.getInPort(2).connection = this.b;
		in3.getInPort(3).connection = this.c;
		
		val part = this.factory.createUnknownPart("TOTALLY_UNKNOWN");
		this.factory.createAttribute(part, "Scope", InstanceScope.LOCAL_VARIABLE.toString);
		this.factory.createAttribute(part, "Instance", "TOTALLY_UNKNOWN_Instance");
		this.factory.createPort("en", part, PortDirection.CONTROL_FLOW_INPUT);
		this.factory.createPort("eno", part, PortDirection.CONTROL_FLOW_OUTPUT);
		
		this.factory.createPort("in1", part, PortDirection.PARAMETER_INPUT);
		this.factory.createPort("in2", part, PortDirection.PARAMETER_INPUT);
		this.factory.createPort("in3", part, PortDirection.PARAMETER_INPUT);
		
		this.factory.createWire(in1.outPort, part.getPort("in1"));
		this.factory.createWire(in2.outPort, part.getPort("in2"));
		this.factory.createWire(in3.outPort, part.getPort("in3"));
		
		val expected = '''
			A #"A";
			A #"B";
			= %L0.0;
			O #"C";
			O #"D";
			= %L0.1;
			X #"A";
			X #"B";
			X #"C";
			= %L0.2;
			CALL "TOTALLY_UNKNOWN_Instance"(
				"in1" := %L0.0,
				"in2" := %L0.1,
				"in3" := %L0.2
			);
		''';
		
		assertCodeMatches(expected, this.unknownPartTranslator.translate(part, ""));
	}
	
}