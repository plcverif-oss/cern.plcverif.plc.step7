/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.EList;
import org.junit.Test;

import cern.plcverif.plc.tia.fbd.BitLogicInstruction;
import cern.plcverif.plc.tia.fbd.BitLogicOperator;
import cern.plcverif.plc.tia.fbd.Block;
import cern.plcverif.plc.tia.fbd.BlockType;
import cern.plcverif.plc.tia.fbd.Coil;
import cern.plcverif.plc.tia.fbd.CoilKind;
import cern.plcverif.plc.tia.fbd.Function;
import cern.plcverif.plc.tia.fbd.InterfaceSection;
import cern.plcverif.plc.tia.fbd.Member;
import cern.plcverif.plc.tia.fbd.Network;
import cern.plcverif.plc.tia.fbd.NetworkElement;
import cern.plcverif.plc.tia.fbd.Symbol;
import cern.plcverif.plc.tia.fbd.Wire;

public class FbdXmlParserTest {

	
	@Test
	public void testFunctionInterface() {
		validateFunctionInterface(parseTestCase("FunctionInterface.xml", TiaPortalVersion.V15));
		validateFunctionInterface(parseTestCase("FunctionInterface.xml", TiaPortalVersion.V15_1));
	}

	private void validateFunctionInterface(Block block) {
		assertEquals(BlockType.FUNCTION, block.getBlockType());
		assertTrue(block instanceof Function);
		
		assertEquals("Int", ((Function) block).getReturnType());
		
		// Input, Output, InOut, Temp, Constant, Return
		assertEquals(6, block.getSections().size());
		
		InterfaceSection input = block.getInterfaceSection("Input");
		
		assertMember(input, "A", "Bool");
		assertMember(input, "B", "Word");
		assertMember(input, "C", "Array[0..5] of Byte");
		assertEquals(3, input.getMembers().size());
		
		InterfaceSection output = block.getInterfaceSection("Output");
		
		assertMember(output, "X", "Bool");
		assertMember(output, "Y", "Word");
		assertEquals(2, output.getMembers().size());
		
		InterfaceSection inout = block.getInterfaceSection("InOut");
		
		assertMember(inout, "P", "Bool");
		assertMember(inout, "Q", "Word");
		assertEquals(2,  inout.getMembers().size());

		InterfaceSection temp = block.getInterfaceSection("Temp");
		assertMember(temp, "T1", "Bool");
		assertMember(temp, "T2", "Word");
		assertEquals(2,  temp.getMembers().size());
		
		InterfaceSection constant = block.getInterfaceSection("Constant");
		assertMember(constant, "C1", "Bool", "");
		assertMember(constant, "C2", "Word", "16#FEFE");
		assertMember(constant, "C3", "Word", "");
		assertMember(constant, "C4", "Bool", "true");
		assertEquals(4,  constant.getMembers().size());
		
		InterfaceSection ret = block.getInterfaceSection("Return");
		assertMember(ret, "Ret_Val", "Int");
	}
	
	@Test
	public void testFunctionBlockInterface() {
		assertEquals(BlockType.FUNCTION_BLOCK, parseTestCase("FunctionBlockInterface.xml", TiaPortalVersion.V15).getBlockType());
		assertEquals(BlockType.FUNCTION_BLOCK, parseTestCase("FunctionBlockInterface.xml", TiaPortalVersion.V15_1).getBlockType());
	}	
	
	@Test
	public void testBitLogicAssignment() {
		validateBitLogicAssignment(parseTestCase("BitLogic.xml", TiaPortalVersion.V15));
	}

	private void validateBitLogicAssignment(Block block) {
		Network network = block.getNetworks().get(0);
		
		// Symbol(#A), Symbol(#P), Coil(ASSIGNMENT)
		EList<NetworkElement> elements = network.getElements();
		assertEquals(3, elements.size());
		
		List<Coil> coils = elementsOfType(elements, Coil.class)
			.collect(Collectors.toList());
		
		assertEquals(1, coils.size());		
		Coil assignment = coils.get(0);
		
		// #P := #A
		assertEquals(CoilKind.ASSIGNMENT, assignment.getKind());
		
		Symbol symA = getSymbol(elements, "#\"A\"");
		Symbol symP = getSymbol(elements, "#\"P\"");
		
		assertSame(symA, assignment.getInPort().getConnection());
		assertSame(symP, assignment.getOperand());
	}
	
	@Test
	public void testBitLogicAnd() {
		validateBitLogicAnd(parseTestCase("BitLogic.xml", TiaPortalVersion.V15));
		validateBitLogicAnd(parseTestCase("BitLogic.xml", TiaPortalVersion.V15_1));
	}

	private void validateBitLogicAnd(Block block) {
		Network network = block.getNetworks().get(1);
		
		// Symbol(#A), Symbol(#B), Symbol(#P), Coil(ASSIGNMENT), AND(), Wire()
		EList<NetworkElement> elements = network.getElements();
		assertEquals(6, elements.size());
		
		BitLogicInstruction bl = elementsOfType(elements, BitLogicInstruction.class).findAny().get();
		Coil coil = elementsOfType(elements, Coil.class).findAny().get();
		
		assertEquals(BitLogicOperator.AND, bl.getOperator());
		
		Symbol symA = getSymbol(elements, "#\"A\"");
		Symbol symB = getSymbol(elements, "#\"B\"");
		Symbol symP = getSymbol(elements, "#\"P\"");
		
		assertSame(symA, bl.getPort("in1").getConnection());
		assertSame(symB, bl.getPort("in2").getConnection());
		
		Wire wire = elementsOfType(elements, Wire.class).findAny().get();
		
		assertSame(wire, bl.getOutPort().getConnection());
		assertSame(bl.getOutPort(), wire.getSource());

		assertSame(coil.getInPort(), wire.getTarget());
		assertSame(wire, coil.getInPort().getConnection());		
		
		assertSame(symP, coil.getOperand());
	}
	
	private static Symbol getSymbol(Collection<? extends NetworkElement> elements, String name) {
		Optional<Symbol> symbol = elementsOfType(elements, Symbol.class, s -> s.getName().equals(name)).findAny();		
		assertTrue(symbol.isPresent());
		
		return symbol.get();
	}
	
	private static <T extends NetworkElement> Stream<T> elementsOfType(Collection<? extends NetworkElement> elements, Class<T> cls) {
		return elementsOfType(elements, cls, e -> true);
	}
	
	private static <T extends NetworkElement> Stream<T> elementsOfType(Collection<? extends NetworkElement> elements, Class<T> cls, Predicate<T> pred) {
		return elements.stream()
			.filter(e -> cls.isInstance(e))
			.map(e -> cls.cast(e))
			.filter(e -> pred.test(e));
	}
	
	private static void assertMember(InterfaceSection section, String name, String dataType, String startValue) {
		Member member = section.getMember(name);
		assertEquals(name, member.getName());
		assertEquals(dataType, member.getDataType());
		
		if (startValue != null) {
			assertEquals(startValue, member.getStartValue());
		}
	}
	
	private static void assertMember(InterfaceSection section, String name, String dataType) {
		assertMember(section, name, dataType, null);
	}
	
	
	private static Block parseTestCase(String name, TiaPortalVersion version) {
		Path path = Paths.get("", "resources/blocks", version.toString().toLowerCase(), name).toAbsolutePath();

		return FbdXmlParser.parse(path.toFile().toString(), version);
	}
	
}