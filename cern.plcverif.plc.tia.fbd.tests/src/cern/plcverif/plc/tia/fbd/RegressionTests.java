/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *   Xaver Fink - add support for variables of type struct
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Test;

import cern.plcverif.plc.tia.fbd.codegen.BlockToStl;
import cern.plcverif.plc.tia.fbd.codegen.OutputWiresTerminalFinderStrategy;
import cern.plcverif.plc.tia.fbd.parser.FbdXmlParser;
import cern.plcverif.plc.tia.fbd.parser.TiaPortalVersion;
import cern.plcverif.plc.tia.fbd.utils.CodeAssert;

public class RegressionTests {
	
	@Test
	public void regressionTest() throws IOException {
		this.runRegressionTest("CallInputParams", TiaPortalVersion.V15);
		this.runRegressionTest("Jumps", TiaPortalVersion.V15_1);
		this.runRegressionTest("AddBlock", TiaPortalVersion.V15_1);
		this.runRegressionTest("JumpConditions", TiaPortalVersion.V16);
		this.runRegressionTest("StructVariable", TiaPortalVersion.V17);
		this.runRegressionTest("BitLogicOps", TiaPortalVersion.V17);
		this.runRegressionTest("LocalDatablock", TiaPortalVersion.V17);
		this.runRegressionTest("DBInstance", TiaPortalVersion.V17);
		this.runRegressionTest("GlobalDB", TiaPortalVersion.V17);
	}
	
	private void runRegressionTest(String name, TiaPortalVersion version) throws IOException {
		String path = Paths.get("", "resources", "regression").toAbsolutePath().toString();
		
		String expected = new String(Files.readAllBytes(Paths.get(path, name + ".awl")));
		
		Block block = FbdXmlParser.parse(Paths.get(path, name + ".xml").toString(), version); 
		String actual = BlockToStl.translate(block, new OutputWiresTerminalFinderStrategy());
		
		CodeAssert.assertCodeMatches(expected, actual);
	}
	
}
