/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.utils;

import java.util.List;

import com.google.common.base.Splitter;
import static org.junit.Assert.assertEquals;

public final class CodeAssert {
	
	private CodeAssert() {}

	public static void assertCodeMatches(CharSequence expected, CharSequence actual) {
		Splitter splitter = Splitter.on(System.lineSeparator())
			.trimResults()
			.omitEmptyStrings();
		
		List<String> expectedLines = splitter.splitToList(expected);
		List<String> actualLines = splitter.splitToList(actual);

		assertEquals(String.join(System.lineSeparator(), expectedLines), String.join(System.lineSeparator(), actualLines));
	}
	
}
