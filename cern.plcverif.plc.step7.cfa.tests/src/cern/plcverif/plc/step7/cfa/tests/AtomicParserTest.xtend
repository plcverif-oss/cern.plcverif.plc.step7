/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.tests

import cern.plcverif.base.common.logging.IPlcverifLogger
import cern.plcverif.base.interfaces.exceptions.AtomParsingException
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.cfa.cfadeclaration.Indexing
import cern.plcverif.base.models.expr.BoolLiteral
import cern.plcverif.base.models.expr.FloatLiteral
import cern.plcverif.base.models.expr.IntLiteral
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.UnknownType
import cern.plcverif.plc.step7.cfa.Step7CodeToCfa
import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.tests.Step7LanguageInjectorProvider
import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(XtextRunner)
@InjectWith(Step7LanguageInjectorProvider)
class AtomicParserTest {
	@Inject
	ParseHelper<ProgramFile> parseHelper

	static final CharSequence EXAMPLE_CODE = '''
		FUNCTION_BLOCK fb1
		VAR
			a : BOOL;
			s : STRUCT
				b : INT;
				END_STRUCT;
			arr : ARRAY[0..2] OF BOOL;
		END_VAR
		BEGIN
			a := true;
			s.b := 5;
			I0.0 := false;
		END_FUNCTION_BLOCK
		
		DATA_BLOCK instance fb1
		BEGIN
		END_DATA_BLOCK
		
		DATA_BLOCK db1 fb1
		BEGIN
		END_DATA_BLOCK
	''';

	@Test
	def parseNamedElementTest() {
		val atomicParser = atomicParserFor(EXAMPLE_CODE);

		// Try to access 'instance.a'
		val fieldA = atomicParser.parseAtom("instance.a");
		Assert.assertNotNull(fieldA);
		Assert.assertTrue(fieldA instanceof FieldRef);
		Assert.assertTrue((fieldA as FieldRef).field.name.equals("a"));
		val fieldAPrefix = (fieldA as FieldRef).prefix;
		Assert.assertTrue(fieldAPrefix instanceof FieldRef);
		Assert.assertTrue((fieldAPrefix as FieldRef).field.name.equals("instance"));
		Assert.assertNull(fieldAPrefix.prefix);
		
		// Try to access 'db1.a'
		val fieldDb1A = atomicParser.parseAtom("db1.a");
		Assert.assertNotNull(fieldDb1A);
		Assert.assertTrue(fieldDb1A instanceof FieldRef);
		Assert.assertTrue((fieldDb1A as FieldRef).field.name.equals("a"));
		val fieldDb1APrefix = (fieldDb1A as FieldRef).prefix;
		Assert.assertTrue(fieldDb1APrefix instanceof FieldRef);
		Assert.assertTrue((fieldDb1APrefix as FieldRef).field.name.equals("db1"));
		Assert.assertNull(fieldDb1APrefix.prefix);
		
		// Try to access 'instance.s.b'
		val fieldSb = atomicParser.parseAtom("instance.s.b");
		Assert.assertNotNull(fieldSb);
		Assert.assertTrue(fieldSb instanceof FieldRef);
		Assert.assertTrue((fieldSb as FieldRef).field.name.equals("b"));
		val fieldS = (fieldSb as FieldRef).prefix;
		Assert.assertTrue(fieldS instanceof FieldRef);
		Assert.assertTrue((fieldS as FieldRef).field.name.equals("s"));
		val fieldSPrefix = (fieldS as FieldRef).prefix;
		Assert.assertTrue(fieldSPrefix instanceof FieldRef);
		Assert.assertTrue((fieldSPrefix as FieldRef).field.name.equals("instance"));
		Assert.assertNull(fieldSPrefix.prefix);
		
		// Try to access 'instance.arr[2]'
		val fieldArr1 = atomicParser.parseAtom("instance.arr[2]");
		Assert.assertNotNull(fieldArr1);
		Assert.assertTrue(fieldArr1 instanceof Indexing);
		Assert.assertTrue((fieldArr1 as Indexing).index instanceof IntLiteral);
		Assert.assertTrue(((fieldArr1 as Indexing).index as IntLiteral).value == 2);
		val fieldArr = (fieldArr1 as Indexing).prefix;
		Assert.assertTrue(fieldArr instanceof FieldRef);
		Assert.assertTrue((fieldArr as FieldRef).field.name.equals("arr"));
		Assert.assertNotNull(fieldArr.prefix);
		Assert.assertTrue(fieldArr.prefix instanceof FieldRef);
		val fieldArrPrefix = fieldArr.prefix as FieldRef;
		Assert.assertTrue(fieldArrPrefix.field.name.equals("instance"));
		Assert.assertNull(fieldArrPrefix.prefix);
		
		// Try to access 'I0.0'
		val fieldI00 = atomicParser.parseAtom("I0.0");
		Assert.assertNotNull(fieldI00);
		Assert.assertTrue(fieldI00 instanceof FieldRef);
		Assert.assertTrue((fieldI00 as FieldRef).field.displayName.equalsIgnoreCase("IX0.0"));
	}
	
	@Test
	def parseLiteralTest() {
		val atomicParser = atomicParserFor(EXAMPLE_CODE);
		
		val literal123 = atomicParser.parseAtom("123");
		Assert.assertTrue(literal123 instanceof IntLiteral);
		Assert.assertEquals(123, (literal123 as IntLiteral).value);
		Assert.assertTrue(literal123.type instanceof UnknownType);
		
		val literal1 = atomicParser.parseAtom("1");
		Assert.assertTrue(literal1 instanceof IntLiteral);
		Assert.assertEquals(1, (literal1 as IntLiteral).value);
		Assert.assertTrue(literal1.type instanceof UnknownType);
		
		val literalInt234 = atomicParser.parseAtom("INT#234");
		Assert.assertTrue(literalInt234 instanceof IntLiteral);
		Assert.assertTrue((literalInt234 as IntLiteral).value == 234);
		Assert.assertTrue(literalInt234.type instanceof IntType);
		Assert.assertTrue((literalInt234.type as IntType).bits == 16);
		
		val literalFloatPi = atomicParser.parseAtom("3.1415");
		Assert.assertTrue(literalFloatPi instanceof FloatLiteral);
		Assert.assertEquals(3.1415, (literalFloatPi as FloatLiteral).value, 0.0001);
		
		val literalTrue = atomicParser.parseAtom("TrUe");
		Assert.assertTrue(literalTrue instanceof BoolLiteral);
		Assert.assertEquals(true, (literalTrue as BoolLiteral).value);
	}
	
	
	@Test(expected = AtomParsingException)
	def void parseInvalidLiteralTest1() {
		atomicParserFor(EXAMPLE_CODE).parseAtom("13rqewgs13");
	}
	
	@Test(expected = AtomParsingException)
	def void parseInvalidNamedTest1() {
		atomicParserFor(EXAMPLE_CODE).parseAtom("xxx");
	}
	
	@Test(expected = AtomParsingException)
	def void parseInvalidNamedTest2() {
		atomicParserFor(EXAMPLE_CODE).parseAtom("instance.a.xxx");
	}
	
	@Test(expected = AtomParsingException)
	def void parseInvalidNamedTest3() {
		atomicParserFor(EXAMPLE_CODE).parseAtom("instance.s.xxx");
	}
	
	@Test(expected = AtomParsingException)
	def void parseInvalidNamedTest4() {
		atomicParserFor(EXAMPLE_CODE).parseAtom("instance.s.b.xxx");
	}
	
	@Test(expected = AtomParsingException)
	def void parseInvalidNamedTest5() {
		atomicParserFor(EXAMPLE_CODE).parseAtom("instance.a[5]");
	}
	
	@Test(expected = AtomParsingException)
	def void parseInvalidNamedTest6() {
		atomicParserFor(EXAMPLE_CODE).parseAtom("I0.5");
	}
	
	private def atomicParserFor(CharSequence code){
		val program = parseHelper.parse(code);
		val result = Step7CodeToCfa.convert(IPlcverifLogger.NULL_LOGGER, program);
		return result.atomicParser;
	}
}
