/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink - new tests
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.tests

import cern.plcverif.base.common.logging.IPlcverifLogger
import cern.plcverif.base.common.utils.IoUtils
import cern.plcverif.base.models.cfa.serialization.CfaToXmi
import cern.plcverif.base.models.cfa.validation.CfaValidation
import cern.plcverif.plc.step7.cfa.Step7CodeToCfa
import cern.plcverif.plc.step7.step7Language.ProgramFile
import cern.plcverif.plc.step7.tests.Step7LanguageInjectorProvider
import com.google.inject.Inject
import org.eclipse.xtext.diagnostics.Severity
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.util.CancelIndicator
import org.eclipse.xtext.validation.CheckMode
import org.eclipse.xtext.validation.IResourceValidator
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import java.io.File
import java.nio.file.Files
import org.junit.BeforeClass
import java.io.IOException
import java.nio.file.Paths

@RunWith(XtextRunner)
@InjectWith(Step7LanguageInjectorProvider)
class BatchStep7ToCfaTest {
	
	public static final boolean CONTINUE_ON_ASSERT_VIOLATION = false; // NOPMD
	public static boolean writeActualOutputs = CONTINUE_ON_ASSERT_VIOLATION;
	
	private static String fileOutputDir;

	@Inject
	ParseHelper<ProgramFile> parseHelper

	@Inject IResourceValidator resourceValidator
	
	@BeforeClass
	def static void setup() throws IOException {
		fileOutputDir = Files.createDirectories(Paths.get("cfd-output")).toString();
	}

	@Test
	def void testAllScl() {
		test("/testcases/scl/valid/RetainVariables");
		test("/testcases/scl/valid/BasicCode");
		test("/testcases/scl/valid/BuiltinFunctions");
		test("/testcases/scl/valid/CompoundDataStructures");
		test("/testcases/scl/valid/ExtractNestedCalls");
		test("/testcases/scl/valid/FloatType");
		test("/testcases/scl/valid/ImplicitFbInstance");
		test("/testcases/scl/valid/NamedConstants");
		test("/testcases/scl/valid/SclCaseStatement");
		test("/testcases/scl/valid/SclExpressions");
		test("/testcases/scl/valid/SclForStatement");
		test("/testcases/scl/valid/SclGotoStatement");
		test("/testcases/scl/valid/SclIfStatement");
		test("/testcases/scl/valid/SclRepeatStatement");
		test("/testcases/scl/valid/SclReturnStatement");
		test("/testcases/scl/valid/SclSubroutineCall");
		test("/testcases/scl/valid/SclSubroutineCallParams");
		test("/testcases/scl/valid/SclWhileStatement");
		test("/testcases/scl/valid/SclRegionStatement");
		test("/testcases/scl/valid/SimpleCode");
		test("/testcases/scl/valid/TimeHandling");
		test("/testcases/scl/valid/TypeConversion");
		test("/testcases/scl/valid/VariableView");
		test("/testcases/scl/valid/VariableView2");
		test("/testcases/scl/valid/Variables");
		test("/testcases/scl/valid/VerificationAssertion");
	}
	
	@Test
	def void testAllStl() {
		test("/testcases/stl/valid/StlAccuStatements");
		test("/testcases/stl/valid/StlAndBeforeOr");
		test("/testcases/stl/valid/StlBasicLogicStatements");
		test("/testcases/stl/valid/StlCallStatements");
		test("/testcases/stl/valid/StlComparisonStatements");
		test("/testcases/stl/valid/StlEdgeStatements");
		test("/testcases/stl/valid/StlMathToCfa");
		test("/testcases/stl/valid/StlMemoryAddresses");
		test("/testcases/stl/valid/StlNestingStack");
		test("/testcases/stl/valid/StlNestingStack2");
		test("/testcases/stl/valid/StlJumpToLabels");
		test("/testcases/stl/valid/StlJumps");
		test("/testcases/stl/valid/StlSubroutineCall");
		test("/testcases/stl/valid/StlWordLogicStatements");
		test("/testcases/stl/valid/StlShiftInstructions");
	}

	private def void test(String pathBase) {
		print('''Testing (AST to CFA) «pathBase»... ''');

		// Load SCL file and reference CFA representation
		val sclFile = IoUtils.readResourceFile(pathBase + ".scl", this.class.classLoader);
		val expectedCfaSerialized = IoUtils.readResourceFile(pathBase + ".scl.cfd", this.class.classLoader);
		Assert.assertNotNull(sclFile);
		Assert.assertNotNull(expectedCfaSerialized);

		// Parse SCL file to AST
		val parsedScl = parseHelper.parse(sclFile);
		Assert.assertNotNull(parsedScl);
		val validationIssues = resourceValidator.validate(parsedScl.eResource, CheckMode.ALL, CancelIndicator.NullImpl);
		val validationErrors = validationIssues.filter[it.severity == Severity.ERROR];
		validationErrors.forEach[System.err.println('''Validation error in «pathBase»: «it»''')];
		Assert.assertTrue('''The SCL file '«pathBase».scl' contains errors.''', validationErrors.isEmpty);

		// Build golden CFA
		// val cfaGolden = XmiToCfa.deserializeFromString(goldenCfaSerialized);
		
		// Represent AST in CFA
		//val networkName = pathBase.split("/").last;
		val cfaFromAst = Step7CodeToCfa.convert(IPlcverifLogger.NULL_LOGGER, parsedScl).cfa;
		CfaValidation.validate(cfaFromAst);

		// Compare the two CFA EMF object trees
		// Assert.assertTrue(EcoreUtil.equals(cfaGolden, cfaFromAst));
		
		// Textual comparison (as it is easier to check manually than XMI diffs)
//		val toText = new CfaToText();
//		val expectedCfa = XmiToCfa.deserializeFromString(expectedCfaSerialized);
//		val expectedText = EmfModelPrinter.print(expectedCfa, toText);
//		val actualText = EmfModelPrinter.print(cfaFromAst, toText);
//		Assert.assertEquals(expectedText, actualText);	
		
		// XMI comparison
		val cfaFromAstInXmi = CfaToXmi.serializeToString(cfaFromAst);
		Assert.assertEquals('''The AST of SCL file '«pathBase».scl' does not match the expectations.''', expectedCfaSerialized.trim,
			cfaFromAstInXmi.trim);
		println("OK");
		if (writeActualOutputs) {
			val writeFile = new File(fileOutputDir, String.format("%s.scl.cfd" , pathBase.split("/").get(4)));
			IoUtils.writeAllContent(writeFile, cfaFromAstInXmi);
		}
		
		if(!CONTINUE_ON_ASSERT_VIOLATION){
			Assert.assertEquals('''The AST of SCL file '«pathBase».scl' does not match the expectations.''', expectedCfaSerialized.trim,
				cfaFromAstInXmi.trim);
			println("OK");
		}
	}
	
	@Test
	def void sentinel() {
		Assert.assertFalse(CONTINUE_ON_ASSERT_VIOLATION);
	}
}
