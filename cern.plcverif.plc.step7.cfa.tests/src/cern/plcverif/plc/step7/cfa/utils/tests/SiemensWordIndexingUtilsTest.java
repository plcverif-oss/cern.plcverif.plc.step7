/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.plc.step7.cfa.utils.tests;

import static cern.plcverif.plc.step7.cfa.util.SiemensWordIndexingUtils.arrayIndexToWordIndex;
import static cern.plcverif.plc.step7.cfa.util.SiemensWordIndexingUtils.wordWithAllOnes;
import static cern.plcverif.plc.step7.cfa.util.SiemensWordIndexingUtils.wordWithSingleOne;
import static cern.plcverif.plc.step7.cfa.util.SiemensWordIndexingUtils.wordWithSingleZero;

import org.junit.Assert;
import org.junit.Test;

import cern.plcverif.base.models.expr.ExpressionSafeFactory;
import cern.plcverif.base.models.expr.IntLiteral;
import cern.plcverif.base.models.expr.IntType;

public class SiemensWordIndexingUtilsTest {
	private static final IntType UINT8_TYPE = ExpressionSafeFactory.INSTANCE.createIntType(false, 8);
	private static final IntType UINT16_TYPE = ExpressionSafeFactory.INSTANCE.createIntType(false, 16);
	private static final IntType UINT32_TYPE = ExpressionSafeFactory.INSTANCE.createIntType(false, 32);

	@Test
	public void wordWithSingleOneTest() {
		wordWithSingleOneTest(0, UINT8_TYPE, 0b1);
		wordWithSingleOneTest(1, UINT8_TYPE, 0b10);
		wordWithSingleOneTest(2, UINT8_TYPE, 0b100);
		wordWithSingleOneTest(7, UINT8_TYPE, 0b1000_0000);

		wordWithSingleOneTest(0, UINT16_TYPE, 0b1);
		wordWithSingleOneTest(1, UINT16_TYPE, 0b10);
		wordWithSingleOneTest(7, UINT16_TYPE, 0b1000_0000);
		wordWithSingleOneTest(8, UINT16_TYPE, 0b1_0000_0000);
		wordWithSingleOneTest(15, UINT16_TYPE, 0b1000_0000_0000_0000);

		wordWithSingleOneTest(0, UINT32_TYPE, 0b1);
		wordWithSingleOneTest(1, UINT32_TYPE, 0b10);
		wordWithSingleOneTest(7, UINT32_TYPE, 0b1000_0000);
		wordWithSingleOneTest(8, UINT32_TYPE, 0b1_0000_0000);
		wordWithSingleOneTest(15, UINT32_TYPE, 0b1000_0000_0000_0000);
		wordWithSingleOneTest(30, UINT32_TYPE, 1073741824L); // 2^30
		wordWithSingleOneTest(31, UINT32_TYPE, 2147483648L); // 2^31
	}

	private static void wordWithSingleOneTest(long index, IntType type, long expectedValue) {
		String execution = String.format("wordWithSingleOneTest(%s,%s)", Long.toString(index), type.toString());

		IntLiteral result = wordWithSingleOne(index, type);
		Assert.assertTrue("The result is not of the given type. " + execution, result.getType().dataTypeEquals(type));
		Assert.assertEquals("The expected value does not match the actual. " + execution, expectedValue,
				result.getValue());
	}

	@Test
	public void wordWithSingleZeroTest() {
		wordWithSingleZeroTest(0, UINT8_TYPE, 0b1111_1110);
		wordWithSingleZeroTest(1, UINT8_TYPE, 0b1111_1101);
		wordWithSingleZeroTest(2, UINT8_TYPE, 0b1111_1011);
		wordWithSingleZeroTest(7, UINT8_TYPE, 0b0111_1111);

		wordWithSingleZeroTest(0, UINT16_TYPE, 0b1111_1111__1111_1110);
		wordWithSingleZeroTest(1, UINT16_TYPE, 0b1111_1111__1111_1101);
		wordWithSingleZeroTest(7, UINT16_TYPE, 0b1111_1111__0111_1111);
		wordWithSingleZeroTest(8, UINT16_TYPE, 0b1111_1110__1111_1111);
		wordWithSingleZeroTest(15, UINT16_TYPE, 0b0111_1111__1111_1111);

		wordWithSingleZeroTest(0, UINT32_TYPE, 0b1111_1111__1111_1111___1111_1111__1111_1110L);
		wordWithSingleZeroTest(1, UINT32_TYPE, 0b1111_1111__1111_1111___1111_1111__1111_1101L);
		wordWithSingleZeroTest(7, UINT32_TYPE, 0b1111_1111__1111_1111___1111_1111__0111_1111L);
		wordWithSingleZeroTest(8, UINT32_TYPE, 0b1111_1111__1111_1111___1111_1110__1111_1111L);
		wordWithSingleZeroTest(15, UINT32_TYPE, 0b1111_1111__1111_1111___0111_1111__1111_1111L);
		wordWithSingleZeroTest(30, UINT32_TYPE, 0b1011_1111__1111_1111___1111_1111__1111_1111L);
		wordWithSingleZeroTest(31, UINT32_TYPE, 0b0111_1111__1111_1111___1111_1111__1111_1111L);
	}

	private static void wordWithSingleZeroTest(long index, IntType type, long expectedValue) {
		String execution = String.format("wordWithSingleZeroTest(%s,%s)", Long.toString(index), type.toString());

		IntLiteral result = wordWithSingleZero(index, type);
		Assert.assertTrue("The result is not of the given type. " + execution, result.getType().dataTypeEquals(type));
		Assert.assertEquals(expectedValue, result.getValue());
	}

	/**
	 * Checks the proper conversion between array indices and BYTE bit indices.
	 */
	@Test
	public void siemensByteTest() {
		for (int i = 0; i < 8; i++) {
			Assert.assertEquals(i, arrayIndexToWordIndex(i, UINT8_TYPE));
		}
	}

	/**
	 * Checks the proper conversion between array indices and WORD bit indices.
	 */
	@Test
	public void siemensWordTest() {
		Assert.assertEquals(0, arrayIndexToWordIndex(8, UINT16_TYPE));
		Assert.assertEquals(1, arrayIndexToWordIndex(9, UINT16_TYPE));
		Assert.assertEquals(2, arrayIndexToWordIndex(10, UINT16_TYPE));
		Assert.assertEquals(3, arrayIndexToWordIndex(11, UINT16_TYPE));
		Assert.assertEquals(4, arrayIndexToWordIndex(12, UINT16_TYPE));
		Assert.assertEquals(5, arrayIndexToWordIndex(13, UINT16_TYPE));
		Assert.assertEquals(6, arrayIndexToWordIndex(14, UINT16_TYPE));
		Assert.assertEquals(7, arrayIndexToWordIndex(15, UINT16_TYPE));
		Assert.assertEquals(8, arrayIndexToWordIndex(0, UINT16_TYPE));
		Assert.assertEquals(9, arrayIndexToWordIndex(1, UINT16_TYPE));
		Assert.assertEquals(10, arrayIndexToWordIndex(2, UINT16_TYPE));
		Assert.assertEquals(11, arrayIndexToWordIndex(3, UINT16_TYPE));
		Assert.assertEquals(12, arrayIndexToWordIndex(4, UINT16_TYPE));
		Assert.assertEquals(13, arrayIndexToWordIndex(5, UINT16_TYPE));
		Assert.assertEquals(14, arrayIndexToWordIndex(6, UINT16_TYPE));
		Assert.assertEquals(15, arrayIndexToWordIndex(7, UINT16_TYPE));
	}

	/**
	 * Checks the proper conversion between array indices and DWORD bit indices.
	 */
	@Test
	public void siemensDwordTest() {
		Assert.assertEquals(0, arrayIndexToWordIndex(24, UINT32_TYPE));
		Assert.assertEquals(7, arrayIndexToWordIndex(31, UINT32_TYPE));
		Assert.assertEquals(8, arrayIndexToWordIndex(16, UINT32_TYPE));
		Assert.assertEquals(15, arrayIndexToWordIndex(23, UINT32_TYPE));

		Assert.assertEquals(16, arrayIndexToWordIndex(8, UINT32_TYPE));
		Assert.assertEquals(23, arrayIndexToWordIndex(15, UINT32_TYPE));
		Assert.assertEquals(24, arrayIndexToWordIndex(0, UINT32_TYPE));
		Assert.assertEquals(31, arrayIndexToWordIndex(7, UINT32_TYPE));
	}

	@Test
	public void wordWithAllOnesTest() {
		Assert.assertEquals(0b1111_1111, wordWithAllOnes(UINT8_TYPE).getValue());
		Assert.assertEquals(0b1111_1111_1111_1111, wordWithAllOnes(UINT16_TYPE).getValue());
		Assert.assertEquals(0b1111_1111_1111_1111_1111_1111_1111_1111L, wordWithAllOnes(UINT32_TYPE).getValue());
	}
}
