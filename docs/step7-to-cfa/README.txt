# See: http://search.cpan.org/~tels/Graph-Easy/bin/graph-easy

graph-easy --from=dot --as=ascii --input "file.dot" --output "file.ascii.txt"
graph-easy --from=dot --as=boxart --input "file.dot" --output "file.boxart.txt"