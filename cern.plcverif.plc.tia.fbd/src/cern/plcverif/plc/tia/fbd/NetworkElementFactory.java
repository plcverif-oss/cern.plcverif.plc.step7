/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd;

import cern.plcverif.plc.tia.fbd.impl.FbdFactoryImpl;

public class NetworkElementFactory {

    private Network network;
    private FbdFactory emf;

    public NetworkElementFactory(Network network, FbdFactory emf) {
        this.network = network;
        this.emf = emf;
    }

    public static NetworkElementFactory create() {
    	FbdFactory emf = FbdFactoryImpl.init();
        Network network = emf.createNetwork();
        network.setOpen(emf.createOpenConnection());

        return new NetworkElementFactory(network, emf);
    }

    // Parts
    // -------------------------------------------------------------------------

    public BitLogicInstruction createBitLogicInstruction(BitLogicOperator operator, int cardinality) {
        BitLogicInstruction inst = this.emf.createBitLogicInstruction();
        inst.setOperator(operator);
        inst.setCardinality(cardinality);

        for (int i = 1; i <= cardinality; ++i) {
            this.createPort(String.format("in%d", i), inst, PortDirection.CONTROL_FLOW_INPUT);
        }
        this.createPort("out", inst, PortDirection.CONTROL_FLOW_OUTPUT);

        this.network.getElements().add(inst);

        return inst;
    }

    public Coil createCoil(CoilKind kind) {
        Coil coil = this.emf.createCoil();
        coil.setKind(kind);

        this.createPort("in", coil, PortDirection.CONTROL_FLOW_INPUT);
        this.createPort("out", coil, PortDirection.CONTROL_FLOW_OUTPUT);

        this.network.getElements().add(coil);

        return coil;
    }

    public CompareInstruction createCompare(CompareOperator operator, MathDataType dataType) {
        CompareInstruction cmp = this.emf.createCompareInstruction();
        cmp.setOperator(operator);
        cmp.setDataType(dataType);

        this.createPort("out", cmp, PortDirection.CONTROL_FLOW_OUTPUT);

        this.network.getElements().add(cmp);

        return cmp;
    }

    public MoveInstruction createMoveInstruction(int numOutputPorts) {
        MoveInstruction move = this.emf.createMoveInstruction();

        this.addEnAndEno(move);

        Port in = this.createPort("in", move, PortDirection.PARAMETER_INPUT);
        move.setInputPort(in);

        for (int i = 1; i <= numOutputPorts; ++i) {
            Port out = this.createPort(String.format("out%d", i), move, PortDirection.PARAMETER_OUTPUT);
            move.getOutputPorts().add(out);
        }

        this.network.getElements().add(move);

        return move;
    }

    // Accesses
    // -------------------------------------------------------------------------

    public Symbol createSymbol(String name) {
        Symbol symbol = this.emf.createSymbol();
        symbol.setName(name);
        this.network.getElements().add(symbol);

        return symbol;
    }

    public Constant createConstant(String value) {
        Constant constant = this.emf.createConstant();
        constant.setValue(value);
        this.network.getElements().add(constant);

        return constant;
    }

    public LabelRef createLabelRef(String label) {
        LabelRef ref = this.emf.createLabelRef();
        ref.setLabel(label);
        this.network.getElements().add(ref);

        return ref;
    }

    public Label createLabel(String name) {
        Label label = this.emf.createLabel();
        label.setName(name);
        this.network.getLabels().add(label);

        return label;
    }

    public Port createPort(String name, Instruction inst) {
        return this.createPort(name, inst, PortDirection.UNKNOWN);
    }

    public Port createPort(String name, Instruction inst, PortDirection direction) {
        Port port = this.emf.createPort();
        port.setName(name);
        port.setInstruction(inst);

        // The default connection is 'Open'
        port.setConnection(this.network.getOpen());
        port.setDirection(direction);

        return port;
    }

    public Wire createWire(Port source, Port target) {
        Wire wire = this.emf.createWire();
        wire.setSource(source);
        wire.setTarget(target);

        source.setConnection(wire);
        target.setConnection(wire);

        this.network.getElements().add(wire);

        return wire;
    }

    public Network getNetwork() {
        return network;
    }

    public FunctionBlockCall createFunctionBlockCall(String calledBlock, String instanceName) {
        FunctionBlockCall call = this.emf.createFunctionBlockCall();
        call.setName(calledBlock);
        call.setInstanceName(instanceName);

        this.addEnAndEno(call);
        this.network.getElements().add(call);

        return call;
    }

    public FunctionCall createFunctionCall(String calledBlock) {
        FunctionCall call = this.emf.createFunctionCall();
        call.setName(calledBlock);

        this.addEnAndEno(call);
        this.network.getElements().add(call);

        return call;
    }

    public UnknownPart createUnknownPart(String name) {
        UnknownPart part = this.emf.createUnknownPart();
        part.setName(name);

        this.network.getElements().add(part);

        return part;
    }

    public Branch createBranch(int numOutPorts) {
        Branch branch = this.emf.createBranch();

        Port input = this.createPort("in", branch, PortDirection.CONTROL_FLOW_INPUT);
        branch.setInputPort(input);
        for (int i = 1; i <= numOutPorts; ++i) {
            Port out = this.createPort(String.format("out%d",  i), branch, PortDirection.CONTROL_FLOW_OUTPUT);
            branch.getOutputPorts().add(out);
        }

        this.network.getElements().add(branch);

        return branch;
    }

    public Jump createJump(JumpKind kind) {
        Jump jump = this.emf.createJump();

        jump.setKind(kind);
        this.createPort("in", jump, PortDirection.CONTROL_FLOW_INPUT);

        this.network.getElements().add(jump);

        return jump;
    }

    public WordLogicInstruction createWordLogicInstruction(WordLogicOperator operator, WordDataType dataType, int cardinality) {
        WordLogicInstruction inst = this.emf.createWordLogicInstruction();
        inst.setDataType(dataType);
        inst.setOperator(operator);

        this.addEnAndEno(inst);

        for (int i = 1; i <= cardinality; ++i) {
            Port in = this.createPort(String.format("in%d", i), inst, PortDirection.PARAMETER_INPUT);
            inst.getInputPorts().add(in);
        }

        Port out = this.createPort("out", inst, PortDirection.PARAMETER_OUTPUT);
        inst.setOutPort(out);

        this.network.getElements().add(inst);

        return inst;
    }
    
    public AddInstruction createAddInstruction(AddOperation operator, MathDataType dataType, int cardinality) {
        AddInstruction inst = this.emf.createAddInstruction();
        inst.setDataType(dataType);
        inst.setOperator(operator);

        this.addEnAndEno(inst);

        for (int i = 1; i <= cardinality; ++i) {
            Port in = this.createPort(String.format("in%d", i), inst, PortDirection.PARAMETER_INPUT);
            inst.getInputPorts().add(in);
        }

        Port out = this.createPort("out", inst, PortDirection.PARAMETER_OUTPUT);
        inst.setOutPort(out);

        this.network.getElements().add(inst);

        return inst;
    }
    
    public DivInstruction createDivInstruction(DivOperation operator, MathDataType dataType) {
        DivInstruction inst = this.emf.createDivInstruction();
        inst.setDataType(dataType);
        inst.setOperator(operator);

        this.addEnAndEno(inst);

        Port in1 = this.createPort("in1", inst, PortDirection.PARAMETER_INPUT);
        inst.getInputPorts().add(in1);
        Port in2 = this.createPort("in2", inst, PortDirection.PARAMETER_INPUT);
        inst.getInputPorts().add(in2);

        Port out = this.createPort("out", inst, PortDirection.PARAMETER_OUTPUT);
        inst.setOutPort(out);

        this.network.getElements().add(inst);

        return inst;
    }

    public ReturnValueInstruction createReturnValue() {
        ReturnValueInstruction returnInst = this.emf.createReturnValueInstruction();

        this.createPort("in", returnInst, PortDirection.CONTROL_FLOW_INPUT);
        this.network.getElements().add(returnInst);

        return returnInst;
    }

    public ReturnInstruction createReturn(ReturnKind kind) {
        ReturnInstruction returnInst = this.emf.createReturnInstruction();
        returnInst.setKind(kind);

        this.createPort("in", returnInst, PortDirection.CONTROL_FLOW_INPUT);
        this.network.getElements().add(returnInst);

        return returnInst;
    }

    public Attribute createAttribute(Instruction inst, String name , String value) {
        Attribute attr = this.emf.createAttribute();

        inst.getAttributes().add(attr);
        attr.setName(name);
        attr.setValue(value);

        return attr;
    }

    private void addEnAndEno(Call call) {
        Port en = this.createPort("en", call, PortDirection.CONTROL_FLOW_INPUT);
        call.setEn(en);
        Port eno = this.createPort("eno", call, PortDirection.CONTROL_FLOW_OUTPUT);
        call.setEno(eno);
    }

}

