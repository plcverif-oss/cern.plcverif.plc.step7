/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd;

/**
 * Interface for finding more information about unknown parts.
 */
public interface UnknownPartResolver {

    /**
     * Creates the known ports of a given unknown part.
     * 
     * @return True, if the resolver was able to resolve the part and create its ports. False otherwise.
     */
    boolean createPortsFor(UnknownPart part);

}

