/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

public final class NetworkHelper {

    private NetworkHelper() {}

    public static Port getPort(Instruction inst, String portName) {
        Optional<Port> port = inst.getPorts().stream().filter(p -> p.getName().equalsIgnoreCase(portName)).findAny();
        if (!port.isPresent()) {
            throw new NoSuchElementException(String.format("Unknown port name '%s' in instruction '%s'.", portName, inst));
        }

        return port.get();
    }

    public static Port getOrInsertPort(Instruction inst, String portName, NetworkElementFactory factory) {
        Optional<Port> port = inst.getPorts().stream().filter(p -> p.getName().equalsIgnoreCase(portName)).findAny();
        if (!port.isPresent()) {
            return factory.createPort(portName, inst);
        }

        return port.get();
    }

    public static List<Port> portsBeginWith(Instruction inst, String prefix) {
        return inst.getPorts().stream()
            .filter(p -> p.getName().startsWith(prefix))
            .collect(Collectors.toList());
    }

    public static List<Port> argumentPorts(Call call) {
        return call.getPorts().stream()
            .filter(p -> p != call.getEn() && p != call.getEno())
            .filter(p -> !(p.getConnection() instanceof OpenConnection))
            .collect(Collectors.toList());
    }

}

