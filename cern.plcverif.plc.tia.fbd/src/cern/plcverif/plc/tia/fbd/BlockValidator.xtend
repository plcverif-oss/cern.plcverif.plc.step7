/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd;

import org.eclipse.emf.common.util.Diagnostic
import org.eclipse.emf.ecore.util.Diagnostician

class BlockValidator {
	
	private new() {}
	
	static def boolean validate(Block block, StringBuilder errors) {
		return new BlockValidator().validateBlock(block, errors);
	}
	
	private def boolean validateBlock(Block block, StringBuilder errors) {
		val diagnostic = Diagnostician.INSTANCE.validate(block);
		
		if (diagnostic.severity == Diagnostic.ERROR || diagnostic.severity == Diagnostic.WARNING) {
			for (Diagnostic child : diagnostic.children) {
				switch (child.severity) {
					case Diagnostic.ERROR,
					case Diagnostic.WARNING,
					case Diagnostic.INFO: {
						errors.append(System.lineSeparator);
						errors.append("\t");
						errors.append(child.message);
					}
				}
			}
			
			return false;
		}
		
		var result = true;
		
		for (Network network : block.networks) {
			for (NetworkElement element : network.elements) {
				result = this.validateElement(element, errors);
			}
		}
		
		return result;
	}
	
	private def dispatch boolean validateElement(NetworkElement element, StringBuilder logger) {
		return true;
	}
		
	private def dispatch boolean validateElement(BitLogicInstruction inst, StringBuilder logger) {
		// Validate that all input ports exist and connected
		for (var i = 1; i <= inst.cardinality; i++) {
			val name = String.format("in%d", i);
			if (!inst.ports.stream.anyMatch[p | p.name == name ]) {
				logger.append(String.format(
					"Expected port 'in%d' to exist in a bit logic instruction with the cardinality of %d.", i, inst.cardinality
				));
				return false;
			}
			
			if (inst.getInPort(i).connection instanceof OpenConnection) {
				logger.append(String.format(
					"Expected port 'in%d' to have a connection in bit logic instruction.", i
				));			
				return false;
			}
		}		
		
		return true;
	}
}