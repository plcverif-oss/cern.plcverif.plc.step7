/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *   Xaver Fink - add support for variables of type struct
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd;

import cern.plcverif.plc.tia.fbd.impl.FbdFactoryImpl;

public final class BlockFactory {

    private Block block;
    private FbdFactory emf;

    private BlockFactory(Block block, FbdFactory emf) {
        this.block = block;
        this.emf = emf;
    }

    public InterfaceSection createSection(String name) {
        InterfaceSection section = this.emf.createInterfaceSection();
        section.setName(name);

        this.block.getSections().add(section);

        return section;
    }

    public Member createMember(MemberContainer section, String memberName, String memberType) {
        Member member = this.emf.createMember();
        member.setName(memberName);
        member.setDataType(memberType);

        section.getMembers().add(member);

        return member;
    }

    public Network createNetwork(int number, String title) {
        Network network = this.emf.createNetwork();
        network.setNumber(number);
        network.setTitle(title);
        network.setBlock(this.block);

        OpenConnection openConn = this.emf.createOpenConnection();
        network.setOpen(openConn);

        return network;
    }

    public static BlockFactory initializeBlock(BlockType blockType) {
        if (blockType == BlockType.FUNCTION) {
            throw new IllegalArgumentException(
                "Block type FUNCTION is not allowed for initializeBlock(). Use 'initializeFunction()' for function creation."
            );
        }

        FbdFactory emf = FbdFactoryImpl.init();
        Block block = emf.createBlock();
        block.setBlockType(blockType);

        return new BlockFactory(block, emf);
    }

    public static BlockFactory initializeFunction(String returnType) {
    	FbdFactory emf = FbdFactoryImpl.init();

        Function block = emf.createFunction();
        block.setBlockType(BlockType.FUNCTION);
        block.setReturnType(returnType);

        return new BlockFactory(block, emf);
    }
    
    public static BlockFactory initializeInstanceDB(String dbName) {
    	FbdFactory emf = FbdFactoryImpl.init();

        InstanceDB block = emf.createInstanceDB();
        block.setBlockType(BlockType.INSTANCE_DB);
        block.setDbName(dbName);

        return new BlockFactory(block, emf);
    }

    public Block getBlock() {
        return block;
    }

    public FbdFactory getEmfFactory() {
        return emf;
    }

}
