/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.parser;

public class FbdXmlParserException extends RuntimeException {

	private static final long serialVersionUID = 3258975242689448251L;

	public FbdXmlParserException(String message)
	{
		super(message);
	}

	public FbdXmlParserException(String message, Throwable previous)
	{
		super(message, previous);
	}

	public FbdXmlParserException(Integer network, String message)
	{
		super(String.format("Could not parse TIA Portal XML: %s (in network no. %d).", message, network));
	}

	public FbdXmlParserException(Integer network, Integer uid, String message)
	{
		super(String.format("Could not parse TIA Portal XML: %s (in network no. %d, element %d).", message, network, uid));
	}

}
