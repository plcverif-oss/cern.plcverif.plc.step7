/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *   Ignacio D. Lopez-Miguel - recognition of more built-in functions 
 *   Xaver Fink - add support for variables of type struct
=======
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.google.common.base.Preconditions;

import cern.plcverif.plc.tia.fbd.AddOperation;
import cern.plcverif.plc.tia.fbd.BitLogicInstruction;
import cern.plcverif.plc.tia.fbd.BitLogicOperator;
import cern.plcverif.plc.tia.fbd.Block;
import cern.plcverif.plc.tia.fbd.BlockFactory;
import cern.plcverif.plc.tia.fbd.BlockType;
import cern.plcverif.plc.tia.fbd.Branch;
import cern.plcverif.plc.tia.fbd.Call;
import cern.plcverif.plc.tia.fbd.Coil;
import cern.plcverif.plc.tia.fbd.CoilKind;
import cern.plcverif.plc.tia.fbd.CompareInstruction;
import cern.plcverif.plc.tia.fbd.CompareOperator;
import cern.plcverif.plc.tia.fbd.Connectable;
import cern.plcverif.plc.tia.fbd.Constant;
import cern.plcverif.plc.tia.fbd.DefaultUnknownPartResolver;
import cern.plcverif.plc.tia.fbd.DivOperation;
import cern.plcverif.plc.tia.fbd.FunctionBlockCall;
import cern.plcverif.plc.tia.fbd.Instruction;
import cern.plcverif.plc.tia.fbd.InterfaceSection;
import cern.plcverif.plc.tia.fbd.Jump;
import cern.plcverif.plc.tia.fbd.JumpKind;
import cern.plcverif.plc.tia.fbd.LabelRef;
import cern.plcverif.plc.tia.fbd.MathDataType;
import cern.plcverif.plc.tia.fbd.Member;
import cern.plcverif.plc.tia.fbd.MemberContainer;
import cern.plcverif.plc.tia.fbd.Network;
import cern.plcverif.plc.tia.fbd.NetworkElement;
import cern.plcverif.plc.tia.fbd.NetworkElementFactory;
import cern.plcverif.plc.tia.fbd.NetworkHelper;
import cern.plcverif.plc.tia.fbd.Port;
import cern.plcverif.plc.tia.fbd.PortDirection;
import cern.plcverif.plc.tia.fbd.ReturnKind;
import cern.plcverif.plc.tia.fbd.ReturnValueInstruction;
import cern.plcverif.plc.tia.fbd.Symbol;
import cern.plcverif.plc.tia.fbd.UnknownPart;
import cern.plcverif.plc.tia.fbd.UnknownPartResolver;
import cern.plcverif.plc.tia.fbd.Value;
import cern.plcverif.plc.tia.fbd.WordDataType;
import cern.plcverif.plc.tia.fbd.WordLogicOperator;
import cern.plcverif.plc.tia.fbd.xml.IterableNodeList;
import cern.plcverif.plc.tia.fbd.xml.XmlDocument;
import cern.plcverif.plc.tia.fbd.xml.XmlDocument.XmlDocumentFactory;

public class FbdXmlParser {

	private XmlDocument xml;
	private TiaPortalVersion version;

	private NetworkElementFactory factory;
	private BlockFactory blockFactory;

	private UnknownPartResolver unknownPartPorts;

	private FbdXmlParser(XmlDocument document, TiaPortalVersion version) {
		this.xml = document;
		this.version = version;
	}

	public static Block parse(String path, TiaPortalVersion version) {
		try {
			XmlDocumentFactory factory = XmlDocument.factory();

			switch (version) {
			case V15:
				factory.addPrefix("SI", "http://www.siemens.com/automation/Openness/SW/Interface/v3").addPrefix("NET",
						"http://www.siemens.com/automation/Openness/SW/NetworkSource/FlgNet/v2");
				break;
			case V15_1:
				factory.addPrefix("SI", "http://www.siemens.com/automation/Openness/SW/Interface/v3").addPrefix("NET",
						"http://www.siemens.com/automation/Openness/SW/NetworkSource/FlgNet/v3");
				break;
			case V16:
				factory.addPrefix("SI", "http://www.siemens.com/automation/Openness/SW/Interface/v4").addPrefix("NET",
						"http://www.siemens.com/automation/Openness/SW/NetworkSource/FlgNet/v4");
				break;
			case V17:
			case V18:
				factory.addPrefix("SI", "http://www.siemens.com/automation/Openness/SW/Interface/v5").addPrefix("NET",
						"http://www.siemens.com/automation/Openness/SW/NetworkSource/FlgNet/v4");
				break;
			default:
				throw new IllegalArgumentException("Unsupported TIA Portal version: " + version.toString());
			}

			XmlDocument document = factory.create(path);

			return new FbdXmlParser(document, version).parse();
		} catch (Exception ex) {
			throw new FbdXmlParserException("Could not load XML source: " + ex.getMessage());
		}
	}

	public Block parse() {
		// Verify we are dealing with the requested version
		this.validateEngineeringVersion();

		Node blockXml = this.xml.getSingleNode(this.xml.getRoot(), "//*[contains(name(),'SW.')]");
		if (blockXml == null) {
			throw new FbdXmlParserException("Expected an <SW.Blocks.(FC|FB|OB)> element in input XML.");
		}

		// Find the block type and initialize a new block.
		BlockType blockType = this.resolveBlockType(blockXml);

		if (blockType == BlockType.FUNCTION) {
			// Functions are special in the sense that they have a return data
			// type which
			// must be known before we can initialize our block information
			// object.
			Node returnXml = this.xml.getSingleNode(blockXml,
					"//SI:Sections/SI:Section[@Name='Return']/SI:Member[@Name='Ret_Val']");
			if (returnXml == null) {
				throw new FbdXmlParserException(
						"Expected a Sections/Section[@Name='Return']/Member[@Name='Ret_Val'] element in function.");
			}

			this.blockFactory = BlockFactory
					.initializeFunction(XmlDocument.getAttribute(returnXml, "Datatype").getNodeValue());
		} else if (blockType == BlockType.INSTANCE_DB) {
			Node dbNameNode = this.xml.getSingleNode(blockXml,
					"./AttributeList/InstanceOfName");
			
			if (dbNameNode == null) {
				throw new FbdXmlParserException(
						"Expected a AttributeList/InstanceOfName element in an instance DB.");
			}
			
			this.blockFactory = BlockFactory
					.initializeInstanceDB(dbNameNode.getTextContent());
		} else {
			this.blockFactory = BlockFactory.initializeBlock(blockType);
		}

		Block block = this.blockFactory.getBlock();

		// Find the block name
		block.setName(this.xml.getSingleNode(blockXml, "./AttributeList/Name").getTextContent());

		// Parse all member data found in the interface sections. If it's an instance DB we do not parse them because that information is redundant.
		if (blockType != BlockType.INSTANCE_DB) {
			IterableNodeList sections = this.xml.getNodes(blockXml, ".//Interface/SI:Sections/SI:Section");
			this.parseInterfaceSections(sections);
		}

		// Now that the block-wide information is parsed, move on to the
		// networks
		IterableNodeList networkNodes = this.xml.getNodes(blockXml, ".//SW.Blocks.CompileUnit");
		for (Node networkNodeXml : networkNodes) {
			String programmingLanguage = this.xml.getSingleNode(networkNodeXml, ".//ProgrammingLanguage")
					.getTextContent();
			if (!Arrays.asList("FBD", "F_FBD").contains(programmingLanguage)) {
				throw new FbdXmlParserException("FBD to STL transformation only works on FBD, F_FBD inputs but "
						+ programmingLanguage + " is specified in the file!");
			}

			this.parseNetwork(networkNodeXml, block.getNetworks().size() + 1);
		}

		return block;
	}

	private Network parseNetwork(Node networkNodeXml, int networkNumber) {
		// Extract title information
		// TODO: Is this the most reliable method?
		Node titleXmlText = this.xml.getSingleNode(networkNodeXml,
				".//MultilingualText[@CompositionName='Title']//Text");
		Network network = this.blockFactory.createNetwork(networkNumber,
				titleXmlText == null ? "" : titleXmlText.getTextContent());

		this.factory = new NetworkElementFactory(network, this.blockFactory.getEmfFactory());
		this.unknownPartPorts = new DefaultUnknownPartResolver(this.factory);

		Node flgNetXml = this.xml.getSingleNode(networkNodeXml, ".//NET:FlgNet");

		if (flgNetXml == null) {
			// Just return an empty network
			return network;
		}

		Map<Integer, NetworkElement> elements = new HashMap<>();

		// First, find all labels
		IterableNodeList labelNodeList = this.xml.getNodes(flgNetXml, "./NET:Labels/NET:LabelDeclaration/NET:Label");
		for (Node labelNodeXml : labelNodeList) {
			String name = XmlDocument.getAttribute(labelNodeXml, "Name").getNodeValue();
			this.factory.createLabel(name);
		}

		// Find all symbols
		IterableNodeList accessNodeList = this.xml.getNodes(flgNetXml, "./NET:Parts/NET:Access");
		for (Node accessXml : accessNodeList) {
			// Create an Access object using the scope information in the XML.
			Value access = this.resolveAccessScope(accessXml);
			Integer accessId = Integer.parseInt(XmlDocument.getAttribute(accessXml, "UId").getNodeValue());

			access.setId(accessId);
			elements.put(accessId, access);
		}

		// Find all parts or calls
		IterableNodeList partList = this.xml.getNodes(flgNetXml, "./NET:Parts/NET:Part | ./NET:Parts/NET:Call");
		for (Node partXml : partList) {
			if (partXml.getNodeName().equals("Part")) {
				Integer uid = Integer.parseInt(XmlDocument.getAttribute(partXml, "UId").getNodeValue());
				String name = XmlDocument.getAttribute(partXml, "Name").getNodeValue();

				Instruction part = this.parsePart(partXml, uid, name);
				elements.put(uid, part);
			} else {
				Integer uid = Integer.parseInt(XmlDocument.getAttribute(partXml, "UId").getNodeValue());
				Call call = this.parseCall(partXml);

				elements.put(uid, call);
			}
		}

		// Collect the wires
		IterableNodeList wireList = this.xml.getNodes(flgNetXml, "./NET:Wires/NET:Wire");
		for (Node wireXml : wireList) {
			Integer wireId = Integer.parseInt(XmlDocument.getAttribute(wireXml, "UId").getNodeValue());

			// Collect all XML nodes which are described in the wire.
			List<Node> xmlConnections = IterableNodeList.of(wireXml.getChildNodes()).stream()
					.filter(n -> n.getNodeType() == Node.ELEMENT_NODE).collect(Collectors.toList());

			// If the wire contains an unconnected port, return early.
			if (xmlConnections.stream().anyMatch(n -> n.getNodeName().equalsIgnoreCase("OpenCon"))) {
				continue;
			}

			List<WireConnection> connections = new ArrayList<>();
			List<NetworkElement> connectedElements = new ArrayList<>();

			for (int i = 0; i < xmlConnections.size(); ++i) {
				Node conn = xmlConnections.get(i);

				// The ID attribute should exist both for 'NameCon' and
				// 'IdentCon' elements.
				Node idAttr = XmlDocument.getAttribute(conn, "UId");
				Integer uid = Integer.parseInt(idAttr.getNodeValue());

				NetworkElement elem = elements.get(uid);
				if (elem == null) {
					throw new FbdXmlParserException(networkNumber, wireId,
							"Wire refers to an unknown element UId: " + uid);
				}

				if (conn.getNodeName().equalsIgnoreCase("NameCon")) {
					connections.add(new NamedConnection(uid,
							XmlDocument.getAttribute(conn, "Name").getNodeValue().toLowerCase()));
				} else {
					connections.add(new IdentConnection(uid));
				}

				connectedElements.add(elem);
			}

			if (connections.size() < 2) {
				throw new FbdXmlParserException(networkNumber, wireId, "A wire must have at least two endings!");
			}

			if (connections.size() == 2) {
				// See if the connection is used to connect a part to its
				// operands.
				Optional<Value> connectedValue = connectedElements.stream().filter(e -> e instanceof Value)
						.map(e -> (Value) e).findAny();

				if (connectedValue.isPresent()) {
					// We may have a value connected as an operand
					Value value = connectedValue.get();

					int otherIdx = connectedElements.indexOf(value) == 0 ? 1 : 0;
					NetworkElement otherElem = connectedElements.get(otherIdx);
					WireConnection otherConn = connections.get(otherIdx);

					if (tryToAddValueAsOperand(value, otherElem, otherConn, networkNumber, wireId)) {
						continue;
					}
				}

				// This is a simple binary wire, connecting two elements
				NetworkElement first = connectedElements.get(0);
				NetworkElement second = connectedElements.get(1);

				// At least one of these must be an instruction
				if (first instanceof Instruction && second instanceof Instruction) {
					Instruction firstInst = (Instruction) first;
					Instruction secondInst = (Instruction) second;

					// Both must be named connections
					Preconditions.checkState(connections.stream().allMatch(c -> c instanceof NamedConnection));

					createInstructionWire(connections, firstInst, secondInst);
				} else if (first instanceof Instruction) {
					createInstructionConnection(connections.get(0), (Instruction) first, second, PortDirection.PARAMETER_OUTPUT);
				} else if (second instanceof Instruction) {
					createInstructionConnection(connections.get(1), (Instruction) second, first, PortDirection.PARAMETER_INPUT);
				} else {
					throw new FbdXmlParserException(networkNumber, wireId,
							"One of the wire endings must be an instruction!");
				}
			} else {
				// This is a multiary wire. Create a branch and split it up.
				this.splitBranchWires(connections, connectedElements);
			}
		}

		return network;
	}

	private boolean tryToAddValueAsOperand(Value value, NetworkElement otherElem, WireConnection otherConn,
			int networkNumber, Integer wireId) {
		if (otherElem instanceof Coil && isNamedConnectionOf(otherConn, "operand")) {
			if (!(value instanceof Symbol)) {
				throw new FbdXmlParserException(networkNumber, wireId, "Coils can only have symbol operands!");
			}

			((Coil) otherElem).setOperand((Symbol) value);
		} else if (otherElem instanceof CompareInstruction) {
			CompareInstruction compare = (CompareInstruction) otherElem;

			if (isNamedConnectionOf(otherConn, "in1")) {
				compare.setFirst(value);
			} else if (isNamedConnectionOf(otherConn, "in2")) {
				compare.setSecond(value);
			} else {
				throw new FbdXmlParserException(networkNumber, wireId,
						"Compare instructions must have in1 and in2 ports!");
			}
		} else if (otherElem instanceof Jump && isNamedConnectionOf(otherConn, "label")) {
			Jump jump = (Jump) otherElem;
			if (!(value instanceof LabelRef)) {
				throw new FbdXmlParserException(networkNumber, wireId, "Jumps can only have label operands!");
			}

			jump.setLabel((LabelRef) value);
		} else if (otherElem instanceof ReturnValueInstruction && isNamedConnectionOf(otherConn, "operand")) {
			ReturnValueInstruction ret = (ReturnValueInstruction) otherElem;
			if (!(value instanceof Value)) {
				throw new FbdXmlParserException(networkNumber, wireId, "Jumps can only have label operands!");
			}

			ret.setOperand(value);
		} else {
			return false;
		}

		return true;
	}

	private void createInstructionWire(List<WireConnection> connections, Instruction firstInst,
			Instruction secondInst) {
		NamedConnection firstConn = (NamedConnection) connections.get(0);
		NamedConnection secondConn = (NamedConnection) connections.get(1);

		// In the case of built-in library block calls, it is possible that a
		// port
		// does not exist yet. Create those ports now.
		Port firstPort = NetworkHelper.getOrInsertPort(firstInst, firstConn.name, this.factory);
		Port secondPort = NetworkHelper.getOrInsertPort(secondInst, secondConn.name, this.factory);

		// Create a wire for this connection
		this.factory.createWire(firstPort, secondPort);
	}

	private void createInstructionConnection(WireConnection connection, Instruction inst, NetworkElement other, PortDirection direction) {
		Preconditions.checkState(connection instanceof NamedConnection);
		Preconditions.checkState(other instanceof Connectable);

		NamedConnection firstConn = (NamedConnection) connection;

		Port port = NetworkHelper.getOrInsertPort(inst, firstConn.name, this.factory);
		port.setConnection((Connectable) other);
		if (port.getDirection() == PortDirection.UNKNOWN) {
			port.setDirection(direction);
		}
	}

	private Branch splitBranchWires(List<WireConnection> connections, List<NetworkElement> connectedElements) {
		Branch branch = this.factory.createBranch(connections.size() - 1);

		List<Instruction> instructions = connectedElements.stream().filter(e -> e instanceof Instruction)
				.map(e -> (Instruction) e).collect(Collectors.toList());

		List<String> connectionNames = connections.stream().filter(c -> c instanceof NamedConnection)
				.map(c -> ((NamedConnection) c).name).collect(Collectors.toList());

		// TODO: These should be instructions, but we should check in TIA Portal
		// to be sure
		Preconditions.checkState(instructions.size() == connectedElements.size());
		Preconditions.checkState(connections.stream().allMatch(c -> c instanceof NamedConnection));

		// Add the first connection to the input port
		Port inputSource = NetworkHelper.getOrInsertPort(instructions.get(0), connectionNames.get(0), this.factory);
		this.factory.createWire(inputSource, branch.getInputPort());

		for (int i = 1; i < instructions.size(); ++i) {
			Port target = NetworkHelper.getOrInsertPort(instructions.get(i), connectionNames.get(i), this.factory);
			this.factory.createWire(branch.getOutputPorts().get(i - 1), target);
		}

		return branch;
	}

	// Network parsing
	// -------------------------------------------------------------------------

	private Call parseCall(Node callXml) {
		Node callInfo = XmlDocument.findElementChildOrError(callXml, "CallInfo");

		String calledBlock = XmlDocument.getAttribute(callInfo, "Name").getNodeValue();
		String callType = XmlDocument.getAttribute(callInfo, "BlockType").getNodeValue();

		Call call;
		if (callType.equals("FB")) {
			// Retrieve the data block instance
			String instanceName = globalNameFromNameComponents(
					this.xml.getNodes(callInfo, "./NET:Instance/NET:Component"));
			FunctionBlockCall fbCall = this.factory.createFunctionBlockCall(calledBlock, instanceName);

			call = fbCall;
		} else {
			call = this.factory.createFunctionCall(calledBlock);
		}

		// TODO: If ENO is disabled...

		// Extract the parameters and add them as ports
		List<Element> params = XmlDocument.findElementChildren(callInfo, "Parameter");
		for (Node paramXml : params) {
			String name = XmlDocument.getAttribute(paramXml, "Name").getNodeValue();
			this.factory.createPort(name.toLowerCase(), call);
		}

		for (String negated : this.findNegatedPorts(callXml)) {
			NetworkHelper.getPort(call, negated).setIsNegated(true);
		}

		return call;
	}

	private Instruction parsePart(Node partXml, Integer uid, String name) {
		Instruction part = null;
		if (Arrays.asList("A", "O", "X").contains(name)) {
			part = this.parseBitLogicPart(partXml, name);
		} else if (Arrays.asList("Coil", "SCoil", "RCoil").contains(name)) {
			part = this.parseCoilPart(partXml, name);
		} else if (Arrays.asList("Eq", "Ge", "Gt", "Le", "Lt", "Ne").contains(name)) {
			part = this.parseComparePart(partXml, name);
		} else if (Arrays.asList("And", "Or", "Xor").contains(name)) {
			part = this.parseWordLogicPart(partXml, name);
		} else if (name.equalsIgnoreCase("Move")) {
			part = this.factory.createMoveInstruction(this.findPartCardinality(partXml));
		} else if (name.equalsIgnoreCase("Jump")) {
			part = this.factory.createJump(JumpKind.JMP);
		} else if (name.equalsIgnoreCase("JumpNot")) {
			part = this.factory.createJump(JumpKind.JMPN);
		} else if (Arrays.asList("Return", "ReturnTrue", "ReturnFalse", "ReturnValue").contains(name)) {
			part = this.createReturnByName(name);
		} else if (Arrays.asList("Add").contains(name)) {
			part = this.parseAddPart(partXml, name);
		} else if (Arrays.asList("Div").contains(name)) {
			part = this.parseDivPart(partXml, name);
		} else {
			part = this.parseUnknownPart(partXml, uid, name);
		}

		part.setId(uid);

		return part;
	}

	// Individual part parsing
	// -------------------------------------------------------------------------

	private UnknownPart parseUnknownPart(Node partXml, Integer uid, String name) {
        UnknownPart unknownPart = this.factory.createUnknownPart(name);

        // See if the part has a declared instance
        Optional<Element> instance = XmlDocument.findElementChild(partXml, "Instance");
        if (instance.isPresent()) {
            String instanceName = globalNameFromNameComponents(XmlDocument.findElementChildren(instance.get(), "Component"));
            String scope = instance.get().getAttributes().getNamedItem("Scope").getNodeValue();
            this.factory.createAttribute(unknownPart, "Instance", instanceName);
            this.factory.createAttribute(unknownPart, "Scope", scope);
        }

        // Add other possible attributes
        List<Element> attrXmlNodes = XmlDocument.findElementChildren(partXml, "TemplateValue");
        for (Node attrNode : attrXmlNodes) {
        	String attrName = XmlDocument.getAttribute(attrNode, "Name").getNodeValue();
        	String attrVal  = attrNode.getTextContent();

        	this.factory.createAttribute(unknownPart, attrName, attrVal);
        }

        // Try to find the ports of this part
        boolean foundPorts = this.unknownPartPorts.createPortsFor(unknownPart);

        Node compileUnitXml = partXml.getParentNode().getParentNode();

        IterableNodeList wireNodes = this.xml.getNodes(compileUnitXml, String.format(".//NET:Wires/NET:Wire/NET:NameCon[@UId='%d']", uid));
        
        for (Node w : wireNodes) {
            String portName = w.getAttributes().getNamedItem("Name").getNodeValue().toLowerCase();
            NetworkHelper.getOrInsertPort(unknownPart, portName, this.factory);
        }

        // If the unknown part resolver was unable to create the required ports, try to find the port directions.
        if (!foundPorts) {
	        for (Port p : unknownPart.getPorts()) {
	            p.setDirection(findPortDirection(unknownPart, p.getName()));
	        }
        }

        return unknownPart;
    }

	private BitLogicInstruction parseBitLogicPart(Node partXml, String name) {
		BitLogicOperator operator;
		switch (name) {
		case "A":
			operator = BitLogicOperator.AND;
			break;
		case "O":
			operator = BitLogicOperator.OR;
			break;
		case "X":
			operator = BitLogicOperator.XOR;
			break;
		default:
			throw new IllegalArgumentException(
					"A bit logic instruction's opcode must be one of the following: 'A', 'O', 'X'");
		}

		int cardinality = this.findPartCardinality(partXml);
		BitLogicInstruction result = this.factory.createBitLogicInstruction(operator, cardinality);

		for (String portName : this.findNegatedPorts(partXml)) {
			Port port = NetworkHelper.getPort(result, portName);
			port.setIsNegated(true);
		}

		return result;
	}

	private Instruction parseCoilPart(Node partXml, String name) {
		List<String> negatedPorts = this.findNegatedPorts(partXml);

		CoilKind kind;
		switch (name) {
		case "Coil":
			if (negatedPorts.contains("operand")) {
				// If the operand input is negated, use NEGATED_ASSIGNMENT
				kind = CoilKind.NEGATED_ASSIGNMENT;
				negatedPorts.remove("operand");
			} else {
				kind = CoilKind.ASSIGNMENT;
			}
			break;
		case "SCoil":
			kind = CoilKind.SET;
			break;
		case "RCoil":
			kind = CoilKind.RESET;
			break;
		default:
			throw new IllegalArgumentException(
					"A coil instruction's opcode must be one of the following: 'Coil', 'SCoil', 'RCoil'");
		}

		Coil coil = this.factory.createCoil(kind);
		negatedPorts.forEach(p -> {
			coil.getPort(p).setIsNegated(true);
		});

		return coil;
	}

	private Instruction parseComparePart(Node partXml, String name) {
		CompareOperator kind;
		switch (name) {
		case "Eq":
			kind = CompareOperator.EQ;
			break;
		case "Ne":
			kind = CompareOperator.NE;
			break;
		case "Gt":
			kind = CompareOperator.GT;
			break;
		case "Ge":
			kind = CompareOperator.GE;
			break;
		case "Lt":
			kind = CompareOperator.LT;
			break;
		case "Le":
			kind = CompareOperator.LE;
			break;
		default:
			throw new IllegalArgumentException(
					"A compare instruction's opcode must be one of the following: 'Eq', 'Ge', 'Gt', 'Le', 'Lt', 'Ne'");
		}

		MathDataType dataType = this.findMathDataType(partXml);

		CompareInstruction compare = this.factory.createCompare(kind, dataType);

		if (this.findNegatedPorts(partXml).contains("out")) {
			compare.getOutPort().setIsNegated(true);
		}

		return compare;
	}

	private Instruction parseWordLogicPart(Node partXml, String name) {
		WordLogicOperator operator;
		switch (name) {
		case "And":
			operator = WordLogicOperator.AND;
			break;
		case "Or":
			operator = WordLogicOperator.OR;
			break;
		case "Xor":
			operator = WordLogicOperator.XOR;
			break;
		default:
			throw new IllegalArgumentException(
					"A word logic instruction's opcode must be one of the following: 'AND', 'OR', 'XOR'");
		}

		WordDataType dataType = this.findWordDataType(partXml);
		int cardinality = this.findPartCardinality(partXml);

		return this.factory.createWordLogicInstruction(operator, dataType, cardinality);
	}

	private Instruction parseAddPart(Node partXml, String name) {
		AddOperation operator = AddOperation.ADD;

		MathDataType dataType = this.findMathDataType(partXml);
		int cardinality = this.findPartCardinality(partXml);

		return this.factory.createAddInstruction(operator, dataType, cardinality);
	}

	private Instruction parseDivPart(Node partXml, String name) {
		DivOperation operator = DivOperation.DIV;

		MathDataType dataType = this.findMathDataType(partXml);

		return this.factory.createDivInstruction(operator, dataType);
	}

	private Instruction createReturnByName(String name) {
		if (name.equals("ReturnValue")) {
			return this.factory.createReturnValue();
		}

		ReturnKind kind;
		switch (name) {
		case "Return":
			kind = ReturnKind.RLO;
			break;
		case "ReturnTrue":
			kind = ReturnKind.TRUE;
			break;
		case "ReturnFalse":
			kind = ReturnKind.FALSE;
			break;
		default:
			throw new IllegalArgumentException(
					"A return instruction's opcode must be on of the following: 'Return', 'ReturnTrue', 'ReturnFalse', 'ReturnValue'");
		}

		return this.factory.createReturn(kind);
	}

	private List<String> findNegatedPorts(Node partXml) {
		return XmlDocument.findElementChildren(partXml, "Negated").stream()
				.map(node -> XmlDocument.getAttribute(node, "Name").getNodeValue()).collect(Collectors.toList());
	}

	private int findPartCardinality(Node partXml) {
		return Integer.parseInt(getTemplateValue(partXml, "Card"));
	}

	private MathDataType findMathDataType(Node partXml) {
		String srcType = getTemplateValue(partXml, "SrcType");
		switch (srcType) {
		case "Int":
		case "Word":
			return MathDataType.INT;
		case "DInt":
		case "DWord":
			return MathDataType.DINT;
		case "Real":
		default:
			throw new FbdXmlParserException("Unknown SrcType in Part's template value: " + srcType);
		}
	}

	private WordDataType findWordDataType(Node partXml) {
		String srcType = getTemplateValue(partXml, "SrcType");
		switch (srcType) {
		case "Word":
			return WordDataType.WORD;
		case "DWord":
			return WordDataType.DWORD;
		default:
			throw new FbdXmlParserException("Unknown SrcType in Part's template value: " + srcType);
		}
	}

	// Block information parsing
	// -------------------------------------------------------------------------

	private void parseInterfaceSections(IterableNodeList sections) {
		for (Node sectionXml : sections) {
			String name = XmlDocument.getAttribute(sectionXml, "Name").getNodeValue();
			InterfaceSection section = this.blockFactory.createSection(name);

			parseMembersRecursively(sectionXml, section);
		}
	}

	private List<Member> parseMembersRecursively(Node node, MemberContainer section) {
		List<Member> members = new ArrayList<>();

		for (Node memberXml : XmlDocument.findElementChildren(node, "Member")) {
			String memberName = XmlDocument.getAttribute(memberXml, "Name").getNodeValue();
			String memberType = XmlDocument.getAttribute(memberXml, "Datatype").getNodeValue();

			Member member = this.blockFactory.createMember(section, memberName, memberType);

			parseMembersRecursively(memberXml, member);

			Optional<Element> startValueXml = XmlDocument.findElementChild(memberXml, "StartValue");
			if (startValueXml.isPresent()) {
				member.setStartValue(startValueXml.get().getTextContent());
			} else {
				member.setStartValue("");
			}

			members.add(member);
		}

		return members;

	}

	private BlockType resolveBlockType(Node blockXml) {
		BlockType blockType;
		if (blockXml.getNodeName().equals("SW.Blocks.FC")) {
			blockType = BlockType.FUNCTION;
		} else if (blockXml.getNodeName().equals("SW.Blocks.FB")) {
			blockType = BlockType.FUNCTION_BLOCK;
		} else if (blockXml.getNodeName().equals("SW.Blocks.OB")) {
			blockType = BlockType.ORGANIZATION_BLOCK;
		} else if (blockXml.getNodeName().equals("SW.Blocks.InstanceDB")) {
			blockType = BlockType.INSTANCE_DB;
		} else if (blockXml.getNodeName().equals("SW.Blocks.GlobalDB")) {
			blockType = BlockType.GLOBAL_DB;
		} else {
			throw new FbdXmlParserException(String.format(
					"Invalid block declaration encountered: %s."
							+ "Allowed declaration elements are: SW.Blocks.FC, SW.Blocks.FB, SW.Blocks.OB.",
					blockXml.getNodeName()));
		}

		return blockType;
	}

	private Value resolveAccessScope(Node accessXml) {
		String scope = accessXml.getAttributes().getNamedItem("Scope").getNodeValue();

		Value access;
		if (scope.equals("LocalVariable") || scope.equals("GlobalVariable")) {
			String name = "#" + globalNameFromAccess((Element) accessXml);
			Symbol symbol = this.factory.createSymbol(name);

			access = symbol;
		} else if (scope.equals("LocalConstant")) {
			String name = String.format("#\"%s\"", XmlDocument
					.getAttribute(XmlDocument.findElementChildOrError(accessXml, "Constant"), "Name").getNodeValue());
			Symbol symbol = this.factory.createSymbol(name);

			access = symbol;
		} else if (scope.equals("TypedConstant") || scope.equals("LiteralConstant")) {
			Node constantNode = XmlDocument.findElementChildOrError(accessXml, "Constant");
			Node valueNode = XmlDocument.findElementChildOrError(constantNode, "ConstantValue");

			String valueStr = valueNode.getTextContent();
			Constant constant = this.factory.createConstant(valueStr);
			constant.setValue(valueStr);

			access = constant;
		} else if (scope.equals("Label")) {
			String labelName = XmlDocument.getAttribute(XmlDocument.findElementChildOrError(accessXml, "Label"), "Name")
					.getNodeValue();
			LabelRef labelRef = this.factory.createLabelRef(labelName);

			access = labelRef;
		} else {
			throw new FbdXmlParserException("Unrecognized access scope encountered in the XML: " + scope);
		}

		return access;
	}

	// Helper methods
	// -------------------------------------------------------------------------

	private void validateEngineeringVersion() {
		Node engineering = this.xml.getSingleNode(this.xml.getRoot(), "/Document/Engineering");
		if (engineering == null) {
			throw new FbdXmlParserException("Expected an <Enginnering> element in the input XML.");
		}

		String versionStr = XmlDocument.getAttribute(engineering, "version").getNodeValue();
		if ((version == TiaPortalVersion.V15 && !versionStr.equals("V15"))
				|| (version == TiaPortalVersion.V15_1 && !versionStr.equals("V15.1"))) {
			throw new FbdXmlParserException(String.format(
					"FBD parser for version %s cannot handle XMLs with engineering version %s", version, versionStr));
		}
	}

	private PortDirection findPortDirection(UnknownPart instruction, String portName) {
		// If we have an 'en' or 'eno' port, assume that they are control flow
		// inputs/outputs.
		if (portName.equals("en")) {
			return PortDirection.CONTROL_FLOW_INPUT;
		} else if (portName.equals("eno")) {
			return PortDirection.CONTROL_FLOW_OUTPUT;
		}

		if (portName.equalsIgnoreCase("S") || portName.equalsIgnoreCase("R1") || portName.equalsIgnoreCase("E_STOP")
				|| portName.equalsIgnoreCase("ACK_NEC") || portName.equalsIgnoreCase("ACK")
				|| portName.equalsIgnoreCase("TIME_DEL")) {
			return PortDirection.PARAMETER_INPUT;
		} else if (portName.equalsIgnoreCase("q") && !instruction.getName().equalsIgnoreCase("ESTOP1")) {
			return PortDirection.CONTROL_FLOW_OUTPUT;
		}

		boolean hasEnPort = instruction.getPorts().stream().anyMatch(p -> p.getName().equals("en"));

		// Try the same with 'in' and 'out'
		if (portName.startsWith("in")) {
			return !hasEnPort ? PortDirection.CONTROL_FLOW_INPUT : PortDirection.PARAMETER_INPUT;
		} else if (portName.startsWith("out")) {
			return !hasEnPort ? PortDirection.CONTROL_FLOW_OUTPUT : PortDirection.PARAMETER_OUTPUT;
		}

		return PortDirection.UNKNOWN;
	}

	private static String globalNameFromNameComponents(Iterable<? extends Node> nodes) {
		List<String> nameComponents = new ArrayList<>();
		for (Node node : nodes) {
			nameComponents.add('\"' + getSymbolNameFromComponent(node) + '\"');
		}

		return String.join(".", nameComponents);
	}

	private String globalNameFromAccess(Element node) {
		Optional<Node> symbol = IterableNodeList.of(node.getChildNodes()).stream()
				.filter(c -> c.getNodeType() == Node.ELEMENT_NODE).filter(c -> c.getNodeName().equals("Symbol"))
				.findFirst();

		if (!symbol.isPresent()) {
			throw new FbdXmlParserException("Expected a 'Symbol' element within an local variable access.");
		}

		List<Node> components = IterableNodeList.of(symbol.get().getChildNodes()).stream()
				.filter(c -> c.getNodeType() == Node.ELEMENT_NODE).filter(c -> c.getNodeName().equals("Component"))
				.collect(Collectors.toList());

		return globalNameFromNameComponents(components);
	}

	private static String getSymbolNameFromComponent(Node componentNode) {
		return XmlDocument.getAttribute(componentNode, "Name").getNodeValue();
	}

	private static String getTemplateValue(Node node, String attrName) {
		Optional<Element> templateValue = XmlDocument.findElementChildren(node).stream()
				.filter(e -> e.getNodeName().equals("TemplateValue"))
				.filter(e -> e.getAttribute("Name").equals(attrName)).findFirst();

		if (!templateValue.isPresent()) {
			throw new FbdXmlParserException("Expected 'Name' attribute in a 'TemplateValue' element.");
		}

		return templateValue.get().getTextContent();
	}

	// Helper classes
	// -------------------------------------------------------------------------

	private static boolean isNamedConnectionOf(WireConnection conn, String name) {
		return conn instanceof WireConnection && ((NamedConnection) conn).name.equalsIgnoreCase(name);
	}

	private abstract static class WireConnection {
		public int targetId;

		public WireConnection(int targetId) {
			this.targetId = targetId;
		}
	}

	private static class NamedConnection extends WireConnection {
		public String name;

		public NamedConnection(int targetId, String name) {
			super(targetId);
			this.targetId = targetId;
			this.name = name;
		}
	}

	private static class IdentConnection extends WireConnection {
		public IdentConnection(int targetId) {
			super(targetId);
			this.targetId = targetId;
		}
	}
}
