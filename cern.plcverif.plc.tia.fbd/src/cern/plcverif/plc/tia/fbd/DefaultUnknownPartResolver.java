/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd;

public class DefaultUnknownPartResolver implements UnknownPartResolver {
    private NetworkElementFactory factory;

    public DefaultUnknownPartResolver(NetworkElementFactory factory) {
        this.factory = factory;
    }

    @Override
    public boolean createPortsFor(UnknownPart part) {
        String name = part.getName().toUpperCase();

        switch (name) {
        case "TON":
        case "TOF":
        case "TP":
            Port in = this.factory.createPort("in", part, PortDirection.CONTROL_FLOW_INPUT);
            Port q = this.factory.createPort("q", part, PortDirection.CONTROL_FLOW_OUTPUT);
            this.factory.createPort("pt", part, PortDirection.PARAMETER_INPUT);
            this.factory.createPort("et", part, PortDirection.PARAMETER_OUTPUT);

            part.setControlFlowInput(in);
            part.setControlFlowOutput(q);
            
            return true;
        case "PBOX":
        case "NBOX":
        	this.factory.createPort("in", part, PortDirection.CONTROL_FLOW_INPUT);
        	this.factory.createPort("out", part, PortDirection.CONTROL_FLOW_OUTPUT);
        	this.factory.createPort("bit", part, PortDirection.PARAMETER_INPUT);
        	
        	return true;
        case "PCOIL":
        case "NCOIL":
        	this.factory.createPort("in", part, PortDirection.CONTROL_FLOW_INPUT);
        	this.factory.createPort("out", part, PortDirection.CONTROL_FLOW_OUTPUT);
        	this.factory.createPort("bit", part, PortDirection.PARAMETER_INPUT);
        	this.factory.createPort("operand", part, PortDirection.PARAMETER_INPUT);
        	
        	return true;
        case "PCONTACT":
        case "NCONTACT":
        	this.factory.createPort("out", part, PortDirection.CONTROL_FLOW_OUTPUT);
        	this.factory.createPort("bit", part, PortDirection.PARAMETER_INPUT);
        	this.factory.createPort("operand", part, PortDirection.PARAMETER_INPUT);

        	return true;
        default:
        	// Could not match anything.
        	return false;
        }
    }
}
