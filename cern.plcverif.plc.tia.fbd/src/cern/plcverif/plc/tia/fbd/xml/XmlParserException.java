/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.xml;

public class XmlParserException extends RuntimeException {

	private static final long serialVersionUID = 4288503354370488611L;

	public XmlParserException() {
		super();
	}

	public XmlParserException(String message, Throwable previous) {
		super(message, previous);
	}

	public XmlParserException(String message) {
		super(message);
	}

	public XmlParserException(Throwable previous) {
		super(previous);
	}

}
