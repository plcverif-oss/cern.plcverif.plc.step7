/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/

package cern.plcverif.plc.tia.fbd.xml;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.common.base.Preconditions;

public final class IterableNodeList implements Iterable<Node> {
	
	private NodeList list;
	
	private IterableNodeList(NodeList list) {
		this.list = list;
	}
	
	public int getLength() {
		return this.list.getLength();
	}
	
	public Node item(int idx) {
		return this.list.item(idx);
	}
	
	public Stream<Node> stream() {
		return StreamSupport.stream(this.spliterator(), /*parallel=*/false);
	}
	
	@Override
	public Iterator<Node> iterator() {
		return new Iterator<Node>() {
			private int idx;
			
			@Override
			public Node next() {
				if (!hasNext()) {
					throw new NoSuchElementException();
				}
				
				return IterableNodeList.this.list.item(idx++);
			}
			
			@Override
			public boolean hasNext() {
				return idx < IterableNodeList.this.list.getLength();
			}
		};
	}
	
	public static IterableNodeList of(NodeList list) {
		Preconditions.checkNotNull(list);
		
		return new IterableNodeList(list);
	}
}
