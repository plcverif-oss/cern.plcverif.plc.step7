/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.xml;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.common.base.Preconditions;

public final class XmlDocument {

    private Document document;
    private XPath xpath;

    private XmlDocument(Document document, XPath xpath) {
        this.document = document;
        this.xpath = xpath;
    }

    public Node getRoot() {
        return this.document;
    }

    public Node selectSingleNodeOrError(Object parent, String xpathStr, String errorMessage) {
        Node node = this.getSingleNode(parent, xpathStr);
        if (node == null) {
            throw new NoSuchElementException(errorMessage);
        }

        return node;
    }

    public IterableNodeList getNodes(Object parent, String xpathStr) {
    	try {
            XPathExpression expr = this.xpath.compile(xpathStr);
            NodeList list = (NodeList) (expr.evaluate(parent, XPathConstants.NODESET));
            
            return IterableNodeList.of(list);
    		
    	} catch (XPathExpressionException ex) {
    		throw new XmlParserException("Invalid XPath expression encountered.", ex);
    	}
    }

    public Node getSingleNode(Object parent, String xpathStr) {
        try {
            XPathExpression expr = this.xpath.compile(xpathStr);
            return (Node) expr.evaluate(parent, XPathConstants.NODE);
        } catch (XPathExpressionException ex) {
            throw new XmlParserException("Invalid XPath expression encountered.", ex);
        }
    }
    
    public String getNamespaceURI(String prefix) {
    	return this.document.lookupNamespaceURI(prefix);
    }
    
    public static Node getAttribute(Node node, String name) {
        Preconditions.checkNotNull(node);
        Preconditions.checkArgument(node.getNodeType() == Node.ELEMENT_NODE, "Can only get attributes of XML elements!");

        Node attr = node.getAttributes().getNamedItem(name);
        if (attr == null) {
            throw new XmlParserException(String.format("Expected attribute with the name '%s' in XML node '%s'!", name, node.getNodeName()));
        }

        return attr;
    }

    public static List<Element> findElementChildren(Node node) {
    	return filterElementChildren(node)
    		.collect(Collectors.toList());
    }
    
    public static List<Element> findElementChildren(Node node, String name) {
    	return filterElementChildren(node)
    		.filter(c -> c.getNodeName().equals(name))
    		.map(c -> (Element) c)
    		.collect(Collectors.toList());
    }
    
    public static Optional<Element> findElementChild(Node node, String name) {
    	List<Element> children = findElementChildren(node, name);
    	if (children.size() != 1) {
    		return Optional.empty();
    	}
    	
    	return Optional.of(children.get(0));
    }
    
    public static Element findElementChildOrError(Node node, String name) {
    	Optional<Element> child = findElementChild(node, name);
    	if (!child.isPresent()) {
    		throw new XmlParserException(String.format("Expected unique child element with the name '%s' within element '%s'.", name, node.getNodeName()));
    	}
    	
    	return child.get();
    }
    
    private static Stream<Element> filterElementChildren(Node node) {
    	return IterableNodeList.of(node.getChildNodes()).stream()
        	.filter(c -> c.getNodeType() == Node.ELEMENT_NODE)
        	.map(c -> (Element) c);    	   	
    }
    
    public static XmlDocumentFactory factory() {
        return new XmlDocumentFactory();
    }

    public static final class XmlDocumentFactory {
        private Map<String, String> prefixes = new HashMap<>();

        public XmlDocumentFactory addPrefix(String prefix, String uri) {
            this.prefixes.put(prefix, uri);
            return this;
        }

        public XmlDocument create(String path) throws XmlParserException, IOException {
        	try {
	            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	            factory.setNamespaceAware(true);
	            factory.setValidating(false);
	            factory.setFeature("http://xml.org/sax/features/validation", false);
	            factory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
	            factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
	
	            DocumentBuilder builder = factory.newDocumentBuilder();
	            Document document = builder.parse(path);
	
	            XPath xpath = XPathFactory.newInstance().newXPath();
	            xpath.setNamespaceContext(new XmlNamespaceContext(this.prefixes));
	
	            return new XmlDocument(document, xpath);
        	} catch (ParserConfigurationException ex) {
        		throw new XmlParserException("Could not configure XML parser: " + ex.getMessage(), ex);
        	} catch (IOException ex) {
        		throw new XmlParserException(String.format("Could not open file '%s': %s", path, ex.getMessage()), ex);
        	} catch (SAXException ex) {
        		throw new XmlParserException(String.format("Could not parse XML file '%s': %s", path, ex.getMessage()), ex);
        	}
        }
    }

    private static final class XmlNamespaceContext implements NamespaceContext {
        private Map<String, String> prefixes = new HashMap<>();

        private XmlNamespaceContext(Map<String, String> prefixes) {
            this.prefixes = prefixes;
        }

        @Override
        public String getNamespaceURI(String prefix) {
            String uri = this.prefixes.get(prefix);

            if (uri == null) {
                throw new IllegalArgumentException("Unknown XML prefix: " + prefix);
            }

            return uri;
        }

        @Override
        public String getPrefix(String namespaceURI) {
            Optional<String> prefix = this.getPrefixesFor(namespaceURI).findFirst();
            if (!prefix.isPresent()) {
                throw new IllegalArgumentException("Unknown XML URI: " + namespaceURI);
            }

            return prefix.get();
        }

        @Override
        public Iterator<String> getPrefixes(String namespaceURI) {
            return this.getPrefixesFor(namespaceURI).iterator();
        }

        private Stream<String> getPrefixesFor(String namespaceURI) {
            return this.prefixes.entrySet().stream().filter(e -> e.getValue().equals(namespaceURI)).map(e -> e.getKey());
        }
    }
}
