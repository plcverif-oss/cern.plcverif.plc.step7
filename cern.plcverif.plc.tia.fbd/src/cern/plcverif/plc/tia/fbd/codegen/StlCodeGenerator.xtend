/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *   Ignacio D. Lopez-Miguel - jumps fixed
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.codegen

import cern.plcverif.plc.tia.fbd.BitLogicInstruction
import cern.plcverif.plc.tia.fbd.BlockCall
import cern.plcverif.plc.tia.fbd.Branch
import cern.plcverif.plc.tia.fbd.Call
import cern.plcverif.plc.tia.fbd.Coil
import cern.plcverif.plc.tia.fbd.CoilKind
import cern.plcverif.plc.tia.fbd.CompareInstruction
import cern.plcverif.plc.tia.fbd.Connectable
import cern.plcverif.plc.tia.fbd.FunctionBlockCall
import cern.plcverif.plc.tia.fbd.FunctionCall
import cern.plcverif.plc.tia.fbd.Jump
import cern.plcverif.plc.tia.fbd.MoveInstruction
import cern.plcverif.plc.tia.fbd.NetworkHelper
import cern.plcverif.plc.tia.fbd.OpenConnection
import cern.plcverif.plc.tia.fbd.Port
import cern.plcverif.plc.tia.fbd.ReturnInstruction
import cern.plcverif.plc.tia.fbd.ReturnKind
import cern.plcverif.plc.tia.fbd.ReturnValueInstruction
import cern.plcverif.plc.tia.fbd.Symbol
import cern.plcverif.plc.tia.fbd.UnknownPart
import cern.plcverif.plc.tia.fbd.Value
import cern.plcverif.plc.tia.fbd.Wire
import cern.plcverif.plc.tia.fbd.WordLogicInstruction
import com.google.common.base.Splitter
import java.util.HashMap
import java.util.List
import java.util.Map

import static cern.plcverif.plc.tia.fbd.codegen.GeneratorHelper.quote;
import cern.plcverif.plc.tia.fbd.AddInstruction
import cern.plcverif.plc.tia.fbd.DivInstruction

class StlCodeGenerator implements NetworkElementTranslator {

	GeneratorState generatorState = new GeneratorState();
	UnknownPartTranslator unknownPartTranslator;

	new(UnknownPartTranslator unknownPartTranslator) {
		this.unknownPartTranslator = unknownPartTranslator;
		this.unknownPartTranslator.setNetworkElementTranslator(this)
	}

	def dispatch String translate(Coil coil) {
		val mnemonic = switch (coil.kind) {
			case ASSIGNMENT: "="
			case NEGATED_ASSIGNMENT: "=" // intentional
			case SET: "S"
			case RESET: "R"
		}

		val operation = '''«mnemonic» «translateConnection(coil.operand)»;'''
		var String connection = this.createPrologue(coil.inPort)

		return '''
		«connection»
		«IF coil.kind == CoilKind.NEGATED_ASSIGNMENT »
			NOT;
			« operation »
			NOT;
		«ELSE»
			« operation »
		«ENDIF»
		«IF coil.outPort.isIsNegated »
			NOT;
		«ENDIF» 
		''';
	}

	def dispatch String translate(BitLogicInstruction bl) {
		val mnemonic = switch (bl.operator) {
			case AND: "A"
			case OR: "O"
			case XOR: "X"
		}

		return '''
« FOR port : NetworkHelper.portsBeginWith(bl, "in") »
« val String connStr = this.translateConnection(port.connection) »
« mnemonic »« IF port.isIsNegated »N« ENDIF »« IF !isSimpleConnection(port.connection) || port.connection instanceof Wire  »(;
« connStr »
);
« ELSE »««« We need the extra whitespace here »»» 
 « connStr »;
« ENDIF »
« ENDFOR »
« IF bl.outPort.isIsNegated »NOT;« ENDIF »
'''
	}

	def dispatch String translate(Branch branch) {
		var variable = this.generatorState.nextTempVariable();

		val String ret = '''
			«this.translateConnection(branch.inputPort.connection)»
			= «variable»;
			A «variable»;
		''';

		this.generatorState.addBranchVariable(branch, variable);
		return ret;
	}

	def dispatch String translate(CompareInstruction compare) {
		val String op = switch (compare.operator) {
			case EQ: "=="
			case NE: "<>"
			case GE: ">="
			case GT: ">"
			case LE: "<="
			case LT: "<"
		}
		val String type = switch (compare.dataType) {
			case INT: "I"
			case DINT: "D"
			case REAL: "R"
		}

		val mnemonic = op + type

		return '''
			L «compare.first»;
			L «compare.second»;
			«mnemonic»;
			« IF compare.outPort.isIsNegated »NOT;«ENDIF»
		'''
	}

	def dispatch String translate(Call call) {
		var String prologue = "";
		var String epilogue = "";

		val en = call.en;
		val eno = call.eno;
		
		if (!(en.connection instanceof OpenConnection)) {
			val String label = this.generatorState.nextJumpLabel();

			prologue = '''
				« this.createPrologue(en) » 
				JCN « label »;
			'''
			epilogue = '''
				« label»: NOP 0;
			'''
		}
		
		if (!(eno.connection instanceof OpenConnection)) {
			epilogue = "A BR;\n" + epilogue;
		}

		return '''
			« prologue »
			«this.translateCall(call)»
			« epilogue »
		'''
	}
	
	def dispatch String translate(Jump jump) {
		val String mnemonic = switch (jump.kind) {
			case JMP: "JC"
			case JMPN: "JCN"
		}
		
		// Every jump should have a condition. We include it with AND as the instruction SET will be placed before the jump.
		// If there is no condition (no input port), we do not add any instruction.
		return '''
			« this.createPrologue(jump.inPort)»
			« mnemonic » « jump.label »;
		'''
	}
		
	def dispatch String translate(AddInstruction add) {
		var String prologue = "";
		var String epilogue = "";
		
		val op = "+"
		val String type = switch (add.dataType) {
			case INT: "I"
			case DINT: "D"
			case REAL: "R"
		}

		val mnemonic = op + type
		
		val en = add.en;
		val eno = add.eno;
		
		if (!(en.connection instanceof OpenConnection)) {
			val String label = this.generatorState.nextJumpLabel();

			prologue = '''
				« this.createPrologue(en) »
				JCN « label »;
			'''
			epilogue = '''
				« label»: NOP 0;
			'''
		}
		
		if (!(eno.connection instanceof OpenConnection)) {
			epilogue = "A BR;\n" + epilogue;
		}

		return '''
		« prologue »
		L «this.translateConnection(add.inputPorts.get(0).connection)»;
		L «this.translateConnection(add.inputPorts.get(1).connection)»;
		«mnemonic»;
		«IF add.inputPorts.size > 2»
		« FOR i : 2..<add.inputPorts.size »
		L « this.translateConnection(add.inputPorts.get(i).connection) »;
		« mnemonic »;
		« ENDFOR »
		«ENDIF»
		T «this.translateConnection(add.outPort.connection)»;
		« IF add.outPort.isIsNegated »NOT;«ENDIF»
		« epilogue »
				'''
	}

	def dispatch String translate(DivInstruction div) {
		var String prologue = "";
		var String epilogue = "";
		
		val op = "/"
		val String type = switch (div.dataType) {
			case INT: "I"
			case DINT: "D"
			case REAL: "R"
		}

		val mnemonic = op + type

		return '''
		« prologue »
		L «this.translateConnection(div.inputPorts.get(0).connection)»;
		L «this.translateConnection(div.inputPorts.get(1).connection)»;
		«mnemonic»;
		T «this.translateConnection(div.outPort.connection)»;
		« IF div.outPort.isIsNegated »NOT;«ENDIF»
		« epilogue »
		'''
	}

	def dispatch String translate(UnknownPart part) {
		// Try to translate the control flow port of this instruction.
		// If there is no dedicated control flow port, just use an empty string
		val String prologue = if (part.controlFlowInput !== null) this.createPrologue(part.controlFlowInput) else "";		
		return '''
			« prologue »
			« this.unknownPartTranslator.translate(part, prologue) »
		''';
	}

	def dispatch String translate(ReturnInstruction ret) {
		val String prologue = this.createPrologue(ret.getPort("in"))
		
		if (ret.kind == ReturnKind.RLO || ret.kind == ReturnKind.TRUE) {
			// According to the docs, these should be the same.
			// Both get executed if and only if RLO is true, but due to this,
			// the return value will always be true.
			return '''
« prologue »
««« Save the value of RLO into BR so it will serve as a return value »»»
SAVE;
««« Do a conditional block end »»»
BEC;
'''
		} else if (ret.kind == ReturnKind.FALSE) {
			return '''
« prologue »
««« We need to return FALSE in BR, so invert the RLO to be able to save that into BR »»»
NOT;
SAVE;
««« Invert the RLO back to the original state »»»
NOT;
««« This gets executed only if RLO is true »»
BEC;
'''
		}
	}

	def dispatch String translate(ReturnValueInstruction ret) {
		val String label = this.generatorState.nextJumpLabel();
		return '''
« this.createPrologue(ret.getPort("in")) »
««« Skip the whole thing if the RLO is not true at this point »»»
JCN «label»;
««« Clear the RLO so the next operation will be a first computation »»»
CLR;
««« Load the return value into the RLO »»»
A « ret.operand »;
««« Save the return value into BR »»»
SAVE;
««« The RLO was originally true if we entered this code path  »»»
SET;
««« An unconditional return is enough - we have done the conditional jump before »»»
BEU;
«label»: NOP 0;
'''
	}

	// Calls and built-in calls
	// -------------------------------------------------------------------------
	def dispatch String translateCall(MoveInstruction move) {
		return '''
L «this.translateConnection(move.inputPort.connection)»;
«FOR port : move.outputPorts»
T « this.translateConnection(port.connection) »;
«ENDFOR»
'''
	}

	def dispatch String translateCall(WordLogicInstruction wl) {
		val String op = switch (wl.operator) {
			case AND: "A"
			case OR: "O"
			case XOR: "XO"
		}
		val String type = switch (wl.dataType) {
			case WORD: "W"
			case DWORD: "D"
		}

		val mnemonic = op + type

		return '''
L «this.translateConnection(wl.inputPorts.get(0).connection)»;
L «this.translateConnection(wl.inputPorts.get(1).connection)»;
«mnemonic»;
«IF wl.inputPorts.size > 2»
« FOR i : 2..<wl.inputPorts.size »««« If we have multiple input ports, just keep "pushing" the ACCU value forward. »»»
L « this.translateConnection(wl.inputPorts.get(i).connection) »;
« mnemonic »;
« ENDFOR »
«ENDIF»
T «this.translateConnection(wl.outPort.connection)»;
'''
	}

	def dispatch translateCall(BlockCall call) {
		// TODO: Some form of cycle detection shoudl be added here, just in case.
				
		val List<Port> complexParams = call.ports.filter[p|p != call.en && p != call.eno].filter [ p |
			// We either want wires connecting to other blocks or negated ports
			(p.connection instanceof Wire && (p.connection as Wire).target.instruction == call) || (p.isIsNegated)
		].toList();

		val Map<Connectable, String> paramVariables = new HashMap();
		for (Port port : complexParams) {
			paramVariables.put(port.connection, this.generatorState.nextTempVariable());
		}

		return '''
			«FOR port : complexParams»
			« this.createPrologue(port) »
			= « paramVariables.get(port.connection) »;
			«ENDFOR»
			«this.getCallName(call)»(
				«FOR port : NetworkHelper.argumentPorts(call) SEPARATOR ","»
					« quote(port.name) » := « IF paramVariables.containsKey(port.connection) »« paramVariables.get(port.connection) »« ELSE »« this.translateConnection(port.connection) »« ENDIF»
				«ENDFOR»
			);
		'''
	}

	private def dispatch String getCallName(FunctionBlockCall call) {
		return '''CALL «quote(call.name)», «quote(call.instanceName)»''';
	}

	private def dispatch String getCallName(FunctionCall call) {
		return '''CALL «quote(call.name)»''';
	}

	private def dispatch String getCallName(UnknownPart call) {
		return '''CALL «quote(call.name)»''';
	}

	def dispatch String translateConnection(OpenConnection conn) {
		return ""
	}

	def dispatch String translateConnection(Value value) {
		return value.toString();
	}
	
	def dispatch String translateConnection(Symbol symbol) {
		// Drop the leading "#" if it is already present
		val subStrIdx = if (symbol.name.startsWith("#")) 1 else 0;
		
		// Put all components in quotes and rejoin them again
		val splitter = Splitter.on(".");
		return "#" + splitter.split(symbol.toString().substring(subStrIdx)).map[s | quote(s)].join(".");	
	}

	def dispatch String translateConnection(Wire wire) {
		if (wire.source.instruction instanceof Branch) {
			var String variable = this.generatorState.getBranchVariable(wire.source.instruction as Branch);
			if (variable !== null) {
				return '''A « variable »;''';
			}
		}


		return this.translate(wire.source.instruction);
	}

	private def String createPrologue(Port port) {		
		if (port.connection instanceof Value) {
			return '''A«IF port.isIsNegated »N«ENDIF» «translateConnection(port.connection)»;'''
		}
		
		return '''
			« this.translateConnection(port.connection) »
			« IF port.isIsNegated»NOT;« ENDIF » 
		'''
	}

	private def boolean isSimpleConnection(Connectable conn) {
		if (conn instanceof Value) {
			return true;
		}

		if (conn instanceof Wire) {
			val source = (conn as Wire).source.instruction
			return source instanceof Branch && this.generatorState.hasBranchVariable(source as Branch);
		}

		return false;
	}
	
	override getState() {
		return this.generatorState;
	}

}