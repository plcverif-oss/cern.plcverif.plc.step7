/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.codegen;

import cern.plcverif.plc.tia.fbd.Connectable;
import cern.plcverif.plc.tia.fbd.Instruction;

public interface NetworkElementTranslator {

    public String translate(Instruction instruction);

    public String translateConnection(Connectable connection);

    public GeneratorState getState();

}
