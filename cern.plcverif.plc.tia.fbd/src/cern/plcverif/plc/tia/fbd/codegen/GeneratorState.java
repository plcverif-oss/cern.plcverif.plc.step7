/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.codegen;

import java.util.HashMap;
import java.util.Map;

import cern.plcverif.plc.tia.fbd.Branch;

public final class GeneratorState {
	
	public static final String LABEL_NAME_PREFIX = "PLCVERIF_LABEL_";
	
	private int localBit = 0;
	private int localByte = 0;
	private int labelCnt = 0;
	
	private Map<Branch, String> branchVariables = new HashMap<>();
	
	public String nextTempVariable() {
		if (localBit == 8) {
			localByte = localByte + 1;
			localBit = 0;
		}

		return String.format("%%L%d.%d", localByte, localBit++);
	}

	public String nextJumpLabel() {
		return LABEL_NAME_PREFIX + (labelCnt++);
	}
	
	public void addBranchVariable(Branch branch, String variableName) {
		this.branchVariables.put(branch, variableName);
	}
	
	public String getBranchVariable(Branch branch) {
		return this.branchVariables.get(branch);
	}
	
	public boolean hasBranchVariable(Branch branch) {
		return this.branchVariables.containsKey(branch);
	}
	
	public void reset() {
		localBit = 0;
		localByte = 0;
		labelCnt = 0;
		branchVariables.clear();
	}
	
}
