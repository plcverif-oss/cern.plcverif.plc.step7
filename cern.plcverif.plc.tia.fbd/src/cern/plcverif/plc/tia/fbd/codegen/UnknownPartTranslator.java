/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.codegen;

import cern.plcverif.plc.tia.fbd.UnknownPart;

public interface UnknownPartTranslator {

	/**
	 * Translates an unknown part.
	 * 
	 * @param part	The part to translate.
	 * @param rloValue The preceding instructions which produce the RLO value for this part.
	 * 
	 * @return The string representation of this unknown part.
	 */
    public String translate(UnknownPart part, String rloValue);

    /**
     * Sets the translator used to translate known connections.
     */
    public void setNetworkElementTranslator(NetworkElementTranslator translator);

}