/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.codegen

import cern.plcverif.plc.tia.fbd.OpenConnection
import cern.plcverif.plc.tia.fbd.Port
import cern.plcverif.plc.tia.fbd.PortDirection
import cern.plcverif.plc.tia.fbd.UnknownPart
import cern.plcverif.plc.tia.fbd.Wire
import java.util.LinkedHashMap
import java.util.Map

import static cern.plcverif.plc.tia.fbd.codegen.GeneratorHelper.*
import cern.plcverif.plc.tia.fbd.InstanceScope
import cern.plcverif.plc.tia.fbd.utils.CodeGenUtils

class DefaultUnknownPartTranslator implements UnknownPartTranslator {
	
	NetworkElementTranslator generator;
	GeneratorState generatorState;
	
	override String translate(UnknownPart inst, String prologue) {
		val String name = inst.name.toUpperCase()
		
		var teststring = 
		
		
		if (#["TON", "TOF", "TP"].contains(name)) {
			return translateTimers(
				name,
				inst.getAttribute("Instance").value,
				prologue,
				inst.getPort("q"),
				inst.getPort("pt"),
				inst.getPort("et")
			);
		} else if (name == "PCOIL" || name == "NCOIL") {			
			val bitVal = inst.getPort("bit")
			val q = inst.getPort("out")
			val label = generatorState.nextJumpLabel()
			
			return '''
				« prologue »
				« IF name == "PCOIL" »FP« ELSE »FN« ENDIF » « generator.translateConnection(bitVal.connection) »;
				JCN « label »;
				= « generator.translateConnection(q.connection) »;
				« IF q.isIsNegated »NOT;« ENDIF »
				« label »: NOP 0;
			'''
		} else if (name == "PBOX" || name == "NBOX") {
			val bitVal = inst.getPort("bit")
			val q = inst.getPort("out")

			return '''
				« prologue »
				« IF name == "PBOX" »FP« ELSE »FN« ENDIF » « generator.translateConnection(bitVal.connection) »;
				« IF q.isIsNegated »NOT;« ENDIF »
			'''
		} else if (name == "PCONTACT" || name == "NCONTACT") {
			val bitVal = inst.getPort("bit")
			val q = inst.getPort("out")
			val op = inst.getPort("operand")
			
			return '''
				« prologue »
				A « generator.translateConnection(op.connection) »;
				« IF name == "PCONTACT" »FP« ELSE »FN« ENDIF » « generator.translateConnection(bitVal.connection) »;
				« IF q.isIsNegated »NOT;« ENDIF »
			'''
		} 
		
		val inputPorts = inst.ports.filter[p | p.direction == PortDirection.PARAMETER_INPUT].toList()
		val outputPorts = inst.ports.filter[p | p.direction == PortDirection.PARAMETER_OUTPUT].toList()

		val Map<Port, String> nonTrivialConnections = new LinkedHashMap();
		for (Port p : inputPorts) {
			if (p.connection instanceof Wire) {
				val String tempVariable = generatorState.nextTempVariable();
				nonTrivialConnections.put(p, tempVariable);
			}
		}
		
		
		if (name == "ESTOP1") {
			val InstanceScope scope = InstanceScope.get(inst.getAttribute("Scope").value)
			val e_stop = inst.getPort("e_stop")
			val ack_nec = inst.getPort("ack_nec")
			val ack = inst.getPort("ack")
			val time_del = inst.getPort("time_del")
			val q = inst.getPort("q")
			val q_delay = inst.getPort("q_delay")
			val ack_req = inst.getPort("ack_req")
			val diag = inst.getPort("diag")
						
			var stringRep = '''
			« prologue »
			« FOR entry : nonTrivialConnections.entrySet() »
				« generator.translateConnection(entry.key.connection) »
				= « entry.value »;
			« ENDFOR »
			CALL « IF scope == InstanceScope.GLOBAL_VARIABLE »« quote(name) », « ENDIF »« quote(inst.getAttribute("Instance").value) »(
			«IF !(e_stop.connection instanceof OpenConnection)»
				"E_STOP" := « IF nonTrivialConnections.containsKey(e_stop) »« nonTrivialConnections.get(e_stop) »« ELSE »« generator.translateConnection(e_stop.connection) »« ENDIF »,
			«ENDIF»
			«IF !(ack_nec.connection instanceof OpenConnection)»
				"ACK_NEC" := « IF nonTrivialConnections.containsKey(ack_nec) »« nonTrivialConnections.get(ack_nec) »« ELSE »« generator.translateConnection(ack_nec.connection) »« ENDIF »,
			«ENDIF»
			«IF !(ack.connection instanceof OpenConnection)»
				"ACK" := « IF nonTrivialConnections.containsKey(ack) »« nonTrivialConnections.get(ack) »« ELSE »« generator.translateConnection(ack.connection) »« ENDIF »,
			«ENDIF»
			«IF !(time_del.connection instanceof OpenConnection)»
				"TIME_DEL" := « IF nonTrivialConnections.containsKey(time_del) »« nonTrivialConnections.get(time_del) »« ELSE »« generator.translateConnection(time_del.connection) »« ENDIF »,
			«ENDIF»
			«IF !(q.connection instanceof OpenConnection)»
				"Q" => « generator.translateConnection(q.connection) »,
			«ENDIF»
			«IF !(q_delay.connection instanceof OpenConnection)»
				"Q_DELAY" => « generator.translateConnection(q_delay.connection) »,
			«ENDIF»
			«IF !(ack_req.connection instanceof OpenConnection)»
				"ACK_REQ" => « generator.translateConnection(ack_req.connection) »,
			«ENDIF»
			«IF !(diag.connection instanceof OpenConnection)»
				"DIAG" => « generator.translateConnection(diag.connection) »
			«ENDIF»);
		'''
			stringRep = CodeGenUtils.sanitizeCallParameters(stringRep)
			return stringRep;
		
			
		} else if (name == "SR" || name == "RS") {
			
			
			val s = if (name == "SR") inst.getPort("s") else inst.getPort("r")
			val r1 = if (name == "SR") inst.getPort("r1") else inst.getPort("s1")
			val operand = inst.getPort("operand")
	
			return '''
			« prologue »
			« FOR entry : nonTrivialConnections.entrySet() »
				« generator.translateConnection(entry.key.connection) »
				= « entry.value »;
			« ENDFOR »
			A « IF nonTrivialConnections.containsKey(s) »« nonTrivialConnections.get(s) »« ELSE »« generator.translateConnection(s.connection) »« ENDIF »;
			« IF name == "SR" »S « ELSE »R « ENDIF » « IF nonTrivialConnections.containsKey(operand) »« nonTrivialConnections.get(operand) »« ELSE »« generator.translateConnection(operand.connection) »« ENDIF »;
			A « IF nonTrivialConnections.containsKey(r1) »« nonTrivialConnections.get(r1) »« ELSE »« generator.translateConnection(r1.connection) »« ENDIF »;
			« IF name == "SR" »R « ELSE »S « ENDIF » « IF nonTrivialConnections.containsKey(operand) »« nonTrivialConnections.get(operand) »« ELSE »« generator.translateConnection(operand.connection) »« ENDIF »;
			A « IF nonTrivialConnections.containsKey(operand) »« nonTrivialConnections.get(operand) »« ELSE »« generator.translateConnection(operand.connection) »« ENDIF »;
			'''
		} 
		
		// Generate a simple call instruction as a fallback
		val InstanceScope scope = InstanceScope.getByName(inst.getAttribute("Scope").value)
		return '''
			« prologue »
			« FOR entry : nonTrivialConnections.entrySet() »
				« generator.translateConnection(entry.key.connection) »
				= « entry.value »;
			« ENDFOR »
			CALL « IF scope == InstanceScope.GLOBAL_VARIABLE »« quote(name) », « ENDIF »« quote(inst.getAttribute("Instance").value) »(
				« FOR Port p : inputPorts + outputPorts SEPARATOR "," »
				« IF !(p.connection instanceof OpenConnection) »
					« quote(p.name) » := « IF nonTrivialConnections.containsKey(p) »« nonTrivialConnections.get(p) »« ELSE »« generator.translateConnection(p.connection) »« ENDIF »
				« ENDIF »
				« ENDFOR »
			);
		'''
	}

	private def String translateTimers(String name, String instanceName, String prologue, Port q, Port pt, Port et) {
		val String tempVar = this.generatorState.nextTempVariable()
		val String outVar = this.generatorState.nextTempVariable()
		
		return '''
			« prologue »
			= « tempVar »;
			CALL « quote(name) », « quote(instanceName) »(
				"IN" := « tempVar »,
				"PT" := « pt.connection »,
				"Q" => « outVar »«IF !(et.connection instanceof OpenConnection)»,
				"ET" => « et.connection »
			«ENDIF»
			);
			A « outVar »;
		'''
	}
	
	override setNetworkElementTranslator(NetworkElementTranslator translator) {
		this.generator = translator
		this.generatorState = translator.state
	}

}
	