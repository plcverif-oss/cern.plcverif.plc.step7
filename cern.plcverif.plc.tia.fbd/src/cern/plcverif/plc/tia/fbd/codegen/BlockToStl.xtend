/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *   Xaver Fink - add support for variables of type struct
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.codegen

import cern.plcverif.plc.tia.fbd.Block
import cern.plcverif.plc.tia.fbd.BlockValidator
import cern.plcverif.plc.tia.fbd.Function
import cern.plcverif.plc.tia.fbd.InterfaceSection
import cern.plcverif.plc.tia.fbd.Member
import cern.plcverif.plc.tia.fbd.Network
import cern.plcverif.plc.tia.fbd.InstanceDB

class BlockToStl {
		
	TerminalFinderStrategy terminalFinder
	StlCodeGenerator stlCodeGen
	UnknownPartTranslator unknownParts
	
	private new(TerminalFinderStrategy terminalFinder) {
		this.terminalFinder = terminalFinder;
		this.unknownParts = new DefaultUnknownPartTranslator();
		this.stlCodeGen = new StlCodeGenerator(unknownParts);
	}

	static def String translate(Block block, TerminalFinderStrategy terminalFinder) {
		return new BlockToStl(terminalFinder)
			.translateBlock(block);
	}

	private def String translateBlock(Block block) {
		// Do a validation pass before attempting to translate
		val errors = new StringBuilder();
		if (!BlockValidator.validate(block, errors)) {
			throw new IllegalArgumentException("Invalid block state.\n" + errors.toString());
		}
		
		return '''
			« block.declarationTypeString » "« block.name »"« IF block instanceof Function » : « block.returnType »«ENDIF»
			« IF block instanceof InstanceDB »"« block.dbName »"«ENDIF»
			« FOR InterfaceSection section : block.sections »
			« IF section.name != "Return" && section.name != "None" && !section.members.isEmpty() »
				« getSectionName(section.name) »
					« FOR Member member : section.members »
						«translateMember(member)»
					« ENDFOR »
				END_VAR
			« ENDIF »
			« ENDFOR »
			BEGIN
			« FOR network : block.networks »
			« this.translateNetwork(network) »
			« ENDFOR »
			END_«block.declarationTypeString»
		'''
	}
	
	private def String translateMember(Member member){
		if (member.members.isEmpty){
			return '''"« member.name »" : « member.dataType »«IF !member.startValue.empty » := « member.startValue»«ENDIF»;'''
		}else{
			return '''
				"« member.name »" : « member.dataType »«IF !member.startValue.empty » := « member.startValue»«ENDIF»
					« FOR Member subMember : member.members »
						«translateMember(subMember)»
					« ENDFOR »
				END_«member.dataType»;'''
		}
	}

	private def String translateNetwork(Network network) {		
		var terminalParts = terminalFinder.findTerminalInstructions(network);

		return '''
			NETWORK // («network.number»)
			TITLE = '«network.title»'
			«FOR label : network.labels»
			« label.name »:
			«ENDFOR»
			«FOR terminal : terminalParts »
			SET;
			« stlCodeGen.translate(terminal)»
			« ENDFOR »
		'''
	}
	
	private def String getSectionName(String name) {
		return switch name.toUpperCase() {
			case "INPUT": "VAR_INPUT"
			case "OUTPUT": "VAR_OUTPUT"
			case "TEMP": "VAR_TEMP"
			case "INOUT": "VAR_IN_OUT"
			//case "CONSTANT": "VAR_CONSTANT"
			case "CONSTANT": "VAR" // TODO: This is no longer valid in TIA Portal, but this is how PLCverif handles it
			case "STATIC": "VAR"
			case "NONE": "VAR"
			default: throw new IllegalArgumentException("Unknown interface section: " + name)
		}
	}
}