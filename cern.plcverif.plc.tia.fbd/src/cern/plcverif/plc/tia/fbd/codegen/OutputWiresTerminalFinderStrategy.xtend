/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.codegen

import cern.plcverif.plc.tia.fbd.Call
import cern.plcverif.plc.tia.fbd.Coil
import cern.plcverif.plc.tia.fbd.Instruction
import cern.plcverif.plc.tia.fbd.Jump
import cern.plcverif.plc.tia.fbd.Network
import cern.plcverif.plc.tia.fbd.PortDirection
import cern.plcverif.plc.tia.fbd.ReturnInstruction
import cern.plcverif.plc.tia.fbd.ReturnValueInstruction
import cern.plcverif.plc.tia.fbd.Wire
import cern.plcverif.plc.tia.fbd.impl.SymbolImpl

/**
 * A terminal finding strategy which uses knowledge about the port interface
 * of certain instructions to guess the terminal nodes of a network.
 */
class OutputWiresTerminalFinderStrategy implements TerminalFinderStrategy {
	
	Network network
	
	override findTerminalInstructions(Network network) {
		this.network = network
		
		return network.elements
			.filter[e | e instanceof Instruction]
			.map[e | e as Instruction]
			.filter[i | this.isTerminal(i)]
			.toList();		
	}
	
	private def dispatch boolean isTerminal(Instruction instruction) {
		val cfOuts = instruction.ports.filter[p | p.direction == PortDirection.CONTROL_FLOW_OUTPUT].toList()
		if (cfOuts.size == 1) {
			if (cfOuts.get(0).connection == this.network.open) {
				return true	
			}
			else {
				return false
			}
			//return cfOuts.get(0).connection == this.network.open
		}
		
		if (cfOuts.size == 0) {
			// If we have not found suitable outputs, and there are no explicitly declared control flow outputs,
			// we treat this node as terminal.
			return true;
		}
		
		// As a fallback, try to find an ENO port
		val eno = instruction.ports.findFirst[p | p.name == "eno"]
		if (eno !== null) {
			return eno.connection == this.network.open
		}
		
		// Try to find an "out" port if there is no "eno"
		val out = instruction.ports.findFirst[p | p.name == "out"]
		if (out !== null) {
			return !(out.connection instanceof Wire);
		}

		// One last try with "q"
		val q = instruction.ports.findFirst[p | p.name == "q"]
		if (q !== null) {
			return !(q.connection instanceof Wire)
		}

		return false
	}
	
	private def dispatch boolean isTerminal(Coil coil) {
		return isOpenPort(coil, "out")
	}
	
	private def dispatch boolean isTerminal(Call call) {
		// We will consider calls terminals if their 'ENO' port is not connected
		isOpenPort(call, "eno")
	}
	
	private def dispatch boolean isTerminal(Jump jump) {
		return true
	}
	
	private def dispatch boolean isTerminal(ReturnInstruction ret) {
		return true
	}
	
	private def dispatch boolean isTerminal(ReturnValueInstruction ret) {
		return true
	}
	
	private def boolean isOpenPort(Instruction inst, String portName) {
		return inst.getPort(portName).connection == this.network.open
	}
}