/*******************************************************************************
 * (C) Copyright CERN 2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink 
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.utils;

import org.eclipse.core.runtime.Platform;

import cern.plcverif.base.common.utils.OsUtils.OperatingSystem;

public final class OSUtils {
	private OSUtils() {
		// Utility class.
	}

	public enum OperatingSystem {
		Win32("win32"), Linux("linux"), MacOs("macosx"), Unknown("");

		private String key;

		private OperatingSystem(String key) {
			this.key = key;
		}

		public String getKey() {
			return key;
		}

		public static OperatingSystem parse(String key) {
			for (OperatingSystem literal : OperatingSystem.values()) {
				if (literal.getKey().equalsIgnoreCase(key)) {
					return literal;
				}
			}

			return Unknown;
		}
	}

	public static OperatingSystem getCurrentOs() {
		String currentOs = Platform.getOS().toLowerCase();
		return OperatingSystem.parse(currentOs);
	}

	public static String getCurrentOsText() {
		return Platform.getOS().toLowerCase();
	}
}