/*******************************************************************************
 * (C) Copyright CERN 2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink 
 *******************************************************************************/
package cern.plcverif.plc.tia.fbd.utils;

import cern.plcverif.plc.tia.fbd.utils.OSUtils.OperatingSystem;

public final class CodeGenUtils {
	
	private CodeGenUtils() {
		// utility class
	}
	
	public static String sanitizeCallParameters(String input) {
		String result = input;
		if (OSUtils.getCurrentOs() == OperatingSystem.Win32) {
			result = result.replace(",\r\n);", ");");
		} else {
			result = result.replace(",\n);", ");");
		}
		return result;
	}
}
