package cern.plcverif.plc.tia.fbd.utils;

import cern.plcverif.plc.tia.fbd.BitLogicInstruction
import cern.plcverif.plc.tia.fbd.Coil
import cern.plcverif.plc.tia.fbd.CompareInstruction
import cern.plcverif.plc.tia.fbd.FunctionBlockCall
import cern.plcverif.plc.tia.fbd.FunctionCall
import cern.plcverif.plc.tia.fbd.Instruction
import cern.plcverif.plc.tia.fbd.Jump
import cern.plcverif.plc.tia.fbd.JumpKind
import cern.plcverif.plc.tia.fbd.LabelRef
import cern.plcverif.plc.tia.fbd.Network
import cern.plcverif.plc.tia.fbd.NetworkElement
import cern.plcverif.plc.tia.fbd.Port
import cern.plcverif.plc.tia.fbd.Symbol
import cern.plcverif.plc.tia.fbd.Value
import cern.plcverif.plc.tia.fbd.Wire
import cern.plcverif.plc.tia.fbd.WordLogicInstruction
import java.util.HashMap

class NetworkDotPrinter {
	
	private new() {}
	
	private def static String printPort(Port port) {
		return '''«IF port.isIsNegated»/«ENDIF»«port.name»«IF port.connection instanceof Value»=« printElement(port.connection).replace('"', "\\\"") »«ENDIF»'''
	}
	
	def static String print(Network network) {
		var sb = new StringBuilder();
		sb.append('''
digraph G {
	node [shape=record];
	rankdir=LR;
''');
		
		val elemMap = new HashMap<NetworkElement, Integer>();
		
		var idx = 0;
		
		for (NetworkElement elem : network.elements.filter[e | !(e instanceof Wire)]) {
			val labelName = printElement(elem);
			if (elem instanceof Instruction) {
				val inst = elem as Instruction
				sb.append('''« idx »[label="« labelName
					.replace('"', "\\\"")
					.replace('<', "\\<")
					.replace('>', "\\>") »«FOR port : inst.ports »|<«port.name»>«printPort(port)»«ENDFOR»"];'''
				);
				sb.append("\n");
				
			}
			
			elemMap.put(elem, idx);			
			idx++;
		}
		
		for (Instruction inst : network.elements.filter[e|e instanceof Instruction].map[e|e as Instruction]) {
			for (Port port : inst.ports) {
				var conn = port.connection
				if (conn instanceof Wire) {
					// We only consider output wires to avoid duplicates					
					var Wire wire = conn as Wire;

					if (wire.target.instruction !== inst) {
						val source = elemMap.get(wire.source.instruction)
						val target = elemMap.get(wire.target.instruction)

						sb.append('''«source»:«wire.source.name» -> «target»:«wire.target.name»''');
						sb.append("\n");
					}
				}
			}
		}	
				
		
		sb.append("};");
		
		return sb.toString();
	}
	
	private def static dispatch String printElement(NetworkElement elem) {
		return elem.eClass.name;
	}
	
	private def static dispatch String printElement(BitLogicInstruction inst) {
		return '''« inst.operator»'''
	}
	
	private def static dispatch String printElement(CompareInstruction inst) {
		return '''« inst.operator»<« inst.dataType»>(« print(inst.first) », «print(inst.second)»)'''
	}
	
	private def static dispatch String printElement(FunctionCall call) {
		return '''FunctionCall(« call.name »)'''
	}
	
	private def static dispatch String printElement(FunctionBlockCall call) {
		return '''FunctionCall(« call.name », « call.instanceName »)'''
	}
	
	private def static dispatch String printElement(Coil coil) {
		return '''Coil(« coil.kind», « print(coil.operand) »)'''
	}
	
	private def static dispatch String printElement(WordLogicInstruction wl) {
		return '''« wl.operator»<«wl.dataType»>()'''
	}
	
	
	private def static dispatch String printElement(Jump jump) {
		return '''Jump«IF jump.kind == JumpKind.JMPN»Not«ENDIF»(« print(jump.label) »)'''
	}
	
	private def static dispatch String printElement(Symbol symbol) {
		return '''« symbol.name »'''
	}
	
	private def static dispatch String printElement(LabelRef labelRef) {
		return '''« labelRef.label »'''
	}
	
}